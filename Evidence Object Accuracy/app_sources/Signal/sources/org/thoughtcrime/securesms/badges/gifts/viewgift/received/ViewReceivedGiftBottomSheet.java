package org.thoughtcrime.securesms.badges.gifts.viewgift.received;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheetConfiguration;
import org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository;
import org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState;
import org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgeDisplay112;
import org.thoughtcrime.securesms.badges.models.BadgeDisplay160;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.components.settings.models.IndeterminateLoadingCircle;
import org.thoughtcrime.securesms.components.settings.models.OutlinedSwitch;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: ViewReceivedGiftBottomSheet.kt */
@Metadata(d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 02\u00020\u0001:\u00010B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\u0010\u0010#\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\b\u0010$\u001a\u00020\u0018H\u0016J\u0012\u0010%\u001a\u00020\u00182\b\u0010&\u001a\u0004\u0018\u00010'H\u0002J,\u0010(\u001a\u00020\u0018*\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\"2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0002J\u0014\u0010-\u001a\u00020\u0018*\u00020\u001c2\u0006\u0010.\u001a\u00020/H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\b8BX\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000e8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u00020\u00128BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0013\u0010\u0014¨\u00061"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftBottomSheet;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "errorDialog", "Landroid/content/DialogInterface;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "messageId", "", "getMessageId", "()J", "progressDialog", "Landroidx/appcompat/app/AlertDialog;", "sentFrom", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getSentFrom", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "viewModel", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState;", "isGiftBadgeExpired", "", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "isGiftBadgeRedeemed", "onDestroy", "onRedemptionError", "throwable", "", "presentForUnexpiredGiftBadge", "controlState", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$ControlState;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "presentSubheading", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewReceivedGiftBottomSheet extends DSLSettingsBottomSheetFragment {
    private static final String ARG_GIFT_BADGE;
    private static final String ARG_MESSAGE_ID;
    private static final String ARG_SENT_FROM;
    public static final Companion Companion = new Companion(null);
    public static final String REQUEST_KEY;
    public static final String RESULT_NOT_NOW;
    private static final String TAG;
    private DialogInterface errorDialog;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private AlertDialog progressDialog;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ViewReceivedGiftViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$viewModel$2
        final /* synthetic */ ViewReceivedGiftBottomSheet this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            RecipientId access$getSentFrom = ViewReceivedGiftBottomSheet.access$getSentFrom(this.this$0);
            long access$getMessageId = ViewReceivedGiftBottomSheet.access$getMessageId(this.this$0);
            ViewGiftRepository viewGiftRepository = new ViewGiftRepository();
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new ViewReceivedGiftViewModel.Factory(access$getSentFrom, access$getMessageId, viewGiftRepository, new BadgeRepository(requireContext));
        }
    });

    /* compiled from: ViewReceivedGiftBottomSheet.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[ViewReceivedGiftState.ControlState.values().length];
            iArr[ViewReceivedGiftState.ControlState.DISPLAY.ordinal()] = 1;
            iArr[ViewReceivedGiftState.ControlState.FEATURE.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
            int[] iArr2 = new int[GiftBadge.RedemptionState.values().length];
            iArr2[GiftBadge.RedemptionState.REDEEMED.ordinal()] = 1;
            $EnumSwitchMapping$1 = iArr2;
        }
    }

    @JvmStatic
    public static final void show(FragmentManager fragmentManager, MmsMessageRecord mmsMessageRecord) {
        Companion.show(fragmentManager, mmsMessageRecord);
    }

    public ViewReceivedGiftBottomSheet() {
        super(0, null, 0.0f, 7, null);
    }

    /* compiled from: ViewReceivedGiftBottomSheet.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftBottomSheet$Companion;", "", "()V", "ARG_GIFT_BADGE", "", "ARG_MESSAGE_ID", "ARG_SENT_FROM", "REQUEST_KEY", "RESULT_NOT_NOW", "TAG", "kotlin.jvm.PlatformType", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager, MmsMessageRecord mmsMessageRecord) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
            ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet = new ViewReceivedGiftBottomSheet();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ViewReceivedGiftBottomSheet.ARG_SENT_FROM, mmsMessageRecord.getRecipient().getId());
            GiftBadge giftBadge = mmsMessageRecord.getGiftBadge();
            Intrinsics.checkNotNull(giftBadge);
            bundle.putByteArray(ViewReceivedGiftBottomSheet.ARG_GIFT_BADGE, giftBadge.toByteArray());
            bundle.putLong(ViewReceivedGiftBottomSheet.ARG_MESSAGE_ID, mmsMessageRecord.getId());
            viewReceivedGiftBottomSheet.setArguments(bundle);
            viewReceivedGiftBottomSheet.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }

    static {
        Companion = new Companion(null);
        String tag = Log.tag(ViewReceivedGiftBottomSheet.class);
        TAG = tag;
        Intrinsics.checkNotNullExpressionValue(tag, "TAG");
        REQUEST_KEY = tag;
    }

    public final RecipientId getSentFrom() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_SENT_FROM);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    public final long getMessageId() {
        return requireArguments().getLong(ARG_MESSAGE_ID);
    }

    public final ViewReceivedGiftViewModel getViewModel() {
        return (ViewReceivedGiftViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        BadgeDisplay112.INSTANCE.register(dSLSettingsAdapter);
        OutlinedSwitch.INSTANCE.register(dSLSettingsAdapter);
        BadgeDisplay160.INSTANCE.register(dSLSettingsAdapter);
        IndeterminateLoadingCircle.INSTANCE.register(dSLSettingsAdapter);
        AlertDialog create = new MaterialAlertDialogBuilder(requireContext()).setView(R.layout.redeeming_gift_dialog).setCancelable(false).create();
        Intrinsics.checkNotNullExpressionValue(create, "MaterialAlertDialogBuild…le(false)\n      .create()");
        this.progressDialog = create;
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = DonationError.Companion.getErrorsForSource(DonationErrorSource.GIFT_REDEMPTION).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewReceivedGiftBottomSheet.$r8$lambda$11WCjQJJBR5NJE1YVkRORkKq_0o(ViewReceivedGiftBottomSheet.this, (DonationError) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "DonationError\n      .get…or(donationError)\n      }");
        lifecycleDisposable2.plusAssign(subscribe);
        LifecycleDisposable lifecycleDisposable3 = this.lifecycleDisposable;
        Disposable subscribe2 = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$$ExternalSyntheticLambda1
            public final /* synthetic */ ViewReceivedGiftBottomSheet f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewReceivedGiftBottomSheet.$r8$lambda$0Kt0__HfgDnOgShf4YQ1R_MBxlM(DSLSettingsAdapter.this, this.f$1, (ViewReceivedGiftState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "viewModel.state.observeO…MappingModelList())\n    }");
        lifecycleDisposable3.plusAssign(subscribe2);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m402bindAdapter$lambda0(ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet, DonationError donationError) {
        Intrinsics.checkNotNullParameter(viewReceivedGiftBottomSheet, "this$0");
        viewReceivedGiftBottomSheet.onRedemptionError(donationError);
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m403bindAdapter$lambda1(DSLSettingsAdapter dSLSettingsAdapter, ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet, ViewReceivedGiftState viewReceivedGiftState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(viewReceivedGiftBottomSheet, "this$0");
        Intrinsics.checkNotNullExpressionValue(viewReceivedGiftState, "state");
        dSLSettingsAdapter.submitList(viewReceivedGiftBottomSheet.getConfiguration(viewReceivedGiftState).toMappingModelList());
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        AlertDialog alertDialog = this.progressDialog;
        if (alertDialog == null) {
            Intrinsics.throwUninitializedPropertyAccessException("progressDialog");
            alertDialog = null;
        }
        alertDialog.hide();
    }

    public final void onRedemptionError(Throwable th) {
        String str = TAG;
        Log.w(str, "onRedemptionError", th, true);
        if (this.errorDialog != null) {
            Log.i(str, "Already displaying an error dialog. Skipping.");
            return;
        }
        DonationErrorDialogs donationErrorDialogs = DonationErrorDialogs.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        this.errorDialog = donationErrorDialogs.show(requireContext, th, new DonationErrorDialogs.DialogCallback(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$onRedemptionError$1
            final /* synthetic */ ViewReceivedGiftBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs.DialogCallback
            public void onDialogDismissed() {
                FragmentKt.findNavController(this.this$0).popBackStack();
            }
        });
    }

    private final DSLConfiguration getConfiguration(ViewReceivedGiftState viewReceivedGiftState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(viewReceivedGiftState, this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$getConfiguration$1
            final /* synthetic */ ViewReceivedGiftState $state;
            final /* synthetic */ ViewReceivedGiftBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                if (this.$state.getGiftBadge() == null) {
                    dSLConfiguration.customPref(IndeterminateLoadingCircle.INSTANCE);
                } else if (ViewReceivedGiftBottomSheet.access$isGiftBadgeExpired(this.this$0, this.$state.getGiftBadge())) {
                    ExpiredGiftSheetConfiguration expiredGiftSheetConfiguration = ExpiredGiftSheetConfiguration.INSTANCE;
                    GiftBadge giftBadge = this.$state.getGiftBadge();
                    final ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet = this.this$0;
                    AnonymousClass1 r2 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$getConfiguration$1.1
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            FragmentActivity requireActivity = viewReceivedGiftBottomSheet.requireActivity();
                            AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
                            Context requireContext = viewReceivedGiftBottomSheet.requireContext();
                            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                            requireActivity.startActivity(companion.subscriptions(requireContext));
                            viewReceivedGiftBottomSheet.requireActivity().finish();
                        }
                    };
                    final ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet2 = this.this$0;
                    expiredGiftSheetConfiguration.forExpiredGiftBadge(dSLConfiguration, giftBadge, r2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$getConfiguration$1.2
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            viewReceivedGiftBottomSheet2.dismissAllowingStateLoss();
                        }
                    });
                } else {
                    AlertDialog alertDialog = null;
                    if (this.$state.getGiftBadge().getRedemptionState() == GiftBadge.RedemptionState.STARTED) {
                        AlertDialog access$getProgressDialog$p = ViewReceivedGiftBottomSheet.access$getProgressDialog$p(this.this$0);
                        if (access$getProgressDialog$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("progressDialog");
                        } else {
                            alertDialog = access$getProgressDialog$p;
                        }
                        alertDialog.show();
                    } else {
                        AlertDialog access$getProgressDialog$p2 = ViewReceivedGiftBottomSheet.access$getProgressDialog$p(this.this$0);
                        if (access$getProgressDialog$p2 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("progressDialog");
                        } else {
                            alertDialog = access$getProgressDialog$p2;
                        }
                        alertDialog.hide();
                    }
                    if (this.$state.getRecipient() != null && !ViewReceivedGiftBottomSheet.access$isGiftBadgeRedeemed(this.this$0, this.$state.getGiftBadge())) {
                        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                        String string = this.this$0.requireContext().getString(R.string.ViewReceivedGiftBottomSheet__s_sent_you_a_gift, this.$state.getRecipient().getShortDisplayName(this.this$0.requireContext()));
                        Intrinsics.checkNotNullExpressionValue(string, "requireContext().getStri…ayName(requireContext()))");
                        dSLConfiguration.noPadTextPref(companion.from(string, DSLSettingsText.CenterModifier.INSTANCE, DSLSettingsText.TitleLargeModifier.INSTANCE));
                        DimensionUnit dimensionUnit = DimensionUnit.DP;
                        dSLConfiguration.space((int) dimensionUnit.toPixels(12.0f));
                        ViewReceivedGiftBottomSheet.access$presentSubheading(this.this$0, dSLConfiguration, this.$state.getRecipient());
                        dSLConfiguration.space((int) dimensionUnit.toPixels(37.0f));
                    }
                    if (this.$state.getBadge() != null && this.$state.getControlState() != null) {
                        ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet3 = this.this$0;
                        ViewReceivedGiftState viewReceivedGiftState2 = this.$state;
                        ViewReceivedGiftBottomSheet.access$presentForUnexpiredGiftBadge(viewReceivedGiftBottomSheet3, dSLConfiguration, viewReceivedGiftState2, viewReceivedGiftState2.getGiftBadge(), this.$state.getControlState(), this.$state.getBadge());
                        dSLConfiguration.space((int) DimensionUnit.DP.toPixels(16.0f));
                    }
                }
            }
        });
    }

    public final void presentSubheading(DSLConfiguration dSLConfiguration, Recipient recipient) {
        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
        String string = requireContext().getString(R.string.ViewReceivedGiftBottomSheet__youve_received_a_gift_badge, recipient.getDisplayName(requireContext()));
        Intrinsics.checkNotNullExpressionValue(string, "requireContext().getStri…ayName(requireContext()))");
        dSLConfiguration.noPadTextPref(companion.from(string, DSLSettingsText.CenterModifier.INSTANCE));
    }

    public final void presentForUnexpiredGiftBadge(DSLConfiguration dSLConfiguration, ViewReceivedGiftState viewReceivedGiftState, GiftBadge giftBadge, ViewReceivedGiftState.ControlState controlState, Badge badge) {
        int i;
        GiftBadge.RedemptionState redemptionState = giftBadge.getRedemptionState();
        boolean z = true;
        if ((redemptionState == null ? -1 : WhenMappings.$EnumSwitchMapping$1[redemptionState.ordinal()]) == 1) {
            dSLConfiguration.customPref(new BadgeDisplay160.Model(badge));
            Recipient recipient = viewReceivedGiftState.getRecipient();
            if (recipient != null) {
                presentSubheading(dSLConfiguration, recipient);
                return;
            }
            return;
        }
        dSLConfiguration.customPref(new BadgeDisplay112.Model(badge, false, 2, null));
        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
        int i2 = WhenMappings.$EnumSwitchMapping$0[controlState.ordinal()];
        if (i2 == 1) {
            i = R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__display_on_profile;
        } else if (i2 == 2) {
            i = R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__make_featured_badge;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        DSLSettingsText from = companion.from(i, new DSLSettingsText.Modifier[0]);
        GiftBadge.RedemptionState redemptionState2 = giftBadge.getRedemptionState();
        GiftBadge.RedemptionState redemptionState3 = GiftBadge.RedemptionState.STARTED;
        dSLConfiguration.customPref(new OutlinedSwitch.Model(null, from, viewReceivedGiftState.getControlChecked(), redemptionState2 != redemptionState3, new Function1<OutlinedSwitch.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$presentForUnexpiredGiftBadge$2
            final /* synthetic */ ViewReceivedGiftBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(OutlinedSwitch.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(OutlinedSwitch.Model model) {
                Intrinsics.checkNotNullParameter(model, "it");
                ViewReceivedGiftBottomSheet.access$getViewModel(this.this$0).setChecked(!model.isChecked());
            }
        }, 1, null));
        if (viewReceivedGiftState.getHasOtherBadges() && viewReceivedGiftState.getDisplayingOtherBadges()) {
            dSLConfiguration.noPadTextPref(companion.from(R.string.ThanksForYourSupportBottomSheetFragment__when_you_have_more, new DSLSettingsText.Modifier[0]));
        }
        dSLConfiguration.space((int) DimensionUnit.DP.toPixels(36.0f));
        dSLConfiguration.primaryButton(companion.from(R.string.ViewReceivedGiftSheet__redeem, new DSLSettingsText.Modifier[0]), giftBadge.getRedemptionState() != redemptionState3, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$presentForUnexpiredGiftBadge$3
            final /* synthetic */ ViewReceivedGiftBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                LifecycleDisposable access$getLifecycleDisposable$p = ViewReceivedGiftBottomSheet.access$getLifecycleDisposable$p(this.this$0);
                Completable redeem = ViewReceivedGiftBottomSheet.access$getViewModel(this.this$0).redeem();
                final ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet = this.this$0;
                AnonymousClass1 r2 = new Function1<Throwable, Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$presentForUnexpiredGiftBadge$3.1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                        invoke(th);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Throwable th) {
                        Intrinsics.checkNotNullParameter(th, "it");
                        ViewReceivedGiftBottomSheet.access$onRedemptionError(viewReceivedGiftBottomSheet, th);
                    }
                };
                final ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet2 = this.this$0;
                access$getLifecycleDisposable$p.plusAssign(SubscribersKt.subscribeBy(redeem, r2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$presentForUnexpiredGiftBadge$3.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        viewReceivedGiftBottomSheet2.dismissAllowingStateLoss();
                    }
                }));
            }
        });
        DSLSettingsText from2 = companion.from(R.string.ViewReceivedGiftSheet__not_now, new DSLSettingsText.Modifier[0]);
        if (giftBadge.getRedemptionState() == redemptionState3) {
            z = false;
        }
        DSLConfiguration.secondaryButtonNoOutline$default(dSLConfiguration, from2, null, z, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet$presentForUnexpiredGiftBadge$4
            final /* synthetic */ ViewReceivedGiftBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                ViewReceivedGiftBottomSheet viewReceivedGiftBottomSheet = this.this$0;
                String str = ViewReceivedGiftBottomSheet.REQUEST_KEY;
                Bundle bundle = new Bundle();
                bundle.putBoolean(ViewReceivedGiftBottomSheet.RESULT_NOT_NOW, true);
                Unit unit = Unit.INSTANCE;
                androidx.fragment.app.FragmentKt.setFragmentResult(viewReceivedGiftBottomSheet, str, bundle);
                this.this$0.dismissAllowingStateLoss();
            }
        }, 2, null);
    }

    public final boolean isGiftBadgeRedeemed(GiftBadge giftBadge) {
        return giftBadge.getRedemptionState() == GiftBadge.RedemptionState.REDEEMED;
    }

    public final boolean isGiftBadgeExpired(GiftBadge giftBadge) {
        try {
            if (new ReceiptCredentialPresentation(giftBadge.getRedemptionToken().toByteArray()).getReceiptExpirationTime() <= TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) {
                return true;
            }
            return false;
        } catch (InvalidInputException e) {
            Log.w(TAG, "Failed to check expiration of given badge.", e);
            return true;
        }
    }
}
