package org.thoughtcrime.securesms.badges.self.none;

import com.annimon.stream.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.badges.self.none.BecomeASustainerViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class BecomeASustainerViewModel$2$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ List f$0;

    public /* synthetic */ BecomeASustainerViewModel$2$$ExternalSyntheticLambda0(List list) {
        this.f$0 = list;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return BecomeASustainerViewModel.AnonymousClass2.m439invoke$lambda0(this.f$0, (BecomeASustainerState) obj);
    }
}
