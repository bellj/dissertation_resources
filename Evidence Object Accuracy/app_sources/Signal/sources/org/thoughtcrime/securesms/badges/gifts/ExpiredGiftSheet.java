package org.thoughtcrime.securesms.badges.gifts;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: ExpiredGiftSheet.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \f2\u00020\u0001:\u0002\u000b\fB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u0014\u0010\u0003\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/ExpiredGiftSheet;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ExpiredGiftSheet extends DSLSettingsBottomSheetFragment {
    private static final String ARG_BADGE;
    public static final Companion Companion = new Companion(null);

    /* compiled from: ExpiredGiftSheet.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/ExpiredGiftSheet$Callback;", "", "onMakeAMonthlyDonation", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Callback {
        void onMakeAMonthlyDonation();
    }

    public ExpiredGiftSheet() {
        super(0, null, 0.0f, 7, null);
    }

    /* compiled from: ExpiredGiftSheet.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/ExpiredGiftSheet$Companion;", "", "()V", "ARG_BADGE", "", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, Badge badge) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(badge, "badge");
            ExpiredGiftSheet expiredGiftSheet = new ExpiredGiftSheet();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ExpiredGiftSheet.ARG_BADGE, badge);
            expiredGiftSheet.setArguments(bundle);
            expiredGiftSheet.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }

    public final Badge getBadge() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_BADGE);
        Intrinsics.checkNotNull(parcelable);
        return (Badge) parcelable;
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ExpiredGiftSheetConfiguration.INSTANCE.register(dSLSettingsAdapter);
        dSLSettingsAdapter.submitList(DslKt.configure(new Function1<DSLConfiguration, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet$bindAdapter$1
            final /* synthetic */ ExpiredGiftSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                ExpiredGiftSheetConfiguration expiredGiftSheetConfiguration = ExpiredGiftSheetConfiguration.INSTANCE;
                Badge access$getBadge = ExpiredGiftSheet.access$getBadge(this.this$0);
                final ExpiredGiftSheet expiredGiftSheet = this.this$0;
                AnonymousClass1 r2 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet$bindAdapter$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ExpiredGiftSheet.Callback callback;
                        ExpiredGiftSheet expiredGiftSheet2 = expiredGiftSheet;
                        ArrayList arrayList = new ArrayList();
                        try {
                            Fragment fragment = expiredGiftSheet2.getParentFragment();
                            while (true) {
                                if (fragment == null) {
                                    FragmentActivity requireActivity = expiredGiftSheet2.requireActivity();
                                    if (requireActivity != null) {
                                        callback = (ExpiredGiftSheet.Callback) requireActivity;
                                    } else {
                                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet.Callback");
                                    }
                                } else if (fragment instanceof ExpiredGiftSheet.Callback) {
                                    callback = fragment;
                                    break;
                                } else {
                                    String name = fragment.getClass().getName();
                                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                                    arrayList.add(name);
                                    fragment = fragment.getParentFragment();
                                }
                            }
                            callback.onMakeAMonthlyDonation();
                        } catch (ClassCastException e) {
                            String name2 = expiredGiftSheet2.requireActivity().getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                            arrayList.add(name2);
                            throw new ListenerNotFoundException(arrayList, e);
                        }
                    }
                };
                final ExpiredGiftSheet expiredGiftSheet2 = this.this$0;
                expiredGiftSheetConfiguration.forExpiredBadge(dSLConfiguration, access$getBadge, r2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet$bindAdapter$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        expiredGiftSheet2.dismissAllowingStateLoss();
                    }
                });
            }
        }).toMappingModelList());
    }
}
