package org.thoughtcrime.securesms.badges.view;

import androidx.viewpager2.widget.ViewPager2;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.LargeBadge;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: ViewBadgeBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"org/thoughtcrime/securesms/badges/view/ViewBadgeBottomSheetDialogFragment$onViewCreated$5", "Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;", "onPageSelected", "", "position", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewBadgeBottomSheetDialogFragment$onViewCreated$5 extends ViewPager2.OnPageChangeCallback {
    final /* synthetic */ MappingAdapter $adapter;
    final /* synthetic */ ViewBadgeBottomSheetDialogFragment this$0;

    public ViewBadgeBottomSheetDialogFragment$onViewCreated$5(MappingAdapter mappingAdapter, ViewBadgeBottomSheetDialogFragment viewBadgeBottomSheetDialogFragment) {
        this.$adapter = mappingAdapter;
        this.this$0 = viewBadgeBottomSheetDialogFragment;
    }

    /* renamed from: onPageSelected$lambda-0 */
    public static final Boolean m461onPageSelected$lambda0(MappingModel mappingModel) {
        return Boolean.valueOf(mappingModel instanceof LargeBadge.Model);
    }

    @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
    public void onPageSelected(int i) {
        Object orElse = this.$adapter.getModel(i).map(new Function() { // from class: org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment$onViewCreated$5$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ViewBadgeBottomSheetDialogFragment$onViewCreated$5.m461onPageSelected$lambda0((MappingModel) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Boolean.FALSE);
        Intrinsics.checkNotNullExpressionValue(orElse, "adapter.getModel(positio…dge.Model }.orElse(false)");
        if (((Boolean) orElse).booleanValue()) {
            this.this$0.getViewModel().onPageSelected(i);
        }
    }
}
