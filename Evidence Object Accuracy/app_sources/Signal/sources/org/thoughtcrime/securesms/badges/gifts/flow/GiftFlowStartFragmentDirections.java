package org.thoughtcrime.securesms.badges.gifts.flow;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;

/* loaded from: classes3.dex */
public class GiftFlowStartFragmentDirections {
    private GiftFlowStartFragmentDirections() {
    }

    public static ActionGiftFlowStartFragmentToSetCurrencyFragment actionGiftFlowStartFragmentToSetCurrencyFragment(boolean z, String[] strArr) {
        return new ActionGiftFlowStartFragmentToSetCurrencyFragment(z, strArr);
    }

    public static NavDirections actionGiftFlowStartFragmentToGiftFlowRecipientSelectionFragment() {
        return new ActionOnlyNavDirections(R.id.action_giftFlowStartFragment_to_giftFlowRecipientSelectionFragment);
    }

    /* loaded from: classes3.dex */
    public static class ActionGiftFlowStartFragmentToSetCurrencyFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_giftFlowStartFragment_to_setCurrencyFragment;
        }

        private ActionGiftFlowStartFragmentToSetCurrencyFragment(boolean z, String[] strArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("isBoost", Boolean.valueOf(z));
            if (strArr != null) {
                hashMap.put("supportedCurrencyCodes", strArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        public ActionGiftFlowStartFragmentToSetCurrencyFragment setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        public ActionGiftFlowStartFragmentToSetCurrencyFragment setSupportedCurrencyCodes(String[] strArr) {
            if (strArr != null) {
                this.arguments.put("supportedCurrencyCodes", strArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("isBoost")) {
                bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
            }
            if (this.arguments.containsKey("supportedCurrencyCodes")) {
                bundle.putStringArray("supportedCurrencyCodes", (String[]) this.arguments.get("supportedCurrencyCodes"));
            }
            return bundle;
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }

        public String[] getSupportedCurrencyCodes() {
            return (String[]) this.arguments.get("supportedCurrencyCodes");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionGiftFlowStartFragmentToSetCurrencyFragment actionGiftFlowStartFragmentToSetCurrencyFragment = (ActionGiftFlowStartFragmentToSetCurrencyFragment) obj;
            if (this.arguments.containsKey("isBoost") != actionGiftFlowStartFragmentToSetCurrencyFragment.arguments.containsKey("isBoost") || getIsBoost() != actionGiftFlowStartFragmentToSetCurrencyFragment.getIsBoost() || this.arguments.containsKey("supportedCurrencyCodes") != actionGiftFlowStartFragmentToSetCurrencyFragment.arguments.containsKey("supportedCurrencyCodes")) {
                return false;
            }
            if (getSupportedCurrencyCodes() == null ? actionGiftFlowStartFragmentToSetCurrencyFragment.getSupportedCurrencyCodes() == null : getSupportedCurrencyCodes().equals(actionGiftFlowStartFragmentToSetCurrencyFragment.getSupportedCurrencyCodes())) {
                return getActionId() == actionGiftFlowStartFragmentToSetCurrencyFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getIsBoost() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getSupportedCurrencyCodes())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionGiftFlowStartFragmentToSetCurrencyFragment(actionId=" + getActionId() + "){isBoost=" + getIsBoost() + ", supportedCurrencyCodes=" + getSupportedCurrencyCodes() + "}";
        }
    }
}
