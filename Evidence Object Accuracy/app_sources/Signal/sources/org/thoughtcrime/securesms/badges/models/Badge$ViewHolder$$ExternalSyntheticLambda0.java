package org.thoughtcrime.securesms.badges.models;

import android.view.View;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class Badge$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Badge.ViewHolder f$0;
    public final /* synthetic */ Badge.Model f$1;

    public /* synthetic */ Badge$ViewHolder$$ExternalSyntheticLambda0(Badge.ViewHolder viewHolder, Badge.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        Badge.ViewHolder.m418bind$lambda0(this.f$0, this.f$1, view);
    }
}
