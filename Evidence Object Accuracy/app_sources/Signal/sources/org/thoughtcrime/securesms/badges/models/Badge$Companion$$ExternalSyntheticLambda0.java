package org.thoughtcrime.securesms.badges.models;

import android.view.View;
import j$.util.function.Function;
import kotlin.jvm.functions.Function3;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class Badge$Companion$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Function3 f$0;

    public /* synthetic */ Badge$Companion$$ExternalSyntheticLambda0(Function3 function3) {
        this.f$0 = function3;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return Badge.Companion.m415register$lambda0(this.f$0, (View) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
