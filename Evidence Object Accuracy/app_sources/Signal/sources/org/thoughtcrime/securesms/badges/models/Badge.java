package org.thoughtcrime.securesms.badges.models;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.glide.BadgeSpriteTransformation;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: Badge.kt */
@Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u001b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\b\u0018\u0000 @2\u00020\u00012\u00020\u0002:\u0006?@ABCDBM\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\b\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0004\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\r¢\u0006\u0002\u0010\u0011J\t\u0010 \u001a\u00020\u0004HÆ\u0003J\t\u0010!\u001a\u00020\u0006HÆ\u0003J\t\u0010\"\u001a\u00020\u0004HÆ\u0003J\t\u0010#\u001a\u00020\u0004HÆ\u0003J\t\u0010$\u001a\u00020\nHÆ\u0003J\t\u0010%\u001a\u00020\u0004HÆ\u0003J\t\u0010&\u001a\u00020\rHÆ\u0003J\t\u0010'\u001a\u00020\u000fHÆ\u0003J\t\u0010(\u001a\u00020\rHÆ\u0003Jc\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00042\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\rHÆ\u0001J\t\u0010*\u001a\u00020+HÖ\u0001J\u0013\u0010,\u001a\u00020\u000f2\b\u0010-\u001a\u0004\u0018\u00010.HÖ\u0003J\t\u0010/\u001a\u00020+HÖ\u0001J\u0006\u00100\u001a\u00020\u000fJ\u0006\u00101\u001a\u00020\u000fJ\u0006\u00102\u001a\u00020\u000fJ\u0006\u00103\u001a\u00020\u000fJ\u000e\u00104\u001a\u00020\u00042\u0006\u00105\u001a\u00020\u0004J\t\u00106\u001a\u00020\u0004HÖ\u0001J\u0010\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0016J\u0019\u0010;\u001a\u0002082\u0006\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020+HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0010\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R\u0011\u0010\u000b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0015R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001f¨\u0006E"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge;", "Landroid/os/Parcelable;", "Lcom/bumptech/glide/load/Key;", ContactRepository.ID_COLUMN, "", "category", "Lorg/thoughtcrime/securesms/badges/models/Badge$Category;", "name", "description", "imageUrl", "Landroid/net/Uri;", "imageDensity", "expirationTimestamp", "", "visible", "", "duration", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/badges/models/Badge$Category;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;JZJ)V", "getCategory", "()Lorg/thoughtcrime/securesms/badges/models/Badge$Category;", "getDescription", "()Ljava/lang/String;", "getDuration", "()J", "getExpirationTimestamp", "getId", "getImageDensity", "getImageUrl", "()Landroid/net/Uri;", "getName", "getVisible", "()Z", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "describeContents", "", "equals", "other", "", "hashCode", "isBoost", "isExpired", "isGift", "isSubscription", "resolveDescription", "shortName", "toString", "updateDiskCacheKey", "", "messageDigest", "Ljava/security/MessageDigest;", "writeToParcel", "parcel", "Landroid/os/Parcel;", "flags", "Category", "Companion", "EmptyModel", "EmptyViewHolder", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Badge implements Parcelable, Key {
    public static final String BOOST_BADGE_ID;
    public static final Parcelable.Creator<Badge> CREATOR = new Creator();
    public static final Companion Companion = new Companion(null);
    public static final String GIFT_BADGE_ID;
    private static final Object SELECTION_CHANGED = new Object();
    private final Category category;
    private final String description;
    private final long duration;
    private final long expirationTimestamp;
    private final String id;
    private final String imageDensity;
    private final Uri imageUrl;
    private final String name;
    private final boolean visible;

    /* compiled from: Badge.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Creator implements Parcelable.Creator<Badge> {
        @Override // android.os.Parcelable.Creator
        public final Badge createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            return new Badge(parcel.readString(), Category.valueOf(parcel.readString()), parcel.readString(), parcel.readString(), (Uri) parcel.readParcelable(Badge.class.getClassLoader()), parcel.readString(), parcel.readLong(), parcel.readInt() != 0, parcel.readLong());
        }

        @Override // android.os.Parcelable.Creator
        public final Badge[] newArray(int i) {
            return new Badge[i];
        }
    }

    public final String component1() {
        return this.id;
    }

    public final Category component2() {
        return this.category;
    }

    public final String component3() {
        return this.name;
    }

    public final String component4() {
        return this.description;
    }

    public final Uri component5() {
        return this.imageUrl;
    }

    public final String component6() {
        return this.imageDensity;
    }

    public final long component7() {
        return this.expirationTimestamp;
    }

    public final boolean component8() {
        return this.visible;
    }

    public final long component9() {
        return this.duration;
    }

    public final Badge copy(String str, Category category, String str2, String str3, Uri uri, String str4, long j, boolean z, long j2) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(category, "category");
        Intrinsics.checkNotNullParameter(str2, "name");
        Intrinsics.checkNotNullParameter(str3, "description");
        Intrinsics.checkNotNullParameter(uri, "imageUrl");
        Intrinsics.checkNotNullParameter(str4, "imageDensity");
        return new Badge(str, category, str2, str3, uri, str4, j, z, j2);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object, com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Badge)) {
            return false;
        }
        Badge badge = (Badge) obj;
        return Intrinsics.areEqual(this.id, badge.id) && this.category == badge.category && Intrinsics.areEqual(this.name, badge.name) && Intrinsics.areEqual(this.description, badge.description) && Intrinsics.areEqual(this.imageUrl, badge.imageUrl) && Intrinsics.areEqual(this.imageDensity, badge.imageDensity) && this.expirationTimestamp == badge.expirationTimestamp && this.visible == badge.visible && this.duration == badge.duration;
    }

    @Override // java.lang.Object, com.bumptech.glide.load.Key
    public int hashCode() {
        int hashCode = ((((((((((((this.id.hashCode() * 31) + this.category.hashCode()) * 31) + this.name.hashCode()) * 31) + this.description.hashCode()) * 31) + this.imageUrl.hashCode()) * 31) + this.imageDensity.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.expirationTimestamp)) * 31;
        boolean z = this.visible;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return ((hashCode + i) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.duration);
    }

    @Override // java.lang.Object
    public String toString() {
        return "Badge(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", description=" + this.description + ", imageUrl=" + this.imageUrl + ", imageDensity=" + this.imageDensity + ", expirationTimestamp=" + this.expirationTimestamp + ", visible=" + this.visible + ", duration=" + this.duration + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        parcel.writeString(this.id);
        parcel.writeString(this.category.name());
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeParcelable(this.imageUrl, i);
        parcel.writeString(this.imageDensity);
        parcel.writeLong(this.expirationTimestamp);
        parcel.writeInt(this.visible ? 1 : 0);
        parcel.writeLong(this.duration);
    }

    public Badge(String str, Category category, String str2, String str3, Uri uri, String str4, long j, boolean z, long j2) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(category, "category");
        Intrinsics.checkNotNullParameter(str2, "name");
        Intrinsics.checkNotNullParameter(str3, "description");
        Intrinsics.checkNotNullParameter(uri, "imageUrl");
        Intrinsics.checkNotNullParameter(str4, "imageDensity");
        this.id = str;
        this.category = category;
        this.name = str2;
        this.description = str3;
        this.imageUrl = uri;
        this.imageDensity = str4;
        this.expirationTimestamp = j;
        this.visible = z;
        this.duration = j2;
    }

    public final String getId() {
        return this.id;
    }

    public final Category getCategory() {
        return this.category;
    }

    public final String getName() {
        return this.name;
    }

    public final String getDescription() {
        return this.description;
    }

    public final Uri getImageUrl() {
        return this.imageUrl;
    }

    public final String getImageDensity() {
        return this.imageDensity;
    }

    public final long getExpirationTimestamp() {
        return this.expirationTimestamp;
    }

    public final boolean getVisible() {
        return this.visible;
    }

    public final long getDuration() {
        return this.duration;
    }

    public final boolean isExpired() {
        return this.expirationTimestamp < System.currentTimeMillis() && this.expirationTimestamp > 0;
    }

    public final boolean isBoost() {
        return Intrinsics.areEqual(this.id, BOOST_BADGE_ID);
    }

    public final boolean isGift() {
        return Intrinsics.areEqual(this.id, GIFT_BADGE_ID);
    }

    public final boolean isSubscription() {
        return !isBoost() && !isGift();
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        Intrinsics.checkNotNullParameter(messageDigest, "messageDigest");
        String str = this.id;
        Charset charset = Key.CHARSET;
        Intrinsics.checkNotNullExpressionValue(charset, "CHARSET");
        byte[] bytes = str.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        messageDigest.update(bytes);
        String uri = this.imageUrl.toString();
        Intrinsics.checkNotNullExpressionValue(uri, "imageUrl.toString()");
        Intrinsics.checkNotNullExpressionValue(charset, "CHARSET");
        byte[] bytes2 = uri.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue(bytes2, "this as java.lang.String).getBytes(charset)");
        messageDigest.update(bytes2);
        String str2 = this.imageDensity;
        Intrinsics.checkNotNullExpressionValue(charset, "CHARSET");
        byte[] bytes3 = str2.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue(bytes3, "this as java.lang.String).getBytes(charset)");
        messageDigest.update(bytes3);
    }

    public final String resolveDescription(String str) {
        Intrinsics.checkNotNullParameter(str, "shortName");
        return StringsKt__StringsJVMKt.replace$default(this.description, "{short_name}", str, false, 4, (Object) null);
    }

    /* compiled from: Badge.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$EmptyModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "()V", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class EmptyModel extends PreferenceModel<EmptyModel> {
        public boolean areItemsTheSame(EmptyModel emptyModel) {
            Intrinsics.checkNotNullParameter(emptyModel, "newItem");
            return true;
        }

        public EmptyModel() {
            super(null, null, null, null, false, 31, null);
        }
    }

    /* compiled from: Badge.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$EmptyViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/Badge$EmptyModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "name", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class EmptyViewHolder extends MappingViewHolder<EmptyModel> {
        private final TextView name;

        public void bind(EmptyModel emptyModel) {
            Intrinsics.checkNotNullParameter(emptyModel, "model");
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public EmptyViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.name)");
            TextView textView = (TextView) findViewById;
            this.name = textView;
            view.setEnabled(false);
            view.setFocusable(false);
            view.setClickable(false);
            view.setVisibility(4);
            textView.setText(" ");
        }
    }

    /* compiled from: Badge.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\u000b\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0000H\u0016J\u0010\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0000H\u0016J\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\f\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\n¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "isSelected", "", "isFaded", "(Lorg/thoughtcrime/securesms/badges/models/Badge;ZZ)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "()Z", "areContentsTheSame", "newItem", "areItemsTheSame", "getChangePayload", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Badge badge;
        private final boolean isFaded;
        private final boolean isSelected;

        public /* synthetic */ Model(Badge badge, boolean z, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(badge, (i & 2) != 0 ? false : z, (i & 4) != 0 ? false : z2);
        }

        public final Badge getBadge() {
            return this.badge;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public final boolean isFaded() {
            return this.isFaded;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Badge badge, boolean z, boolean z2) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(badge, "badge");
            this.badge = badge;
            this.isSelected = z;
            this.isFaded = z2;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.badge.getId(), this.badge.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(this.badge, model.badge) && this.isSelected == model.isSelected && this.isFaded == model.isFaded;
        }

        public Object getChangePayload(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            if (!Intrinsics.areEqual(this.badge, model.badge) || this.isSelected == model.isSelected) {
                return null;
            }
            return Badge.SELECTION_CHANGED;
        }
    }

    /* compiled from: Badge.kt */
    @Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B1\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\"\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\n¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R*\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/Badge$Model;", "itemView", "Landroid/view/View;", "onBadgeClicked", "Lkotlin/Function3;", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "", "", "Lorg/thoughtcrime/securesms/badges/models/OnBadgeClicked;", "(Landroid/view/View;Lkotlin/jvm/functions/Function3;)V", "badge", "Landroid/widget/ImageView;", "check", "checkAnimator", "Landroid/animation/ObjectAnimator;", "name", "Landroid/widget/TextView;", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ImageView badge;
        private final ImageView check;
        private ObjectAnimator checkAnimator;
        private final TextView name;
        private final Function3<Badge, Boolean, Boolean, Unit> onBadgeClicked;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function3<? super org.thoughtcrime.securesms.badges.models.Badge, ? super java.lang.Boolean, ? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function3<? super Badge, ? super Boolean, ? super Boolean, Unit> function3) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function3, "onBadgeClicked");
            this.onBadgeClicked = function3;
            View findViewById = view.findViewById(R.id.checkmark);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.checkmark)");
            ImageView imageView = (ImageView) findViewById;
            this.check = imageView;
            View findViewById2 = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.badge)");
            this.badge = (ImageView) findViewById2;
            View findViewById3 = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.name)");
            this.name = (TextView) findViewById3;
            imageView.setSelected(true);
        }

        public void bind(Model model) {
            ObjectAnimator objectAnimator;
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setOnClickListener(new Badge$ViewHolder$$ExternalSyntheticLambda0(this, model));
            ObjectAnimator objectAnimator2 = this.checkAnimator;
            if (objectAnimator2 != null) {
                objectAnimator2.cancel();
            }
            List<Object> list = this.payload;
            Intrinsics.checkNotNullExpressionValue(list, "payload");
            if (!list.isEmpty()) {
                if (model.isSelected()) {
                    objectAnimator = ObjectAnimator.ofFloat(this.check, "alpha", 1.0f);
                } else {
                    objectAnimator = ObjectAnimator.ofFloat(this.check, "alpha", 0.0f);
                }
                this.checkAnimator = objectAnimator;
                if (objectAnimator != null) {
                    objectAnimator.start();
                    return;
                }
                return;
            }
            this.badge.setAlpha((model.getBadge().isExpired() || model.isFaded()) ? 0.5f : 1.0f);
            GlideApp.with(this.badge).load((Object) model.getBadge()).downsample(DownsampleStrategy.NONE).diskCacheStrategy(DiskCacheStrategy.NONE).transform((Transformation<Bitmap>) new BadgeSpriteTransformation(BadgeSpriteTransformation.Size.BADGE_64, model.getBadge().getImageDensity(), ThemeUtil.isDarkTheme(this.context))).into(this.badge);
            if (model.isSelected()) {
                this.check.setAlpha(1.0f);
            } else {
                this.check.setAlpha(0.0f);
            }
            this.name.setText(model.getBadge().getName());
        }

        /* renamed from: bind$lambda-0 */
        public static final void m418bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.onBadgeClicked.invoke(model.getBadge(), Boolean.valueOf(model.isSelected()), Boolean.valueOf(model.isFaded()));
        }
    }

    /* compiled from: Badge.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$Category;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "Donor", "Other", "Testing", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Category {
        Donor("donor"),
        Other("other"),
        Testing("testing");
        
        public static final Companion Companion = new Companion(null);
        private final String code;

        Category(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }

        /* compiled from: Badge.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$Category$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/badges/models/Badge$Category;", "code", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final Category fromCode(String str) {
                Intrinsics.checkNotNullParameter(str, "code");
                if (Intrinsics.areEqual(str, "donor")) {
                    return Category.Donor;
                }
                if (Intrinsics.areEqual(str, "testing")) {
                    return Category.Testing;
                }
                return Category.Other;
            }
        }
    }

    /* compiled from: Badge.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J2\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\b0\fj\u0002`\u000fR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/Badge$Companion;", "", "()V", "BOOST_BADGE_ID", "", "GIFT_BADGE_ID", "SELECTION_CHANGED", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onBadgeClicked", "Lkotlin/Function3;", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "", "Lorg/thoughtcrime/securesms/badges/models/OnBadgeClicked;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* renamed from: register$lambda-0 */
        public static final MappingViewHolder m415register$lambda0(Function3 function3, View view) {
            Intrinsics.checkNotNullParameter(function3, "$onBadgeClicked");
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new ViewHolder(view, function3);
        }

        public final void register(MappingAdapter mappingAdapter, Function3<? super Badge, ? super Boolean, ? super Boolean, Unit> function3) {
            Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
            Intrinsics.checkNotNullParameter(function3, "onBadgeClicked");
            mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Badge$Companion$$ExternalSyntheticLambda0(function3), R.layout.badge_preference_view));
            mappingAdapter.registerFactory(EmptyModel.class, new LayoutFactory(new Badge$Companion$$ExternalSyntheticLambda1(), R.layout.badge_preference_view));
        }

        /* renamed from: register$lambda-1 */
        public static final MappingViewHolder m416register$lambda1(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new EmptyViewHolder(view);
        }
    }
}
