package org.thoughtcrime.securesms.badges.models;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: ExpiredBadge.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/ExpiredBadge;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ExpiredBadge {
    public static final ExpiredBadge INSTANCE = new ExpiredBadge();

    private ExpiredBadge() {
    }

    /* compiled from: ExpiredBadge.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/ExpiredBadge$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Badge badge;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Badge badge) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(badge, "badge");
            this.badge = badge;
        }

        public final Badge getBadge() {
            return this.badge;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.badge.getId(), this.badge.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(model.badge, this.badge);
        }
    }

    /* compiled from: ExpiredBadge.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/models/ExpiredBadge$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/badges/models/ExpiredBadge$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badge;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.expired_badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.expired_badge)");
            this.badge = (BadgeImageView) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.badge.setBadge(model.getBadge());
        }
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m423register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.badges.models.ExpiredBadge$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ExpiredBadge.m422$r8$lambda$756AT__Lq8n7FWTS1AIEN6j4o0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.expired_badge_preference));
    }
}
