package org.thoughtcrime.securesms.badges.gifts;

import android.animation.FloatEvaluator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewGroupKt;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.Sequence;
import kotlin.sequences.SequencesKt___SequencesJvmKt;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Projection;

/* compiled from: OpenableGiftItemDecoration.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 12\u00020\u00012\u00020\u0002:\u000212B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0018\u0010 \u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u000e\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\bJ\u0010\u0010$\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020&H\u0016J \u0010'\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u001d2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020\u001b2\u0006\u0010.\u001a\u00020/H\u0002J\u0010\u00100\u001a\u00020\u001b2\u0006\u0010.\u001a\u00020/H\u0002R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\b0\u0016X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\b0\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0004¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "animationState", "", "", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState;", "animatorDurationScale", "", "bowDrawable", "Landroid/graphics/drawable/Drawable;", "bowHeight", "bowPaint", "Landroid/graphics/Paint;", "bowWidth", "boxPaint", "lineWidth", "", "messageIdsOpenedThisSession", "", "messageIdsShakenThisSession", "rect", "Landroid/graphics/RectF;", "drawGiftBow", "", "canvas", "Landroid/graphics/Canvas;", "projection", "Lorg/thoughtcrime/securesms/util/Projection;", "drawGiftBox", "hasOpenedGiftThisSession", "", "messageRecordId", "onDestroy", "owner", "Landroidx/lifecycle/LifecycleOwner;", "onDrawOver", "c", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "startOpenAnimation", "child", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;", "startShakeAnimation", "Companion", "GiftAnimationState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class OpenableGiftItemDecoration extends RecyclerView.ItemDecoration implements DefaultLifecycleObserver {
    public static final Companion Companion = new Companion(null);
    private static final FloatEvaluator EVALUATOR = new FloatEvaluator();
    private static final int ONE_FRAME_RELATIVE_TO_30_FPS_MILLIS;
    private static final long OPEN_BOW_START_DELAY;
    private static final long OPEN_BOX_START_DELAY;
    private static final long OPEN_DURATION_MILLIS;
    private static final long SHAKE_DURATION_MILLIS;
    private static final AccelerateDecelerateInterpolator TRANSLATION_X_INTERPOLATOR = new AccelerateDecelerateInterpolator();
    private static final AnticipateInterpolator TRANSLATION_Y_INTERPOLATOR = new AnticipateInterpolator(3.0f);
    private final Map<Long, GiftAnimationState> animationState = new LinkedHashMap();
    private final float animatorDurationScale;
    private final Drawable bowDrawable;
    private final float bowHeight;
    private final Paint bowPaint;
    private final float bowWidth;
    private final Paint boxPaint;
    private final int lineWidth;
    private final Set<Long> messageIdsOpenedThisSession = new LinkedHashSet();
    private final Set<Long> messageIdsShakenThisSession = new LinkedHashSet();
    private final RectF rect = new RectF();

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public OpenableGiftItemDecoration(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.animatorDurationScale = Settings.Global.getFloat(context.getContentResolver(), "animator_duration_scale", 1.0f);
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        this.lineWidth = (int) dimensionUnit.toPixels(16.0f);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(ContextCompat.getColor(context, R.color.core_ultramarine));
        this.boxPaint = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(-1);
        this.bowPaint = paint2;
        this.bowWidth = dimensionUnit.toPixels(80.0f);
        this.bowHeight = dimensionUnit.toPixels(60.0f);
        Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.ic_gift_bow);
        Intrinsics.checkNotNull(drawable);
        this.bowDrawable = drawable;
    }

    public final boolean hasOpenedGiftThisSession(long j) {
        return this.messageIdsOpenedThisSession.contains(Long.valueOf(j));
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onDestroy(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
        this.animationState.clear();
    }

    /* JADX INFO: finally extract failed */
    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        boolean z;
        Intrinsics.checkNotNullParameter(canvas, "c");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        Sequence sequence = SequencesKt___SequencesJvmKt.filterIsInstance(ViewGroupKt.getChildren(recyclerView), OpenableGift.class);
        Set<Long> keySet = this.animationState.keySet();
        ArrayList<Number> arrayList = new ArrayList();
        Iterator<T> it = keySet.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            long longValue = ((Number) next).longValue();
            Iterator it2 = sequence.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z2 = false;
                    break;
                }
                if (((OpenableGift) it2.next()).getGiftId() == longValue) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (!z2) {
                arrayList.add(next);
            }
        }
        for (Number number : arrayList) {
            this.animationState.remove(Long.valueOf(number.longValue()));
        }
        boolean z3 = false;
        for (OpenableGift openableGift : SequencesKt___SequencesKt.filterNot(SequencesKt___SequencesKt.filterNot(sequence, new Function1<OpenableGift, Boolean>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration$onDrawOver$notAnimated$1
            final /* synthetic */ OpenableGiftItemDecoration this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            public final Boolean invoke(OpenableGift openableGift2) {
                Intrinsics.checkNotNullParameter(openableGift2, "it");
                return Boolean.valueOf(OpenableGiftItemDecoration.access$getAnimationState$p(this.this$0).containsKey(Long.valueOf(openableGift2.getGiftId())));
            }
        }), new Function1<OpenableGift, Boolean>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration$onDrawOver$2
            final /* synthetic */ OpenableGiftItemDecoration this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            public final Boolean invoke(OpenableGift openableGift2) {
                Intrinsics.checkNotNullParameter(openableGift2, "it");
                return Boolean.valueOf(OpenableGiftItemDecoration.access$getMessageIdsOpenedThisSession$p(this.this$0).contains(Long.valueOf(openableGift2.getGiftId())));
            }
        })) {
            Projection openableGiftProjection = openableGift.getOpenableGiftProjection(false);
            if (openableGiftProjection != null) {
                openableGift.setOpenGiftCallback(new Function1<OpenableGift, Unit>(openableGift, this, recyclerView) { // from class: org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration$onDrawOver$3$1
                    final /* synthetic */ OpenableGift $child;
                    final /* synthetic */ RecyclerView $parent;
                    final /* synthetic */ OpenableGiftItemDecoration this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.$child = r1;
                        this.this$0 = r2;
                        this.$parent = r3;
                    }

                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(OpenableGift openableGift2) {
                        invoke(openableGift2);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(OpenableGift openableGift2) {
                        Intrinsics.checkNotNullParameter(openableGift2, "it");
                        this.$child.clearOpenGiftCallback();
                        if (openableGift2.getOpenableGiftProjection(true) != null) {
                            OpenableGiftItemDecoration.access$getMessageIdsOpenedThisSession$p(this.this$0).add(Long.valueOf(openableGift2.getGiftId()));
                            OpenableGiftItemDecoration.access$startOpenAnimation(this.this$0, openableGift2);
                            this.$parent.invalidate();
                        }
                    }
                });
                if (this.messageIdsShakenThisSession.contains(Long.valueOf(openableGift.getGiftId()))) {
                    drawGiftBox(canvas, openableGiftProjection);
                    drawGiftBow(canvas, openableGiftProjection);
                } else {
                    this.messageIdsShakenThisSession.add(Long.valueOf(openableGift.getGiftId()));
                    startShakeAnimation(openableGift);
                    drawGiftBox(canvas, openableGiftProjection);
                    drawGiftBow(canvas, openableGiftProjection);
                    z3 = true;
                }
                openableGiftProjection.release();
            }
        }
        for (OpenableGift openableGift2 : SequencesKt___SequencesKt.filter(sequence, new Function1<OpenableGift, Boolean>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration$onDrawOver$4
            final /* synthetic */ OpenableGiftItemDecoration this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            public final Boolean invoke(OpenableGift openableGift3) {
                Intrinsics.checkNotNullParameter(openableGift3, "it");
                return Boolean.valueOf(OpenableGiftItemDecoration.access$getAnimationState$p(this.this$0).containsKey(Long.valueOf(openableGift3.getGiftId())));
            }
        })) {
            GiftAnimationState giftAnimationState = this.animationState.get(Long.valueOf(openableGift2.getGiftId()));
            Intrinsics.checkNotNull(giftAnimationState);
            GiftAnimationState giftAnimationState2 = giftAnimationState;
            int save = canvas.save();
            try {
                if (!giftAnimationState2.update(this.animatorDurationScale, canvas, new Function2<Canvas, Projection, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration$onDrawOver$5$1$isThisAnimationRunning$1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    @Override // kotlin.jvm.functions.Function2
                    public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                        invoke((Canvas) obj, (Projection) obj2);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Canvas canvas2, Projection projection) {
                        Intrinsics.checkNotNullParameter(canvas2, "p0");
                        Intrinsics.checkNotNullParameter(projection, "p1");
                        OpenableGiftItemDecoration.access$drawGiftBox((OpenableGiftItemDecoration) this.receiver, canvas2, projection);
                    }
                }, new Function2<Canvas, Projection, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration$onDrawOver$5$1$isThisAnimationRunning$2
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    @Override // kotlin.jvm.functions.Function2
                    public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                        invoke((Canvas) obj, (Projection) obj2);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(Canvas canvas2, Projection projection) {
                        Intrinsics.checkNotNullParameter(canvas2, "p0");
                        Intrinsics.checkNotNullParameter(projection, "p1");
                        OpenableGiftItemDecoration.access$drawGiftBow((OpenableGiftItemDecoration) this.receiver, canvas2, projection);
                    }
                })) {
                    this.animationState.remove(Long.valueOf(openableGift2.getGiftId()));
                }
                canvas.restoreToCount(save);
                z3 = true;
            } catch (Throwable th) {
                canvas.restoreToCount(save);
                throw th;
            }
        }
        if (z3) {
            recyclerView.invalidate();
        }
    }

    public final void drawGiftBox(Canvas canvas, Projection projection) {
        canvas.drawPath(projection.getPath(), this.boxPaint);
        this.rect.set((projection.getX() + ((float) (projection.getWidth() / 2))) - ((float) (this.lineWidth / 2)), projection.getY(), projection.getX() + ((float) (projection.getWidth() / 2)) + ((float) (this.lineWidth / 2)), projection.getY() + ((float) projection.getHeight()));
        canvas.drawRect(this.rect, this.bowPaint);
        this.rect.set(projection.getX(), (projection.getY() + ((float) (projection.getHeight() / 2))) - ((float) (this.lineWidth / 2)), projection.getX() + ((float) projection.getWidth()), projection.getY() + ((float) (projection.getHeight() / 2)) + ((float) (this.lineWidth / 2)));
        canvas.drawRect(this.rect, this.bowPaint);
    }

    public final void drawGiftBow(Canvas canvas, Projection projection) {
        float f = (float) 2;
        this.rect.set((projection.getX() + ((float) (projection.getWidth() / 2))) - (this.bowWidth / f), projection.getY(), projection.getX() + ((float) (projection.getWidth() / 2)) + (this.bowWidth / f), projection.getY() + this.bowHeight);
        Drawable drawable = this.bowDrawable;
        RectF rectF = this.rect;
        Rect rect = new Rect();
        rectF.roundOut(rect);
        drawable.setBounds(rect);
        int save = canvas.save();
        canvas.translate(0.0f, (((float) projection.getHeight()) - this.rect.height()) * 0.53932583f);
        try {
            this.bowDrawable.draw(canvas);
        } finally {
            canvas.restoreToCount(save);
        }
    }

    private final void startShakeAnimation(OpenableGift openableGift) {
        this.animationState.put(Long.valueOf(openableGift.getGiftId()), new GiftAnimationState.ShakeAnimationState(openableGift, System.currentTimeMillis()));
    }

    public final void startOpenAnimation(OpenableGift openableGift) {
        this.animationState.put(Long.valueOf(openableGift.getGiftId()), new GiftAnimationState.OpenAnimationState(openableGift, System.currentTimeMillis()));
    }

    /* compiled from: OpenableGiftItemDecoration.kt */
    @Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u001b\u001cB\u001f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\\\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u000e0\u00172\u0018\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u000e0\u0017H$JJ\u0010\r\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u000f\u001a\u00020\u00102\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u000e0\u00172\u0018\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u000e0\u0017R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u0001\u0002\u001d\u001e¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState;", "", "openableGift", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;", "startTime", "", "duration", "(Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;JJ)V", "getDuration", "()J", "getOpenableGift", "()Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;", "getStartTime", "update", "", "canvas", "Landroid/graphics/Canvas;", "projection", "Lorg/thoughtcrime/securesms/util/Projection;", "progress", "", "lastFrameProgress", "drawBox", "Lkotlin/Function2;", "drawBow", "", "animatorDurationScale", "OpenAnimationState", "ShakeAnimationState", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState$ShakeAnimationState;", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState$OpenAnimationState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class GiftAnimationState {
        private final long duration;
        private final OpenableGift openableGift;
        private final long startTime;

        public /* synthetic */ GiftAnimationState(OpenableGift openableGift, long j, long j2, DefaultConstructorMarker defaultConstructorMarker) {
            this(openableGift, j, j2);
        }

        protected abstract void update(Canvas canvas, Projection projection, float f, float f2, Function2<? super Canvas, ? super Projection, Unit> function2, Function2<? super Canvas, ? super Projection, Unit> function22);

        private GiftAnimationState(OpenableGift openableGift, long j, long j2) {
            this.openableGift = openableGift;
            this.startTime = j;
            this.duration = j2;
        }

        public final long getDuration() {
            return this.duration;
        }

        public final OpenableGift getOpenableGift() {
            return this.openableGift;
        }

        public final long getStartTime() {
            return this.startTime;
        }

        /* compiled from: OpenableGiftItemDecoration.kt */
        @Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\\\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\n2\u0018\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\f0\u00132\u0018\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\f0\u0013H\u0014¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState$ShakeAnimationState;", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState;", "openableGift", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;", "startTime", "", "(Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;J)V", "getTranslation", "", "progress", "", "update", "", "canvas", "Landroid/graphics/Canvas;", "projection", "Lorg/thoughtcrime/securesms/util/Projection;", "lastFrameProgress", "drawBox", "Lkotlin/Function2;", "drawBow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class ShakeAnimationState extends GiftAnimationState {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public ShakeAnimationState(OpenableGift openableGift, long j) {
                super(openableGift, j, OpenableGiftItemDecoration.SHAKE_DURATION_MILLIS, null);
                Intrinsics.checkNotNullParameter(openableGift, "openableGift");
            }

            @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration.GiftAnimationState
            protected void update(Canvas canvas, Projection projection, float f, float f2, Function2<? super Canvas, ? super Projection, Unit> function2, Function2<? super Canvas, ? super Projection, Unit> function22) {
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                Intrinsics.checkNotNullParameter(projection, "projection");
                Intrinsics.checkNotNullParameter(function2, "drawBox");
                Intrinsics.checkNotNullParameter(function22, "drawBow");
                float translation = (float) getTranslation(f);
                int save = canvas.save();
                canvas.translate(translation, 0.0f);
                try {
                    function2.invoke(canvas, projection);
                    canvas.restoreToCount(save);
                    save = canvas.save();
                    canvas.translate((float) getTranslation(f2), 0.0f);
                    try {
                        function22.invoke(canvas, projection);
                    } finally {
                    }
                } finally {
                }
            }

            private final double getTranslation(float f) {
                Float evaluate = OpenableGiftItemDecoration.EVALUATOR.evaluate(OpenableGiftItemDecoration.TRANSLATION_X_INTERPOLATOR.getInterpolation(f), (Number) Float.valueOf(0.0f), (Number) Float.valueOf(360.0f));
                double d = (double) 0.25f;
                Intrinsics.checkNotNullExpressionValue(evaluate, "evaluated");
                double floatValue = (double) (((float) 4) * evaluate.floatValue());
                Double.isNaN(floatValue);
                double d2 = (double) 180.0f;
                Double.isNaN(d2);
                double sin = Math.sin((floatValue * 3.141592653589793d) / d2);
                Double.isNaN(d);
                Double.isNaN(d2);
                return ((d * sin) * d2) / 3.141592653589793d;
            }
        }

        /* compiled from: OpenableGiftItemDecoration.kt */
        @Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\\\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00152\u0018\u0010\u0017\u001a\u0014\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000f0\u00182\u0018\u0010\u0019\u001a\u0014\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000f0\u0018H\u0014R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState$OpenAnimationState;", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$GiftAnimationState;", "openableGift", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;", "startTime", "", "(Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;J)V", "bowRotationInterpolator", "Landroid/view/animation/Interpolator;", "kotlin.jvm.PlatformType", "bowRotationPath", "Landroid/graphics/Path;", "boxRotationInterpolator", "boxRotationPath", "update", "", "canvas", "Landroid/graphics/Canvas;", "projection", "Lorg/thoughtcrime/securesms/util/Projection;", "progress", "", "lastFrameProgress", "drawBox", "Lkotlin/Function2;", "drawBow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class OpenAnimationState extends GiftAnimationState {
            private final Interpolator bowRotationInterpolator;
            private final Path bowRotationPath;
            private final Interpolator boxRotationInterpolator;
            private final Path boxRotationPath;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public OpenAnimationState(OpenableGift openableGift, long j) {
                super(openableGift, j, OpenableGiftItemDecoration.OPEN_DURATION_MILLIS, null);
                Intrinsics.checkNotNullParameter(openableGift, "openableGift");
                Path path = new Path();
                path.lineTo(0.13f, -0.75f);
                path.lineTo(0.26f, 0.0f);
                path.lineTo(0.73f, -1.375f);
                path.lineTo(1.0f, 1.0f);
                this.bowRotationPath = path;
                Path path2 = new Path();
                path2.lineTo(0.63f, -1.6f);
                path2.lineTo(1.0f, 1.0f);
                this.boxRotationPath = path2;
                this.bowRotationInterpolator = PathInterpolatorCompat.create(path);
                this.boxRotationInterpolator = PathInterpolatorCompat.create(path2);
            }

            @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration.GiftAnimationState
            protected void update(Canvas canvas, Projection projection, float f, float f2, Function2<? super Canvas, ? super Projection, Unit> function2, Function2<? super Canvas, ? super Projection, Unit> function22) {
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                Intrinsics.checkNotNullParameter(projection, "projection");
                Intrinsics.checkNotNullParameter(function2, "drawBox");
                Intrinsics.checkNotNullParameter(function22, "drawBow");
                float sign = getOpenableGift().getAnimationSign().getSign();
                float duration = ((float) OpenableGiftItemDecoration.OPEN_BOX_START_DELAY) / ((float) getDuration());
                Float valueOf = Float.valueOf(0.0f);
                float max = Math.max(0.0f, f - duration) / (1.0f - duration);
                float duration2 = ((float) OpenableGiftItemDecoration.OPEN_BOW_START_DELAY) / ((float) getDuration());
                float max2 = Math.max(0.0f, f - duration2) / (1.0f - duration2);
                float interpolation = OpenableGiftItemDecoration.TRANSLATION_X_INTERPOLATOR.getInterpolation(max);
                FloatEvaluator floatEvaluator = OpenableGiftItemDecoration.EVALUATOR;
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                Float evaluate = floatEvaluator.evaluate(interpolation, (Number) valueOf, (Number) Float.valueOf(dimensionUnit.toPixels(18.0f * sign)));
                Float evaluate2 = OpenableGiftItemDecoration.EVALUATOR.evaluate(OpenableGiftItemDecoration.TRANSLATION_Y_INTERPOLATOR.getInterpolation(max), (Number) valueOf, (Number) Float.valueOf(dimensionUnit.toPixels(355.0f)));
                Float evaluate3 = OpenableGiftItemDecoration.EVALUATOR.evaluate(this.bowRotationInterpolator.getInterpolation(max2), (Number) valueOf, (Number) Float.valueOf(8.0f * sign));
                Float evaluate4 = OpenableGiftItemDecoration.EVALUATOR.evaluate(this.boxRotationInterpolator.getInterpolation(max), (Number) valueOf, (Number) Float.valueOf(sign * -5.0f));
                Intrinsics.checkNotNullExpressionValue(evaluate, "evaluatedX");
                float floatValue = evaluate.floatValue();
                Intrinsics.checkNotNullExpressionValue(evaluate2, "evaluatedY");
                float floatValue2 = evaluate2.floatValue();
                int save = canvas.save();
                canvas.translate(floatValue, floatValue2);
                try {
                    Intrinsics.checkNotNullExpressionValue(evaluate4, "evaluatedBoxRotation");
                    float floatValue3 = evaluate4.floatValue();
                    float x = projection.getX() + (((float) projection.getWidth()) / 2.0f);
                    float y = projection.getY() + (((float) projection.getHeight()) / 2.0f);
                    int save2 = canvas.save();
                    canvas.rotate(floatValue3, x, y);
                    function2.invoke(canvas, projection);
                    Intrinsics.checkNotNullExpressionValue(evaluate3, "evaluatedBowRotation");
                    float floatValue4 = evaluate3.floatValue();
                    float x2 = projection.getX() + (((float) projection.getWidth()) / 2.0f);
                    float y2 = projection.getY() + (((float) projection.getHeight()) / 2.0f);
                    int save3 = canvas.save();
                    canvas.rotate(floatValue4, x2, y2);
                    function22.invoke(canvas, projection);
                    canvas.restoreToCount(save3);
                    canvas.restoreToCount(save2);
                } finally {
                    canvas.restoreToCount(save);
                }
            }
        }

        public final boolean update(float f, Canvas canvas, Function2<? super Canvas, ? super Projection, Unit> function2, Function2<? super Canvas, ? super Projection, Unit> function22) {
            Intrinsics.checkNotNullParameter(canvas, "canvas");
            Intrinsics.checkNotNullParameter(function2, "drawBox");
            Intrinsics.checkNotNullParameter(function22, "drawBow");
            Projection openableGiftProjection = this.openableGift.getOpenableGiftProjection(true);
            if (openableGiftProjection == null) {
                return false;
            }
            if (f <= 0.0f) {
                update(canvas, openableGiftProjection, 0.0f, 0.0f, function2, function22);
                openableGiftProjection.release();
                return false;
            }
            long currentTimeMillis = System.currentTimeMillis();
            float max = Math.max(0.0f, ((float) ((currentTimeMillis - this.startTime) - ((long) 33))) / (((float) this.duration) * f));
            float f2 = ((float) (currentTimeMillis - this.startTime)) / (((float) this.duration) * f);
            if (f2 > 1.0f) {
                update(canvas, openableGiftProjection, 1.0f, 1.0f, function2, function22);
                openableGiftProjection.release();
                return false;
            }
            update(canvas, openableGiftProjection, f2, max, function2, function22);
            openableGiftProjection.release();
            return true;
        }
    }

    /* compiled from: OpenableGiftItemDecoration.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGiftItemDecoration$Companion;", "", "()V", "EVALUATOR", "Landroid/animation/FloatEvaluator;", "ONE_FRAME_RELATIVE_TO_30_FPS_MILLIS", "", "OPEN_BOW_START_DELAY", "", "OPEN_BOX_START_DELAY", "OPEN_DURATION_MILLIS", "SHAKE_DURATION_MILLIS", "TRANSLATION_X_INTERPOLATOR", "Landroid/view/animation/AccelerateDecelerateInterpolator;", "TRANSLATION_Y_INTERPOLATOR", "Landroid/view/animation/AnticipateInterpolator;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
