package org.thoughtcrime.securesms.badges.gifts.flow;

import io.reactivex.rxjava3.functions.Function;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class GiftFlowRepository$$ExternalSyntheticLambda2 implements Function {
    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return ((ServiceResponse) obj).flattenResult();
    }
}
