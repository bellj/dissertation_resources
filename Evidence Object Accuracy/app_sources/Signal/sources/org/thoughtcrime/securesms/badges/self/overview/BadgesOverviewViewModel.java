package org.thoughtcrime.securesms.badges.self.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import j$.util.Optional;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewState;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: BadgesOverviewViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 '2\u00020\u0001:\u0002'(B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b%\u0010&J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002J\b\u0010\u0006\u001a\u00020\u0004H\u0014R\u0014\u0010\b\u001a\u00020\u00078\u0002X\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0014\u0010\u000b\u001a\u00020\n8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R8\u0010\u0014\u001a&\u0012\f\u0012\n \u0013*\u0004\u0018\u00010\u00120\u0012 \u0013*\u0012\u0012\f\u0012\n \u0013*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u00110\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00168\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00120\u001b8\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0017\u0010!\u001a\u00020 8\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewViewModel;", "Landroidx/lifecycle/ViewModel;", "", "displayBadgesOnProfile", "", "setDisplayBadgesOnProfile", "onCleared", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "badgeRepository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewEvent;", "kotlin.jvm.PlatformType", "eventSubject", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Landroidx/lifecycle/LiveData;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/core/Observable;", "events", "Lio/reactivex/rxjava3/core/Observable;", "getEvents", "()Lio/reactivex/rxjava3/core/Observable;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "getDisposables", "()Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "<init>", "(Lorg/thoughtcrime/securesms/badges/BadgeRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;)V", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class BadgesOverviewViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(BadgesOverviewViewModel.class);
    private final BadgeRepository badgeRepository;
    private final CompositeDisposable disposables;
    private final PublishSubject<BadgesOverviewEvent> eventSubject;
    private final Observable<BadgesOverviewEvent> events;
    private final LiveData<BadgesOverviewState> state;
    private final Store<BadgesOverviewState> store;
    private final SubscriptionsRepository subscriptionsRepository;

    /* JADX DEBUG: Type inference failed for r1v7. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public BadgesOverviewViewModel(BadgeRepository badgeRepository, SubscriptionsRepository subscriptionsRepository) {
        Intrinsics.checkNotNullParameter(badgeRepository, "badgeRepository");
        Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
        this.badgeRepository = badgeRepository;
        this.subscriptionsRepository = subscriptionsRepository;
        Store<BadgesOverviewState> store = new Store<>(new BadgesOverviewState(null, null, null, false, null, false, 63, null));
        this.store = store;
        PublishSubject<BadgesOverviewEvent> create = PublishSubject.create();
        this.eventSubject = create;
        LiveData<BadgesOverviewState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Observable<BadgesOverviewEvent> observeOn = create.observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "eventSubject.observeOn(A…dSchedulers.mainThread())");
        this.events = observeOn;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        store.update(Recipient.live(Recipient.self().getId()).getLiveDataResolved(), new Store.Action() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return BadgesOverviewViewModel.m445_init_$lambda0((Recipient) obj, (BadgesOverviewState) obj2);
            }
        });
        Observable<Boolean> distinctUntilChanged = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged();
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "InternetConnectionObserv…  .distinctUntilChanged()");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(distinctUntilChanged, (Function1) null, (Function0) null, new Function1<Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel.2
            final /* synthetic */ BadgesOverviewViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                invoke(bool);
                return Unit.INSTANCE;
            }

            /* renamed from: invoke$lambda-0 */
            public static final BadgesOverviewState m452invoke$lambda0(Boolean bool, BadgesOverviewState badgesOverviewState) {
                Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "it");
                Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
                return BadgesOverviewState.copy$default(badgesOverviewState, null, null, null, false, null, bool.booleanValue(), 31, null);
            }

            public final void invoke(Boolean bool) {
                this.this$0.store.update(new BadgesOverviewViewModel$2$$ExternalSyntheticLambda0(bool));
            }
        }, 3, (Object) null));
        Single zip = Single.zip(subscriptionsRepository.getActiveSubscription(), subscriptionsRepository.getSubscriptions(), new BiFunction() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return BadgesOverviewViewModel.m446_init_$lambda2((ActiveSubscription) obj, (List) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(zip, "zip(\n      subscriptions…nal.empty()\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy(zip, AnonymousClass4.INSTANCE, new Function1<Optional<String>, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel.5
            final /* synthetic */ BadgesOverviewViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Optional<String> optional) {
                invoke(optional);
                return Unit.INSTANCE;
            }

            /* renamed from: invoke$lambda-0 */
            public static final BadgesOverviewState m454invoke$lambda0(Optional optional, BadgesOverviewState badgesOverviewState) {
                Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "it");
                return BadgesOverviewState.copy$default(badgesOverviewState, null, null, null, false, (String) optional.orElse(null), false, 47, null);
            }

            public final void invoke(Optional<String> optional) {
                this.this$0.store.update(new BadgesOverviewViewModel$5$$ExternalSyntheticLambda0(optional));
            }
        }));
    }

    public final LiveData<BadgesOverviewState> getState() {
        return this.state;
    }

    public final Observable<BadgesOverviewEvent> getEvents() {
        return this.events;
    }

    public final CompositeDisposable getDisposables() {
        return this.disposables;
    }

    /* renamed from: _init_$lambda-0 */
    public static final BadgesOverviewState m445_init_$lambda0(Recipient recipient, BadgesOverviewState badgesOverviewState) {
        BadgesOverviewState.Stage stage = badgesOverviewState.getStage() == BadgesOverviewState.Stage.INIT ? BadgesOverviewState.Stage.READY : badgesOverviewState.getStage();
        List<Badge> badges = recipient.getBadges();
        boolean displayBadgesOnProfile = SignalStore.donationsValues().getDisplayBadgesOnProfile();
        Badge featuredBadge = recipient.getFeaturedBadge();
        Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "state");
        Intrinsics.checkNotNullExpressionValue(badges, "badges");
        return BadgesOverviewState.copy$default(badgesOverviewState, stage, badges, featuredBadge, displayBadgesOnProfile, null, false, 48, null);
    }

    /* renamed from: _init_$lambda-2 */
    public static final Optional m446_init_$lambda2(ActiveSubscription activeSubscription, List list) {
        String str;
        Object obj;
        Badge badge;
        boolean z;
        if (!activeSubscription.isActive()) {
            ActiveSubscription.Subscription activeSubscription2 = activeSubscription.getActiveSubscription();
            if (activeSubscription2 != null && activeSubscription2.willCancelAtPeriodEnd()) {
                Intrinsics.checkNotNullExpressionValue(list, "all");
                Iterator it = list.iterator();
                while (true) {
                    str = null;
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    Subscription subscription = (Subscription) obj;
                    ActiveSubscription.Subscription activeSubscription3 = activeSubscription.getActiveSubscription();
                    if (activeSubscription3 == null || subscription.getLevel() != activeSubscription3.getLevel()) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                Subscription subscription2 = (Subscription) obj;
                if (!(subscription2 == null || (badge = subscription2.getBadge()) == null)) {
                    str = badge.getId();
                }
                return Optional.ofNullable(str);
            }
        }
        return Optional.empty();
    }

    /* renamed from: setDisplayBadgesOnProfile$lambda-3 */
    public static final BadgesOverviewState m447setDisplayBadgesOnProfile$lambda3(BadgesOverviewState badgesOverviewState) {
        Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "it");
        return BadgesOverviewState.copy$default(badgesOverviewState, BadgesOverviewState.Stage.UPDATING_BADGE_DISPLAY_STATE, null, null, false, null, false, 62, null);
    }

    public final void setDisplayBadgesOnProfile(boolean z) {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BadgesOverviewViewModel.m447setDisplayBadgesOnProfile$lambda3((BadgesOverviewState) obj);
            }
        });
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = BadgeRepository.setVisibilityForAllBadges$default(this.badgeRepository, z, null, 2, null).subscribe(new Action() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                BadgesOverviewViewModel.m448setDisplayBadgesOnProfile$lambda5(BadgesOverviewViewModel.this);
            }
        }, new Consumer() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BadgesOverviewViewModel.m450setDisplayBadgesOnProfile$lambda7(BadgesOverviewViewModel.this, (Throwable) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "badgeRepository.setVisib…ROFILE)\n        }\n      )");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: setDisplayBadgesOnProfile$lambda-5 */
    public static final void m448setDisplayBadgesOnProfile$lambda5(BadgesOverviewViewModel badgesOverviewViewModel) {
        Intrinsics.checkNotNullParameter(badgesOverviewViewModel, "this$0");
        badgesOverviewViewModel.store.update(new Function() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BadgesOverviewViewModel.m449setDisplayBadgesOnProfile$lambda5$lambda4((BadgesOverviewState) obj);
            }
        });
    }

    /* renamed from: setDisplayBadgesOnProfile$lambda-5$lambda-4 */
    public static final BadgesOverviewState m449setDisplayBadgesOnProfile$lambda5$lambda4(BadgesOverviewState badgesOverviewState) {
        Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "it");
        return BadgesOverviewState.copy$default(badgesOverviewState, BadgesOverviewState.Stage.READY, null, null, false, null, false, 62, null);
    }

    /* renamed from: setDisplayBadgesOnProfile$lambda-7 */
    public static final void m450setDisplayBadgesOnProfile$lambda7(BadgesOverviewViewModel badgesOverviewViewModel, Throwable th) {
        Intrinsics.checkNotNullParameter(badgesOverviewViewModel, "this$0");
        Log.e(TAG, "Failed to update visibility.", th);
        badgesOverviewViewModel.store.update(new Function() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BadgesOverviewViewModel.m451setDisplayBadgesOnProfile$lambda7$lambda6((BadgesOverviewState) obj);
            }
        });
        badgesOverviewViewModel.eventSubject.onNext(BadgesOverviewEvent.FAILED_TO_UPDATE_PROFILE);
    }

    /* renamed from: setDisplayBadgesOnProfile$lambda-7$lambda-6 */
    public static final BadgesOverviewState m451setDisplayBadgesOnProfile$lambda7$lambda6(BadgesOverviewState badgesOverviewState) {
        Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "it");
        return BadgesOverviewState.copy$default(badgesOverviewState, BadgesOverviewState.Stage.READY, null, null, false, null, false, 62, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: BadgesOverviewViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "badgeRepository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "(Lorg/thoughtcrime/securesms/badges/BadgeRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final BadgeRepository badgeRepository;
        private final SubscriptionsRepository subscriptionsRepository;

        public Factory(BadgeRepository badgeRepository, SubscriptionsRepository subscriptionsRepository) {
            Intrinsics.checkNotNullParameter(badgeRepository, "badgeRepository");
            Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
            this.badgeRepository = badgeRepository;
            this.subscriptionsRepository = subscriptionsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new BadgesOverviewViewModel(this.badgeRepository, this.subscriptionsRepository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* compiled from: BadgesOverviewViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
