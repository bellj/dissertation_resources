package org.thoughtcrime.securesms.badges.self.overview;

import android.content.Context;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.FragmentKt;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragmentDirections;
import org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewState;
import org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel;
import org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.services.DonationsService;

/* compiled from: BadgesOverviewFragment.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/badges/self/overview/BadgesOverviewState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgesOverviewFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(BadgesOverviewViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$viewModel$2
        final /* synthetic */ BadgesOverviewFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            BadgeRepository badgeRepository = new BadgeRepository(requireContext);
            DonationsService donationsService = ApplicationDependencies.getDonationsService();
            Intrinsics.checkNotNullExpressionValue(donationsService, "getDonationsService()");
            return new BadgesOverviewViewModel.Factory(badgeRepository, new SubscriptionsRepository(donationsService));
        }
    });

    /* compiled from: BadgesOverviewFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[BadgesOverviewEvent.values().length];
            iArr[BadgesOverviewEvent.FAILED_TO_UPDATE_PROFILE.ordinal()] = 1;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public BadgesOverviewFragment() {
        super(R.string.ManageProfileFragment_badges, 0, 0, new Function1<Context, RecyclerView.LayoutManager>(Badges.INSTANCE) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment.1
            public final RecyclerView.LayoutManager invoke(Context context) {
                Intrinsics.checkNotNullParameter(context, "p0");
                return ((Badges) this.receiver).createLayoutManagerForGridWithBadges(context);
            }
        }, 6, null);
    }

    public final BadgesOverviewViewModel getViewModel() {
        return (BadgesOverviewViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        Badge.Companion.register(dSLSettingsAdapter, new Function3<Badge, Boolean, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$bindAdapter$1
            final /* synthetic */ BadgesOverviewFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function3
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2, Object obj3) {
                invoke((Badge) obj, ((Boolean) obj2).booleanValue(), ((Boolean) obj3).booleanValue());
                return Unit.INSTANCE;
            }

            public final void invoke(Badge badge, boolean z, boolean z2) {
                Intrinsics.checkNotNullParameter(badge, "badge");
                if (badge.isExpired() || z2) {
                    NavController findNavController = FragmentKt.findNavController(this.this$0);
                    BadgesOverviewFragmentDirections.ActionBadgeManageFragmentToExpiredBadgeDialog actionBadgeManageFragmentToExpiredBadgeDialog = BadgesOverviewFragmentDirections.actionBadgeManageFragmentToExpiredBadgeDialog(badge, null, null);
                    Intrinsics.checkNotNullExpressionValue(actionBadgeManageFragmentToExpiredBadgeDialog, "actionBadgeManageFragmen…Dialog(badge, null, null)");
                    SafeNavigation.safeNavigate(findNavController, actionBadgeManageFragmentToExpiredBadgeDialog);
                    return;
                }
                ViewBadgeBottomSheetDialogFragment.Companion companion = ViewBadgeBottomSheetDialogFragment.Companion;
                FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
                Intrinsics.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                RecipientId id = Recipient.self().getId();
                Intrinsics.checkNotNullExpressionValue(id, "self().id");
                companion.show(parentFragmentManager, id, badge);
            }
        });
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ BadgesOverviewFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BadgesOverviewFragment.$r8$lambda$yNbQjuCf0UmBmncJ5MzjTrIIngk(DSLSettingsAdapter.this, this.f$1, (BadgesOverviewState) obj);
            }
        });
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getEvents().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BadgesOverviewFragment.$r8$lambda$Ae8OvzmIysqSJincTbxoLU0axfc(BadgesOverviewFragment.this, (BadgesOverviewEvent) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.events.subscri….show()\n        }\n      }");
        lifecycleDisposable2.add(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m440bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, BadgesOverviewFragment badgesOverviewFragment, BadgesOverviewState badgesOverviewState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(badgesOverviewFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(badgesOverviewState, "state");
        dSLSettingsAdapter.submitList(badgesOverviewFragment.getConfiguration(badgesOverviewState).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m441bindAdapter$lambda1(BadgesOverviewFragment badgesOverviewFragment, BadgesOverviewEvent badgesOverviewEvent) {
        Intrinsics.checkNotNullParameter(badgesOverviewFragment, "this$0");
        Intrinsics.checkNotNullParameter(badgesOverviewEvent, "event");
        if (WhenMappings.$EnumSwitchMapping$0[badgesOverviewEvent.ordinal()] == 1) {
            Toast.makeText(badgesOverviewFragment.requireContext(), (int) R.string.BadgesOverviewFragment__failed_to_update_profile, 1).show();
        }
    }

    private final DSLConfiguration getConfiguration(BadgesOverviewState badgesOverviewState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, badgesOverviewState) { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$getConfiguration$1
            final /* synthetic */ BadgesOverviewState $state;
            final /* synthetic */ BadgesOverviewFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$state = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                String name;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.BadgesOverviewFragment__my_badges);
                Badges badges = Badges.INSTANCE;
                Context requireContext = this.this$0.requireContext();
                Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                badges.displayBadges(dSLConfiguration, requireContext, this.$state.getAllUnlockedBadges(), (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : this.$state.getFadedBadgeId());
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.BadgesOverviewFragment__display_badges_on_profile, new DSLSettingsText.Modifier[0]);
                boolean displayBadgesOnProfile = this.$state.getDisplayBadgesOnProfile();
                BadgesOverviewState.Stage stage = this.$state.getStage();
                BadgesOverviewState.Stage stage2 = BadgesOverviewState.Stage.READY;
                boolean z = stage == stage2 && this.$state.getHasUnexpiredBadges() && this.$state.getHasInternet();
                boolean z2 = this.$state.getStage() == BadgesOverviewState.Stage.UPDATING_BADGE_DISPLAY_STATE;
                final BadgesOverviewFragment badgesOverviewFragment = this.this$0;
                final BadgesOverviewState badgesOverviewState2 = this.$state;
                dSLConfiguration.asyncSwitchPref(from, z, displayBadgesOnProfile, z2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        BadgesOverviewFragment.access$getViewModel(badgesOverviewFragment).setDisplayBadgesOnProfile(!badgesOverviewState2.getDisplayBadgesOnProfile());
                    }
                });
                DSLSettingsText from2 = companion.from(R.string.BadgesOverviewFragment__featured_badge, new DSLSettingsText.Modifier[0]);
                Badge featuredBadge = this.$state.getFeaturedBadge();
                DSLSettingsText from3 = (featuredBadge == null || (name = featuredBadge.getName()) == null) ? null : companion.from(name, new DSLSettingsText.Modifier[0]);
                boolean z3 = this.$state.getStage() == stage2 && this.$state.getHasUnexpiredBadges() && this.$state.getHasInternet();
                final BadgesOverviewFragment badgesOverviewFragment2 = this.this$0;
                dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : from3, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : z3, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = FragmentKt.findNavController(badgesOverviewFragment2);
                        NavDirections actionBadgeManageFragmentToFeaturedBadgeFragment = BadgesOverviewFragmentDirections.actionBadgeManageFragmentToFeaturedBadgeFragment();
                        Intrinsics.checkNotNullExpressionValue(actionBadgeManageFragmentToFeaturedBadgeFragment, "actionBadgeManageFragmentToFeaturedBadgeFragment()");
                        SafeNavigation.safeNavigate(findNavController, actionBadgeManageFragmentToFeaturedBadgeFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
            }
        });
    }
}
