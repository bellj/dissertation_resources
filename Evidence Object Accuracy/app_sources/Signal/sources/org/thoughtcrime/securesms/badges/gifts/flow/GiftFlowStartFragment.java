package org.thoughtcrime.securesms.badges.gifts.flow;

import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.Currency;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragmentDirections;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowState;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowViewModel;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftRowItem;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.CurrencySelection;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.NetworkFailure;
import org.thoughtcrime.securesms.components.settings.models.IndeterminateLoadingCircle;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: GiftFlowStartFragment.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\fH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowStartFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowState;", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowStartFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate;

    public GiftFlowStartFragment() {
        super(0, 0, R.layout.gift_flow_start_fragment, null, 11, null);
        GiftFlowStartFragment$viewModel$2 giftFlowStartFragment$viewModel$2 = new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$viewModel$2
            final /* synthetic */ GiftFlowStartFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                FragmentActivity requireActivity = this.this$0.requireActivity();
                Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                return requireActivity;
            }
        };
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(GiftFlowViewModel.class), new Function0<ViewModelStore>(giftFlowStartFragment$viewModel$2) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$special$$inlined$viewModels$1
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$viewModel$3
            final /* synthetic */ GiftFlowStartFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelProvider.Factory invoke() {
                DonationPaymentComponent donationPaymentComponent;
                GiftFlowRepository giftFlowRepository = new GiftFlowRepository();
                GiftFlowStartFragment giftFlowStartFragment = this.this$0;
                ArrayList arrayList = new ArrayList();
                try {
                    Fragment fragment = giftFlowStartFragment.getParentFragment();
                    while (true) {
                        if (fragment == null) {
                            FragmentActivity requireActivity = giftFlowStartFragment.requireActivity();
                            if (requireActivity != null) {
                                donationPaymentComponent = (DonationPaymentComponent) requireActivity;
                            } else {
                                throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent");
                            }
                        } else if (fragment instanceof DonationPaymentComponent) {
                            donationPaymentComponent = fragment;
                            break;
                        } else {
                            String name = fragment.getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                            arrayList.add(name);
                            fragment = fragment.getParentFragment();
                        }
                    }
                    return new GiftFlowViewModel.Factory(giftFlowRepository, donationPaymentComponent.getDonationPaymentRepository());
                } catch (ClassCastException e) {
                    String name2 = giftFlowStartFragment.requireActivity().getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                    arrayList.add(name2);
                    throw new ListenerNotFoundException(arrayList, e);
                }
            }
        });
    }

    public final GiftFlowViewModel getViewModel() {
        return (GiftFlowViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        CurrencySelection.INSTANCE.register(dSLSettingsAdapter);
        GiftRowItem.INSTANCE.register(dSLSettingsAdapter);
        NetworkFailure.INSTANCE.register(dSLSettingsAdapter);
        IndeterminateLoadingCircle.INSTANCE.register(dSLSettingsAdapter);
        View findViewById = requireView().findViewById(R.id.next);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GiftFlowStartFragment.$r8$lambda$oCUIJlp_K8KuivwjRvkfBsba8Mc(GiftFlowStartFragment.this, view);
            }
        });
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(findViewById, dSLSettingsAdapter, this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$0;
            public final /* synthetic */ DSLSettingsAdapter f$1;
            public final /* synthetic */ GiftFlowStartFragment f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                GiftFlowStartFragment.m386$r8$lambda$LyqWIBxZNQfmPT1ssLO4_a10ak(this.f$0, this.f$1, this.f$2, (GiftFlowState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.observeO…MappingModelList())\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m387bindAdapter$lambda0(GiftFlowStartFragment giftFlowStartFragment, View view) {
        Intrinsics.checkNotNullParameter(giftFlowStartFragment, "this$0");
        SafeNavigation.safeNavigate(FragmentKt.findNavController(giftFlowStartFragment), (int) R.id.action_giftFlowStartFragment_to_giftFlowRecipientSelectionFragment);
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m388bindAdapter$lambda1(View view, DSLSettingsAdapter dSLSettingsAdapter, GiftFlowStartFragment giftFlowStartFragment, GiftFlowState giftFlowState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(giftFlowStartFragment, "this$0");
        view.setEnabled(giftFlowState.getStage() == GiftFlowState.Stage.READY);
        Intrinsics.checkNotNullExpressionValue(giftFlowState, "state");
        dSLSettingsAdapter.submitList(giftFlowStartFragment.getConfiguration(giftFlowState).toMappingModelList());
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ViewUtil.hideKeyboard(requireContext(), requireView());
    }

    private final DSLConfiguration getConfiguration(GiftFlowState giftFlowState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(giftFlowState, this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$getConfiguration$1
            final /* synthetic */ GiftFlowState $state;
            final /* synthetic */ GiftFlowStartFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                FiatMoney fiatMoney;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                Currency currency = this.$state.getCurrency();
                boolean z = this.$state.getStage() == GiftFlowState.Stage.READY;
                final GiftFlowStartFragment giftFlowStartFragment = this.this$0;
                dSLConfiguration.customPref(new CurrencySelection.Model(currency, z, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        Object[] array = GiftFlowStartFragment.access$getViewModel(giftFlowStartFragment).getSupportedCurrencyCodes().toArray(new String[0]);
                        if (array != null) {
                            GiftFlowStartFragmentDirections.ActionGiftFlowStartFragmentToSetCurrencyFragment actionGiftFlowStartFragmentToSetCurrencyFragment = GiftFlowStartFragmentDirections.actionGiftFlowStartFragmentToSetCurrencyFragment(true, (String[]) array);
                            Intrinsics.checkNotNullExpressionValue(actionGiftFlowStartFragmentToSetCurrencyFragment, "actionGiftFlowStartFragm…cyCodes().toTypedArray())");
                            SafeNavigation.safeNavigate(FragmentKt.findNavController(giftFlowStartFragment), actionGiftFlowStartFragmentToSetCurrencyFragment);
                            return;
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                    }
                }));
                if (this.$state.getStage() == GiftFlowState.Stage.FAILURE) {
                    final GiftFlowStartFragment giftFlowStartFragment2 = this.this$0;
                    dSLConfiguration.customPref(new NetworkFailure.Model(new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowStartFragment$getConfiguration$1.2
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            GiftFlowStartFragment.access$getViewModel(giftFlowStartFragment2).retry();
                        }
                    }));
                } else if (this.$state.getStage() == GiftFlowState.Stage.INIT) {
                    dSLConfiguration.customPref(IndeterminateLoadingCircle.INSTANCE);
                } else if (this.$state.getGiftBadge() != null && (fiatMoney = this.$state.getGiftPrices().get(this.$state.getCurrency())) != null) {
                    dSLConfiguration.customPref(new GiftRowItem.Model(this.$state.getGiftBadge(), fiatMoney));
                }
            }
        });
    }
}
