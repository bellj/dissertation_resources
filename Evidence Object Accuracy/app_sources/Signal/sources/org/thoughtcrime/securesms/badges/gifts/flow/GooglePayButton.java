package org.thoughtcrime.securesms.badges.gifts.flow;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: GooglePayButton.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GooglePayButton;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "setOnGooglePayClickListener", "", "action", "Lkotlin/Function0;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GooglePayButton extends FrameLayout {
    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public GooglePayButton(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ GooglePayButton(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public GooglePayButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        FrameLayout.inflate(context, R.layout.donate_with_googlepay_button, this);
    }

    /* renamed from: setOnGooglePayClickListener$lambda-0 */
    public static final void m394setOnGooglePayClickListener$lambda0(Function0 function0, View view) {
        Intrinsics.checkNotNullParameter(function0, "$action");
        function0.invoke();
    }

    public final void setOnGooglePayClickListener(Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(function0, "action");
        getChildAt(0).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GooglePayButton$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GooglePayButton.m394setOnGooglePayClickListener$lambda0(Function0.this, view);
            }
        });
    }
}
