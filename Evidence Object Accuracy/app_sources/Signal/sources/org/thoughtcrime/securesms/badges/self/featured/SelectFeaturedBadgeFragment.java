package org.thoughtcrime.securesms.badges.self.featured;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgePreview;
import org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeState;
import org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeViewModel;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;

/* compiled from: SelectFeaturedBadgeFragment.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\u001a\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u00062\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u001b\u0010\b\u001a\u00020\t8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "save", "Landroid/view/View;", "scrollShadow", "viewModel", "Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/badges/self/featured/SelectFeaturedBadgeState;", "getMaterial3OnScrollHelper", "Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper;", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SelectFeaturedBadgeFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private View save;
    private View scrollShadow;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SelectFeaturedBadgeViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$viewModel$2
        final /* synthetic */ SelectFeaturedBadgeFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new SelectFeaturedBadgeViewModel.Factory(new BadgeRepository(requireContext));
        }
    });

    /* compiled from: SelectFeaturedBadgeFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[SelectFeaturedBadgeEvent.values().length];
            iArr[SelectFeaturedBadgeEvent.NO_BADGE_SELECTED.ordinal()] = 1;
            iArr[SelectFeaturedBadgeEvent.FAILED_TO_UPDATE_PROFILE.ordinal()] = 2;
            iArr[SelectFeaturedBadgeEvent.SAVE_SUCCESSFUL.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public SelectFeaturedBadgeFragment() {
        super(R.string.BadgesOverviewFragment__featured_badge, 0, R.layout.select_featured_badge_fragment, new Function1<Context, RecyclerView.LayoutManager>(Badges.INSTANCE) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment.1
            public final RecyclerView.LayoutManager invoke(Context context) {
                Intrinsics.checkNotNullParameter(context, "p0");
                return ((Badges) this.receiver).createLayoutManagerForGridWithBadges(context);
            }
        }, 2, null);
    }

    public final SelectFeaturedBadgeViewModel getViewModel() {
        return (SelectFeaturedBadgeViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.scroll_shadow);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.scroll_shadow)");
        this.scrollShadow = findViewById;
        super.onViewCreated(view, bundle);
        View findViewById2 = view.findViewById(R.id.save);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.save)");
        this.save = findViewById2;
        if (findViewById2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("save");
            findViewById2 = null;
        }
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SelectFeaturedBadgeFragment.$r8$lambda$_bkHm03ln_peC73g5aV6JBovvNY(SelectFeaturedBadgeFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m430onViewCreated$lambda0(SelectFeaturedBadgeFragment selectFeaturedBadgeFragment, View view) {
        Intrinsics.checkNotNullParameter(selectFeaturedBadgeFragment, "this$0");
        selectFeaturedBadgeFragment.getViewModel().save();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public Material3OnScrollHelper getMaterial3OnScrollHelper(Toolbar toolbar) {
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        View view = this.scrollShadow;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrollShadow");
            view = null;
        }
        return new Material3OnScrollHelper(requireActivity, view);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        Badge.Companion.register(dSLSettingsAdapter, new Function3<Badge, Boolean, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$bindAdapter$1
            final /* synthetic */ SelectFeaturedBadgeFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function3
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2, Object obj3) {
                invoke((Badge) obj, ((Boolean) obj2).booleanValue(), ((Boolean) obj3).booleanValue());
                return Unit.INSTANCE;
            }

            public final void invoke(Badge badge, boolean z, boolean z2) {
                Intrinsics.checkNotNullParameter(badge, "badge");
                if (!z) {
                    SelectFeaturedBadgeFragment.access$getViewModel(this.this$0).setSelectedBadge(badge);
                }
            }
        });
        View findViewById = requireView().findViewById(R.id.preview);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.preview)");
        BadgePreview.ViewHolder viewHolder = new BadgePreview.ViewHolder(findViewById);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getEvents().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SelectFeaturedBadgeFragment.$r8$lambda$l2aPfpfG35tAiu8s3FiGqp3cb2o(SelectFeaturedBadgeFragment.this, (SelectFeaturedBadgeEvent) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.events.subscri…BackStack()\n      }\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(new Ref$BooleanRef(), viewHolder, dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ Ref$BooleanRef f$1;
            public final /* synthetic */ BadgePreview.ViewHolder f$2;
            public final /* synthetic */ DSLSettingsAdapter f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SelectFeaturedBadgeFragment.m427$r8$lambda$rFrNcmWg8Dic79MqpuxqKwOeHs(SelectFeaturedBadgeFragment.this, this.f$1, this.f$2, this.f$3, (SelectFeaturedBadgeState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m428bindAdapter$lambda1(SelectFeaturedBadgeFragment selectFeaturedBadgeFragment, SelectFeaturedBadgeEvent selectFeaturedBadgeEvent) {
        Intrinsics.checkNotNullParameter(selectFeaturedBadgeFragment, "this$0");
        Intrinsics.checkNotNullParameter(selectFeaturedBadgeEvent, "event");
        int i = WhenMappings.$EnumSwitchMapping$0[selectFeaturedBadgeEvent.ordinal()];
        if (i == 1) {
            Toast.makeText(selectFeaturedBadgeFragment.requireContext(), (int) R.string.SelectFeaturedBadgeFragment__you_must_select_a_badge, 1).show();
        } else if (i == 2) {
            Toast.makeText(selectFeaturedBadgeFragment.requireContext(), (int) R.string.SelectFeaturedBadgeFragment__failed_to_update_profile, 1).show();
        } else if (i == 3) {
            FragmentKt.findNavController(selectFeaturedBadgeFragment).popBackStack();
        }
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m429bindAdapter$lambda2(SelectFeaturedBadgeFragment selectFeaturedBadgeFragment, Ref$BooleanRef ref$BooleanRef, BadgePreview.ViewHolder viewHolder, DSLSettingsAdapter dSLSettingsAdapter, SelectFeaturedBadgeState selectFeaturedBadgeState) {
        Intrinsics.checkNotNullParameter(selectFeaturedBadgeFragment, "this$0");
        Intrinsics.checkNotNullParameter(ref$BooleanRef, "$hasBoundPreview");
        Intrinsics.checkNotNullParameter(viewHolder, "$previewViewHolder");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        View view = selectFeaturedBadgeFragment.save;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("save");
            view = null;
        }
        view.setEnabled(selectFeaturedBadgeState.getStage() == SelectFeaturedBadgeState.Stage.READY);
        if (ref$BooleanRef.element) {
            viewHolder.setPayload(CollectionsKt__CollectionsJVMKt.listOf(Unit.INSTANCE));
        } else {
            ref$BooleanRef.element = true;
        }
        viewHolder.bind((BadgePreview.ViewHolder) new BadgePreview.BadgeModel.FeaturedModel(selectFeaturedBadgeState.getSelectedBadge()));
        Intrinsics.checkNotNullExpressionValue(selectFeaturedBadgeState, "state");
        dSLSettingsAdapter.submitList(selectFeaturedBadgeFragment.getConfiguration(selectFeaturedBadgeState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(SelectFeaturedBadgeState selectFeaturedBadgeState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, selectFeaturedBadgeState) { // from class: org.thoughtcrime.securesms.badges.self.featured.SelectFeaturedBadgeFragment$getConfiguration$1
            final /* synthetic */ SelectFeaturedBadgeState $state;
            final /* synthetic */ SelectFeaturedBadgeFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$state = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.SelectFeaturedBadgeFragment__select_a_badge);
                Badges badges = Badges.INSTANCE;
                Context requireContext = this.this$0.requireContext();
                Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                badges.displayBadges(dSLConfiguration, requireContext, this.$state.getAllUnlockedBadges(), (r13 & 4) != 0 ? null : this.$state.getSelectedBadge(), (r13 & 8) != 0 ? null : null);
            }
        });
    }
}
