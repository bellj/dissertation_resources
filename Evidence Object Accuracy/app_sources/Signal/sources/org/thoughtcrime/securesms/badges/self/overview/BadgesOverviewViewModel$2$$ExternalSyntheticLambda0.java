package org.thoughtcrime.securesms.badges.self.overview;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.badges.self.overview.BadgesOverviewViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class BadgesOverviewViewModel$2$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ Boolean f$0;

    public /* synthetic */ BadgesOverviewViewModel$2$$ExternalSyntheticLambda0(Boolean bool) {
        this.f$0 = bool;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return BadgesOverviewViewModel.AnonymousClass2.m452invoke$lambda0(this.f$0, (BadgesOverviewState) obj);
    }
}
