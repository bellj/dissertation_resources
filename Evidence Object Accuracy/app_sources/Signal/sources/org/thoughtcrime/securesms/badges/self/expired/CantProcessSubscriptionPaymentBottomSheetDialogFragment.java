package org.thoughtcrime.securesms.badges.self.expired;

import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.models.SplashImage;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.CommunicationActions;

/* compiled from: CantProcessSubscriptionPaymentBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0002¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/expired/CantProcessSubscriptionPaymentBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class CantProcessSubscriptionPaymentBottomSheetDialogFragment extends DSLSettingsBottomSheetFragment {
    public CantProcessSubscriptionPaymentBottomSheetDialogFragment() {
        super(0, null, 0.0f, 7, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        SplashImage.INSTANCE.register(dSLSettingsAdapter);
        dSLSettingsAdapter.submitList(getConfiguration().toMappingModelList());
    }

    private final DSLConfiguration getConfiguration() {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this) { // from class: org.thoughtcrime.securesms.badges.self.expired.CantProcessSubscriptionPaymentBottomSheetDialogFragment$getConfiguration$1
            final /* synthetic */ CantProcessSubscriptionPaymentBottomSheetDialogFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new SplashImage.Model(R.drawable.ic_card_process, 0, 2, null));
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText.CenterModifier centerModifier = DSLSettingsText.CenterModifier.INSTANCE;
                dSLConfiguration.sectionHeaderPref(companion.from(R.string.CantProcessSubscriptionPaymentBottomSheetDialogFragment__cant_process_subscription_payment, centerModifier));
                String string = this.this$0.requireContext().getString(R.string.CantProcessSubscriptionPaymentBottomSheetDialogFragment__were_having_trouble);
                Intrinsics.checkNotNullExpressionValue(string, "requireContext().getStri…ent__were_having_trouble)");
                int color = ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_accent_primary);
                final CantProcessSubscriptionPaymentBottomSheetDialogFragment cantProcessSubscriptionPaymentBottomSheetDialogFragment = this.this$0;
                DSLConfiguration.textPref$default(dSLConfiguration, null, companion.from(string, new DSLSettingsText.LearnMoreModifier(color, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.self.expired.CantProcessSubscriptionPaymentBottomSheetDialogFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        CommunicationActions.openBrowserLink(cantProcessSubscriptionPaymentBottomSheetDialogFragment.requireContext(), cantProcessSubscriptionPaymentBottomSheetDialogFragment.requireContext().getString(R.string.donation_decline_code_error_url));
                    }
                }), centerModifier), 1, null);
                DSLSettingsText from = companion.from(17039370, new DSLSettingsText.Modifier[0]);
                final CantProcessSubscriptionPaymentBottomSheetDialogFragment cantProcessSubscriptionPaymentBottomSheetDialogFragment2 = this.this$0;
                DSLConfiguration.primaryButton$default(dSLConfiguration, from, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.self.expired.CantProcessSubscriptionPaymentBottomSheetDialogFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        cantProcessSubscriptionPaymentBottomSheetDialogFragment2.dismissAllowingStateLoss();
                    }
                }, 2, null);
                DSLSettingsText from2 = companion.from(R.string.CantProcessSubscriptionPaymentBottomSheetDialogFragment__dont_show_this_again, new DSLSettingsText.Modifier[0]);
                final CantProcessSubscriptionPaymentBottomSheetDialogFragment cantProcessSubscriptionPaymentBottomSheetDialogFragment3 = this.this$0;
                DSLConfiguration.secondaryButtonNoOutline$default(dSLConfiguration, from2, null, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.self.expired.CantProcessSubscriptionPaymentBottomSheetDialogFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SignalStore.donationsValues().setShowCantProcessDialog(false);
                        cantProcessSubscriptionPaymentBottomSheetDialogFragment3.dismissAllowingStateLoss();
                    }
                }, 6, null);
            }
        });
    }
}
