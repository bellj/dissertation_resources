package org.thoughtcrime.securesms.badges.self.none;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.BadgePreview;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* compiled from: BecomeASustainerFragment.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerState;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BecomeASustainerFragment extends DSLSettingsBottomSheetFragment {
    public static final Companion Companion = new Companion(null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(BecomeASustainerViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.badges.self.none.BecomeASustainerFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.self.none.BecomeASustainerFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, BecomeASustainerFragment$viewModel$2.INSTANCE);

    @JvmStatic
    public static final void show(FragmentManager fragmentManager) {
        Companion.show(fragmentManager);
    }

    public BecomeASustainerFragment() {
        super(0, null, 0.0f, 7, null);
    }

    private final BecomeASustainerViewModel getViewModel() {
        return (BecomeASustainerViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        BadgePreview.INSTANCE.register(dSLSettingsAdapter);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.badges.self.none.BecomeASustainerFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ BecomeASustainerFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BecomeASustainerFragment.m436$r8$lambda$2twgZvlMMJG2Klgr9Bs_3KMH1w(DSLSettingsAdapter.this, this.f$1, (BecomeASustainerState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m437bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, BecomeASustainerFragment becomeASustainerFragment, BecomeASustainerState becomeASustainerState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(becomeASustainerFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(becomeASustainerState, "it");
        dSLSettingsAdapter.submitList(becomeASustainerFragment.getConfiguration(becomeASustainerState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(BecomeASustainerState becomeASustainerState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(becomeASustainerState, this) { // from class: org.thoughtcrime.securesms.badges.self.none.BecomeASustainerFragment$getConfiguration$1
            final /* synthetic */ BecomeASustainerState $state;
            final /* synthetic */ BecomeASustainerFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new BadgePreview.BadgeModel.FeaturedModel(this.$state.getBadge()));
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText.CenterModifier centerModifier = DSLSettingsText.CenterModifier.INSTANCE;
                dSLConfiguration.sectionHeaderPref(companion.from(R.string.BecomeASustainerFragment__get_badges, centerModifier, DSLSettingsText.TitleLargeModifier.INSTANCE));
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                dSLConfiguration.space((int) dimensionUnit.toPixels(8.0f));
                dSLConfiguration.noPadTextPref(companion.from(R.string.BecomeASustainerFragment__signal_is_a_non_profit, centerModifier, new DSLSettingsText.TextAppearanceModifier(R.style.Signal_Text_BodyMedium), new DSLSettingsText.ColorModifier(ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_colorOnSurfaceVariant))));
                dSLConfiguration.space((int) dimensionUnit.toPixels(32.0f));
                DSLSettingsText from = companion.from(R.string.BecomeASustainerMegaphone__become_a_sustainer, new DSLSettingsText.Modifier[0]);
                final BecomeASustainerFragment becomeASustainerFragment = this.this$0;
                DSLConfiguration.tonalButton$default(dSLConfiguration, from, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.badges.self.none.BecomeASustainerFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        becomeASustainerFragment.requireActivity().finish();
                        FragmentActivity requireActivity = becomeASustainerFragment.requireActivity();
                        AppSettingsActivity.Companion companion2 = AppSettingsActivity.Companion;
                        Context requireContext = becomeASustainerFragment.requireContext();
                        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                        requireActivity.startActivity(companion2.subscriptions(requireContext).setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING));
                    }
                }, 2, null);
                dSLConfiguration.space((int) dimensionUnit.toPixels(32.0f));
            }
        });
    }

    /* compiled from: BecomeASustainerFragment.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/badges/self/none/BecomeASustainerFragment$Companion;", "", "()V", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            new BecomeASustainerFragment().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }
}
