package org.thoughtcrime.securesms.badges.gifts.viewgift.sent;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository;
import org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftViewModel;
import org.thoughtcrime.securesms.badges.models.BadgeDisplay112;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: ViewSentGiftBottomSheet.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u0014\u0010\u0003\u001a\u00020\u00048BX\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\r\u001a\u00020\u000e8BX\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftBottomSheet;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "getGiftBadge", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "sentTo", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getSentTo", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "viewModel", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftState;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewSentGiftBottomSheet extends DSLSettingsBottomSheetFragment {
    private static final String ARG_GIFT_BADGE;
    private static final String ARG_SENT_TO;
    public static final Companion Companion = new Companion(null);
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ViewSentGiftViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftBottomSheet$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftBottomSheet$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftBottomSheet$viewModel$2
        final /* synthetic */ ViewSentGiftBottomSheet this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new ViewSentGiftViewModel.Factory(ViewSentGiftBottomSheet.access$getSentTo(this.this$0), ViewSentGiftBottomSheet.access$getGiftBadge(this.this$0), new ViewGiftRepository());
        }
    });

    @JvmStatic
    public static final void show(FragmentManager fragmentManager, MmsMessageRecord mmsMessageRecord) {
        Companion.show(fragmentManager, mmsMessageRecord);
    }

    public ViewSentGiftBottomSheet() {
        super(0, null, 0.0f, 7, null);
    }

    /* compiled from: ViewSentGiftBottomSheet.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftBottomSheet$Companion;", "", "()V", "ARG_GIFT_BADGE", "", "ARG_SENT_TO", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager, MmsMessageRecord mmsMessageRecord) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
            ViewSentGiftBottomSheet viewSentGiftBottomSheet = new ViewSentGiftBottomSheet();
            Bundle bundle = new Bundle();
            bundle.putParcelable(ViewSentGiftBottomSheet.ARG_SENT_TO, mmsMessageRecord.getRecipient().getId());
            GiftBadge giftBadge = mmsMessageRecord.getGiftBadge();
            Intrinsics.checkNotNull(giftBadge);
            bundle.putByteArray(ViewSentGiftBottomSheet.ARG_GIFT_BADGE, giftBadge.toByteArray());
            viewSentGiftBottomSheet.setArguments(bundle);
            viewSentGiftBottomSheet.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }

    public final RecipientId getSentTo() {
        Parcelable parcelable = requireArguments().getParcelable(ARG_SENT_TO);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    public final GiftBadge getGiftBadge() {
        GiftBadge parseFrom = GiftBadge.parseFrom(requireArguments().getByteArray(ARG_GIFT_BADGE));
        Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(requireArgumen…yteArray(ARG_GIFT_BADGE))");
        return parseFrom;
    }

    private final ViewSentGiftViewModel getViewModel() {
        return (ViewSentGiftViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        BadgeDisplay112.INSTANCE.register(dSLSettingsAdapter);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftBottomSheet$$ExternalSyntheticLambda0
            public final /* synthetic */ ViewSentGiftBottomSheet f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewSentGiftBottomSheet.$r8$lambda$XtOQjofag8pG8HlvJQPhzBV9h18(DSLSettingsAdapter.this, this.f$1, (ViewSentGiftState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.observeO…MappingModelList())\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m412bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, ViewSentGiftBottomSheet viewSentGiftBottomSheet, ViewSentGiftState viewSentGiftState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(viewSentGiftBottomSheet, "this$0");
        Intrinsics.checkNotNullExpressionValue(viewSentGiftState, "state");
        dSLSettingsAdapter.submitList(viewSentGiftBottomSheet.getConfiguration(viewSentGiftState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(ViewSentGiftState viewSentGiftState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(viewSentGiftState, this) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftBottomSheet$getConfiguration$1
            final /* synthetic */ ViewSentGiftState $state;
            final /* synthetic */ ViewSentGiftBottomSheet this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText.CenterModifier centerModifier = DSLSettingsText.CenterModifier.INSTANCE;
                dSLConfiguration.noPadTextPref(companion.from(R.string.ViewSentGiftBottomSheet__thanks_for_your_support, centerModifier, DSLSettingsText.TitleLargeModifier.INSTANCE));
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                dSLConfiguration.space((int) dimensionUnit.toPixels(8.0f));
                if (this.$state.getRecipient() != null) {
                    String string = this.this$0.getString(R.string.ViewSentGiftBottomSheet__youve_gifted_a_badge, this.$state.getRecipient().getDisplayName(this.this$0.requireContext()));
                    Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.ViewS…ayName(requireContext()))");
                    dSLConfiguration.noPadTextPref(companion.from(string, centerModifier));
                    dSLConfiguration.space((int) dimensionUnit.toPixels(30.0f));
                }
                if (this.$state.getBadge() != null) {
                    dSLConfiguration.customPref(new BadgeDisplay112.Model(this.$state.getBadge(), false, 2, null));
                }
            }
        });
    }
}
