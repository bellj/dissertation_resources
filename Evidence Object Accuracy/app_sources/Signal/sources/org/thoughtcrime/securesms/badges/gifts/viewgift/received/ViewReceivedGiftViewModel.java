package org.thoughtcrime.securesms.badges.gifts.viewgift.received;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository;
import org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.DonationReceiptRedemptionJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: ViewReceivedGiftViewModel.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u001bH\u0014J\u0006\u0010\u001c\u001a\u00020\u0017J\u000e\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u0019R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0015X\u0004¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftViewModel;", "Landroidx/lifecycle/ViewModel;", "sentFrom", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "messageId", "", "repository", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;", "badgeRepository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JLorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;Lorg/thoughtcrime/securesms/badges/BadgeRepository;)V", "getBadgeRepository", "()Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "awaitRedemptionCompletion", "Lio/reactivex/rxjava3/core/Completable;", "setAsPrimary", "", "onCleared", "", "redeem", "setChecked", "isChecked", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewReceivedGiftViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ViewReceivedGiftViewModel.class);
    private final BadgeRepository badgeRepository;
    private final CompositeDisposable disposables;
    private final long messageId;
    private final Flowable<ViewReceivedGiftState> state;
    private final RxStore<ViewReceivedGiftState> store;

    /* compiled from: ViewReceivedGiftViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[JobTracker.JobState.values().length];
            iArr[JobTracker.JobState.SUCCESS.ordinal()] = 1;
            iArr[JobTracker.JobState.FAILURE.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public final BadgeRepository getBadgeRepository() {
        return this.badgeRepository;
    }

    public ViewReceivedGiftViewModel(RecipientId recipientId, long j, ViewGiftRepository viewGiftRepository, BadgeRepository badgeRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "sentFrom");
        Intrinsics.checkNotNullParameter(viewGiftRepository, "repository");
        Intrinsics.checkNotNullParameter(badgeRepository, "badgeRepository");
        this.messageId = j;
        this.badgeRepository = badgeRepository;
        RxStore<ViewReceivedGiftState> rxStore = new RxStore<>(new ViewReceivedGiftState(null, null, null, null, false, false, null, null, 255, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        this.state = rxStore.getStateFlowable();
        Disposable subscribe = Recipient.observable(recipientId).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewReceivedGiftViewModel.m406_init_$lambda0(ViewReceivedGiftViewModel.this, (Recipient) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "observable(sentFrom).sub…ient = recipient) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        Disposable subscribe2 = viewGiftRepository.getGiftBadge(j).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewReceivedGiftViewModel.m407_init_$lambda1(ViewReceivedGiftViewModel.this, (GiftBadge) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "repository.getGiftBadge(… giftBadge)\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe2);
        Disposable subscribe3 = viewGiftRepository.getGiftBadge(j).firstOrError().flatMap(new Function() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ViewReceivedGiftViewModel.m408_init_$lambda2(ViewGiftRepository.this, (GiftBadge) obj);
            }
        }).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewReceivedGiftViewModel.m409_init_$lambda4(ViewReceivedGiftViewModel.this, (Badge) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe3, "repository\n      .getGif…      )\n        }\n      }");
        DisposableKt.plusAssign(compositeDisposable, subscribe3);
    }

    /* compiled from: ViewReceivedGiftViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Flowable<ViewReceivedGiftState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m406_init_$lambda0(ViewReceivedGiftViewModel viewReceivedGiftViewModel, Recipient recipient) {
        Intrinsics.checkNotNullParameter(viewReceivedGiftViewModel, "this$0");
        viewReceivedGiftViewModel.store.update(new Function1<ViewReceivedGiftState, ViewReceivedGiftState>(recipient) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$1$1
            final /* synthetic */ Recipient $recipient;

            /* access modifiers changed from: package-private */
            {
                this.$recipient = r1;
            }

            public final ViewReceivedGiftState invoke(ViewReceivedGiftState viewReceivedGiftState) {
                Intrinsics.checkNotNullParameter(viewReceivedGiftState, "it");
                return viewReceivedGiftState.copy((r18 & 1) != 0 ? viewReceivedGiftState.recipient : this.$recipient, (r18 & 2) != 0 ? viewReceivedGiftState.giftBadge : null, (r18 & 4) != 0 ? viewReceivedGiftState.badge : null, (r18 & 8) != 0 ? viewReceivedGiftState.controlState : null, (r18 & 16) != 0 ? viewReceivedGiftState.hasOtherBadges : false, (r18 & 32) != 0 ? viewReceivedGiftState.displayingOtherBadges : false, (r18 & 64) != 0 ? viewReceivedGiftState.userCheckSelection : null, (r18 & 128) != 0 ? viewReceivedGiftState.redemptionState : null);
            }
        });
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m407_init_$lambda1(ViewReceivedGiftViewModel viewReceivedGiftViewModel, GiftBadge giftBadge) {
        Intrinsics.checkNotNullParameter(viewReceivedGiftViewModel, "this$0");
        viewReceivedGiftViewModel.store.update(new Function1<ViewReceivedGiftState, ViewReceivedGiftState>(giftBadge) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$2$1
            final /* synthetic */ GiftBadge $giftBadge;

            /* access modifiers changed from: package-private */
            {
                this.$giftBadge = r1;
            }

            public final ViewReceivedGiftState invoke(ViewReceivedGiftState viewReceivedGiftState) {
                Intrinsics.checkNotNullParameter(viewReceivedGiftState, "it");
                return viewReceivedGiftState.copy((r18 & 1) != 0 ? viewReceivedGiftState.recipient : null, (r18 & 2) != 0 ? viewReceivedGiftState.giftBadge : this.$giftBadge, (r18 & 4) != 0 ? viewReceivedGiftState.badge : null, (r18 & 8) != 0 ? viewReceivedGiftState.controlState : null, (r18 & 16) != 0 ? viewReceivedGiftState.hasOtherBadges : false, (r18 & 32) != 0 ? viewReceivedGiftState.displayingOtherBadges : false, (r18 & 64) != 0 ? viewReceivedGiftState.userCheckSelection : null, (r18 & 128) != 0 ? viewReceivedGiftState.redemptionState : null);
            }
        });
    }

    /* renamed from: _init_$lambda-2 */
    public static final SingleSource m408_init_$lambda2(ViewGiftRepository viewGiftRepository, GiftBadge giftBadge) {
        Intrinsics.checkNotNullParameter(viewGiftRepository, "$repository");
        Intrinsics.checkNotNullExpressionValue(giftBadge, "it");
        return viewGiftRepository.getBadge(giftBadge);
    }

    /* renamed from: _init_$lambda-4 */
    public static final void m409_init_$lambda4(ViewReceivedGiftViewModel viewReceivedGiftViewModel, Badge badge) {
        Intrinsics.checkNotNullParameter(viewReceivedGiftViewModel, "this$0");
        List<Badge> badges = Recipient.self().getBadges();
        Intrinsics.checkNotNullExpressionValue(badges, "self().badges");
        ArrayList arrayList = new ArrayList();
        for (Object obj : badges) {
            if (!Intrinsics.areEqual(((Badge) obj).getId(), badge.getId())) {
                arrayList.add(obj);
            }
        }
        boolean isEmpty = arrayList.isEmpty();
        boolean z = true;
        boolean z2 = !isEmpty;
        boolean displayBadgesOnProfile = SignalStore.donationsValues().getDisplayBadgesOnProfile();
        if (!z2 || !displayBadgesOnProfile) {
            z = false;
        }
        viewReceivedGiftViewModel.store.update(new Function1<ViewReceivedGiftState, ViewReceivedGiftState>(displayBadgesOnProfile, badge, z2, z) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$4$1
            final /* synthetic */ Badge $badge;
            final /* synthetic */ boolean $displayingBadges;
            final /* synthetic */ boolean $displayingOtherBadges;
            final /* synthetic */ boolean $hasOtherBadges;

            /* access modifiers changed from: package-private */
            {
                this.$displayingBadges = r1;
                this.$badge = r2;
                this.$hasOtherBadges = r3;
                this.$displayingOtherBadges = r4;
            }

            public final ViewReceivedGiftState invoke(ViewReceivedGiftState viewReceivedGiftState) {
                Intrinsics.checkNotNullParameter(viewReceivedGiftState, "it");
                return viewReceivedGiftState.copy((r18 & 1) != 0 ? viewReceivedGiftState.recipient : null, (r18 & 2) != 0 ? viewReceivedGiftState.giftBadge : null, (r18 & 4) != 0 ? viewReceivedGiftState.badge : this.$badge, (r18 & 8) != 0 ? viewReceivedGiftState.controlState : this.$displayingBadges ? ViewReceivedGiftState.ControlState.FEATURE : ViewReceivedGiftState.ControlState.DISPLAY, (r18 & 16) != 0 ? viewReceivedGiftState.hasOtherBadges : this.$hasOtherBadges, (r18 & 32) != 0 ? viewReceivedGiftState.displayingOtherBadges : this.$displayingOtherBadges, (r18 & 64) != 0 ? viewReceivedGiftState.userCheckSelection : null, (r18 & 128) != 0 ? viewReceivedGiftState.redemptionState : null);
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.dispose();
    }

    public final void setChecked(boolean z) {
        this.store.update(new Function1<ViewReceivedGiftState, ViewReceivedGiftState>(z) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$setChecked$1
            final /* synthetic */ boolean $isChecked;

            /* access modifiers changed from: package-private */
            {
                this.$isChecked = r1;
            }

            public final ViewReceivedGiftState invoke(ViewReceivedGiftState viewReceivedGiftState) {
                Intrinsics.checkNotNullParameter(viewReceivedGiftState, "state");
                return viewReceivedGiftState.copy((r18 & 1) != 0 ? viewReceivedGiftState.recipient : null, (r18 & 2) != 0 ? viewReceivedGiftState.giftBadge : null, (r18 & 4) != 0 ? viewReceivedGiftState.badge : null, (r18 & 8) != 0 ? viewReceivedGiftState.controlState : null, (r18 & 16) != 0 ? viewReceivedGiftState.hasOtherBadges : false, (r18 & 32) != 0 ? viewReceivedGiftState.displayingOtherBadges : false, (r18 & 64) != 0 ? viewReceivedGiftState.userCheckSelection : Boolean.valueOf(this.$isChecked), (r18 & 128) != 0 ? viewReceivedGiftState.redemptionState : null);
            }
        });
    }

    public final Completable redeem() {
        Completable completable;
        ViewReceivedGiftState state = this.store.getState();
        if (state.getControlState() == null || state.getBadge() == null) {
            Completable error = Completable.error(new Exception("Cannot enqueue a redemption without a control state or badge."));
            Intrinsics.checkNotNullExpressionValue(error, "{\n      Completable.erro… state or badge.\"))\n    }");
            return error;
        }
        if (state.getControlState() == ViewReceivedGiftState.ControlState.DISPLAY) {
            completable = BadgeRepository.setVisibilityForAllBadges$default(this.badgeRepository, state.getControlChecked(), null, 2, null).andThen(awaitRedemptionCompletion(false));
        } else if (state.getControlChecked()) {
            completable = awaitRedemptionCompletion(true);
        } else {
            completable = awaitRedemptionCompletion(false);
        }
        Intrinsics.checkNotNullExpressionValue(completable, "{\n      if (snapshot.con…tion(false)\n      }\n    }");
        return completable;
    }

    private final Completable awaitRedemptionCompletion(boolean z) {
        Completable create = Completable.create(new CompletableOnSubscribe(z) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.core.CompletableOnSubscribe
            public final void subscribe(CompletableEmitter completableEmitter) {
                ViewReceivedGiftViewModel.m410awaitRedemptionCompletion$lambda6(ViewReceivedGiftViewModel.this, this.f$1, completableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create {\n      Log.i(TAG…EDEMPTION))\n      }\n    }");
        return create;
    }

    /* renamed from: awaitRedemptionCompletion$lambda-6 */
    public static final void m410awaitRedemptionCompletion$lambda6(ViewReceivedGiftViewModel viewReceivedGiftViewModel, boolean z, CompletableEmitter completableEmitter) {
        Intrinsics.checkNotNullParameter(viewReceivedGiftViewModel, "this$0");
        String str = TAG;
        Log.i(str, "Enqueuing gift redemption and awaiting result...", true);
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        DonationReceiptRedemptionJob.createJobChainForGift(viewReceivedGiftViewModel.messageId, z).enqueue(new JobTracker.JobListener(countDownLatch) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ CountDownLatch f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobListener
            public final void onStateChanged(Job job, JobTracker.JobState jobState) {
                ViewReceivedGiftViewModel.m411awaitRedemptionCompletion$lambda6$lambda5(Ref$ObjectRef.this, this.f$1, job, jobState);
            }
        });
        try {
            if (countDownLatch.await(10, TimeUnit.SECONDS)) {
                JobTracker.JobState jobState = (JobTracker.JobState) ref$ObjectRef.element;
                int i = jobState == null ? -1 : WhenMappings.$EnumSwitchMapping$0[jobState.ordinal()];
                if (i == 1) {
                    Log.d(str, "Gift redemption job chain succeeded.", true);
                    completableEmitter.onComplete();
                } else if (i != 2) {
                    Log.w(str, "Gift redemption job chain ignored due to in-progress jobs.", true);
                    completableEmitter.onError(DonationError.Companion.timeoutWaitingForToken(DonationErrorSource.GIFT_REDEMPTION));
                } else {
                    Log.d(str, "Gift redemption job chain failed permanently.", true);
                    completableEmitter.onError(DonationError.Companion.genericBadgeRedemptionFailure(DonationErrorSource.GIFT_REDEMPTION));
                }
            } else {
                Log.w(str, "Timeout awaiting for gift token redemption and profile refresh", true);
                completableEmitter.onError(DonationError.Companion.timeoutWaitingForToken(DonationErrorSource.GIFT_REDEMPTION));
            }
        } catch (InterruptedException unused) {
            Log.w(TAG, "Interrupted awaiting for gift token redemption and profile refresh", true);
            completableEmitter.onError(DonationError.Companion.timeoutWaitingForToken(DonationErrorSource.GIFT_REDEMPTION));
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: org.thoughtcrime.securesms.jobmanager.JobTracker$JobState */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: awaitRedemptionCompletion$lambda-6$lambda-5 */
    public static final void m411awaitRedemptionCompletion$lambda6$lambda5(Ref$ObjectRef ref$ObjectRef, CountDownLatch countDownLatch, Job job, JobTracker.JobState jobState) {
        Intrinsics.checkNotNullParameter(ref$ObjectRef, "$finalJobState");
        Intrinsics.checkNotNullParameter(countDownLatch, "$countDownLatch");
        Intrinsics.checkNotNullParameter(job, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(jobState, "state");
        if (jobState.isComplete()) {
            ref$ObjectRef.element = jobState;
            countDownLatch.countDown();
        }
    }

    /* compiled from: ViewReceivedGiftViewModel.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ%\u0010\u000b\u001a\u0002H\f\"\b\b\u0000\u0010\f*\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\f0\u000fH\u0016¢\u0006\u0002\u0010\u0010R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "sentFrom", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "messageId", "", "repository", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;", "badgeRepository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JLorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;Lorg/thoughtcrime/securesms/badges/BadgeRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final BadgeRepository badgeRepository;
        private final long messageId;
        private final ViewGiftRepository repository;
        private final RecipientId sentFrom;

        public Factory(RecipientId recipientId, long j, ViewGiftRepository viewGiftRepository, BadgeRepository badgeRepository) {
            Intrinsics.checkNotNullParameter(recipientId, "sentFrom");
            Intrinsics.checkNotNullParameter(viewGiftRepository, "repository");
            Intrinsics.checkNotNullParameter(badgeRepository, "badgeRepository");
            this.sentFrom = recipientId;
            this.messageId = j;
            this.repository = viewGiftRepository;
            this.badgeRepository = badgeRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ViewReceivedGiftViewModel(this.sentFrom, this.messageId, this.repository, this.badgeRepository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftViewModel.Factory.create");
        }
    }
}
