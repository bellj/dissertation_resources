package org.thoughtcrime.securesms.badges.gifts.flow;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchState;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: GiftFlowRecipientSelectionFragment.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\fH\u0016J\b\u0010\u0018\u001a\u00020\fH\u0016J\u001a\u0010\u0019\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\f2\u0006\u0010\u001f\u001a\u00020\u001dH\u0016R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowRecipientSelectionFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/SearchConfigurationProvider;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/badges/gifts/flow/GiftFlowViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "exitFlow", "", "getContainer", "Landroid/view/ViewGroup;", "getDialogBackgroundColor", "", "getSearchConfiguration", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "contactSearchState", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "onFinishForwardAction", "onSearchInputFocused", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setResult", "bundle", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GiftFlowRecipientSelectionFragment extends Fragment implements MultiselectForwardFragment.Callback, SearchConfigurationProvider {
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(GiftFlowViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRecipientSelectionFragment$viewModel$2
        final /* synthetic */ GiftFlowRecipientSelectionFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRecipientSelectionFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void exitFlow() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public int getDialogBackgroundColor() {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onFinishForwardAction() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onSearchInputFocused() {
    }

    public GiftFlowRecipientSelectionFragment() {
        super(R.layout.gift_flow_recipient_selection_fragment);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return MultiselectForwardFragment.Callback.DefaultImpls.getStorySendRequirements(this);
    }

    private final GiftFlowViewModel getViewModel() {
        return (GiftFlowViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        ((Toolbar) view.findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRecipientSelectionFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GiftFlowRecipientSelectionFragment.$r8$lambda$YX4LWHgYqu9jDTsKylvUUA0svOg(GiftFlowRecipientSelectionFragment.this, view2);
            }
        });
        if (bundle == null) {
            getChildFragmentManager().beginTransaction().replace(R.id.multiselect_container, MultiselectForwardFragment.Companion.create(new MultiselectForwardFragmentArgs(false, CollectionsKt__CollectionsKt.emptyList(), 0, true, false, true, 0, null, 212, null))).commit();
        }
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m379onViewCreated$lambda0(GiftFlowRecipientSelectionFragment giftFlowRecipientSelectionFragment, View view) {
        Intrinsics.checkNotNullParameter(giftFlowRecipientSelectionFragment, "this$0");
        giftFlowRecipientSelectionFragment.requireActivity().onBackPressed();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider
    public ContactSearchConfiguration getSearchConfiguration(FragmentManager fragmentManager, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
        Intrinsics.checkNotNullParameter(contactSearchState, "contactSearchState");
        return ContactSearchConfiguration.Companion.build(new Function1<ContactSearchConfiguration.Builder, Unit>(contactSearchState) { // from class: org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRecipientSelectionFragment$getSearchConfiguration$1
            final /* synthetic */ ContactSearchState $contactSearchState;

            /* access modifiers changed from: package-private */
            {
                this.$contactSearchState = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ContactSearchConfiguration.Builder builder) {
                invoke(builder);
                return Unit.INSTANCE;
            }

            public final void invoke(ContactSearchConfiguration.Builder builder) {
                Intrinsics.checkNotNullParameter(builder, "$this$build");
                builder.setQuery(this.$contactSearchState.getQuery());
                String query = builder.getQuery();
                if (query == null || query.length() == 0) {
                    builder.addSection(new ContactSearchConfiguration.Section.Recents(0, ContactSearchConfiguration.Section.Recents.Mode.INDIVIDUALS, false, false, false, false, true, null, 157, null));
                }
                builder.addSection(new ContactSearchConfiguration.Section.Individuals(false, ContactSearchConfiguration.TransportType.PUSH, true, null, 8, null));
            }
        });
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void setResult(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        ArrayList<ContactSearchKey.ParcelableRecipientSearchKey> parcelableArrayList = bundle.getParcelableArrayList(MultiselectForwardFragment.RESULT_SELECTION);
        Intrinsics.checkNotNull(parcelableArrayList);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(parcelableArrayList, 10));
        for (ContactSearchKey.ParcelableRecipientSearchKey parcelableRecipientSearchKey : parcelableArrayList) {
            arrayList.add(parcelableRecipientSearchKey.asRecipientSearchKey());
        }
        if (!arrayList.isEmpty()) {
            getViewModel().setSelectedContact((ContactSearchKey.RecipientSearchKey) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) arrayList)));
            SafeNavigation.safeNavigate(FragmentKt.findNavController(this), (int) R.id.action_giftFlowRecipientSelectionFragment_to_giftFlowConfirmationFragment);
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public ViewGroup getContainer() {
        return (ViewGroup) requireView();
    }
}
