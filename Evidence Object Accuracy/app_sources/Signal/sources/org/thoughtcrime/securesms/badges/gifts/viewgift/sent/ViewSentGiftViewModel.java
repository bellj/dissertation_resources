package org.thoughtcrime.securesms.badges.gifts.viewgift.sent;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.gifts.viewgift.ViewGiftRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: ViewSentGiftViewModel.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0014R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftViewModel;", "Landroidx/lifecycle/ViewModel;", "sentFrom", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "repository", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "onCleared", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewSentGiftViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final Flowable<ViewSentGiftState> state;
    private final RxStore<ViewSentGiftState> store;

    public ViewSentGiftViewModel(RecipientId recipientId, GiftBadge giftBadge, ViewGiftRepository viewGiftRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "sentFrom");
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        Intrinsics.checkNotNullParameter(viewGiftRepository, "repository");
        RxStore<ViewSentGiftState> rxStore = new RxStore<>(new ViewSentGiftState(null, null, 3, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        this.state = rxStore.getStateFlowable();
        Disposable subscribe = Recipient.observable(recipientId).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewSentGiftViewModel.m413_init_$lambda0(ViewSentGiftViewModel.this, (Recipient) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "observable(sentFrom).sub…ient = recipient) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        Disposable subscribe2 = viewGiftRepository.getBadge(giftBadge).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ViewSentGiftViewModel.m414_init_$lambda1(ViewSentGiftViewModel.this, (Badge) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "repository.getBadge(gift…e\n        )\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe2);
    }

    public final Flowable<ViewSentGiftState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m413_init_$lambda0(ViewSentGiftViewModel viewSentGiftViewModel, Recipient recipient) {
        Intrinsics.checkNotNullParameter(viewSentGiftViewModel, "this$0");
        viewSentGiftViewModel.store.update(new Function1<ViewSentGiftState, ViewSentGiftState>(recipient) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftViewModel$1$1
            final /* synthetic */ Recipient $recipient;

            /* access modifiers changed from: package-private */
            {
                this.$recipient = r1;
            }

            public final ViewSentGiftState invoke(ViewSentGiftState viewSentGiftState) {
                Intrinsics.checkNotNullParameter(viewSentGiftState, "it");
                return ViewSentGiftState.copy$default(viewSentGiftState, this.$recipient, null, 2, null);
            }
        });
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m414_init_$lambda1(ViewSentGiftViewModel viewSentGiftViewModel, Badge badge) {
        Intrinsics.checkNotNullParameter(viewSentGiftViewModel, "this$0");
        viewSentGiftViewModel.store.update(new Function1<ViewSentGiftState, ViewSentGiftState>(badge) { // from class: org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftViewModel$2$1
            final /* synthetic */ Badge $badge;

            /* access modifiers changed from: package-private */
            {
                this.$badge = r1;
            }

            public final ViewSentGiftState invoke(ViewSentGiftState viewSentGiftState) {
                Intrinsics.checkNotNullParameter(viewSentGiftState, "it");
                return ViewSentGiftState.copy$default(viewSentGiftState, null, this.$badge, 1, null);
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.dispose();
    }

    /* compiled from: ViewSentGiftViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/sent/ViewSentGiftViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "sentFrom", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "repository", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;Lorg/thoughtcrime/securesms/badges/gifts/viewgift/ViewGiftRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final GiftBadge giftBadge;
        private final ViewGiftRepository repository;
        private final RecipientId sentFrom;

        public Factory(RecipientId recipientId, GiftBadge giftBadge, ViewGiftRepository viewGiftRepository) {
            Intrinsics.checkNotNullParameter(recipientId, "sentFrom");
            Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
            Intrinsics.checkNotNullParameter(viewGiftRepository, "repository");
            this.sentFrom = recipientId;
            this.giftBadge = giftBadge;
            this.repository = viewGiftRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ViewSentGiftViewModel(this.sentFrom, this.giftBadge, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftViewModel.Factory.create");
        }
    }
}
