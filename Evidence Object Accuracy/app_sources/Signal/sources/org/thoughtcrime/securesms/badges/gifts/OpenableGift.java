package org.thoughtcrime.securesms.badges.gifts;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.util.Projection;

/* compiled from: OpenableGift.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001:\u0001\u000fJ\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0007H&J\u0012\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000bH&J\u001c\u0010\f\u001a\u00020\u00032\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00030\u000eH&¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift;", "", "clearOpenGiftCallback", "", "getAnimationSign", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift$AnimationSign;", "getGiftId", "", "getOpenableGiftProjection", "Lorg/thoughtcrime/securesms/util/Projection;", "isAnimating", "", "setOpenGiftCallback", "openGift", "Lkotlin/Function1;", "AnimationSign", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public interface OpenableGift {
    void clearOpenGiftCallback();

    AnimationSign getAnimationSign();

    long getGiftId();

    Projection getOpenableGiftProjection(boolean z);

    void setOpenGiftCallback(Function1<? super OpenableGift, Unit> function1);

    /* compiled from: OpenableGift.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0007\b\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift$AnimationSign;", "", "sign", "", "(Ljava/lang/String;IF)V", "getSign", "()F", "POSITIVE", "NEGATIVE", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum AnimationSign {
        POSITIVE(1.0f),
        NEGATIVE(-1.0f);
        
        public static final Companion Companion = new Companion(null);
        private final float sign;

        @JvmStatic
        public static final AnimationSign get(boolean z, boolean z2) {
            return Companion.get(z, z2);
        }

        AnimationSign(float f) {
            this.sign = f;
        }

        public final float getSign() {
            return this.sign;
        }

        /* compiled from: OpenableGift.kt */
        @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift$AnimationSign$Companion;", "", "()V", "get", "Lorg/thoughtcrime/securesms/badges/gifts/OpenableGift$AnimationSign;", "isLtr", "", "isOutgoing", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            @JvmStatic
            public final AnimationSign get(boolean z, boolean z2) {
                if (z && z2) {
                    return AnimationSign.NEGATIVE;
                }
                if (z) {
                    return AnimationSign.POSITIVE;
                }
                if (z2) {
                    return AnimationSign.POSITIVE;
                }
                return AnimationSign.NEGATIVE;
            }
        }
    }
}
