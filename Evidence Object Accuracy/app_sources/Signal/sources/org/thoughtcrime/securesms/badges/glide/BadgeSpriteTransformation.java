package org.thoughtcrime.securesms.badges.glide;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Map;
import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: BadgeSpriteTransformation.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001a2\u00020\u0001:\u0005\u001a\u001b\u001c\u001d\u001eB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0013\u0010\n\u001a\u00020\u00072\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J(\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u000eH\u0014J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation;", "Lcom/bumptech/glide/load/resource/bitmap/BitmapTransformation;", MediaPreviewActivity.SIZE_EXTRA, "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Size;", "density", "", "isDarkTheme", "", "(Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Size;Ljava/lang/String;Z)V", ContactRepository.ID_COLUMN, "equals", "other", "", "hashCode", "", "transform", "Landroid/graphics/Bitmap;", "pool", "Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;", "toTransform", "outWidth", "outHeight", "updateDiskCacheKey", "", "messageDigest", "Ljava/security/MessageDigest;", "Companion", "Density", "Frame", "FrameSet", "Size", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BadgeSpriteTransformation extends BitmapTransformation {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(BadgeSpriteTransformation.class);
    private static final int VERSION;
    private final String density;
    private final String id;
    private final boolean isDarkTheme;
    private final Size size;

    public BadgeSpriteTransformation(Size size, String str, boolean z) {
        Intrinsics.checkNotNullParameter(size, MediaPreviewActivity.SIZE_EXTRA);
        Intrinsics.checkNotNullParameter(str, "density");
        this.size = size;
        this.density = str;
        this.isDarkTheme = z;
        this.id = "BadgeSpriteTransformation(" + size.getCode() + ',' + str + ',' + z + ").3";
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        Intrinsics.checkNotNullParameter(messageDigest, "messageDigest");
        String str = this.id;
        Charset charset = Key.CHARSET;
        Intrinsics.checkNotNullExpressionValue(charset, "CHARSET");
        byte[] bytes = str.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        messageDigest.update(bytes);
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        String str = null;
        BadgeSpriteTransformation badgeSpriteTransformation = obj instanceof BadgeSpriteTransformation ? (BadgeSpriteTransformation) obj : null;
        if (badgeSpriteTransformation != null) {
            str = badgeSpriteTransformation.id;
        }
        return Intrinsics.areEqual(str, this.id);
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override // com.bumptech.glide.load.resource.bitmap.BitmapTransformation
    protected Bitmap transform(BitmapPool bitmapPool, Bitmap bitmap, int i, int i2) {
        Intrinsics.checkNotNullParameter(bitmapPool, "pool");
        Intrinsics.checkNotNullParameter(bitmap, "toTransform");
        Bitmap bitmap2 = bitmapPool.get(i, i2, Bitmap.Config.ARGB_8888);
        Intrinsics.checkNotNullExpressionValue(bitmap2, "pool.get(outWidth, outHe… Bitmap.Config.ARGB_8888)");
        Rect inBounds = Companion.getInBounds(this.density, this.size, this.isDarkTheme);
        Canvas canvas = new Canvas(bitmap2);
        if (inBounds.width() == i && inBounds.height() == i2) {
            String str = TAG;
            Log.d(str, "Bitmap size match, performing direct draw. (" + i + " x " + i2 + ')');
            canvas.drawBitmap(bitmap, inBounds, new Rect(0, 0, i, i2), (Paint) null);
        } else {
            String str2 = TAG;
            Log.d(str2, "Bitmap size mismatch, performing filtered scale. (Wanted " + i + " x " + i2 + " but got " + inBounds.width() + " x " + inBounds.height() + ')');
            Bitmap bitmap3 = bitmapPool.get(inBounds.width(), inBounds.height(), Bitmap.Config.ARGB_8888);
            Intrinsics.checkNotNullExpressionValue(bitmap3, "pool.get(inBounds.width(… Bitmap.Config.ARGB_8888)");
            new Canvas(bitmap3).drawBitmap(bitmap, inBounds, new Rect(0, 0, inBounds.width(), inBounds.height()), (Paint) null);
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap3, i, i2, true);
            bitmapPool.put(bitmap3);
            canvas.drawBitmap(createScaledBitmap, 0.0f, 0.0f, (Paint) null);
            createScaledBitmap.recycle();
        }
        return bitmap2;
    }

    /* compiled from: BadgeSpriteTransformation.kt */
    /* JADX WARN: Init of enum SMALL can be incorrect */
    /* JADX WARN: Init of enum MEDIUM can be incorrect */
    /* JADX WARN: Init of enum LARGE can be incorrect */
    /* JADX WARN: Init of enum BADGE_64 can be incorrect */
    /* JADX WARN: Init of enum BADGE_112 can be incorrect */
    /* JADX WARN: Init of enum XLARGE can be incorrect */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\b\u0001\u0018\u0000 \u00132\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0013B#\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\u0002\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Size;", "", "code", "", "frameMap", "", "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Density;", "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$FrameSet;", "(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)V", "getCode", "()Ljava/lang/String;", "getFrameMap", "()Ljava/util/Map;", "SMALL", "MEDIUM", "LARGE", "BADGE_64", "BADGE_112", "XLARGE", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Size {
        SMALL("small", MapsKt__MapsKt.mapOf(TuplesKt.to(r3, new FrameSet(new Frame(124, 1, 12, 12), new Frame(145, 31, 12, 12))), TuplesKt.to(r4, new FrameSet(new Frame(163, 1, 16, 16), new Frame(189, 39, 16, 16))), TuplesKt.to(r8, new FrameSet(new Frame(244, 1, 24, 24), new Frame(283, 58, 24, 24))), TuplesKt.to(r9, new FrameSet(new Frame(323, 1, 32, 32), new Frame(373, 75, 32, 32))), TuplesKt.to(r6, new FrameSet(new Frame(483, 1, 48, 48), new Frame(557, 111, 48, 48))), TuplesKt.to(r10, new FrameSet(new Frame(643, 1, 64, 64), new Frame(741, 147, 64, 64))))),
        MEDIUM("medium", MapsKt__MapsKt.mapOf(TuplesKt.to(r3, new FrameSet(new Frame(124, 16, 18, 18), new Frame(160, 31, 18, 18))), TuplesKt.to(r4, new FrameSet(new Frame(163, 19, 24, 24), new Frame(207, 39, 24, 24))), TuplesKt.to(r8, new FrameSet(new Frame(244, 28, 36, 36), new Frame(310, 58, 36, 36))), TuplesKt.to(r9, new FrameSet(new Frame(323, 35, 48, 48), new Frame(407, 75, 48, 48))), TuplesKt.to(r6, new FrameSet(new Frame(483, 51, 72, 72), new Frame(607, 111, 72, 72))), TuplesKt.to(r10, new FrameSet(new Frame(643, 67, 96, 96), new Frame(807, 147, 96, 96))))),
        LARGE("large", MapsKt__MapsKt.mapOf(TuplesKt.to(r3, new FrameSet(new Frame(145, 1, 27, 27), new Frame(124, 46, 27, 27))), TuplesKt.to(r4, new FrameSet(new Frame(189, 1, 36, 36), new Frame(163, 57, 36, 36))), TuplesKt.to(r8, new FrameSet(new Frame(283, 1, 54, 54), new Frame(244, 85, 54, 54))), TuplesKt.to(r9, new FrameSet(new Frame(373, 1, 72, 72), new Frame(323, 109, 72, 72))), TuplesKt.to(r6, new FrameSet(new Frame(557, 1, 108, 108), new Frame(483, 161, 108, 108))), TuplesKt.to(r10, new FrameSet(new Frame(741, 1, 144, 144), new Frame(643, 213, 144, 144))))),
        BADGE_64("badge_64", MapsKt__MapsKt.mapOf(TuplesKt.to(r3, new FrameSet(new Frame(124, 73, 48, 48), new Frame(124, 73, 48, 48))), TuplesKt.to(r4, new FrameSet(new Frame(163, 97, 64, 64), new Frame(163, 97, 64, 64))), TuplesKt.to(r8, new FrameSet(new Frame(244, 145, 96, 96), new Frame(244, 145, 96, 96))), TuplesKt.to(r9, new FrameSet(new Frame(323, 193, 128, 128), new Frame(323, 193, 128, 128))), TuplesKt.to(r6, new FrameSet(new Frame(483, 289, 192, 192), new Frame(483, 289, 192, 192))), TuplesKt.to(r10, new FrameSet(new Frame(643, 385, 256, 256), new Frame(643, 385, 256, 256))))),
        BADGE_112("badge_112", MapsKt__MapsKt.mapOf(TuplesKt.to(r3, new FrameSet(new Frame(181, 1, 84, 84), new Frame(181, 1, 84, 84))), TuplesKt.to(r4, new FrameSet(new Frame(233, 1, R.styleable.AppCompatTheme_tooltipForegroundColor, R.styleable.AppCompatTheme_tooltipForegroundColor), new Frame(233, 1, R.styleable.AppCompatTheme_tooltipForegroundColor, R.styleable.AppCompatTheme_tooltipForegroundColor))), TuplesKt.to(r8, new FrameSet(new Frame(349, 1, 168, 168), new Frame(349, 1, 168, 168))), TuplesKt.to(r9, new FrameSet(new Frame(457, 1, 224, 224), new Frame(457, 1, 224, 224))), TuplesKt.to(r6, new FrameSet(new Frame(681, 1, 336, 336), new Frame(681, 1, 336, 336))), TuplesKt.to(r10, new FrameSet(new Frame(905, 1, 448, 448), new Frame(905, 1, 448, 448))))),
        XLARGE("xlarge", MapsKt__MapsKt.mapOf(TuplesKt.to(r3, new FrameSet(new Frame(1, 1, 120, 120), new Frame(1, 1, 120, 120))), TuplesKt.to(r4, new FrameSet(new Frame(1, 1, 160, 160), new Frame(1, 1, 160, 160))), TuplesKt.to(r8, new FrameSet(new Frame(1, 1, 240, 240), new Frame(1, 1, 240, 240))), TuplesKt.to(r9, new FrameSet(new Frame(1, 1, 320, 320), new Frame(1, 1, 320, 320))), TuplesKt.to(r6, new FrameSet(new Frame(1, 1, 480, 480), new Frame(1, 1, 480, 480))), TuplesKt.to(r10, new FrameSet(new Frame(1, 1, 640, 640), new Frame(1, 1, 640, 640)))));
        
        public static final Companion Companion = new Companion(null);
        private final String code;
        private final Map<Density, FrameSet> frameMap;

        Size(String str, Map map) {
            this.code = str;
            this.frameMap = map;
        }

        public final String getCode() {
            return this.code;
        }

        public final Map<Density, FrameSet> getFrameMap() {
            return this.frameMap;
        }

        static {
            Density density = Density.LDPI;
            Density density2 = Density.MDPI;
            Density density3 = Density.HDPI;
            Density density4 = Density.XHDPI;
            Density density5 = Density.XXHDPI;
            Density density6 = Density.XXXHDPI;
            Companion = new Companion(null);
        }

        /* compiled from: BadgeSpriteTransformation.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Size$Companion;", "", "()V", "fromInteger", "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Size;", "integer", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes3.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final Size fromInteger(int i) {
                if (i == 0) {
                    return Size.SMALL;
                }
                if (i == 1) {
                    return Size.MEDIUM;
                }
                if (i == 2) {
                    return Size.LARGE;
                }
                if (i == 3) {
                    return Size.XLARGE;
                }
                if (i == 4) {
                    return Size.BADGE_64;
                }
                if (i != 5) {
                    return Size.LARGE;
                }
                return Size.BADGE_112;
            }
        }
    }

    /* compiled from: BadgeSpriteTransformation.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Density;", "", "density", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getDensity", "()Ljava/lang/String;", "LDPI", "MDPI", "HDPI", "XHDPI", "XXHDPI", "XXXHDPI", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Density {
        LDPI("ldpi"),
        MDPI("mdpi"),
        HDPI("hdpi"),
        XHDPI("xhdpi"),
        XXHDPI("xxhdpi"),
        XXXHDPI("xxxhdpi");
        
        private final String density;

        Density(String str) {
            this.density = str;
        }

        public final String getDensity() {
            return this.density;
        }
    }

    /* compiled from: BadgeSpriteTransformation.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$FrameSet;", "", "light", "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Frame;", "dark", "(Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Frame;Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Frame;)V", "getDark", "()Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Frame;", "getLight", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class FrameSet {
        private final Frame dark;
        private final Frame light;

        public static /* synthetic */ FrameSet copy$default(FrameSet frameSet, Frame frame, Frame frame2, int i, Object obj) {
            if ((i & 1) != 0) {
                frame = frameSet.light;
            }
            if ((i & 2) != 0) {
                frame2 = frameSet.dark;
            }
            return frameSet.copy(frame, frame2);
        }

        public final Frame component1() {
            return this.light;
        }

        public final Frame component2() {
            return this.dark;
        }

        public final FrameSet copy(Frame frame, Frame frame2) {
            Intrinsics.checkNotNullParameter(frame, "light");
            Intrinsics.checkNotNullParameter(frame2, "dark");
            return new FrameSet(frame, frame2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FrameSet)) {
                return false;
            }
            FrameSet frameSet = (FrameSet) obj;
            return Intrinsics.areEqual(this.light, frameSet.light) && Intrinsics.areEqual(this.dark, frameSet.dark);
        }

        public int hashCode() {
            return (this.light.hashCode() * 31) + this.dark.hashCode();
        }

        public String toString() {
            return "FrameSet(light=" + this.light + ", dark=" + this.dark + ')';
        }

        public FrameSet(Frame frame, Frame frame2) {
            Intrinsics.checkNotNullParameter(frame, "light");
            Intrinsics.checkNotNullParameter(frame2, "dark");
            this.light = frame;
            this.dark = frame2;
        }

        public final Frame getDark() {
            return this.dark;
        }

        public final Frame getLight() {
            return this.light;
        }
    }

    /* compiled from: BadgeSpriteTransformation.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J1\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÖ\u0001J\u0006\u0010\u0016\u001a\u00020\u0017J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Frame;", "", "x", "", "y", "width", "height", "(IIII)V", "getHeight", "()I", "getWidth", "getX", "getY", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "toBounds", "Landroid/graphics/Rect;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Frame {
        private final int height;
        private final int width;
        private final int x;
        private final int y;

        public static /* synthetic */ Frame copy$default(Frame frame, int i, int i2, int i3, int i4, int i5, Object obj) {
            if ((i5 & 1) != 0) {
                i = frame.x;
            }
            if ((i5 & 2) != 0) {
                i2 = frame.y;
            }
            if ((i5 & 4) != 0) {
                i3 = frame.width;
            }
            if ((i5 & 8) != 0) {
                i4 = frame.height;
            }
            return frame.copy(i, i2, i3, i4);
        }

        public final int component1() {
            return this.x;
        }

        public final int component2() {
            return this.y;
        }

        public final int component3() {
            return this.width;
        }

        public final int component4() {
            return this.height;
        }

        public final Frame copy(int i, int i2, int i3, int i4) {
            return new Frame(i, i2, i3, i4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Frame)) {
                return false;
            }
            Frame frame = (Frame) obj;
            return this.x == frame.x && this.y == frame.y && this.width == frame.width && this.height == frame.height;
        }

        public int hashCode() {
            return (((((this.x * 31) + this.y) * 31) + this.width) * 31) + this.height;
        }

        public String toString() {
            return "Frame(x=" + this.x + ", y=" + this.y + ", width=" + this.width + ", height=" + this.height + ')';
        }

        public Frame(int i, int i2, int i3, int i4) {
            this.x = i;
            this.y = i2;
            this.width = i3;
            this.height = i4;
        }

        public final int getX() {
            return this.x;
        }

        public final int getY() {
            return this.y;
        }

        public final int getWidth() {
            return this.width;
        }

        public final int getHeight() {
            return this.height;
        }

        public final Rect toBounds() {
            int i = this.x;
            int i2 = this.y;
            return new Rect(i, i2, this.width + i, this.height + i2);
        }
    }

    /* compiled from: BadgeSpriteTransformation.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004H\u0002J \u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J \u0010\u0011\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "VERSION", "", "getDensity", "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Density;", "density", "getFrame", "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Frame;", MediaPreviewActivity.SIZE_EXTRA, "Lorg/thoughtcrime/securesms/badges/glide/BadgeSpriteTransformation$Size;", "isDarkTheme", "", "getInBounds", "Landroid/graphics/Rect;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        private final Density getDensity(String str) {
            Density[] values = Density.values();
            for (Density density : values) {
                if (Intrinsics.areEqual(density.getDensity(), str)) {
                    return density;
                }
            }
            throw new NoSuchElementException("Array contains no element matching the predicate.");
        }

        private final Frame getFrame(Size size, Density density, boolean z) {
            FrameSet frameSet = size.getFrameMap().get(density);
            Intrinsics.checkNotNull(frameSet);
            FrameSet frameSet2 = frameSet;
            return z ? frameSet2.getDark() : frameSet2.getLight();
        }

        public final Rect getInBounds(String str, Size size, boolean z) {
            return getFrame(size, getDensity(str), z).toBounds();
        }
    }
}
