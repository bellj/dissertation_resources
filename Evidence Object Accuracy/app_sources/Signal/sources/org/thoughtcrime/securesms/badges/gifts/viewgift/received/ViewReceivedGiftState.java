package org.thoughtcrime.securesms.badges.gifts.viewgift.received;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ViewReceivedGiftState.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u000223B_\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J\u000b\u0010!\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\tHÆ\u0003J\t\u0010%\u001a\u00020\u000bHÆ\u0003J\t\u0010&\u001a\u00020\u000bHÆ\u0003J\u0010\u0010'\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0002\u0010\u001fJ\t\u0010(\u001a\u00020\u000fHÆ\u0003Jh\u0010)\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u000b2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\u000fHÆ\u0001¢\u0006\u0002\u0010*J\u0013\u0010+\u001a\u00020\u000b2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010-\u001a\u00020\u000bJ\t\u0010.\u001a\u00020/HÖ\u0001J\t\u00100\u001a\u000201HÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\f\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0016R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0015\u0010\r\u001a\u0004\u0018\u00010\u000b¢\u0006\n\n\u0002\u0010 \u001a\u0004\b\u001e\u0010\u001f¨\u00064"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState;", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "controlState", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$ControlState;", "hasOtherBadges", "", "displayingOtherBadges", "userCheckSelection", "redemptionState", "Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$RedemptionState;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$ControlState;ZZLjava/lang/Boolean;Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$RedemptionState;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getControlState", "()Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$ControlState;", "getDisplayingOtherBadges", "()Z", "getGiftBadge", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "getHasOtherBadges", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getRedemptionState", "()Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$RedemptionState;", "getUserCheckSelection", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$ControlState;ZZLjava/lang/Boolean;Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$RedemptionState;)Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState;", "equals", "other", "getControlChecked", "hashCode", "", "toString", "", "ControlState", "RedemptionState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ViewReceivedGiftState {
    private final Badge badge;
    private final ControlState controlState;
    private final boolean displayingOtherBadges;
    private final GiftBadge giftBadge;
    private final boolean hasOtherBadges;
    private final Recipient recipient;
    private final RedemptionState redemptionState;
    private final Boolean userCheckSelection;

    /* compiled from: ViewReceivedGiftState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$ControlState;", "", "(Ljava/lang/String;I)V", "DISPLAY", "FEATURE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum ControlState {
        DISPLAY,
        FEATURE
    }

    /* compiled from: ViewReceivedGiftState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/badges/gifts/viewgift/received/ViewReceivedGiftState$RedemptionState;", "", "(Ljava/lang/String;I)V", "NONE", "IN_PROGRESS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum RedemptionState {
        NONE,
        IN_PROGRESS
    }

    public ViewReceivedGiftState() {
        this(null, null, null, null, false, false, null, null, 255, null);
    }

    public final Recipient component1() {
        return this.recipient;
    }

    public final GiftBadge component2() {
        return this.giftBadge;
    }

    public final Badge component3() {
        return this.badge;
    }

    public final ControlState component4() {
        return this.controlState;
    }

    public final boolean component5() {
        return this.hasOtherBadges;
    }

    public final boolean component6() {
        return this.displayingOtherBadges;
    }

    public final Boolean component7() {
        return this.userCheckSelection;
    }

    public final RedemptionState component8() {
        return this.redemptionState;
    }

    public final ViewReceivedGiftState copy(Recipient recipient, GiftBadge giftBadge, Badge badge, ControlState controlState, boolean z, boolean z2, Boolean bool, RedemptionState redemptionState) {
        Intrinsics.checkNotNullParameter(redemptionState, "redemptionState");
        return new ViewReceivedGiftState(recipient, giftBadge, badge, controlState, z, z2, bool, redemptionState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ViewReceivedGiftState)) {
            return false;
        }
        ViewReceivedGiftState viewReceivedGiftState = (ViewReceivedGiftState) obj;
        return Intrinsics.areEqual(this.recipient, viewReceivedGiftState.recipient) && Intrinsics.areEqual(this.giftBadge, viewReceivedGiftState.giftBadge) && Intrinsics.areEqual(this.badge, viewReceivedGiftState.badge) && this.controlState == viewReceivedGiftState.controlState && this.hasOtherBadges == viewReceivedGiftState.hasOtherBadges && this.displayingOtherBadges == viewReceivedGiftState.displayingOtherBadges && Intrinsics.areEqual(this.userCheckSelection, viewReceivedGiftState.userCheckSelection) && this.redemptionState == viewReceivedGiftState.redemptionState;
    }

    public int hashCode() {
        Recipient recipient = this.recipient;
        int i = 0;
        int hashCode = (recipient == null ? 0 : recipient.hashCode()) * 31;
        GiftBadge giftBadge = this.giftBadge;
        int hashCode2 = (hashCode + (giftBadge == null ? 0 : giftBadge.hashCode())) * 31;
        Badge badge = this.badge;
        int hashCode3 = (hashCode2 + (badge == null ? 0 : badge.hashCode())) * 31;
        ControlState controlState = this.controlState;
        int hashCode4 = (hashCode3 + (controlState == null ? 0 : controlState.hashCode())) * 31;
        boolean z = this.hasOtherBadges;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (hashCode4 + i3) * 31;
        boolean z2 = this.displayingOtherBadges;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        int i7 = (i6 + i2) * 31;
        Boolean bool = this.userCheckSelection;
        if (bool != null) {
            i = bool.hashCode();
        }
        return ((i7 + i) * 31) + this.redemptionState.hashCode();
    }

    public String toString() {
        return "ViewReceivedGiftState(recipient=" + this.recipient + ", giftBadge=" + this.giftBadge + ", badge=" + this.badge + ", controlState=" + this.controlState + ", hasOtherBadges=" + this.hasOtherBadges + ", displayingOtherBadges=" + this.displayingOtherBadges + ", userCheckSelection=" + this.userCheckSelection + ", redemptionState=" + this.redemptionState + ')';
    }

    public ViewReceivedGiftState(Recipient recipient, GiftBadge giftBadge, Badge badge, ControlState controlState, boolean z, boolean z2, Boolean bool, RedemptionState redemptionState) {
        Intrinsics.checkNotNullParameter(redemptionState, "redemptionState");
        this.recipient = recipient;
        this.giftBadge = giftBadge;
        this.badge = badge;
        this.controlState = controlState;
        this.hasOtherBadges = z;
        this.displayingOtherBadges = z2;
        this.userCheckSelection = bool;
        this.redemptionState = redemptionState;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final GiftBadge getGiftBadge() {
        return this.giftBadge;
    }

    public final Badge getBadge() {
        return this.badge;
    }

    public final ControlState getControlState() {
        return this.controlState;
    }

    public final boolean getHasOtherBadges() {
        return this.hasOtherBadges;
    }

    public final boolean getDisplayingOtherBadges() {
        return this.displayingOtherBadges;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ViewReceivedGiftState(org.thoughtcrime.securesms.recipients.Recipient r9, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge r10, org.thoughtcrime.securesms.badges.models.Badge r11, org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState.ControlState r12, boolean r13, boolean r14, java.lang.Boolean r15, org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState.RedemptionState r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r8 = this;
            r0 = r17
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = r2
            goto L_0x000a
        L_0x0009:
            r1 = r9
        L_0x000a:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0010
            r3 = r2
            goto L_0x0011
        L_0x0010:
            r3 = r10
        L_0x0011:
            r4 = r0 & 4
            if (r4 == 0) goto L_0x0017
            r4 = r2
            goto L_0x0018
        L_0x0017:
            r4 = r11
        L_0x0018:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x001d
            goto L_0x001e
        L_0x001d:
            r2 = r12
        L_0x001e:
            r5 = r0 & 16
            r6 = 0
            if (r5 == 0) goto L_0x0025
            r5 = 0
            goto L_0x0026
        L_0x0025:
            r5 = r13
        L_0x0026:
            r7 = r0 & 32
            if (r7 == 0) goto L_0x002b
            goto L_0x002c
        L_0x002b:
            r6 = r14
        L_0x002c:
            r7 = r0 & 64
            if (r7 == 0) goto L_0x0033
            java.lang.Boolean r7 = java.lang.Boolean.FALSE
            goto L_0x0034
        L_0x0033:
            r7 = r15
        L_0x0034:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x003b
            org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState$RedemptionState r0 = org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState.RedemptionState.NONE
            goto L_0x003d
        L_0x003b:
            r0 = r16
        L_0x003d:
            r9 = r8
            r10 = r1
            r11 = r3
            r12 = r4
            r13 = r2
            r14 = r5
            r15 = r6
            r16 = r7
            r17 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState.<init>(org.thoughtcrime.securesms.recipients.Recipient, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge, org.thoughtcrime.securesms.badges.models.Badge, org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState$ControlState, boolean, boolean, java.lang.Boolean, org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftState$RedemptionState, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Boolean getUserCheckSelection() {
        return this.userCheckSelection;
    }

    public final RedemptionState getRedemptionState() {
        return this.redemptionState;
    }

    public final boolean getControlChecked() {
        Boolean bool = this.userCheckSelection;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (this.controlState != ControlState.FEATURE && this.displayingOtherBadges) {
            return true;
        }
        return false;
    }
}
