package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.BlockedListMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceBlockedUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(MultiDeviceBlockedUpdateJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public MultiDeviceBlockedUpdateJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private MultiDeviceBlockedUpdateJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else {
            RecipientDatabase recipients = SignalDatabase.recipients();
            RecipientDatabase.RecipientReader readerForBlocked = recipients.readerForBlocked(recipients.getBlocked());
            try {
                LinkedList linkedList = new LinkedList();
                LinkedList linkedList2 = new LinkedList();
                while (true) {
                    Recipient next = readerForBlocked.getNext();
                    if (next == null) {
                        ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forBlocked(new BlockedListMessage(linkedList, linkedList2)), UnidentifiedAccessUtil.getAccessForSync(this.context));
                        readerForBlocked.close();
                        return;
                    } else if (next.isPushGroup()) {
                        linkedList2.add(next.requireGroupId().getDecodedId());
                    } else if (next.isMaybeRegistered() && (next.hasServiceId() || next.hasE164())) {
                        linkedList.add(RecipientUtil.toSignalServiceAddress(this.context, next));
                    }
                }
            } catch (Throwable th) {
                if (readerForBlocked != null) {
                    try {
                        readerForBlocked.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof ServerRejectedException) && (exc instanceof PushNetworkException)) {
            return true;
        }
        return false;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceBlockedUpdateJob> {
        public MultiDeviceBlockedUpdateJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceBlockedUpdateJob(parameters);
        }
    }
}
