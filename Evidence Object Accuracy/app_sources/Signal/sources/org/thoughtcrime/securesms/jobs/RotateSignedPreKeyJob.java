package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.thoughtcrime.securesms.crypto.PreKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceIdType;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class RotateSignedPreKeyJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(RotateSignedPreKeyJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public RotateSignedPreKeyJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxInstancesForFactory(1).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(2)).build());
    }

    private RotateSignedPreKeyJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null || SignalStore.account().getPni() == null) {
            Log.w(TAG, "Not registered. Skipping.");
            return;
        }
        String str = TAG;
        Log.i(str, "Rotating signed prekey...");
        ACI aci = SignalStore.account().getAci();
        PNI pni = SignalStore.account().getPni();
        if (aci == null) {
            Log.w(str, "ACI is unset!");
        } else if (pni == null) {
            Log.w(str, "PNI is unset!");
        } else {
            rotate(ServiceIdType.ACI, ApplicationDependencies.getProtocolStore().aci(), SignalStore.account().aciPreKeys());
            rotate(ServiceIdType.PNI, ApplicationDependencies.getProtocolStore().pni(), SignalStore.account().pniPreKeys());
        }
    }

    private void rotate(ServiceIdType serviceIdType, SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore) throws IOException {
        SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
        SignedPreKeyRecord generateAndStoreSignedPreKey = PreKeyUtil.generateAndStoreSignedPreKey(signalProtocolStore, preKeyMetadataStore, false);
        signalServiceAccountManager.setSignedPreKey(serviceIdType, generateAndStoreSignedPreKey);
        preKeyMetadataStore.setActiveSignedPreKeyId(generateAndStoreSignedPreKey.getId());
        preKeyMetadataStore.setSignedPreKeyRegistered(true);
        preKeyMetadataStore.setSignedPreKeyFailureCount(0);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        PreKeyMetadataStore aciPreKeys = SignalStore.account().aciPreKeys();
        PreKeyMetadataStore pniPreKeys = SignalStore.account().pniPreKeys();
        aciPreKeys.setSignedPreKeyFailureCount(aciPreKeys.getSignedPreKeyFailureCount() + 1);
        pniPreKeys.setSignedPreKeyFailureCount(pniPreKeys.getSignedPreKeyFailureCount() + 1);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RotateSignedPreKeyJob> {
        public RotateSignedPreKeyJob create(Job.Parameters parameters, Data data) {
            return new RotateSignedPreKeyJob(parameters);
        }
    }
}
