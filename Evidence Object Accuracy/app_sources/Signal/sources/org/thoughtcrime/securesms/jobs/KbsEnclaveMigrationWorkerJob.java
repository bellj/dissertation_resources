package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.BackoffUtil;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.pin.PinState;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* loaded from: classes4.dex */
public class KbsEnclaveMigrationWorkerJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(KbsEnclaveMigrationWorkerJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public KbsEnclaveMigrationWorkerJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(-1).setMaxAttempts(-1).setQueue(KEY).setMaxInstancesForFactory(1).build());
    }

    private KbsEnclaveMigrationWorkerJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UnauthenticatedResponseException {
        String pin = SignalStore.kbsValues().getPin();
        if (SignalStore.kbsValues().hasOptedOut()) {
            Log.w(TAG, "Opted out of KBS! Nothing to migrate.");
        } else if (pin == null) {
            Log.w(TAG, "No PIN available! Can't migrate!");
        } else {
            PinState.onMigrateToNewEnclave(pin);
            Log.i(TAG, "Migration successful!");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return (exc instanceof IOException) || (exc instanceof UnauthenticatedResponseException);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public long getNextRunAttemptBackoff(int i, Exception exc) {
        if (!(exc instanceof NonSuccessfulResponseCodeException) || !((NonSuccessfulResponseCodeException) exc).is5xx()) {
            return super.getNextRunAttemptBackoff(i, exc);
        }
        return BackoffUtil.exponentialBackoff(i, FeatureFlags.getServerErrorMaxBackoff());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        throw new AssertionError("This job should never fail. " + getClass().getSimpleName());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<KbsEnclaveMigrationWorkerJob> {
        public KbsEnclaveMigrationWorkerJob create(Job.Parameters parameters, Data data) {
            return new KbsEnclaveMigrationWorkerJob(parameters);
        }
    }
}
