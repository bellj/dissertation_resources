package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.contactshare.Contact;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PushSendJob$$ExternalSyntheticLambda4 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((Contact) obj).getAvatar();
    }
}
