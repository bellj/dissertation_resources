package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ListUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.ReadMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceReadUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_IDS;
    private static final String TAG = Log.tag(MultiDeviceReadUpdateJob.class);
    private List<SerializableSyncMessageId> messageIds;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private MultiDeviceReadUpdateJob(List<MessageDatabase.SyncMessageId> list) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), SendReadReceiptJob.ensureSize(list, 500));
    }

    private MultiDeviceReadUpdateJob(Job.Parameters parameters, List<MessageDatabase.SyncMessageId> list) {
        super(parameters);
        this.messageIds = new LinkedList();
        for (MessageDatabase.SyncMessageId syncMessageId : list) {
            this.messageIds.add(new SerializableSyncMessageId(syncMessageId.getRecipientId().serialize(), syncMessageId.getTimetamp()));
        }
    }

    public static void enqueue(List<MessageDatabase.SyncMessageId> list) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        List<List> chunk = ListUtil.chunk(list, 500);
        if (chunk.size() > 1) {
            String str = TAG;
            Log.w(str, "Large receipt count! Had to break into multiple chunks. Total count: " + list.size());
        }
        for (List list2 : chunk) {
            jobManager.add(new MultiDeviceReadUpdateJob(list2));
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        int size = this.messageIds.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            try {
                strArr[i] = JsonUtils.toJson(this.messageIds.get(i));
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
        return new Data.Builder().putStringArray("message_ids", strArr).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device...");
        } else {
            LinkedList linkedList = new LinkedList();
            for (SerializableSyncMessageId serializableSyncMessageId : this.messageIds) {
                Recipient resolved = Recipient.resolved(RecipientId.from(serializableSyncMessageId.recipientId));
                if (!resolved.isGroup() && !resolved.isDistributionList() && resolved.isMaybeRegistered()) {
                    linkedList.add(new ReadMessage(RecipientUtil.getOrFetchServiceId(this.context, resolved), serializableSyncMessageId.timestamp));
                }
            }
            ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forRead(linkedList), UnidentifiedAccessUtil.getAccessForSync(this.context));
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes.dex */
    public static class SerializableSyncMessageId implements Serializable {
        private static final long serialVersionUID;
        @JsonProperty
        private final String recipientId;
        @JsonProperty
        private final long timestamp;

        private SerializableSyncMessageId(@JsonProperty("recipientId") String str, @JsonProperty("timestamp") long j) {
            this.recipientId = str;
            this.timestamp = j;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceReadUpdateJob> {
        public MultiDeviceReadUpdateJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceReadUpdateJob(parameters, Stream.of(data.getStringArray("message_ids")).map(new MultiDeviceReadUpdateJob$Factory$$ExternalSyntheticLambda0()).map(new MultiDeviceReadUpdateJob$Factory$$ExternalSyntheticLambda1()).toList());
        }

        public static /* synthetic */ SerializableSyncMessageId lambda$create$0(String str) {
            try {
                return (SerializableSyncMessageId) JsonUtils.fromJson(str, SerializableSyncMessageId.class);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        public static /* synthetic */ MessageDatabase.SyncMessageId lambda$create$1(SerializableSyncMessageId serializableSyncMessageId) {
            return new MessageDatabase.SyncMessageId(RecipientId.from(serializableSyncMessageId.recipientId), serializableSyncMessageId.timestamp);
        }
    }
}
