package org.thoughtcrime.securesms.jobs;

import android.app.Application;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ListUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceReceiptMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class SendViewedReceiptJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ADDRESS;
    private static final String KEY_MESSAGE_IDS;
    private static final String KEY_MESSAGE_SENT_TIMESTAMPS;
    private static final String KEY_RECIPIENT;
    private static final String KEY_THREAD;
    private static final String KEY_TIMESTAMP;
    private static final String TAG = Log.tag(SendViewedReceiptJob.class);
    private final List<MessageId> messageIds;
    private final List<Long> messageSentTimestamps;
    private final RecipientId recipientId;
    private final long threadId;
    private final long timestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public SendViewedReceiptJob(long j, RecipientId recipientId, long j2, MessageId messageId) {
        this(j, recipientId, Collections.singletonList(Long.valueOf(j2)), Collections.singletonList(messageId));
    }

    private SendViewedReceiptJob(long j, RecipientId recipientId, List<Long> list, List<MessageId> list2) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), j, recipientId, SendReadReceiptJob.ensureSize(list, 500), SendReadReceiptJob.ensureSize(list2, 500), System.currentTimeMillis());
    }

    private SendViewedReceiptJob(Job.Parameters parameters, long j, RecipientId recipientId, List<Long> list, List<MessageId> list2, long j2) {
        super(parameters);
        this.threadId = j;
        this.recipientId = recipientId;
        this.messageSentTimestamps = list;
        this.messageIds = list2;
        this.timestamp = j2;
    }

    public static void enqueue(long j, RecipientId recipientId, List<MessageDatabase.MarkedMessageInfo> list) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        List<List> chunk = ListUtil.chunk(list, 500);
        if (chunk.size() > 1) {
            String str = TAG;
            Log.w(str, "Large receipt count! Had to break into multiple chunks. Total count: " + list.size());
        }
        for (List list2 : chunk) {
            jobManager.add(new SendViewedReceiptJob(j, recipientId, (List) Collection$EL.stream(list2).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.SendViewedReceiptJob$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return SendViewedReceiptJob.lambda$enqueue$0((MessageDatabase.MarkedMessageInfo) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toList()), (List) Collection$EL.stream(list2).map(new SendReadReceiptJob$$ExternalSyntheticLambda2()).collect(Collectors.toList())));
        }
    }

    public static /* synthetic */ Long lambda$enqueue$0(MessageDatabase.MarkedMessageInfo markedMessageInfo) {
        return Long.valueOf(markedMessageInfo.getSyncMessageId().getTimetamp());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient", this.recipientId.serialize()).putLongListAsArray("message_ids", this.messageSentTimestamps).putStringListAsArray(KEY_MESSAGE_IDS, (List) Collection$EL.stream(this.messageIds).map(new SendReadReceiptJob$$ExternalSyntheticLambda0()).collect(Collectors.toList())).putLong("timestamp", this.timestamp).putLong("thread", this.threadId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isReadReceiptsEnabled(this.context)) {
            Log.w(TAG, "Read receipts not enabled!");
        } else if (this.messageSentTimestamps.isEmpty()) {
            Log.w(TAG, "No sync timestamps!");
        } else if (!RecipientUtil.isMessageRequestAccepted(this.context, this.threadId)) {
            Log.w(TAG, "Refusing to send receipts to untrusted recipient");
        } else {
            Recipient resolved = Recipient.resolved(this.recipientId);
            if (resolved.isSelf()) {
                Log.i(TAG, "Not sending view receipt to self.");
            } else if (resolved.isBlocked()) {
                Log.w(TAG, "Refusing to send receipts to blocked recipient");
            } else if (resolved.isGroup()) {
                Log.w(TAG, "Refusing to send receipts to group");
            } else if (resolved.isUnregistered()) {
                String str = TAG;
                Log.w(str, resolved.getId() + " not registered!");
            } else {
                SendMessageResult sendReceipt = ApplicationDependencies.getSignalServiceMessageSender().sendReceipt(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, Recipient.resolved(this.recipientId)), new SignalServiceReceiptMessage(SignalServiceReceiptMessage.Type.VIEWED, this.messageSentTimestamps, this.timestamp));
                if (Util.hasItems(this.messageIds)) {
                    SignalDatabase.messageLog().insertIfPossible(this.recipientId, this.timestamp, sendReceipt, ContentHint.IMPLICIT, this.messageIds);
                }
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof ServerRejectedException) && (exc instanceof PushNetworkException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Failed to send read receipts to: " + this.recipientId);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<SendViewedReceiptJob> {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        public SendViewedReceiptJob create(Job.Parameters parameters, Data data) {
            RecipientId recipientId;
            long j = data.getLong("timestamp");
            List<Long> longArrayAsList = data.getLongArrayAsList("message_ids");
            List list = (List) Collection$EL.stream(data.hasStringArray(SendViewedReceiptJob.KEY_MESSAGE_IDS) ? data.getStringArrayAsList(SendViewedReceiptJob.KEY_MESSAGE_IDS) : Collections.emptyList()).map(new SendReadReceiptJob$Factory$$ExternalSyntheticLambda0()).collect(Collectors.toList());
            long j2 = data.getLong("thread");
            if (data.hasString("recipient")) {
                recipientId = RecipientId.from(data.getString("recipient"));
            } else {
                recipientId = Recipient.external(this.application, data.getString("address")).getId();
            }
            return new SendViewedReceiptJob(parameters, j2, recipientId, longArrayAsList, list, j);
        }
    }
}
