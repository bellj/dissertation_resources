package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.KbsEnclave;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.pin.KbsEnclaves;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* loaded from: classes4.dex */
public class ClearFallbackKbsEnclaveJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ENCLAVE_NAME;
    private static final String KEY_MR_ENCLAVE;
    private static final String KEY_SERVICE_ID;
    private static final String TAG = Log.tag(ClearFallbackKbsEnclaveJob.class);
    private final KbsEnclave enclave;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    ClearFallbackKbsEnclaveJob(KbsEnclave kbsEnclave) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(90)).setMaxAttempts(-1).setQueue(KEY).build(), kbsEnclave);
    }

    public static void clearAll() {
        if (KbsEnclaves.fallbacks().isEmpty()) {
            Log.i(TAG, "No fallbacks!");
            return;
        }
        JobManager jobManager = ApplicationDependencies.getJobManager();
        for (KbsEnclave kbsEnclave : KbsEnclaves.fallbacks()) {
            jobManager.add(new ClearFallbackKbsEnclaveJob(kbsEnclave));
        }
    }

    private ClearFallbackKbsEnclaveJob(Job.Parameters parameters, KbsEnclave kbsEnclave) {
        super(parameters);
        this.enclave = kbsEnclave;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_ENCLAVE_NAME, this.enclave.getEnclaveName()).putString(KEY_SERVICE_ID, this.enclave.getServiceId()).putString(KEY_MR_ENCLAVE, this.enclave.getMrEnclave()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UnauthenticatedResponseException {
        String str = TAG;
        Log.i(str, "Preparing to delete data from " + this.enclave.getEnclaveName());
        ApplicationDependencies.getKeyBackupService(this.enclave).newPinChangeSession().removePin();
        Log.i(str, "Successfully deleted the data from " + this.enclave.getEnclaveName());
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof NonSuccessfulResponseCodeException)) {
            return true;
        }
        int code = ((NonSuccessfulResponseCodeException) exc).getCode();
        if (code != 404) {
            if (code != 508) {
                return true;
            }
            return false;
        } else if (getRunAttempt() < 3) {
            return true;
        } else {
            return false;
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public long getNextRunAttemptBackoff(int i, Exception exc) {
        if (!(exc instanceof NonSuccessfulResponseCodeException) || ((NonSuccessfulResponseCodeException) exc).getCode() != 404) {
            return super.getNextRunAttemptBackoff(i, exc);
        }
        return TimeUnit.DAYS.toMillis(1);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Job failed! It is likely that the old enclave is offline.");
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ClearFallbackKbsEnclaveJob> {
        public ClearFallbackKbsEnclaveJob create(Job.Parameters parameters, Data data) {
            return new ClearFallbackKbsEnclaveJob(parameters, new KbsEnclave(data.getString(ClearFallbackKbsEnclaveJob.KEY_ENCLAVE_NAME), data.getString(ClearFallbackKbsEnclaveJob.KEY_SERVICE_ID), data.getString(ClearFallbackKbsEnclaveJob.KEY_MR_ENCLAVE)));
        }
    }
}
