package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import com.android.mms.dom.smil.parser.SmilXmlSerializer;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduComposer;
import com.google.android.mms.pdu_alt.PduPart;
import com.google.android.mms.pdu_alt.SendConf;
import com.google.android.mms.pdu_alt.SendReq;
import com.google.android.mms.smil.SmilHelper;
import com.klinker.android.send_message.Utils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import org.signal.core.util.Hex;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobLogger;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.CompatMmsConnection;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.MmsSendResult;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.phonenumbers.NumberUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.transport.InsecureFallbackApprovalException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class MmsSendJob extends SendJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String TAG = Log.tag(MmsSendJob.class);
    private final long messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    private MmsSendJob(long j) {
        this(new Job.Parameters.Builder().setQueue("mms-operation").addConstraint(NetworkConstraint.KEY).setMaxAttempts(15).build(), j);
    }

    public static void enqueue(Context context, JobManager jobManager, long j) {
        try {
            OutgoingMediaMessage outgoingMessage = SignalDatabase.mms().getOutgoingMessage(j);
            List<? extends Job> list = Stream.of(outgoingMessage.getAttachments()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.MmsSendJob$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MmsSendJob.lambda$enqueue$0(OutgoingMediaMessage.this, (Attachment) obj);
                }
            }).toList();
            jobManager.startChain(list).then(new MmsSendJob(j)).enqueue();
        } catch (NoSuchMessageException | MmsException e) {
            throw new AssertionError(e);
        }
    }

    public static /* synthetic */ Job lambda$enqueue$0(OutgoingMediaMessage outgoingMediaMessage, Attachment attachment) {
        return AttachmentCompressionJob.fromAttachment((DatabaseAttachment) attachment, true, outgoingMediaMessage.getSubscriptionId());
    }

    private MmsSendJob(Job.Parameters parameters, long j) {
        super(parameters);
        this.messageId = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        SignalDatabase.mms().markAsSending(this.messageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.SendJob
    public void onSend() throws MmsException, NoSuchMessageException, IOException {
        Throwable e;
        MmsDatabase mms = SignalDatabase.mms();
        OutgoingMediaMessage outgoingMessage = mms.getOutgoingMessage(this.messageId);
        if (mms.isSent(this.messageId)) {
            String str = TAG;
            Log.w(str, "Message " + this.messageId + " was already sent. Ignoring.");
            return;
        }
        try {
            String str2 = TAG;
            Log.i(str2, "Sending message: " + this.messageId);
            SendReq constructSendPdu = constructSendPdu(outgoingMessage);
            validateDestinations(outgoingMessage, constructSendPdu);
            getSendResult(new CompatMmsConnection(this.context).send(getPduBytes(constructSendPdu), outgoingMessage.getSubscriptionId()), constructSendPdu);
            mms.markAsSent(this.messageId, false);
            SendJob.markAttachmentsUploaded(this.messageId, outgoingMessage);
            Log.i(str2, "Sent message: " + this.messageId);
        } catch (IOException e2) {
            e = e2;
            Log.w(TAG, e);
            mms.markAsSentFailed(this.messageId);
            notifyMediaMessageDeliveryFailed(this.context, this.messageId);
        } catch (InsecureFallbackApprovalException e3) {
            Log.w(TAG, e3);
            mms.markAsPendingInsecureSmsFallback(this.messageId);
            notifyMediaMessageDeliveryFailed(this.context, this.messageId);
        } catch (UndeliverableMessageException e4) {
            e = e4;
            Log.w(TAG, e);
            mms.markAsSentFailed(this.messageId);
            notifyMediaMessageDeliveryFailed(this.context, this.messageId);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.i(str, JobLogger.format(this, "onFailure() messageId: " + this.messageId));
        SignalDatabase.mms().markAsSentFailed(this.messageId);
        notifyMediaMessageDeliveryFailed(this.context, this.messageId);
    }

    private byte[] getPduBytes(SendReq sendReq) throws IOException, UndeliverableMessageException, InsecureFallbackApprovalException {
        byte[] make = new PduComposer(this.context, sendReq).make();
        if (make != null) {
            return make;
        }
        throw new UndeliverableMessageException("PDU composition failed, null payload");
    }

    private MmsSendResult getSendResult(SendConf sendConf, SendReq sendReq) throws UndeliverableMessageException {
        if (sendConf == null) {
            throw new UndeliverableMessageException("No M-Send.conf received in response to send.");
        } else if (sendConf.getResponseStatus() != 128) {
            throw new UndeliverableMessageException("Got bad response: " + sendConf.getResponseStatus());
        } else if (!isInconsistentResponse(sendReq, sendConf)) {
            return new MmsSendResult(sendConf.getMessageId(), sendConf.getResponseStatus());
        } else {
            throw new UndeliverableMessageException("Mismatched response!");
        }
    }

    private boolean isInconsistentResponse(SendReq sendReq, SendConf sendConf) {
        String str = TAG;
        Log.i(str, "Comparing: " + Hex.toString(sendReq.getTransactionId()));
        Log.i(str, "With:      " + Hex.toString(sendConf.getTransactionId()));
        return !Arrays.equals(sendReq.getTransactionId(), sendConf.getTransactionId());
    }

    private void validateDestinations(EncodedStringValue[] encodedStringValueArr) throws UndeliverableMessageException {
        String str;
        if (encodedStringValueArr != null) {
            for (EncodedStringValue encodedStringValue : encodedStringValueArr) {
                if (encodedStringValue == null || !NumberUtil.isValidSmsOrEmail(encodedStringValue.getString())) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Invalid destination: ");
                    if (encodedStringValue == null) {
                        str = null;
                    } else {
                        str = encodedStringValue.getString();
                    }
                    sb.append(str);
                    throw new UndeliverableMessageException(sb.toString());
                }
            }
        }
    }

    private void validateDestinations(OutgoingMediaMessage outgoingMediaMessage, SendReq sendReq) throws UndeliverableMessageException {
        validateDestinations(sendReq.getTo());
        validateDestinations(sendReq.getCc());
        validateDestinations(sendReq.getBcc());
        if (sendReq.getTo() == null && sendReq.getCc() == null && sendReq.getBcc() == null) {
            throw new UndeliverableMessageException("No to, cc, or bcc specified!");
        } else if (outgoingMediaMessage.isSecure()) {
            throw new UndeliverableMessageException("Attempt to send encrypted MMS?");
        }
    }

    private SendReq constructSendPdu(OutgoingMediaMessage outgoingMediaMessage) throws UndeliverableMessageException {
        int i;
        SendReq sendReq = new SendReq();
        String myNumber = getMyNumber(this.context);
        MediaConstraints.getMmsMediaConstraints(outgoingMediaMessage.getSubscriptionId());
        List<Attachment> attachments = outgoingMediaMessage.getAttachments();
        if (!TextUtils.isEmpty(myNumber)) {
            sendReq.setFrom(new EncodedStringValue(myNumber));
        } else {
            sendReq.setFrom(new EncodedStringValue(SignalStore.account().getE164()));
        }
        if (outgoingMediaMessage.getRecipient().isMmsGroup()) {
            for (Recipient recipient : SignalDatabase.groups().getGroupMembers(outgoingMediaMessage.getRecipient().requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)) {
                if (!recipient.hasSmsAddress()) {
                    throw new UndeliverableMessageException("One of the group recipients did not have an SMS address! " + recipient.getId());
                } else if (outgoingMediaMessage.getDistributionType() == 1) {
                    sendReq.addBcc(new EncodedStringValue(recipient.requireSmsAddress()));
                } else {
                    sendReq.addTo(new EncodedStringValue(recipient.requireSmsAddress()));
                }
            }
        } else if (outgoingMediaMessage.getRecipient().hasSmsAddress()) {
            sendReq.addTo(new EncodedStringValue(outgoingMediaMessage.getRecipient().requireSmsAddress()));
        } else {
            throw new UndeliverableMessageException("Recipient did not have an SMS address! " + outgoingMediaMessage.getRecipient().getId());
        }
        sendReq.setDate(System.currentTimeMillis() / 1000);
        PduBody pduBody = new PduBody();
        if (!TextUtils.isEmpty(outgoingMediaMessage.getBody())) {
            PduPart pduPart = new PduPart();
            String valueOf = String.valueOf(System.currentTimeMillis());
            pduPart.setData(Util.toUtf8Bytes(outgoingMediaMessage.getBody()));
            pduPart.setCharset(106);
            pduPart.setContentType("text/plain".getBytes());
            pduPart.setContentId(valueOf.getBytes());
            pduPart.setContentLocation((valueOf + ".txt").getBytes());
            pduPart.setName((valueOf + ".txt").getBytes());
            pduBody.addPart(pduPart);
            i = (int) (((long) 0) + getPartSize(pduPart));
        } else {
            i = 0;
        }
        for (Attachment attachment : attachments) {
            try {
            } catch (IOException e) {
                Log.w(TAG, e);
            }
            if (attachment.getUri() == null) {
                throw new IOException("Assertion failed, attachment for outgoing MMS has no data!");
                break;
            }
            String fileName = attachment.getFileName();
            PduPart pduPart2 = new PduPart();
            if (fileName == null) {
                fileName = String.valueOf(Math.abs(new SecureRandom().nextLong()));
                String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(attachment.getContentType());
                if (extensionFromMimeType != null) {
                    fileName = fileName + "." + extensionFromMimeType;
                }
            }
            if (attachment.getContentType().startsWith(DraftDatabase.Draft.TEXT)) {
                pduPart2.setCharset(106);
            }
            pduPart2.setContentType(attachment.getContentType().getBytes());
            pduPart2.setContentLocation(fileName.getBytes());
            pduPart2.setName(fileName.getBytes());
            int lastIndexOf = fileName.lastIndexOf(".");
            if (lastIndexOf != -1) {
                fileName = fileName.substring(0, lastIndexOf);
            }
            pduPart2.setContentId(fileName.getBytes());
            pduPart2.setData(StreamUtil.readFully(PartAuthority.getAttachmentStream(this.context, attachment.getUri())));
            pduBody.addPart(pduPart2);
            i = (int) (((long) i) + getPartSize(pduPart2));
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(pduBody), byteArrayOutputStream);
        PduPart pduPart3 = new PduPart();
        pduPart3.setContentId("smil".getBytes());
        pduPart3.setContentLocation("smil.xml".getBytes());
        pduPart3.setContentType("application/smil".getBytes());
        pduPart3.setData(byteArrayOutputStream.toByteArray());
        pduBody.addPart(0, pduPart3);
        sendReq.setBody(pduBody);
        sendReq.setMessageSize((long) i);
        sendReq.setMessageClass("personal".getBytes());
        sendReq.setExpiry(604800);
        try {
            sendReq.setPriority(129);
            sendReq.setDeliveryReport(129);
            sendReq.setReadReport(129);
        } catch (InvalidHeaderValueException unused) {
        }
        return sendReq;
    }

    private long getPartSize(PduPart pduPart) {
        return (long) (pduPart.getName().length + pduPart.getContentLocation().length + pduPart.getContentType().length + pduPart.getData().length + pduPart.getContentId().length);
    }

    private void notifyMediaMessageDeliveryFailed(Context context, long j) {
        long threadIdForMessage = SignalDatabase.mms().getThreadIdForMessage(j);
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(threadIdForMessage);
        if (recipientForThreadId != null) {
            ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(context, recipientForThreadId, ConversationId.forConversation(threadIdForMessage));
        }
    }

    private String getMyNumber(Context context) throws UndeliverableMessageException {
        try {
            return Utils.getMyPhoneNumber(context);
        } catch (SecurityException e) {
            throw new UndeliverableMessageException(e);
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<MmsSendJob> {
        public MmsSendJob create(Job.Parameters parameters, Data data) {
            return new MmsSendJob(parameters, data.getLong("message_id"));
        }
    }
}
