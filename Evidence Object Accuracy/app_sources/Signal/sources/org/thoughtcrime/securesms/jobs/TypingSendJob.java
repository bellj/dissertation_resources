package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import j$.util.Optional;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.CancelationException;
import org.whispersystems.signalservice.api.messages.SignalServiceTypingMessage;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;

/* loaded from: classes4.dex */
public class TypingSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_THREAD_ID;
    private static final String KEY_TYPING;
    private static final String TAG = Log.tag(TypingSendJob.class);
    private long threadId;
    private boolean typing;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public TypingSendJob(long j, boolean z) {
        this(new Job.Parameters.Builder().setQueue(getQueue(j)).setMaxAttempts(1).setLifespan(TimeUnit.SECONDS.toMillis(5)).addConstraint(NetworkConstraint.KEY).setMemoryOnly(true).build(), j, z);
    }

    public static String getQueue(long j) {
        return "TYPING_" + j;
    }

    private TypingSendJob(Job.Parameters parameters, long j, boolean z) {
        super(parameters);
        this.threadId = j;
        this.typing = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("thread_id", this.threadId).putBoolean(KEY_TYPING, this.typing).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (TextSecurePreferences.isTypingIndicatorsEnabled(this.context)) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Sending typing ");
            sb.append(this.typing ? "started" : "stopped");
            sb.append(" for thread ");
            sb.append(this.threadId);
            Log.d(str, sb.toString());
            Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(this.threadId);
            if (recipientForThreadId == null) {
                Log.w(str, "Tried to send a typing indicator to a non-existent thread.");
            } else if (recipientForThreadId.isBlocked()) {
                Log.w(str, "Not sending typing indicators to blocked recipients.");
            } else if (recipientForThreadId.isSelf()) {
                Log.w(str, "Not sending typing indicators to self.");
            } else if (recipientForThreadId.isPushV1Group() || recipientForThreadId.isMmsGroup()) {
                Log.w(str, "Not sending typing indicators to unsupported groups.");
            } else if (!recipientForThreadId.isRegistered() || recipientForThreadId.isForceSmsSelection()) {
                Log.w(str, "Not sending typing indicators to non-Signal recipients.");
            } else {
                List<Recipient> singletonList = Collections.singletonList(recipientForThreadId);
                Optional empty = Optional.empty();
                if (recipientForThreadId.isGroup()) {
                    singletonList = SignalDatabase.groups().getGroupMembers(recipientForThreadId.requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF);
                    empty = Optional.of(recipientForThreadId.requireGroupId().getDecodedId());
                }
                try {
                    GroupSendUtil.sendTypingMessage(this.context, (GroupId.V2) recipientForThreadId.getGroupId().map(new PushGroupSendJob$$ExternalSyntheticLambda24()).orElse(null), RecipientUtil.getEligibleForSending(Stream.of(singletonList).map(new PushGroupSendJob$$ExternalSyntheticLambda1()).toList()), new SignalServiceTypingMessage(this.typing ? SignalServiceTypingMessage.Action.STARTED : SignalServiceTypingMessage.Action.STOPPED, System.currentTimeMillis(), empty), new CancelationSignal() { // from class: org.thoughtcrime.securesms.jobs.TypingSendJob$$ExternalSyntheticLambda0
                        @Override // org.whispersystems.signalservice.internal.push.http.CancelationSignal
                        public final boolean isCanceled() {
                            return TypingSendJob.this.isCanceled();
                        }
                    });
                } catch (CancelationException unused) {
                    Log.w(TAG, "Canceled during send!");
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<TypingSendJob> {
        public TypingSendJob create(Job.Parameters parameters, Data data) {
            return new TypingSendJob(parameters, data.getLong("thread_id"), data.getBoolean(TypingSendJob.KEY_TYPING));
        }
    }
}
