package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.KeepMessagesDuration;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class TrimThreadJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_THREAD_ID;
    private static final String QUEUE_PREFIX;
    private static final String TAG = Log.tag(TrimThreadJob.class);
    private final long threadId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public static void enqueueAsync(long j) {
        if (SignalStore.settings().getKeepMessagesDuration() != KeepMessagesDuration.FOREVER || SignalStore.settings().isTrimByLengthEnabled()) {
            SignalExecutors.BOUNDED.execute(new Runnable(j) { // from class: org.thoughtcrime.securesms.jobs.TrimThreadJob$$ExternalSyntheticLambda0
                public final /* synthetic */ long f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    TrimThreadJob.lambda$enqueueAsync$0(this.f$0);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$enqueueAsync$0(long j) {
        ApplicationDependencies.getJobManager().add(new TrimThreadJob(j));
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private TrimThreadJob(long r4) {
        /*
            r3 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "TrimThreadJob_"
            r1.append(r2)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            r1 = 2
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxInstancesForQueue(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            r3.<init>(r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.TrimThreadJob.<init>(long):void");
    }

    private TrimThreadJob(Job.Parameters parameters, long j) {
        super(parameters);
        this.threadId = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("thread_id", this.threadId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        KeepMessagesDuration keepMessagesDuration = SignalStore.settings().getKeepMessagesDuration();
        SignalDatabase.threads().trimThread(this.threadId, SignalStore.settings().isTrimByLengthEnabled() ? SignalStore.settings().getThreadTrimLength() : Integer.MAX_VALUE, keepMessagesDuration != KeepMessagesDuration.FOREVER ? System.currentTimeMillis() - keepMessagesDuration.getDuration() : 0);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Canceling trim attempt: " + this.threadId);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<TrimThreadJob> {
        public TrimThreadJob create(Job.Parameters parameters, Data data) {
            return new TrimThreadJob(parameters, data.getLong("thread_id"));
        }
    }
}
