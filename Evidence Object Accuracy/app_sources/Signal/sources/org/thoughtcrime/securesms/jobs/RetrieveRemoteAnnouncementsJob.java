package org.thoughtcrime.securesms.jobs;

import androidx.core.os.LocaleListCompat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RemoteMegaphoneRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob;
import org.thoughtcrime.securesms.keyvalue.ReleaseChannelValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.s3.S3;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: RetrieveRemoteAnnouncementsJob.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\r\u0018\u0000 #2\u00020\u0001:\t#$%&'()*+B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\bH\u0016J\b\u0010\u000e\u001a\u00020\bH\u0014J\u0014\u0010\u000f\u001a\u00020\u00032\n\u0010\u0010\u001a\u00060\u0011j\u0002`\u0012H\u0014J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\u0016\u0010\u001d\u001a\u00020\b2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00160\u001fH\u0002J\u0016\u0010 \u001a\u00020\b2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u001a0\u001fH\u0002J\u0012\u0010\"\u001a\b\u0012\u0004\u0012\u00020\f0\u001f*\u00020\fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", RetrieveRemoteAnnouncementsJob.KEY_FORCE, "", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(ZLorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "fetchManifest", "", "manifestMd5", "", "getFactoryKey", "", "onFailure", "onRun", "onShouldRetry", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "resolveMegaphone", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$FullRemoteMegaphone;", "remoteMegaphone", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;", "resolveReleaseNote", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$FullReleaseNote;", "releaseNote", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "updateMegaphones", "megaphones", "", "updateReleaseNotes", "announcements", "getLocaleUrls", "Companion", "Factory", "FullReleaseNote", "FullRemoteMegaphone", "ReleaseNote", "ReleaseNotes", "RemoteMegaphone", "TranslatedReleaseNote", "TranslatedRemoteMegaphone", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RetrieveRemoteAnnouncementsJob extends BaseJob {
    private static final String BASE_RELEASE_NOTE;
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String KEY_FORCE;
    private static final String MANIFEST;
    private static final long RETRIEVE_FREQUENCY = TimeUnit.DAYS.toMillis(3);
    private static final String TAG = Log.tag(RetrieveRemoteAnnouncementsJob.class);
    private final boolean force;

    public /* synthetic */ RetrieveRemoteAnnouncementsJob(boolean z, Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(z, parameters);
    }

    @JvmStatic
    public static final void enqueue() {
        Companion.enqueue();
    }

    @JvmStatic
    public static final void enqueue(boolean z) {
        Companion.enqueue(z);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private RetrieveRemoteAnnouncementsJob(boolean z, Job.Parameters parameters) {
        super(parameters);
        this.force = z;
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \u000b*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$Companion;", "", "()V", "BASE_RELEASE_NOTE", "", "KEY", "KEY_FORCE", "MANIFEST", "RETRIEVE_FREQUENCY", "", "TAG", "kotlin.jvm.PlatformType", "enqueue", "", RetrieveRemoteAnnouncementsJob.KEY_FORCE, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final void enqueue() {
            enqueue$default(this, false, 1, null);
        }

        private Companion() {
        }

        public static /* synthetic */ void enqueue$default(Companion companion, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                z = false;
            }
            companion.enqueue(z);
        }

        @JvmStatic
        public final void enqueue(boolean z) {
            if (!SignalStore.account().isRegistered()) {
                Log.i(RetrieveRemoteAnnouncementsJob.TAG, "Not registered, skipping.");
            } else if (z || System.currentTimeMillis() >= SignalStore.releaseChannelValues().getNextScheduledCheck()) {
                Job.Parameters build = new Job.Parameters.Builder().setQueue(RetrieveRemoteAnnouncementsJob.KEY).setMaxInstancesForFactory(1).setMaxAttempts(3).addConstraint(NetworkConstraint.KEY).build();
                Intrinsics.checkNotNullExpressionValue(build, "Builder()\n          .set…t.KEY)\n          .build()");
                ApplicationDependencies.getJobManager().startChain(CreateReleaseChannelJob.Companion.create()).then(new RetrieveRemoteAnnouncementsJob(z, build, null)).enqueue();
            } else {
                Log.i(RetrieveRemoteAnnouncementsJob.TAG, "Too soon to check for updated release notes");
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data build = new Data.Builder().putBoolean(KEY_FORCE, this.force).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder().putBoolean(KEY_FORCE, force).build()");
        return build;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        if (!SignalStore.account().isRegistered()) {
            Log.i(TAG, "Not registered, skipping.");
            return;
        }
        ReleaseChannelValues releaseChannelValues = SignalStore.releaseChannelValues();
        Intrinsics.checkNotNullExpressionValue(releaseChannelValues, "releaseChannelValues()");
        if (releaseChannelValues.getReleaseChannelRecipientId() == null) {
            Log.w(TAG, "Release Channel recipient is null, this shouldn't happen, will try to create on next run");
        } else if (this.force || System.currentTimeMillis() >= releaseChannelValues.getNextScheduledCheck()) {
            byte[] objectMD5 = S3.INSTANCE.getObjectMD5(MANIFEST);
            if (objectMD5 == null) {
                Log.i(TAG, "Unable to retrieve manifest MD5");
                return;
            }
            if (releaseChannelValues.getHighestVersionNoteReceived() == 0) {
                Log.i(TAG, "First check, saving code and skipping download");
                releaseChannelValues.setHighestVersionNoteReceived(BuildConfig.CANONICAL_VERSION_CODE);
            } else if (this.force || !MessageDigest.isEqual(objectMD5, releaseChannelValues.getPreviousManifestMd5())) {
                fetchManifest(objectMD5);
            } else {
                Log.i(TAG, "Manifest has not changed since last fetch.");
            }
            releaseChannelValues.setPreviousManifestMd5(objectMD5);
            releaseChannelValues.setNextScheduledCheck(System.currentTimeMillis() + RETRIEVE_FREQUENCY);
        } else {
            Log.i(TAG, "Too soon to check for updated release notes");
        }
    }

    private final void fetchManifest(byte[] bArr) {
        String str = TAG;
        Log.i(str, "Updating release notes to " + Hex.toStringCondensed(bArr));
        ReleaseNotes releaseNotes = (ReleaseNotes) S3.INSTANCE.getAndVerifyObject(MANIFEST, ReleaseNotes.class, bArr).getResult().orElse(null);
        if (releaseNotes != null) {
            updateReleaseNotes(releaseNotes.getAnnouncements());
            List<RemoteMegaphone> megaphones = releaseNotes.getMegaphones();
            if (megaphones == null) {
                megaphones = CollectionsKt__CollectionsKt.emptyList();
            }
            updateMegaphones(megaphones);
            return;
        }
        Log.w(str, "Unable to retrieve manifest json");
    }

    /* JADX WARNING: Removed duplicated region for block: B:104:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x02e4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x023a  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0251  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void updateReleaseNotes(java.util.List<org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob.ReleaseNote> r30) {
        /*
        // Method dump skipped, instructions count: 788
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob.updateReleaseNotes(java.util.List):void");
    }

    private final void updateMegaphones(List<RemoteMegaphone> list) {
        boolean z;
        boolean z2;
        List<FullRemoteMegaphone> list2 = SequencesKt___SequencesKt.toList(SequencesKt___SequencesKt.map(SequencesKt___SequencesKt.filter(CollectionsKt___CollectionsKt.asSequence(list), RetrieveRemoteAnnouncementsJob$updateMegaphones$resolvedMegaphones$1.INSTANCE), new Function1<RemoteMegaphone, FullRemoteMegaphone>(this) { // from class: org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob$updateMegaphones$resolvedMegaphones$2
            final /* synthetic */ RetrieveRemoteAnnouncementsJob this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            public final RetrieveRemoteAnnouncementsJob.FullRemoteMegaphone invoke(RetrieveRemoteAnnouncementsJob.RemoteMegaphone remoteMegaphone) {
                Intrinsics.checkNotNullParameter(remoteMegaphone, "it");
                return this.this$0.resolveMegaphone(remoteMegaphone);
            }
        }));
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            for (FullRemoteMegaphone fullRemoteMegaphone : list2) {
                if (fullRemoteMegaphone == null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    z = true;
                    break;
                }
            }
        }
        z = false;
        if (!z) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            List<RemoteMegaphoneRecord> all = SignalDatabase.Companion.remoteMegaphones().getAll();
            LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(all, 10)), 16));
            for (Object obj : all) {
                linkedHashMap.put(((RemoteMegaphoneRecord) obj).getUuid(), obj);
            }
            for (FullRemoteMegaphone fullRemoteMegaphone2 : CollectionsKt___CollectionsKt.filterNotNull(list2)) {
                String uuid = fullRemoteMegaphone2.getRemoteMegaphone().getUuid();
                linkedHashSet.add(uuid);
                if (linkedHashMap.containsKey(uuid)) {
                    SignalDatabase.Companion.remoteMegaphones().update(uuid, fullRemoteMegaphone2.getRemoteMegaphone().getPriority(), fullRemoteMegaphone2.getRemoteMegaphone().getCountries(), fullRemoteMegaphone2.getTranslation().getTitle(), fullRemoteMegaphone2.getTranslation().getBody(), fullRemoteMegaphone2.getTranslation().getPrimaryCtaText(), fullRemoteMegaphone2.getTranslation().getSecondaryCtaText());
                } else {
                    long priority = fullRemoteMegaphone2.getRemoteMegaphone().getPriority();
                    String countries = fullRemoteMegaphone2.getRemoteMegaphone().getCountries();
                    String androidMinVersion = fullRemoteMegaphone2.getRemoteMegaphone().getAndroidMinVersion();
                    Intrinsics.checkNotNull(androidMinVersion);
                    int parseInt = Integer.parseInt(androidMinVersion);
                    Long dontShowBeforeEpochSeconds = fullRemoteMegaphone2.getRemoteMegaphone().getDontShowBeforeEpochSeconds();
                    long millis = dontShowBeforeEpochSeconds != null ? TimeUnit.SECONDS.toMillis(dontShowBeforeEpochSeconds.longValue()) : 0;
                    Long dontShowAfterEpochSeconds = fullRemoteMegaphone2.getRemoteMegaphone().getDontShowAfterEpochSeconds();
                    long millis2 = dontShowAfterEpochSeconds != null ? TimeUnit.SECONDS.toMillis(dontShowAfterEpochSeconds.longValue()) : Long.MAX_VALUE;
                    Long showForNumberOfDays = fullRemoteMegaphone2.getRemoteMegaphone().getShowForNumberOfDays();
                    long longValue = showForNumberOfDays != null ? showForNumberOfDays.longValue() : 0;
                    String conditionalId = fullRemoteMegaphone2.getRemoteMegaphone().getConditionalId();
                    RemoteMegaphoneRecord.ActionId.Companion companion = RemoteMegaphoneRecord.ActionId.Companion;
                    RemoteMegaphoneRecord remoteMegaphoneRecord = new RemoteMegaphoneRecord(0, priority, uuid, countries, parseInt, millis, millis2, longValue, conditionalId, companion.from(fullRemoteMegaphone2.getRemoteMegaphone().getPrimaryCtaId()), companion.from(fullRemoteMegaphone2.getRemoteMegaphone().getSecondaryCtaId()), fullRemoteMegaphone2.getTranslation().getImage(), null, fullRemoteMegaphone2.getTranslation().getTitle(), fullRemoteMegaphone2.getTranslation().getBody(), fullRemoteMegaphone2.getTranslation().getPrimaryCtaText(), fullRemoteMegaphone2.getTranslation().getSecondaryCtaText(), 0, 0, 397313, null);
                    SignalDatabase.Companion.remoteMegaphones().insert(remoteMegaphoneRecord);
                    if (remoteMegaphoneRecord.getImageUrl() != null) {
                        ApplicationDependencies.getJobManager().add(new FetchRemoteMegaphoneImageJob(remoteMegaphoneRecord.getUuid(), remoteMegaphoneRecord.getImageUrl()));
                    }
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                if (!linkedHashSet.contains((String) entry.getKey())) {
                    linkedHashMap2.put(entry.getKey(), entry.getValue());
                }
            }
            LinkedHashMap linkedHashMap3 = new LinkedHashMap();
            for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
                if (((RemoteMegaphoneRecord) entry2.getValue()).getMinimumVersion() != Integer.MAX_VALUE) {
                    linkedHashMap3.put(entry2.getKey(), entry2.getValue());
                }
            }
            if (!linkedHashMap3.isEmpty()) {
                String str = TAG;
                Log.i(str, "Clearing " + linkedHashMap3.size() + " stale megaphones " + linkedHashMap3.keySet());
                for (Map.Entry entry3 : linkedHashMap3.entrySet()) {
                    String str2 = (String) entry3.getKey();
                    RemoteMegaphoneRecord remoteMegaphoneRecord2 = (RemoteMegaphoneRecord) entry3.getValue();
                    if (remoteMegaphoneRecord2.getImageUri() != null) {
                        BlobProvider.getInstance().delete(this.context, remoteMegaphoneRecord2.getImageUri());
                    }
                    SignalDatabase.Companion.remoteMegaphones().clear(str2);
                }
                return;
            }
            return;
        }
        Log.w(TAG, "Some megaphones did not resolve, will retry later.");
        throw new RetryLaterException();
    }

    public final FullReleaseNote resolveReleaseNote(ReleaseNote releaseNote) {
        for (String str : getLocaleUrls("/static/release-notes/" + releaseNote.getUuid())) {
            ServiceResponse andVerifyObject$default = S3.getAndVerifyObject$default(S3.INSTANCE, str, TranslatedReleaseNote.class, null, 4, null);
            if (andVerifyObject$default.getResult().isPresent()) {
                Object obj = andVerifyObject$default.getResult().get();
                Intrinsics.checkNotNullExpressionValue(obj, "translationJson.result.get()");
                return new FullReleaseNote(releaseNote, (TranslatedReleaseNote) obj);
            } else if (andVerifyObject$default.getStatus() != 404 && !(andVerifyObject$default.getExecutionError().orElse(null) instanceof S3.Md5FailureException)) {
                throw new RetryLaterException();
            }
        }
        return null;
    }

    public final FullRemoteMegaphone resolveMegaphone(RemoteMegaphone remoteMegaphone) {
        for (String str : getLocaleUrls("/static/release-notes/" + remoteMegaphone.getUuid())) {
            ServiceResponse andVerifyObject$default = S3.getAndVerifyObject$default(S3.INSTANCE, str, TranslatedRemoteMegaphone.class, null, 4, null);
            if (andVerifyObject$default.getResult().isPresent()) {
                Object obj = andVerifyObject$default.getResult().get();
                Intrinsics.checkNotNullExpressionValue(obj, "translationJson.result.get()");
                return new FullRemoteMegaphone(remoteMegaphone, (TranslatedRemoteMegaphone) obj);
            } else if (andVerifyObject$default.getStatus() != 404 && !(andVerifyObject$default.getExecutionError().orElse(null) instanceof S3.Md5FailureException)) {
                throw new RetryLaterException();
            }
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return (exc instanceof RetryLaterException) || (exc instanceof IOException);
    }

    private final List<String> getLocaleUrls(String str) {
        LocaleListCompat localeListCompat = LocaleListCompat.getDefault();
        Intrinsics.checkNotNullExpressionValue(localeListCompat, "getDefault()");
        ArrayList arrayList = new ArrayList();
        if (!Intrinsics.areEqual(SignalStore.settings().getLanguage(), "zz")) {
            arrayList.add(str + '/' + SignalStore.settings().getLanguage() + ".json");
        }
        int size = localeListCompat.size();
        for (int i = 0; i < size; i++) {
            Locale locale = localeListCompat.get(i);
            Intrinsics.checkNotNullExpressionValue(locale, "localeList.get(index)");
            String language = locale.getLanguage();
            Intrinsics.checkNotNullExpressionValue(language, "locale.language");
            boolean z = true;
            if (language.length() > 0) {
                String country = locale.getCountry();
                Intrinsics.checkNotNullExpressionValue(country, "locale.country");
                if (country.length() <= 0) {
                    z = false;
                }
                if (z) {
                    arrayList.add(str + '/' + locale.getLanguage() + '_' + locale.getCountry() + ".json");
                }
                arrayList.add(str + '/' + locale.getLanguage() + ".json");
            }
        }
        arrayList.add(str + "/en.json");
        return arrayList;
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$FullReleaseNote;", "", "releaseNote", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;", "translation", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedReleaseNote;", "(Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedReleaseNote;)V", "getReleaseNote", "()Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;", "getTranslation", "()Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedReleaseNote;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FullReleaseNote {
        private final ReleaseNote releaseNote;
        private final TranslatedReleaseNote translation;

        public static /* synthetic */ FullReleaseNote copy$default(FullReleaseNote fullReleaseNote, ReleaseNote releaseNote, TranslatedReleaseNote translatedReleaseNote, int i, Object obj) {
            if ((i & 1) != 0) {
                releaseNote = fullReleaseNote.releaseNote;
            }
            if ((i & 2) != 0) {
                translatedReleaseNote = fullReleaseNote.translation;
            }
            return fullReleaseNote.copy(releaseNote, translatedReleaseNote);
        }

        public final ReleaseNote component1() {
            return this.releaseNote;
        }

        public final TranslatedReleaseNote component2() {
            return this.translation;
        }

        public final FullReleaseNote copy(ReleaseNote releaseNote, TranslatedReleaseNote translatedReleaseNote) {
            Intrinsics.checkNotNullParameter(releaseNote, "releaseNote");
            Intrinsics.checkNotNullParameter(translatedReleaseNote, "translation");
            return new FullReleaseNote(releaseNote, translatedReleaseNote);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FullReleaseNote)) {
                return false;
            }
            FullReleaseNote fullReleaseNote = (FullReleaseNote) obj;
            return Intrinsics.areEqual(this.releaseNote, fullReleaseNote.releaseNote) && Intrinsics.areEqual(this.translation, fullReleaseNote.translation);
        }

        public int hashCode() {
            return (this.releaseNote.hashCode() * 31) + this.translation.hashCode();
        }

        public String toString() {
            return "FullReleaseNote(releaseNote=" + this.releaseNote + ", translation=" + this.translation + ')';
        }

        public FullReleaseNote(ReleaseNote releaseNote, TranslatedReleaseNote translatedReleaseNote) {
            Intrinsics.checkNotNullParameter(releaseNote, "releaseNote");
            Intrinsics.checkNotNullParameter(translatedReleaseNote, "translation");
            this.releaseNote = releaseNote;
            this.translation = translatedReleaseNote;
        }

        public final ReleaseNote getReleaseNote() {
            return this.releaseNote;
        }

        public final TranslatedReleaseNote getTranslation() {
            return this.translation;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$FullRemoteMegaphone;", "", "remoteMegaphone", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;", "translation", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedRemoteMegaphone;", "(Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedRemoteMegaphone;)V", "getRemoteMegaphone", "()Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;", "getTranslation", "()Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedRemoteMegaphone;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FullRemoteMegaphone {
        private final RemoteMegaphone remoteMegaphone;
        private final TranslatedRemoteMegaphone translation;

        public static /* synthetic */ FullRemoteMegaphone copy$default(FullRemoteMegaphone fullRemoteMegaphone, RemoteMegaphone remoteMegaphone, TranslatedRemoteMegaphone translatedRemoteMegaphone, int i, Object obj) {
            if ((i & 1) != 0) {
                remoteMegaphone = fullRemoteMegaphone.remoteMegaphone;
            }
            if ((i & 2) != 0) {
                translatedRemoteMegaphone = fullRemoteMegaphone.translation;
            }
            return fullRemoteMegaphone.copy(remoteMegaphone, translatedRemoteMegaphone);
        }

        public final RemoteMegaphone component1() {
            return this.remoteMegaphone;
        }

        public final TranslatedRemoteMegaphone component2() {
            return this.translation;
        }

        public final FullRemoteMegaphone copy(RemoteMegaphone remoteMegaphone, TranslatedRemoteMegaphone translatedRemoteMegaphone) {
            Intrinsics.checkNotNullParameter(remoteMegaphone, "remoteMegaphone");
            Intrinsics.checkNotNullParameter(translatedRemoteMegaphone, "translation");
            return new FullRemoteMegaphone(remoteMegaphone, translatedRemoteMegaphone);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FullRemoteMegaphone)) {
                return false;
            }
            FullRemoteMegaphone fullRemoteMegaphone = (FullRemoteMegaphone) obj;
            return Intrinsics.areEqual(this.remoteMegaphone, fullRemoteMegaphone.remoteMegaphone) && Intrinsics.areEqual(this.translation, fullRemoteMegaphone.translation);
        }

        public int hashCode() {
            return (this.remoteMegaphone.hashCode() * 31) + this.translation.hashCode();
        }

        public String toString() {
            return "FullRemoteMegaphone(remoteMegaphone=" + this.remoteMegaphone + ", translation=" + this.translation + ')';
        }

        public FullRemoteMegaphone(RemoteMegaphone remoteMegaphone, TranslatedRemoteMegaphone translatedRemoteMegaphone) {
            Intrinsics.checkNotNullParameter(remoteMegaphone, "remoteMegaphone");
            Intrinsics.checkNotNullParameter(translatedRemoteMegaphone, "translation");
            this.remoteMegaphone = remoteMegaphone;
            this.translation = translatedRemoteMegaphone;
        }

        public final RemoteMegaphone getRemoteMegaphone() {
            return this.remoteMegaphone;
        }

        public final TranslatedRemoteMegaphone getTranslation() {
            return this.translation;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B'\u0012\u000e\b\u0001\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0010\b\u0001\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0011\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003HÆ\u0003J+\u0010\r\u001a\u00020\u00002\u000e\b\u0003\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0010\b\u0003\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0019\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNotes;", "", "announcements", "", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;", "megaphones", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;", "(Ljava/util/List;Ljava/util/List;)V", "getAnnouncements", "()Ljava/util/List;", "getMegaphones", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class ReleaseNotes {
        private final List<ReleaseNote> announcements;
        private final List<RemoteMegaphone> megaphones;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob$ReleaseNotes */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ReleaseNotes copy$default(ReleaseNotes releaseNotes, List list, List list2, int i, Object obj) {
            if ((i & 1) != 0) {
                list = releaseNotes.announcements;
            }
            if ((i & 2) != 0) {
                list2 = releaseNotes.megaphones;
            }
            return releaseNotes.copy(list, list2);
        }

        public final List<ReleaseNote> component1() {
            return this.announcements;
        }

        public final List<RemoteMegaphone> component2() {
            return this.megaphones;
        }

        public final ReleaseNotes copy(@JsonProperty List<ReleaseNote> list, @JsonProperty List<RemoteMegaphone> list2) {
            Intrinsics.checkNotNullParameter(list, "announcements");
            return new ReleaseNotes(list, list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ReleaseNotes)) {
                return false;
            }
            ReleaseNotes releaseNotes = (ReleaseNotes) obj;
            return Intrinsics.areEqual(this.announcements, releaseNotes.announcements) && Intrinsics.areEqual(this.megaphones, releaseNotes.megaphones);
        }

        public int hashCode() {
            int hashCode = this.announcements.hashCode() * 31;
            List<RemoteMegaphone> list = this.megaphones;
            return hashCode + (list == null ? 0 : list.hashCode());
        }

        public String toString() {
            return "ReleaseNotes(announcements=" + this.announcements + ", megaphones=" + this.megaphones + ')';
        }

        public ReleaseNotes(@JsonProperty List<ReleaseNote> list, @JsonProperty List<RemoteMegaphone> list2) {
            Intrinsics.checkNotNullParameter(list, "announcements");
            this.announcements = list;
            this.megaphones = list2;
        }

        public final List<ReleaseNote> getAnnouncements() {
            return this.announcements;
        }

        public final List<RemoteMegaphone> getMegaphones() {
            return this.megaphones;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B?\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003JC\u0010\u0014\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;", "", RecipientDatabase.SERVICE_ID, "", "countries", "androidMinVersion", "link", "ctaId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAndroidMinVersion", "()Ljava/lang/String;", "getCountries", "getCtaId", "getLink", "getUuid", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class ReleaseNote {
        private final String androidMinVersion;
        private final String countries;
        private final String ctaId;
        private final String link;
        private final String uuid;

        public static /* synthetic */ ReleaseNote copy$default(ReleaseNote releaseNote, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
            if ((i & 1) != 0) {
                str = releaseNote.uuid;
            }
            if ((i & 2) != 0) {
                str2 = releaseNote.countries;
            }
            if ((i & 4) != 0) {
                str3 = releaseNote.androidMinVersion;
            }
            if ((i & 8) != 0) {
                str4 = releaseNote.link;
            }
            if ((i & 16) != 0) {
                str5 = releaseNote.ctaId;
            }
            return releaseNote.copy(str, str2, str3, str4, str5);
        }

        public final String component1() {
            return this.uuid;
        }

        public final String component2() {
            return this.countries;
        }

        public final String component3() {
            return this.androidMinVersion;
        }

        public final String component4() {
            return this.link;
        }

        public final String component5() {
            return this.ctaId;
        }

        public final ReleaseNote copy(@JsonProperty String str, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty String str4, @JsonProperty String str5) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            return new ReleaseNote(str, str2, str3, str4, str5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ReleaseNote)) {
                return false;
            }
            ReleaseNote releaseNote = (ReleaseNote) obj;
            return Intrinsics.areEqual(this.uuid, releaseNote.uuid) && Intrinsics.areEqual(this.countries, releaseNote.countries) && Intrinsics.areEqual(this.androidMinVersion, releaseNote.androidMinVersion) && Intrinsics.areEqual(this.link, releaseNote.link) && Intrinsics.areEqual(this.ctaId, releaseNote.ctaId);
        }

        public int hashCode() {
            int hashCode = this.uuid.hashCode() * 31;
            String str = this.countries;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.androidMinVersion;
            int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.link;
            int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.ctaId;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "ReleaseNote(uuid=" + this.uuid + ", countries=" + this.countries + ", androidMinVersion=" + this.androidMinVersion + ", link=" + this.link + ", ctaId=" + this.ctaId + ')';
        }

        public ReleaseNote(@JsonProperty String str, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty String str4, @JsonProperty String str5) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            this.uuid = str;
            this.countries = str2;
            this.androidMinVersion = str3;
            this.link = str4;
            this.ctaId = str5;
        }

        public final String getUuid() {
            return this.uuid;
        }

        public final String getCountries() {
            return this.countries;
        }

        public final String getAndroidMinVersion() {
            return this.androidMinVersion;
        }

        public final String getLink() {
            return this.link;
        }

        public final String getCtaId() {
            return this.ctaId;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b$\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001By\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000eJ\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0005HÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\"\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\u0014J\u0010\u0010#\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\u0014J\u0010\u0010$\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\u0014J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0001\u0010'\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\r\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010(J\u0013\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010,\u001a\u00020-HÖ\u0001J\t\u0010.\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010R\u0015\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u0015\u001a\u0004\b\u0013\u0010\u0014R\u0015\u0010\b\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u0015\u001a\u0004\b\u0016\u0010\u0014R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0010R\u0015\u0010\n\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u0015\u001a\u0004\b\u001b\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0010¨\u0006/"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;", "", RecipientDatabase.SERVICE_ID, "", "priority", "", "countries", "androidMinVersion", "dontShowBeforeEpochSeconds", "dontShowAfterEpochSeconds", "showForNumberOfDays", "conditionalId", "primaryCtaId", "secondaryCtaId", "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAndroidMinVersion", "()Ljava/lang/String;", "getConditionalId", "getCountries", "getDontShowAfterEpochSeconds", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getDontShowBeforeEpochSeconds", "getPrimaryCtaId", "getPriority", "()J", "getSecondaryCtaId", "getShowForNumberOfDays", "getUuid", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$RemoteMegaphone;", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class RemoteMegaphone {
        private final String androidMinVersion;
        private final String conditionalId;
        private final String countries;
        private final Long dontShowAfterEpochSeconds;
        private final Long dontShowBeforeEpochSeconds;
        private final String primaryCtaId;
        private final long priority;
        private final String secondaryCtaId;
        private final Long showForNumberOfDays;
        private final String uuid;

        public final String component1() {
            return this.uuid;
        }

        public final String component10() {
            return this.secondaryCtaId;
        }

        public final long component2() {
            return this.priority;
        }

        public final String component3() {
            return this.countries;
        }

        public final String component4() {
            return this.androidMinVersion;
        }

        public final Long component5() {
            return this.dontShowBeforeEpochSeconds;
        }

        public final Long component6() {
            return this.dontShowAfterEpochSeconds;
        }

        public final Long component7() {
            return this.showForNumberOfDays;
        }

        public final String component8() {
            return this.conditionalId;
        }

        public final String component9() {
            return this.primaryCtaId;
        }

        public final RemoteMegaphone copy(@JsonProperty String str, @JsonProperty long j, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty Long l, @JsonProperty Long l2, @JsonProperty Long l3, @JsonProperty String str4, @JsonProperty String str5, @JsonProperty String str6) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            return new RemoteMegaphone(str, j, str2, str3, l, l2, l3, str4, str5, str6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RemoteMegaphone)) {
                return false;
            }
            RemoteMegaphone remoteMegaphone = (RemoteMegaphone) obj;
            return Intrinsics.areEqual(this.uuid, remoteMegaphone.uuid) && this.priority == remoteMegaphone.priority && Intrinsics.areEqual(this.countries, remoteMegaphone.countries) && Intrinsics.areEqual(this.androidMinVersion, remoteMegaphone.androidMinVersion) && Intrinsics.areEqual(this.dontShowBeforeEpochSeconds, remoteMegaphone.dontShowBeforeEpochSeconds) && Intrinsics.areEqual(this.dontShowAfterEpochSeconds, remoteMegaphone.dontShowAfterEpochSeconds) && Intrinsics.areEqual(this.showForNumberOfDays, remoteMegaphone.showForNumberOfDays) && Intrinsics.areEqual(this.conditionalId, remoteMegaphone.conditionalId) && Intrinsics.areEqual(this.primaryCtaId, remoteMegaphone.primaryCtaId) && Intrinsics.areEqual(this.secondaryCtaId, remoteMegaphone.secondaryCtaId);
        }

        public int hashCode() {
            int hashCode = ((this.uuid.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.priority)) * 31;
            String str = this.countries;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.androidMinVersion;
            int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
            Long l = this.dontShowBeforeEpochSeconds;
            int hashCode4 = (hashCode3 + (l == null ? 0 : l.hashCode())) * 31;
            Long l2 = this.dontShowAfterEpochSeconds;
            int hashCode5 = (hashCode4 + (l2 == null ? 0 : l2.hashCode())) * 31;
            Long l3 = this.showForNumberOfDays;
            int hashCode6 = (hashCode5 + (l3 == null ? 0 : l3.hashCode())) * 31;
            String str3 = this.conditionalId;
            int hashCode7 = (hashCode6 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.primaryCtaId;
            int hashCode8 = (hashCode7 + (str4 == null ? 0 : str4.hashCode())) * 31;
            String str5 = this.secondaryCtaId;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode8 + i;
        }

        public String toString() {
            return "RemoteMegaphone(uuid=" + this.uuid + ", priority=" + this.priority + ", countries=" + this.countries + ", androidMinVersion=" + this.androidMinVersion + ", dontShowBeforeEpochSeconds=" + this.dontShowBeforeEpochSeconds + ", dontShowAfterEpochSeconds=" + this.dontShowAfterEpochSeconds + ", showForNumberOfDays=" + this.showForNumberOfDays + ", conditionalId=" + this.conditionalId + ", primaryCtaId=" + this.primaryCtaId + ", secondaryCtaId=" + this.secondaryCtaId + ')';
        }

        public RemoteMegaphone(@JsonProperty String str, @JsonProperty long j, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty Long l, @JsonProperty Long l2, @JsonProperty Long l3, @JsonProperty String str4, @JsonProperty String str5, @JsonProperty String str6) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            this.uuid = str;
            this.priority = j;
            this.countries = str2;
            this.androidMinVersion = str3;
            this.dontShowBeforeEpochSeconds = l;
            this.dontShowAfterEpochSeconds = l2;
            this.showForNumberOfDays = l3;
            this.conditionalId = str4;
            this.primaryCtaId = str5;
            this.secondaryCtaId = str6;
        }

        public final String getUuid() {
            return this.uuid;
        }

        public final long getPriority() {
            return this.priority;
        }

        public final String getCountries() {
            return this.countries;
        }

        public final String getAndroidMinVersion() {
            return this.androidMinVersion;
        }

        public final Long getDontShowBeforeEpochSeconds() {
            return this.dontShowBeforeEpochSeconds;
        }

        public final Long getDontShowAfterEpochSeconds() {
            return this.dontShowAfterEpochSeconds;
        }

        public final Long getShowForNumberOfDays() {
            return this.showForNumberOfDays;
        }

        public final String getConditionalId() {
            return this.conditionalId;
        }

        public final String getPrimaryCtaId() {
            return this.primaryCtaId;
        }

        public final String getSecondaryCtaId() {
            return this.secondaryCtaId;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B_\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0001\u0010\b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\t\u001a\u00020\u0003\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0003Jc\u0010\u001d\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\b\b\u0003\u0010\b\u001a\u00020\u00032\b\b\u0003\u0010\t\u001a\u00020\u00032\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\rR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\r¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedReleaseNote;", "", RecipientDatabase.SERVICE_ID, "", DraftDatabase.Draft.IMAGE, "imageWidth", "imageHeight", "linkText", MultiselectForwardFragment.DIALOG_TITLE, "body", "callToActionText", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBody", "()Ljava/lang/String;", "getCallToActionText", "getImage", "getImageHeight", "getImageWidth", "getLinkText", "getTitle", "getUuid", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class TranslatedReleaseNote {
        private final String body;
        private final String callToActionText;
        private final String image;
        private final String imageHeight;
        private final String imageWidth;
        private final String linkText;
        private final String title;
        private final String uuid;

        public final String component1() {
            return this.uuid;
        }

        public final String component2() {
            return this.image;
        }

        public final String component3() {
            return this.imageWidth;
        }

        public final String component4() {
            return this.imageHeight;
        }

        public final String component5() {
            return this.linkText;
        }

        public final String component6() {
            return this.title;
        }

        public final String component7() {
            return this.body;
        }

        public final String component8() {
            return this.callToActionText;
        }

        public final TranslatedReleaseNote copy(@JsonProperty String str, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty String str4, @JsonProperty String str5, @JsonProperty String str6, @JsonProperty String str7, @JsonProperty String str8) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            Intrinsics.checkNotNullParameter(str6, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(str7, "body");
            return new TranslatedReleaseNote(str, str2, str3, str4, str5, str6, str7, str8);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TranslatedReleaseNote)) {
                return false;
            }
            TranslatedReleaseNote translatedReleaseNote = (TranslatedReleaseNote) obj;
            return Intrinsics.areEqual(this.uuid, translatedReleaseNote.uuid) && Intrinsics.areEqual(this.image, translatedReleaseNote.image) && Intrinsics.areEqual(this.imageWidth, translatedReleaseNote.imageWidth) && Intrinsics.areEqual(this.imageHeight, translatedReleaseNote.imageHeight) && Intrinsics.areEqual(this.linkText, translatedReleaseNote.linkText) && Intrinsics.areEqual(this.title, translatedReleaseNote.title) && Intrinsics.areEqual(this.body, translatedReleaseNote.body) && Intrinsics.areEqual(this.callToActionText, translatedReleaseNote.callToActionText);
        }

        public int hashCode() {
            int hashCode = this.uuid.hashCode() * 31;
            String str = this.image;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.imageWidth;
            int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.imageHeight;
            int hashCode4 = (hashCode3 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.linkText;
            int hashCode5 = (((((hashCode4 + (str4 == null ? 0 : str4.hashCode())) * 31) + this.title.hashCode()) * 31) + this.body.hashCode()) * 31;
            String str5 = this.callToActionText;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            return "TranslatedReleaseNote(uuid=" + this.uuid + ", image=" + this.image + ", imageWidth=" + this.imageWidth + ", imageHeight=" + this.imageHeight + ", linkText=" + this.linkText + ", title=" + this.title + ", body=" + this.body + ", callToActionText=" + this.callToActionText + ')';
        }

        public TranslatedReleaseNote(@JsonProperty String str, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty String str4, @JsonProperty String str5, @JsonProperty String str6, @JsonProperty String str7, @JsonProperty String str8) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            Intrinsics.checkNotNullParameter(str6, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(str7, "body");
            this.uuid = str;
            this.image = str2;
            this.imageWidth = str3;
            this.imageHeight = str4;
            this.linkText = str5;
            this.title = str6;
            this.body = str7;
            this.callToActionText = str8;
        }

        public final String getUuid() {
            return this.uuid;
        }

        public final String getImage() {
            return this.image;
        }

        public final String getImageWidth() {
            return this.imageWidth;
        }

        public final String getImageHeight() {
            return this.imageHeight;
        }

        public final String getLinkText() {
            return this.linkText;
        }

        public final String getTitle() {
            return this.title;
        }

        public final String getBody() {
            return this.body;
        }

        public final String getCallToActionText() {
            return this.callToActionText;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BG\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003JK\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$TranslatedRemoteMegaphone;", "", RecipientDatabase.SERVICE_ID, "", DraftDatabase.Draft.IMAGE, MultiselectForwardFragment.DIALOG_TITLE, "body", "primaryCtaText", "secondaryCtaText", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBody", "()Ljava/lang/String;", "getImage", "getPrimaryCtaText", "getSecondaryCtaText", "getTitle", "getUuid", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class TranslatedRemoteMegaphone {
        private final String body;
        private final String image;
        private final String primaryCtaText;
        private final String secondaryCtaText;
        private final String title;
        private final String uuid;

        public static /* synthetic */ TranslatedRemoteMegaphone copy$default(TranslatedRemoteMegaphone translatedRemoteMegaphone, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
            if ((i & 1) != 0) {
                str = translatedRemoteMegaphone.uuid;
            }
            if ((i & 2) != 0) {
                str2 = translatedRemoteMegaphone.image;
            }
            if ((i & 4) != 0) {
                str3 = translatedRemoteMegaphone.title;
            }
            if ((i & 8) != 0) {
                str4 = translatedRemoteMegaphone.body;
            }
            if ((i & 16) != 0) {
                str5 = translatedRemoteMegaphone.primaryCtaText;
            }
            if ((i & 32) != 0) {
                str6 = translatedRemoteMegaphone.secondaryCtaText;
            }
            return translatedRemoteMegaphone.copy(str, str2, str3, str4, str5, str6);
        }

        public final String component1() {
            return this.uuid;
        }

        public final String component2() {
            return this.image;
        }

        public final String component3() {
            return this.title;
        }

        public final String component4() {
            return this.body;
        }

        public final String component5() {
            return this.primaryCtaText;
        }

        public final String component6() {
            return this.secondaryCtaText;
        }

        public final TranslatedRemoteMegaphone copy(@JsonProperty String str, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty String str4, @JsonProperty String str5, @JsonProperty String str6) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            Intrinsics.checkNotNullParameter(str3, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(str4, "body");
            return new TranslatedRemoteMegaphone(str, str2, str3, str4, str5, str6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TranslatedRemoteMegaphone)) {
                return false;
            }
            TranslatedRemoteMegaphone translatedRemoteMegaphone = (TranslatedRemoteMegaphone) obj;
            return Intrinsics.areEqual(this.uuid, translatedRemoteMegaphone.uuid) && Intrinsics.areEqual(this.image, translatedRemoteMegaphone.image) && Intrinsics.areEqual(this.title, translatedRemoteMegaphone.title) && Intrinsics.areEqual(this.body, translatedRemoteMegaphone.body) && Intrinsics.areEqual(this.primaryCtaText, translatedRemoteMegaphone.primaryCtaText) && Intrinsics.areEqual(this.secondaryCtaText, translatedRemoteMegaphone.secondaryCtaText);
        }

        public int hashCode() {
            int hashCode = this.uuid.hashCode() * 31;
            String str = this.image;
            int i = 0;
            int hashCode2 = (((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.title.hashCode()) * 31) + this.body.hashCode()) * 31;
            String str2 = this.primaryCtaText;
            int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.secondaryCtaText;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "TranslatedRemoteMegaphone(uuid=" + this.uuid + ", image=" + this.image + ", title=" + this.title + ", body=" + this.body + ", primaryCtaText=" + this.primaryCtaText + ", secondaryCtaText=" + this.secondaryCtaText + ')';
        }

        public TranslatedRemoteMegaphone(@JsonProperty String str, @JsonProperty String str2, @JsonProperty String str3, @JsonProperty String str4, @JsonProperty String str5, @JsonProperty String str6) {
            Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
            Intrinsics.checkNotNullParameter(str3, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(str4, "body");
            this.uuid = str;
            this.image = str2;
            this.title = str3;
            this.body = str4;
            this.primaryCtaText = str5;
            this.secondaryCtaText = str6;
        }

        public final String getUuid() {
            return this.uuid;
        }

        public final String getImage() {
            return this.image;
        }

        public final String getTitle() {
            return this.title;
        }

        public final String getBody() {
            return this.body;
        }

        public final String getPrimaryCtaText() {
            return this.primaryCtaText;
        }

        public final String getSecondaryCtaText() {
            return this.secondaryCtaText;
        }
    }

    /* compiled from: RetrieveRemoteAnnouncementsJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RetrieveRemoteAnnouncementsJob> {
        public RetrieveRemoteAnnouncementsJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new RetrieveRemoteAnnouncementsJob(data.getBoolean(RetrieveRemoteAnnouncementsJob.KEY_FORCE), parameters, null);
        }
    }
}
