package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.blurhash.BlurHashEncoder;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.service.GenericForegroundService;
import org.thoughtcrime.securesms.service.NotificationController;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.push.exceptions.ResumeLocationInvalidException;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec;

/* loaded from: classes4.dex */
public final class AttachmentUploadJob extends BaseJob {
    private static final int FOREGROUND_LIMIT;
    public static final String KEY;
    private static final String KEY_FORCE_V2;
    private static final String KEY_ROW_ID;
    private static final String KEY_UNIQUE_ID;
    private static final String TAG = Log.tag(AttachmentUploadJob.class);
    private static final long UPLOAD_REUSE_THRESHOLD = TimeUnit.DAYS.toMillis(3);
    private final AttachmentId attachmentId;
    private boolean forceV2;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public AttachmentUploadJob(AttachmentId attachmentId) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), attachmentId, false);
    }

    private AttachmentUploadJob(Job.Parameters parameters, AttachmentId attachmentId, boolean z) {
        super(parameters);
        this.attachmentId = attachmentId;
        this.forceV2 = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong(KEY_ROW_ID, this.attachmentId.getRowId()).putLong("unique_id", this.attachmentId.getUniqueId()).putBoolean(KEY_FORCE_V2, this.forceV2).build();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012e  */
    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRun() throws java.lang.Exception {
        /*
        // Method dump skipped, instructions count: 318
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.AttachmentUploadJob.onRun():void");
    }

    private NotificationController getNotificationForAttachment(Attachment attachment) {
        if (attachment.getSize() < 10485760) {
            return null;
        }
        Context context = this.context;
        return GenericForegroundService.startForegroundTask(context, context.getString(R.string.AttachmentUploadJob_uploading_media));
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        if (isCanceled()) {
            SignalDatabase.attachments().deleteAttachment(this.attachmentId);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof ResumeLocationInvalidException) && (exc instanceof IOException) && !(exc instanceof NotPushRegisteredException)) {
            return true;
        }
        return false;
    }

    private SignalServiceAttachmentStream getAttachmentFor(Attachment attachment, NotificationController notificationController, ResumableUploadSpec resumableUploadSpec) throws InvalidAttachmentException {
        if (attachment.getUri() == null || attachment.getSize() == 0) {
            throw new InvalidAttachmentException(new IOException("Assertion failed, outgoing attachment has no data!"));
        }
        try {
            SignalServiceAttachment.Builder withListener = SignalServiceAttachment.newStreamBuilder().withStream(PartAuthority.getAttachmentStream(this.context, attachment.getUri())).withContentType(attachment.getContentType()).withLength(attachment.getSize()).withFileName(attachment.getFileName()).withVoiceNote(attachment.isVoiceNote()).withBorderless(attachment.isBorderless()).withGif(attachment.isVideoGif()).withWidth(attachment.getWidth()).withHeight(attachment.getHeight()).withUploadTimestamp(System.currentTimeMillis()).withCaption(attachment.getCaption()).withCancelationSignal(new CancelationSignal() { // from class: org.thoughtcrime.securesms.jobs.AttachmentUploadJob$$ExternalSyntheticLambda0
                @Override // org.whispersystems.signalservice.internal.push.http.CancelationSignal
                public final boolean isCanceled() {
                    return AttachmentUploadJob.this.isCanceled();
                }
            }).withResumableUploadSpec(resumableUploadSpec).withListener(new SignalServiceAttachment.ProgressListener(notificationController) { // from class: org.thoughtcrime.securesms.jobs.AttachmentUploadJob$$ExternalSyntheticLambda1
                public final /* synthetic */ NotificationController f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment.ProgressListener
                public final void onAttachmentProgress(long j, long j2) {
                    AttachmentUploadJob.$r8$lambda$4It4nAMaQoDQV_GmSzImHRGjTyU(Attachment.this, this.f$1, j, j2);
                }
            });
            if (MediaUtil.isImageType(attachment.getContentType())) {
                return withListener.withBlurHash(getImageBlurHash(attachment)).build();
            }
            if (MediaUtil.isVideoType(attachment.getContentType())) {
                return withListener.withBlurHash(getVideoBlurHash(attachment)).build();
            }
            return withListener.build();
        } catch (IOException e) {
            throw new InvalidAttachmentException(e);
        }
    }

    public static /* synthetic */ void lambda$getAttachmentFor$0(Attachment attachment, NotificationController notificationController, long j, long j2) {
        EventBus.getDefault().postSticky(new PartProgressEvent(attachment, PartProgressEvent.Type.NETWORK, j, j2));
        if (notificationController != null) {
            notificationController.setProgress(j, j2);
        }
    }

    private String getImageBlurHash(Attachment attachment) throws IOException {
        if (attachment.getBlurHash() != null) {
            return attachment.getBlurHash().getHash();
        }
        if (attachment.getUri() == null) {
            return null;
        }
        InputStream attachmentStream = PartAuthority.getAttachmentStream(this.context, attachment.getUri());
        try {
            String encode = BlurHashEncoder.encode(attachmentStream);
            if (attachmentStream != null) {
                attachmentStream.close();
            }
            return encode;
        } catch (Throwable th) {
            if (attachmentStream != null) {
                try {
                    attachmentStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private String getVideoBlurHash(Attachment attachment) throws IOException {
        if (attachment.getBlurHash() != null) {
            return attachment.getBlurHash().getHash();
        }
        if (Build.VERSION.SDK_INT < 23) {
            Log.w(TAG, "Video thumbnails not supported...");
            return null;
        }
        Context context = this.context;
        Uri uri = attachment.getUri();
        Objects.requireNonNull(uri);
        Bitmap videoThumbnail = MediaUtil.getVideoThumbnail(context, uri, 1000);
        if (videoThumbnail == null) {
            return null;
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(videoThumbnail, 100, 100, false);
        videoThumbnail.recycle();
        Log.i(TAG, "Generated video thumbnail...");
        String encode = BlurHashEncoder.encode(createScaledBitmap);
        createScaledBitmap.recycle();
        return encode;
    }

    /* loaded from: classes4.dex */
    public class InvalidAttachmentException extends Exception {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        InvalidAttachmentException(String str) {
            super(str);
            AttachmentUploadJob.this = r1;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        InvalidAttachmentException(Exception exc) {
            super(exc);
            AttachmentUploadJob.this = r1;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AttachmentUploadJob> {
        public AttachmentUploadJob create(Job.Parameters parameters, Data data) {
            return new AttachmentUploadJob(parameters, new AttachmentId(data.getLong(AttachmentUploadJob.KEY_ROW_ID), data.getLong("unique_id")), data.getBooleanOrDefault(AttachmentUploadJob.KEY_FORCE_V2, false));
        }
    }
}
