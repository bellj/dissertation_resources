package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.metadata.certificate.InvalidCertificateException;
import org.signal.libsignal.metadata.certificate.SenderCertificate;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.thoughtcrime.securesms.TextSecureExpiredException;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactModelMapper;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.BackoffUtil;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.SubmitRateLimitPushChallengeJob;
import org.thoughtcrime.securesms.keyvalue.CertificateType;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.BitmapDecodingException;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServicePreview;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public abstract class PushSendJob extends SendJob {
    private static final long CERTIFICATE_EXPIRATION_BUFFER = TimeUnit.DAYS.toMillis(1);
    private static final long PUSH_CHALLENGE_TIMEOUT = TimeUnit.SECONDS.toMillis(10);
    private static final String TAG = Log.tag(PushSendJob.class);

    public static /* synthetic */ boolean lambda$getAttachmentPointersFor$2(SignalServiceAttachment signalServiceAttachment) {
        return signalServiceAttachment != null;
    }

    protected abstract void onPushSend() throws Exception;

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public PushSendJob(Job.Parameters parameters) {
        super(parameters);
    }

    public static Job.Parameters constructParameters(Recipient recipient, boolean z) {
        return new Job.Parameters.Builder().setQueue(recipient.getId().toQueueKey(z)).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.SendJob
    protected final void onSend() throws Exception {
        if (SignalStore.account().aciPreKeys().getSignedPreKeyFailureCount() > 5) {
            ApplicationDependencies.getJobManager().add(new RotateSignedPreKeyJob());
            throw new TextSecureExpiredException("Too many signed prekey rotation failures");
        } else if (Recipient.self().isRegistered()) {
            onPushSend();
            if (SignalStore.rateLimit().needsRecaptcha()) {
                Log.i(TAG, "Successfully sent message. Assuming reCAPTCHA no longer needed.");
                SignalStore.rateLimit().onProofAccepted();
            }
        } else {
            throw new NotPushRegisteredException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onRetry() {
        String str = TAG;
        Log.i(str, "onRetry()");
        if (getRunAttempt() > 1) {
            Log.i(str, "Scheduling service outage detection job.");
            ApplicationDependencies.getJobManager().add(new ServiceOutageDetectionJob());
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if ((exc instanceof ServerRejectedException) || (exc instanceof NotPushRegisteredException)) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException) || (exc instanceof ProofRequiredException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public long getNextRunAttemptBackoff(int i, Exception exc) {
        if (exc instanceof ProofRequiredException) {
            long retryAfterSeconds = ((ProofRequiredException) exc).getRetryAfterSeconds();
            String str = TAG;
            warn(str, "[Proof Required] Retry-After is " + retryAfterSeconds + " seconds.");
            if (retryAfterSeconds >= 0) {
                return TimeUnit.SECONDS.toMillis(retryAfterSeconds);
            }
        } else if (exc instanceof NonSuccessfulResponseCodeException) {
            if (((NonSuccessfulResponseCodeException) exc).is5xx()) {
                return BackoffUtil.exponentialBackoff(i, FeatureFlags.getServerErrorMaxBackoff());
            }
        } else if (exc instanceof RetryLaterException) {
            long backoff = ((RetryLaterException) exc).getBackoff();
            if (backoff >= 0) {
                return backoff;
            }
        }
        return super.getNextRunAttemptBackoff(i, exc);
    }

    public Optional<byte[]> getProfileKey(Recipient recipient) {
        if (recipient.resolve().isSystemContact() || recipient.resolve().isProfileSharing()) {
            return Optional.of(ProfileKeyUtil.getSelfProfileKey().serialize());
        }
        return Optional.empty();
    }

    protected SignalServiceAttachment getAttachmentFor(Attachment attachment) {
        try {
            if (attachment.getUri() == null || attachment.getSize() == 0) {
                throw new IOException("Assertion failed, outgoing attachment has no data!");
            }
            return SignalServiceAttachment.newStreamBuilder().withStream(PartAuthority.getAttachmentStream(this.context, attachment.getUri())).withContentType(attachment.getContentType()).withLength(attachment.getSize()).withFileName(attachment.getFileName()).withVoiceNote(attachment.isVoiceNote()).withBorderless(attachment.isBorderless()).withGif(attachment.isVideoGif()).withWidth(attachment.getWidth()).withHeight(attachment.getHeight()).withCaption(attachment.getCaption()).withListener(new SignalServiceAttachment.ProgressListener() { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda0
                @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment.ProgressListener
                public final void onAttachmentProgress(long j, long j2) {
                    PushSendJob.lambda$getAttachmentFor$0(Attachment.this, j, j2);
                }
            }).build();
        } catch (IOException e) {
            Log.w(TAG, "Couldn't open attachment", e);
            return null;
        }
    }

    public static /* synthetic */ void lambda$getAttachmentFor$0(Attachment attachment, long j, long j2) {
        EventBus.getDefault().postSticky(new PartProgressEvent(attachment, PartProgressEvent.Type.NETWORK, j, j2));
    }

    public static Set<String> enqueueCompressingAndUploadAttachmentsChains(JobManager jobManager, OutgoingMediaMessage outgoingMediaMessage) {
        LinkedList linkedList = new LinkedList();
        linkedList.addAll(outgoingMediaMessage.getAttachments());
        linkedList.addAll(Stream.of(outgoingMediaMessage.getLinkPreviews()).map(new PushSendJob$$ExternalSyntheticLambda2()).filter(new UnidentifiedAccessUtil$$ExternalSyntheticLambda0()).map(new PushSendJob$$ExternalSyntheticLambda3()).toList());
        linkedList.addAll(Stream.of(outgoingMediaMessage.getSharedContacts()).map(new PushSendJob$$ExternalSyntheticLambda4()).withoutNulls().map(new PushSendJob$$ExternalSyntheticLambda5()).withoutNulls().toList());
        return new HashSet(Stream.of(linkedList).map(new Function(jobManager) { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda6
            public final /* synthetic */ JobManager f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushSendJob.lambda$enqueueCompressingAndUploadAttachmentsChains$1(OutgoingMediaMessage.this, this.f$1, (Attachment) obj);
            }
        }).toList());
    }

    public static /* synthetic */ String lambda$enqueueCompressingAndUploadAttachmentsChains$1(OutgoingMediaMessage outgoingMediaMessage, JobManager jobManager, Attachment attachment) {
        DatabaseAttachment databaseAttachment = (DatabaseAttachment) attachment;
        AttachmentUploadJob attachmentUploadJob = new AttachmentUploadJob(databaseAttachment.getAttachmentId());
        if (outgoingMediaMessage.isGroup()) {
            jobManager.startChain(AttachmentCompressionJob.fromAttachment(databaseAttachment, false, -1)).then(attachmentUploadJob).enqueue();
        } else {
            jobManager.startChain(AttachmentCompressionJob.fromAttachment(databaseAttachment, false, -1)).then(new ResumableUploadSpecJob()).then(attachmentUploadJob).enqueue();
        }
        return attachmentUploadJob.getId();
    }

    public List<SignalServiceAttachment> getAttachmentPointersFor(List<Attachment> list) {
        return Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushSendJob.this.getAttachmentPointerFor((Attachment) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda10
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushSendJob.lambda$getAttachmentPointersFor$2((SignalServiceAttachment) obj);
            }
        }).toList();
    }

    public SignalServiceAttachment getAttachmentPointerFor(Attachment attachment) {
        Bitmap videoThumbnail;
        if (TextUtils.isEmpty(attachment.getLocation())) {
            Log.w(TAG, "empty content id");
            return null;
        } else if (TextUtils.isEmpty(attachment.getKey())) {
            Log.w(TAG, "empty encrypted key");
            return null;
        } else {
            try {
                SignalServiceAttachmentRemoteId from = SignalServiceAttachmentRemoteId.from(attachment.getLocation());
                byte[] decode = Base64.decode(attachment.getKey());
                int width = attachment.getWidth();
                int height = attachment.getHeight();
                if ((width == 0 || height == 0) && MediaUtil.hasVideoThumbnail(this.context, attachment.getUri()) && (videoThumbnail = MediaUtil.getVideoThumbnail(this.context, attachment.getUri(), 1000)) != null) {
                    width = videoThumbnail.getWidth();
                    height = videoThumbnail.getHeight();
                }
                return new SignalServiceAttachmentPointer(attachment.getCdnNumber(), from, attachment.getContentType(), decode, Optional.of(Integer.valueOf(Util.toIntExact(attachment.getSize()))), Optional.empty(), width, height, Optional.ofNullable(attachment.getDigest()), Optional.ofNullable(attachment.getFileName()), attachment.isVoiceNote(), attachment.isBorderless(), attachment.isVideoGif(), Optional.ofNullable(attachment.getCaption()), Optional.ofNullable(attachment.getBlurHash()).map(new AttachmentDownloadJob$$ExternalSyntheticLambda0()), attachment.getUploadTimestamp());
            } catch (IOException | ArithmeticException e) {
                Log.w(TAG, e);
                return null;
            }
        }
    }

    public static void notifyMediaMessageDeliveryFailed(Context context, long j) {
        long threadIdForMessage = SignalDatabase.mms().getThreadIdForMessage(j);
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(threadIdForMessage);
        ParentStoryId.GroupReply parentStoryIdForGroupReply = SignalDatabase.mms().getParentStoryIdForGroupReply(j);
        if (threadIdForMessage != -1 && recipientForThreadId != null) {
            ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(context, recipientForThreadId, ConversationId.fromThreadAndReply(threadIdForMessage, parentStoryIdForGroupReply));
        }
    }

    public Optional<SignalServiceDataMessage.Quote> getQuoteFor(OutgoingMediaMessage outgoingMediaMessage) throws IOException {
        BitmapUtil.ScaleResult scaleResult;
        Bitmap videoThumbnail;
        if (outgoingMediaMessage.getOutgoingQuote() == null) {
            return Optional.empty();
        }
        long id = outgoingMediaMessage.getOutgoingQuote().getId();
        String text = outgoingMediaMessage.getOutgoingQuote().getText();
        RecipientId author = outgoingMediaMessage.getOutgoingQuote().getAuthor();
        List<SignalServiceDataMessage.Mention> mentionsFor = getMentionsFor(outgoingMediaMessage.getOutgoingQuote().getMentions());
        QuoteModel.Type type = outgoingMediaMessage.getOutgoingQuote().getType();
        LinkedList linkedList = new LinkedList();
        for (Attachment attachment : Stream.of(outgoingMediaMessage.getOutgoingQuote().getAttachments()).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushSendJob.lambda$getQuoteFor$3((Attachment) obj);
            }
        }).toList()) {
            try {
                boolean isImageType = MediaUtil.isImageType(attachment.getContentType());
                SignalServiceAttachmentStream signalServiceAttachmentStream = null;
                String str = MediaUtil.IMAGE_JPEG;
                if (!isImageType || attachment.getUri() == null) {
                    scaleResult = (Build.VERSION.SDK_INT < 23 || !MediaUtil.isVideoType(attachment.getContentType()) || attachment.getUri() == null || (videoThumbnail = MediaUtil.getVideoThumbnail(this.context, attachment.getUri(), 1000)) == null) ? null : BitmapUtil.createScaledBytes(this.context, videoThumbnail, 100, 100, 512000);
                } else {
                    scaleResult = BitmapUtil.createScaledBytes(this.context, new DecryptableStreamUriLoader.DecryptableUri(attachment.getUri()), 100, 100, 512000, BitmapUtil.getCompressFormatForContentType(attachment.getContentType()));
                    str = attachment.getContentType();
                }
                if (scaleResult != null) {
                    signalServiceAttachmentStream = SignalServiceAttachment.newStreamBuilder().withContentType(str).withWidth(scaleResult.getWidth()).withHeight(scaleResult.getHeight()).withLength((long) scaleResult.getBitmap().length).withStream(new ByteArrayInputStream(scaleResult.getBitmap())).withResumableUploadSpec(ApplicationDependencies.getSignalServiceMessageSender().getResumableUploadSpec()).build();
                }
                linkedList.add(new SignalServiceDataMessage.Quote.QuotedAttachment(attachment.isVideoGif() ? MediaUtil.IMAGE_GIF : attachment.getContentType(), attachment.getFileName(), signalServiceAttachmentStream));
            } catch (BitmapDecodingException e) {
                Log.w(TAG, e);
            }
        }
        Recipient resolved = Recipient.resolved(author);
        if (resolved.isMaybeRegistered()) {
            return Optional.of(new SignalServiceDataMessage.Quote(id, RecipientUtil.getOrFetchServiceId(this.context, resolved), text, linkedList, mentionsFor, type.getDataMessageType()));
        }
        return Optional.empty();
    }

    public static /* synthetic */ boolean lambda$getQuoteFor$3(Attachment attachment) {
        return MediaUtil.isViewOnceType(attachment.getContentType());
    }

    public Optional<SignalServiceDataMessage.Sticker> getStickerFor(OutgoingMediaMessage outgoingMediaMessage) {
        String str = null;
        Attachment attachment = (Attachment) Stream.of(outgoingMediaMessage.getAttachments()).filter(new PushDistributionListSendJob$$ExternalSyntheticLambda0()).findFirst().orElse(null);
        if (attachment == null) {
            return Optional.empty();
        }
        try {
            byte[] fromStringCondensed = Hex.fromStringCondensed(attachment.getSticker().getPackId());
            byte[] fromStringCondensed2 = Hex.fromStringCondensed(attachment.getSticker().getPackKey());
            int stickerId = attachment.getSticker().getStickerId();
            StickerRecord sticker = SignalDatabase.stickers().getSticker(attachment.getSticker().getPackId(), stickerId, false);
            if (sticker != null) {
                str = sticker.getEmoji();
            }
            return Optional.of(new SignalServiceDataMessage.Sticker(fromStringCondensed, fromStringCondensed2, stickerId, str, getAttachmentPointerFor(attachment)));
        } catch (IOException e) {
            Log.w(TAG, "Failed to decode sticker id/key", e);
            return Optional.empty();
        }
    }

    public Optional<SignalServiceDataMessage.Reaction> getStoryReactionFor(OutgoingMediaMessage outgoingMediaMessage, SignalServiceDataMessage.StoryContext storyContext) {
        if (outgoingMediaMessage.isStoryReaction()) {
            return Optional.of(new SignalServiceDataMessage.Reaction(outgoingMediaMessage.getBody(), false, storyContext.getAuthorServiceId(), storyContext.getSentTimestamp()));
        }
        return Optional.empty();
    }

    public List<SharedContact> getSharedContactsFor(OutgoingMediaMessage outgoingMediaMessage) {
        LinkedList linkedList = new LinkedList();
        for (Contact contact : outgoingMediaMessage.getSharedContacts()) {
            SharedContact.Builder localToRemoteBuilder = ContactModelMapper.localToRemoteBuilder(contact);
            SharedContact.Avatar avatar = null;
            if (!(contact.getAvatar() == null || contact.getAvatar().getAttachment() == null)) {
                avatar = SharedContact.Avatar.newBuilder().withAttachment(getAttachmentFor(contact.getAvatarAttachment())).withProfileFlag(contact.getAvatar().isProfile()).build();
            }
            localToRemoteBuilder.setAvatar(avatar);
            linkedList.add(localToRemoteBuilder.build());
        }
        return linkedList;
    }

    public List<SignalServicePreview> getPreviewsFor(OutgoingMediaMessage outgoingMediaMessage) {
        return Stream.of(outgoingMediaMessage.getLinkPreviews()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushSendJob.this.lambda$getPreviewsFor$4((LinkPreview) obj);
            }
        }).toList();
    }

    public /* synthetic */ SignalServicePreview lambda$getPreviewsFor$4(LinkPreview linkPreview) {
        return new SignalServicePreview(linkPreview.getUrl(), linkPreview.getTitle(), linkPreview.getDescription(), linkPreview.getDate(), Optional.ofNullable(linkPreview.getThumbnail().isPresent() ? getAttachmentPointerFor(linkPreview.getThumbnail().get()) : null));
    }

    public List<SignalServiceDataMessage.Mention> getMentionsFor(List<Mention> list) {
        return Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushSendJob.lambda$getMentionsFor$5((Mention) obj);
            }
        }).toList();
    }

    public static /* synthetic */ SignalServiceDataMessage.Mention lambda$getMentionsFor$5(Mention mention) {
        return new SignalServiceDataMessage.Mention(Recipient.resolved(mention.getRecipientId()).requireServiceId(), mention.getStart(), mention.getLength());
    }

    public SignalServiceDataMessage.GiftBadge getGiftBadgeFor(OutgoingMediaMessage outgoingMediaMessage) throws UndeliverableMessageException {
        GiftBadge giftBadge = outgoingMediaMessage.getGiftBadge();
        if (giftBadge == null) {
            return null;
        }
        try {
            return new SignalServiceDataMessage.GiftBadge(new ReceiptCredentialPresentation(giftBadge.getRedemptionToken().toByteArray()));
        } catch (InvalidInputException e) {
            throw new UndeliverableMessageException(e);
        }
    }

    public void rotateSenderCertificateIfNecessary() throws IOException {
        try {
            Collection<CertificateType> requiredCertificateTypes = SignalStore.phoneNumberPrivacy().getRequiredCertificateTypes();
            String str = TAG;
            Log.i(str, "Ensuring we have these certificates " + requiredCertificateTypes);
            for (CertificateType certificateType : requiredCertificateTypes) {
                byte[] unidentifiedAccessCertificate = SignalStore.certificateValues().getUnidentifiedAccessCertificate(certificateType);
                if (unidentifiedAccessCertificate != null) {
                    SenderCertificate senderCertificate = new SenderCertificate(unidentifiedAccessCertificate);
                    if (System.currentTimeMillis() <= senderCertificate.getExpiration() - CERTIFICATE_EXPIRATION_BUFFER) {
                        Log.d(TAG, String.format("Certificate %s is valid", certificateType));
                    } else {
                        throw new InvalidCertificateException(String.format(Locale.US, "Certificate %s is expired, or close to it. Expires on: %d, currently: %d", certificateType, Long.valueOf(senderCertificate.getExpiration()), Long.valueOf(System.currentTimeMillis())));
                    }
                } else {
                    throw new InvalidCertificateException(String.format("No certificate %s was present.", certificateType));
                }
            }
            Log.d(TAG, "All certificates are valid.");
        } catch (InvalidCertificateException e) {
            Log.w(TAG, "A certificate was invalid at send time. Fetching new ones.", e);
            if (!ApplicationDependencies.getJobManager().runSynchronously(new RotateCertificateJob(), 5000).isPresent()) {
                throw new IOException("Timeout rotating certificate");
            }
        }
    }

    public static void handleProofRequiredException(Context context, ProofRequiredException proofRequiredException, Recipient recipient, long j, long j2, boolean z) throws ProofRequiredException, RetryLaterException {
        String str = TAG;
        Log.w(str, "[Proof Required] Options: " + proofRequiredException.getOptions());
        try {
            if (proofRequiredException.getOptions().contains(ProofRequiredException.Option.PUSH_CHALLENGE)) {
                ApplicationDependencies.getSignalServiceAccountManager().requestRateLimitPushChallenge();
                StringBuilder sb = new StringBuilder();
                sb.append("[Proof Required] Successfully requested a challenge. Waiting up to ");
                long j3 = PUSH_CHALLENGE_TIMEOUT;
                sb.append(j3);
                sb.append(" ms.");
                Log.i(str, sb.toString());
                if (!new PushChallengeRequest(j3).blockUntilSuccess()) {
                    Log.w(str, "Failed to respond to the push challenge in time. Falling back.");
                } else {
                    Log.i(str, "Successfully responded to a push challenge. Retrying message send.");
                    throw new RetryLaterException(1);
                }
            }
        } catch (NonSuccessfulResponseCodeException e) {
            String str2 = TAG;
            Log.w(str2, "[Proof Required] Could not request a push challenge (" + e.getCode() + "). Falling back.", e);
        } catch (IOException e2) {
            Log.w(TAG, "[Proof Required] Network error when requesting push challenge. Retrying later.");
            throw new RetryLaterException(e2);
        }
        String str3 = TAG;
        Log.w(str3, "[Proof Required] Marking message as rate-limited. (id: " + j2 + ", mms: " + z + ", thread: " + j + ")");
        if (z) {
            SignalDatabase.mms().markAsRateLimited(j2);
        } else {
            SignalDatabase.sms().markAsRateLimited(j2);
        }
        if (proofRequiredException.getOptions().contains(ProofRequiredException.Option.RECAPTCHA)) {
            Log.i(str3, "[Proof Required] ReCAPTCHA required.");
            SignalStore.rateLimit().markNeedsRecaptcha(proofRequiredException.getToken());
            if (recipient != null) {
                ApplicationDependencies.getMessageNotifier().notifyProofRequired(context, recipient, ConversationId.fromThreadAndReply(j, SignalDatabase.mms().getParentStoryIdForGroupReply(j2)));
            } else {
                Log.w(str3, "[Proof Required] No recipient! Couldn't notify.");
            }
        }
        throw proofRequiredException;
    }

    /* loaded from: classes.dex */
    public static class PushChallengeRequest {
        private final EventBus eventBus;
        private final CountDownLatch latch;
        private final long timeout;

        private PushChallengeRequest(long j) {
            this.timeout = j;
            this.latch = new CountDownLatch(1);
            this.eventBus = EventBus.getDefault();
        }

        public boolean blockUntilSuccess() {
            boolean z;
            try {
                this.eventBus.register(this);
                try {
                    z = this.latch.await(this.timeout, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    Log.w(PushSendJob.TAG, "[Proof Required] Interrupted?", e);
                    z = false;
                }
                return z;
            } finally {
                this.eventBus.unregister(this);
            }
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onSuccessReceived(SubmitRateLimitPushChallengeJob.SuccessEvent successEvent) {
            Log.i(PushSendJob.TAG, "[Proof Required] Received a successful result!");
            this.latch.countDown();
        }
    }
}
