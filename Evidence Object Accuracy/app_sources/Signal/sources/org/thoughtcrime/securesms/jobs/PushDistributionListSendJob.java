package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.GroupDatabase$GroupRecord$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SentStorySyncManifest;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobLogger;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.messages.StorySendUtil;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public final class PushDistributionListSendJob extends PushSendJob {
    public static final String KEY;
    private static final String KEY_FILTERED_RECIPIENT_IDS;
    private static final String KEY_MESSAGE_ID;
    private static final String TAG = Log.tag(PushDistributionListSendJob.class);
    private final Set<RecipientId> filterRecipientIds;
    private final long messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public PushDistributionListSendJob(long j, RecipientId recipientId, boolean z, Set<RecipientId> set) {
        this(new Job.Parameters.Builder().setQueue(recipientId.toQueueKey(z)).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), j, set);
    }

    private PushDistributionListSendJob(Job.Parameters parameters, long j, Set<RecipientId> set) {
        super(parameters);
        this.messageId = j;
        this.filterRecipientIds = set;
    }

    public static void enqueue(Context context, JobManager jobManager, long j, RecipientId recipientId, Set<RecipientId> set) {
        try {
            if (Recipient.resolved(recipientId).isDistributionList()) {
                OutgoingMediaMessage outgoingMessage = SignalDatabase.mms().getOutgoingMessage(j);
                if (outgoingMessage.getStoryType().isStory()) {
                    if (!outgoingMessage.getStoryType().isTextStory()) {
                        SignalDatabase.attachments().updateAttachmentCaption(((DatabaseAttachment) outgoingMessage.getAttachments().get(0)).getAttachmentId(), outgoingMessage.getBody());
                    }
                    Set<String> enqueueCompressingAndUploadAttachmentsChains = PushSendJob.enqueueCompressingAndUploadAttachmentsChains(jobManager, outgoingMessage);
                    jobManager.add(new PushDistributionListSendJob(j, recipientId, !enqueueCompressingAndUploadAttachmentsChains.isEmpty(), set), enqueueCompressingAndUploadAttachmentsChains, enqueueCompressingAndUploadAttachmentsChains.isEmpty() ? null : recipientId.toQueueKey());
                    return;
                }
                throw new AssertionError("Only story messages are currently supported! MessageId: " + j);
            }
            throw new AssertionError("Not a distribution list! MessageId: " + j);
        } catch (NoSuchMessageException | MmsException e) {
            Log.w(TAG, "Failed to enqueue message.", e);
            SignalDatabase.mms().markAsSentFailed(j);
            PushSendJob.notifyMediaMessageDeliveryFailed(context, j);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putString(KEY_FILTERED_RECIPIENT_IDS, RecipientId.toSerializedList(this.filterRecipientIds)).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        SignalDatabase.mms().markAsSending(this.messageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.PushSendJob
    public void onPushSend() throws IOException, MmsException, NoSuchMessageException, RetryLaterException {
        List<Recipient> list;
        MmsDatabase mms = SignalDatabase.mms();
        OutgoingMediaMessage outgoingMessage = mms.getOutgoingMessage(this.messageId);
        Set<NetworkFailure> networkFailures = outgoingMessage.getNetworkFailures();
        Set<IdentityKeyMismatch> identityKeyMismatches = outgoingMessage.getIdentityKeyMismatches();
        if (!outgoingMessage.getStoryType().isStory()) {
            throw new MmsException("Only story sends are currently supported!");
        } else if (mms.isSent(this.messageId)) {
            String str = TAG;
            String valueOf = String.valueOf(outgoingMessage.getSentTimeMillis());
            log(str, valueOf, "Message " + this.messageId + " was already sent. Ignoring.");
        } else if (outgoingMessage.getRecipient().resolve().isDistributionList()) {
            try {
                String str2 = TAG;
                String valueOf2 = String.valueOf(outgoingMessage.getSentTimeMillis());
                log(str2, valueOf2, "Sending message: " + this.messageId + ", Recipient: " + outgoingMessage.getRecipient().getId() + ", Attachments: " + buildAttachmentString(outgoingMessage.getAttachments()));
                if (Util.hasItems(this.filterRecipientIds)) {
                    list = new ArrayList<>(this.filterRecipientIds.size() + networkFailures.size());
                    list.addAll((Collection) Collection$EL.stream(this.filterRecipientIds).map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).collect(Collectors.toList()));
                    list.addAll((Collection) Collection$EL.stream(networkFailures).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushDistributionListSendJob$$ExternalSyntheticLambda3
                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return PushDistributionListSendJob.this.lambda$onPushSend$0((NetworkFailure) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }).distinct().map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).collect(Collectors.toList()));
                } else if (!networkFailures.isEmpty()) {
                    list = Stream.of(networkFailures).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.jobs.PushDistributionListSendJob$$ExternalSyntheticLambda4
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return PushDistributionListSendJob.this.lambda$onPushSend$1((NetworkFailure) obj);
                        }
                    }).distinct().map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList();
                } else {
                    list = Stream.of(Stories.getRecipientsToSendTo(this.messageId, outgoingMessage.getSentTimeMillis(), outgoingMessage.getStoryType().isStoryWithReplies())).distinctBy(new ContactUtil$$ExternalSyntheticLambda3()).toList();
                }
                List<SendMessageResult> deliver = deliver(outgoingMessage, list);
                Log.i(str2, JobLogger.format(this, "Finished send."));
                PushGroupSendJob.processGroupMessageResults(this.context, this.messageId, -1, null, outgoingMessage, deliver, list, Collections.emptyList(), networkFailures, identityKeyMismatches);
            } catch (UndeliverableMessageException | UntrustedIdentityException e) {
                warn(TAG, String.valueOf(outgoingMessage.getSentTimeMillis()), e);
                mms.markAsSentFailed(this.messageId);
                PushSendJob.notifyMediaMessageDeliveryFailed(this.context, this.messageId);
            }
        } else {
            throw new MmsException("Message recipient isn't a distribution list!");
        }
    }

    public /* synthetic */ RecipientId lambda$onPushSend$0(NetworkFailure networkFailure) {
        return networkFailure.getRecipientId(this.context);
    }

    public /* synthetic */ RecipientId lambda$onPushSend$1(NetworkFailure networkFailure) {
        return networkFailure.getRecipientId(this.context);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        SignalDatabase.mms().markAsSentFailed(this.messageId);
    }

    private List<SendMessageResult> deliver(OutgoingMediaMessage outgoingMediaMessage, List<Recipient> list) throws IOException, UntrustedIdentityException, UndeliverableMessageException {
        SignalServiceStoryMessage forFileAttachment;
        try {
            rotateSenderCertificateIfNecessary();
            List<SignalServiceAttachment> attachmentPointersFor = getAttachmentPointersFor(Stream.of(outgoingMediaMessage.getAttachments()).filterNot(new PushDistributionListSendJob$$ExternalSyntheticLambda0()).toList());
            boolean anyMatch = Stream.of(SignalDatabase.groupReceipts().getGroupReceiptInfo(this.messageId)).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushDistributionListSendJob$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return PushDistributionListSendJob.lambda$deliver$2((GroupReceiptDatabase.GroupReceiptInfo) obj);
                }
            });
            if (outgoingMediaMessage.getStoryType().isTextStory()) {
                forFileAttachment = SignalServiceStoryMessage.forTextAttachment(Recipient.self().getProfileKey(), null, StorySendUtil.deserializeBodyToStoryTextAttachment(outgoingMediaMessage, new Function1() { // from class: org.thoughtcrime.securesms.jobs.PushDistributionListSendJob$$ExternalSyntheticLambda2
                    @Override // kotlin.jvm.functions.Function1
                    public final Object invoke(Object obj) {
                        return PushDistributionListSendJob.this.getPreviewsFor((OutgoingMediaMessage) obj);
                    }
                }), outgoingMediaMessage.getStoryType().isStoryWithReplies());
            } else if (!attachmentPointersFor.isEmpty()) {
                forFileAttachment = SignalServiceStoryMessage.forFileAttachment(Recipient.self().getProfileKey(), null, attachmentPointersFor.get(0), outgoingMediaMessage.getStoryType().isStoryWithReplies());
            } else {
                throw new UndeliverableMessageException("No attachment on non-text story.");
            }
            SentStorySyncManifest fullSentStorySyncManifest = SignalDatabase.storySends().getFullSentStorySyncManifest(this.messageId, outgoingMediaMessage.getSentTimeMillis());
            return GroupSendUtil.sendStoryMessage(this.context, outgoingMediaMessage.getRecipient().requireDistributionListId(), list, anyMatch, new MessageId(this.messageId, true), outgoingMediaMessage.getSentTimeMillis(), forFileAttachment, fullSentStorySyncManifest != null ? fullSentStorySyncManifest.toRecipientsSet() : Collections.emptySet());
        } catch (ServerRejectedException e) {
            throw new UndeliverableMessageException(e);
        }
    }

    public static /* synthetic */ boolean lambda$deliver$2(GroupReceiptDatabase.GroupReceiptInfo groupReceiptInfo) {
        return groupReceiptInfo.getStatus() > 0;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PushDistributionListSendJob> {
        public PushDistributionListSendJob create(Job.Parameters parameters, Data data) {
            return new PushDistributionListSendJob(parameters, data.getLong("message_id"), new HashSet(RecipientId.fromSerializedList(data.getStringOrDefault(PushDistributionListSendJob.KEY_FILTERED_RECIPIENT_IDS, ""))));
        }
    }
}
