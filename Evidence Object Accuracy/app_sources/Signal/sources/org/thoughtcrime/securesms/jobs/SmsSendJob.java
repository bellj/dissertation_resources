package org.thoughtcrime.securesms.jobs;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsManager;
import java.util.ArrayList;
import java.util.Iterator;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkOrCellServiceConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.SmsDeliveryListener;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;

/* loaded from: classes4.dex */
public class SmsSendJob extends SendJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_RUN_ATTEMPT;
    private static final int MAX_ATTEMPTS;
    private static final String TAG = Log.tag(SmsSendJob.class);
    private final long messageId;
    private final int runAttempt;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public SmsSendJob(long j, Recipient recipient) {
        this(j, recipient, 0);
    }

    public SmsSendJob(long j, Recipient recipient, int i) {
        this(constructParameters(recipient), j, i);
    }

    private SmsSendJob(Job.Parameters parameters, long j, int i) {
        super(parameters);
        this.messageId = j;
        this.runAttempt = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putInt(KEY_RUN_ATTEMPT, this.runAttempt).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        SignalDatabase.sms().markAsSending(this.messageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.SendJob
    public void onSend() throws NoSuchMessageException, TooManyRetriesException, UndeliverableMessageException {
        if (this.runAttempt < 15) {
            SmsMessageRecord smsMessage = SignalDatabase.sms().getSmsMessage(this.messageId);
            if (!smsMessage.isPending() && !smsMessage.isFailed()) {
                String str = TAG;
                warn(str, "Message " + this.messageId + " was already sent. Ignoring.");
            } else if (smsMessage.getRecipient().hasSmsAddress()) {
                try {
                    String str2 = TAG;
                    String valueOf = String.valueOf(smsMessage.getDateSent());
                    log(str2, valueOf, "Sending message: " + this.messageId + " (attempt " + this.runAttempt + ")");
                    deliver(smsMessage);
                    String valueOf2 = String.valueOf(smsMessage.getDateSent());
                    StringBuilder sb = new StringBuilder();
                    sb.append("Sent message: ");
                    sb.append(this.messageId);
                    log(str2, valueOf2, sb.toString());
                } catch (UndeliverableMessageException e) {
                    warn(TAG, e);
                    SignalDatabase.sms().markAsSentFailed(smsMessage.getId());
                    ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(this.context, smsMessage.getRecipient(), ConversationId.fromMessageRecord(smsMessage));
                }
            } else {
                throw new UndeliverableMessageException("Recipient didn't have an SMS address! " + smsMessage.getRecipient().getId());
            }
        } else {
            warn(TAG, "Hit the retry limit. Failing.");
            throw new TooManyRetriesException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        warn(str, "onFailure() messageId: " + this.messageId);
        long threadIdForMessage = SignalDatabase.sms().getThreadIdForMessage(this.messageId);
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(threadIdForMessage);
        SignalDatabase.sms().markAsSentFailed(this.messageId);
        if (threadIdForMessage == -1 || recipientForThreadId == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Could not find message! threadId: ");
            sb.append(threadIdForMessage);
            sb.append(", recipient: ");
            sb.append(recipientForThreadId != null ? recipientForThreadId.getId().toString() : "null");
            Log.w(str, sb.toString());
            return;
        }
        ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(this.context, recipientForThreadId, ConversationId.forConversation(threadIdForMessage));
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00bd A[Catch: NullPointerException -> 0x00e3, IllegalArgumentException | NullPointerException -> 0x00e1, TryCatch #4 {IllegalArgumentException | NullPointerException -> 0x00e1, blocks: (B:21:0x00b7, B:23:0x00bd, B:27:0x00d1, B:28:0x00d8), top: B:38:0x00b7 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void deliver(org.thoughtcrime.securesms.database.model.SmsMessageRecord r14) throws org.thoughtcrime.securesms.transport.UndeliverableMessageException {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.SmsSendJob.deliver(org.thoughtcrime.securesms.database.model.SmsMessageRecord):void");
    }

    private ArrayList<PendingIntent> constructSentIntents(long j, long j2, ArrayList<String> arrayList) {
        ArrayList<PendingIntent> arrayList2 = new ArrayList<>(arrayList.size());
        boolean z = true;
        if (arrayList.size() <= 1) {
            z = false;
        }
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next();
            Context context = this.context;
            arrayList2.add(PendingIntent.getBroadcast(context, 0, constructSentIntent(context, j, j2, z), 0));
        }
        return arrayList2;
    }

    private ArrayList<PendingIntent> constructDeliveredIntents(long j, long j2, ArrayList<String> arrayList) {
        if (!SignalStore.settings().isSmsDeliveryReportsEnabled()) {
            return null;
        }
        ArrayList<PendingIntent> arrayList2 = new ArrayList<>(arrayList.size());
        boolean z = true;
        if (arrayList.size() <= 1) {
            z = false;
        }
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next();
            Context context = this.context;
            arrayList2.add(PendingIntent.getBroadcast(context, 0, constructDeliveredIntent(context, j, j2, z), 0));
        }
        return arrayList2;
    }

    private Intent constructSentIntent(Context context, long j, long j2, boolean z) {
        Intent intent = new Intent(SmsDeliveryListener.SENT_SMS_ACTION, Uri.parse("custom://" + j + System.currentTimeMillis()), context, SmsDeliveryListener.class);
        intent.putExtra("type", j2);
        intent.putExtra("message_id", j);
        intent.putExtra(KEY_RUN_ATTEMPT, Math.max(this.runAttempt, getRunAttempt()));
        intent.putExtra("is_multipart", z);
        return intent;
    }

    private Intent constructDeliveredIntent(Context context, long j, long j2, boolean z) {
        Intent intent = new Intent(SmsDeliveryListener.DELIVERED_SMS_ACTION, Uri.parse("custom://" + j + System.currentTimeMillis()), context, SmsDeliveryListener.class);
        intent.putExtra("type", j2);
        intent.putExtra("message_id", j);
        intent.putExtra("is_multipart", z);
        return intent;
    }

    private SmsManager getSmsManagerFor(int i) {
        if (Build.VERSION.SDK_INT < 22 || i == -1) {
            return SmsManager.getDefault();
        }
        return SmsManager.getSmsManagerForSubscriptionId(i);
    }

    private static Job.Parameters constructParameters(Recipient recipient) {
        Job.Parameters.Builder maxAttempts = new Job.Parameters.Builder().setMaxAttempts(15);
        return maxAttempts.setQueue(recipient.getId().toQueueKey() + "::SMS").addConstraint(NetworkOrCellServiceConstraint.KEY).build();
    }

    /* loaded from: classes4.dex */
    private static class TooManyRetriesException extends Exception {
        private TooManyRetriesException() {
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<SmsSendJob> {
        public SmsSendJob create(Job.Parameters parameters, Data data) {
            return new SmsSendJob(parameters, data.getLong("message_id"), data.getInt(SmsSendJob.KEY_RUN_ATTEMPT));
        }
    }
}
