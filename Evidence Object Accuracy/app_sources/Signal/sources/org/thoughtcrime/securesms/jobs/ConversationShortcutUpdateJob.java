package org.thoughtcrime.securesms.jobs;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ConversationShortcutUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(ConversationShortcutUpdateJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public static void enqueue() {
        ApplicationDependencies.getJobManager().add(new ConversationShortcutUpdateJob());
    }

    private ConversationShortcutUpdateJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).setLifespan(TimeUnit.MINUTES.toMillis(15)).setMaxInstancesForFactory(1).build());
    }

    private ConversationShortcutUpdateJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (TextSecurePreferences.isScreenLockEnabled(this.context)) {
            Log.i(TAG, "Screen lock enabled. Clearing shortcuts.");
            ConversationUtil.clearAllShortcuts(this.context);
            return;
        }
        ThreadDatabase threads = SignalDatabase.threads();
        int maxShortcuts = ConversationUtil.getMaxShortcuts(this.context);
        ArrayList arrayList = new ArrayList(maxShortcuts);
        ThreadDatabase.Reader readerFor = threads.readerFor(threads.getRecentConversationList(maxShortcuts, false, false, false, true, !Util.isDefaultSmsProvider(this.context), false));
        while (true) {
            try {
                ThreadRecord next = readerFor.getNext();
                if (next == null) {
                    break;
                }
                arrayList.add(next.getRecipient().resolve());
            } catch (Throwable th) {
                if (readerFor != null) {
                    try {
                        readerFor.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        readerFor.close();
        if (ConversationUtil.setActiveShortcuts(this.context, arrayList)) {
            ConversationUtil.removeLongLivedShortcuts(this.context, threads.getArchivedRecipients());
            return;
        }
        throw new RetryLaterException();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryLaterException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ConversationShortcutUpdateJob> {
        public ConversationShortcutUpdateJob create(Job.Parameters parameters, Data data) {
            return new ConversationShortcutUpdateJob(parameters);
        }
    }
}
