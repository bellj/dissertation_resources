package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.MobileCoinLedgerWrapper;
import org.thoughtcrime.securesms.transport.RetryLaterException;

/* loaded from: classes4.dex */
public final class PaymentLedgerUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_PAYMENT_UUID;
    private static final String TAG = Log.tag(PaymentLedgerUpdateJob.class);
    private final UUID paymentUuid;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return true;
    }

    public static PaymentLedgerUpdateJob updateLedgerToReflectPayment(UUID uuid) {
        return new PaymentLedgerUpdateJob(uuid);
    }

    public static PaymentLedgerUpdateJob updateLedger() {
        return new PaymentLedgerUpdateJob(null);
    }

    private PaymentLedgerUpdateJob(UUID uuid) {
        this(new Job.Parameters.Builder().setQueue("Payments").setMaxAttempts(10).setMaxInstancesForQueue(1).build(), uuid);
    }

    private PaymentLedgerUpdateJob(Job.Parameters parameters, UUID uuid) {
        super(parameters);
        this.paymentUuid = uuid;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws IOException, RetryLaterException {
        if (!SignalStore.paymentsValues().mobileCoinPaymentsEnabled()) {
            Log.w(TAG, "Payments are not enabled");
            return;
        }
        Long l = null;
        if (this.paymentUuid != null) {
            PaymentDatabase.PaymentTransaction payment = SignalDatabase.payments().getPayment(this.paymentUuid);
            if (payment != null) {
                l = Long.valueOf(payment.getBlockIndex());
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Fetching for a payment ");
                sb.append(this.paymentUuid);
                sb.append(" ");
                sb.append(l.longValue() > 0 ? "non-zero" : "zero");
                Log.i(str, sb.toString());
            } else {
                String str2 = TAG;
                Log.w(str2, "Payment not found " + this.paymentUuid);
            }
        }
        MobileCoinLedgerWrapper tryGetFullLedger = ApplicationDependencies.getPayments().getWallet().tryGetFullLedger(l);
        if (tryGetFullLedger != null) {
            Log.i(TAG, "Ledger fetched successfully");
            SignalStore.paymentsValues().setMobileCoinFullLedger(tryGetFullLedger);
            return;
        }
        Log.i(TAG, "Ledger not updated yet, waiting for a minimum block index");
        throw new RetryLaterException();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder builder = new Data.Builder();
        UUID uuid = this.paymentUuid;
        return builder.putString(KEY_PAYMENT_UUID, uuid != null ? uuid.toString() : null).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to get ledger");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PaymentLedgerUpdateJob> {
        public PaymentLedgerUpdateJob create(Job.Parameters parameters, Data data) {
            String string = data.getString(PaymentLedgerUpdateJob.KEY_PAYMENT_UUID);
            return new PaymentLedgerUpdateJob(parameters, string != null ? UUID.fromString(string) : null);
        }
    }
}
