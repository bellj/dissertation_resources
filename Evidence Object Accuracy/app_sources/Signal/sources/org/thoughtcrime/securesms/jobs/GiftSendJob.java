package org.thoughtcrime.securesms.jobs;

import com.google.protobuf.ByteString;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$BooleanRef;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.gifts.Gifts;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SearchDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.sharing.MultiShareSender;
import org.thoughtcrime.securesms.sms.MessageSender;

/* compiled from: GiftSendJob.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006B!\b\u0002\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u0005H\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/GiftSendJob;", "Lorg/thoughtcrime/securesms/jobmanager/Job;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "additionalMessage", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;)V", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/lang/String;)V", "getFactoryKey", "onFailure", "", "run", "Lorg/thoughtcrime/securesms/jobmanager/Job$Result;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GiftSendJob extends Job {
    public static final Companion Companion = new Companion(null);
    public static final String DATA_ADDITIONAL_MESSAGE;
    public static final String DATA_RECIPIENT_ID;
    public static final String KEY;
    private static final String TAG = Log.tag(GiftSendJob.class);
    private final String additionalMessage;
    private final RecipientId recipientId;

    public /* synthetic */ GiftSendJob(Job.Parameters parameters, RecipientId recipientId, String str, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters, recipientId, str);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    private GiftSendJob(Job.Parameters parameters, RecipientId recipientId, String str) {
        super(parameters);
        this.recipientId = recipientId;
        this.additionalMessage = str;
    }

    /* compiled from: GiftSendJob.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \b*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/GiftSendJob$Companion;", "", "()V", "DATA_ADDITIONAL_MESSAGE", "", "DATA_RECIPIENT_ID", "KEY", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GiftSendJob(org.thoughtcrime.securesms.recipients.RecipientId r3, java.lang.String r4) {
        /*
            r2 = this;
            java.lang.String r0 = "recipientId"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder()\n        .build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.GiftSendJob.<init>(org.thoughtcrime.securesms.recipients.RecipientId, java.lang.String):void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data build = new Data.Builder().putLong(DATA_RECIPIENT_ID, this.recipientId.toLong()).putString(DATA_ADDITIONAL_MESSAGE, this.additionalMessage).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n    .putLong(D…onalMessage)\n    .build()");
        return build;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Job.Result run() {
        Job.Result result;
        String str = TAG;
        Log.i(str, "Getting data and generating message for gift send to " + this.recipientId);
        Data inputData = getInputData();
        byte[] stringAsBlob = inputData != null ? inputData.getStringAsBlob(DonationReceiptRedemptionJob.INPUT_RECEIPT_CREDENTIAL_PRESENTATION) : null;
        if (stringAsBlob == null) {
            Job.Result failure = Job.Result.failure();
            Intrinsics.checkNotNullExpressionValue(failure, "failure()");
            return failure;
        }
        Recipient resolved = Recipient.resolved(this.recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (resolved.isGroup() || resolved.isDistributionList() || resolved.getRegistered() != RecipientDatabase.RegisteredState.REGISTERED) {
            Log.w(str, "Invalid recipient " + this.recipientId + " for gift send.");
            Job.Result failure2 = Job.Result.failure();
            Intrinsics.checkNotNullExpressionValue(failure2, "failure()");
            return failure2;
        }
        long orCreateThreadIdFor = SignalDatabase.Companion.threads().getOrCreateThreadIdFor(resolved);
        Gifts gifts = Gifts.INSTANCE;
        long millis = TimeUnit.SECONDS.toMillis((long) resolved.getExpiresInSeconds());
        long currentTimeMillis = System.currentTimeMillis();
        GiftBadge build = GiftBadge.newBuilder().setRedemptionToken(ByteString.copyFrom(stringAsBlob)).build();
        Intrinsics.checkNotNullExpressionValue(build, "build()");
        OutgoingMediaMessage createOutgoingGiftMessage = gifts.createOutgoingGiftMessage(resolved, build, currentTimeMillis, millis);
        Log.i(str, "Sending gift badge to " + this.recipientId + SearchDatabase.SNIPPET_WRAP);
        Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        MessageSender.send(this.context, createOutgoingGiftMessage, orCreateThreadIdFor, false, (String) null, (MessageDatabase.InsertListener) new MessageDatabase.InsertListener() { // from class: org.thoughtcrime.securesms.jobs.GiftSendJob$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.database.MessageDatabase.InsertListener
            public final void onComplete() {
                GiftSendJob.m1912run$lambda0(Ref$BooleanRef.this);
            }
        });
        if (ref$BooleanRef.element) {
            Log.i(str, "Successfully inserted outbox message for gift", true);
            if (this.additionalMessage != null) {
                Log.i(str, "Sending additional message...");
                if (MultiShareSender.sendSync(new MultiShareArgs.Builder(SetsKt__SetsJVMKt.setOf(new ContactSearchKey.RecipientSearchKey.KnownRecipient(this.recipientId))).withDraftText(this.additionalMessage).build()).containsFailures()) {
                    Log.w(str, "Failed to send additional message, but gift sent fine.", true);
                }
                result = Job.Result.success();
            } else {
                result = Job.Result.success();
            }
            Intrinsics.checkNotNullExpressionValue(result, "{\n      Log.i(TAG, \"Succ…t.success()\n      }\n    }");
            return result;
        }
        Log.w(str, "Failed to insert outbox message for gift", true);
        Job.Result failure3 = Job.Result.failure();
        Intrinsics.checkNotNullExpressionValue(failure3, "{\n      Log.w(TAG, \"Fail…   Result.failure()\n    }");
        return failure3;
    }

    /* renamed from: run$lambda-0 */
    public static final void m1912run$lambda0(Ref$BooleanRef ref$BooleanRef) {
        Intrinsics.checkNotNullParameter(ref$BooleanRef, "$didInsert");
        ref$BooleanRef.element = true;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Failed to submit send of gift badge to " + this.recipientId);
    }

    /* compiled from: GiftSendJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/GiftSendJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/GiftSendJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<GiftSendJob> {
        public GiftSendJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            RecipientId from = RecipientId.from(data.getLong(GiftSendJob.DATA_RECIPIENT_ID));
            String stringOrDefault = data.getStringOrDefault(GiftSendJob.DATA_ADDITIONAL_MESSAGE, null);
            Intrinsics.checkNotNullExpressionValue(from, "recipientId");
            return new GiftSendJob(parameters, from, stringOrDefault, null);
        }
    }
}
