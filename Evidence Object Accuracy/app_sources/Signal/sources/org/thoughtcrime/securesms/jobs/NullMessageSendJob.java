package org.thoughtcrime.securesms.jobs;

import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class NullMessageSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_RECIPIENT_ID;
    private static final String TAG = Log.tag(NullMessageSendJob.class);
    private final RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public NullMessageSendJob(RecipientId recipientId) {
        this(recipientId, new Job.Parameters.Builder().setQueue(recipientId.toQueueKey()).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private NullMessageSendJob(RecipientId recipientId, Job.Parameters parameters) {
        super(parameters);
        this.recipientId = recipientId;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient_id", this.recipientId.serialize()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        Recipient resolved = Recipient.resolved(this.recipientId);
        if (resolved.isGroup()) {
            Log.w(TAG, "Groups are not supported!");
            return;
        }
        if (resolved.isUnregistered()) {
            String str = TAG;
            Log.w(str, resolved.getId() + " not registered!");
        }
        try {
            ApplicationDependencies.getSignalServiceMessageSender().sendNullMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, resolved));
        } catch (UntrustedIdentityException unused) {
            Log.w(TAG, "Unable to send null message.");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<NullMessageSendJob> {
        public NullMessageSendJob create(Job.Parameters parameters, Data data) {
            return new NullMessageSendJob(RecipientId.from(data.getString("recipient_id")), parameters);
        }
    }
}
