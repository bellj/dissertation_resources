package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.DecryptionsDrainedConstraint;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class ProfileKeySendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_RECIPIENTS;
    private static final String KEY_THREAD;
    private static final String TAG = Log.tag(ProfileKeySendJob.class);
    private final List<RecipientId> recipients;
    private final long threadId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public static ProfileKeySendJob create(long j, boolean z) {
        List list;
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(j);
        if (recipientForThreadId == null) {
            Log.w(TAG, "Thread no longer valid! Aborting.");
            return null;
        } else if (!recipientForThreadId.isPushV2Group()) {
            if (recipientForThreadId.isGroup()) {
                list = Stream.of(RecipientUtil.getEligibleForSending(Recipient.resolvedList(recipientForThreadId.getParticipantIds()))).map(new ContactUtil$$ExternalSyntheticLambda3()).toList();
            } else {
                list = Stream.of(recipientForThreadId.getId()).toList();
            }
            list.remove(Recipient.self().getId());
            if (!z) {
                return new ProfileKeySendJob(new Job.Parameters.Builder().setQueue(recipientForThreadId.getId().toQueueKey()).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), j, list);
            }
            Job.Parameters.Builder builder = new Job.Parameters.Builder();
            return new ProfileKeySendJob(builder.setQueue("ProfileKeySendJob_" + recipientForThreadId.getId().toQueueKey()).setMaxInstancesForQueue(1).addConstraint(NetworkConstraint.KEY).addConstraint(DecryptionsDrainedConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), j, list);
        } else {
            throw new AssertionError("Do not send profile keys directly for GV2");
        }
    }

    private ProfileKeySendJob(Job.Parameters parameters, long j, List<RecipientId> list) {
        super(parameters);
        this.threadId = j;
        this.recipients = list;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (SignalDatabase.threads().getRecipientForThreadId(this.threadId) == null) {
            Log.w(TAG, "Thread no longer present");
        } else {
            List<Recipient> deliver = deliver(Stream.of(this.recipients).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList());
            for (Recipient recipient : deliver) {
                this.recipients.remove(recipient.getId());
            }
            String str = TAG;
            Log.i(str, "Completed now: " + deliver.size() + ", Remaining: " + this.recipients.size());
            if (!this.recipients.isEmpty()) {
                Log.w(str, "Still need to send to " + this.recipients.size() + " recipients. Retrying.");
                throw new RetryLaterException();
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if ((exc instanceof ServerRejectedException) || (exc instanceof NotPushRegisteredException)) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("thread", this.threadId).putString("recipients", RecipientId.toSerializedList(this.recipients)).build();
    }

    private List<Recipient> deliver(List<Recipient> list) throws IOException, UntrustedIdentityException {
        return GroupSendJobHelper.getCompletedSends(list, GroupSendUtil.sendUnresendableDataMessage(this.context, null, list, false, ContentHint.IMPLICIT, SignalServiceDataMessage.newBuilder().asProfileKeyUpdate(true).withTimestamp(System.currentTimeMillis()).withProfileKey(Recipient.self().resolve().getProfileKey()).build())).completed;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ProfileKeySendJob> {
        public ProfileKeySendJob create(Job.Parameters parameters, Data data) {
            return new ProfileKeySendJob(parameters, data.getLong("thread"), RecipientId.fromSerializedList(data.getString("recipients")));
        }
    }
}
