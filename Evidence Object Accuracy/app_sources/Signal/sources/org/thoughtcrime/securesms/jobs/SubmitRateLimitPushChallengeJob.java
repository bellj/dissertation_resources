package org.thoughtcrime.securesms.jobs;

import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.ratelimit.RateLimitUtil;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public final class SubmitRateLimitPushChallengeJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_CHALLENGE;
    private final String challenge;

    /* loaded from: classes4.dex */
    public static final class SuccessEvent {
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public SubmitRateLimitPushChallengeJob(String str) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.HOURS.toMillis(1)).setMaxAttempts(-1).build(), str);
    }

    private SubmitRateLimitPushChallengeJob(Job.Parameters parameters, String str) {
        super(parameters);
        this.challenge = str;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_CHALLENGE, this.challenge).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        ApplicationDependencies.getSignalServiceAccountManager().submitRateLimitPushChallenge(this.challenge);
        SignalStore.rateLimit().onProofAccepted();
        EventBus.getDefault().post(new SuccessEvent());
        RateLimitUtil.retryAllRateLimitedMessages(this.context);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<SubmitRateLimitPushChallengeJob> {
        public SubmitRateLimitPushChallengeJob create(Job.Parameters parameters, Data data) {
            return new SubmitRateLimitPushChallengeJob(parameters, data.getString(SubmitRateLimitPushChallengeJob.KEY_CHALLENGE));
        }
    }
}
