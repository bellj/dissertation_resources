package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceVerifiedUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_DESTINATION;
    private static final String KEY_IDENTITY_KEY;
    private static final String KEY_TIMESTAMP;
    private static final String KEY_VERIFIED_STATUS;
    private static final String TAG = Log.tag(MultiDeviceVerifiedUpdateJob.class);
    private RecipientId destination;
    private byte[] identityKey;
    private long timestamp;
    private IdentityDatabase.VerifiedStatus verifiedStatus;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* synthetic */ MultiDeviceVerifiedUpdateJob(Job.Parameters parameters, RecipientId recipientId, byte[] bArr, IdentityDatabase.VerifiedStatus verifiedStatus, long j, AnonymousClass1 r7) {
        this(parameters, recipientId, bArr, verifiedStatus, j);
    }

    public MultiDeviceVerifiedUpdateJob(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue("__MULTI_DEVICE_VERIFIED_UPDATE__").setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), recipientId, identityKey.serialize(), verifiedStatus, System.currentTimeMillis());
    }

    private MultiDeviceVerifiedUpdateJob(Job.Parameters parameters, RecipientId recipientId, byte[] bArr, IdentityDatabase.VerifiedStatus verifiedStatus, long j) {
        super(parameters);
        this.destination = recipientId;
        this.identityKey = bArr;
        this.verifiedStatus = verifiedStatus;
        this.timestamp = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_DESTINATION, this.destination.serialize()).putString(KEY_IDENTITY_KEY, Base64.encodeBytes(this.identityKey)).putInt(KEY_VERIFIED_STATUS, this.verifiedStatus.toInt()).putLong("timestamp", this.timestamp).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (Recipient.self().isRegistered()) {
            try {
                if (!TextSecurePreferences.isMultiDevice(this.context)) {
                    Log.i(TAG, "Not multi device...");
                } else if (this.destination == null) {
                    Log.w(TAG, "No destination...");
                } else {
                    SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
                    Recipient resolved = Recipient.resolved(this.destination);
                    if (resolved.isUnregistered()) {
                        String str = TAG;
                        Log.w(str, resolved.getId() + " not registered!");
                        return;
                    }
                    signalServiceMessageSender.sendSyncMessage(SignalServiceSyncMessage.forVerified(new VerifiedMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), new IdentityKey(this.identityKey, 0), getVerifiedState(this.verifiedStatus), this.timestamp)), UnidentifiedAccessUtil.getAccessFor(this.context, resolved));
                }
            } catch (InvalidKeyException e) {
                throw new IOException(e);
            }
        } else {
            throw new NotPushRegisteredException();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.MultiDeviceVerifiedUpdateJob$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus;

        static {
            int[] iArr = new int[IdentityDatabase.VerifiedStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus = iArr;
            try {
                iArr[IdentityDatabase.VerifiedStatus.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[IdentityDatabase.VerifiedStatus.VERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[IdentityDatabase.VerifiedStatus.UNVERIFIED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private VerifiedMessage.VerifiedState getVerifiedState(IdentityDatabase.VerifiedStatus verifiedStatus) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[verifiedStatus.ordinal()];
        if (i == 1) {
            return VerifiedMessage.VerifiedState.DEFAULT;
        }
        if (i == 2) {
            return VerifiedMessage.VerifiedState.VERIFIED;
        }
        if (i == 3) {
            return VerifiedMessage.VerifiedState.UNVERIFIED;
        }
        throw new AssertionError("Unknown status: " + this.verifiedStatus);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceVerifiedUpdateJob> {
        public MultiDeviceVerifiedUpdateJob create(Job.Parameters parameters, Data data) {
            try {
                return new MultiDeviceVerifiedUpdateJob(parameters, RecipientId.from(data.getString(MultiDeviceVerifiedUpdateJob.KEY_DESTINATION)), Base64.decode(data.getString(MultiDeviceVerifiedUpdateJob.KEY_IDENTITY_KEY)), IdentityDatabase.VerifiedStatus.forState(data.getInt(MultiDeviceVerifiedUpdateJob.KEY_VERIFIED_STATUS)), data.getLong("timestamp"), null);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}
