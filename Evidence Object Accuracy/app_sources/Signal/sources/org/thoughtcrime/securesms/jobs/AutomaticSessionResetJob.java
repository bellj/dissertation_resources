package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.DeviceLastResetTime;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.DecryptionsDrainedConstraint;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;

/* loaded from: classes4.dex */
public class AutomaticSessionResetJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_DEVICE_ID;
    private static final String KEY_RECIPIENT_ID;
    private static final String KEY_SENT_TIMESTAMP;
    private static final String TAG = Log.tag(AutomaticSessionResetJob.class);
    private final int deviceId;
    private final RecipientId recipientId;
    private final long sentTimestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public AutomaticSessionResetJob(RecipientId recipientId, int i, long j) {
        this(new Job.Parameters.Builder().setQueue(PushProcessMessageJob.getQueueName(recipientId)).addConstraint(DecryptionsDrainedConstraint.KEY).setMaxInstancesForQueue(1).build(), recipientId, i, j);
    }

    private AutomaticSessionResetJob(Job.Parameters parameters, RecipientId recipientId, int i, long j) {
        super(parameters);
        this.recipientId = recipientId;
        this.deviceId = i;
        this.sentTimestamp = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient_id", this.recipientId.serialize()).putInt("device_id", this.deviceId).putLong("sent_timestamp", this.sentTimestamp).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        ApplicationDependencies.getProtocolStore().aci().sessions().archiveSession(this.recipientId, this.deviceId);
        SignalDatabase.senderKeyShared().deleteAllFor(this.recipientId);
        insertLocalMessage();
        if (FeatureFlags.automaticSessionReset()) {
            long millis = TimeUnit.SECONDS.toMillis((long) FeatureFlags.automaticSessionResetIntervalSeconds());
            DeviceLastResetTime lastSessionResetTimes = SignalDatabase.recipients().getLastSessionResetTimes(this.recipientId);
            long currentTimeMillis = System.currentTimeMillis() - getLastResetTime(lastSessionResetTimes, this.deviceId);
            String str = TAG;
            Log.i(str, "DeviceId: " + this.deviceId + ", Reset interval: " + millis + ", Time since last reset: " + currentTimeMillis);
            if (currentTimeMillis > millis) {
                Log.i(str, "We're good! Sending a null message.");
                SignalDatabase.recipients().setLastSessionResetTime(this.recipientId, setLastResetTime(lastSessionResetTimes, this.deviceId, System.currentTimeMillis()));
                Log.i(str, "Marked last reset time: " + System.currentTimeMillis());
                sendNullMessage();
                Log.i(str, "Successfully sent!");
                return;
            }
            Log.w(str, "Too soon! Time since last reset: " + currentTimeMillis);
            return;
        }
        Log.w(TAG, "Automatic session reset send disabled!");
    }

    private void insertLocalMessage() {
        ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(SignalDatabase.sms().insertChatSessionRefreshedMessage(this.recipientId, (long) this.deviceId, this.sentTimestamp).getThreadId()));
    }

    private void sendNullMessage() throws IOException {
        Recipient resolved = Recipient.resolved(this.recipientId);
        if (resolved.isUnregistered()) {
            String str = TAG;
            Log.w(str, resolved.getId() + " not registered!");
            return;
        }
        try {
            ApplicationDependencies.getSignalServiceMessageSender().sendNullMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, resolved));
        } catch (UntrustedIdentityException unused) {
            Log.w(TAG, "Unable to send null message.");
        }
    }

    private long getLastResetTime(DeviceLastResetTime deviceLastResetTime, int i) {
        for (DeviceLastResetTime.Pair pair : deviceLastResetTime.getResetTimeList()) {
            if (pair.getDeviceId() == i) {
                return pair.getLastResetTime();
            }
        }
        return 0;
    }

    private DeviceLastResetTime setLastResetTime(DeviceLastResetTime deviceLastResetTime, int i, long j) {
        DeviceLastResetTime.Builder newBuilder = DeviceLastResetTime.newBuilder();
        for (DeviceLastResetTime.Pair pair : deviceLastResetTime.getResetTimeList()) {
            if (pair.getDeviceId() != i) {
                newBuilder.addResetTime(pair);
            }
        }
        newBuilder.addResetTime(DeviceLastResetTime.Pair.newBuilder().setDeviceId(i).setLastResetTime(j));
        return newBuilder.build();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AutomaticSessionResetJob> {
        public AutomaticSessionResetJob create(Job.Parameters parameters, Data data) {
            return new AutomaticSessionResetJob(parameters, RecipientId.from(data.getString("recipient_id")), data.getInt("device_id"), data.getLong("sent_timestamp"));
        }
    }
}
