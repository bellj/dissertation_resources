package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.service.SmsDeliveryListener;

/* loaded from: classes4.dex */
public class SmsSentJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ACTION;
    private static final String KEY_IS_MULTIPART;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_RESULT;
    private static final String KEY_RUN_ATTEMPT;
    private static final String TAG = Log.tag(SmsSentJob.class);
    private final String action;
    private final boolean isMultipart;
    private final long messageId;
    private final int result;
    private final int runAttempt;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public SmsSentJob(long j, boolean z, String str, int i, int i2) {
        this(new Job.Parameters.Builder().build(), j, z, str, i, i2);
    }

    private SmsSentJob(Job.Parameters parameters, long j, boolean z, String str, int i, int i2) {
        super(parameters);
        this.messageId = j;
        this.isMultipart = z;
        this.action = str;
        this.result = i;
        this.runAttempt = i2;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putBoolean(KEY_IS_MULTIPART, this.isMultipart).putString(KEY_ACTION, this.action).putInt("result", this.result).putInt(KEY_RUN_ATTEMPT, this.runAttempt).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        String str = TAG;
        Log.i(str, "Got SMS callback: " + this.action + " , " + this.result);
        String str2 = this.action;
        str2.hashCode();
        if (str2.equals(SmsDeliveryListener.SENT_SMS_ACTION)) {
            handleSentResult(this.messageId, this.result);
        } else if (str2.equals(SmsDeliveryListener.DELIVERED_SMS_ACTION)) {
            handleDeliveredResult(this.messageId, this.result);
        }
    }

    private void handleDeliveredResult(long j, int i) {
        SignalDatabase.sms().markSmsStatus(j, i);
    }

    private void handleSentResult(long j, int i) {
        try {
            SmsDatabase sms = SignalDatabase.sms();
            SmsMessageRecord smsMessage = sms.getSmsMessage(j);
            if (i == -1) {
                sms.markAsSent(j, false);
            } else if (i != 2 && i != 4) {
                sms.markAsSentFailed(j);
                ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(this.context, smsMessage.getRecipient(), ConversationId.forConversation(smsMessage.getThreadId()));
            } else if (this.isMultipart) {
                Log.w(TAG, "Service connectivity problem, but not retrying due to multipart");
                sms.markAsSentFailed(j);
                ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(this.context, smsMessage.getRecipient(), ConversationId.forConversation(smsMessage.getThreadId()));
            } else {
                Log.w(TAG, "Service connectivity problem, requeuing...");
                ApplicationDependencies.getJobManager().add(new SmsSendJob(j, smsMessage.getIndividualRecipient(), this.runAttempt + 1));
            }
        } catch (NoSuchMessageException e) {
            while (true) {
                Log.w(TAG, e);
                return;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<SmsSentJob> {
        public SmsSentJob create(Job.Parameters parameters, Data data) {
            return new SmsSentJob(parameters, data.getLong("message_id"), data.getBooleanOrDefault(SmsSentJob.KEY_IS_MULTIPART, true), data.getString(SmsSentJob.KEY_ACTION), data.getInt("result"), data.getInt(SmsSentJob.KEY_RUN_ATTEMPT));
        }
    }
}
