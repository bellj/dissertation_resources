package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public final class PaymentNotificationSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_RECIPIENT;
    private static final String KEY_UUID;
    private static final String TAG = Log.tag(PaymentNotificationSendJob.class);
    private final RecipientId recipientId;
    private final UUID uuid;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public PaymentNotificationSendJob(RecipientId recipientId, UUID uuid, String str) {
        this(new Job.Parameters.Builder().setQueue(str).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), recipientId, uuid);
    }

    private PaymentNotificationSendJob(Job.Parameters parameters, RecipientId recipientId, UUID uuid) {
        super(parameters);
        this.recipientId = recipientId;
        this.uuid = uuid;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient", this.recipientId.serialize()).putString("uuid", this.uuid.toString()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (Recipient.self().isRegistered()) {
            PaymentDatabase payments = SignalDatabase.payments();
            Recipient resolved = Recipient.resolved(this.recipientId);
            if (resolved.isUnregistered()) {
                String str = TAG;
                Log.w(str, this.recipientId + " not registered!");
                return;
            }
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            SignalServiceAddress signalServiceAddress = RecipientUtil.toSignalServiceAddress(this.context, resolved);
            Optional<UnidentifiedAccessPair> accessFor = UnidentifiedAccessUtil.getAccessFor(this.context, resolved);
            PaymentDatabase.PaymentTransaction payment = payments.getPayment(this.uuid);
            if (payment == null) {
                String str2 = TAG;
                Log.w(str2, "Could not find payment, cannot send notification " + this.uuid);
            } else if (payment.getReceipt() == null) {
                String str3 = TAG;
                Log.w(str3, "Could not find payment receipt, cannot send notification " + this.uuid);
            } else {
                SendMessageResult sendDataMessage = signalServiceMessageSender.sendDataMessage(signalServiceAddress, accessFor, ContentHint.DEFAULT, SignalServiceDataMessage.newBuilder().withPayment(new SignalServiceDataMessage.Payment(new SignalServiceDataMessage.PaymentNotification(payment.getReceipt(), payment.getNote()))).build(), SignalServiceMessageSender.IndividualSendEvents.EMPTY);
                if (sendDataMessage.getIdentityFailure() != null) {
                    String str4 = TAG;
                    Log.w(str4, "Identity failure for " + resolved.getId());
                } else if (sendDataMessage.isUnregisteredFailure()) {
                    String str5 = TAG;
                    Log.w(str5, "Unregistered failure for " + resolved.getId());
                } else if (sendDataMessage.getSuccess() != null) {
                    Log.i(TAG, String.format("Payment notification sent to %s for %s", this.recipientId, this.uuid));
                } else {
                    throw new RetryLaterException();
                }
            }
        } else {
            throw new NotPushRegisteredException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if ((exc instanceof ServerRejectedException) || (exc instanceof NotPushRegisteredException)) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, String.format("Failed to send payment notification to recipient %s for %s", this.recipientId, this.uuid));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PaymentNotificationSendJob> {
        public PaymentNotificationSendJob create(Job.Parameters parameters, Data data) {
            return new PaymentNotificationSendJob(parameters, RecipientId.from(data.getString("recipient")), UUID.fromString(data.getString("uuid")));
        }
    }
}
