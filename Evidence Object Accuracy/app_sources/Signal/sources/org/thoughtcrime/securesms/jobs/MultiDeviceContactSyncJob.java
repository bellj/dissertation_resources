package org.thoughtcrime.securesms.jobs;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceContact;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsInputStream;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.MissingConfigurationException;
import org.whispersystems.signalservice.api.util.AttachmentPointerUtil;

/* compiled from: MultiDeviceContactSyncJob.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00192\u00020\u0001:\u0002\u0019\u001aB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u000e\u001a\u00020\rH\u0014J\u0014\u0010\u000f\u001a\u00020\u00102\n\u0010\u0011\u001a\u00060\u0012j\u0002`\u0013H\u0014J\u0010\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceContactSyncJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "contactsAttachment", "Lorg/whispersystems/signalservice/api/messages/SignalServiceAttachmentPointer;", "(Lorg/whispersystems/signalservice/api/messages/SignalServiceAttachmentPointer;)V", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "attachmentPointer", "", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;[B)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "processContactFile", "inputStream", "Ljava/io/InputStream;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiDeviceContactSyncJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    public static final String KEY_ATTACHMENT_POINTER;
    private static final long MAX_ATTACHMENT_SIZE;
    private static final String TAG = Log.tag(MultiDeviceContactSyncJob.class);
    private final byte[] attachmentPointer;

    /* compiled from: MultiDeviceContactSyncJob.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[VerifiedMessage.VerifiedState.values().length];
            iArr[VerifiedMessage.VerifiedState.VERIFIED.ordinal()] = 1;
            iArr[VerifiedMessage.VerifiedState.UNVERIFIED.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MultiDeviceContactSyncJob(Job.Parameters parameters, byte[] bArr) {
        super(parameters);
        Intrinsics.checkNotNullParameter(parameters, "parameters");
        Intrinsics.checkNotNullParameter(bArr, "attachmentPointer");
        this.attachmentPointer = bArr;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MultiDeviceContactSyncJob(org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer r3) {
        /*
            r2 = this;
            java.lang.String r0 = "contactsAttachment"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.String r1 = "MultiDeviceContactSyncJob"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder()\n      .setQueu…tSyncJob\")\n      .build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            org.whispersystems.signalservice.internal.push.SignalServiceProtos$AttachmentPointer r3 = org.whispersystems.signalservice.api.util.AttachmentPointerUtil.createAttachmentPointer(r3)
            byte[] r3 = r3.toByteArray()
            java.lang.String r1 = "createAttachmentPointer(…Attachment).toByteArray()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r1)
            r2.<init>(r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.MultiDeviceContactSyncJob.<init>(org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer):void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data build = new Data.Builder().putBlobAsString(KEY_ATTACHMENT_POINTER, this.attachmentPointer).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n      .putBlob…ntPointer)\n      .build()");
        return build;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (SignalStore.account().isPrimaryDevice()) {
            Log.i(TAG, "Not linked device, aborting...");
        } else {
            SignalServiceAttachmentPointer createSignalAttachmentPointer = AttachmentPointerUtil.createSignalAttachmentPointer(this.attachmentPointer);
            Intrinsics.checkNotNullExpressionValue(createSignalAttachmentPointer, "createSignalAttachmentPointer(attachmentPointer)");
            try {
                File forNonAutoEncryptingSingleSessionOnDisk = BlobProvider.getInstance().forNonAutoEncryptingSingleSessionOnDisk(this.context);
                Intrinsics.checkNotNullExpressionValue(forNonAutoEncryptingSingleSessionOnDisk, "getInstance().forNonAuto…gleSessionOnDisk(context)");
                InputStream retrieveAttachment = ApplicationDependencies.getSignalServiceMessageReceiver().retrieveAttachment(createSignalAttachmentPointer, forNonAutoEncryptingSingleSessionOnDisk, MAX_ATTACHMENT_SIZE);
                th = null;
                try {
                    processContactFile(retrieveAttachment);
                    Unit unit = Unit.INSTANCE;
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            } catch (InvalidMessageException e) {
                throw new IOException(e);
            } catch (MissingConfigurationException e2) {
                throw new IOException(e2);
            }
        }
    }

    private final void processContactFile(InputStream inputStream) {
        IdentityDatabase.VerifiedStatus verifiedStatus;
        DeviceContactsInputStream deviceContactsInputStream = new DeviceContactsInputStream(inputStream);
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        RecipientDatabase recipients = companion.recipients();
        ThreadDatabase threads = companion.threads();
        DeviceContact read = deviceContactsInputStream.read();
        while (read != null) {
            Recipient externalPush = Recipient.externalPush(new SignalServiceAddress(read.getAddress().getServiceId(), read.getAddress().getNumber().orElse(null)));
            Intrinsics.checkNotNullExpressionValue(externalPush, "externalPush(SignalServi…ess.number.orElse(null)))");
            if (externalPush.isSelf()) {
                read = deviceContactsInputStream.read();
            } else {
                if (read.getName().isPresent()) {
                    RecipientId id = externalPush.getId();
                    Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
                    String str = read.getName().get();
                    Intrinsics.checkNotNullExpressionValue(str, "contact.name.get()");
                    recipients.setSystemContactName(id, str);
                }
                if (read.getExpirationTimer().isPresent()) {
                    RecipientId id2 = externalPush.getId();
                    Intrinsics.checkNotNullExpressionValue(id2, "recipient.id");
                    Integer num = read.getExpirationTimer().get();
                    Intrinsics.checkNotNullExpressionValue(num, "contact.expirationTimer.get()");
                    recipients.setExpireMessages(id2, num.intValue());
                }
                if (read.getProfileKey().isPresent()) {
                    ProfileKey profileKey = read.getProfileKey().get();
                    Intrinsics.checkNotNullExpressionValue(profileKey, "contact.profileKey.get()");
                    RecipientId id3 = externalPush.getId();
                    Intrinsics.checkNotNullExpressionValue(id3, "recipient.id");
                    recipients.setProfileKey(id3, profileKey);
                }
                if (read.getVerified().isPresent()) {
                    VerifiedMessage.VerifiedState verified = read.getVerified().get().getVerified();
                    int i = verified == null ? -1 : WhenMappings.$EnumSwitchMapping$0[verified.ordinal()];
                    if (i == 1) {
                        verifiedStatus = IdentityDatabase.VerifiedStatus.VERIFIED;
                    } else if (i != 2) {
                        verifiedStatus = IdentityDatabase.VerifiedStatus.DEFAULT;
                    } else {
                        verifiedStatus = IdentityDatabase.VerifiedStatus.UNVERIFIED;
                    }
                    ApplicationDependencies.getProtocolStore().aci().identities().saveIdentityWithoutSideEffects(externalPush.getId(), read.getVerified().get().getIdentityKey(), verifiedStatus, false, read.getVerified().get().getTimestamp(), true);
                }
                RecipientId id4 = externalPush.getId();
                Intrinsics.checkNotNullExpressionValue(id4, "recipient.id");
                recipients.setBlocked(id4, read.isBlocked());
                ThreadRecord threadRecord = threads.getThreadRecord(threads.getThreadIdFor(externalPush.getId()));
                if (!(threadRecord == null || read.isArchived() == threadRecord.isArchived())) {
                    if (read.isArchived()) {
                        threads.archiveConversation(threadRecord.getThreadId());
                    } else {
                        threads.unarchiveConversation(threadRecord.getThreadId());
                    }
                }
                if (read.getAvatar().isPresent()) {
                    try {
                        AvatarHelper.setSyncAvatar(this.context, externalPush.getId(), read.getAvatar().get().getInputStream());
                    } catch (IOException unused) {
                        String str2 = TAG;
                        Log.w(str2, "Unable to set sync avatar for " + externalPush.getId());
                    }
                }
                read = deviceContactsInputStream.read();
            }
        }
    }

    /* compiled from: MultiDeviceContactSyncJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceContactSyncJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/MultiDeviceContactSyncJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceContactSyncJob> {
        public MultiDeviceContactSyncJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            byte[] stringAsBlob = data.getStringAsBlob(MultiDeviceContactSyncJob.KEY_ATTACHMENT_POINTER);
            Intrinsics.checkNotNullExpressionValue(stringAsBlob, "data.getStringAsBlob(KEY_ATTACHMENT_POINTER)");
            return new MultiDeviceContactSyncJob(parameters, stringAsBlob);
        }
    }

    /* compiled from: MultiDeviceContactSyncJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceContactSyncJob$Companion;", "", "()V", "KEY", "", "KEY_ATTACHMENT_POINTER", "MAX_ATTACHMENT_SIZE", "", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
