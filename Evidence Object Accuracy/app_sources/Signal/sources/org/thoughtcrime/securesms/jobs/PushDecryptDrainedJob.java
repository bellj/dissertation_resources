package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public class PushDecryptDrainedJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(PushDecryptDrainedJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public PushDecryptDrainedJob() {
        this(new Job.Parameters.Builder().setQueue(PushDecryptMessageJob.QUEUE).build());
    }

    private PushDecryptDrainedJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        Log.i(TAG, "Decryptions are caught-up.");
        ApplicationDependencies.getIncomingMessageObserver().notifyDecryptionsDrained();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushDecryptDrainedJob> {
        public PushDecryptDrainedJob create(Job.Parameters parameters, Data data) {
            return new PushDecryptDrainedJob(parameters);
        }
    }
}
