package org.thoughtcrime.securesms.jobs;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewOnceOpenMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceViewOnceOpenJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String TAG = Log.tag(MultiDeviceViewOnceOpenJob.class);
    private SerializableSyncMessageId messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public MultiDeviceViewOnceOpenJob(MessageDatabase.SyncMessageId syncMessageId) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), syncMessageId);
    }

    private MultiDeviceViewOnceOpenJob(Job.Parameters parameters, MessageDatabase.SyncMessageId syncMessageId) {
        super(parameters);
        this.messageId = new SerializableSyncMessageId(syncMessageId.getRecipientId().serialize(), syncMessageId.getTimetamp());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        try {
            return new Data.Builder().putString("message_id", JsonUtils.toJson(this.messageId)).build();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device...");
        } else {
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            Recipient resolved = Recipient.resolved(RecipientId.from(this.messageId.recipientId));
            if (resolved.isUnregistered()) {
                String str = TAG;
                Log.w(str, resolved.getId() + " not registered!");
                return;
            }
            signalServiceMessageSender.sendSyncMessage(SignalServiceSyncMessage.forViewOnceOpen(new ViewOnceOpenMessage(RecipientUtil.getOrFetchServiceId(this.context, resolved), this.messageId.timestamp)), UnidentifiedAccessUtil.getAccessForSync(this.context));
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes.dex */
    public static class SerializableSyncMessageId implements Serializable {
        private static final long serialVersionUID;
        @JsonProperty
        private final String recipientId;
        @JsonProperty
        private final long timestamp;

        private SerializableSyncMessageId(@JsonProperty("recipientId") String str, @JsonProperty("timestamp") long j) {
            this.recipientId = str;
            this.timestamp = j;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceViewOnceOpenJob> {
        public MultiDeviceViewOnceOpenJob create(Job.Parameters parameters, Data data) {
            try {
                SerializableSyncMessageId serializableSyncMessageId = (SerializableSyncMessageId) JsonUtils.fromJson(data.getString("message_id"), SerializableSyncMessageId.class);
                return new MultiDeviceViewOnceOpenJob(parameters, new MessageDatabase.SyncMessageId(RecipientId.from(serializableSyncMessageId.recipientId), serializableSyncMessageId.timestamp));
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}
