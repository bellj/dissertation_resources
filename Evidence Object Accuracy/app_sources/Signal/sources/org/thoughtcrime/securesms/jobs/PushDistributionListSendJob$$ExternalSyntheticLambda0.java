package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.attachments.Attachment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PushDistributionListSendJob$$ExternalSyntheticLambda0 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((Attachment) obj).isSticker();
    }
}
