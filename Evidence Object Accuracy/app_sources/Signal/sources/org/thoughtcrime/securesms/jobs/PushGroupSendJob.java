package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobLogger;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.messages.StorySendUtil;
import org.thoughtcrime.securesms.mms.MessageGroupContext;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.RecipientAccessList;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupV2;
import org.whispersystems.signalservice.api.messages.SignalServicePreview;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class PushGroupSendJob extends PushSendJob {
    public static final String KEY;
    private static final String KEY_FILTER_RECIPIENTS;
    private static final String KEY_MESSAGE_ID;
    private static final String TAG = Log.tag(PushGroupSendJob.class);
    private final Set<RecipientId> filterRecipients;
    private final long messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public PushGroupSendJob(long j, RecipientId recipientId, Set<RecipientId> set, boolean z) {
        this(new Job.Parameters.Builder().setQueue(recipientId.toQueueKey(z)).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), j, set);
    }

    private PushGroupSendJob(Job.Parameters parameters, long j, Set<RecipientId> set) {
        super(parameters);
        this.messageId = j;
        this.filterRecipients = set;
    }

    public static void enqueue(Context context, JobManager jobManager, long j, RecipientId recipientId, Set<RecipientId> set) {
        try {
            Recipient resolved = Recipient.resolved(recipientId);
            if (resolved.isPushGroup()) {
                OutgoingMediaMessage outgoingMessage = SignalDatabase.mms().getOutgoingMessage(j);
                Set<String> enqueueCompressingAndUploadAttachmentsChains = PushSendJob.enqueueCompressingAndUploadAttachmentsChains(jobManager, outgoingMessage);
                if (outgoingMessage.getGiftBadge() == null) {
                    if (!SignalDatabase.groups().isActive(resolved.requireGroupId()) && !isGv2UpdateMessage(outgoingMessage)) {
                        throw new MmsException("Inactive group!");
                    }
                    jobManager.add(new PushGroupSendJob(j, recipientId, set, !enqueueCompressingAndUploadAttachmentsChains.isEmpty()), enqueueCompressingAndUploadAttachmentsChains, enqueueCompressingAndUploadAttachmentsChains.isEmpty() ? null : recipientId.toQueueKey());
                    return;
                }
                throw new MmsException("Cannot send a gift badge to a group!");
            }
            throw new AssertionError("Not a group!");
        } catch (NoSuchMessageException | MmsException e) {
            Log.w(TAG, "Failed to enqueue message.", e);
            SignalDatabase.mms().markAsSentFailed(j);
            PushSendJob.notifyMediaMessageDeliveryFailed(context, j);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putString(KEY_FILTER_RECIPIENTS, RecipientId.toSerializedList(this.filterRecipients)).build();
    }

    private static boolean isGv2UpdateMessage(OutgoingMediaMessage outgoingMediaMessage) {
        return (outgoingMediaMessage instanceof OutgoingGroupUpdateMessage) && ((OutgoingGroupUpdateMessage) outgoingMediaMessage).isV2Group();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        SignalDatabase.mms().markAsSending(this.messageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.PushSendJob
    public void onPushSend() throws IOException, MmsException, NoSuchMessageException, RetryLaterException {
        List<Recipient> list;
        SignalLocalMetrics.GroupMessageSend.onJobStarted(this.messageId);
        MmsDatabase mms = SignalDatabase.mms();
        OutgoingMediaMessage outgoingMessage = mms.getOutgoingMessage(this.messageId);
        long threadId = mms.getMessageRecord(this.messageId).getThreadId();
        Set<NetworkFailure> networkFailures = outgoingMessage.getNetworkFailures();
        Set<IdentityKeyMismatch> identityKeyMismatches = outgoingMessage.getIdentityKeyMismatches();
        ApplicationDependencies.getJobManager().cancelAllInQueue(TypingSendJob.getQueue(threadId));
        if (mms.isSent(this.messageId)) {
            String str = TAG;
            String valueOf = String.valueOf(outgoingMessage.getSentTimeMillis());
            log(str, valueOf, "Message " + this.messageId + " was already sent. Ignoring.");
            return;
        }
        Recipient resolve = outgoingMessage.getRecipient().resolve();
        if (!resolve.isPushGroup()) {
            throw new MmsException("Message recipient isn't a group!");
        } else if (!resolve.isPushV1Group()) {
            try {
                String str2 = TAG;
                String valueOf2 = String.valueOf(outgoingMessage.getSentTimeMillis());
                log(str2, valueOf2, "Sending message: " + this.messageId + ", Recipient: " + outgoingMessage.getRecipient().getId() + ", Thread: " + threadId + ", Attachments: " + buildAttachmentString(outgoingMessage.getAttachments()));
                if (!resolve.resolve().isProfileSharing() && !mms.isGroupQuitMessage(this.messageId)) {
                    RecipientUtil.shareProfileIfFirstSecureMessage(this.context, resolve);
                }
                List arrayList = new ArrayList();
                if (Util.hasItems(this.filterRecipients)) {
                    list = new ArrayList<>(this.filterRecipients.size() + networkFailures.size());
                    list.addAll(Stream.of(this.filterRecipients).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList());
                    list.addAll(Stream.of(networkFailures).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda2
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return PushGroupSendJob.this.lambda$onPushSend$0((NetworkFailure) obj);
                        }
                    }).distinct().map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList());
                } else if (!networkFailures.isEmpty()) {
                    list = Stream.of(networkFailures).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda3
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return PushGroupSendJob.this.lambda$onPushSend$1((NetworkFailure) obj);
                        }
                    }).distinct().map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList();
                } else {
                    GroupRecipientResult groupMessageRecipients = getGroupMessageRecipients(resolve.requireGroupId(), this.messageId);
                    list = groupMessageRecipients.target;
                    arrayList = groupMessageRecipients.skipped;
                }
                processGroupMessageResults(this.context, this.messageId, threadId, resolve, outgoingMessage, deliver(outgoingMessage, resolve, list), list, arrayList, networkFailures, identityKeyMismatches);
                Log.i(str2, JobLogger.format(this, "Finished send."));
            } catch (UndeliverableMessageException | UntrustedIdentityException e) {
                warn(TAG, String.valueOf(outgoingMessage.getSentTimeMillis()), e);
                mms.markAsSentFailed(this.messageId);
                PushSendJob.notifyMediaMessageDeliveryFailed(this.context, this.messageId);
            }
            SignalLocalMetrics.GroupMessageSend.onJobFinished(this.messageId);
        } else {
            throw new MmsException("No GV1 messages can be sent anymore!");
        }
    }

    public /* synthetic */ RecipientId lambda$onPushSend$0(NetworkFailure networkFailure) {
        return networkFailure.getRecipientId(this.context);
    }

    public /* synthetic */ RecipientId lambda$onPushSend$1(NetworkFailure networkFailure) {
        return networkFailure.getRecipientId(this.context);
    }

    @Override // org.thoughtcrime.securesms.jobs.PushSendJob, org.thoughtcrime.securesms.jobmanager.Job
    public void onRetry() {
        SignalLocalMetrics.GroupMessageSend.cancel(this.messageId);
        super.onRetry();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        SignalDatabase.mms().markAsSentFailed(this.messageId);
    }

    private List<SendMessageResult> deliver(OutgoingMediaMessage outgoingMediaMessage, Recipient recipient, List<Recipient> list) throws IOException, UntrustedIdentityException, UndeliverableMessageException {
        List<Recipient> list2;
        SignalServiceStoryMessage forFileAttachment;
        try {
            rotateSenderCertificateIfNecessary();
            GroupId.Push requirePush = recipient.requireGroupId().requirePush();
            Optional<byte[]> profileKey = getProfileKey(recipient);
            Optional<SignalServiceDataMessage.Sticker> stickerFor = getStickerFor(outgoingMediaMessage);
            List<SharedContact> sharedContactsFor = getSharedContactsFor(outgoingMediaMessage);
            List<SignalServicePreview> previewsFor = getPreviewsFor(outgoingMediaMessage);
            List<SignalServiceDataMessage.Mention> mentionsFor = getMentionsFor(outgoingMediaMessage.getMentions());
            List<SignalServiceAttachment> attachmentPointersFor = getAttachmentPointersFor(Stream.of(outgoingMediaMessage.getAttachments()).filterNot(new PushDistributionListSendJob$$ExternalSyntheticLambda0()).toList());
            boolean anyMatch = Stream.of(SignalDatabase.groupReceipts().getGroupReceiptInfo(this.messageId)).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda21
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return PushGroupSendJob.lambda$deliver$2((GroupReceiptDatabase.GroupReceiptInfo) obj);
                }
            });
            if (outgoingMediaMessage.getStoryType().isStory()) {
                Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(requirePush);
                if (group.isPresent()) {
                    GroupDatabase.V2GroupProperties requireV2GroupProperties = group.get().requireV2GroupProperties();
                    SignalServiceGroupV2 build = SignalServiceGroupV2.newBuilder(requireV2GroupProperties.getGroupMasterKey()).withRevision(requireV2GroupProperties.getGroupRevision()).build();
                    if (outgoingMediaMessage.getStoryType().isTextStory()) {
                        forFileAttachment = SignalServiceStoryMessage.forTextAttachment(Recipient.self().getProfileKey(), build, StorySendUtil.deserializeBodyToStoryTextAttachment(outgoingMediaMessage, new Function1() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda22
                            @Override // kotlin.jvm.functions.Function1
                            public final Object invoke(Object obj) {
                                return PushGroupSendJob.this.getPreviewsFor((OutgoingMediaMessage) obj);
                            }
                        }), outgoingMediaMessage.getStoryType().isStoryWithReplies());
                    } else if (!attachmentPointersFor.isEmpty()) {
                        forFileAttachment = SignalServiceStoryMessage.forFileAttachment(Recipient.self().getProfileKey(), build, attachmentPointersFor.get(0), outgoingMediaMessage.getStoryType().isStoryWithReplies());
                    } else {
                        throw new UndeliverableMessageException("No attachment on non-text story.");
                    }
                    return GroupSendUtil.sendGroupStoryMessage(this.context, requirePush.requireV2(), list, anyMatch, new MessageId(this.messageId, true), outgoingMediaMessage.getSentTimeMillis(), forFileAttachment);
                }
                throw new UndeliverableMessageException("No group found! " + requirePush);
            } else if (outgoingMediaMessage.isGroup()) {
                OutgoingGroupUpdateMessage outgoingGroupUpdateMessage = (OutgoingGroupUpdateMessage) outgoingMediaMessage;
                if (outgoingGroupUpdateMessage.isV2Group()) {
                    MessageGroupContext.GroupV2Properties requireGroupV2Properties = outgoingGroupUpdateMessage.requireGroupV2Properties();
                    SignalServiceProtos.GroupContextV2 groupContext = requireGroupV2Properties.getGroupContext();
                    SignalServiceGroupV2.Builder withRevision = SignalServiceGroupV2.newBuilder(requireGroupV2Properties.getGroupMasterKey()).withRevision(groupContext.getRevision());
                    ByteString groupChange = groupContext.getGroupChange();
                    if (groupChange != null) {
                        withRevision.withSignedGroupChange(groupChange.toByteArray());
                    }
                    return GroupSendUtil.sendResendableDataMessage(this.context, recipient.requireGroupId().requireV2(), list, anyMatch, ContentHint.IMPLICIT, new MessageId(this.messageId, true), SignalServiceDataMessage.newBuilder().withTimestamp(outgoingMediaMessage.getSentTimeMillis()).withExpiration(recipient.getExpiresInSeconds()).asGroupMessage(withRevision.build()).build());
                }
                throw new UndeliverableMessageException("Messages can no longer be sent to V1 groups!");
            } else {
                Optional<GroupDatabase.GroupRecord> group2 = SignalDatabase.groups().getGroup(recipient.requireGroupId());
                if (group2.isPresent() && group2.get().isAnnouncementGroup() && !group2.get().isAdmin(Recipient.self())) {
                    throw new UndeliverableMessageException("Non-admins cannot send messages in announcement groups!");
                }
                SignalServiceDataMessage.Builder withTimestamp = SignalServiceDataMessage.newBuilder().withTimestamp(outgoingMediaMessage.getSentTimeMillis());
                GroupUtil.setDataMessageGroupContext(this.context, withTimestamp, requirePush);
                SignalServiceDataMessage.Builder withMentions = withTimestamp.withAttachments(attachmentPointersFor).withBody(outgoingMediaMessage.getBody()).withExpiration((int) (outgoingMediaMessage.getExpiresIn() / 1000)).withViewOnce(outgoingMediaMessage.isViewOnce()).asExpirationUpdate(outgoingMediaMessage.isExpirationUpdate()).withProfileKey(profileKey.orElse(null)).withSticker(stickerFor.orElse(null)).withSharedContacts(sharedContactsFor).withPreviews(previewsFor).withMentions(mentionsFor);
                if (outgoingMediaMessage.getParentStoryId() != null) {
                    try {
                        MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(outgoingMediaMessage.getParentStoryId().asMessageId().getId());
                        Recipient self = messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getIndividualRecipient();
                        List<Recipient> list3 = (List) Collection$EL.stream(list).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda23
                            @Override // j$.util.function.Predicate
                            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                                return Predicate.CC.$default$and(this, predicate);
                            }

                            @Override // j$.util.function.Predicate
                            public /* synthetic */ j$.util.function.Predicate negate() {
                                return Predicate.CC.$default$negate(this);
                            }

                            @Override // j$.util.function.Predicate
                            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                                return Predicate.CC.$default$or(this, predicate);
                            }

                            @Override // j$.util.function.Predicate
                            public final boolean test(Object obj) {
                                return PushGroupSendJob.lambda$deliver$3((Recipient) obj);
                            }
                        }).collect(Collectors.toList());
                        SignalServiceDataMessage.StoryContext storyContext = new SignalServiceDataMessage.StoryContext(self.requireServiceId(), messageRecord.getDateSent());
                        withMentions.withStoryContext(storyContext);
                        Optional<SignalServiceDataMessage.Reaction> storyReactionFor = getStoryReactionFor(outgoingMediaMessage, storyContext);
                        if (storyReactionFor.isPresent()) {
                            withMentions.withReaction(storyReactionFor.get());
                            withMentions.withBody(null);
                        }
                        list2 = list3;
                    } catch (NoSuchMessageException e) {
                        throw new UndeliverableMessageException(e);
                    }
                } else {
                    withMentions.withQuote(getQuoteFor(outgoingMediaMessage).orElse(null));
                    list2 = list;
                }
                Log.i(TAG, JobLogger.format(this, "Beginning message send."));
                return GroupSendUtil.sendResendableDataMessage(this.context, (GroupId.V2) recipient.getGroupId().map(new PushGroupSendJob$$ExternalSyntheticLambda24()).orElse(null), list2, anyMatch, ContentHint.RESENDABLE, new MessageId(this.messageId, true), withMentions.build());
            }
        } catch (ServerRejectedException e2) {
            throw new UndeliverableMessageException(e2);
        }
    }

    public static /* synthetic */ boolean lambda$deliver$2(GroupReceiptDatabase.GroupReceiptInfo groupReceiptInfo) {
        return groupReceiptInfo.getStatus() > 0;
    }

    public static /* synthetic */ boolean lambda$deliver$3(Recipient recipient) {
        return recipient.getStoriesCapability() == Recipient.Capability.SUPPORTED;
    }

    public static long getMessageId(Data data) {
        return data.getLong("message_id");
    }

    public static void processGroupMessageResults(Context context, long j, long j2, Recipient recipient, OutgoingMediaMessage outgoingMediaMessage, List<SendMessageResult> list, List<Recipient> list2, List<RecipientId> list3, Set<NetworkFailure> set, Set<IdentityKeyMismatch> set2) throws RetryLaterException, ProofRequiredException {
        ArrayList arrayList;
        boolean z;
        MmsDatabase mms = SignalDatabase.mms();
        RecipientAccessList recipientAccessList = new RecipientAccessList(list2);
        List list4 = Stream.of(list).filter(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((SendMessageResult) obj).isNetworkFailure();
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$4(RecipientAccessList.this, (SendMessageResult) obj);
            }
        }).toList();
        List list5 = Stream.of(list).filter(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda13
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$5((SendMessageResult) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda14
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$6(RecipientAccessList.this, (SendMessageResult) obj);
            }
        }).toList();
        ProofRequiredException proofRequiredException = (ProofRequiredException) Stream.of(list).filter(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$7((SendMessageResult) obj);
            }
        }).findLast().map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((SendMessageResult) obj).getProofRequiredFailure();
            }
        }).orElse(null);
        List list6 = Stream.of(Stream.of(list).filter(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda17
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$8((SendMessageResult) obj);
            }
        }).toList()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda18
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$9(RecipientAccessList.this, (SendMessageResult) obj);
            }
        }).toList();
        Set set3 = (Set) Stream.of(list6).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda19
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (RecipientId) ((Pair) obj).first();
            }
        }).collect(com.annimon.stream.Collectors.toSet());
        Collection<?> list7 = Stream.of(set).filter(new com.annimon.stream.function.Predicate(set3, context) { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda20
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ Context f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$10(this.f$0, this.f$1, (NetworkFailure) obj);
            }
        }).toList();
        Collection<?> list8 = Stream.of(set2).filter(new com.annimon.stream.function.Predicate(set3, context) { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda5
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ Context f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$11(this.f$0, this.f$1, (IdentityKeyMismatch) obj);
            }
        }).toList();
        List<RecipientId> list9 = Stream.of(list).filter(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((SendMessageResult) obj).isUnregisteredFailure();
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushGroupSendJob.lambda$processGroupMessageResults$12((SendMessageResult) obj);
            }
        }).toList();
        ArrayList arrayList2 = new ArrayList(list9);
        arrayList2.addAll(list3);
        if (list4.size() > 0 || list5.size() > 0 || proofRequiredException != null || list9.size() > 0) {
            String str = TAG;
            Locale locale = Locale.US;
            Object[] objArr = new Object[4];
            boolean z2 = false;
            objArr[0] = Integer.valueOf(list4.size());
            objArr[1] = Integer.valueOf(list5.size());
            if (proofRequiredException != null) {
                z2 = true;
            }
            objArr[2] = Boolean.valueOf(z2);
            objArr[3] = Integer.valueOf(list9.size());
            arrayList = arrayList2;
            Log.w(str, String.format(locale, "Failed to send to some recipients. Network: %d, Identity: %d, ProofRequired: %s, Unregistered: %d", objArr));
        } else {
            arrayList = arrayList2;
        }
        RecipientDatabase recipients = SignalDatabase.recipients();
        for (RecipientId recipientId : list9) {
            recipients.markUnregistered(recipientId);
        }
        set.removeAll(list7);
        set.addAll(list4);
        mms.setNetworkFailures(j, set);
        set2.removeAll(list8);
        set2.addAll(list5);
        mms.setMismatchedIdentities(j, set2);
        SignalDatabase.groupReceipts().setUnidentified(list6, j);
        if (proofRequiredException != null) {
            z = true;
            PushSendJob.handleProofRequiredException(context, proofRequiredException, recipient, j2, j, true);
        } else {
            z = true;
        }
        if (set.isEmpty() && set2.isEmpty()) {
            mms.markAsSent(j, z);
            SendJob.markAttachmentsUploaded(j, outgoingMediaMessage);
            if (arrayList.size() > 0) {
                SignalDatabase.groupReceipts().setSkipped(arrayList, j);
            }
            if (outgoingMediaMessage.getExpiresIn() > 0 && !outgoingMediaMessage.isExpirationUpdate()) {
                mms.markExpireStarted(j);
                ApplicationDependencies.getExpiringMessageManager().scheduleDeletion(j, true, outgoingMediaMessage.getExpiresIn());
            }
            if (outgoingMediaMessage.isViewOnce()) {
                SignalDatabase.attachments().deleteAttachmentFilesForViewOnceMessage(j);
            }
            if (outgoingMediaMessage.getStoryType().isStory()) {
                ApplicationDependencies.getExpireStoriesManager().scheduleIfNecessary();
            }
        } else if (!set2.isEmpty()) {
            String str2 = TAG;
            Log.w(str2, "Failing because there were " + set2.size() + " identity mismatches.");
            mms.markAsSentFailed(j);
            PushSendJob.notifyMediaMessageDeliveryFailed(context, j);
            RetrieveProfileJob.enqueue((Set) Stream.of(set2).map(new Function(context) { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda8
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((IdentityKeyMismatch) obj).getRecipientId(this.f$0);
                }
            }).collect(com.annimon.stream.Collectors.toSet()));
        } else if (!list4.isEmpty()) {
            long longValue = ((Long) Collection$EL.stream(list).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda9
                @Override // j$.util.function.Predicate
                public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ j$.util.function.Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return PushGroupSendJob.lambda$processGroupMessageResults$14((SendMessageResult) obj);
                }
            }).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda10
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return PushGroupSendJob.lambda$processGroupMessageResults$15((SendMessageResult) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).max(new Comparator() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda11
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return Long.compare(((Long) obj).longValue(), ((Long) obj2).longValue());
                }
            }).orElse(-1L)).longValue();
            String str3 = TAG;
            Log.w(str3, "Retrying because there were " + list4.size() + " network failures. retryAfter: " + longValue);
            throw new RetryLaterException(longValue);
        }
    }

    public static /* synthetic */ NetworkFailure lambda$processGroupMessageResults$4(RecipientAccessList recipientAccessList, SendMessageResult sendMessageResult) {
        return new NetworkFailure(recipientAccessList.requireIdByAddress(sendMessageResult.getAddress()));
    }

    public static /* synthetic */ boolean lambda$processGroupMessageResults$5(SendMessageResult sendMessageResult) {
        return sendMessageResult.getIdentityFailure() != null;
    }

    public static /* synthetic */ IdentityKeyMismatch lambda$processGroupMessageResults$6(RecipientAccessList recipientAccessList, SendMessageResult sendMessageResult) {
        return new IdentityKeyMismatch(recipientAccessList.requireIdByAddress(sendMessageResult.getAddress()), sendMessageResult.getIdentityFailure().getIdentityKey());
    }

    public static /* synthetic */ boolean lambda$processGroupMessageResults$7(SendMessageResult sendMessageResult) {
        return sendMessageResult.getProofRequiredFailure() != null;
    }

    public static /* synthetic */ boolean lambda$processGroupMessageResults$8(SendMessageResult sendMessageResult) {
        return sendMessageResult.getSuccess() != null;
    }

    public static /* synthetic */ Pair lambda$processGroupMessageResults$9(RecipientAccessList recipientAccessList, SendMessageResult sendMessageResult) {
        return new Pair(recipientAccessList.requireIdByAddress(sendMessageResult.getAddress()), Boolean.valueOf(sendMessageResult.getSuccess().isUnidentified()));
    }

    public static /* synthetic */ boolean lambda$processGroupMessageResults$10(Set set, Context context, NetworkFailure networkFailure) {
        return set.contains(networkFailure.getRecipientId(context));
    }

    public static /* synthetic */ boolean lambda$processGroupMessageResults$11(Set set, Context context, IdentityKeyMismatch identityKeyMismatch) {
        return set.contains(identityKeyMismatch.getRecipientId(context));
    }

    public static /* synthetic */ RecipientId lambda$processGroupMessageResults$12(SendMessageResult sendMessageResult) {
        return RecipientId.from(sendMessageResult.getAddress());
    }

    public static /* synthetic */ boolean lambda$processGroupMessageResults$14(SendMessageResult sendMessageResult) {
        return sendMessageResult.getRateLimitFailure() != null;
    }

    public static /* synthetic */ Long lambda$processGroupMessageResults$15(SendMessageResult sendMessageResult) {
        return sendMessageResult.getRateLimitFailure().getRetryAfterMilliseconds().orElse(-1L);
    }

    private static GroupRecipientResult getGroupMessageRecipients(GroupId groupId, long j) {
        List list;
        List<GroupReceiptDatabase.GroupReceiptInfo> groupReceiptInfo = SignalDatabase.groupReceipts().getGroupReceiptInfo(j);
        if (!groupReceiptInfo.isEmpty()) {
            list = Stream.of(groupReceiptInfo).map(new PushGroupSendJob$$ExternalSyntheticLambda0()).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).distinctBy(new ContactUtil$$ExternalSyntheticLambda3()).toList();
        } else {
            String str = TAG;
            Log.w(str, "No destinations found for group message " + groupId + " using current group membership");
            list = Stream.of(SignalDatabase.groups().getGroupMembers(groupId, GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)).map(new PushGroupSendJob$$ExternalSyntheticLambda1()).distinctBy(new ContactUtil$$ExternalSyntheticLambda3()).toList();
        }
        List<Recipient> eligibleForSending = RecipientUtil.getEligibleForSending(list);
        return new GroupRecipientResult(eligibleForSending, Stream.of(SetUtil.difference(list, eligibleForSending)).map(new ContactUtil$$ExternalSyntheticLambda3()).toList());
    }

    /* loaded from: classes4.dex */
    public static class GroupRecipientResult {
        private final List<RecipientId> skipped;
        private final List<Recipient> target;

        private GroupRecipientResult(List<Recipient> list, List<RecipientId> list2) {
            this.target = list;
            this.skipped = list2;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PushGroupSendJob> {
        public PushGroupSendJob create(Job.Parameters parameters, Data data) {
            String stringOrDefault = data.getStringOrDefault(PushGroupSendJob.KEY_FILTER_RECIPIENTS, "");
            return new PushGroupSendJob(parameters, data.getLong("message_id"), stringOrDefault != null ? new HashSet(RecipientId.fromSerializedList(stringOrDefault)) : Collections.emptySet());
        }
    }
}
