package org.thoughtcrime.securesms.jobs;

import java.util.Objects;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.payments.FailureReason;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.payments.PaymentSubmissionResult;
import org.thoughtcrime.securesms.payments.PaymentTransactionId;
import org.thoughtcrime.securesms.payments.TransactionSubmissionResult;
import org.thoughtcrime.securesms.payments.Wallet;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public final class PaymentSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ADDRESS;
    private static final String KEY_AMOUNT;
    private static final String KEY_FEE;
    private static final String KEY_NOTE;
    private static final String KEY_RECIPIENT;
    private static final String KEY_TIMESTAMP;
    private static final String KEY_UUID;
    static final String QUEUE;
    private static final String TAG = Log.tag(PaymentSendJob.class);
    private final Money amount;
    private final String note;
    private final MobileCoinPublicAddress publicAddress;
    private final RecipientId recipientId;
    private final long timestamp;
    private final Money totalFee;
    private final UUID uuid;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    /* synthetic */ PaymentSendJob(Job.Parameters parameters, UUID uuid, long j, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, String str, Money money, Money money2, AnonymousClass1 r10) {
        this(parameters, uuid, j, recipientId, mobileCoinPublicAddress, str, money, money2);
    }

    public static UUID enqueuePayment(RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, String str, Money money, Money money2) {
        UUID randomUUID = UUID.randomUUID();
        long currentTimeMillis = System.currentTimeMillis();
        Job.Parameters build = new Job.Parameters.Builder().setQueue(QUEUE).setMaxAttempts(1).build();
        Objects.requireNonNull(mobileCoinPublicAddress);
        JobManager.Chain then = ApplicationDependencies.getJobManager().startChain(new PaymentSendJob(build, randomUUID, currentTimeMillis, recipientId, mobileCoinPublicAddress, str, money, money2)).then(new PaymentTransactionCheckJob(randomUUID, QUEUE)).then(new MultiDeviceOutgoingPaymentSyncJob(randomUUID));
        if (recipientId != null) {
            then.then(new PaymentNotificationSendJob(recipientId, randomUUID, recipientId.toQueueKey(true)));
        }
        then.then(PaymentLedgerUpdateJob.updateLedgerToReflectPayment(randomUUID)).enqueue();
        return randomUUID;
    }

    private PaymentSendJob(Job.Parameters parameters, UUID uuid, long j, RecipientId recipientId, MobileCoinPublicAddress mobileCoinPublicAddress, String str, Money money, Money money2) {
        super(parameters);
        this.uuid = uuid;
        this.timestamp = j;
        this.recipientId = recipientId;
        this.publicAddress = mobileCoinPublicAddress;
        this.note = str;
        this.amount = money;
        this.totalFee = money2;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!SignalStore.paymentsValues().mobileCoinPaymentsEnabled()) {
            Log.w(TAG, "Payments are not enabled");
        } else {
            Stopwatch stopwatch = new Stopwatch("Payment submission");
            Wallet wallet = ApplicationDependencies.getPayments().getWallet();
            PaymentDatabase payments = SignalDatabase.payments();
            payments.createOutgoingPayment(this.uuid, this.recipientId, this.publicAddress, this.timestamp, this.note, this.amount);
            String str = TAG;
            Log.i(str, "Payment record created " + this.uuid);
            stopwatch.split("Record created");
            try {
                PaymentSubmissionResult sendPayment = wallet.sendPayment(this.publicAddress, this.amount.requireMobileCoin(), this.totalFee.requireMobileCoin());
                stopwatch.split("Payment submitted");
                if (sendPayment.containsDefrags()) {
                    Log.i(str, "Payment contains " + sendPayment.defrags().size() + " defrags, main payment" + this.uuid);
                    RecipientId id = Recipient.self().getId();
                    MobileCoinPublicAddress mobileCoinPublicAddress = wallet.getMobileCoinPublicAddress();
                    for (TransactionSubmissionResult transactionSubmissionResult : sendPayment.defrags()) {
                        UUID randomUUID = UUID.randomUUID();
                        PaymentTransactionId.MobileCoin mobileCoin = (PaymentTransactionId.MobileCoin) transactionSubmissionResult.getTransactionId();
                        payments.createDefrag(randomUUID, id, mobileCoinPublicAddress, this.timestamp - 1, mobileCoin.getFee(), mobileCoin.getTransaction(), mobileCoin.getReceipt());
                        String str2 = TAG;
                        Log.i(str2, "Defrag entered with id " + randomUUID);
                        ApplicationDependencies.getJobManager().startChain(new PaymentTransactionCheckJob(randomUUID, QUEUE)).then(new MultiDeviceOutgoingPaymentSyncJob(randomUUID)).enqueue();
                        mobileCoinPublicAddress = mobileCoinPublicAddress;
                    }
                    stopwatch.split("Defrag");
                }
                TransactionSubmissionResult.ErrorCode errorCode = sendPayment.getErrorCode();
                int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$TransactionSubmissionResult$ErrorCode[errorCode.ordinal()];
                if (i == 1) {
                    payments.markPaymentFailed(this.uuid, FailureReason.INSUFFICIENT_FUNDS);
                    throw new PaymentException("Payment failed due to " + errorCode);
                } else if (i == 2) {
                    payments.markPaymentFailed(this.uuid, FailureReason.UNKNOWN);
                    throw new PaymentException("Payment failed due to " + errorCode);
                } else if (i != 3) {
                    if (i == 4) {
                        String str3 = TAG;
                        Log.i(str3, "Payment submission complete");
                        TransactionSubmissionResult nonDefrag = sendPayment.getNonDefrag();
                        Objects.requireNonNull(nonDefrag);
                        PaymentTransactionId.MobileCoin mobileCoin2 = (PaymentTransactionId.MobileCoin) nonDefrag.getTransactionId();
                        payments.markPaymentSubmitted(this.uuid, mobileCoin2.getTransaction(), mobileCoin2.getReceipt(), mobileCoin2.getFee());
                        Log.i(str3, "Payment record updated " + this.uuid);
                    }
                    stopwatch.split("Update database record");
                    stopwatch.stop(TAG);
                } else {
                    payments.markPaymentFailed(this.uuid, FailureReason.NETWORK);
                    throw new PaymentException("Payment failed due to " + errorCode);
                }
            } catch (Exception e) {
                Log.w(TAG, "Unknown payment failure", e);
                payments.markPaymentFailed(this.uuid, FailureReason.UNKNOWN);
                throw e;
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.PaymentSendJob$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$TransactionSubmissionResult$ErrorCode;

        static {
            int[] iArr = new int[TransactionSubmissionResult.ErrorCode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$TransactionSubmissionResult$ErrorCode = iArr;
            try {
                iArr[TransactionSubmissionResult.ErrorCode.INSUFFICIENT_FUNDS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$TransactionSubmissionResult$ErrorCode[TransactionSubmissionResult.ErrorCode.GENERIC_FAILURE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$TransactionSubmissionResult$ErrorCode[TransactionSubmissionResult.ErrorCode.NETWORK_FAILURE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$TransactionSubmissionResult$ErrorCode[TransactionSubmissionResult.ErrorCode.NONE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder putLong = new Data.Builder().putString("uuid", this.uuid.toString()).putLong("timestamp", this.timestamp);
        RecipientId recipientId = this.recipientId;
        String str = null;
        Data.Builder putString = putLong.putString("recipient", recipientId != null ? recipientId.serialize() : null);
        MobileCoinPublicAddress mobileCoinPublicAddress = this.publicAddress;
        if (mobileCoinPublicAddress != null) {
            str = mobileCoinPublicAddress.getPaymentAddressBase58();
        }
        return putString.putString(KEY_ADDRESS, str).putString(KEY_NOTE, this.note).putString(KEY_AMOUNT, this.amount.serialize()).putString(KEY_FEE, this.totalFee.serialize()).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Failed to make payment to " + this.recipientId);
    }

    /* loaded from: classes4.dex */
    public static final class PaymentException extends Exception {
        PaymentException(String str) {
            super(str);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PaymentSendJob> {
        public PaymentSendJob create(Job.Parameters parameters, Data data) {
            return new PaymentSendJob(parameters, UUID.fromString(data.getString("uuid")), data.getLong("timestamp"), RecipientId.fromNullable(data.getString("recipient")), MobileCoinPublicAddress.fromBase58NullableOrThrow(data.getString(PaymentSendJob.KEY_ADDRESS)), data.getString(PaymentSendJob.KEY_NOTE), Money.parseOrThrow(data.getString(PaymentSendJob.KEY_AMOUNT)), Money.parseOrThrow(data.getString(PaymentSendJob.KEY_FEE)), null);
        }
    }
}
