package org.thoughtcrime.securesms.jobs;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* compiled from: MultiDeviceSubscriptionSyncRequestJob.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\bH\u0014J\u0014\u0010\n\u001a\u00020\u000b2\n\u0010\f\u001a\u00060\rj\u0002`\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u0010H\u0016¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceSubscriptionSyncRequestJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiDeviceSubscriptionSyncRequestJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String TAG = Log.tag(MultiDeviceSubscriptionSyncRequestJob.class);

    public /* synthetic */ MultiDeviceSubscriptionSyncRequestJob(Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters);
    }

    @JvmStatic
    public static final void enqueue() {
        Companion.enqueue();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    private MultiDeviceSubscriptionSyncRequestJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* compiled from: MultiDeviceSubscriptionSyncRequestJob.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceSubscriptionSyncRequestJob$Companion;", "", "()V", "KEY", "", "TAG", "kotlin.jvm.PlatformType", "enqueue", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void enqueue() {
            Job.Parameters build = new Job.Parameters.Builder().setQueue(MultiDeviceSubscriptionSyncRequestJob.KEY).setMaxInstancesForFactory(2).addConstraint(NetworkConstraint.KEY).setMaxAttempts(10).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder()\n          .set…ts(10)\n          .build()");
            ApplicationDependencies.getJobManager().add(new MultiDeviceSubscriptionSyncRequestJob(build, null));
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data data = Data.EMPTY;
        Intrinsics.checkNotNullExpressionValue(data, "EMPTY");
        return data;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Did not succeed!");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else {
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            Intrinsics.checkNotNullExpressionValue(signalServiceMessageSender, "getSignalServiceMessageSender()");
            signalServiceMessageSender.sendSyncMessage(SignalServiceSyncMessage.forFetchLatest(SignalServiceSyncMessage.FetchType.SUBSCRIPTION_STATUS), UnidentifiedAccessUtil.getAccessForSync(this.context));
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return (exc instanceof PushNetworkException) && !(exc instanceof ServerRejectedException);
    }

    /* compiled from: MultiDeviceSubscriptionSyncRequestJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceSubscriptionSyncRequestJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/MultiDeviceSubscriptionSyncRequestJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceSubscriptionSyncRequestJob> {
        public MultiDeviceSubscriptionSyncRequestJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new MultiDeviceSubscriptionSyncRequestJob(parameters, null);
        }
    }
}
