package org.thoughtcrime.securesms.jobs;

import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.NotificationInd;
import com.google.android.mms.pdu_alt.PduParser;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class MmsReceiveJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_DATA;
    private static final String KEY_SUBSCRIPTION_ID;
    private static final String TAG = Log.tag(MmsReceiveJob.class);
    private byte[] data;
    private int subscriptionId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public MmsReceiveJob(byte[] bArr, int i) {
        this(new Job.Parameters.Builder().setMaxAttempts(25).build(), bArr, i);
    }

    private MmsReceiveJob(Job.Parameters parameters, byte[] bArr, int i) {
        super(parameters);
        this.data = bArr;
        this.subscriptionId = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_DATA, Base64.encodeBytes(this.data)).putInt("subscription_id", this.subscriptionId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        if (this.data == null) {
            Log.w(TAG, "Received NULL pdu, ignoring...");
            return;
        }
        GenericPdu genericPdu = null;
        try {
            genericPdu = new PduParser(this.data).parse();
        } catch (RuntimeException e) {
            Log.w(TAG, e);
        }
        if (isNotification(genericPdu) && isBlocked(genericPdu)) {
            Log.w(TAG, "Received an MMS from a blocked user. Ignoring.");
        } else if (isNotification(genericPdu) && isSelf(genericPdu)) {
            Log.w(TAG, "Received an MMS from ourselves! Ignoring.");
        } else if (isNotification(genericPdu)) {
            Pair<Long, Long> insertMessageInbox = SignalDatabase.mms().insertMessageInbox((NotificationInd) genericPdu, this.subscriptionId);
            Log.i(TAG, "Inserted received MMS notification...");
            ApplicationDependencies.getJobManager().add(new MmsDownloadJob(insertMessageInbox.first().longValue(), insertMessageInbox.second().longValue(), true));
        } else {
            Log.w(TAG, "Unable to process MMS.");
        }
    }

    private boolean isBlocked(GenericPdu genericPdu) {
        if (genericPdu.getFrom() == null || genericPdu.getFrom().getTextString() == null) {
            return false;
        }
        return Recipient.external(this.context, Util.toIsoString(genericPdu.getFrom().getTextString())).isBlocked();
    }

    private boolean isSelf(GenericPdu genericPdu) {
        if (genericPdu.getFrom() == null || genericPdu.getFrom().getTextString() == null) {
            return false;
        }
        return Recipient.external(this.context, Util.toIsoString(genericPdu.getFrom().getTextString())).isSelf();
    }

    private boolean isNotification(GenericPdu genericPdu) {
        return genericPdu != null && genericPdu.getMessageType() == 130;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MmsReceiveJob> {
        public MmsReceiveJob create(Job.Parameters parameters, Data data) {
            try {
                return new MmsReceiveJob(parameters, Base64.decode(data.getString(MmsReceiveJob.KEY_DATA)), data.getInt("subscription_id"));
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}
