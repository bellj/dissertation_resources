package org.thoughtcrime.securesms.jobs;

import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public final class FailingJob extends Job {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private FailingJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Job.Result run() {
        return Job.Result.failure();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<FailingJob> {
        public FailingJob create(Job.Parameters parameters, Data data) {
            return new FailingJob(parameters);
        }
    }
}
