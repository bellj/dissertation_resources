package org.thoughtcrime.securesms.jobs;

import android.app.Application;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.api.groupsv2.NoCredentialForRedemptionTimeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class GroupV1MigrationJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_RECIPIENT_ID;
    private static final long REFRESH_INTERVAL = TimeUnit.HOURS.toMillis(1);
    private static final int ROUTINE_LIMIT;
    private static final String TAG = Log.tag(GroupV1MigrationJob.class);
    private final RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private GroupV1MigrationJob(RecipientId recipientId) {
        this(new Job.Parameters.Builder().setQueue(recipientId.toQueueKey()).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(7)).addConstraint(NetworkConstraint.KEY).build(), recipientId);
    }

    private GroupV1MigrationJob(Job.Parameters parameters, RecipientId recipientId) {
        super(parameters);
        this.recipientId = recipientId;
    }

    public static void enqueuePossibleAutoMigrate(RecipientId recipientId) {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.jobs.GroupV1MigrationJob$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                GroupV1MigrationJob.lambda$enqueuePossibleAutoMigrate$0(RecipientId.this);
            }
        });
    }

    public static /* synthetic */ void lambda$enqueuePossibleAutoMigrate$0(RecipientId recipientId) {
        if (Recipient.resolved(recipientId).isPushV1Group()) {
            ApplicationDependencies.getJobManager().add(new GroupV1MigrationJob(recipientId));
        }
    }

    public static void enqueueRoutineMigrationsIfNecessary(Application application) {
        if (!SignalStore.registrationValues().isRegistrationComplete() || !SignalStore.account().isRegistered() || SignalStore.account().getAci() == null) {
            Log.i(TAG, "Registration not complete. Skipping.");
            return;
        }
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.misc().getLastGv1RoutineMigrationTime();
        if (currentTimeMillis < REFRESH_INTERVAL) {
            String str = TAG;
            Log.i(str, "Too soon to refresh. Did the last refresh " + currentTimeMillis + " ms ago.");
            return;
        }
        SignalStore.misc().setLastGv1RoutineMigrationTime(System.currentTimeMillis());
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.jobs.GroupV1MigrationJob$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                GroupV1MigrationJob.lambda$enqueueRoutineMigrationsIfNecessary$2();
            }
        });
    }

    public static /* synthetic */ void lambda$enqueueRoutineMigrationsIfNecessary$2() {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        List<ThreadRecord> recentV1Groups = SignalDatabase.threads().getRecentV1Groups(20);
        HashSet hashSet = new HashSet();
        if (recentV1Groups.size() > 0) {
            String str = TAG;
            Log.d(str, "About to enqueue refreshes for " + recentV1Groups.size() + " groups.");
        }
        for (ThreadRecord threadRecord : recentV1Groups) {
            jobManager.add(new GroupV1MigrationJob(threadRecord.getRecipient().getId()));
            hashSet.addAll(Stream.of(Recipient.resolvedList(threadRecord.getRecipient().getParticipantIds())).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.GroupV1MigrationJob$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return GroupV1MigrationJob.lambda$enqueueRoutineMigrationsIfNecessary$1((Recipient) obj);
                }
            }).map(new ContactUtil$$ExternalSyntheticLambda3()).toList());
        }
        if (hashSet.size() > 0) {
            String str2 = TAG;
            Log.w(str2, "Enqueuing profile refreshes for " + hashSet.size() + " GV1 participants.");
            RetrieveProfileJob.enqueue(hashSet);
        }
    }

    public static /* synthetic */ boolean lambda$enqueueRoutineMigrationsIfNecessary$1(Recipient recipient) {
        return recipient.getGroupsV1MigrationCapability() != Recipient.Capability.SUPPORTED;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient_id", this.recipientId.serialize()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws IOException, GroupChangeBusyException, RetryLaterException {
        if (Recipient.resolved(this.recipientId).isBlocked()) {
            Log.i(TAG, "Group blocked. Skipping.");
            return;
        }
        try {
            GroupsV1MigrationUtil.migrate(this.context, this.recipientId, false);
        } catch (GroupsV1MigrationUtil.InvalidMigrationStateException unused) {
            Log.w(TAG, "Invalid migration state. Skipping.");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof NoCredentialForRedemptionTimeException) || (exc instanceof GroupChangeBusyException) || (exc instanceof RetryLaterException);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<GroupV1MigrationJob> {
        public GroupV1MigrationJob create(Job.Parameters parameters, Data data) {
            return new GroupV1MigrationJob(parameters, RecipientId.from(data.getString("recipient_id")));
        }
    }
}
