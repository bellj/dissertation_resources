package org.thoughtcrime.securesms.jobs;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.StickerPackOperationMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceStickerPackSyncJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(MultiDeviceStickerPackSyncJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public MultiDeviceStickerPackSyncJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
    }

    public MultiDeviceStickerPackSyncJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else {
            LinkedList linkedList = new LinkedList();
            StickerDatabase.StickerPackRecordReader stickerPackRecordReader = new StickerDatabase.StickerPackRecordReader(SignalDatabase.stickers().getInstalledStickerPacks());
            while (true) {
                try {
                    StickerPackRecord next = stickerPackRecordReader.getNext();
                    if (next != null) {
                        linkedList.add(new StickerPackOperationMessage(Hex.fromStringCondensed(next.getPackId()), Hex.fromStringCondensed(next.getPackKey()), StickerPackOperationMessage.Type.INSTALL));
                    } else {
                        stickerPackRecordReader.close();
                        ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forStickerPackOperations(linkedList), UnidentifiedAccessUtil.getAccessForSync(this.context));
                        return;
                    }
                } catch (Throwable th) {
                    try {
                        stickerPackRecordReader.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to sync sticker pack operation!");
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<MultiDeviceStickerPackSyncJob> {
        public MultiDeviceStickerPackSyncJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceStickerPackSyncJob(parameters);
        }
    }
}
