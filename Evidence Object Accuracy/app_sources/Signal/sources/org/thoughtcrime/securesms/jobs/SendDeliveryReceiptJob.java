package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceReceiptMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class SendDeliveryReceiptJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_MESSAGE_SENT_TIMESTAMP;
    private static final String KEY_RECIPIENT;
    private static final String KEY_TIMESTAMP;
    private static final String TAG = Log.tag(SendReadReceiptJob.class);
    private final MessageId messageId;
    private final long messageSentTimestamp;
    private final RecipientId recipientId;
    private final long timestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public SendDeliveryReceiptJob(RecipientId recipientId, long j, MessageId messageId) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).setQueue(recipientId.toQueueKey()).build(), recipientId, j, messageId, System.currentTimeMillis());
    }

    private SendDeliveryReceiptJob(Job.Parameters parameters, RecipientId recipientId, long j, MessageId messageId, long j2) {
        super(parameters);
        this.recipientId = recipientId;
        this.messageSentTimestamp = j;
        this.messageId = messageId;
        this.timestamp = j2;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder putLong = new Data.Builder().putString("recipient", this.recipientId.serialize()).putLong("message_id", this.messageSentTimestamp).putLong("timestamp", this.timestamp);
        MessageId messageId = this.messageId;
        if (messageId != null) {
            putLong.putString(KEY_MESSAGE_ID, messageId.serialize());
        }
        return putLong.build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException, UndeliverableMessageException {
        if (Recipient.self().isRegistered()) {
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            Recipient resolved = Recipient.resolved(this.recipientId);
            if (resolved.isSelf()) {
                Log.i(TAG, "Not sending to self, abort");
            } else if (resolved.isUnregistered()) {
                String str = TAG;
                Log.w(str, resolved.getId() + " is unregistered!");
            } else {
                SendMessageResult sendReceipt = signalServiceMessageSender.sendReceipt(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, resolved), new SignalServiceReceiptMessage(SignalServiceReceiptMessage.Type.DELIVERY, Collections.singletonList(Long.valueOf(this.messageSentTimestamp)), this.timestamp));
                if (this.messageId != null) {
                    SignalDatabase.messageLog().insertIfPossible(this.recipientId, this.timestamp, sendReceipt, ContentHint.IMPLICIT, this.messageId);
                }
            }
        } else {
            throw new NotPushRegisteredException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof ServerRejectedException) && (exc instanceof PushNetworkException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Failed to send delivery receipt to: " + this.recipientId);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<SendDeliveryReceiptJob> {
        public SendDeliveryReceiptJob create(Job.Parameters parameters, Data data) {
            return new SendDeliveryReceiptJob(parameters, RecipientId.from(data.getString("recipient")), data.getLong("message_id"), data.hasString(SendDeliveryReceiptJob.KEY_MESSAGE_ID) ? MessageId.deserialize(data.getString(SendDeliveryReceiptJob.KEY_MESSAGE_ID)) : null, data.getLong("timestamp"));
        }
    }
}
