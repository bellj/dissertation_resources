package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class PushGroupUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String KEY_SOURCE;
    private static final String TAG = Log.tag(PushGroupUpdateJob.class);
    private final GroupId groupId;
    private final RecipientId source;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public PushGroupUpdateJob(RecipientId recipientId, GroupId groupId) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), recipientId, groupId);
    }

    private PushGroupUpdateJob(Job.Parameters parameters, RecipientId recipientId, GroupId groupId) {
        super(parameters);
        this.source = recipientId;
        this.groupId = groupId;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("source", this.source.serialize()).putString("group_id", this.groupId.toString()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (Recipient.self().isRegistered()) {
            Recipient resolved = Recipient.resolved(this.source);
            if (resolved.isUnregistered()) {
                String str = TAG;
                Log.w(str, resolved.getId() + " not registered!");
                return;
            }
            Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(this.groupId);
            SignalServiceAttachmentStream signalServiceAttachmentStream = null;
            if (group == null || !group.isPresent()) {
                String str2 = TAG;
                Log.w(str2, "No information for group record info request: " + this.groupId.toString());
                return;
            }
            if (AvatarHelper.hasAvatar(this.context, group.get().getRecipientId())) {
                signalServiceAttachmentStream = SignalServiceAttachment.newStreamBuilder().withContentType(MediaUtil.IMAGE_JPEG).withStream(AvatarHelper.getAvatar(this.context, group.get().getRecipientId())).withLength(AvatarHelper.getAvatarLength(this.context, group.get().getRecipientId())).build();
            }
            LinkedList linkedList = new LinkedList();
            for (RecipientId recipientId : group.get().getMembers()) {
                Recipient resolved2 = Recipient.resolved(recipientId);
                if (resolved2.isMaybeRegistered()) {
                    linkedList.add(RecipientUtil.toSignalServiceAddress(this.context, resolved2));
                }
            }
            ApplicationDependencies.getSignalServiceMessageSender().sendDataMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, resolved), ContentHint.DEFAULT, SignalServiceDataMessage.newBuilder().asGroupMessage(SignalServiceGroup.newBuilder(SignalServiceGroup.Type.UPDATE).withAvatar(signalServiceAttachmentStream).withId(this.groupId.getDecodedId()).withMembers(linkedList).withName(group.get().getTitle()).build()).withTimestamp(System.currentTimeMillis()).withExpiration(Recipient.resolved(SignalDatabase.recipients().getOrInsertFromGroupId(this.groupId)).getExpiresInSeconds()).build(), SignalServiceMessageSender.IndividualSendEvents.EMPTY);
            return;
        }
        throw new NotPushRegisteredException();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        Log.w(TAG, exc);
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushGroupUpdateJob> {
        public PushGroupUpdateJob create(Job.Parameters parameters, Data data) {
            return new PushGroupUpdateJob(parameters, RecipientId.from(data.getString("source")), GroupId.parseOrThrow(data.getString("group_id")));
        }
    }
}
