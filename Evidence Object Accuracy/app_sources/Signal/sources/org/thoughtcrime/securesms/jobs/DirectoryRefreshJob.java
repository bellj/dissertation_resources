package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class DirectoryRefreshJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_NOTIFY_OF_NEW_USERS;
    private static final String KEY_RECIPIENT;
    private static final String TAG = Log.tag(DirectoryRefreshJob.class);
    private boolean notifyOfNewUsers;
    private Recipient recipient;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public DirectoryRefreshJob(boolean z) {
        this(null, z);
    }

    public DirectoryRefreshJob(Recipient recipient, boolean z) {
        this(new Job.Parameters.Builder().setQueue(StorageSyncJob.QUEUE_KEY).addConstraint(NetworkConstraint.KEY).setMaxAttempts(10).build(), recipient, z);
    }

    private DirectoryRefreshJob(Job.Parameters parameters, Recipient recipient, boolean z) {
        super(parameters);
        this.recipient = recipient;
        this.notifyOfNewUsers = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder builder = new Data.Builder();
        Recipient recipient = this.recipient;
        return builder.putString("recipient", recipient != null ? recipient.getId().serialize() : null).putBoolean(KEY_NOTIFY_OF_NEW_USERS, this.notifyOfNewUsers).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        Log.i(TAG, "DirectoryRefreshJob.onRun()");
        Recipient recipient = this.recipient;
        if (recipient == null) {
            ContactDiscovery.refreshAll(this.context, this.notifyOfNewUsers);
        } else {
            ContactDiscovery.refresh(this.context, recipient, this.notifyOfNewUsers);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<DirectoryRefreshJob> {
        public DirectoryRefreshJob create(Job.Parameters parameters, Data data) {
            String string = data.hasString("recipient") ? data.getString("recipient") : null;
            return new DirectoryRefreshJob(parameters, string != null ? Recipient.resolved(RecipientId.from(string)) : null, data.getBoolean(DirectoryRefreshJob.KEY_NOTIFY_OF_NEW_USERS));
        }
    }
}
