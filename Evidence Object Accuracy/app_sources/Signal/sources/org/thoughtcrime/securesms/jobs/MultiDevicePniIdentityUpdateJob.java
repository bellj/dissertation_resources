package org.thoughtcrime.securesms.jobs;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public class MultiDevicePniIdentityUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(MultiDevicePniIdentityUpdateJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public MultiDevicePniIdentityUpdateJob() {
        this(new Job.Parameters.Builder().setQueue("__MULTI_DEVICE_PNI_IDENTITY_UPDATE_JOB__").setMaxInstancesForFactory(1).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private MultiDevicePniIdentityUpdateJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else if (SignalStore.account().isLinkedDevice()) {
            Log.i(TAG, "Not primary device, aborting...");
        } else {
            IdentityKeyPair pniIdentityKey = SignalStore.account().getPniIdentityKey();
            ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forPniIdentity(SignalServiceProtos.SyncMessage.PniIdentity.newBuilder().setPublicKey(ByteString.copyFrom(pniIdentityKey.getPublicKey().serialize())).setPrivateKey(ByteString.copyFrom(pniIdentityKey.getPrivateKey().serialize())).build()), UnidentifiedAccessUtil.getAccessForSync(this.context));
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDevicePniIdentityUpdateJob> {
        public MultiDevicePniIdentityUpdateJob create(Job.Parameters parameters, Data data) {
            return new MultiDevicePniIdentityUpdateJob(parameters);
        }
    }
}
