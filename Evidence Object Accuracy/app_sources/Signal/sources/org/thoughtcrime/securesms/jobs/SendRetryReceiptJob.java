package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.message.DecryptionErrorMessage;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public final class SendRetryReceiptJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ERROR_MESSAGE;
    private static final String KEY_GROUP_ID;
    private static final String KEY_RECIPIENT_ID;
    private static final String TAG = Log.tag(SendRetryReceiptJob.class);
    private final DecryptionErrorMessage errorMessage;
    private final Optional<GroupId> groupId;
    private final RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public SendRetryReceiptJob(RecipientId recipientId, Optional<GroupId> optional, DecryptionErrorMessage decryptionErrorMessage) {
        this(recipientId, optional, decryptionErrorMessage, new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(recipientId.toQueueKey()).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
    }

    private SendRetryReceiptJob(RecipientId recipientId, Optional<GroupId> optional, DecryptionErrorMessage decryptionErrorMessage, Job.Parameters parameters) {
        super(parameters);
        this.recipientId = recipientId;
        this.groupId = optional;
        this.errorMessage = decryptionErrorMessage;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder putBlobAsString = new Data.Builder().putString("recipient_id", this.recipientId.serialize()).putBlobAsString(KEY_ERROR_MESSAGE, this.errorMessage.serialize());
        if (this.groupId.isPresent()) {
            putBlobAsString.putBlobAsString("group_id", this.groupId.get().getDecodedId());
        }
        return putBlobAsString.build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        Recipient resolved = Recipient.resolved(this.recipientId);
        if (resolved.isUnregistered()) {
            String str = TAG;
            Log.w(str, resolved.getId() + " not registered!");
            return;
        }
        SignalServiceAddress signalServiceAddress = RecipientUtil.toSignalServiceAddress(this.context, resolved);
        Optional<UnidentifiedAccessPair> accessFor = UnidentifiedAccessUtil.getAccessFor(this.context, resolved);
        Optional<U> map = this.groupId.map(new SendRetryReceiptJob$$ExternalSyntheticLambda0());
        String str2 = TAG;
        Log.i(str2, "Sending retry receipt for " + this.errorMessage.getTimestamp() + " to " + this.recipientId + ", device: " + this.errorMessage.getDeviceId());
        ApplicationDependencies.getSignalServiceMessageSender().sendRetryReceipt(signalServiceAddress, accessFor, map, this.errorMessage);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<SendRetryReceiptJob> {
        public SendRetryReceiptJob create(Job.Parameters parameters, Data data) {
            try {
                return new SendRetryReceiptJob(RecipientId.from(data.getString("recipient_id")), data.hasString("group_id") ? Optional.of(GroupId.pushOrThrow(data.getStringAsBlob("group_id"))) : Optional.empty(), new DecryptionErrorMessage(data.getStringAsBlob(SendRetryReceiptJob.KEY_ERROR_MESSAGE)), parameters);
            } catch (InvalidMessageException e) {
                throw new AssertionError(e);
            }
        }
    }
}
