package org.thoughtcrime.securesms.jobs;

import android.app.Application;
import android.text.TextUtils;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Collection$EL;
import j$.util.function.Consumer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ListUtil;
import org.signal.core.util.SetUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda10;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda9;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.api.crypto.ProfileCipher;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.services.ProfileService;
import org.whispersystems.signalservice.api.util.ExpiringProfileCredentialUtil;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes.dex */
public class RetrieveProfileJob extends BaseJob {
    private static final String DEDUPE_KEY_RETRIEVE_AVATAR;
    public static final String KEY;
    private static final String KEY_RECIPIENTS;
    private static final String TAG = Log.tag(RetrieveProfileJob.class);
    private final Set<RecipientId> recipientIds;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public static void enqueueAsync(RecipientId recipientId) {
        SignalExecutors.BOUNDED_IO.execute(new Runnable() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda11
            @Override // java.lang.Runnable
            public final void run() {
                RetrieveProfileJob.lambda$enqueueAsync$0(RecipientId.this);
            }
        });
    }

    public static /* synthetic */ void lambda$enqueueAsync$0(RecipientId recipientId) {
        ApplicationDependencies.getJobManager().add(forRecipient(recipientId));
    }

    public static void enqueue(RecipientId recipientId) {
        ApplicationDependencies.getJobManager().add(forRecipient(recipientId));
    }

    public static void enqueue(Set<RecipientId> set) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        for (Job job : forRecipients(set)) {
            jobManager.add(job);
        }
    }

    public static Job forRecipient(RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.isSelf()) {
            return new RefreshOwnProfileJob();
        }
        if (resolved.isGroup()) {
            return new RetrieveProfileJob(new HashSet(SignalDatabase.groups().getGroupMemberIds(resolved.requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)));
        }
        return new RetrieveProfileJob(Collections.singleton(recipientId));
    }

    public static List<Job> forRecipients(Set<RecipientId> set) {
        ApplicationDependencies.getApplication();
        HashSet hashSet = new HashSet(set.size());
        boolean z = false;
        for (RecipientId recipientId : set) {
            Recipient resolved = Recipient.resolved(recipientId);
            if (resolved.isSelf()) {
                z = true;
            } else if (resolved.isGroup()) {
                hashSet.addAll(Stream.of(SignalDatabase.groups().getGroupMembers(resolved.requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)).map(new ContactUtil$$ExternalSyntheticLambda3()).toList());
            } else {
                hashSet.add(recipientId);
            }
        }
        ArrayList arrayList = new ArrayList(2);
        if (z) {
            arrayList.add(new RefreshOwnProfileJob());
        }
        if (hashSet.size() > 0) {
            arrayList.add(new RetrieveProfileJob(hashSet));
        }
        return arrayList;
    }

    public static void enqueueRoutineFetchIfNecessary(Application application) {
        if (!SignalStore.registrationValues().isRegistrationComplete() || !SignalStore.account().isRegistered() || SignalStore.account().getAci() == null) {
            Log.i(TAG, "Registration not complete. Skipping.");
            return;
        }
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.misc().getLastProfileRefreshTime();
        if (currentTimeMillis < TimeUnit.HOURS.toMillis(12)) {
            String str = TAG;
            Log.i(str, "Too soon to refresh. Did the last refresh " + currentTimeMillis + " ms ago.");
            return;
        }
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                RetrieveProfileJob.lambda$enqueueRoutineFetchIfNecessary$1();
            }
        });
    }

    public static /* synthetic */ void lambda$enqueueRoutineFetchIfNecessary$1() {
        RecipientDatabase recipients = SignalDatabase.recipients();
        long currentTimeMillis = System.currentTimeMillis();
        TimeUnit timeUnit = TimeUnit.DAYS;
        List<RecipientId> recipientsForRoutineProfileFetch = recipients.getRecipientsForRoutineProfileFetch(currentTimeMillis - timeUnit.toMillis(30), currentTimeMillis - timeUnit.toMillis(1), 50);
        recipientsForRoutineProfileFetch.add(Recipient.self().getId());
        if (recipientsForRoutineProfileFetch.size() > 0) {
            String str = TAG;
            Log.i(str, "Optimistically refreshing " + recipientsForRoutineProfileFetch.size() + " eligible recipient(s).");
            enqueue(new HashSet(recipientsForRoutineProfileFetch));
        } else {
            Log.i(TAG, "No recipients to refresh.");
        }
        SignalStore.misc().setLastProfileRefreshTime(System.currentTimeMillis());
    }

    public RetrieveProfileJob(Set<RecipientId> set) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setMaxAttempts(3).build(), set);
    }

    private RetrieveProfileJob(Job.Parameters parameters, Set<RecipientId> set) {
        super(parameters);
        this.recipientIds = set;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putStringListAsArray("recipients", Stream.of(this.recipientIds).map(new SafetyNumberChangeDialog$$ExternalSyntheticLambda5()).toList()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, RetryLaterException {
        if (!SignalStore.account().isRegistered()) {
            Log.w(TAG, "Unregistered. Skipping.");
            return;
        }
        Stopwatch stopwatch = new Stopwatch("RetrieveProfile");
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientUtil.ensureUuidsAreAvailable(this.context, Stream.of(Recipient.resolvedList(this.recipientIds)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return RetrieveProfileJob.lambda$onRun$2((Recipient) obj);
            }
        }).toList());
        List<Recipient> resolvedList = Recipient.resolvedList(this.recipientIds);
        stopwatch.split("resolve-ensure");
        List list = Stream.of(resolvedList).filter(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda10()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return RetrieveProfileJob.this.lambda$onRun$3((Recipient) obj);
            }
        }).toList();
        stopwatch.split("requests");
        OperationState operationState = (OperationState) Observable.mergeDelayError(list).observeOn(Schedulers.io(), true).scan(new OperationState(), new BiFunction() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return RetrieveProfileJob.lambda$onRun$4((RetrieveProfileJob.OperationState) obj, (Pair) obj2);
            }
        }).lastOrError().blockingGet();
        stopwatch.split("responses");
        Set difference = SetUtil.difference(this.recipientIds, operationState.retries);
        Map<RecipientId, ? extends ServiceId> map = (Map) Stream.of(operationState.profiles).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (Recipient) ((Pair) obj).first();
            }
        }).filterNot(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda9()).collect(Collectors.toMap(new ContactUtil$$ExternalSyntheticLambda3(), new Function() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return RetrieveProfileJob.lambda$onRun$5((Recipient) obj);
            }
        }));
        Collection$EL.stream(ListUtil.chunk(operationState.profiles, 150)).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda10
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                RetrieveProfileJob.this.lambda$onRun$7((List) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        recipients.markProfilesFetched(difference, System.currentTimeMillis());
        if ((operationState.unregistered.size() > 0 || map.size() > 0) && !FeatureFlags.phoneNumberPrivacy()) {
            String str = TAG;
            Log.i(str, "Marking " + map.size() + " users as registered and " + operationState.unregistered.size() + " users as unregistered.");
            recipients.bulkUpdatedRegisteredStatus(map, operationState.unregistered);
        }
        stopwatch.split("process");
        for (Pair<Recipient, ProfileAndCredential> pair : operationState.profiles) {
            setIdentityKey(pair.first(), pair.second().getProfile().getIdentityKey());
        }
        stopwatch.split("identityKeys");
        long count = Stream.of(operationState.profiles).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (Recipient) ((Pair) obj).first();
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((Recipient) obj).getProfileKey();
            }
        }).withoutNulls().count();
        String str2 = TAG;
        Log.d(str2, String.format(Locale.US, "Started with %d recipient(s). Found %d profile(s), and had keys for %d of them. Will retry %d.", Integer.valueOf(resolvedList.size()), Integer.valueOf(operationState.profiles.size()), Long.valueOf(count), Integer.valueOf(operationState.retries.size())));
        stopwatch.stop(str2);
        this.recipientIds.clear();
        this.recipientIds.addAll(operationState.retries);
        if (this.recipientIds.size() > 0) {
            throw new RetryLaterException();
        }
    }

    public static /* synthetic */ boolean lambda$onRun$2(Recipient recipient) {
        return recipient.getRegistered() != RecipientDatabase.RegisteredState.NOT_REGISTERED;
    }

    public /* synthetic */ Observable lambda$onRun$3(Recipient recipient) {
        return ProfileUtil.retrieveProfile(this.context, recipient, getRequestType(recipient)).toObservable();
    }

    public static /* synthetic */ OperationState lambda$onRun$4(OperationState operationState, Pair pair) throws Throwable {
        Recipient recipient = (Recipient) pair.first();
        ProfileService.ProfileResponseProcessor profileResponseProcessor = new ProfileService.ProfileResponseProcessor((ServiceResponse) pair.second());
        if (profileResponseProcessor.hasResult()) {
            operationState.profiles.add(profileResponseProcessor.getResult(recipient));
        } else if (profileResponseProcessor.notFound()) {
            String str = TAG;
            Log.w(str, "Failed to find a profile for " + recipient.getId());
            if (recipient.isRegistered()) {
                operationState.unregistered.add(recipient.getId());
            }
        } else if (profileResponseProcessor.genericIoError()) {
            operationState.retries.add(recipient.getId());
        } else {
            String str2 = TAG;
            Log.w(str2, "Failed to retrieve profile for " + recipient.getId(), profileResponseProcessor.getError());
        }
        return operationState;
    }

    public static /* synthetic */ ServiceId lambda$onRun$5(Recipient recipient) {
        return recipient.getServiceId().orElse(null);
    }

    public /* synthetic */ void lambda$onRun$7(List list) {
        SignalDatabase.runInTransaction(new Runnable(list) { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RetrieveProfileJob.this.lambda$onRun$6(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onRun$6(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            process((Recipient) pair.first(), (ProfileAndCredential) pair.second());
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryLaterException;
    }

    private void process(Recipient recipient, ProfileAndCredential profileAndCredential) {
        SignalServiceProfile profile = profileAndCredential.getProfile();
        ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(recipient.getProfileKey());
        setProfileName(recipient, profile.getName());
        setProfileAbout(recipient, profile.getAbout(), profile.getAboutEmoji());
        setProfileAvatar(recipient, profile.getAvatar());
        setProfileBadges(recipient, profile.getBadges());
        clearUsername(recipient);
        setProfileCapabilities(recipient, profile.getCapabilities());
        setUnidentifiedAccessMode(recipient, profile.getUnidentifiedAccess(), profile.isUnrestrictedUnidentifiedAccess());
        if (profileKeyOrNull != null) {
            profileAndCredential.getExpiringProfileKeyCredential().ifPresent(new Consumer(recipient, profileKeyOrNull) { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda12
                public final /* synthetic */ Recipient f$1;
                public final /* synthetic */ ProfileKey f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    RetrieveProfileJob.this.lambda$process$8(this.f$1, this.f$2, (ExpiringProfileKeyCredential) obj);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ Consumer andThen(Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
        }
    }

    private void setProfileBadges(Recipient recipient, List<SignalServiceProfile.Badge> list) {
        if (list != null) {
            List<Badge> list2 = (List) Collection$EL.stream(list).map(new RefreshOwnProfileJob$$ExternalSyntheticLambda11()).collect(j$.util.stream.Collectors.toList());
            if (list2.size() != recipient.getBadges().size()) {
                String str = TAG;
                Log.i(str, "Likely change in badges for " + recipient.getId() + ". Going from " + recipient.getBadges().size() + " badge(s) to " + list2.size() + ".");
            }
            SignalDatabase.recipients().setBadges(recipient.getId(), list2);
        }
    }

    /* renamed from: setExpiringProfileKeyCredential */
    public void lambda$process$8(Recipient recipient, ProfileKey profileKey, ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        SignalDatabase.recipients().setProfileKeyCredential(recipient.getId(), profileKey, expiringProfileKeyCredential);
    }

    private static SignalServiceProfile.RequestType getRequestType(Recipient recipient) {
        if (ExpiringProfileCredentialUtil.isValid(recipient.getExpiringProfileKeyCredential())) {
            return SignalServiceProfile.RequestType.PROFILE;
        }
        return SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL;
    }

    private void setIdentityKey(Recipient recipient, String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                Log.w(TAG, "Identity key is missing on profile!");
                return;
            }
            IdentityKey identityKey = new IdentityKey(Base64.decode(str), 0);
            if (!ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipient.getId()).isPresent()) {
                String str2 = TAG;
                Log.w(str2, "Still first use for " + recipient.getId());
                return;
            }
            IdentityUtil.saveIdentity(recipient.requireServiceId().toString(), identityKey);
        } catch (IOException | InvalidKeyException e) {
            Log.w(TAG, e);
        }
    }

    private void setUnidentifiedAccessMode(Recipient recipient, String str, boolean z) {
        boolean z2;
        RecipientDatabase recipients = SignalDatabase.recipients();
        ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(recipient.getProfileKey());
        if (z && str != null) {
            RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode = recipient.getUnidentifiedAccessMode();
            RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode2 = RecipientDatabase.UnidentifiedAccessMode.UNRESTRICTED;
            if (unidentifiedAccessMode != unidentifiedAccessMode2) {
                Log.i(TAG, "Marking recipient UD status as unrestricted.");
                recipients.setUnidentifiedAccessMode(recipient.getId(), unidentifiedAccessMode2);
            }
        } else if (profileKeyOrNull == null || str == null) {
            RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode3 = recipient.getUnidentifiedAccessMode();
            RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode4 = RecipientDatabase.UnidentifiedAccessMode.DISABLED;
            if (unidentifiedAccessMode3 != unidentifiedAccessMode4) {
                Log.i(TAG, "Marking recipient UD status as disabled.");
                recipients.setUnidentifiedAccessMode(recipient.getId(), unidentifiedAccessMode4);
            }
        } else {
            try {
                z2 = new ProfileCipher(profileKeyOrNull).verifyUnidentifiedAccess(Base64.decode(str));
            } catch (IOException e) {
                Log.w(TAG, e);
                z2 = false;
            }
            RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode5 = z2 ? RecipientDatabase.UnidentifiedAccessMode.ENABLED : RecipientDatabase.UnidentifiedAccessMode.DISABLED;
            if (recipient.getUnidentifiedAccessMode() != unidentifiedAccessMode5) {
                String str2 = TAG;
                Log.i(str2, "Marking recipient UD status as " + unidentifiedAccessMode5.name() + " after verification.");
                recipients.setUnidentifiedAccessMode(recipient.getId(), unidentifiedAccessMode5);
            }
        }
    }

    private void setProfileName(Recipient recipient, String str) {
        try {
            ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(recipient.getProfileKey());
            if (profileKeyOrNull != null) {
                String emptyIfNull = Util.emptyIfNull(ProfileUtil.decryptString(profileKeyOrNull, str));
                if (TextUtils.isEmpty(emptyIfNull)) {
                    String str2 = TAG;
                    Log.w(str2, "No name set on the profile for " + recipient.getId() + " -- Leaving it alone");
                    return;
                }
                ProfileName fromSerialized = ProfileName.fromSerialized(emptyIfNull);
                ProfileName profileName = recipient.getProfileName();
                if (!fromSerialized.equals(profileName)) {
                    String str3 = TAG;
                    Log.i(str3, "Profile name updated. Writing new value.");
                    SignalDatabase.recipients().setProfileName(recipient.getId(), fromSerialized);
                    String profileName2 = fromSerialized.toString();
                    String profileName3 = profileName.toString();
                    if (recipient.isBlocked() || recipient.isGroup() || recipient.isSelf() || profileName3.isEmpty() || profileName2.equals(profileName3)) {
                        Locale locale = Locale.US;
                        Object[] objArr = new Object[5];
                        boolean z = false;
                        objArr[0] = Boolean.valueOf(recipient.isBlocked());
                        objArr[1] = Boolean.valueOf(recipient.isGroup());
                        objArr[2] = Boolean.valueOf(recipient.isSelf());
                        objArr[3] = Boolean.valueOf(profileName3.isEmpty());
                        if (!profileName2.equals(profileName3)) {
                            z = true;
                        }
                        objArr[4] = Boolean.valueOf(z);
                        Log.i(str3, String.format(locale, "Name changed, but wasn't relevant to write an event. blocked: %s, group: %s, self: %s, firstSet: %s, displayChange: %s", objArr));
                        return;
                    }
                    Log.i(str3, "Writing a profile name change event for " + recipient.getId());
                    SignalDatabase.sms().insertProfileNameChangeMessages(recipient, profileName2, profileName3);
                }
            }
        } catch (IOException e) {
            Log.w(TAG, e);
        } catch (InvalidCiphertextException unused) {
            String str4 = TAG;
            Log.w(str4, "Bad profile key for " + recipient.getId());
        }
    }

    private void setProfileAbout(Recipient recipient, String str, String str2) {
        try {
            ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(recipient.getProfileKey());
            if (profileKeyOrNull != null) {
                SignalDatabase.recipients().setAbout(recipient.getId(), ProfileUtil.decryptString(profileKeyOrNull, str), ProfileUtil.decryptString(profileKeyOrNull, str2));
            }
        } catch (IOException | InvalidCiphertextException e) {
            Log.w(TAG, e);
        }
    }

    private static void setProfileAvatar(Recipient recipient, String str) {
        if (recipient.getProfileKey() != null && !Util.equals(str, recipient.getProfileAvatar())) {
            SignalDatabase.runPostSuccessfulTransaction(DEDUPE_KEY_RETRIEVE_AVATAR + recipient.getId(), new Runnable(str) { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda3
                public final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    RetrieveProfileJob.lambda$setProfileAvatar$10(Recipient.this, this.f$1);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$setProfileAvatar$10(Recipient recipient, String str) {
        SignalExecutors.BOUNDED.execute(new Runnable(str) { // from class: org.thoughtcrime.securesms.jobs.RetrieveProfileJob$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RetrieveProfileJob.lambda$setProfileAvatar$9(Recipient.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$setProfileAvatar$9(Recipient recipient, String str) {
        ApplicationDependencies.getJobManager().add(new RetrieveProfileAvatarJob(recipient, str));
    }

    private void clearUsername(Recipient recipient) {
        SignalDatabase.recipients().setUsername(recipient.getId(), null);
    }

    private void setProfileCapabilities(Recipient recipient, SignalServiceProfile.Capabilities capabilities) {
        if (capabilities != null) {
            SignalDatabase.recipients().setCapabilities(recipient.getId(), capabilities);
        }
    }

    /* loaded from: classes4.dex */
    public static class OperationState {
        final List<Pair<Recipient, ProfileAndCredential>> profiles;
        final Set<RecipientId> retries;
        final Set<RecipientId> unregistered;

        private OperationState() {
            this.retries = new HashSet();
            this.unregistered = new HashSet();
            this.profiles = new ArrayList();
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RetrieveProfileJob> {
        public RetrieveProfileJob create(Job.Parameters parameters, Data data) {
            return new RetrieveProfileJob(parameters, (Set) Stream.of(data.getStringArray("recipients")).map(new SafetyNumberChangeDialog$$ExternalSyntheticLambda3()).collect(Collectors.toSet()));
        }
    }
}
