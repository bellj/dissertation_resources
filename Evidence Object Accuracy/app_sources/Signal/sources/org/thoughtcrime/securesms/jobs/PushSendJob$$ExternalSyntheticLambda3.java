package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import j$.util.Optional;
import org.thoughtcrime.securesms.attachments.Attachment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PushSendJob$$ExternalSyntheticLambda3 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return (Attachment) ((Optional) obj).get();
    }
}
