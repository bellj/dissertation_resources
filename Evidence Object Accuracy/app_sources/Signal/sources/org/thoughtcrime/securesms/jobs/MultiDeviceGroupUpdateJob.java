package org.thoughtcrime.securesms.jobs;

import android.net.Uri;
import android.os.ParcelFileDescriptor;
import j$.util.Optional;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsMapper;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceGroup;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceGroupsOutputStream;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceGroupUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(MultiDeviceGroupUpdateJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public MultiDeviceGroupUpdateJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private MultiDeviceGroupUpdateJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else if (SignalStore.account().isLinkedDevice()) {
            Log.i(TAG, "Not primary device, aborting...");
        } else {
            ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
            boolean z = false;
            Uri createForSingleSessionOnDiskAsync = BlobProvider.getInstance().forData(new ParcelFileDescriptor.AutoCloseInputStream(createPipe[0]), 0).withFileName("multidevice-group-update").createForSingleSessionOnDiskAsync(this.context, new BlobProvider.SuccessListener() { // from class: org.thoughtcrime.securesms.jobs.MultiDeviceGroupUpdateJob$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.providers.BlobProvider.SuccessListener
                public final void onSuccess() {
                    MultiDeviceGroupUpdateJob.lambda$onRun$0();
                }
            }, new BlobProvider.ErrorListener() { // from class: org.thoughtcrime.securesms.jobs.MultiDeviceGroupUpdateJob$$ExternalSyntheticLambda1
                @Override // org.thoughtcrime.securesms.providers.BlobProvider.ErrorListener
                public final void onError(IOException iOException) {
                    MultiDeviceGroupUpdateJob.lambda$onRun$1(iOException);
                }
            });
            try {
                GroupDatabase.Reader groups = SignalDatabase.groups().getGroups();
                DeviceGroupsOutputStream deviceGroupsOutputStream = new DeviceGroupsOutputStream(new ParcelFileDescriptor.AutoCloseOutputStream(createPipe[1]));
                while (true) {
                    GroupDatabase.GroupRecord next = groups.getNext();
                    if (next == null) {
                        break;
                    } else if (next.isV1Group()) {
                        LinkedList linkedList = new LinkedList();
                        for (Recipient recipient : RecipientUtil.getEligibleForSending(Recipient.resolvedList(next.getMembers()))) {
                            linkedList.add(RecipientUtil.toSignalServiceAddress(this.context, recipient));
                        }
                        RecipientId orInsertFromPossiblyMigratedGroupId = SignalDatabase.recipients().getOrInsertFromPossiblyMigratedGroupId(next.getId());
                        Recipient resolved = Recipient.resolved(orInsertFromPossiblyMigratedGroupId);
                        deviceGroupsOutputStream.write(new DeviceGroup(next.getId().getDecodedId(), Optional.ofNullable(next.getTitle()), linkedList, getAvatar(next.getRecipientId()), next.isActive(), resolved.getExpiresInSeconds() > 0 ? Optional.of(Integer.valueOf(resolved.getExpiresInSeconds())) : Optional.empty(), Optional.of(ChatColorsMapper.getMaterialColor(resolved.getChatColors()).serialize()), resolved.isBlocked(), Optional.ofNullable(SignalDatabase.threads().getInboxPositions().get(orInsertFromPossiblyMigratedGroupId)), SignalDatabase.threads().getArchivedRecipients().contains(orInsertFromPossiblyMigratedGroupId)));
                        z = true;
                    }
                }
                deviceGroupsOutputStream.close();
                if (z) {
                    sendUpdate(ApplicationDependencies.getSignalServiceMessageSender(), BlobProvider.getInstance().getStream(this.context, createForSingleSessionOnDiskAsync), BlobProvider.getInstance().calculateFileSize(this.context, createForSingleSessionOnDiskAsync));
                } else {
                    Log.w(TAG, "No groups present for sync message. Sending an empty update.");
                    sendUpdate(ApplicationDependencies.getSignalServiceMessageSender(), null, 0);
                }
                groups.close();
            } finally {
                BlobProvider.getInstance().delete(this.context, createForSingleSessionOnDiskAsync);
            }
        }
    }

    public static /* synthetic */ void lambda$onRun$0() {
        Log.i(TAG, "Write successful.");
    }

    public static /* synthetic */ void lambda$onRun$1(IOException iOException) {
        Log.w(TAG, "Error during write.", iOException);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof ServerRejectedException) && (exc instanceof PushNetworkException)) {
            return true;
        }
        return false;
    }

    private void sendUpdate(SignalServiceMessageSender signalServiceMessageSender, InputStream inputStream, long j) throws IOException, UntrustedIdentityException {
        SignalServiceAttachmentStream signalServiceAttachmentStream;
        if (j > 0) {
            signalServiceAttachmentStream = SignalServiceAttachment.newStreamBuilder().withStream(inputStream).withContentType(MediaUtil.OCTET).withLength(j).build();
        } else {
            signalServiceAttachmentStream = SignalServiceAttachment.emptyStream(MediaUtil.OCTET);
        }
        signalServiceMessageSender.sendSyncMessage(SignalServiceSyncMessage.forGroups(signalServiceAttachmentStream), UnidentifiedAccessUtil.getAccessForSync(this.context));
    }

    private Optional<SignalServiceAttachmentStream> getAvatar(RecipientId recipientId) throws IOException {
        if (!AvatarHelper.hasAvatar(this.context, recipientId)) {
            return Optional.empty();
        }
        return Optional.of(SignalServiceAttachment.newStreamBuilder().withStream(AvatarHelper.getAvatar(this.context, recipientId)).withContentType("image/*").withLength(AvatarHelper.getAvatarLength(this.context, recipientId)).build());
    }

    private File createTempFile(String str) throws IOException {
        File createTempFile = File.createTempFile(str, "tmp", this.context.getCacheDir());
        createTempFile.deleteOnExit();
        return createTempFile;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceGroupUpdateJob> {
        public MultiDeviceGroupUpdateJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceGroupUpdateJob(parameters);
        }
    }
}
