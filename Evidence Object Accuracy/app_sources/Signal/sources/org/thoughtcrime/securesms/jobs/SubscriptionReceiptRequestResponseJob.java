package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.donations.StripeDeclineCode;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredential;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialRequestContext;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialResponse;
import org.signal.libsignal.zkgroup.receipts.ReceiptSerial;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes4.dex */
public class SubscriptionReceiptRequestResponseJob extends BaseJob {
    private static final String DATA_IS_FOR_KEEP_ALIVE;
    private static final String DATA_REQUEST_BYTES;
    private static final String DATA_SUBSCRIBER_ID;
    public static final String KEY;
    public static final Object MUTEX = new Object();
    private static final String TAG = Log.tag(SubscriptionReceiptRequestResponseJob.class);
    private final boolean isForKeepAlive;
    private final ReceiptCredentialRequestContext requestContext;
    private final SubscriberId subscriberId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private static SubscriptionReceiptRequestResponseJob createJob(SubscriberId subscriberId, boolean z) {
        return new SubscriptionReceiptRequestResponseJob(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(DonationReceiptRedemptionJob.SUBSCRIPTION_QUEUE).setMaxInstancesForQueue(1).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), generateRequestContext(), subscriberId, z);
    }

    public static ReceiptCredentialRequestContext generateRequestContext() {
        Log.d(TAG, "Generating request credentials context for token redemption...", true);
        try {
            return ApplicationDependencies.getClientZkReceiptOperations().createReceiptCredentialRequestContext(new SecureRandom(), new ReceiptSerial(Util.getSecretBytes(16)));
        } catch (InvalidInputException | VerificationFailedException e) {
            Log.e(TAG, "Failed to create credential.", e);
            throw new AssertionError(e);
        }
    }

    public static JobManager.Chain createSubscriptionContinuationJobChain() {
        return createSubscriptionContinuationJobChain(false);
    }

    public static JobManager.Chain createSubscriptionContinuationJobChain(boolean z) {
        SubscriptionReceiptRequestResponseJob createJob = createJob(SignalStore.donationsValues().requireSubscriber().getSubscriberId(), z);
        DonationReceiptRedemptionJob createJobForSubscription = DonationReceiptRedemptionJob.createJobForSubscription(createJob.getErrorSource());
        RefreshOwnProfileJob forSubscription = RefreshOwnProfileJob.forSubscription();
        return ApplicationDependencies.getJobManager().startChain(createJob).then(createJobForSubscription).then(forSubscription).then(new MultiDeviceProfileContentUpdateJob());
    }

    private SubscriptionReceiptRequestResponseJob(Job.Parameters parameters, ReceiptCredentialRequestContext receiptCredentialRequestContext, SubscriberId subscriberId, boolean z) {
        super(parameters);
        this.requestContext = receiptCredentialRequestContext;
        this.subscriberId = subscriberId;
        this.isForKeepAlive = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putBlobAsString(DATA_SUBSCRIBER_ID, this.subscriberId.getBytes()).putBoolean(DATA_IS_FOR_KEEP_ALIVE, this.isForKeepAlive).putBlobAsString(DATA_REQUEST_BYTES, this.requestContext.serialize()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        synchronized (MUTEX) {
            doRun();
        }
    }

    private void doRun() throws Exception {
        ActiveSubscription latestSubscriptionInformation = getLatestSubscriptionInformation();
        ActiveSubscription.Subscription activeSubscription = latestSubscriptionInformation.getActiveSubscription();
        if (activeSubscription == null) {
            Log.w(TAG, "Subscription is null.", true);
            throw new RetryableException();
        } else if (activeSubscription.isFailedPayment()) {
            ActiveSubscription.ChargeFailure chargeFailure = latestSubscriptionInformation.getChargeFailure();
            if (chargeFailure != null) {
                String str = TAG;
                Log.w(str, "Subscription payment charge failure code: " + chargeFailure.getCode() + ", message: " + chargeFailure.getMessage(), true);
            }
            if (this.isForKeepAlive) {
                String str2 = TAG;
                Log.w(str2, "Subscription payment failure in active subscription response (status = " + activeSubscription.getStatus() + ").", true);
                onPaymentFailure(activeSubscription.getStatus(), chargeFailure, activeSubscription.getEndOfCurrentPeriod(), true);
                throw new Exception("Active subscription hit a payment failure: " + activeSubscription.getStatus());
            }
            String str3 = TAG;
            Log.w(str3, "New subscription has hit a payment failure. (status = " + activeSubscription.getStatus() + ").", true);
            onPaymentFailure(activeSubscription.getStatus(), chargeFailure, activeSubscription.getEndOfCurrentPeriod(), false);
            throw new Exception("New subscription has hit a payment failure: " + activeSubscription.getStatus());
        } else if (!activeSubscription.isActive()) {
            ActiveSubscription.ChargeFailure chargeFailure2 = latestSubscriptionInformation.getChargeFailure();
            if (chargeFailure2 != null) {
                String str4 = TAG;
                Log.w(str4, "Subscription payment charge failure code: " + chargeFailure2.getCode() + ", message: " + chargeFailure2.getMessage(), true);
                if (!this.isForKeepAlive) {
                    Log.w(str4, "Initial subscription payment failed, treating as a permanent failure.");
                    onPaymentFailure(activeSubscription.getStatus(), chargeFailure2, activeSubscription.getEndOfCurrentPeriod(), false);
                    throw new Exception("New subscription has hit a payment failure.");
                }
            }
            String str5 = TAG;
            Log.w(str5, "Subscription is not yet active. Status: " + activeSubscription.getStatus(), true);
            throw new RetryableException();
        } else if (!activeSubscription.isCanceled()) {
            String str6 = TAG;
            Log.i(str6, "Recording end of period from active subscription: " + activeSubscription.getStatus(), true);
            SignalStore.donationsValues().setLastEndOfPeriod(activeSubscription.getEndOfCurrentPeriod());
            MultiDeviceSubscriptionSyncRequestJob.enqueue();
            Log.d(str6, "Submitting receipt credential request.");
            ServiceResponse<ReceiptCredentialResponse> blockingGet = ApplicationDependencies.getDonationsService().submitReceiptCredentialRequest(this.subscriberId, this.requestContext.getRequest()).blockingGet();
            if (blockingGet.getApplicationError().isPresent()) {
                handleApplicationError(blockingGet);
            } else if (blockingGet.getResult().isPresent()) {
                ReceiptCredential receiptCredential = getReceiptCredential(blockingGet.getResult().get());
                if (isCredentialValid(activeSubscription, receiptCredential)) {
                    ReceiptCredentialPresentation receiptCredentialPresentation = getReceiptCredentialPresentation(receiptCredential);
                    Log.d(str6, "Validated credential. Recording receipt and handing off to redemption job.", true);
                    SignalDatabase.donationReceipts().addReceipt(DonationReceiptRecord.createForSubscription(activeSubscription));
                    setOutputData(new Data.Builder().putBlobAsString(DonationReceiptRedemptionJob.INPUT_RECEIPT_CREDENTIAL_PRESENTATION, receiptCredentialPresentation.serialize()).build());
                    return;
                }
                DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(getErrorSource()));
                throw new IOException("Could not validate receipt credential");
            } else {
                Log.w(str6, "Encountered a retryable exception: " + blockingGet.getStatus(), blockingGet.getExecutionError().orElse(null), true);
                throw new RetryableException();
            }
        } else {
            Log.w(TAG, "Subscription is marked as cancelled, but it's possible that the user cancelled and then later tried to resubscribe. Scheduling a retry.", true);
            throw new RetryableException();
        }
    }

    private ActiveSubscription getLatestSubscriptionInformation() throws Exception {
        ServiceResponse<ActiveSubscription> blockingGet = ApplicationDependencies.getDonationsService().getSubscription(this.subscriberId).blockingGet();
        if (blockingGet.getResult().isPresent()) {
            return blockingGet.getResult().get();
        }
        if (blockingGet.getApplicationError().isPresent()) {
            Log.w(TAG, "Unrecoverable error getting the user's current subscription. Failing.", blockingGet.getApplicationError().get(), true);
            DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(getErrorSource()));
            throw new IOException(blockingGet.getApplicationError().get());
        }
        throw new RetryableException();
    }

    private ReceiptCredentialPresentation getReceiptCredentialPresentation(ReceiptCredential receiptCredential) throws RetryableException {
        try {
            return ApplicationDependencies.getClientZkReceiptOperations().createReceiptCredentialPresentation(receiptCredential);
        } catch (VerificationFailedException e) {
            Log.w(TAG, "getReceiptCredentialPresentation: encountered a verification failure in zk", e, true);
            throw new RetryableException();
        }
    }

    private ReceiptCredential getReceiptCredential(ReceiptCredentialResponse receiptCredentialResponse) throws RetryableException {
        try {
            return ApplicationDependencies.getClientZkReceiptOperations().receiveReceiptCredential(this.requestContext, receiptCredentialResponse);
        } catch (VerificationFailedException e) {
            Log.w(TAG, "getReceiptCredential: encountered a verification failure in zk", e, true);
            throw new RetryableException();
        }
    }

    private void handleApplicationError(ServiceResponse<ReceiptCredentialResponse> serviceResponse) throws Exception {
        int status = serviceResponse.getStatus();
        if (status == 204) {
            Log.w(TAG, "Payment is still processing. Trying again.", serviceResponse.getApplicationError().get(), true);
            SignalStore.donationsValues().clearSubscriptionRedemptionFailed();
            throw new RetryableException();
        } else if (status == 400) {
            Log.w(TAG, "Receipt credential request failed to validate.", serviceResponse.getApplicationError().get(), true);
            DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(getErrorSource()));
            throw new Exception(serviceResponse.getApplicationError().get());
        } else if (status != 409) {
            switch (status) {
                case 402:
                    Log.w(TAG, "Payment looks like a failure but may be retried.", serviceResponse.getApplicationError().get(), true);
                    throw new RetryableException();
                case 403:
                    Log.w(TAG, "SubscriberId password mismatch or account auth was present.", serviceResponse.getApplicationError().get(), true);
                    DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(getErrorSource()));
                    throw new Exception(serviceResponse.getApplicationError().get());
                case 404:
                    Log.w(TAG, "SubscriberId not found or misformed.", serviceResponse.getApplicationError().get(), true);
                    DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(getErrorSource()));
                    throw new Exception(serviceResponse.getApplicationError().get());
                default:
                    String str = TAG;
                    Log.w(str, "Encountered a server failure response: " + serviceResponse.getStatus(), serviceResponse.getApplicationError().get(), true);
                    throw new RetryableException();
            }
        } else {
            onAlreadyRedeemed(serviceResponse);
            throw new Exception(serviceResponse.getApplicationError().get());
        }
    }

    private void onPaymentFailure(String str, ActiveSubscription.ChargeFailure chargeFailure, long j, boolean z) {
        DonationError donationError;
        SignalStore.donationsValues().setShouldCancelSubscriptionBeforeNextSubscribeAttempt(true);
        if (z) {
            Log.d(TAG, "Is for a keep-alive and we have a status. Setting UnexpectedSubscriptionCancelation state...", true);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationChargeFailure(chargeFailure);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationReason(str);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationTimestamp(j);
            MultiDeviceSubscriptionSyncRequestJob.enqueue();
        } else if (chargeFailure != null) {
            String str2 = TAG;
            Log.d(str2, "Charge failure detected: " + chargeFailure, true);
            StripeDeclineCode fromCode = StripeDeclineCode.Companion.getFromCode(chargeFailure.getOutcomeNetworkReason());
            if (fromCode.isKnown()) {
                donationError = new DonationError.PaymentSetupError.DeclinedError(getErrorSource(), new Exception(chargeFailure.getMessage()), fromCode);
            } else {
                DonationErrorSource errorSource = getErrorSource();
                donationError = new DonationError.PaymentSetupError.CodedError(errorSource, new Exception("Card was declined. " + chargeFailure.getCode()), chargeFailure.getCode());
            }
            Log.w(str2, "Not for a keep-alive and we have a charge failure. Routing a payment setup error...", true);
            DonationError.routeDonationError(this.context, donationError);
        } else {
            Log.d(TAG, "Not for a keep-alive and we have a failure status. Routing a payment setup error...", true);
            DonationError.routeDonationError(this.context, new DonationError.PaymentSetupError.GenericError(getErrorSource(), new Exception("Got a failure status from the subscription object.")));
        }
    }

    private void onAlreadyRedeemed(ServiceResponse<ReceiptCredentialResponse> serviceResponse) {
        if (this.isForKeepAlive) {
            Log.i(TAG, "KeepAlive: Latest paid receipt on subscription already redeemed with a different request credential, ignoring.", serviceResponse.getApplicationError().get(), true);
            return;
        }
        Log.w(TAG, "Latest paid receipt on subscription already redeemed with a different request credential.", serviceResponse.getApplicationError().get(), true);
        DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(getErrorSource()));
    }

    private DonationErrorSource getErrorSource() {
        return this.isForKeepAlive ? DonationErrorSource.KEEP_ALIVE : DonationErrorSource.SUBSCRIPTION;
    }

    private static boolean isCredentialValid(ActiveSubscription.Subscription subscription, ReceiptCredential receiptCredential) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        long seconds2 = TimeUnit.DAYS.toSeconds(90) + seconds;
        boolean z = ((long) subscription.getLevel()) == receiptCredential.getReceiptLevel();
        boolean z2 = subscription.getEndOfCurrentPeriod() < receiptCredential.getReceiptExpirationTime();
        boolean z3 = receiptCredential.getReceiptExpirationTime() % 86400 == 0;
        boolean z4 = receiptCredential.getReceiptExpirationTime() > seconds;
        boolean z5 = receiptCredential.getReceiptExpirationTime() <= seconds2;
        String str = TAG;
        Log.d(str, "Credential validation: isSameLevel(" + z + ") isExpirationAfterSub(" + z2 + ") isExpiration86400(" + z3 + ") isExpirationInTheFuture(" + z4 + ") isExpirationWithinMax(" + z5 + ")", true);
        if (!z || !z2 || !z3 || !z4 || !z5) {
            return false;
        }
        return true;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryableException;
    }

    /* loaded from: classes4.dex */
    public static final class RetryableException extends Exception {
        RetryableException() {
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<SubscriptionReceiptRequestResponseJob> {
        public SubscriptionReceiptRequestResponseJob create(Job.Parameters parameters, Data data) {
            ReceiptCredentialRequestContext receiptCredentialRequestContext;
            SubscriberId fromBytes = SubscriberId.fromBytes(data.getStringAsBlob(SubscriptionReceiptRequestResponseJob.DATA_SUBSCRIBER_ID));
            boolean booleanOrDefault = data.getBooleanOrDefault(SubscriptionReceiptRequestResponseJob.DATA_IS_FOR_KEEP_ALIVE, false);
            byte[] stringAsBlob = data.getStringAsBlob(SubscriptionReceiptRequestResponseJob.DATA_REQUEST_BYTES);
            if (stringAsBlob == null) {
                Log.i(SubscriptionReceiptRequestResponseJob.TAG, "Generating a request context for a legacy instance of SubscriptionReceiptRequestResponseJob", true);
                receiptCredentialRequestContext = SubscriptionReceiptRequestResponseJob.generateRequestContext();
            } else {
                try {
                    receiptCredentialRequestContext = new ReceiptCredentialRequestContext(stringAsBlob);
                } catch (InvalidInputException e) {
                    Log.e(SubscriptionReceiptRequestResponseJob.TAG, "Failed to generate request context from bytes", e);
                    throw new AssertionError(e);
                }
            }
            return new SubscriptionReceiptRequestResponseJob(parameters, receiptCredentialRequestContext, fromBytes, booleanOrDefault);
        }
    }
}
