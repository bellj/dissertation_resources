package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.MessageRequestResponseMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceMessageRequestResponseJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_THREAD_RECIPIENT;
    private static final String KEY_TYPE;
    private static final String TAG = Log.tag(MultiDeviceMessageRequestResponseJob.class);
    private final RecipientId threadRecipient;
    private final Type type;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* synthetic */ MultiDeviceMessageRequestResponseJob(Job.Parameters parameters, RecipientId recipientId, Type type, AnonymousClass1 r4) {
        this(parameters, recipientId, type);
    }

    public static MultiDeviceMessageRequestResponseJob forAccept(RecipientId recipientId) {
        return new MultiDeviceMessageRequestResponseJob(recipientId, Type.ACCEPT);
    }

    public static MultiDeviceMessageRequestResponseJob forDelete(RecipientId recipientId) {
        return new MultiDeviceMessageRequestResponseJob(recipientId, Type.DELETE);
    }

    public static MultiDeviceMessageRequestResponseJob forBlock(RecipientId recipientId) {
        return new MultiDeviceMessageRequestResponseJob(recipientId, Type.BLOCK);
    }

    public static MultiDeviceMessageRequestResponseJob forBlockAndDelete(RecipientId recipientId) {
        return new MultiDeviceMessageRequestResponseJob(recipientId, Type.BLOCK_AND_DELETE);
    }

    public static MultiDeviceMessageRequestResponseJob forBlockAndReportSpam(RecipientId recipientId) {
        return new MultiDeviceMessageRequestResponseJob(recipientId, Type.BLOCK);
    }

    private MultiDeviceMessageRequestResponseJob(RecipientId recipientId, Type type) {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(1)).build(), recipientId, type);
    }

    private MultiDeviceMessageRequestResponseJob(Job.Parameters parameters, RecipientId recipientId, Type type) {
        super(parameters);
        this.threadRecipient = recipientId;
        this.type = type;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_THREAD_RECIPIENT, this.threadRecipient.serialize()).putInt("type", this.type.serialize()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        MessageRequestResponseMessage messageRequestResponseMessage;
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else {
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            Recipient resolved = Recipient.resolved(this.threadRecipient);
            if (resolved.isGroup() || resolved.hasServiceId()) {
                if (resolved.isGroup()) {
                    messageRequestResponseMessage = MessageRequestResponseMessage.forGroup(resolved.getGroupId().get().getDecodedId(), localToRemoteType(this.type));
                } else {
                    messageRequestResponseMessage = resolved.isMaybeRegistered() ? MessageRequestResponseMessage.forIndividual(RecipientUtil.getOrFetchServiceId(this.context, resolved), localToRemoteType(this.type)) : null;
                }
                if (messageRequestResponseMessage != null) {
                    signalServiceMessageSender.sendSyncMessage(SignalServiceSyncMessage.forMessageRequestResponse(messageRequestResponseMessage), UnidentifiedAccessUtil.getAccessForSync(this.context));
                    return;
                }
                String str = TAG;
                Log.w(str, resolved.getId() + " not registered!");
                return;
            }
            Log.i(TAG, "Queued for non-group recipient without ServiceId");
        }
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.MultiDeviceMessageRequestResponseJob$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceMessageRequestResponseJob$Type;

        static {
            int[] iArr = new int[Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceMessageRequestResponseJob$Type = iArr;
            try {
                iArr[Type.ACCEPT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceMessageRequestResponseJob$Type[Type.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceMessageRequestResponseJob$Type[Type.BLOCK.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceMessageRequestResponseJob$Type[Type.BLOCK_AND_DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    private static MessageRequestResponseMessage.Type localToRemoteType(Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceMessageRequestResponseJob$Type[type.ordinal()];
        if (i == 1) {
            return MessageRequestResponseMessage.Type.ACCEPT;
        }
        if (i == 2) {
            return MessageRequestResponseMessage.Type.DELETE;
        }
        if (i == 3) {
            return MessageRequestResponseMessage.Type.BLOCK;
        }
        if (i != 4) {
            return MessageRequestResponseMessage.Type.UNKNOWN;
        }
        return MessageRequestResponseMessage.Type.BLOCK_AND_DELETE;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public enum Type {
        UNKNOWN(0),
        ACCEPT(1),
        DELETE(2),
        BLOCK(3),
        BLOCK_AND_DELETE(4);
        
        private final int value;

        Type(int i) {
            this.value = i;
        }

        int serialize() {
            return this.value;
        }

        static Type deserialize(int i) {
            Type[] values = values();
            for (Type type : values) {
                if (type.value == i) {
                    return type;
                }
            }
            throw new AssertionError("Unknown type: " + i);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceMessageRequestResponseJob> {
        public MultiDeviceMessageRequestResponseJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceMessageRequestResponseJob(parameters, RecipientId.from(data.getString(MultiDeviceMessageRequestResponseJob.KEY_THREAD_RECIPIENT)), Type.deserialize(data.getInt("type")), null);
        }
    }
}
