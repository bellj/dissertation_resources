package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes4.dex */
public final class AvatarGroupsV2DownloadJob extends BaseJob {
    private static final long AVATAR_DOWNLOAD_FAIL_SAFE_MAX_SIZE = ByteUnit.MEGABYTES.toBytes(5);
    private static final String CDN_KEY;
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String TAG = Log.tag(AvatarGroupsV2DownloadJob.class);
    private final String cdnKey;
    private final GroupId.V2 groupId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AvatarGroupsV2DownloadJob(org.thoughtcrime.securesms.groups.GroupId.V2 r4, java.lang.String r5) {
        /*
            r3 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.String r1 = "NetworkConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "AvatarGroupsV2DownloadJob::"
            r1.append(r2)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            r1 = 10
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxAttempts(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            r3.<init>(r0, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.AvatarGroupsV2DownloadJob.<init>(org.thoughtcrime.securesms.groups.GroupId$V2, java.lang.String):void");
    }

    private AvatarGroupsV2DownloadJob(Job.Parameters parameters, GroupId.V2 v2, String str) {
        super(parameters);
        this.groupId = v2;
        this.cdnKey = str;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("group_id", this.groupId.toString()).putString(CDN_KEY, this.cdnKey).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        GroupDatabase groups = SignalDatabase.groups();
        Optional<GroupDatabase.GroupRecord> group = groups.getGroup(this.groupId);
        try {
            if (!group.isPresent()) {
                Log.w(TAG, "Cannot download avatar for unknown group");
                return;
            }
            ByteArrayInputStream byteArrayInputStream = null;
            if (this.cdnKey.length() == 0) {
                String str = TAG;
                Log.w(str, "Removing avatar for group " + this.groupId);
                AvatarHelper.setAvatar(this.context, group.get().getRecipientId(), null);
                groups.onAvatarUpdated(this.groupId, false);
                return;
            }
            String str2 = TAG;
            Log.i(str2, "Downloading new avatar for group " + this.groupId);
            byte[] downloadGroupAvatarBytes = downloadGroupAvatarBytes(this.context, group.get().requireV2GroupProperties().getGroupMasterKey(), this.cdnKey);
            Context context = this.context;
            RecipientId recipientId = group.get().getRecipientId();
            if (downloadGroupAvatarBytes != null) {
                byteArrayInputStream = new ByteArrayInputStream(downloadGroupAvatarBytes);
            }
            AvatarHelper.setAvatar(context, recipientId, byteArrayInputStream);
            groups.onAvatarUpdated(this.groupId, true);
        } catch (NonSuccessfulResponseCodeException e) {
            Log.w(TAG, e);
        }
    }

    public static byte[] downloadGroupAvatarBytes(Context context, GroupMasterKey groupMasterKey, String str) throws IOException {
        if (str.length() == 0) {
            return null;
        }
        GroupSecretParams deriveFromMasterKey = GroupSecretParams.deriveFromMasterKey(groupMasterKey);
        File createTempFile = File.createTempFile("avatar", "gv2", context.getCacheDir());
        createTempFile.deleteOnExit();
        try {
            FileInputStream retrieveGroupsV2ProfileAvatar = ApplicationDependencies.getSignalServiceMessageReceiver().retrieveGroupsV2ProfileAvatar(str, createTempFile, AVATAR_DOWNLOAD_FAIL_SAFE_MAX_SIZE);
            byte[] bArr = new byte[(int) createTempFile.length()];
            StreamUtil.readFully(retrieveGroupsV2ProfileAvatar, bArr);
            byte[] decryptAvatar = ApplicationDependencies.getGroupsV2Operations().forGroup(deriveFromMasterKey).decryptAvatar(bArr);
            if (retrieveGroupsV2ProfileAvatar != null) {
                retrieveGroupsV2ProfileAvatar.close();
            }
            return decryptAvatar;
        } finally {
            if (createTempFile.exists() && !createTempFile.delete()) {
                Log.w(TAG, "Unable to delete temp avatar file");
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AvatarGroupsV2DownloadJob> {
        public AvatarGroupsV2DownloadJob create(Job.Parameters parameters, Data data) {
            return new AvatarGroupsV2DownloadJob(parameters, GroupId.parseOrThrow(data.getString("group_id")).requireV2(), data.getString(AvatarGroupsV2DownloadJob.CDN_KEY));
        }
    }
}
