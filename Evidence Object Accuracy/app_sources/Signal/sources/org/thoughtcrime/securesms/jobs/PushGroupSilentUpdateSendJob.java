package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.InvalidProtocolBufferException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupV2;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class PushGroupSilentUpdateSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_CONTEXT_V2;
    private static final String KEY_INITIAL_RECIPIENT_COUNT;
    private static final String KEY_RECIPIENTS;
    private static final String KEY_TIMESTAMP;
    private static final String TAG = Log.tag(PushGroupSilentUpdateSendJob.class);
    private final SignalServiceProtos.GroupContextV2 groupContextV2;
    private final int initialRecipientCount;
    private final List<RecipientId> recipients;
    private final long timestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public static Job create(Context context, GroupId.V2 v2, DecryptedGroup decryptedGroup, OutgoingGroupUpdateMessage outgoingGroupUpdateMessage) {
        Set set = (Set) Stream.concat(Stream.of(DecryptedGroupUtil.toUuidList(decryptedGroup.getMembersList())), Stream.of(DecryptedGroupUtil.pendingToUuidList(decryptedGroup.getPendingMembersList()))).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSilentUpdateSendJob$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSilentUpdateSendJob.lambda$create$0((UUID) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSilentUpdateSendJob$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSilentUpdateSendJob.lambda$create$1((UUID) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSilentUpdateSendJob$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushGroupSilentUpdateSendJob.lambda$create$2((UUID) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.PushGroupSilentUpdateSendJob$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return PushGroupSilentUpdateSendJob.lambda$create$3((Recipient) obj);
            }
        }).map(new ContactUtil$$ExternalSyntheticLambda3()).collect(Collectors.toSet());
        return new PushGroupSilentUpdateSendJob(new ArrayList(set), set.size(), outgoingGroupUpdateMessage.getSentTimeMillis(), outgoingGroupUpdateMessage.requireGroupV2Properties().getGroupContext(), new Job.Parameters.Builder().setQueue(Recipient.externalGroupExact(v2).getId().toQueueKey()).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    public static /* synthetic */ boolean lambda$create$0(UUID uuid) {
        return !UuidUtil.UNKNOWN_UUID.equals(uuid);
    }

    public static /* synthetic */ boolean lambda$create$1(UUID uuid) {
        return !SignalStore.account().requireAci().uuid().equals(uuid);
    }

    public static /* synthetic */ Recipient lambda$create$2(UUID uuid) {
        return Recipient.externalPush(ServiceId.from(uuid));
    }

    public static /* synthetic */ boolean lambda$create$3(Recipient recipient) {
        return recipient.getRegistered() != RecipientDatabase.RegisteredState.NOT_REGISTERED;
    }

    private PushGroupSilentUpdateSendJob(List<RecipientId> list, int i, long j, SignalServiceProtos.GroupContextV2 groupContextV2, Job.Parameters parameters) {
        super(parameters);
        this.recipients = list;
        this.initialRecipientCount = i;
        this.groupContextV2 = groupContextV2;
        this.timestamp = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipients", RecipientId.toSerializedList(this.recipients)).putInt(KEY_INITIAL_RECIPIENT_COUNT, this.initialRecipientCount).putLong("timestamp", this.timestamp).putString(KEY_GROUP_CONTEXT_V2, Base64.encodeBytes(this.groupContextV2.toByteArray())).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (Recipient.self().isRegistered()) {
            GroupId.V2 v2 = GroupId.v2(GroupUtil.requireMasterKey(this.groupContextV2.getMasterKey().toByteArray()));
            if (Recipient.externalGroupExact(v2).isBlocked()) {
                String str = TAG;
                Log.i(str, "Not updating group state for blocked group " + v2);
                return;
            }
            List<Recipient> deliver = deliver(Stream.of(this.recipients).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList(), v2);
            for (Recipient recipient : deliver) {
                this.recipients.remove(recipient.getId());
            }
            String str2 = TAG;
            Log.i(str2, "Completed now: " + deliver.size() + ", Remaining: " + this.recipients.size());
            if (!this.recipients.isEmpty()) {
                Log.w(str2, "Still need to send to " + this.recipients.size() + " recipients. Retrying.");
                throw new RetryLaterException();
            }
            return;
        }
        throw new NotPushRegisteredException();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if ((exc instanceof ServerRejectedException) || (exc instanceof NotPushRegisteredException)) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Failed to send remote delete to all recipients! (" + (this.initialRecipientCount - this.recipients.size()) + "/" + this.initialRecipientCount + ")");
    }

    private List<Recipient> deliver(List<Recipient> list, GroupId.V2 v2) throws IOException, UntrustedIdentityException {
        return GroupSendJobHelper.getCompletedSends(list, GroupSendUtil.sendUnresendableDataMessage(this.context, v2, list, false, ContentHint.IMPLICIT, SignalServiceDataMessage.newBuilder().withTimestamp(this.timestamp).asGroupMessage(SignalServiceGroupV2.fromProtobuf(this.groupContextV2)).build())).completed;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PushGroupSilentUpdateSendJob> {
        public PushGroupSilentUpdateSendJob create(Job.Parameters parameters, Data data) {
            try {
                return new PushGroupSilentUpdateSendJob(RecipientId.fromSerializedList(data.getString("recipients")), data.getInt(PushGroupSilentUpdateSendJob.KEY_INITIAL_RECIPIENT_COUNT), data.getLong("timestamp"), SignalServiceProtos.GroupContextV2.parseFrom(Base64.decodeOrThrow(data.getString(PushGroupSilentUpdateSendJob.KEY_GROUP_CONTEXT_V2))), parameters);
            } catch (InvalidProtocolBufferException e) {
                throw new AssertionError(e);
            }
        }
    }
}
