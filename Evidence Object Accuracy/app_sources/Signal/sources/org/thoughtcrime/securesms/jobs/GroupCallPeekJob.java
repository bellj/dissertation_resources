package org.thoughtcrime.securesms.jobs;

import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.DecryptionsDrainedConstraint;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class GroupCallPeekJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_RECIPIENT_ID;
    private static final String QUEUE;
    private final RecipientId groupRecipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public static void enqueue(RecipientId recipientId) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        String str = QUEUE + recipientId.serialize();
        Job.Parameters.Builder addConstraint = new Job.Parameters.Builder().setQueue(str).addConstraint(DecryptionsDrainedConstraint.KEY);
        jobManager.cancelAllInQueue(str);
        jobManager.add(new GroupCallPeekJob(addConstraint.build(), recipientId));
    }

    private GroupCallPeekJob(Job.Parameters parameters, RecipientId recipientId) {
        super(parameters);
        this.groupRecipientId = recipientId;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        ApplicationDependencies.getJobManager().add(new GroupCallPeekWorkerJob(this.groupRecipientId));
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_GROUP_RECIPIENT_ID, this.groupRecipientId.serialize()).build();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<GroupCallPeekJob> {
        public GroupCallPeekJob create(Job.Parameters parameters, Data data) {
            return new GroupCallPeekJob(parameters, RecipientId.from(data.getString(GroupCallPeekJob.KEY_GROUP_RECIPIENT_ID)));
        }
    }
}
