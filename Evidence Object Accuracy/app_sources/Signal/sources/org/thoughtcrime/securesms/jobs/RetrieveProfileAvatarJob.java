package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class RetrieveProfileAvatarJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_PROFILE_AVATAR;
    private static final String KEY_RECIPIENT;
    private static final int MAX_PROFILE_SIZE_BYTES;
    private static final String TAG = Log.tag(RetrieveProfileAvatarJob.class);
    private final String profileAvatar;
    private final Recipient recipient;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RetrieveProfileAvatarJob(org.thoughtcrime.securesms.recipients.Recipient r5, java.lang.String r6) {
        /*
            r4 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "RetrieveProfileAvatarJob::"
            r1.append(r2)
            org.thoughtcrime.securesms.recipients.RecipientId r2 = r5.getId()
            java.lang.String r2 = r2.toQueueKey()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            java.lang.String r1 = "NetworkConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.HOURS
            r2 = 1
            long r1 = r1.toMillis(r2)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setLifespan(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            r4.<init>(r0, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.RetrieveProfileAvatarJob.<init>(org.thoughtcrime.securesms.recipients.Recipient, java.lang.String):void");
    }

    private RetrieveProfileAvatarJob(Job.Parameters parameters, Recipient recipient, String str) {
        super(parameters);
        this.recipient = recipient;
        this.profileAvatar = str;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_PROFILE_AVATAR, this.profileAvatar).putString("recipient", this.recipient.getId().serialize()).build();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:41:0x0096 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v5, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v6, types: [java.io.File] */
    /* JADX WARN: Type inference failed for: r1v9, types: [java.io.File] */
    /* JADX WARN: Type inference failed for: r2v5, types: [org.whispersystems.signalservice.api.SignalServiceMessageReceiver] */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0110, code lost:
        if (r1 != 0) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0113, code lost:
        r0.setProfileAvatar(r8.recipient.getId(), r8.profileAvatar);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011e, code lost:
        return;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRun() throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.RetrieveProfileAvatarJob.onRun():void");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RetrieveProfileAvatarJob> {
        public RetrieveProfileAvatarJob create(Job.Parameters parameters, Data data) {
            return new RetrieveProfileAvatarJob(parameters, Recipient.resolved(RecipientId.from(data.getString("recipient"))), data.getString(RetrieveProfileAvatarJob.KEY_PROFILE_AVATAR));
        }
    }
}
