package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;

/* loaded from: classes4.dex */
public class ResumableUploadSpecJob extends BaseJob {
    public static final String KEY;
    static final String KEY_RESUME_SPEC;
    private static final String TAG = Log.tag(ResumableUploadSpecJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public ResumableUploadSpecJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private ResumableUploadSpecJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        setOutputData(new Data.Builder().putString(KEY_RESUME_SPEC, ApplicationDependencies.getSignalServiceMessageSender().getResumableUploadSpec().serialize()).build());
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ResumableUploadSpecJob> {
        public ResumableUploadSpecJob create(Job.Parameters parameters, Data data) {
            return new ResumableUploadSpecJob(parameters);
        }
    }
}
