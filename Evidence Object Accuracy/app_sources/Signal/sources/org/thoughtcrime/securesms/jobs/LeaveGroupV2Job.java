package org.thoughtcrime.securesms.jobs;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* compiled from: LeaveGroupV2Job.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0002\u0014\u0015B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u000bH\u0014J\u0014\u0010\r\u001a\u00020\u000e2\n\u0010\u000f\u001a\u00060\u0010j\u0002`\u0011H\u0014J\b\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/LeaveGroupV2Job;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "(Lorg/thoughtcrime/securesms/groups/GroupId$V2;)V", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;Lorg/thoughtcrime/securesms/groups/GroupId$V2;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LeaveGroupV2Job extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private final GroupId.V2 groupId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public LeaveGroupV2Job(Job.Parameters parameters, GroupId.V2 v2) {
        super(parameters);
        Intrinsics.checkNotNullParameter(parameters, "parameters");
        Intrinsics.checkNotNullParameter(v2, "groupId");
        this.groupId = v2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LeaveGroupV2Job(org.thoughtcrime.securesms.groups.GroupId.V2 r3) {
        /*
            r2 = this;
            java.lang.String r0 = "groupId"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.String r1 = "LeaveGroupV2Job"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            java.lang.String r1 = "WebsocketDrainedConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            r1 = -1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxAttempts(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder()\n      .setQueu…UNLIMITED)\n      .build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.LeaveGroupV2Job.<init>(org.thoughtcrime.securesms.groups.GroupId$V2):void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data build = new Data.Builder().putString("group_id", this.groupId.toString()).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n      .putStri…oString())\n      .build()");
        return build;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        ApplicationDependencies.getJobManager().add(new LeaveGroupV2WorkerJob(this.groupId));
    }

    /* compiled from: LeaveGroupV2Job.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/LeaveGroupV2Job$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/LeaveGroupV2Job;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<LeaveGroupV2Job> {
        public LeaveGroupV2Job create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            GroupId.V2 requireV2 = GroupId.parseOrThrow(data.getString("group_id")).requireV2();
            Intrinsics.checkNotNullExpressionValue(requireV2, "parseOrThrow(data.getStr…EY_GROUP_ID)).requireV2()");
            return new LeaveGroupV2Job(parameters, requireV2);
        }
    }

    /* compiled from: LeaveGroupV2Job.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/LeaveGroupV2Job$Companion;", "", "()V", "KEY", "", "KEY_GROUP_ID", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
