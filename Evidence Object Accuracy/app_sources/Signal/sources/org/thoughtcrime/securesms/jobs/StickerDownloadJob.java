package org.thoughtcrime.securesms.jobs;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.IncomingSticker;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class StickerDownloadJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_CONTENT_TYPE;
    private static final String KEY_COVER;
    private static final String KEY_EMOJI;
    private static final String KEY_INSTALLED;
    private static final String KEY_NOTIFY;
    private static final String KEY_PACK_AUTHOR;
    private static final String KEY_PACK_ID;
    private static final String KEY_PACK_KEY;
    private static final String KEY_PACK_TITLE;
    private static final String KEY_STICKER_ID;
    private static final String TAG = Log.tag(StickerDownloadJob.class);
    private final boolean notify;
    private final IncomingSticker sticker;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public StickerDownloadJob(IncomingSticker incomingSticker, boolean z) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(30)).build(), incomingSticker, z);
    }

    private StickerDownloadJob(Job.Parameters parameters, IncomingSticker incomingSticker, boolean z) {
        super(parameters);
        this.sticker = incomingSticker;
        this.notify = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_PACK_ID, this.sticker.getPackId()).putString(KEY_PACK_KEY, this.sticker.getPackKey()).putString(KEY_PACK_TITLE, this.sticker.getPackTitle()).putString(KEY_PACK_AUTHOR, this.sticker.getPackAuthor()).putInt(KEY_STICKER_ID, this.sticker.getStickerId()).putString("emoji", this.sticker.getEmoji()).putString("content_type", this.sticker.getContentType()).putBoolean(KEY_COVER, this.sticker.isCover()).putBoolean(KEY_INSTALLED, this.sticker.isInstalled()).putBoolean(KEY_NOTIFY, this.notify).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        StickerDatabase stickers = SignalDatabase.stickers();
        StickerRecord sticker = stickers.getSticker(this.sticker.getPackId(), this.sticker.getStickerId(), this.sticker.isCover());
        if (sticker != null) {
            try {
                InputStream attachmentStream = PartAuthority.getAttachmentStream(this.context, sticker.getUri());
                if (attachmentStream != null) {
                    Log.w(TAG, "Sticker already downloaded.");
                    attachmentStream.close();
                    return;
                } else if (attachmentStream != null) {
                    attachmentStream.close();
                }
            } catch (FileNotFoundException unused) {
                Log.w(TAG, "Sticker file no longer exists, downloading again.");
            }
        }
        if (stickers.isPackInstalled(this.sticker.getPackId()) || this.sticker.isCover()) {
            stickers.insertSticker(this.sticker, ApplicationDependencies.getSignalServiceMessageReceiver().retrieveSticker(Hex.fromStringCondensed(this.sticker.getPackId()), Hex.fromStringCondensed(this.sticker.getPackKey()), this.sticker.getStickerId()), this.notify);
            return;
        }
        Log.w(TAG, "Pack is no longer installed.");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to download sticker!");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<StickerDownloadJob> {
        public StickerDownloadJob create(Job.Parameters parameters, Data data) {
            return new StickerDownloadJob(parameters, new IncomingSticker(data.getString(StickerDownloadJob.KEY_PACK_ID), data.getString(StickerDownloadJob.KEY_PACK_KEY), data.getString(StickerDownloadJob.KEY_PACK_TITLE), data.getString(StickerDownloadJob.KEY_PACK_AUTHOR), data.getInt(StickerDownloadJob.KEY_STICKER_ID), data.getString("emoji"), data.getString("content_type"), data.getBoolean(StickerDownloadJob.KEY_COVER), data.getBoolean(StickerDownloadJob.KEY_INSTALLED)), data.getBoolean(StickerDownloadJob.KEY_NOTIFY));
        }
    }
}
