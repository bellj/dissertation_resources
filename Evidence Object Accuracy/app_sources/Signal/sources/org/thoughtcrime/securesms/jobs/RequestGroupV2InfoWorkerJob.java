package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.groupsv2.NoCredentialForRedemptionTimeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public final class RequestGroupV2InfoWorkerJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String KEY_TO_REVISION;
    private static final String TAG = Log.tag(RequestGroupV2InfoWorkerJob.class);
    private final GroupId.V2 groupId;
    private final int toRevision;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public RequestGroupV2InfoWorkerJob(GroupId.V2 v2, int i) {
        this(new Job.Parameters.Builder().setQueue(PushProcessMessageJob.getQueueName(Recipient.externalGroupExact(v2).getId())).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), v2, i);
    }

    private RequestGroupV2InfoWorkerJob(Job.Parameters parameters, GroupId.V2 v2, int i) {
        super(parameters);
        this.groupId = v2;
        this.toRevision = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("group_id", this.groupId.toString()).putInt(KEY_TO_REVISION, this.toRevision).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, GroupNotAMemberException, GroupChangeBusyException {
        if (this.toRevision == Integer.MAX_VALUE) {
            Log.i(TAG, "Updating group to latest revision");
        } else {
            String str = TAG;
            Log.i(str, "Updating group to revision " + this.toRevision);
        }
        Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(this.groupId);
        if (!group.isPresent()) {
            Log.w(TAG, "Group not found");
        } else if (Recipient.externalGroupExact(this.groupId).isBlocked()) {
            String str2 = TAG;
            Log.i(str2, "Not fetching group info for blocked group " + this.groupId);
        } else {
            GroupManager.updateGroupFromServer(this.context, group.get().requireV2GroupProperties().getGroupMasterKey(), this.toRevision, System.currentTimeMillis(), null);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof NoCredentialForRedemptionTimeException) || (exc instanceof GroupChangeBusyException);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RequestGroupV2InfoWorkerJob> {
        public RequestGroupV2InfoWorkerJob create(Job.Parameters parameters, Data data) {
            return new RequestGroupV2InfoWorkerJob(parameters, GroupId.parseOrThrow(data.getString("group_id")).requireV2(), data.getInt(RequestGroupV2InfoWorkerJob.KEY_TO_REVISION));
        }
    }
}
