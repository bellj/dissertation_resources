package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.PreKeyUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

@Deprecated
/* loaded from: classes4.dex */
public class CleanPreKeysJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(CleanPreKeysJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    private CleanPreKeysJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        PreKeyUtil.cleanSignedPreKeys(ApplicationDependencies.getProtocolStore().aci(), SignalStore.account().aciPreKeys());
        PreKeyUtil.cleanSignedPreKeys(ApplicationDependencies.getProtocolStore().pni(), SignalStore.account().pniPreKeys());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to execute clean signed prekeys task.");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<CleanPreKeysJob> {
        public CleanPreKeysJob create(Job.Parameters parameters, Data data) {
            return new CleanPreKeysJob(parameters);
        }
    }
}
