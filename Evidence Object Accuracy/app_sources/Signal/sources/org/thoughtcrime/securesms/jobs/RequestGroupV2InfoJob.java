package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.DecryptionsDrainedConstraint;

/* loaded from: classes4.dex */
public final class RequestGroupV2InfoJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String KEY_TO_REVISION;
    private static final String TAG = Log.tag(RequestGroupV2InfoJob.class);
    private final GroupId.V2 groupId;
    private final int toRevision;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public RequestGroupV2InfoJob(GroupId.V2 v2, int i) {
        this(new Job.Parameters.Builder().setQueue("RequestGroupV2InfoSyncJob").addConstraint(DecryptionsDrainedConstraint.KEY).setMaxAttempts(-1).build(), v2, i);
    }

    public RequestGroupV2InfoJob(GroupId.V2 v2) {
        this(v2, Integer.MAX_VALUE);
    }

    private RequestGroupV2InfoJob(Job.Parameters parameters, GroupId.V2 v2, int i) {
        super(parameters);
        this.groupId = v2;
        this.toRevision = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("group_id", this.groupId.toString()).putInt(KEY_TO_REVISION, this.toRevision).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        ApplicationDependencies.getJobManager().add(new RequestGroupV2InfoWorkerJob(this.groupId, this.toRevision));
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RequestGroupV2InfoJob> {
        public RequestGroupV2InfoJob create(Job.Parameters parameters, Data data) {
            return new RequestGroupV2InfoJob(parameters, GroupId.parseOrThrow(data.getString("group_id")).requireV2(), data.getInt(RequestGroupV2InfoJob.KEY_TO_REVISION));
        }
    }
}
