package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.GroupSendJobHelper;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class RemoteDeleteSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_INITIAL_RECIPIENT_COUNT;
    private static final String KEY_IS_MMS;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_RECIPIENTS;
    private static final String TAG = Log.tag(RemoteDeleteSendJob.class);
    private final int initialRecipientCount;
    private final boolean isMms;
    private final long messageId;
    private final List<RecipientId> recipients;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public static JobManager.Chain create(long j, boolean z) throws NoSuchMessageException {
        MessageRecord messageRecord;
        List<RecipientId> list;
        if (z) {
            messageRecord = SignalDatabase.mms().getMessageRecord(j);
        } else {
            messageRecord = SignalDatabase.sms().getSmsMessage(j);
        }
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(messageRecord.getThreadId());
        if (recipientForThreadId != null) {
            if (recipientForThreadId.isDistributionList()) {
                list = SignalDatabase.storySends().getRemoteDeleteRecipients(messageRecord.getId(), messageRecord.getTimestamp());
                if (list.isEmpty()) {
                    return ApplicationDependencies.getJobManager().startChain(MultiDeviceStorySendSyncJob.create(messageRecord.getDateSent(), j));
                }
            } else if (recipientForThreadId.isGroup()) {
                list = Stream.of(recipientForThreadId.getParticipantIds()).toList();
            } else {
                list = Stream.of(recipientForThreadId.getId()).toList();
            }
            list.remove(Recipient.self().getId());
            RemoteDeleteSendJob remoteDeleteSendJob = new RemoteDeleteSendJob(j, z, list, list.size(), new Job.Parameters.Builder().setQueue(recipientForThreadId.getId().toQueueKey()).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
            if (recipientForThreadId.isDistributionList()) {
                return ApplicationDependencies.getJobManager().startChain(remoteDeleteSendJob).then(MultiDeviceStorySendSyncJob.create(messageRecord.getDateSent(), j));
            }
            return ApplicationDependencies.getJobManager().startChain(remoteDeleteSendJob);
        }
        throw new AssertionError("We have a message, but couldn't find the thread!");
    }

    private RemoteDeleteSendJob(long j, boolean z, List<RecipientId> list, int i, Job.Parameters parameters) {
        super(parameters);
        this.messageId = j;
        this.isMms = z;
        this.recipients = list;
        this.initialRecipientCount = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putBoolean("is_mms", this.isMms).putString("recipients", RecipientId.toSerializedList(this.recipients)).putInt(KEY_INITIAL_RECIPIENT_COUNT, this.initialRecipientCount).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        MessageRecord messageRecord;
        MessageDatabase messageDatabase;
        if (Recipient.self().isRegistered()) {
            if (this.isMms) {
                messageDatabase = SignalDatabase.mms();
                messageRecord = SignalDatabase.mms().getMessageRecord(this.messageId);
            } else {
                messageDatabase = SignalDatabase.sms();
                messageRecord = SignalDatabase.sms().getSmsMessage(this.messageId);
            }
            long dateSent = messageRecord.getDateSent();
            Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(messageRecord.getThreadId());
            if (recipientForThreadId == null) {
                throw new AssertionError("We have a message, but couldn't find the thread!");
            } else if (messageRecord.isOutgoing()) {
                List list = Stream.of(this.recipients).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList();
                List<Recipient> eligibleForSending = RecipientUtil.getEligibleForSending(Stream.of(this.recipients).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList());
                List<RecipientId> list2 = Stream.of(SetUtil.difference(list, eligibleForSending)).map(new ContactUtil$$ExternalSyntheticLambda3()).toList();
                GroupSendJobHelper.SendResult deliver = deliver(recipientForThreadId, eligibleForSending, dateSent);
                for (Recipient recipient : deliver.completed) {
                    this.recipients.remove(recipient.getId());
                }
                for (RecipientId recipientId : list2) {
                    this.recipients.remove(recipientId);
                }
                List join = Util.join(list2, deliver.skipped);
                String str = TAG;
                Log.i(str, "Completed now: " + deliver.completed.size() + ", Skipped: " + join.size() + ", Remaining: " + this.recipients.size());
                if (join.size() > 0 && this.isMms && messageRecord.getRecipient().isGroup()) {
                    SignalDatabase.groupReceipts().setSkipped(join, this.messageId);
                }
                if (this.recipients.isEmpty()) {
                    messageDatabase.markAsSent(this.messageId, true);
                    return;
                }
                Log.w(str, "Still need to send to " + this.recipients.size() + " recipients. Retrying.");
                throw new RetryLaterException();
            } else {
                throw new IllegalStateException("Cannot delete a message that isn't yours!");
            }
        } else {
            throw new NotPushRegisteredException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if ((exc instanceof ServerRejectedException) || (exc instanceof NotPushRegisteredException)) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Failed to send remote delete to all recipients! (" + (this.initialRecipientCount - this.recipients.size()) + "/" + this.initialRecipientCount + ")");
    }

    private GroupSendJobHelper.SendResult deliver(Recipient recipient, List<Recipient> list, long j) throws IOException, UntrustedIdentityException {
        SignalServiceDataMessage.Builder withRemoteDelete = SignalServiceDataMessage.newBuilder().withTimestamp(System.currentTimeMillis()).withRemoteDelete(new SignalServiceDataMessage.RemoteDelete(j));
        if (recipient.isGroup()) {
            GroupUtil.setDataMessageGroupContext(this.context, withRemoteDelete, recipient.requireGroupId().requirePush());
        }
        return GroupSendJobHelper.getCompletedSends(list, GroupSendUtil.sendResendableDataMessage(this.context, (GroupId.V2) recipient.getGroupId().map(new PushGroupSendJob$$ExternalSyntheticLambda24()).orElse(null), list, false, ContentHint.RESENDABLE, new MessageId(this.messageId, this.isMms), withRemoteDelete.build()));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<RemoteDeleteSendJob> {
        public RemoteDeleteSendJob create(Job.Parameters parameters, Data data) {
            return new RemoteDeleteSendJob(data.getLong("message_id"), data.getBoolean("is_mms"), RecipientId.fromSerializedList(data.getString("recipients")), data.getInt(RemoteDeleteSendJob.KEY_INITIAL_RECIPIENT_COUNT), parameters);
        }
    }
}
