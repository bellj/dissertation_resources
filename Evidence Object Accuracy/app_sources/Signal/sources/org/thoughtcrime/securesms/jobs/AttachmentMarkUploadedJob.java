package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public final class AttachmentMarkUploadedJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_ROW_ID;
    private static final String KEY_UNIQUE_ID;
    private static final String TAG = Log.tag(AttachmentMarkUploadedJob.class);
    private final AttachmentId attachmentId;
    private final long messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public AttachmentMarkUploadedJob(long j, AttachmentId attachmentId) {
        this(new Job.Parameters.Builder().setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), j, attachmentId);
    }

    private AttachmentMarkUploadedJob(Job.Parameters parameters, long j, AttachmentId attachmentId) {
        super(parameters);
        this.attachmentId = attachmentId;
        this.messageId = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong(KEY_ROW_ID, this.attachmentId.getRowId()).putLong("unique_id", this.attachmentId.getUniqueId()).putLong("message_id", this.messageId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        AttachmentDatabase attachments = SignalDatabase.attachments();
        DatabaseAttachment attachment = attachments.getAttachment(this.attachmentId);
        if (attachment != null) {
            attachments.markAttachmentUploaded(this.messageId, attachment);
            return;
        }
        throw new InvalidAttachmentException("Cannot find the specified attachment.");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    private class InvalidAttachmentException extends Exception {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        InvalidAttachmentException(String str) {
            super(str);
            AttachmentMarkUploadedJob.this = r1;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AttachmentMarkUploadedJob> {
        public AttachmentMarkUploadedJob create(Job.Parameters parameters, Data data) {
            return new AttachmentMarkUploadedJob(parameters, data.getLong("message_id"), new AttachmentId(data.getLong(AttachmentMarkUploadedJob.KEY_ROW_ID), data.getLong("unique_id")));
        }
    }
}
