package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class RequestGroupInfoJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String KEY_SOURCE;
    private static final String TAG = Log.tag(RequestGroupInfoJob.class);
    private final GroupId groupId;
    private final RecipientId source;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public RequestGroupInfoJob(RecipientId recipientId, GroupId groupId) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), recipientId, groupId);
    }

    private RequestGroupInfoJob(Job.Parameters parameters, RecipientId recipientId, GroupId groupId) {
        super(parameters);
        this.source = recipientId;
        this.groupId = groupId;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("source", this.source.serialize()).putString("group_id", this.groupId.toString()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (Recipient.self().isRegistered()) {
            SignalServiceDataMessage build = SignalServiceDataMessage.newBuilder().asGroupMessage(SignalServiceGroup.newBuilder(SignalServiceGroup.Type.REQUEST_INFO).withId(this.groupId.getDecodedId()).build()).withTimestamp(System.currentTimeMillis()).build();
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            Recipient resolved = Recipient.resolved(this.source);
            if (resolved.isUnregistered()) {
                String str = TAG;
                Log.w(str, resolved.getId() + " is unregistered!");
                return;
            }
            signalServiceMessageSender.sendDataMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, resolved), ContentHint.IMPLICIT, build, SignalServiceMessageSender.IndividualSendEvents.EMPTY);
            return;
        }
        throw new NotPushRegisteredException();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RequestGroupInfoJob> {
        public RequestGroupInfoJob create(Job.Parameters parameters, Data data) {
            return new RequestGroupInfoJob(parameters, RecipientId.from(data.getString("source")), GroupId.parseOrThrow(data.getString("group_id")));
        }
    }
}
