package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PushGroupSendJob$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((Recipient) obj).resolve();
    }
}
