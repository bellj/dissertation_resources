package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.thoughtcrime.securesms.crypto.PreKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.ServiceIdType;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class CreateSignedPreKeyJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(CreateSignedPreKeyJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private CreateSignedPreKeyJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setMaxInstancesForFactory(1).setQueue(KEY).setLifespan(TimeUnit.DAYS.toMillis(30)).setMaxAttempts(-1).build());
    }

    private CreateSignedPreKeyJob(Job.Parameters parameters) {
        super(parameters);
    }

    public static void enqueueIfNeeded() {
        if (!SignalStore.account().aciPreKeys().isSignedPreKeyRegistered() || !SignalStore.account().pniPreKeys().isSignedPreKeyRegistered()) {
            Log.i(TAG, "Some signed prekeys aren't registered yet. Enqueuing a job.");
            ApplicationDependencies.getJobManager().add(new CreateSignedPreKeyJob());
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null || SignalStore.account().getPni() == null) {
            Log.w(TAG, "Not yet registered...");
            return;
        }
        createPreKeys(ServiceIdType.ACI, SignalStore.account().getAci(), ApplicationDependencies.getProtocolStore().aci(), SignalStore.account().aciPreKeys());
        createPreKeys(ServiceIdType.PNI, SignalStore.account().getPni(), ApplicationDependencies.getProtocolStore().pni(), SignalStore.account().pniPreKeys());
    }

    private void createPreKeys(ServiceIdType serviceIdType, ServiceId serviceId, SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore) throws IOException {
        if (serviceId == null) {
            warn(TAG, "AccountId not set!");
        } else if (preKeyMetadataStore.isSignedPreKeyRegistered()) {
            String str = TAG;
            warn(str, "Signed prekey for " + serviceIdType + " already registered...");
        } else {
            ApplicationDependencies.getSignalServiceAccountManager().setSignedPreKey(serviceIdType, PreKeyUtil.generateAndStoreSignedPreKey(signalProtocolStore, preKeyMetadataStore, true));
            preKeyMetadataStore.setSignedPreKeyRegistered(true);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<CreateSignedPreKeyJob> {
        public CreateSignedPreKeyJob create(Job.Parameters parameters, Data data) {
            return new CreateSignedPreKeyJob(parameters);
        }
    }
}
