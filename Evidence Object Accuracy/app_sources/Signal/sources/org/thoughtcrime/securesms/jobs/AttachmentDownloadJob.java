package org.thoughtcrime.securesms.jobs;

import android.text.TextUtils;
import j$.util.Optional;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Okio;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobLogger;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.s3.S3;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.AttachmentUtil;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId;
import org.whispersystems.signalservice.api.push.exceptions.MissingConfigurationException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.RangeException;

/* loaded from: classes4.dex */
public final class AttachmentDownloadJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_MANUAL;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_PART_ROW_ID;
    private static final String KEY_PAR_UNIQUE_ID;
    private static final int MAX_ATTACHMENT_SIZE;
    private static final String TAG = Log.tag(AttachmentDownloadJob.class);
    private boolean manual;
    private long messageId;
    private long partRowId;
    private long partUniqueId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AttachmentDownloadJob(long r8, org.thoughtcrime.securesms.attachments.AttachmentId r10, boolean r11) {
        /*
            r7 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "AttachmentDownloadJob"
            r1.append(r2)
            long r2 = r10.getRowId()
            r1.append(r2)
            java.lang.String r2 = "-"
            r1.append(r2)
            long r2 = r10.getUniqueId()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            java.lang.String r1 = "NetworkConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS
            r2 = 1
            long r1 = r1.toMillis(r2)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setLifespan(r1)
            r1 = -1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxAttempts(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r2 = r0.build()
            r1 = r7
            r3 = r8
            r5 = r10
            r6 = r11
            r1.<init>(r2, r3, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.AttachmentDownloadJob.<init>(long, org.thoughtcrime.securesms.attachments.AttachmentId, boolean):void");
    }

    private AttachmentDownloadJob(Job.Parameters parameters, long j, AttachmentId attachmentId, boolean z) {
        super(parameters);
        this.messageId = j;
        this.partRowId = attachmentId.getRowId();
        this.partUniqueId = attachmentId.getUniqueId();
        this.manual = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putLong(KEY_PART_ROW_ID, this.partRowId).putLong(KEY_PAR_UNIQUE_ID, this.partUniqueId).putBoolean(KEY_MANUAL, this.manual).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        String str = TAG;
        Log.i(str, "onAdded() messageId: " + this.messageId + "  partRowId: " + this.partRowId + "  partUniqueId: " + this.partUniqueId + "  manual: " + this.manual);
        AttachmentDatabase attachments = SignalDatabase.attachments();
        AttachmentId attachmentId = new AttachmentId(this.partRowId, this.partUniqueId);
        DatabaseAttachment attachment = attachments.getAttachment(attachmentId);
        if (!((attachment == null || attachment.getTransferState() == 0) ? false : true)) {
            return;
        }
        if (this.manual || AttachmentUtil.isAutoDownloadPermitted(this.context, attachment)) {
            Log.i(str, "onAdded() Marking attachment progress as 'started'");
            attachments.setTransferState(this.messageId, attachmentId, 1);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        doWork();
        if (!SignalDatabase.mms().isStory(this.messageId)) {
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(0));
        }
    }

    public void doWork() throws IOException, RetryLaterException {
        String str = TAG;
        Log.i(str, "onRun() messageId: " + this.messageId + "  partRowId: " + this.partRowId + "  partUniqueId: " + this.partUniqueId + "  manual: " + this.manual);
        AttachmentDatabase attachments = SignalDatabase.attachments();
        AttachmentId attachmentId = new AttachmentId(this.partRowId, this.partUniqueId);
        DatabaseAttachment attachment = attachments.getAttachment(attachmentId);
        if (attachment == null) {
            Log.w(str, "attachment no longer exists.");
        } else if (!attachment.isInProgress()) {
            Log.w(str, "Attachment was already downloaded.");
        } else if (this.manual || AttachmentUtil.isAutoDownloadPermitted(this.context, attachment)) {
            Log.i(str, "Downloading push part " + attachmentId);
            attachments.setTransferState(this.messageId, attachmentId, 1);
            if (attachment.getCdnNumber() != -1) {
                retrieveAttachment(this.messageId, attachmentId, attachment);
            } else {
                retrieveUrlAttachment(this.messageId, attachmentId, attachment);
            }
        } else {
            Log.w(str, "Attachment can't be auto downloaded...");
            attachments.setTransferState(this.messageId, attachmentId, 2);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, JobLogger.format(this, "onFailure() messageId: " + this.messageId + "  partRowId: " + this.partRowId + "  partUniqueId: " + this.partUniqueId + "  manual: " + this.manual));
        markFailed(this.messageId, new AttachmentId(this.partRowId, this.partUniqueId));
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof RetryLaterException);
    }

    private void retrieveAttachment(long j, AttachmentId attachmentId, Attachment attachment) throws IOException, RetryLaterException {
        Throwable e;
        AttachmentDatabase attachments = SignalDatabase.attachments();
        File orCreateTransferFile = attachments.getOrCreateTransferFile(attachmentId);
        try {
            attachments.insertAttachmentsForPlaceholder(j, attachmentId, ApplicationDependencies.getSignalServiceMessageReceiver().retrieveAttachment(createAttachmentPointer(attachment), orCreateTransferFile, 157286400, new SignalServiceAttachment.ProgressListener() { // from class: org.thoughtcrime.securesms.jobs.AttachmentDownloadJob$$ExternalSyntheticLambda1
                @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment.ProgressListener
                public final void onAttachmentProgress(long j2, long j3) {
                    AttachmentDownloadJob.lambda$retrieveAttachment$0(Attachment.this, j2, j3);
                }
            }));
        } catch (InvalidMessageException e2) {
            e = e2;
            Log.w(TAG, "Experienced exception while trying to download an attachment.", e);
            markFailed(j, attachmentId);
        } catch (InvalidPartException e3) {
            e = e3;
            Log.w(TAG, "Experienced exception while trying to download an attachment.", e);
            markFailed(j, attachmentId);
        } catch (MmsException e4) {
            e = e4;
            Log.w(TAG, "Experienced exception while trying to download an attachment.", e);
            markFailed(j, attachmentId);
        } catch (MissingConfigurationException e5) {
            e = e5;
            Log.w(TAG, "Experienced exception while trying to download an attachment.", e);
            markFailed(j, attachmentId);
        } catch (RangeException e6) {
            String str = TAG;
            Log.w(str, "Range exception, file size " + orCreateTransferFile.length(), e6);
            if (orCreateTransferFile.delete()) {
                Log.i(str, "Deleted temp download file to recover");
                throw new RetryLaterException(e6);
            }
            throw new IOException("Failed to delete temp download file following range exception");
        } catch (NonSuccessfulResponseCodeException e7) {
            e = e7;
            Log.w(TAG, "Experienced exception while trying to download an attachment.", e);
            markFailed(j, attachmentId);
        }
    }

    public static /* synthetic */ void lambda$retrieveAttachment$0(Attachment attachment, long j, long j2) {
        EventBus.getDefault().postSticky(new PartProgressEvent(attachment, PartProgressEvent.Type.NETWORK, j, j2));
    }

    private SignalServiceAttachmentPointer createAttachmentPointer(Attachment attachment) throws InvalidPartException {
        if (TextUtils.isEmpty(attachment.getLocation())) {
            throw new InvalidPartException("empty content id");
        } else if (!TextUtils.isEmpty(attachment.getKey())) {
            try {
                SignalServiceAttachmentRemoteId from = SignalServiceAttachmentRemoteId.from(attachment.getLocation());
                byte[] decode = Base64.decode(attachment.getKey());
                if (attachment.getDigest() != null) {
                    String str = TAG;
                    Log.i(str, "Downloading attachment with digest: " + Hex.toString(attachment.getDigest()));
                } else {
                    Log.i(TAG, "Downloading attachment with no digest...");
                }
                return new SignalServiceAttachmentPointer(attachment.getCdnNumber(), from, null, decode, Optional.of(Integer.valueOf(Util.toIntExact(attachment.getSize()))), Optional.empty(), 0, 0, Optional.ofNullable(attachment.getDigest()), Optional.ofNullable(attachment.getFileName()), attachment.isVoiceNote(), attachment.isBorderless(), attachment.isVideoGif(), Optional.empty(), Optional.ofNullable(attachment.getBlurHash()).map(new AttachmentDownloadJob$$ExternalSyntheticLambda0()), attachment.getUploadTimestamp());
            } catch (IOException | ArithmeticException e) {
                Log.w(TAG, e);
                throw new InvalidPartException(e);
            }
        } else {
            throw new InvalidPartException("empty encrypted key");
        }
    }

    private void retrieveUrlAttachment(long j, AttachmentId attachmentId, Attachment attachment) throws IOException {
        try {
            String fileName = attachment.getFileName();
            Objects.requireNonNull(fileName);
            Response object = S3.getObject(fileName);
            ResponseBody body = object.body();
            if (body != null) {
                SignalDatabase.attachments().insertAttachmentsForPlaceholder(j, attachmentId, Okio.buffer(body.source()).inputStream());
            }
            object.close();
        } catch (MmsException e) {
            Log.w(TAG, "Experienced exception while trying to download an attachment.", e);
            markFailed(j, attachmentId);
        }
    }

    private void markFailed(long j, AttachmentId attachmentId) {
        try {
            SignalDatabase.attachments().setTransferProgressFailed(attachmentId, j);
        } catch (MmsException e) {
            Log.w(TAG, e);
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidPartException extends Exception {
        InvalidPartException(String str) {
            super(str);
        }

        InvalidPartException(Exception exc) {
            super(exc);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AttachmentDownloadJob> {
        public AttachmentDownloadJob create(Job.Parameters parameters, Data data) {
            return new AttachmentDownloadJob(parameters, data.getLong("message_id"), new AttachmentId(data.getLong(AttachmentDownloadJob.KEY_PART_ROW_ID), data.getLong(AttachmentDownloadJob.KEY_PAR_UNIQUE_ID)), data.getBoolean(AttachmentDownloadJob.KEY_MANUAL));
        }
    }
}
