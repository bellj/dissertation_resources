package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.jobs.MultiDeviceReadUpdateJob;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MultiDeviceReadUpdateJob$Factory$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return MultiDeviceReadUpdateJob.Factory.lambda$create$1((MultiDeviceReadUpdateJob.SerializableSyncMessageId) obj);
    }
}
