package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;

/* loaded from: classes4.dex */
public final class AvatarGroupsV1DownloadJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String TAG = Log.tag(AvatarGroupsV1DownloadJob.class);
    private final GroupId.V1 groupId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public AvatarGroupsV1DownloadJob(GroupId.V1 v1) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setMaxAttempts(10).build(), v1);
    }

    private AvatarGroupsV1DownloadJob(Job.Parameters parameters, GroupId.V1 v1) {
        super(parameters);
        this.groupId = v1;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("group_id", this.groupId.toString()).build();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 2, insn: 0x00fa: IF  (r2 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:23:0x0100, block:B:21:0x00fa
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 258
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.AvatarGroupsV1DownloadJob.onRun():void");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AvatarGroupsV1DownloadJob> {
        public AvatarGroupsV1DownloadJob create(Job.Parameters parameters, Data data) {
            return new AvatarGroupsV1DownloadJob(parameters, GroupId.parseOrThrow(data.getString("group_id")).requireV1());
        }
    }
}
