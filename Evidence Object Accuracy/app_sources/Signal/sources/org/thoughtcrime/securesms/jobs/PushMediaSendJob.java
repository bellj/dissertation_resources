package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import com.annimon.stream.Stream;
import j$.util.Optional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;
import org.thoughtcrime.securesms.transport.InsecureFallbackApprovalException;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;

/* loaded from: classes4.dex */
public class PushMediaSendJob extends PushSendJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String TAG = Log.tag(PushMediaSendJob.class);
    private long messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public PushMediaSendJob(long j, Recipient recipient, boolean z) {
        this(PushSendJob.constructParameters(recipient, z), j);
    }

    private PushMediaSendJob(Job.Parameters parameters, long j) {
        super(parameters);
        this.messageId = j;
    }

    public static void enqueue(Context context, JobManager jobManager, long j, Recipient recipient) {
        try {
            if (recipient.hasServiceId()) {
                Set<String> enqueueCompressingAndUploadAttachmentsChains = PushSendJob.enqueueCompressingAndUploadAttachmentsChains(jobManager, SignalDatabase.mms().getOutgoingMessage(j));
                jobManager.add(new PushMediaSendJob(j, recipient, enqueueCompressingAndUploadAttachmentsChains.size() > 0), enqueueCompressingAndUploadAttachmentsChains, recipient.getId().toQueueKey());
                return;
            }
            throw new AssertionError("No ServiceId!");
        } catch (NoSuchMessageException | MmsException e) {
            Log.w(TAG, "Failed to enqueue message.", e);
            SignalDatabase.mms().markAsSentFailed(j);
            PushSendJob.notifyMediaMessageDeliveryFailed(context, j);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        SignalDatabase.mms().markAsSending(this.messageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.PushSendJob
    public void onPushSend() throws IOException, MmsException, NoSuchMessageException, UndeliverableMessageException, RetryLaterException {
        long j;
        ProofRequiredException e;
        RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode;
        ExpiringMessageManager expiringMessageManager = ApplicationDependencies.getExpiringMessageManager();
        MmsDatabase mms = SignalDatabase.mms();
        OutgoingMediaMessage outgoingMessage = mms.getOutgoingMessage(this.messageId);
        long threadId = mms.getMessageRecord(this.messageId).getThreadId();
        if (mms.isSent(this.messageId)) {
            String str = TAG;
            String valueOf = String.valueOf(outgoingMessage.getSentTimeMillis());
            warn(str, valueOf, "Message " + this.messageId + " was already sent. Ignoring.");
            return;
        }
        try {
            try {
                String str2 = TAG;
                String valueOf2 = String.valueOf(outgoingMessage.getSentTimeMillis());
                log(str2, valueOf2, "Sending message: " + this.messageId + ", Recipient: " + outgoingMessage.getRecipient().getId() + ", Thread: " + threadId + ", Attachments: " + buildAttachmentString(outgoingMessage.getAttachments()));
                RecipientUtil.shareProfileIfFirstSecureMessage(this.context, outgoingMessage.getRecipient());
                Recipient fresh = outgoingMessage.getRecipient().fresh();
                byte[] profileKey = fresh.getProfileKey();
                RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode2 = fresh.getUnidentifiedAccessMode();
                boolean deliver = deliver(outgoingMessage);
                mms.markAsSent(this.messageId, true);
                SendJob.markAttachmentsUploaded(this.messageId, outgoingMessage);
                mms.markUnidentified(this.messageId, deliver);
                if (fresh.isSelf()) {
                    j = threadId;
                    try {
                        MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(fresh.getId(), outgoingMessage.getSentTimeMillis());
                        SignalDatabase.mmsSms().incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
                        SignalDatabase.mmsSms().incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
                        SignalDatabase.mmsSms().incrementViewedReceiptCount(syncMessageId, System.currentTimeMillis());
                    } catch (ProofRequiredException e2) {
                        e = e2;
                        PushSendJob.handleProofRequiredException(this.context, e, SignalDatabase.threads().getRecipientForThreadId(j), j, this.messageId, true);
                        return;
                    }
                } else {
                    j = threadId;
                }
                if (deliver && unidentifiedAccessMode2 == RecipientDatabase.UnidentifiedAccessMode.UNKNOWN && profileKey == null) {
                    log(str2, String.valueOf(outgoingMessage.getSentTimeMillis()), "Marking recipient as UD-unrestricted following a UD send.");
                    SignalDatabase.recipients().setUnidentifiedAccessMode(fresh.getId(), RecipientDatabase.UnidentifiedAccessMode.UNRESTRICTED);
                } else if (deliver && unidentifiedAccessMode2 == RecipientDatabase.UnidentifiedAccessMode.UNKNOWN) {
                    log(str2, String.valueOf(outgoingMessage.getSentTimeMillis()), "Marking recipient as UD-enabled following a UD send.");
                    SignalDatabase.recipients().setUnidentifiedAccessMode(fresh.getId(), RecipientDatabase.UnidentifiedAccessMode.ENABLED);
                } else if (!deliver && unidentifiedAccessMode2 != (unidentifiedAccessMode = RecipientDatabase.UnidentifiedAccessMode.DISABLED)) {
                    log(str2, String.valueOf(outgoingMessage.getSentTimeMillis()), "Marking recipient as UD-disabled following a non-UD send.");
                    SignalDatabase.recipients().setUnidentifiedAccessMode(fresh.getId(), unidentifiedAccessMode);
                }
                if (outgoingMessage.getExpiresIn() > 0 && !outgoingMessage.isExpirationUpdate()) {
                    mms.markExpireStarted(this.messageId);
                    expiringMessageManager.scheduleDeletion(this.messageId, true, outgoingMessage.getExpiresIn());
                }
                if (outgoingMessage.isViewOnce()) {
                    SignalDatabase.attachments().deleteAttachmentFilesForViewOnceMessage(this.messageId);
                }
                String valueOf3 = String.valueOf(outgoingMessage.getSentTimeMillis());
                log(str2, valueOf3, "Sent message: " + this.messageId);
            } catch (ProofRequiredException e3) {
                e = e3;
                j = threadId;
            }
        } catch (InsecureFallbackApprovalException e4) {
            warn(TAG, "Failure", e4);
            mms.markAsPendingInsecureSmsFallback(this.messageId);
            PushSendJob.notifyMediaMessageDeliveryFailed(this.context, this.messageId);
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
        } catch (UntrustedIdentityException e5) {
            warn(TAG, "Failure", e5);
            RecipientId id = Recipient.external(this.context, e5.getIdentifier()).getId();
            mms.addMismatchedIdentity(this.messageId, id, e5.getIdentityKey());
            mms.markAsSentFailed(this.messageId);
            RetrieveProfileJob.enqueue(id);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        SignalDatabase.mms().markAsSentFailed(this.messageId);
        PushSendJob.notifyMediaMessageDeliveryFailed(this.context, this.messageId);
    }

    private boolean deliver(OutgoingMediaMessage outgoingMediaMessage) throws IOException, InsecureFallbackApprovalException, UntrustedIdentityException, UndeliverableMessageException {
        UnregisteredUserException e;
        FileNotFoundException e2;
        Recipient fresh;
        NoSuchMessageException e3;
        if (outgoingMediaMessage.getRecipient() != null) {
            try {
                try {
                    rotateSenderCertificateIfNecessary();
                    fresh = outgoingMediaMessage.getRecipient().fresh();
                } catch (ServerRejectedException e4) {
                    throw new UndeliverableMessageException(e4);
                }
            } catch (FileNotFoundException e5) {
                e2 = e5;
            } catch (UnregisteredUserException e6) {
                e = e6;
            }
            try {
                if (!fresh.isUnregistered()) {
                    SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
                    SignalServiceAddress signalServiceAddress = RecipientUtil.toSignalServiceAddress(this.context, fresh);
                    List<SignalServiceAttachment> attachmentPointersFor = getAttachmentPointersFor(Stream.of(outgoingMediaMessage.getAttachments()).filterNot(new PushDistributionListSendJob$$ExternalSyntheticLambda0()).toList());
                    Optional<byte[]> profileKey = getProfileKey(fresh);
                    Optional<SignalServiceDataMessage.Sticker> stickerFor = getStickerFor(outgoingMediaMessage);
                    List<SharedContact> sharedContactsFor = getSharedContactsFor(outgoingMediaMessage);
                    SignalServiceDataMessage.Builder asExpirationUpdate = SignalServiceDataMessage.newBuilder().withBody(outgoingMediaMessage.getBody()).withAttachments(attachmentPointersFor).withTimestamp(outgoingMediaMessage.getSentTimeMillis()).withExpiration((int) (outgoingMediaMessage.getExpiresIn() / 1000)).withViewOnce(outgoingMediaMessage.isViewOnce()).withProfileKey(profileKey.orElse(null)).withSticker(stickerFor.orElse(null)).withSharedContacts(sharedContactsFor).withPreviews(getPreviewsFor(outgoingMediaMessage)).withGiftBadge(getGiftBadgeFor(outgoingMediaMessage)).asExpirationUpdate(outgoingMediaMessage.isExpirationUpdate());
                    if (outgoingMediaMessage.getParentStoryId() != null) {
                        try {
                            MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(outgoingMediaMessage.getParentStoryId().asMessageId().getId());
                            SignalServiceDataMessage.StoryContext storyContext = new SignalServiceDataMessage.StoryContext((messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getRecipient()).requireServiceId(), messageRecord.getDateSent());
                            asExpirationUpdate.withStoryContext(storyContext);
                            try {
                                Optional<SignalServiceDataMessage.Reaction> storyReactionFor = getStoryReactionFor(outgoingMediaMessage, storyContext);
                                if (storyReactionFor.isPresent()) {
                                    asExpirationUpdate.withReaction(storyReactionFor.get());
                                    asExpirationUpdate.withBody(null);
                                }
                            } catch (NoSuchMessageException e7) {
                                e3 = e7;
                                throw new UndeliverableMessageException(e3);
                            }
                        } catch (NoSuchMessageException e8) {
                            e3 = e8;
                        }
                    } else {
                        asExpirationUpdate.withQuote(getQuoteFor(outgoingMediaMessage).orElse(null));
                    }
                    if (outgoingMediaMessage.getGiftBadge() != null) {
                        asExpirationUpdate.withBody(null);
                    }
                    SignalServiceDataMessage build = asExpirationUpdate.build();
                    if (Util.equals(SignalStore.account().getAci(), signalServiceAddress.getServiceId())) {
                        Optional<UnidentifiedAccessPair> accessForSync = UnidentifiedAccessUtil.getAccessForSync(this.context);
                        SignalDatabase.messageLog().insertIfPossible(fresh.getId(), outgoingMediaMessage.getSentTimeMillis(), signalServiceMessageSender.sendSyncMessage(build), ContentHint.RESENDABLE, new MessageId(this.messageId, true));
                        return accessForSync.isPresent();
                    }
                    Optional<UnidentifiedAccessPair> accessFor = UnidentifiedAccessUtil.getAccessFor(this.context, fresh);
                    ContentHint contentHint = ContentHint.RESENDABLE;
                    SendMessageResult sendDataMessage = signalServiceMessageSender.sendDataMessage(signalServiceAddress, accessFor, contentHint, build, SignalServiceMessageSender.IndividualSendEvents.EMPTY);
                    SignalDatabase.messageLog().insertIfPossible(fresh.getId(), outgoingMediaMessage.getSentTimeMillis(), sendDataMessage, contentHint, new MessageId(this.messageId, true));
                    return sendDataMessage.getSuccess().isUnidentified();
                }
                throw new UndeliverableMessageException(fresh.getId() + " not registered!");
            } catch (FileNotFoundException e9) {
                e2 = e9;
                warn(TAG, String.valueOf(outgoingMediaMessage.getSentTimeMillis()), e2);
                throw new UndeliverableMessageException(e2);
            } catch (UnregisteredUserException e10) {
                e = e10;
                warn(TAG, String.valueOf(outgoingMediaMessage.getSentTimeMillis()), e);
                throw new InsecureFallbackApprovalException(e);
            }
        } else {
            throw new UndeliverableMessageException("No destination address.");
        }
    }

    public static long getMessageId(Data data) {
        return data.getLong("message_id");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushMediaSendJob> {
        public PushMediaSendJob create(Job.Parameters parameters, Data data) {
            return new PushMediaSendJob(parameters, data.getLong("message_id"));
        }
    }
}
