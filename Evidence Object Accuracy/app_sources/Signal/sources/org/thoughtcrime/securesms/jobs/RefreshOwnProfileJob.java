package org.thoughtcrime.securesms.jobs;

import android.text.TextUtils;
import j$.util.Collection$EL;
import j$.util.Comparator;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.function.ToLongFunction;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.api.crypto.ProfileCipher;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.util.ExpiringProfileCredentialUtil;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes4.dex */
public class RefreshOwnProfileJob extends BaseJob {
    private static final String BOOST_QUEUE;
    public static final String KEY;
    private static final String SUBSCRIPTION_QUEUE;
    private static final String TAG = Log.tag(RefreshOwnProfileJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public RefreshOwnProfileJob() {
        this(ProfileUploadJob.QUEUE);
    }

    private RefreshOwnProfileJob(String str) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(str).setMaxInstancesForFactory(1).setMaxAttempts(10).build());
    }

    public static RefreshOwnProfileJob forSubscription() {
        return new RefreshOwnProfileJob(SUBSCRIPTION_QUEUE);
    }

    public static RefreshOwnProfileJob forBoost() {
        return new RefreshOwnProfileJob(BOOST_QUEUE);
    }

    private RefreshOwnProfileJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (!SignalStore.account().isRegistered() || TextUtils.isEmpty(SignalStore.account().getE164())) {
            Log.w(TAG, "Not yet registered!");
        } else if (SignalStore.kbsValues().hasPin() && !SignalStore.kbsValues().hasOptedOut() && SignalStore.storageService().getLastSyncTime() == 0) {
            Log.i(TAG, "Registered with PIN but haven't completed storage sync yet.");
        } else if (SignalStore.registrationValues().hasUploadedProfile() || !SignalStore.account().isPrimaryDevice()) {
            Recipient self = Recipient.self();
            ProfileAndCredential retrieveProfileSync = ProfileUtil.retrieveProfileSync(this.context, self, getRequestType(self), false);
            SignalServiceProfile profile = retrieveProfileSync.getProfile();
            if (!Util.isEmpty(profile.getName()) || !Util.isEmpty(profile.getAvatar()) || !Util.isEmpty(profile.getAbout()) || !Util.isEmpty(profile.getAboutEmoji())) {
                setProfileName(profile.getName());
                setProfileAbout(profile.getAbout(), profile.getAboutEmoji());
                setProfileAvatar(profile.getAvatar());
                setProfileCapabilities(profile.getCapabilities());
                setProfileBadges(profile.getBadges());
                ensureUnidentifiedAccessCorrect(profile.getUnidentifiedAccess(), profile.isUnrestrictedUnidentifiedAccess());
                retrieveProfileSync.getExpiringProfileKeyCredential().ifPresent(new Consumer(self) { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda17
                    public final /* synthetic */ Recipient f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // j$.util.function.Consumer
                    public final void accept(Object obj) {
                        RefreshOwnProfileJob.this.lambda$onRun$0(this.f$1, (ExpiringProfileKeyCredential) obj);
                    }

                    @Override // j$.util.function.Consumer
                    public /* synthetic */ Consumer andThen(Consumer consumer) {
                        return Consumer.CC.$default$andThen(this, consumer);
                    }
                });
                StoryOnboardingDownloadJob.Companion.enqueueIfNeeded();
                return;
            }
            Log.w(TAG, "The profile we retrieved was empty! Ignoring it.");
        } else {
            Log.i(TAG, "Registered but haven't uploaded profile yet.");
        }
    }

    public /* synthetic */ void lambda$onRun$0(Recipient recipient, ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        setExpiringProfileKeyCredential(recipient, ProfileKeyUtil.getSelfProfileKey(), expiringProfileKeyCredential);
    }

    private void setExpiringProfileKeyCredential(Recipient recipient, ProfileKey profileKey, ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        SignalDatabase.recipients().setProfileKeyCredential(recipient.getId(), profileKey, expiringProfileKeyCredential);
    }

    private static SignalServiceProfile.RequestType getRequestType(Recipient recipient) {
        if (ExpiringProfileCredentialUtil.isValid(recipient.getExpiringProfileKeyCredential())) {
            return SignalServiceProfile.RequestType.PROFILE;
        }
        return SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    private void setProfileName(String str) {
        try {
            ProfileName fromSerialized = ProfileName.fromSerialized(ProfileUtil.decryptString(ProfileKeyUtil.getSelfProfileKey(), str));
            if (!fromSerialized.isEmpty()) {
                Log.d(TAG, "Saving non-empty name.");
                SignalDatabase.recipients().setProfileName(Recipient.self().getId(), fromSerialized);
                return;
            }
            Log.w(TAG, "Ignoring empty name.");
        } catch (IOException | InvalidCiphertextException e) {
            while (true) {
                Log.w(TAG, e);
                return;
            }
        }
    }

    private void setProfileAbout(String str, String str2) {
        try {
            ProfileKey selfProfileKey = ProfileKeyUtil.getSelfProfileKey();
            String decryptString = ProfileUtil.decryptString(selfProfileKey, str);
            String decryptString2 = ProfileUtil.decryptString(selfProfileKey, str2);
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Saving ");
            String str4 = "non-";
            sb.append(!Util.isEmpty(decryptString) ? str4 : "");
            sb.append("empty about.");
            Log.d(str3, sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Saving ");
            if (Util.isEmpty(decryptString2)) {
                str4 = "";
            }
            sb2.append(str4);
            sb2.append("empty emoji.");
            Log.d(str3, sb2.toString());
            SignalDatabase.recipients().setAbout(Recipient.self().getId(), decryptString, decryptString2);
        } catch (IOException | InvalidCiphertextException e) {
            Log.w(TAG, e);
        }
    }

    private static void setProfileAvatar(String str) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Saving ");
        sb.append(!Util.isEmpty(str) ? "non-" : "");
        sb.append("empty avatar.");
        Log.d(str2, sb.toString());
        ApplicationDependencies.getJobManager().add(new RetrieveProfileAvatarJob(Recipient.self(), str));
    }

    private void setProfileCapabilities(SignalServiceProfile.Capabilities capabilities) {
        if (capabilities != null) {
            SignalDatabase.recipients().setCapabilities(Recipient.self().getId(), capabilities);
        }
    }

    private void ensureUnidentifiedAccessCorrect(String str, boolean z) {
        boolean z2;
        if (str == null) {
            Log.w(TAG, "No unidentified access is set remotely! Refreshing attributes.");
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        } else if (TextSecurePreferences.isUniversalUnidentifiedAccess(this.context) != z) {
            String str2 = TAG;
            Log.w(str2, "The universal access flag doesn't match our local value (local: " + TextSecurePreferences.isUniversalUnidentifiedAccess(this.context) + ", remote: " + z + ")! Refreshing attributes.");
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        } else {
            try {
                z2 = new ProfileCipher(ProfileKeyUtil.getSelfProfileKey()).verifyUnidentifiedAccess(Base64.decode(str));
            } catch (IOException e) {
                Log.w(TAG, "Failed to decode unidentified access!", e);
                z2 = false;
            }
            if (!z2) {
                Log.w(TAG, "Unidentified access failed to verify! Refreshing attributes.");
                ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
            }
        }
    }

    private void setProfileBadges(List<SignalServiceProfile.Badge> list) throws IOException {
        if (list != null) {
            Set set = (Set) Collection$EL.stream(Recipient.self().getBadges()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda0
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.lambda$setProfileBadges$1((Badge) obj);
                }
            }).map(new RefreshOwnProfileJob$$ExternalSyntheticLambda9()).collect(Collectors.toSet());
            Set set2 = (Set) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda12
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.lambda$setProfileBadges$2((SignalServiceProfile.Badge) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda13
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ((SignalServiceProfile.Badge) obj).getId();
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toSet());
            boolean anyMatch = Collection$EL.stream(set2).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda14
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.isSubscription((String) obj);
                }
            });
            boolean anyMatch2 = Collection$EL.stream(set).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda14
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.isSubscription((String) obj);
                }
            });
            boolean anyMatch3 = Collection$EL.stream(set2).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda15
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.isBoost((String) obj);
                }
            });
            boolean anyMatch4 = Collection$EL.stream(set).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda15
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.isBoost((String) obj);
                }
            });
            boolean anyMatch5 = Collection$EL.stream(set2).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda16
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.isGift((String) obj);
                }
            });
            boolean anyMatch6 = Collection$EL.stream(set).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda16
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.isGift((String) obj);
                }
            });
            if (!anyMatch && anyMatch2) {
                String str = TAG;
                Log.d(str, "Marking subscription badge as expired, should notify next time the conversation list is open.", true);
                SignalStore.donationsValues().setExpiredBadge((Badge) Collection$EL.stream(Recipient.self().getBadges()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda1
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return RefreshOwnProfileJob.lambda$setProfileBadges$3((Badge) obj);
                    }
                }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda2
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return RefreshOwnProfileJob.lambda$setProfileBadges$4((Badge) obj);
                    }
                }).max(Comparator.CC.comparingLong(new ToLongFunction() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda3
                    @Override // j$.util.function.ToLongFunction
                    public final long applyAsLong(Object obj) {
                        return ((Badge) obj).getExpirationTimestamp();
                    }
                })).get());
                if (!SignalStore.donationsValues().isUserManuallyCancelled()) {
                    Log.d(str, "Detected an unexpected subscription expiry.", true);
                    Subscriber subscriber = SignalStore.donationsValues().getSubscriber();
                    boolean z = false;
                    if (subscriber != null) {
                        ServiceResponse<ActiveSubscription> blockingGet = ApplicationDependencies.getDonationsService().getSubscription(subscriber.getSubscriberId()).blockingGet();
                        if (blockingGet.getResult().isPresent()) {
                            ActiveSubscription activeSubscription = blockingGet.getResult().get();
                            if (activeSubscription.isFailedPayment()) {
                                Log.d(str, "Unexpected expiry due to payment failure.", true);
                                z = true;
                            }
                            if (activeSubscription.getChargeFailure() != null) {
                                Log.d(str, "Active payment contains a charge failure: " + activeSubscription.getChargeFailure().getCode(), true);
                            }
                        }
                    }
                    if (!z) {
                        Log.d(str, "Unexpected expiry due to inactivity.", true);
                    }
                    MultiDeviceSubscriptionSyncRequestJob.enqueue();
                    SignalStore.donationsValues().setShouldCancelSubscriptionBeforeNextSubscribeAttempt(true);
                }
            } else if (anyMatch3 || !anyMatch4) {
                Badge expiredBadge = SignalStore.donationsValues().getExpiredBadge();
                if (expiredBadge != null && expiredBadge.isSubscription() && anyMatch) {
                    Log.d(TAG, "Remote has subscription badges. Clearing local expired subscription badge.", true);
                    SignalStore.donationsValues().setExpiredBadge(null);
                } else if (expiredBadge != null && expiredBadge.isBoost() && anyMatch3) {
                    Log.d(TAG, "Remote has boost badges. Clearing local expired boost badge.", true);
                    SignalStore.donationsValues().setExpiredBadge(null);
                }
            } else {
                Log.d(TAG, "Marking boost badge as expired, should notify next time the conversation list is open.", true);
                SignalStore.donationsValues().setExpiredBadge((Badge) Collection$EL.stream(Recipient.self().getBadges()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda4
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return RefreshOwnProfileJob.lambda$setProfileBadges$5((Badge) obj);
                    }
                }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda5
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return RefreshOwnProfileJob.lambda$setProfileBadges$6((Badge) obj);
                    }
                }).max(Comparator.CC.comparingLong(new ToLongFunction() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda3
                    @Override // j$.util.function.ToLongFunction
                    public final long applyAsLong(Object obj) {
                        return ((Badge) obj).getExpirationTimestamp();
                    }
                })).get());
            }
            if (!anyMatch5 && anyMatch6) {
                Log.d(TAG, "Marking gift badge as expired, should notify next time the manage donations screen is open.", true);
                SignalStore.donationsValues().setExpiredGiftBadge((Badge) Collection$EL.stream(Recipient.self().getBadges()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda6
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return RefreshOwnProfileJob.lambda$setProfileBadges$7((Badge) obj);
                    }
                }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda7
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return RefreshOwnProfileJob.lambda$setProfileBadges$8((Badge) obj);
                    }
                }).max(Comparator.CC.comparingLong(new ToLongFunction() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda3
                    @Override // j$.util.function.ToLongFunction
                    public final long applyAsLong(Object obj) {
                        return ((Badge) obj).getExpirationTimestamp();
                    }
                })).get());
            } else if (anyMatch5) {
                Log.d(TAG, "We have remote gift badges. Clearing local expired gift badge.", true);
                SignalStore.donationsValues().setExpiredGiftBadge(null);
            }
            boolean anyMatch7 = Collection$EL.stream(list).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda8
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ((SignalServiceProfile.Badge) obj).isVisible();
                }
            });
            boolean anyMatch8 = Collection$EL.stream(list).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob$$ExternalSyntheticLambda10
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return RefreshOwnProfileJob.lambda$setProfileBadges$9((SignalServiceProfile.Badge) obj);
                }
            });
            List<Badge> list2 = (List) Collection$EL.stream(list).map(new RefreshOwnProfileJob$$ExternalSyntheticLambda11()).collect(Collectors.toList());
            if (!anyMatch7 || !anyMatch8) {
                SignalDatabase.recipients().setBadges(Recipient.self().getId(), list2);
                return;
            }
            boolean displayBadgesOnProfile = SignalStore.donationsValues().getDisplayBadgesOnProfile();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Detected mixed visibility of badges. Telling the server to mark them all ");
            sb.append(displayBadgesOnProfile ? "" : "not");
            sb.append(" visible.");
            Log.d(str2, sb.toString(), true);
            SignalDatabase.recipients().setBadges(Recipient.self().getId(), new BadgeRepository(this.context).setVisibilityForAllBadgesSync(displayBadgesOnProfile, list2));
        }
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$1(Badge badge) {
        return badge.getCategory() == Badge.Category.Donor;
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$2(SignalServiceProfile.Badge badge) {
        return Objects.equals(badge.getCategory(), Badge.Category.Donor.getCode());
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$3(Badge badge) {
        return badge.getCategory() == Badge.Category.Donor;
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$4(Badge badge) {
        return isSubscription(badge.getId());
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$5(Badge badge) {
        return badge.getCategory() == Badge.Category.Donor;
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$6(Badge badge) {
        return isBoost(badge.getId());
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$7(Badge badge) {
        return badge.getCategory() == Badge.Category.Donor;
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$8(Badge badge) {
        return isGift(badge.getId());
    }

    public static /* synthetic */ boolean lambda$setProfileBadges$9(SignalServiceProfile.Badge badge) {
        return !badge.isVisible();
    }

    public static boolean isSubscription(String str) {
        return !isBoost(str) && !isGift(str);
    }

    public static boolean isBoost(String str) {
        return Objects.equals(str, Badge.BOOST_BADGE_ID);
    }

    public static boolean isGift(String str) {
        return Objects.equals(str, Badge.GIFT_BADGE_ID);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RefreshOwnProfileJob> {
        public RefreshOwnProfileJob create(Job.Parameters parameters, Data data) {
            return new RefreshOwnProfileJob(parameters);
        }
    }
}
