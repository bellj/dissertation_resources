package org.thoughtcrime.securesms.jobs;

import android.app.Application;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.messages.MessageContentProcessor;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.whispersystems.signalservice.api.groupsv2.NoCredentialForRedemptionTimeException;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public final class PushProcessMessageJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_EXCEPTION_DEVICE;
    private static final String KEY_EXCEPTION_GROUP_ID;
    private static final String KEY_EXCEPTION_SENDER;
    private static final String KEY_MESSAGE_PLAINTEXT;
    private static final String KEY_MESSAGE_STATE;
    private static final String KEY_SMS_MESSAGE_ID;
    private static final String KEY_TIMESTAMP;
    public static final String QUEUE_PREFIX;
    public static final String TAG = Log.tag(PushProcessMessageJob.class);
    private final SignalServiceContent content;
    private final MessageContentProcessor.ExceptionMetadata exceptionMetadata;
    private final MessageContentProcessor.MessageState messageState;
    private final long smsMessageId;
    private final long timestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public PushProcessMessageJob(SignalServiceContent signalServiceContent, long j, long j2) {
        this(MessageContentProcessor.MessageState.DECRYPTED_OK, signalServiceContent, null, j, j2);
    }

    public PushProcessMessageJob(MessageContentProcessor.MessageState messageState, MessageContentProcessor.ExceptionMetadata exceptionMetadata, long j, long j2) {
        this(messageState, null, exceptionMetadata, j, j2);
    }

    public PushProcessMessageJob(MessageContentProcessor.MessageState messageState, SignalServiceContent signalServiceContent, MessageContentProcessor.ExceptionMetadata exceptionMetadata, long j, long j2) {
        this(createParameters(signalServiceContent, exceptionMetadata), messageState, signalServiceContent, exceptionMetadata, j, j2);
    }

    private PushProcessMessageJob(Job.Parameters parameters, MessageContentProcessor.MessageState messageState, SignalServiceContent signalServiceContent, MessageContentProcessor.ExceptionMetadata exceptionMetadata, long j, long j2) {
        super(parameters);
        this.messageState = messageState;
        this.exceptionMetadata = exceptionMetadata;
        this.content = signalServiceContent;
        this.smsMessageId = j;
        this.timestamp = j2;
    }

    public static String getQueueName(RecipientId recipientId) {
        return QUEUE_PREFIX + recipientId.toQueueKey();
    }

    private static Job.Parameters createParameters(SignalServiceContent signalServiceContent, MessageContentProcessor.ExceptionMetadata exceptionMetadata) {
        Recipient recipient;
        String str;
        Application application = ApplicationDependencies.getApplication();
        Job.Parameters.Builder maxAttempts = new Job.Parameters.Builder().setMaxAttempts(-1);
        String str2 = QUEUE_PREFIX;
        if (signalServiceContent != null) {
            SignalServiceGroupContext groupContextIfPresent = GroupUtil.getGroupContextIfPresent(signalServiceContent);
            if (groupContextIfPresent != null) {
                try {
                    GroupId idFromGroupContext = GroupUtil.idFromGroupContext(groupContextIfPresent);
                    str2 = getQueueName(Recipient.externalPossiblyMigratedGroup(idFromGroupContext).getId());
                    if (idFromGroupContext.isV2() && (groupContextIfPresent.getGroupV2().get().getRevision() > SignalDatabase.groups().getGroupV2Revision(idFromGroupContext.requireV2()) || SignalDatabase.groups().getGroupV1ByExpectedV2(idFromGroupContext.requireV2()).isPresent())) {
                        Log.i(TAG, "Adding network constraint to group-related job.");
                        maxAttempts.addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(30));
                    }
                } catch (BadGroupIdException unused) {
                    String str3 = TAG;
                    Log.w(str3, "Bad groupId! Using default queue. ID: " + signalServiceContent.getTimestamp());
                }
            } else {
                if (!signalServiceContent.getSyncMessage().isPresent() || !signalServiceContent.getSyncMessage().get().getSent().isPresent() || !signalServiceContent.getSyncMessage().get().getSent().get().getDestination().isPresent()) {
                    str = getQueueName(RecipientId.from(signalServiceContent.getSender()));
                } else {
                    str = getQueueName(RecipientId.from(signalServiceContent.getSyncMessage().get().getSent().get().getDestination().get()));
                }
                str2 = str;
            }
        } else if (exceptionMetadata != null) {
            if (exceptionMetadata.getGroupId() != null) {
                recipient = Recipient.externalPossiblyMigratedGroup(exceptionMetadata.getGroupId());
            } else {
                recipient = Recipient.external(application, exceptionMetadata.getSender());
            }
            str2 = getQueueName(recipient.getId());
        }
        maxAttempts.setQueue(str2);
        return maxAttempts.build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder putLong = new Data.Builder().putInt(KEY_MESSAGE_STATE, this.messageState.ordinal()).putLong(KEY_SMS_MESSAGE_ID, this.smsMessageId).putLong("timestamp", this.timestamp);
        if (this.messageState == MessageContentProcessor.MessageState.DECRYPTED_OK) {
            SignalServiceContent signalServiceContent = this.content;
            Objects.requireNonNull(signalServiceContent);
            putLong.putString(KEY_MESSAGE_PLAINTEXT, Base64.encodeBytes(signalServiceContent.serialize()));
        } else {
            Objects.requireNonNull(this.exceptionMetadata);
            putLong.putString(KEY_EXCEPTION_SENDER, this.exceptionMetadata.getSender()).putInt(KEY_EXCEPTION_DEVICE, this.exceptionMetadata.getSenderDevice()).putString(KEY_EXCEPTION_GROUP_ID, this.exceptionMetadata.getGroupId() == null ? null : this.exceptionMetadata.getGroupId().toString());
        }
        return putLong.build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        MessageContentProcessor.forNormalContent(this.context).process(this.messageState, this.content, this.exceptionMetadata, this.timestamp, this.smsMessageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof NoCredentialForRedemptionTimeException) || (exc instanceof GroupChangeBusyException);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushProcessMessageJob> {
        public PushProcessMessageJob create(Job.Parameters parameters, Data data) {
            try {
                MessageContentProcessor.MessageState messageState = MessageContentProcessor.MessageState.values()[data.getInt(PushProcessMessageJob.KEY_MESSAGE_STATE)];
                if (messageState == MessageContentProcessor.MessageState.DECRYPTED_OK) {
                    return new PushProcessMessageJob(parameters, messageState, SignalServiceContent.deserialize(Base64.decode(data.getString(PushProcessMessageJob.KEY_MESSAGE_PLAINTEXT))), null, data.getLong(PushProcessMessageJob.KEY_SMS_MESSAGE_ID), data.getLong("timestamp"));
                }
                return new PushProcessMessageJob(parameters, messageState, null, new MessageContentProcessor.ExceptionMetadata(data.getString(PushProcessMessageJob.KEY_EXCEPTION_SENDER), data.getInt(PushProcessMessageJob.KEY_EXCEPTION_DEVICE), GroupId.parseNullableOrThrow(data.getStringOrDefault(PushProcessMessageJob.KEY_EXCEPTION_GROUP_ID, null))), data.getLong(PushProcessMessageJob.KEY_SMS_MESSAGE_ID), data.getLong("timestamp"));
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}
