package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import j$.util.Collection$EL;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda38;
import org.thoughtcrime.securesms.database.GroupDatabase$GroupRecord$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.ReactionDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class ReactionSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_INITIAL_RECIPIENT_COUNT;
    private static final String KEY_IS_MMS;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_REACTION_AUTHOR;
    private static final String KEY_REACTION_DATE_RECEIVED;
    private static final String KEY_REACTION_DATE_SENT;
    private static final String KEY_REACTION_EMOJI;
    private static final String KEY_RECIPIENTS;
    private static final String KEY_REMOVE;
    private static final String TAG = Log.tag(ReactionSendJob.class);
    private final int initialRecipientCount;
    private final MessageId messageId;
    private final ReactionRecord reaction;
    private final List<RecipientId> recipients;
    private final boolean remove;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public static ReactionSendJob create(Context context, MessageId messageId, ReactionRecord reactionRecord, boolean z) throws NoSuchMessageException {
        MessageRecord messageRecord;
        List list;
        if (messageId.isMms()) {
            messageRecord = SignalDatabase.mms().getMessageRecord(messageId.getId());
        } else {
            messageRecord = SignalDatabase.sms().getSmsMessage(messageId.getId());
        }
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(messageRecord.getThreadId());
        if (recipientForThreadId != null) {
            RecipientId id = Recipient.self().getId();
            if (recipientForThreadId.isGroup()) {
                list = (List) Collection$EL.stream(RecipientUtil.getEligibleForSending(Recipient.resolvedList(recipientForThreadId.getParticipantIds()))).map(new ConversationListFragment$$ExternalSyntheticLambda38()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.ReactionSendJob$$ExternalSyntheticLambda2
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return ReactionSendJob.lambda$create$0(RecipientId.this, (RecipientId) obj);
                    }
                }).collect(Collectors.toList());
            } else {
                list = Collections.singletonList(recipientForThreadId.getId());
            }
            return new ReactionSendJob(messageId, list, list.size(), reactionRecord, z, new Job.Parameters.Builder().setQueue(recipientForThreadId.getId().toQueueKey()).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
        }
        throw new AssertionError("We have a message, but couldn't find the thread!");
    }

    public static /* synthetic */ boolean lambda$create$0(RecipientId recipientId, RecipientId recipientId2) {
        return !recipientId2.equals(recipientId);
    }

    private ReactionSendJob(MessageId messageId, List<RecipientId> list, int i, ReactionRecord reactionRecord, boolean z, Job.Parameters parameters) {
        super(parameters);
        this.messageId = messageId;
        this.recipients = list;
        this.initialRecipientCount = i;
        this.reaction = reactionRecord;
        this.remove = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId.getId()).putBoolean("is_mms", this.messageId.isMms()).putString(KEY_REACTION_EMOJI, this.reaction.getEmoji()).putString(KEY_REACTION_AUTHOR, this.reaction.getAuthor().serialize()).putLong(KEY_REACTION_DATE_SENT, this.reaction.getDateSent()).putLong(KEY_REACTION_DATE_RECEIVED, this.reaction.getDateReceived()).putBoolean(KEY_REMOVE, this.remove).putString("recipients", RecipientId.toSerializedList(this.recipients)).putInt(KEY_INITIAL_RECIPIENT_COUNT, this.initialRecipientCount).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        MessageRecord messageRecord;
        if (Recipient.self().isRegistered()) {
            ReactionDatabase reactions = SignalDatabase.reactions();
            if (this.messageId.isMms()) {
                messageRecord = SignalDatabase.mms().getMessageRecord(this.messageId.getId());
            } else {
                messageRecord = SignalDatabase.sms().getSmsMessage(this.messageId.getId());
            }
            Recipient self = messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getIndividualRecipient();
            long dateSent = messageRecord.getDateSent();
            if (!self.getId().equals(SignalStore.releaseChannelValues().getReleaseChannelRecipientId())) {
                if (!this.remove && !reactions.hasReaction(this.messageId, this.reaction)) {
                    Log.w(TAG, "Went to add a reaction, but it's no longer present on the message!");
                } else if (!this.remove || !reactions.hasReaction(this.messageId, this.reaction)) {
                    Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(messageRecord.getThreadId());
                    if (recipientForThreadId == null) {
                        throw new AssertionError("We have a message, but couldn't find the thread!");
                    } else if (recipientForThreadId.isPushV1Group() || recipientForThreadId.isMmsGroup()) {
                        Log.w(TAG, "Cannot send reactions to legacy groups.");
                    } else {
                        List list = (List) Collection$EL.stream(this.recipients).map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).collect(Collectors.toList());
                        List<Recipient> deliver = deliver(recipientForThreadId, (List) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.ReactionSendJob$$ExternalSyntheticLambda1
                            @Override // j$.util.function.Predicate
                            public /* synthetic */ Predicate and(Predicate predicate) {
                                return Predicate.CC.$default$and(this, predicate);
                            }

                            @Override // j$.util.function.Predicate
                            public /* synthetic */ Predicate negate() {
                                return Predicate.CC.$default$negate(this);
                            }

                            @Override // j$.util.function.Predicate
                            public /* synthetic */ Predicate or(Predicate predicate) {
                                return Predicate.CC.$default$or(this, predicate);
                            }

                            @Override // j$.util.function.Predicate
                            public final boolean test(Object obj) {
                                return ((Recipient) obj).isMaybeRegistered();
                            }
                        }).collect(Collectors.toList()), self, dateSent);
                        this.recipients.removeAll((List) Collection$EL.stream(list).filter(new ReactionSendJob$$ExternalSyntheticLambda0()).map(new ConversationListFragment$$ExternalSyntheticLambda38()).collect(Collectors.toList()));
                        this.recipients.removeAll((Collection) Collection$EL.stream(deliver).map(new ConversationListFragment$$ExternalSyntheticLambda38()).collect(Collectors.toList()));
                        String str = TAG;
                        Log.i(str, "Completed now: " + deliver.size() + ", Remaining: " + this.recipients.size());
                        if (!this.recipients.isEmpty()) {
                            Log.w(str, "Still need to send to " + this.recipients.size() + " recipients. Retrying.");
                            throw new RetryLaterException();
                        }
                    }
                } else {
                    Log.w(TAG, "Went to remove a reaction, but it's still there!");
                }
            }
        } else {
            throw new NotPushRegisteredException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if ((exc instanceof ServerRejectedException) || (exc instanceof NotPushRegisteredException)) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        if (this.recipients.size() < this.initialRecipientCount) {
            String str = TAG;
            Log.w(str, "Only sent a reaction to " + this.recipients.size() + "/" + this.initialRecipientCount + " recipients. Still, it sent to someone, so it stays.");
            return;
        }
        String str2 = TAG;
        Log.w(str2, "Failed to send the reaction to all recipients!");
        ReactionDatabase reactions = SignalDatabase.reactions();
        if (this.remove && !reactions.hasReaction(this.messageId, this.reaction)) {
            Log.w(str2, "Reaction removal failed, so adding the reaction back.");
            reactions.addReaction(this.messageId, this.reaction);
        } else if (this.remove || !reactions.hasReaction(this.messageId, this.reaction)) {
            Log.w(str2, "Reaction state didn't match what we'd expect to revert it, so we're just leaving it alone.");
        } else {
            Log.w(str2, "Reaction addition failed, so removing the reaction.");
            reactions.deleteReaction(this.messageId, this.reaction.getAuthor());
        }
    }

    private List<Recipient> deliver(Recipient recipient, List<Recipient> list, Recipient recipient2, long j) throws IOException, UntrustedIdentityException {
        SignalServiceDataMessage.Builder withReaction = SignalServiceDataMessage.newBuilder().withTimestamp(System.currentTimeMillis()).withReaction(buildReaction(this.context, this.reaction, this.remove, recipient2, j));
        if (recipient.isGroup()) {
            GroupUtil.setDataMessageGroupContext(this.context, withReaction, recipient.requireGroupId().requirePush());
        }
        SignalServiceDataMessage build = withReaction.build();
        List list2 = (List) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.ReactionSendJob$$ExternalSyntheticLambda3
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ReactionSendJob.lambda$deliver$1((Recipient) obj);
            }
        }).collect(Collectors.toList());
        boolean z = list2.size() != list.size();
        List<SendMessageResult> sendResendableDataMessage = GroupSendUtil.sendResendableDataMessage(this.context, (GroupId.V2) recipient.getGroupId().map(new PushGroupSendJob$$ExternalSyntheticLambda24()).orElse(null), list2, false, ContentHint.RESENDABLE, this.messageId, build);
        if (z) {
            sendResendableDataMessage.add(ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(build));
        }
        return GroupSendJobHelper.getCompletedSends(list, sendResendableDataMessage).completed;
    }

    public static /* synthetic */ boolean lambda$deliver$1(Recipient recipient) {
        return !recipient.isSelf();
    }

    private static SignalServiceDataMessage.Reaction buildReaction(Context context, ReactionRecord reactionRecord, boolean z, Recipient recipient, long j) throws IOException {
        return new SignalServiceDataMessage.Reaction(reactionRecord.getEmoji(), z, RecipientUtil.getOrFetchServiceId(context, recipient), j);
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ReactionSendJob> {
        public ReactionSendJob create(Job.Parameters parameters, Data data) {
            long j = data.getLong("message_id");
            boolean z = data.getBoolean("is_mms");
            return new ReactionSendJob(new MessageId(j, z), RecipientId.fromSerializedList(data.getString("recipients")), data.getInt(ReactionSendJob.KEY_INITIAL_RECIPIENT_COUNT), new ReactionRecord(data.getString(ReactionSendJob.KEY_REACTION_EMOJI), RecipientId.from(data.getString(ReactionSendJob.KEY_REACTION_AUTHOR)), data.getLong(ReactionSendJob.KEY_REACTION_DATE_SENT), data.getLong(ReactionSendJob.KEY_REACTION_DATE_RECEIVED)), data.getBoolean(ReactionSendJob.KEY_REMOVE), parameters);
        }
    }
}
