package org.thoughtcrime.securesms.jobs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.RecipientAccessList;
import org.whispersystems.signalservice.api.messages.SendMessageResult;

/* loaded from: classes4.dex */
public final class GroupSendJobHelper {
    private static final String TAG = Log.tag(GroupSendJobHelper.class);

    private GroupSendJobHelper() {
    }

    public static SendResult getCompletedSends(List<Recipient> list, Collection<SendMessageResult> collection) {
        RecipientAccessList recipientAccessList = new RecipientAccessList(list);
        ArrayList arrayList = new ArrayList(collection.size());
        ArrayList arrayList2 = new ArrayList();
        for (SendMessageResult sendMessageResult : collection) {
            Recipient requireByAddress = recipientAccessList.requireByAddress(sendMessageResult.getAddress());
            if (sendMessageResult.getIdentityFailure() != null) {
                String str = TAG;
                Log.w(str, "Identity failure for " + requireByAddress.getId());
            }
            if (sendMessageResult.isUnregisteredFailure()) {
                String str2 = TAG;
                Log.w(str2, "Unregistered failure for " + requireByAddress.getId());
                arrayList2.add(requireByAddress.getId());
            }
            if (sendMessageResult.getProofRequiredFailure() != null) {
                String str3 = TAG;
                Log.w(str3, "Proof required failure for " + requireByAddress.getId());
                arrayList2.add(requireByAddress.getId());
            }
            if (sendMessageResult.getSuccess() != null || sendMessageResult.getIdentityFailure() != null || sendMessageResult.getProofRequiredFailure() != null || sendMessageResult.isUnregisteredFailure()) {
                arrayList.add(requireByAddress);
            }
        }
        return new SendResult(arrayList, arrayList2);
    }

    /* loaded from: classes4.dex */
    public static class SendResult {
        public final List<Recipient> completed;
        public final List<RecipientId> skipped;

        public SendResult(List<Recipient> list, List<RecipientId> list2) {
            this.completed = list;
            this.skipped = list2;
        }
    }
}
