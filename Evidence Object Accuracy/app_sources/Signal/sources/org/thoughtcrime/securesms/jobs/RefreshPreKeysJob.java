package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.thoughtcrime.securesms.crypto.PreKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceAccountDataStoreImpl;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ServiceIdType;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class RefreshPreKeysJob extends BaseJob {
    public static final String KEY;
    private static final int PREKEY_MINIMUM;
    private static final long REFRESH_INTERVAL = TimeUnit.DAYS.toMillis(3);
    private static final String TAG = Log.tag(RefreshPreKeysJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public RefreshPreKeysJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxInstancesForFactory(1).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(30)).build());
    }

    public static void scheduleIfNecessary() {
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.misc().getLastPrekeyRefreshTime();
        if (currentTimeMillis > REFRESH_INTERVAL) {
            String str = TAG;
            Log.i(str, "Scheduling a prekey refresh. Time since last schedule: " + currentTimeMillis + " ms");
            ApplicationDependencies.getJobManager().add(new RefreshPreKeysJob());
        }
    }

    private RefreshPreKeysJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null || SignalStore.account().getPni() == null) {
            Log.w(TAG, "Not registered. Skipping.");
            return;
        }
        SignalServiceAccountDataStoreImpl aci = ApplicationDependencies.getProtocolStore().aci();
        PreKeyMetadataStore aciPreKeys = SignalStore.account().aciPreKeys();
        SignalServiceAccountDataStoreImpl pni = ApplicationDependencies.getProtocolStore().pni();
        PreKeyMetadataStore pniPreKeys = SignalStore.account().pniPreKeys();
        if (refreshKeys(ServiceIdType.ACI, aci, aciPreKeys)) {
            PreKeyUtil.cleanSignedPreKeys(aci, aciPreKeys);
        }
        if (refreshKeys(ServiceIdType.PNI, pni, pniPreKeys)) {
            PreKeyUtil.cleanSignedPreKeys(pni, pniPreKeys);
        }
        SignalStore.misc().setLastPrekeyRefreshTime(System.currentTimeMillis());
        Log.i(TAG, "Successfully refreshed prekeys.");
    }

    private boolean refreshKeys(ServiceIdType serviceIdType, SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore) throws IOException {
        String str = "[" + serviceIdType + "] ";
        SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
        int preKeysCount = signalServiceAccountManager.getPreKeysCount(serviceIdType);
        String str2 = TAG;
        log(str2, str + "Available keys: " + preKeysCount);
        if (preKeysCount < 10 || !preKeyMetadataStore.isSignedPreKeyRegistered()) {
            List<PreKeyRecord> generateAndStoreOneTimePreKeys = PreKeyUtil.generateAndStoreOneTimePreKeys(signalProtocolStore, preKeyMetadataStore);
            SignedPreKeyRecord generateAndStoreSignedPreKey = PreKeyUtil.generateAndStoreSignedPreKey(signalProtocolStore, preKeyMetadataStore, false);
            IdentityKeyPair identityKeyPair = signalProtocolStore.getIdentityKeyPair();
            log(str2, str + "Registering new prekeys...");
            signalServiceAccountManager.setPreKeys(serviceIdType, identityKeyPair.getPublicKey(), generateAndStoreSignedPreKey, generateAndStoreOneTimePreKeys);
            preKeyMetadataStore.setActiveSignedPreKeyId(generateAndStoreSignedPreKey.getId());
            preKeyMetadataStore.setSignedPreKeyRegistered(true);
            log(str2, str + "Need to clean prekeys.");
            return true;
        }
        log(str2, str + "Available keys sufficient.");
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof NonSuccessfulResponseCodeException) && (exc instanceof PushNetworkException)) {
            return true;
        }
        return false;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RefreshPreKeysJob> {
        public RefreshPreKeysJob create(Job.Parameters parameters, Data data) {
            return new RefreshPreKeysJob(parameters);
        }
    }
}
