package org.thoughtcrime.securesms.jobs;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.EmojiSearchData;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.EmojiValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.s3.S3;
import org.thoughtcrime.securesms.util.dynamiclanguage.DynamicLanguageContextWrapper;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes4.dex */
public final class EmojiSearchIndexDownloadJob extends BaseJob {
    private static final long INTERVAL_WITHOUT_INDEX;
    private static final long INTERVAL_WITH_INDEX;
    public static final String KEY;
    private static final String TAG = Log.tag(EmojiSearchIndexDownloadJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    static {
        TAG = Log.tag(EmojiSearchIndexDownloadJob.class);
        TimeUnit timeUnit = TimeUnit.DAYS;
        INTERVAL_WITHOUT_INDEX = timeUnit.toMillis(1);
        INTERVAL_WITH_INDEX = timeUnit.toMillis(7);
    }

    private EmojiSearchIndexDownloadJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).setMaxInstancesForFactory(2).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private EmojiSearchIndexDownloadJob(Job.Parameters parameters) {
        super(parameters);
    }

    public static void scheduleImmediately() {
        ApplicationDependencies.getJobManager().add(new EmojiSearchIndexDownloadJob());
    }

    public static void scheduleIfNecessary() {
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.emojiValues().getLastSearchIndexCheck();
        boolean z = true;
        if (!SignalStore.emojiValues().hasSearchIndex() ? currentTimeMillis <= INTERVAL_WITHOUT_INDEX : currentTimeMillis <= INTERVAL_WITH_INDEX) {
            z = false;
        }
        if (z) {
            String str = TAG;
            Log.i(str, "Need to check. It's been " + currentTimeMillis + " ms since the last check.");
            scheduleImmediately();
            return;
        }
        String str2 = TAG;
        Log.d(str2, "Do not need to check. It's been " + currentTimeMillis + " ms since the last check.");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        Manifest downloadManifest = downloadManifest();
        String findMatchingLanguage = findMatchingLanguage(DynamicLanguageContextWrapper.getUsersSelectedLocale(this.context), downloadManifest.getLanguages());
        if (downloadManifest.getVersion() != SignalStore.emojiValues().getSearchVersion() || !findMatchingLanguage.equals(SignalStore.emojiValues().getSearchLanguage())) {
            String str = TAG;
            Log.i(str, "Need to get a new search index. Downloading version: " + downloadManifest.getVersion() + ", language: " + findMatchingLanguage);
            SignalDatabase.emojiSearch().setSearchIndex(downloadSearchIndex(downloadManifest.getVersion(), findMatchingLanguage));
            SignalStore.emojiValues().onSearchIndexUpdated(downloadManifest.getVersion(), findMatchingLanguage);
            SignalStore.emojiValues().setLastSearchIndexCheck(System.currentTimeMillis());
            Log.i(str, "Success! Now at version: " + downloadManifest.getVersion() + ", language: " + findMatchingLanguage);
            return;
        }
        String str2 = TAG;
        Log.i(str2, "Already using the latest version of " + downloadManifest.getVersion() + " with the correct language " + findMatchingLanguage);
        SignalStore.emojiValues().setLastSearchIndexCheck(System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return (exc instanceof IOException) && !(exc instanceof NonSuccessfulResponseCodeException);
    }

    private static Manifest downloadManifest() throws IOException {
        return (Manifest) JsonUtil.fromJson(S3.getString("/dynamic/android/emoji/search/manifest.json"), Manifest.class);
    }

    private static List<EmojiSearchData> downloadSearchIndex(int i, String str) throws IOException {
        return Arrays.asList((EmojiSearchData[]) JsonUtil.fromJson(S3.getString("/static/android/emoji/search/" + i + "/" + str + ".json"), EmojiSearchData[].class));
    }

    private static String findMatchingLanguage(Locale locale, List<String> list) {
        String str = null;
        for (String str2 : list) {
            Locale locale2 = new Locale(str2);
            if (locale.getLanguage().equals(locale2.getLanguage())) {
                if (locale.getVariant().equals(locale2.getVariant())) {
                    String str3 = TAG;
                    Log.d(str3, "Found an exact match: " + str2);
                    return str2;
                } else if (locale.getVariant().equals("")) {
                    String str4 = TAG;
                    Log.d(str4, "Found the parent language: " + str2);
                    str = str2;
                }
            }
        }
        if (str != null) {
            String str5 = TAG;
            Log.i(str5, "No exact match found. Using parent language: " + str);
            return str;
        } else if (list.contains("en")) {
            Log.w(TAG, "No match, so falling back to en locale.");
            return "en";
        } else if (list.contains("en_US")) {
            Log.w(TAG, "No match, so falling back to en_US locale.");
            return "en_US";
        } else {
            Log.w(TAG, "No match and no english fallback! Must return no language!");
            return EmojiValues.NO_LANGUAGE;
        }
    }

    /* loaded from: classes.dex */
    public static class Manifest {
        @JsonProperty
        private List<String> languages;
        @JsonProperty
        private int version;

        public int getVersion() {
            return this.version;
        }

        public List<String> getLanguages() {
            List<String> list = this.languages;
            return list != null ? list : Collections.emptyList();
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<EmojiSearchIndexDownloadJob> {
        public EmojiSearchIndexDownloadJob create(Job.Parameters parameters, Data data) {
            return new EmojiSearchIndexDownloadJob(parameters);
        }
    }
}
