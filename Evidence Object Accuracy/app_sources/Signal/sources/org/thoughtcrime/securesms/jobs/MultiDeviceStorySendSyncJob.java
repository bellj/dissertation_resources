package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SentStorySyncManifest;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessageRecipient;
import org.whispersystems.signalservice.api.messages.multidevice.SentTranscriptMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* compiled from: MultiDeviceStorySendSyncJob.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00182\u00020\u0001:\u0003\u0018\u0019\u001aB\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0016\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u0010H\u0014J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\b\u0010\u0016\u001a\u00020\u0017H\u0016R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceStorySendSyncJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "sentTimestamp", "", "deletedMessageId", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;JJ)V", "buildSentTranscript", "Lorg/whispersystems/signalservice/api/messages/multidevice/SentTranscriptMessage;", "recipientsSet", "", "Lorg/whispersystems/signalservice/api/messages/SignalServiceStoryMessageRecipient;", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "RetryableException", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiDeviceStorySendSyncJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    private static final String DATA_DELETED_MESSAGE_ID;
    private static final String DATA_SENT_TIMESTAMP;
    public static final String KEY;
    private static final String TAG = Log.tag(MultiDeviceStorySendSyncJob.class);
    private final long deletedMessageId;
    private final long sentTimestamp;

    /* compiled from: MultiDeviceStorySendSyncJob.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceStorySendSyncJob$RetryableException;", "Ljava/lang/Exception;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RetryableException extends Exception {
    }

    public /* synthetic */ MultiDeviceStorySendSyncJob(Job.Parameters parameters, long j, long j2, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters, j, j2);
    }

    @JvmStatic
    public static final MultiDeviceStorySendSyncJob create(long j, long j2) {
        return Companion.create(j, j2);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private MultiDeviceStorySendSyncJob(Job.Parameters parameters, long j, long j2) {
        super(parameters);
        this.sentTimestamp = j;
        this.deletedMessageId = j2;
    }

    /* compiled from: MultiDeviceStorySendSyncJob.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \b*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceStorySendSyncJob$Companion;", "", "()V", "DATA_DELETED_MESSAGE_ID", "", "DATA_SENT_TIMESTAMP", "KEY", "TAG", "kotlin.jvm.PlatformType", "create", "Lorg/thoughtcrime/securesms/jobs/MultiDeviceStorySendSyncJob;", "sentTimestamp", "", "deletedMessageId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final MultiDeviceStorySendSyncJob create(long j, long j2) {
            Job.Parameters build = new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(1)).setQueue(MultiDeviceStorySendSyncJob.KEY).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder()\n          .add…e(KEY)\n          .build()");
            return new MultiDeviceStorySendSyncJob(build, j, j2, null);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data build = new Data.Builder().putLong(DATA_SENT_TIMESTAMP, this.sentTimestamp).putLong(DATA_DELETED_MESSAGE_ID, this.deletedMessageId).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n      .putLong…MessageId)\n      .build()");
        return build;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        Set<RecipientId> recipientIdsForManifestUpdate = companion.storySends().getRecipientIdsForManifestUpdate(this.sentTimestamp, this.deletedMessageId);
        if (recipientIdsForManifestUpdate.isEmpty()) {
            Log.i(TAG, "No recipients requiring a manifest update. Dropping.");
            return;
        }
        SentStorySyncManifest sentStorySyncManifestForUpdate = companion.storySends().getSentStorySyncManifestForUpdate(this.sentTimestamp, recipientIdsForManifestUpdate);
        if (sentStorySyncManifestForUpdate.getEntries().isEmpty()) {
            Log.i(TAG, "No entries in updated manifest. Dropping.");
            return;
        }
        SignalServiceSyncMessage forSentTranscript = SignalServiceSyncMessage.forSentTranscript(buildSentTranscript(sentStorySyncManifestForUpdate.toRecipientsSet()));
        Intrinsics.checkNotNullExpressionValue(forSentTranscript, "forSentTranscript(buildS…ranscript(recipientsSet))");
        if (!ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(forSentTranscript, Optional.empty()).isSuccess()) {
            throw new RetryableException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return exc instanceof RetryableException;
    }

    private final SentTranscriptMessage buildSentTranscript(Set<? extends SignalServiceStoryMessageRecipient> set) {
        return new SentTranscriptMessage(Optional.of(new SignalServiceAddress(Recipient.self().requireServiceId())), this.sentTimestamp, Optional.empty(), 0, MapsKt__MapsKt.emptyMap(), true, Optional.empty(), set);
    }

    /* compiled from: MultiDeviceStorySendSyncJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/MultiDeviceStorySendSyncJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/MultiDeviceStorySendSyncJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceStorySendSyncJob> {
        public MultiDeviceStorySendSyncJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new MultiDeviceStorySendSyncJob(parameters, data.getLong(MultiDeviceStorySendSyncJob.DATA_SENT_TIMESTAMP), data.getLong(MultiDeviceStorySendSyncJob.DATA_DELETED_MESSAGE_ID), null);
        }
    }
}
