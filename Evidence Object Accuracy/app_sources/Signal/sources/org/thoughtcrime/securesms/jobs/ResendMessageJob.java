package org.thoughtcrime.securesms.jobs;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public class ResendMessageJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_CONTENT;
    private static final String KEY_CONTENT_HINT;
    private static final String KEY_DISTRIBUTION_ID;
    private static final String KEY_GROUP_ID;
    private static final String KEY_RECIPIENT_ID;
    private static final String KEY_SENT_TIMESTAMP;
    private static final String TAG = Log.tag(ResendMessageJob.class);
    private final SignalServiceProtos.Content content;
    private final ContentHint contentHint;
    private final DistributionId distributionId;
    private final GroupId.V2 groupId;
    private final RecipientId recipientId;
    private final long sentTimestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public ResendMessageJob(RecipientId recipientId, long j, SignalServiceProtos.Content content, ContentHint contentHint, GroupId.V2 v2, DistributionId distributionId) {
        this(recipientId, j, content, contentHint, v2, distributionId, new Job.Parameters.Builder().setQueue(recipientId.toQueueKey()).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).addConstraint(NetworkConstraint.KEY).build());
    }

    private ResendMessageJob(RecipientId recipientId, long j, SignalServiceProtos.Content content, ContentHint contentHint, GroupId.V2 v2, DistributionId distributionId, Job.Parameters parameters) {
        super(parameters);
        this.recipientId = recipientId;
        this.sentTimestamp = j;
        this.content = content;
        this.contentHint = contentHint;
        this.groupId = v2;
        this.distributionId = distributionId;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder putInt = new Data.Builder().putString("recipient_id", this.recipientId.serialize()).putLong("sent_timestamp", this.sentTimestamp).putBlobAsString("content", this.content.toByteArray()).putInt("content_hint", this.contentHint.getType());
        GroupId.V2 v2 = this.groupId;
        String str = null;
        Data.Builder putBlobAsString = putInt.putBlobAsString("group_id", v2 != null ? v2.getDecodedId() : null);
        DistributionId distributionId = this.distributionId;
        if (distributionId != null) {
            str = distributionId.toString();
        }
        return putBlobAsString.putString("distribution_id", str).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (SignalStore.internalValues().delayResends()) {
            Log.w(TAG, "Delaying resend by 10 sec because of an internal preference.");
            ThreadUtil.sleep(10000);
        }
        SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
        Recipient resolved = Recipient.resolved(this.recipientId);
        if (resolved.isUnregistered()) {
            String str = TAG;
            Log.w(str, resolved.getId() + " is unregistered!");
            return;
        }
        SignalServiceAddress signalServiceAddress = RecipientUtil.toSignalServiceAddress(this.context, resolved);
        Optional<UnidentifiedAccessPair> accessFor = UnidentifiedAccessUtil.getAccessFor(this.context, resolved);
        SignalServiceProtos.Content content = this.content;
        if (this.distributionId != null) {
            Optional<GroupDatabase.GroupRecord> groupByDistributionId = SignalDatabase.groups().getGroupByDistributionId(this.distributionId);
            if (!groupByDistributionId.isPresent()) {
                Log.w(TAG, "Could not find a matching group for the distributionId! Skipping message send.");
                return;
            } else if (!groupByDistributionId.get().getMembers().contains(this.recipientId)) {
                Log.w(TAG, "The target user is no longer in the group! Skipping message send.");
                return;
            } else {
                content = content.toBuilder().setSenderKeyDistributionMessage(ByteString.copyFrom(signalServiceMessageSender.getOrCreateNewGroupSession(this.distributionId).serialize())).build();
            }
        }
        SendMessageResult resendContent = signalServiceMessageSender.resendContent(signalServiceAddress, accessFor, this.sentTimestamp, content, this.contentHint, Optional.ofNullable(this.groupId).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.ResendMessageJob$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((GroupId.V2) obj).getDecodedId();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }));
        if (resendContent.isSuccess() && this.distributionId != null) {
            ApplicationDependencies.getProtocolStore().aci().markSenderKeySharedWith(this.distributionId, (List) Collection$EL.stream(resendContent.getSuccess().getDevices()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.ResendMessageJob$$ExternalSyntheticLambda1
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ResendMessageJob.lambda$onRun$0(Recipient.this, (Integer) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toList()));
        }
    }

    public static /* synthetic */ SignalProtocolAddress lambda$onRun$0(Recipient recipient, Integer num) {
        return recipient.requireServiceId().toProtocolAddress(num.intValue());
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<ResendMessageJob> {
        public ResendMessageJob create(Job.Parameters parameters, Data data) {
            try {
                SignalServiceProtos.Content parseFrom = SignalServiceProtos.Content.parseFrom(data.getStringAsBlob("content"));
                byte[] stringAsBlob = data.getStringAsBlob("group_id");
                GroupId.V2 requireV2 = stringAsBlob != null ? GroupId.pushOrThrow(stringAsBlob).requireV2() : null;
                String string = data.getString("distribution_id");
                return new ResendMessageJob(RecipientId.from(data.getString("recipient_id")), data.getLong("sent_timestamp"), parseFrom, ContentHint.fromType(data.getInt("content_hint")), requireV2, string != null ? DistributionId.from(string) : null, parameters);
            } catch (InvalidProtocolBufferException e) {
                throw new AssertionError(e);
            }
        }
    }
}
