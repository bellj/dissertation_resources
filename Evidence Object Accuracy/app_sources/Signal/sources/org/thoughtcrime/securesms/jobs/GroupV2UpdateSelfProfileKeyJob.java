package org.thoughtcrime.securesms.jobs;

import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Predicate;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupInsufficientRightsException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.DecryptionsDrainedConstraint;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.groupsv2.NoCredentialForRedemptionTimeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public final class GroupV2UpdateSelfProfileKeyJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String QUEUE;
    private static final String TAG = Log.tag(GroupV2UpdateSelfProfileKeyJob.class);
    private final GroupId.V2 groupId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return "GroupV2UpdateSelfProfileKeyJob";
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public static GroupV2UpdateSelfProfileKeyJob withoutLimits(GroupId.V2 v2) {
        return new GroupV2UpdateSelfProfileKeyJob(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).setQueue("GroupV2UpdateSelfProfileKeyJob").build(), v2);
    }

    public static GroupV2UpdateSelfProfileKeyJob withQueueLimits(GroupId.V2 v2) {
        Job.Parameters.Builder maxAttempts = new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).addConstraint(DecryptionsDrainedConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1);
        return new GroupV2UpdateSelfProfileKeyJob(maxAttempts.setQueue("GroupV2UpdateSelfProfileKeyJob_" + v2.toString()).setMaxInstancesForQueue(1).build(), v2);
    }

    public static void enqueueForGroupsIfNecessary() {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null || !Recipient.self().isRegistered()) {
            Log.w(TAG, "Not yet registered!");
            return;
        }
        byte[] profileKey = Recipient.self().getProfileKey();
        if (profileKey == null) {
            Log.w(TAG, "No profile key set!");
            return;
        }
        ByteString copyFrom = ByteString.copyFrom(profileKey);
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.misc().getLastGv2ProfileCheckTime();
        if (currentTimeMillis < TimeUnit.DAYS.toMillis(1)) {
            String str = TAG;
            Log.d(str, "Too soon. Last check was " + currentTimeMillis + " ms ago.");
            return;
        }
        Log.i(TAG, "Running routine check.");
        SignalStore.misc().setLastGv2ProfileCheckTime(System.currentTimeMillis());
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.jobs.GroupV2UpdateSelfProfileKeyJob$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                GroupV2UpdateSelfProfileKeyJob.lambda$enqueueForGroupsIfNecessary$1(ByteString.this);
            }
        });
    }

    public static /* synthetic */ void lambda$enqueueForGroupsIfNecessary$1(ByteString byteString) {
        boolean z = false;
        for (GroupId.V2 v2 : SignalDatabase.groups().getAllGroupV2Ids()) {
            Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(v2);
            if (!group.isPresent()) {
                String str = TAG;
                Log.w(str, "Group " + group + " no longer exists?");
            } else {
                DecryptedMember decryptedMember = (DecryptedMember) Collection$EL.stream(group.get().requireV2GroupProperties().getDecryptedGroup().getMembersList()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.GroupV2UpdateSelfProfileKeyJob$$ExternalSyntheticLambda0
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return GroupV2UpdateSelfProfileKeyJob.lambda$enqueueForGroupsIfNecessary$0(ByteString.this, (DecryptedMember) obj);
                    }
                }).findFirst().orElse(null);
                if (decryptedMember != null && !decryptedMember.getProfileKey().equals(byteString)) {
                    String str2 = TAG;
                    Log.w(str2, "Profile key mismatch for group " + v2 + " -- enqueueing job");
                    z = true;
                    ApplicationDependencies.getJobManager().add(withQueueLimits(v2));
                }
            }
        }
        if (!z) {
            Log.i(TAG, "No mismatches found.");
        }
    }

    public static /* synthetic */ boolean lambda$enqueueForGroupsIfNecessary$0(ByteString byteString, DecryptedMember decryptedMember) {
        return decryptedMember.getUuid().equals(byteString);
    }

    private GroupV2UpdateSelfProfileKeyJob(Job.Parameters parameters, GroupId.V2 v2) {
        super(parameters);
        this.groupId = v2;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("group_id", this.groupId.toString()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, GroupNotAMemberException, GroupChangeFailedException, GroupInsufficientRightsException, GroupChangeBusyException {
        String str = TAG;
        Log.i(str, "Ensuring profile key up to date on group " + this.groupId);
        GroupManager.updateSelfProfileKeyInGroup(this.context, this.groupId);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof NoCredentialForRedemptionTimeException) || (exc instanceof GroupChangeBusyException);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<GroupV2UpdateSelfProfileKeyJob> {
        public GroupV2UpdateSelfProfileKeyJob create(Job.Parameters parameters, Data data) {
            return new GroupV2UpdateSelfProfileKeyJob(parameters, GroupId.parseOrThrow(data.getString("group_id")).requireV2());
        }
    }
}
