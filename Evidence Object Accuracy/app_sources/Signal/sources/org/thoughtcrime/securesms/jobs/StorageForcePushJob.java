package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.UnknownStorageIdDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.storage.StorageSyncModels;
import org.thoughtcrime.securesms.storage.StorageSyncValidations;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.storage.StorageKey;

/* loaded from: classes4.dex */
public class StorageForcePushJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(StorageForcePushJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public StorageForcePushJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(StorageSyncJob.QUEUE_KEY).setMaxInstancesForFactory(1).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
    }

    private StorageForcePushJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws IOException, RetryLaterException {
        if (SignalStore.account().isLinkedDevice()) {
            Log.i(TAG, "Only the primary device can force push");
        } else if (!SignalStore.account().isRegistered() || SignalStore.account().getE164() == null || Recipient.self().getStorageServiceId() == null) {
            Log.w(TAG, "User not registered. Skipping.");
        } else {
            StorageKey orCreateStorageKey = SignalStore.storageService().getOrCreateStorageKey();
            SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
            RecipientDatabase recipients = SignalDatabase.recipients();
            UnknownStorageIdDatabase unknownStorageIds = SignalDatabase.unknownStorageIds();
            long storageManifestVersion = signalServiceAccountManager.getStorageManifestVersion();
            Map<RecipientId, StorageId> contactStorageSyncIdsMap = recipients.getContactStorageSyncIdsMap();
            long j = storageManifestVersion + 1;
            Map<RecipientId, StorageId> generateContactStorageIds = generateContactStorageIds(contactStorageSyncIdsMap);
            List<SignalStorageRecord> list = Stream.of(contactStorageSyncIdsMap.keySet()).map(new StorageForcePushJob$$ExternalSyntheticLambda0(recipients)).withoutNulls().map(new Function(generateContactStorageIds) { // from class: org.thoughtcrime.securesms.jobs.StorageForcePushJob$$ExternalSyntheticLambda1
                public final /* synthetic */ Map f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return StorageForcePushJob.lambda$onRun$0(this.f$0, (RecipientRecord) obj);
                }
            }).toList();
            SignalStorageRecord buildAccountRecord = StorageSyncHelper.buildAccountRecord(this.context, Recipient.self().fresh());
            ArrayList arrayList = new ArrayList(generateContactStorageIds.values());
            list.add(buildAccountRecord);
            arrayList.add(buildAccountRecord.getId());
            SignalStorageManifest signalStorageManifest = new SignalStorageManifest(j, arrayList);
            StorageSyncValidations.validateForcePush(signalStorageManifest, list, Recipient.self().fresh());
            try {
                if (j > 1) {
                    String str = TAG;
                    Log.i(str, String.format(Locale.ENGLISH, "Force-pushing data. Inserting %d IDs.", Integer.valueOf(list.size())));
                    if (signalServiceAccountManager.resetStorageRecords(orCreateStorageKey, signalStorageManifest, list).isPresent()) {
                        Log.w(str, "Hit a conflict. Trying again.");
                        throw new RetryLaterException();
                    }
                } else {
                    String str2 = TAG;
                    Log.i(str2, String.format(Locale.ENGLISH, "First version, normal push. Inserting %d IDs.", Integer.valueOf(list.size())));
                    if (signalServiceAccountManager.writeStorageRecords(orCreateStorageKey, signalStorageManifest, list, Collections.emptyList()).isPresent()) {
                        Log.w(str2, "Hit a conflict. Trying again.");
                        throw new RetryLaterException();
                    }
                }
                String str3 = TAG;
                Log.i(str3, "Force push succeeded. Updating local manifest version to: " + j);
                SignalStore.storageService().setManifest(signalStorageManifest);
                recipients.applyStorageIdUpdates(generateContactStorageIds);
                recipients.applyStorageIdUpdates(Collections.singletonMap(Recipient.self().getId(), buildAccountRecord.getId()));
                unknownStorageIds.deleteAll();
            } catch (InvalidKeyException e) {
                Log.w(TAG, "Hit an invalid key exception, which likely indicates a conflict.");
                throw new RetryLaterException(e);
            }
        }
    }

    public static /* synthetic */ SignalStorageRecord lambda$onRun$0(Map map, RecipientRecord recipientRecord) {
        StorageId storageId = (StorageId) map.get(recipientRecord.getId());
        Objects.requireNonNull(storageId);
        return StorageSyncModels.localToRemoteRecord(recipientRecord, storageId.getRaw());
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof RetryLaterException);
    }

    private static Map<RecipientId, StorageId> generateContactStorageIds(Map<RecipientId, StorageId> map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry<RecipientId, StorageId> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), entry.getValue().withNewBytes(StorageSyncHelper.generateKey()));
        }
        return hashMap;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<StorageForcePushJob> {
        public StorageForcePushJob create(Job.Parameters parameters, Data data) {
            return new StorageForcePushJob(parameters);
        }
    }
}
