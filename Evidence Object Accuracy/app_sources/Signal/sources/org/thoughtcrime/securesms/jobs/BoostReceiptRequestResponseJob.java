package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.donations.StripeApi;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredential;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialRequestContext;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialResponse;
import org.signal.libsignal.zkgroup.receipts.ReceiptSerial;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes4.dex */
public class BoostReceiptRequestResponseJob extends BaseJob {
    private static final String BOOST_QUEUE;
    private static final String DATA_BADGE_LEVEL;
    private static final String DATA_ERROR_SOURCE;
    private static final String DATA_PAYMENT_INTENT_ID;
    private static final String DATA_REQUEST_BYTES;
    private static final String GIFT_QUEUE;
    public static final String KEY;
    private static final String TAG = Log.tag(BoostReceiptRequestResponseJob.class);
    private final long badgeLevel;
    private final DonationErrorSource donationErrorSource;
    private final String paymentIntentId;
    private ReceiptCredentialRequestContext requestContext;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private static BoostReceiptRequestResponseJob createJob(StripeApi.PaymentIntent paymentIntent, DonationErrorSource donationErrorSource, long j) {
        return new BoostReceiptRequestResponseJob(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(donationErrorSource == DonationErrorSource.BOOST ? BOOST_QUEUE : GIFT_QUEUE).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), null, paymentIntent.getId(), donationErrorSource, j);
    }

    public static JobManager.Chain createJobChainForBoost(StripeApi.PaymentIntent paymentIntent) {
        BoostReceiptRequestResponseJob createJob = createJob(paymentIntent, DonationErrorSource.BOOST, Long.parseLong(SubscriptionLevels.BOOST_LEVEL));
        DonationReceiptRedemptionJob createJobForBoost = DonationReceiptRedemptionJob.createJobForBoost();
        RefreshOwnProfileJob forBoost = RefreshOwnProfileJob.forBoost();
        return ApplicationDependencies.getJobManager().startChain(createJob).then(createJobForBoost).then(forBoost).then(new MultiDeviceProfileContentUpdateJob());
    }

    public static JobManager.Chain createJobChainForGift(StripeApi.PaymentIntent paymentIntent, RecipientId recipientId, String str, long j) {
        BoostReceiptRequestResponseJob createJob = createJob(paymentIntent, DonationErrorSource.GIFT, j);
        return ApplicationDependencies.getJobManager().startChain(createJob).then(new GiftSendJob(recipientId, str));
    }

    private BoostReceiptRequestResponseJob(Job.Parameters parameters, ReceiptCredentialRequestContext receiptCredentialRequestContext, String str, DonationErrorSource donationErrorSource, long j) {
        super(parameters);
        this.requestContext = receiptCredentialRequestContext;
        this.paymentIntentId = str;
        this.donationErrorSource = donationErrorSource;
        this.badgeLevel = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder putLong = new Data.Builder().putString(DATA_PAYMENT_INTENT_ID, this.paymentIntentId).putString("data.error.source", this.donationErrorSource.serialize()).putLong(DATA_BADGE_LEVEL, this.badgeLevel);
        ReceiptCredentialRequestContext receiptCredentialRequestContext = this.requestContext;
        if (receiptCredentialRequestContext != null) {
            putLong.putBlobAsString(DATA_REQUEST_BYTES, receiptCredentialRequestContext.serialize());
        }
        return putLong.build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (this.requestContext == null) {
            Log.d(TAG, "Creating request context..");
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr = new byte[16];
            secureRandom.nextBytes(bArr);
            this.requestContext = ApplicationDependencies.getClientZkReceiptOperations().createReceiptCredentialRequestContext(secureRandom, new ReceiptSerial(bArr));
        } else {
            Log.d(TAG, "Reusing request context from previous run", true);
        }
        String str = TAG;
        Log.d(str, "Submitting credential to server", true);
        ServiceResponse<ReceiptCredentialResponse> blockingGet = ApplicationDependencies.getDonationsService().submitBoostReceiptCredentialRequest(this.paymentIntentId, this.requestContext.getRequest()).blockingGet();
        if (blockingGet.getApplicationError().isPresent()) {
            handleApplicationError(this.context, blockingGet, this.donationErrorSource);
        } else if (blockingGet.getResult().isPresent()) {
            ReceiptCredential receiptCredential = getReceiptCredential(blockingGet.getResult().get());
            if (isCredentialValid(receiptCredential)) {
                Log.d(str, "Validated credential. Handing off to next job.", true);
                setOutputData(new Data.Builder().putBlobAsString(DonationReceiptRedemptionJob.INPUT_RECEIPT_CREDENTIAL_PRESENTATION, getReceiptCredentialPresentation(receiptCredential).serialize()).build());
                return;
            }
            DonationError.routeDonationError(this.context, DonationError.badgeCredentialVerificationFailure(this.donationErrorSource));
            throw new IOException("Could not validate receipt credential");
        } else {
            Log.w(str, "Encountered a retryable exception: " + blockingGet.getStatus(), blockingGet.getExecutionError().orElse(null), true);
            throw new RetryableException();
        }
    }

    private static void handleApplicationError(Context context, ServiceResponse<ReceiptCredentialResponse> serviceResponse, DonationErrorSource donationErrorSource) throws Exception {
        Throwable th = serviceResponse.getApplicationError().get();
        int status = serviceResponse.getStatus();
        if (status == 204) {
            Log.w(TAG, "User payment not be completed yet.", th, true);
            throw new RetryableException();
        } else if (status == 400) {
            Log.w(TAG, "Receipt credential request failed to validate.", th, true);
            DonationError.routeDonationError(context, DonationError.genericBadgeRedemptionFailure(donationErrorSource));
            throw new Exception(th);
        } else if (status == 402) {
            Log.w(TAG, "User payment failed.", th, true);
            DonationError.routeDonationError(context, DonationError.genericPaymentFailure(donationErrorSource));
            throw new Exception(th);
        } else if (status != 409) {
            String str = TAG;
            Log.w(str, "Encountered a server failure: " + serviceResponse.getStatus(), th, true);
            throw new RetryableException();
        } else {
            Log.w(TAG, "Receipt already redeemed with a different request credential.", serviceResponse.getApplicationError().get(), true);
            DonationError.routeDonationError(context, DonationError.genericBadgeRedemptionFailure(donationErrorSource));
            throw new Exception(th);
        }
    }

    private ReceiptCredentialPresentation getReceiptCredentialPresentation(ReceiptCredential receiptCredential) throws RetryableException {
        try {
            return ApplicationDependencies.getClientZkReceiptOperations().createReceiptCredentialPresentation(receiptCredential);
        } catch (VerificationFailedException e) {
            Log.w(TAG, "getReceiptCredentialPresentation: encountered a verification failure in zk", e, true);
            this.requestContext = null;
            throw new RetryableException();
        }
    }

    private ReceiptCredential getReceiptCredential(ReceiptCredentialResponse receiptCredentialResponse) throws RetryableException {
        try {
            return ApplicationDependencies.getClientZkReceiptOperations().receiveReceiptCredential(this.requestContext, receiptCredentialResponse);
        } catch (VerificationFailedException e) {
            Log.w(TAG, "getReceiptCredential: encountered a verification failure in zk", e, true);
            this.requestContext = null;
            throw new RetryableException();
        }
    }

    private boolean isCredentialValid(ReceiptCredential receiptCredential) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        long seconds2 = TimeUnit.DAYS.toSeconds(90) + seconds;
        boolean z = receiptCredential.getReceiptLevel() == this.badgeLevel;
        boolean z2 = receiptCredential.getReceiptExpirationTime() % 86400 == 0;
        boolean z3 = receiptCredential.getReceiptExpirationTime() > seconds;
        boolean z4 = receiptCredential.getReceiptExpirationTime() <= seconds2;
        String str = TAG;
        Log.d(str, "Credential validation: isCorrectLevel(" + z + " actual: " + receiptCredential.getReceiptLevel() + ", expected: " + this.badgeLevel + ") isExpiration86400(" + z2 + ") isExpirationInTheFuture(" + z3 + ") isExpirationWithinMax(" + z4 + ")", true);
        if (!z || !z2 || !z3 || !z4) {
            return false;
        }
        return true;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryableException;
    }

    /* loaded from: classes4.dex */
    public static final class RetryableException extends Exception {
        RetryableException() {
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<BoostReceiptRequestResponseJob> {
        public BoostReceiptRequestResponseJob create(Job.Parameters parameters, Data data) {
            String string = data.getString(BoostReceiptRequestResponseJob.DATA_PAYMENT_INTENT_ID);
            DonationErrorSource deserialize = DonationErrorSource.deserialize(data.getStringOrDefault("data.error.source", DonationErrorSource.BOOST.serialize()));
            long longOrDefault = data.getLongOrDefault(BoostReceiptRequestResponseJob.DATA_BADGE_LEVEL, Long.parseLong(SubscriptionLevels.BOOST_LEVEL));
            try {
                if (data.hasString(BoostReceiptRequestResponseJob.DATA_REQUEST_BYTES)) {
                    return new BoostReceiptRequestResponseJob(parameters, new ReceiptCredentialRequestContext(data.getStringAsBlob(BoostReceiptRequestResponseJob.DATA_REQUEST_BYTES)), string, deserialize, longOrDefault);
                }
                return new BoostReceiptRequestResponseJob(parameters, null, string, deserialize, longOrDefault);
            } catch (InvalidInputException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
