package org.thoughtcrime.securesms.jobs;

import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes4.dex */
public final class SenderKeyDistributionSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_ID;
    private static final String KEY_RECIPIENT_ID;
    private static final String TAG = Log.tag(SenderKeyDistributionSendJob.class);
    private final GroupId.V2 groupId;
    private final RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public SenderKeyDistributionSendJob(RecipientId recipientId, GroupId.V2 v2) {
        this(recipientId, v2, new Job.Parameters.Builder().setQueue(recipientId.toQueueKey()).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).setMaxInstancesForQueue(1).build());
    }

    private SenderKeyDistributionSendJob(RecipientId recipientId, GroupId.V2 v2, Job.Parameters parameters) {
        super(parameters);
        this.recipientId = recipientId;
        this.groupId = v2;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient_id", this.recipientId.serialize()).putBlobAsString("group_id", this.groupId.getDecodedId()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        GroupDatabase groups = SignalDatabase.groups();
        if (!groups.isCurrentMember(this.groupId, this.recipientId)) {
            String str = TAG;
            Log.w(str, this.recipientId + " is no longer a member of " + this.groupId + "! Not sending.");
            return;
        }
        Recipient resolved = Recipient.resolved(this.recipientId);
        if (resolved.getSenderKeyCapability() != Recipient.Capability.SUPPORTED) {
            String str2 = TAG;
            Log.w(str2, this.recipientId + " does not support sender key! Not sending.");
        } else if (resolved.isUnregistered()) {
            String str3 = TAG;
            Log.w(str3, resolved.getId() + " not registered!");
        } else {
            SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
            List<SignalServiceAddress> singletonList = Collections.singletonList(RecipientUtil.toSignalServiceAddress(this.context, resolved));
            DistributionId orCreateDistributionId = groups.getOrCreateDistributionId(this.groupId);
            SendMessageResult sendMessageResult = signalServiceMessageSender.sendSenderKeyDistributionMessage(orCreateDistributionId, singletonList, UnidentifiedAccessUtil.getAccessFor(this.context, Collections.singletonList(resolved)), signalServiceMessageSender.getOrCreateNewGroupSession(orCreateDistributionId), Optional.of(this.groupId.getDecodedId())).get(0);
            if (sendMessageResult.isSuccess()) {
                ApplicationDependencies.getProtocolStore().aci().markSenderKeySharedWith(orCreateDistributionId, (List) Collection$EL.stream(sendMessageResult.getSuccess().getDevices()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.SenderKeyDistributionSendJob$$ExternalSyntheticLambda0
                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return SenderKeyDistributionSendJob.lambda$onRun$0(Recipient.this, (Integer) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }).collect(Collectors.toList()));
            }
        }
    }

    public static /* synthetic */ SignalProtocolAddress lambda$onRun$0(Recipient recipient, Integer num) {
        return recipient.requireServiceId().toProtocolAddress(num.intValue());
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<SenderKeyDistributionSendJob> {
        public SenderKeyDistributionSendJob create(Job.Parameters parameters, Data data) {
            return new SenderKeyDistributionSendJob(RecipientId.from(data.getString("recipient_id")), GroupId.pushOrThrow(data.getStringAsBlob("group_id")).requireV2(), parameters);
        }
    }
}
