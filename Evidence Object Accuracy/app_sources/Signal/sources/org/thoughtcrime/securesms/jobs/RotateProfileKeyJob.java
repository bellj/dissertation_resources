package org.thoughtcrime.securesms.jobs;

import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class RotateProfileKeyJob extends BaseJob {
    public static String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public RotateProfileKeyJob() {
        this(new Job.Parameters.Builder().setQueue("__ROTATE_PROFILE_KEY__").setMaxInstancesForFactory(2).build());
    }

    private RotateProfileKeyJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        SignalDatabase.recipients().setProfileKey(Recipient.self().getId(), ProfileKeyUtil.createNew());
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RotateProfileKeyJob> {
        public RotateProfileKeyJob create(Job.Parameters parameters, Data data) {
            return new RotateProfileKeyJob(parameters);
        }
    }
}
