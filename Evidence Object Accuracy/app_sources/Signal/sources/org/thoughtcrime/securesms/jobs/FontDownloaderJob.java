package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.graphics.Typeface;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.fonts.Fonts;
import org.thoughtcrime.securesms.fonts.SupportedScript;
import org.thoughtcrime.securesms.fonts.TextFont;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.util.FutureTaskListener;
import org.thoughtcrime.securesms.util.LocaleUtil;

/* compiled from: FontDownloaderJob.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0014J\u0014\u0010\u000b\u001a\u00020\f2\n\u0010\r\u001a\u00060\u000ej\u0002`\u000fH\u0014J\b\u0010\u0010\u001a\u00020\u0011H\u0016¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/FontDownloaderJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "()V", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class FontDownloaderJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String TAG = Log.tag(FontDownloaderJob.class);

    public /* synthetic */ FontDownloaderJob(Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return true;
    }

    private FontDownloaderJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* compiled from: FontDownloaderJob.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/FontDownloaderJob$Companion;", "", "()V", "KEY", "", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FontDownloaderJob() {
        /*
            r4 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.String r1 = "NetworkConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS
            r2 = 30
            long r1 = r1.toMillis(r2)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setLifespan(r1)
            r1 = -1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxAttempts(r1)
            r1 = 1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxInstancesForFactory(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder()\n      .addCons…Factory(1)\n      .build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r4.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.FontDownloaderJob.<init>():void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data data = Data.EMPTY;
        Intrinsics.checkNotNullExpressionValue(data, "EMPTY");
        return data;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        SupportedScript supportedScript = Fonts.INSTANCE.getSupportedScript(LocaleUtil.INSTANCE.getLocaleDefaults(), SupportedScript.UNKNOWN);
        TextFont[] values = TextFont.values();
        ArrayList arrayList = new ArrayList(values.length);
        for (TextFont textFont : values) {
            Fonts fonts = Fonts.INSTANCE;
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            arrayList.add(fonts.resolveFont(context, textFont, supportedScript));
        }
        List<Fonts.FontResult.Async> list = CollectionsKt___CollectionsJvmKt.filterIsInstance(arrayList, Fonts.FontResult.Async.class);
        if (list.isEmpty()) {
            Log.i(TAG, "Already downloaded fonts for locale.");
            return;
        }
        CountDownLatch countDownLatch = new CountDownLatch(list.size());
        AtomicInteger atomicInteger = new AtomicInteger(0);
        FontDownloaderJob$onRun$listener$1 fontDownloaderJob$onRun$listener$1 = new FutureTaskListener<Typeface>(countDownLatch, atomicInteger) { // from class: org.thoughtcrime.securesms.jobs.FontDownloaderJob$onRun$listener$1
            final /* synthetic */ CountDownLatch $countDownLatch;
            final /* synthetic */ AtomicInteger $failure;

            /* access modifiers changed from: package-private */
            {
                this.$countDownLatch = r1;
                this.$failure = r2;
            }

            public void onSuccess(Typeface typeface) {
                this.$countDownLatch.countDown();
            }

            @Override // org.thoughtcrime.securesms.util.FutureTaskListener
            public void onFailure(ExecutionException executionException) {
                this.$failure.getAndIncrement();
                this.$countDownLatch.countDown();
            }
        };
        for (Fonts.FontResult.Async async : list) {
            async.getFuture().addListener(fontDownloaderJob$onRun$listener$1);
        }
        countDownLatch.await();
        if (atomicInteger.get() > 0) {
            throw new Exception("Failed to download " + atomicInteger.get() + " fonts. Scheduling a retry.");
        }
    }

    /* compiled from: FontDownloaderJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/FontDownloaderJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/FontDownloaderJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<FontDownloaderJob> {
        public FontDownloaderJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new FontDownloaderJob(parameters, null);
        }
    }
}
