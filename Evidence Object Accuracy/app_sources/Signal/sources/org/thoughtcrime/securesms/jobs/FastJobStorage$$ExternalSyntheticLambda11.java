package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import java.util.List;
import java.util.Map;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class FastJobStorage$$ExternalSyntheticLambda11 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return (List) ((Map.Entry) obj).getValue();
    }
}
