package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class FastJobStorage$$ExternalSyntheticLambda12 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return Stream.of((List) obj);
    }
}
