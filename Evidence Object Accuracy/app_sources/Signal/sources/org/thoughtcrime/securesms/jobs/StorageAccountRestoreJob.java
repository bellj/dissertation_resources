package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.storage.StorageKey;

/* loaded from: classes4.dex */
public class StorageAccountRestoreJob extends BaseJob {
    public static String KEY;
    public static long LIFESPAN = TimeUnit.SECONDS.toMillis(20);
    private static final String TAG = Log.tag(StorageAccountRestoreJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public StorageAccountRestoreJob() {
        this(new Job.Parameters.Builder().setQueue(StorageSyncJob.QUEUE_KEY).addConstraint(NetworkConstraint.KEY).setMaxInstancesForFactory(1).setMaxAttempts(1).setLifespan(LIFESPAN).build());
    }

    private StorageAccountRestoreJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
        StorageKey orCreateStorageKey = SignalStore.storageService().getOrCreateStorageKey();
        String str = TAG;
        Log.i(str, "Retrieving manifest...");
        Optional<SignalStorageManifest> storageManifest = signalServiceAccountManager.getStorageManifest(orCreateStorageKey);
        if (!storageManifest.isPresent()) {
            Log.w(str, "Manifest did not exist or was undecryptable (bad key). Not restoring. Force-pushing.");
            ApplicationDependencies.getJobManager().add(new StorageForcePushJob());
            return;
        }
        Log.i(str, "Resetting the local manifest to an empty state so that it will sync later.");
        SignalStore.storageService().setManifest(SignalStorageManifest.EMPTY);
        Optional<StorageId> accountStorageId = storageManifest.get().getAccountStorageId();
        if (!accountStorageId.isPresent()) {
            Log.w(str, "Manifest had no account record! Not restoring.");
            return;
        }
        Log.i(str, "Retrieving account record...");
        List<SignalStorageRecord> readStorageRecords = signalServiceAccountManager.readStorageRecords(orCreateStorageKey, Collections.singletonList(accountStorageId.get()));
        SignalStorageRecord signalStorageRecord = readStorageRecords.size() > 0 ? readStorageRecords.get(0) : null;
        if (signalStorageRecord == null) {
            Log.w(str, "Could not find account record, even though we had an ID! Not restoring.");
            return;
        }
        SignalAccountRecord orElse = signalStorageRecord.getAccount().orElse(null);
        if (orElse == null) {
            Log.w(str, "The storage record didn't actually have an account on it! Not restoring.");
            return;
        }
        Log.i(str, "Applying changes locally...");
        SignalDatabase.getRawDatabase().beginTransaction();
        try {
            StorageSyncHelper.applyAccountStorageSyncUpdates(this.context, Recipient.self(), orElse, false);
            SignalDatabase.getRawDatabase().setTransactionSuccessful();
            SignalDatabase.getRawDatabase().endTransaction();
            JobManager jobManager = ApplicationDependencies.getJobManager();
            if (orElse.getAvatarUrlPath().isPresent()) {
                Log.i(str, "Fetching avatar...");
                Optional<JobTracker.JobState> runSynchronously = jobManager.runSynchronously(new RetrieveProfileAvatarJob(Recipient.self(), orElse.getAvatarUrlPath().get()), LIFESPAN / 2);
                if (runSynchronously.isPresent()) {
                    Log.i(str, "Avatar retrieved successfully. " + runSynchronously.get());
                } else {
                    Log.w(str, "Avatar retrieval did not complete in time (or otherwise failed).");
                }
            } else {
                Log.i(str, "No avatar present. Not fetching.");
            }
            Log.i(str, "Refreshing attributes...");
            Optional<JobTracker.JobState> runSynchronously2 = jobManager.runSynchronously(new RefreshAttributesJob(), LIFESPAN / 2);
            if (runSynchronously2.isPresent()) {
                Log.i(str, "Attributes refreshed successfully. " + runSynchronously2.get());
                return;
            }
            Log.w(str, "Attribute refresh did not complete in time (or otherwise failed).");
        } catch (Throwable th) {
            SignalDatabase.getRawDatabase().endTransaction();
            throw th;
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<StorageAccountRestoreJob> {
        public StorageAccountRestoreJob create(Job.Parameters parameters, Data data) {
            return new StorageAccountRestoreJob(parameters);
        }
    }
}
