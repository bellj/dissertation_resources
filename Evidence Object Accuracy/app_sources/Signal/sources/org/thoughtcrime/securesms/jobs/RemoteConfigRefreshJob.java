package org.thoughtcrime.securesms.jobs;

import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class RemoteConfigRefreshJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(RemoteConfigRefreshJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public RemoteConfigRefreshJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).setMaxInstancesForFactory(1).addConstraint(NetworkConstraint.KEY).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
    }

    private RemoteConfigRefreshJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (!SignalStore.account().isRegistered()) {
            Log.w(TAG, "Not registered. Skipping.");
        } else {
            FeatureFlags.update(ApplicationDependencies.getSignalServiceAccountManager().getRemoteConfig());
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RemoteConfigRefreshJob> {
        public RemoteConfigRefreshJob create(Job.Parameters parameters, Data data) {
            return new RemoteConfigRefreshJob(parameters);
        }
    }
}
