package org.thoughtcrime.securesms.jobs;

import android.text.TextUtils;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.AppCapabilities;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.KbsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.secondary.DeviceNameCipher;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.account.AccountAttributes;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.push.exceptions.NetworkFailureException;

/* loaded from: classes4.dex */
public class RefreshAttributesJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_FORCED;
    private static final String TAG = Log.tag(RefreshAttributesJob.class);
    private static volatile boolean hasRefreshedThisAppCycle;
    private final boolean forced;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public RefreshAttributesJob() {
        this(true);
    }

    public RefreshAttributesJob(boolean z) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(KEY).setMaxInstancesForFactory(2).build(), z);
    }

    private RefreshAttributesJob(Job.Parameters parameters, boolean z) {
        super(parameters);
        this.forced = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putBoolean(KEY_FORCED, this.forced).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        String str;
        String str2;
        byte[] bArr;
        if (!SignalStore.account().isRegistered() || SignalStore.account().getE164() == null) {
            Log.w(TAG, "Not yet registered. Skipping.");
        } else if (this.forced || !hasRefreshedThisAppCycle) {
            int registrationId = SignalStore.account().getRegistrationId();
            boolean z = !SignalStore.account().isFcmEnabled();
            byte[] deriveAccessKeyFrom = UnidentifiedAccess.deriveAccessKeyFrom(ProfileKeyUtil.getSelfProfileKey());
            boolean isUniversalUnidentifiedAccess = TextSecurePreferences.isUniversalUnidentifiedAccess(this.context);
            KbsValues kbsValues = SignalStore.kbsValues();
            if (kbsValues.isV2RegistrationLockEnabled()) {
                str = kbsValues.getRegistrationLockToken();
                str2 = null;
            } else if (TextSecurePreferences.isV1RegistrationLockEnabled(this.context)) {
                str2 = TextSecurePreferences.getDeprecatedV1RegistrationLockPin(this.context);
                str = null;
            } else {
                str2 = null;
                str = null;
            }
            boolean isDiscoverable = SignalStore.phoneNumberPrivacy().getPhoneNumberListingMode().isDiscoverable();
            String deviceName = SignalStore.account().getDeviceName();
            if (deviceName == null) {
                bArr = null;
            } else {
                bArr = DeviceNameCipher.encryptDeviceName(deviceName.getBytes(StandardCharsets.UTF_8), SignalStore.account().getAciIdentityKey());
            }
            boolean z2 = false;
            AccountAttributes.Capabilities capabilities = AppCapabilities.getCapabilities(kbsValues.hasPin() && !kbsValues.hasOptedOut());
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Calling setAccountAttributes() reglockV1? ");
            sb.append(!TextUtils.isEmpty(str2));
            sb.append(", reglockV2? ");
            sb.append(!TextUtils.isEmpty(str));
            sb.append(", pin? ");
            sb.append(kbsValues.hasPin());
            sb.append("\n    Phone number discoverable : ");
            sb.append(isDiscoverable);
            sb.append("\n    Device Name : ");
            if (bArr != null) {
                z2 = true;
            }
            sb.append(z2);
            sb.append("\n  Capabilities:\n    Storage? ");
            sb.append(capabilities.isStorage());
            sb.append("\n    GV2? ");
            sb.append(capabilities.isGv2());
            sb.append("\n    GV1 Migration? ");
            sb.append(capabilities.isGv1Migration());
            sb.append("\n    Sender Key? ");
            sb.append(capabilities.isSenderKey());
            sb.append("\n    Announcement Groups? ");
            sb.append(capabilities.isAnnouncementGroup());
            sb.append("\n    Change Number? ");
            sb.append(capabilities.isChangeNumber());
            sb.append("\n    Stories? ");
            sb.append(capabilities.isStories());
            sb.append("\n    Gift Badges? ");
            sb.append(capabilities.isGiftBadges());
            sb.append("\n    UUID? ");
            sb.append(capabilities.isUuid());
            Log.i(str3, sb.toString());
            ApplicationDependencies.getSignalServiceAccountManager().setAccountAttributes(null, registrationId, z, str2, str, deriveAccessKeyFrom, isUniversalUnidentifiedAccess, capabilities, isDiscoverable, bArr);
            hasRefreshedThisAppCycle = true;
        } else {
            Log.d(TAG, "Already refreshed this app cycle. Skipping.");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof NetworkFailureException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to update account attributes!");
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<RefreshAttributesJob> {
        public RefreshAttributesJob create(Job.Parameters parameters, Data data) {
            return new RefreshAttributesJob(parameters, data.getBooleanOrDefault(RefreshAttributesJob.KEY_FORCED, true));
        }
    }
}
