package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public class AttachmentCopyJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_DESTINATION_IDS;
    private static final String KEY_SOURCE_ID;
    private final List<AttachmentId> destinationIds;
    private final AttachmentId sourceId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return true;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public AttachmentCopyJob(AttachmentId attachmentId, List<AttachmentId> list) {
        this(new Job.Parameters.Builder().setQueue(KEY).setMaxAttempts(3).build(), attachmentId, list);
    }

    private AttachmentCopyJob(Job.Parameters parameters, AttachmentId attachmentId, List<AttachmentId> list) {
        super(parameters);
        this.sourceId = attachmentId;
        this.destinationIds = list;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        try {
            String json = JsonUtils.toJson(this.sourceId);
            String[] strArr = new String[this.destinationIds.size()];
            for (int i = 0; i < this.destinationIds.size(); i++) {
                strArr[i] = JsonUtils.toJson(this.destinationIds.get(i));
            }
            return new Data.Builder().putString(KEY_SOURCE_ID, json).putStringArray(KEY_DESTINATION_IDS, strArr).build();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        AttachmentDatabase attachments = SignalDatabase.attachments();
        for (AttachmentId attachmentId : this.destinationIds) {
            attachments.copyAttachmentData(this.sourceId, attachmentId);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AttachmentCopyJob> {
        public AttachmentCopyJob create(Job.Parameters parameters, Data data) {
            try {
                String string = data.getString(AttachmentCopyJob.KEY_SOURCE_ID);
                String[] stringArray = data.getStringArray(AttachmentCopyJob.KEY_DESTINATION_IDS);
                AttachmentId attachmentId = (AttachmentId) JsonUtils.fromJson(string, AttachmentId.class);
                ArrayList arrayList = new ArrayList(stringArray.length);
                for (String str : stringArray) {
                    arrayList.add((AttachmentId) JsonUtils.fromJson(str, AttachmentId.class));
                }
                return new AttachmentCopyJob(parameters, attachmentId, arrayList);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}
