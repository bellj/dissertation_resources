package org.thoughtcrime.securesms.jobs;

import io.reactivex.rxjava3.functions.Predicate;
import j$.util.Optional;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;
import org.whispersystems.signalservice.internal.util.StaticCredentialsProvider;
import org.whispersystems.signalservice.internal.websocket.WebSocketConnection;

/* compiled from: CheckServiceReachabilityJob.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0014J\u0014\u0010\u000b\u001a\u00020\f2\n\u0010\r\u001a\u00060\u000ej\u0002`\u000fH\u0014J\b\u0010\u0010\u001a\u00020\u0011H\u0016¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/CheckServiceReachabilityJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "()V", "params", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CheckServiceReachabilityJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String TAG = Log.tag(CheckServiceReachabilityJob.class);

    public /* synthetic */ CheckServiceReachabilityJob(Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters);
    }

    @JvmStatic
    public static final void enqueueIfNecessary() {
        Companion.enqueueIfNecessary();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return false;
    }

    private CheckServiceReachabilityJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CheckServiceReachabilityJob() {
        /*
            r4 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.String r1 = "NetworkConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.HOURS
            r2 = 12
            long r1 = r1.toMillis(r2)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setLifespan(r1)
            r1 = 1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxAttempts(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder()\n      .addCons…ttempts(1)\n      .build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r4.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.CheckServiceReachabilityJob.<init>():void");
    }

    /* compiled from: CheckServiceReachabilityJob.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/CheckServiceReachabilityJob$Companion;", "", "()V", "KEY", "", "TAG", "kotlin.jvm.PlatformType", "enqueueIfNecessary", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void enqueueIfNecessary() {
            boolean isCensored = ApplicationDependencies.getSignalServiceNetworkAccess().isCensored();
            long currentTimeMillis = System.currentTimeMillis() - SignalStore.misc().getLastCensorshipServiceReachabilityCheckTime();
            if (SignalStore.account().isRegistered() && isCensored && currentTimeMillis > TimeUnit.DAYS.toMillis(1)) {
                ApplicationDependencies.getJobManager().add(new CheckServiceReachabilityJob());
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data data = Data.EMPTY;
        Intrinsics.checkNotNullExpressionValue(data, "EMPTY");
        return data;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        if (!SignalStore.account().isRegistered()) {
            Log.w(TAG, "Not registered, skipping.");
            SignalStore.misc().setLastCensorshipServiceReachabilityCheckTime(System.currentTimeMillis());
        } else if (!ApplicationDependencies.getSignalServiceNetworkAccess().isCensored()) {
            Log.w(TAG, "Not currently censored, skipping.");
            SignalStore.misc().setLastCensorshipServiceReachabilityCheckTime(System.currentTimeMillis());
        } else {
            SignalStore.misc().setLastCensorshipServiceReachabilityCheckTime(System.currentTimeMillis());
            WebSocketConnection webSocketConnection = new WebSocketConnection("uncensored-test", ApplicationDependencies.getSignalServiceNetworkAccess().getUncensoredConfiguration(), Optional.of(new StaticCredentialsProvider(SignalStore.account().getAci(), SignalStore.account().getPni(), SignalStore.account().getE164(), SignalStore.account().getDeviceId(), SignalStore.account().getServicePassword())), BuildConfig.SIGNAL_AGENT, null, "");
            try {
                try {
                    long currentTimeMillis = System.currentTimeMillis();
                    WebSocketConnectionState blockingFirst = webSocketConnection.connect().filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.CheckServiceReachabilityJob$$ExternalSyntheticLambda0
                        @Override // io.reactivex.rxjava3.functions.Predicate
                        public final boolean test(Object obj) {
                            return CheckServiceReachabilityJob.m1903$r8$lambda$kFBDXZat5C9TR3oFQRHNy0LyE((WebSocketConnectionState) obj);
                        }
                    }).timeout(30, TimeUnit.SECONDS).blockingFirst(WebSocketConnectionState.FAILED);
                    Intrinsics.checkNotNullExpressionValue(blockingFirst, "uncensoredWebsocket.conn…etConnectionState.FAILED)");
                    if (blockingFirst == WebSocketConnectionState.CONNECTED) {
                        String str = TAG;
                        Log.i(str, "Established connection in " + (System.currentTimeMillis() - currentTimeMillis) + " ms! Service is reachable!");
                        SignalStore.misc().setServiceReachableWithoutCircumvention(true);
                    } else {
                        String str2 = TAG;
                        Log.w(str2, "Failed to establish a connection in " + (System.currentTimeMillis() - currentTimeMillis) + " ms.");
                        SignalStore.misc().setServiceReachableWithoutCircumvention(false);
                    }
                } catch (Exception e) {
                    Log.w(TAG, "Failed to connect to the websocket.", e);
                    SignalStore.misc().setServiceReachableWithoutCircumvention(false);
                }
            } finally {
                webSocketConnection.disconnect();
            }
        }
    }

    /* renamed from: onRun$lambda-0 */
    public static final boolean m1904onRun$lambda0(WebSocketConnectionState webSocketConnectionState) {
        return webSocketConnectionState == WebSocketConnectionState.CONNECTED || webSocketConnectionState == WebSocketConnectionState.FAILED;
    }

    /* compiled from: CheckServiceReachabilityJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/CheckServiceReachabilityJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/CheckServiceReachabilityJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<CheckServiceReachabilityJob> {
        public CheckServiceReachabilityJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new CheckServiceReachabilityJob(parameters, null);
        }
    }
}
