package org.thoughtcrime.securesms.jobs;

import androidx.core.os.LocaleListCompat;
import com.fasterxml.jackson.core.JsonParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.IntIterator;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt___RangesKt;
import org.json.JSONArray;
import org.json.JSONObject;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.releasechannel.ReleaseChannel;
import org.thoughtcrime.securesms.s3.S3;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* compiled from: StoryOnboardingDownloadJob.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00132\u00020\u0001:\u0002\u0013\u0014B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bH\u0002J\b\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\nH\u0014J\u0014\u0010\f\u001a\u00020\r2\n\u0010\u000e\u001a\u00060\u000fj\u0002`\u0010H\u0014J\b\u0010\u0011\u001a\u00020\u0012H\u0016¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/StoryOnboardingDownloadJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "getFactoryKey", "", "getLocaleCodes", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class StoryOnboardingDownloadJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String ONBOARDING_EXTENSION;
    private static final int ONBOARDING_IMAGE_COUNT;
    private static final int ONBOARDING_IMAGE_HEIGHT;
    private static final String ONBOARDING_IMAGE_PATH;
    private static final int ONBOARDING_IMAGE_WIDTH;
    private static final String ONBOARDING_MANIFEST_ENDPOINT;
    private static final String TAG = Log.tag(StoryOnboardingDownloadJob.class);

    public /* synthetic */ StoryOnboardingDownloadJob(Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    private StoryOnboardingDownloadJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* compiled from: StoryOnboardingDownloadJob.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0002J\u0006\u0010\u0010\u001a\u00020\u0011R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n \r*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/StoryOnboardingDownloadJob$Companion;", "", "()V", "KEY", "", "ONBOARDING_EXTENSION", "ONBOARDING_IMAGE_COUNT", "", "ONBOARDING_IMAGE_HEIGHT", "ONBOARDING_IMAGE_PATH", "ONBOARDING_IMAGE_WIDTH", "ONBOARDING_MANIFEST_ENDPOINT", "TAG", "kotlin.jvm.PlatformType", "create", "Lorg/thoughtcrime/securesms/jobmanager/Job;", "enqueueIfNeeded", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        private final Job create() {
            Job.Parameters build = new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(StoryOnboardingDownloadJob.KEY).setMaxInstancesForFactory(1).setMaxAttempts(3).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder()\n          .add…pts(3)\n          .build()");
            return new StoryOnboardingDownloadJob(build, null);
        }

        public final void enqueueIfNeeded() {
            if (!SignalStore.storyValues().getHasDownloadedOnboardingStory() && Stories.isFeatureAvailable()) {
                Log.d(StoryOnboardingDownloadJob.TAG, "Attempting to enqueue StoryOnboardingDownloadJob...");
                ApplicationDependencies.getJobManager().startChain(CreateReleaseChannelJob.Companion.create()).then(create()).enqueue();
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data data = Data.EMPTY;
        Intrinsics.checkNotNullExpressionValue(data, "EMPTY");
        return data;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:79:0x00a9 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v8, resolved type: org.json.JSONArray */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v5, types: [java.lang.Throwable] */
    /* JADX WARN: Type inference failed for: r2v27, types: [org.json.JSONArray] */
    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        JSONArray jSONArray;
        if (SignalStore.storyValues().getHasDownloadedOnboardingStory()) {
            Log.i(TAG, "Already downloaded onboarding story. Exiting.");
            return;
        }
        RecipientId releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId();
        if (releaseChannelRecipientId != null) {
            MessageDatabase.Reader<MessageRecord> allStoriesFor = SignalDatabase.Companion.mms().getAllStoriesFor(releaseChannelRecipientId, -1);
            try {
                Intrinsics.checkNotNullExpressionValue(allStoriesFor, "reader");
                for (MessageRecord messageRecord : allStoriesFor) {
                    SignalDatabase.Companion.mms().deleteMessage(messageRecord.getId());
                }
                Unit unit = Unit.INSTANCE;
                th = 0;
                try {
                    JSONObject jSONObject = new JSONObject(S3.getString(ONBOARDING_MANIFEST_ENDPOINT));
                    if (!jSONObject.has("languages")) {
                        Log.w(TAG, "Could not find languages set in manifest.");
                        throw new Exception("Could not find languages set in manifest.");
                    } else if (jSONObject.has("version")) {
                        String string = jSONObject.getString("version");
                        String str = TAG;
                        Log.i(str, "Using images for manifest version " + string);
                        JSONObject jSONObject2 = jSONObject.getJSONObject("languages");
                        List<String> localeCodes = getLocaleCodes();
                        Iterator<String> it = localeCodes.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            String next = it.next();
                            if (jSONObject2.has(next)) {
                                jSONArray = jSONObject2.getJSONArray(next);
                                break;
                            }
                        }
                        if (jSONArray != 0) {
                            long orCreateThreadIdFor = SignalDatabase.Companion.threads().getOrCreateThreadIdFor(Recipient.resolved(releaseChannelRecipientId));
                            Log.i(TAG, "Inserting messages...");
                            IntRange intRange = RangesKt___RangesKt.until(0, jSONArray.length());
                            ArrayList<MessageDatabase.InsertResult> arrayList = new ArrayList();
                            Iterator<Integer> it2 = intRange.iterator();
                            while (it2.hasNext()) {
                                int nextInt = ((IntIterator) it2).nextInt();
                                ReleaseChannel releaseChannel = ReleaseChannel.INSTANCE;
                                MessageDatabase.InsertResult insertReleaseChannelMessage$default = ReleaseChannel.insertReleaseChannelMessage$default(releaseChannel, releaseChannelRecipientId, "", orCreateThreadIdFor, "/static/android/stories/onboarding/" + string + '/' + jSONArray.getString(nextInt) + ONBOARDING_EXTENSION, ONBOARDING_IMAGE_WIDTH, ONBOARDING_IMAGE_HEIGHT, null, null, StoryType.STORY_WITHOUT_REPLIES, 192, null);
                                Thread.sleep(5);
                                if (insertReleaseChannelMessage$default != null) {
                                    arrayList.add(insertReleaseChannelMessage$default);
                                }
                                arrayList = arrayList;
                                releaseChannelRecipientId = releaseChannelRecipientId;
                            }
                            if (arrayList.size() != 5) {
                                Log.w(TAG, "Failed to insert some search results. Deleting the ones we added and trying again later.");
                                for (MessageDatabase.InsertResult insertResult : arrayList) {
                                    SignalDatabase.Companion.mms().deleteMessage(insertResult.getMessageId());
                                }
                                throw new RetryLaterException();
                            }
                            String str2 = TAG;
                            Log.d(str2, "Marking onboarding story downloaded.");
                            SignalStore.storyValues().setHasDownloadedOnboardingStory(true);
                            Log.i(str2, "Enqueueing download jobs...");
                            for (MessageDatabase.InsertResult insertResult2 : arrayList) {
                                List<DatabaseAttachment> attachmentsForMessage = SignalDatabase.Companion.attachments().getAttachmentsForMessage(insertResult2.getMessageId());
                                Intrinsics.checkNotNullExpressionValue(attachmentsForMessage, "SignalDatabase.attachmen…e(insertResult.messageId)");
                                for (DatabaseAttachment databaseAttachment : attachmentsForMessage) {
                                    ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(insertResult2.getMessageId(), databaseAttachment.getAttachmentId(), true));
                                }
                            }
                            return;
                        }
                        String str3 = TAG;
                        Log.w(str3, "Could not find a candidate for the language set: " + localeCodes);
                        throw new Exception("Failed to locate onboarding image set.");
                    } else {
                        Log.w(TAG, "Could not find version in manifest");
                        throw new Exception("Could not find version in manifest.");
                    }
                } catch (JsonParseException e) {
                    Log.w(TAG, "Returned data could not be parsed into JSON", e);
                    throw e;
                } catch (NonSuccessfulResponseCodeException e2) {
                    Log.w(TAG, "Returned non-successful response code from server.", e2);
                    throw new RetryLaterException();
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        } else {
            Log.w(TAG, "Cannot create story onboarding without release channel recipient.");
            throw new Exception("No release channel recipient.");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return exc instanceof RetryLaterException;
    }

    private final List<String> getLocaleCodes() {
        LocaleListCompat localeListCompat = LocaleListCompat.getDefault();
        Intrinsics.checkNotNullExpressionValue(localeListCompat, "getDefault()");
        ArrayList arrayList = new ArrayList();
        if (!Intrinsics.areEqual(SignalStore.settings().getLanguage(), "zz")) {
            arrayList.add(SignalStore.settings().getLanguage());
        }
        int size = localeListCompat.size();
        for (int i = 0; i < size; i++) {
            Locale locale = localeListCompat.get(i);
            Intrinsics.checkNotNullExpressionValue(locale, "localeList.get(index)");
            String language = locale.getLanguage();
            Intrinsics.checkNotNullExpressionValue(language, "locale.language");
            boolean z = true;
            if (language.length() > 0) {
                String country = locale.getCountry();
                Intrinsics.checkNotNullExpressionValue(country, "locale.country");
                if (country.length() <= 0) {
                    z = false;
                }
                if (z) {
                    arrayList.add(locale.getLanguage() + '_' + locale.getCountry());
                }
                arrayList.add(locale.getLanguage());
            }
        }
        arrayList.add("en");
        return arrayList;
    }

    /* compiled from: StoryOnboardingDownloadJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/StoryOnboardingDownloadJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/StoryOnboardingDownloadJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<StoryOnboardingDownloadJob> {
        public StoryOnboardingDownloadJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new StoryOnboardingDownloadJob(parameters, null);
        }
    }
}
