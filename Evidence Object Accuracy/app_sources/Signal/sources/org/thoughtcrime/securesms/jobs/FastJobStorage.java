package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.JobDatabase;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.jobmanager.JobController$$ExternalSyntheticLambda6;
import org.thoughtcrime.securesms.jobmanager.persistence.ConstraintSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.DependencySpec;
import org.thoughtcrime.securesms.jobmanager.persistence.FullSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;
import org.thoughtcrime.securesms.jobmanager.persistence.JobStorage;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class FastJobStorage implements JobStorage {
    private static final String TAG = Log.tag(FastJobStorage.class);
    private final Map<String, List<ConstraintSpec>> constraintsByJobId = new HashMap();
    private final Map<String, List<DependencySpec>> dependenciesByJobId = new HashMap();
    private final JobDatabase jobDatabase;
    private final List<JobSpec> jobs = new ArrayList();

    public FastJobStorage(JobDatabase jobDatabase) {
        this.jobDatabase = jobDatabase;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized void init() {
        List<JobSpec> allJobSpecs = this.jobDatabase.getAllJobSpecs();
        List<ConstraintSpec> allConstraintSpecs = this.jobDatabase.getAllConstraintSpecs();
        List<DependencySpec> allDependencySpecs = this.jobDatabase.getAllDependencySpecs();
        this.jobs.addAll(allJobSpecs);
        for (ConstraintSpec constraintSpec : allConstraintSpecs) {
            List<ConstraintSpec> list = (List) Util.getOrDefault(this.constraintsByJobId, constraintSpec.getJobSpecId(), new LinkedList());
            list.add(constraintSpec);
            this.constraintsByJobId.put(constraintSpec.getJobSpecId(), list);
        }
        for (DependencySpec dependencySpec : allDependencySpecs) {
            List<DependencySpec> list2 = (List) Util.getOrDefault(this.dependenciesByJobId, dependencySpec.getJobId(), new LinkedList());
            list2.add(dependencySpec);
            this.dependenciesByJobId.put(dependencySpec.getJobId(), list2);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized void insertJobs(List<FullSpec> list) {
        List<FullSpec> list2 = Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((FullSpec) obj).isMemoryOnly();
            }
        }).toList();
        if (list2.size() > 0) {
            this.jobDatabase.insertJobs(list2);
        }
        for (FullSpec fullSpec : list) {
            this.jobs.add(fullSpec.getJobSpec());
            this.constraintsByJobId.put(fullSpec.getJobSpec().getId(), fullSpec.getConstraintSpecs());
            this.dependenciesByJobId.put(fullSpec.getJobSpec().getId(), fullSpec.getDependencySpecs());
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized JobSpec getJobSpec(String str) {
        for (JobSpec jobSpec : this.jobs) {
            if (jobSpec.getId().equals(str)) {
                return jobSpec;
            }
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized List<JobSpec> getAllJobSpecs() {
        return new ArrayList(this.jobs);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized List<JobSpec> getPendingJobsWithNoDependenciesInCreatedOrder(long j) {
        Optional<JobSpec> migrationJob = getMigrationJob();
        if (migrationJob.isPresent() && !migrationJob.get().isRunning() && migrationJob.get().getNextRunAttemptTime() <= j) {
            return Collections.singletonList(migrationJob.get());
        } else if (migrationJob.isPresent()) {
            return Collections.emptyList();
        } else {
            return Stream.of(this.jobs).groupBy(new Function() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda5
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return FastJobStorage.lambda$getPendingJobsWithNoDependenciesInCreatedOrder$0((JobSpec) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return FastJobStorage.lambda$getPendingJobsWithNoDependenciesInCreatedOrder$2((Map.Entry) obj);
                }
            }).withoutNulls().filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda7
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return FastJobStorage.this.lambda$getPendingJobsWithNoDependenciesInCreatedOrder$3((JobSpec) obj);
                }
            }).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda8
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ((JobSpec) obj).isRunning();
                }
            }).filter(new Predicate(j) { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda9
                public final /* synthetic */ long f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return FastJobStorage.lambda$getPendingJobsWithNoDependenciesInCreatedOrder$4(this.f$0, (JobSpec) obj);
                }
            }).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda10
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return FastJobStorage.lambda$getPendingJobsWithNoDependenciesInCreatedOrder$5((JobSpec) obj, (JobSpec) obj2);
                }
            }).toList();
        }
    }

    public static /* synthetic */ String lambda$getPendingJobsWithNoDependenciesInCreatedOrder$0(JobSpec jobSpec) {
        String queueKey = jobSpec.getQueueKey();
        if (queueKey != null) {
            return queueKey;
        }
        return jobSpec.getId();
    }

    public static /* synthetic */ int lambda$getPendingJobsWithNoDependenciesInCreatedOrder$1(JobSpec jobSpec, JobSpec jobSpec2) {
        return Long.compare(jobSpec.getCreateTime(), jobSpec2.getCreateTime());
    }

    public static /* synthetic */ JobSpec lambda$getPendingJobsWithNoDependenciesInCreatedOrder$2(Map.Entry entry) {
        return (JobSpec) Stream.of((Iterable) entry.getValue()).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda13
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return FastJobStorage.lambda$getPendingJobsWithNoDependenciesInCreatedOrder$1((JobSpec) obj, (JobSpec) obj2);
            }
        }).findFirst().orElse(null);
    }

    public /* synthetic */ boolean lambda$getPendingJobsWithNoDependenciesInCreatedOrder$3(JobSpec jobSpec) {
        List<DependencySpec> list = this.dependenciesByJobId.get(jobSpec.getId());
        return list == null || list.isEmpty();
    }

    public static /* synthetic */ boolean lambda$getPendingJobsWithNoDependenciesInCreatedOrder$4(long j, JobSpec jobSpec) {
        return jobSpec.getNextRunAttemptTime() <= j;
    }

    public static /* synthetic */ int lambda$getPendingJobsWithNoDependenciesInCreatedOrder$5(JobSpec jobSpec, JobSpec jobSpec2) {
        return Long.compare(jobSpec.getCreateTime(), jobSpec2.getCreateTime());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized List<JobSpec> getJobsInQueue(String str) {
        return Stream.of(this.jobs).filter(new Predicate(str) { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda18
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$getJobsInQueue$6(this.f$0, (JobSpec) obj);
            }
        }).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda19
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return FastJobStorage.lambda$getJobsInQueue$7((JobSpec) obj, (JobSpec) obj2);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$getJobsInQueue$6(String str, JobSpec jobSpec) {
        return str.equals(jobSpec.getQueueKey());
    }

    public static /* synthetic */ int lambda$getJobsInQueue$7(JobSpec jobSpec, JobSpec jobSpec2) {
        return Long.compare(jobSpec.getCreateTime(), jobSpec2.getCreateTime());
    }

    private Optional<JobSpec> getMigrationJob() {
        return Optional.ofNullable((JobSpec) Stream.of(this.jobs).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda14
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$getMigrationJob$8((JobSpec) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.this.firstInQueue((JobSpec) obj);
            }
        }).findFirst().orElse(null));
    }

    public static /* synthetic */ boolean lambda$getMigrationJob$8(JobSpec jobSpec) {
        return Job.Parameters.MIGRATION_QUEUE_KEY.equals(jobSpec.getQueueKey());
    }

    public boolean firstInQueue(JobSpec jobSpec) {
        if (jobSpec.getQueueKey() == null) {
            return true;
        }
        return ((JobSpec) Stream.of(this.jobs).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$firstInQueue$9(JobSpec.this, (JobSpec) obj);
            }
        }).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda2
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return FastJobStorage.lambda$firstInQueue$10((JobSpec) obj, (JobSpec) obj2);
            }
        }).toList().get(0)).equals(jobSpec);
    }

    public static /* synthetic */ boolean lambda$firstInQueue$9(JobSpec jobSpec, JobSpec jobSpec2) {
        return Util.equals(jobSpec2.getQueueKey(), jobSpec.getQueueKey());
    }

    public static /* synthetic */ int lambda$firstInQueue$10(JobSpec jobSpec, JobSpec jobSpec2) {
        return Long.compare(jobSpec.getCreateTime(), jobSpec2.getCreateTime());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized int getJobCountForFactory(String str) {
        return (int) Stream.of(this.jobs).filter(new Predicate(str) { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$getJobCountForFactory$11(this.f$0, (JobSpec) obj);
            }
        }).count();
    }

    public static /* synthetic */ boolean lambda$getJobCountForFactory$11(String str, JobSpec jobSpec) {
        return jobSpec.getFactoryKey().equals(str);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized int getJobCountForFactoryAndQueue(String str, String str2) {
        return (int) Stream.of(this.jobs).filter(new Predicate(str, str2) { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda17
            public final /* synthetic */ String f$0;
            public final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$getJobCountForFactoryAndQueue$12(this.f$0, this.f$1, (JobSpec) obj);
            }
        }).count();
    }

    public static /* synthetic */ boolean lambda$getJobCountForFactoryAndQueue$12(String str, String str2, JobSpec jobSpec) {
        return str.equals(jobSpec.getFactoryKey()) && str2.equals(jobSpec.getQueueKey());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public boolean areQueuesEmpty(Set<String> set) {
        return Stream.of(this.jobs).noneMatch(new Predicate(set) { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda3
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$areQueuesEmpty$13(this.f$0, (JobSpec) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$areQueuesEmpty$13(Set set, JobSpec jobSpec) {
        return jobSpec.getQueueKey() != null && set.contains(jobSpec.getQueueKey());
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0028 A[Catch: all -> 0x007f, TryCatch #0 {, blocks: (B:4:0x0005, B:6:0x000b, B:10:0x0015, B:11:0x001c, B:12:0x0022, B:14:0x0028, B:16:0x0038), top: B:22:0x0005 }] */
    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void updateJobRunningState(java.lang.String r22, boolean r23) {
        /*
            r21 = this;
            r1 = r21
            r0 = r22
            monitor-enter(r21)
            org.thoughtcrime.securesms.jobmanager.persistence.JobSpec r2 = r21.getJobById(r22)     // Catch: all -> 0x007f
            if (r2 == 0) goto L_0x0015
            boolean r2 = r2.isMemoryOnly()     // Catch: all -> 0x007f
            if (r2 != 0) goto L_0x0012
            goto L_0x0015
        L_0x0012:
            r15 = r23
            goto L_0x001c
        L_0x0015:
            org.thoughtcrime.securesms.database.JobDatabase r2 = r1.jobDatabase     // Catch: all -> 0x007f
            r15 = r23
            r2.updateJobRunningState(r0, r15)     // Catch: all -> 0x007f
        L_0x001c:
            java.util.List<org.thoughtcrime.securesms.jobmanager.persistence.JobSpec> r2 = r1.jobs     // Catch: all -> 0x007f
            java.util.ListIterator r2 = r2.listIterator()     // Catch: all -> 0x007f
        L_0x0022:
            boolean r3 = r2.hasNext()     // Catch: all -> 0x007f
            if (r3 == 0) goto L_0x007d
            java.lang.Object r3 = r2.next()     // Catch: all -> 0x007f
            org.thoughtcrime.securesms.jobmanager.persistence.JobSpec r3 = (org.thoughtcrime.securesms.jobmanager.persistence.JobSpec) r3     // Catch: all -> 0x007f
            java.lang.String r4 = r3.getId()     // Catch: all -> 0x007f
            boolean r4 = r4.equals(r0)     // Catch: all -> 0x007f
            if (r4 == 0) goto L_0x0078
            org.thoughtcrime.securesms.jobmanager.persistence.JobSpec r13 = new org.thoughtcrime.securesms.jobmanager.persistence.JobSpec     // Catch: all -> 0x007f
            java.lang.String r4 = r3.getId()     // Catch: all -> 0x007f
            java.lang.String r5 = r3.getFactoryKey()     // Catch: all -> 0x007f
            java.lang.String r6 = r3.getQueueKey()     // Catch: all -> 0x007f
            long r7 = r3.getCreateTime()     // Catch: all -> 0x007f
            long r9 = r3.getNextRunAttemptTime()     // Catch: all -> 0x007f
            int r11 = r3.getRunAttempt()     // Catch: all -> 0x007f
            int r12 = r3.getMaxAttempts()     // Catch: all -> 0x007f
            long r16 = r3.getLifespan()     // Catch: all -> 0x007f
            java.lang.String r18 = r3.getSerializedData()     // Catch: all -> 0x007f
            java.lang.String r19 = r3.getSerializedInputData()     // Catch: all -> 0x007f
            boolean r20 = r3.isMemoryOnly()     // Catch: all -> 0x007f
            r3 = r13
            r0 = r13
            r13 = r16
            r15 = r18
            r16 = r19
            r17 = r23
            r18 = r20
            r3.<init>(r4, r5, r6, r7, r9, r11, r12, r13, r15, r16, r17, r18)     // Catch: all -> 0x007f
            r2.set(r0)     // Catch: all -> 0x007f
        L_0x0078:
            r0 = r22
            r15 = r23
            goto L_0x0022
        L_0x007d:
            monitor-exit(r21)
            return
        L_0x007f:
            r0 = move-exception
            monitor-exit(r21)
            goto L_0x0083
        L_0x0082:
            throw r0
        L_0x0083:
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.FastJobStorage.updateJobRunningState(java.lang.String, boolean):void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized void updateJobAfterRetry(String str, boolean z, int i, long j, String str2) {
        JobSpec jobById = getJobById(str);
        if (jobById == null || !jobById.isMemoryOnly()) {
            this.jobDatabase.updateJobAfterRetry(str, z, i, j, str2);
        }
        ListIterator<JobSpec> listIterator = this.jobs.listIterator();
        while (listIterator.hasNext()) {
            JobSpec next = listIterator.next();
            if (next.getId().equals(str)) {
                listIterator.set(new JobSpec(next.getId(), next.getFactoryKey(), next.getQueueKey(), next.getCreateTime(), j, i, next.getMaxAttempts(), next.getLifespan(), str2, next.getSerializedInputData(), z, next.isMemoryOnly()));
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized void updateAllJobsToBePending() {
        this.jobDatabase.updateAllJobsToBePending();
        ListIterator<JobSpec> listIterator = this.jobs.listIterator();
        while (listIterator.hasNext()) {
            JobSpec next = listIterator.next();
            listIterator.set(new JobSpec(next.getId(), next.getFactoryKey(), next.getQueueKey(), next.getCreateTime(), next.getNextRunAttemptTime(), next.getRunAttempt(), next.getMaxAttempts(), next.getLifespan(), next.getSerializedData(), next.getSerializedInputData(), false, next.isMemoryOnly()));
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public void updateJobs(List<JobSpec> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (JobSpec jobSpec : list) {
            JobSpec jobById = getJobById(jobSpec.getId());
            if (jobById == null || !jobById.isMemoryOnly()) {
                arrayList.add(jobSpec);
            }
        }
        if (arrayList.size() > 0) {
            this.jobDatabase.updateJobs(arrayList);
        }
        Map map = (Map) Stream.of(list).collect(Collectors.toMap(new JobController$$ExternalSyntheticLambda6()));
        ListIterator<JobSpec> listIterator = this.jobs.listIterator();
        while (listIterator.hasNext()) {
            JobSpec jobSpec2 = (JobSpec) map.get(listIterator.next().getId());
            if (jobSpec2 != null) {
                listIterator.set(jobSpec2);
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized void deleteJob(String str) {
        deleteJobs(Collections.singletonList(str));
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized void deleteJobs(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (String str : list) {
            JobSpec jobById = getJobById(str);
            if (jobById == null || !jobById.isMemoryOnly()) {
                arrayList.add(str);
            }
        }
        if (arrayList.size() > 0) {
            this.jobDatabase.deleteJobs(arrayList);
        }
        HashSet hashSet = new HashSet(list);
        Iterator<JobSpec> it = this.jobs.iterator();
        while (it.hasNext()) {
            if (hashSet.contains(it.next().getId())) {
                it.remove();
            }
        }
        for (String str2 : list) {
            this.constraintsByJobId.remove(str2);
            this.dependenciesByJobId.remove(str2);
            for (Map.Entry<String, List<DependencySpec>> entry : this.dependenciesByJobId.entrySet()) {
                Iterator<DependencySpec> it2 = entry.getValue().iterator();
                while (it2.hasNext()) {
                    if (it2.next().getDependsOnJobId().equals(str2)) {
                        it2.remove();
                    }
                }
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized List<ConstraintSpec> getConstraintSpecs(String str) {
        return (List) Util.getOrDefault(this.constraintsByJobId, str, new LinkedList());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized List<ConstraintSpec> getAllConstraintSpecs() {
        return Stream.of(this.constraintsByJobId).map(new FastJobStorage$$ExternalSyntheticLambda11()).flatMap(new FastJobStorage$$ExternalSyntheticLambda12()).toList();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public synchronized List<DependencySpec> getDependencySpecsThatDependOnJob(String str) {
        ArrayList arrayList;
        List<DependencySpec> singleLayerOfDependencySpecsThatDependOnJob = getSingleLayerOfDependencySpecsThatDependOnJob(str);
        arrayList = new ArrayList(singleLayerOfDependencySpecsThatDependOnJob);
        do {
            singleLayerOfDependencySpecsThatDependOnJob.clear();
            for (String str2 : (Set) Stream.of(singleLayerOfDependencySpecsThatDependOnJob).map(new JobController$$ExternalSyntheticLambda3()).collect(Collectors.toSet())) {
                singleLayerOfDependencySpecsThatDependOnJob.addAll(getSingleLayerOfDependencySpecsThatDependOnJob(str2));
            }
            arrayList.addAll(singleLayerOfDependencySpecsThatDependOnJob);
        } while (!singleLayerOfDependencySpecsThatDependOnJob.isEmpty());
        return arrayList;
    }

    private List<DependencySpec> getSingleLayerOfDependencySpecsThatDependOnJob(String str) {
        return Stream.of(this.dependenciesByJobId.entrySet()).map(new FastJobStorage$$ExternalSyntheticLambda11()).flatMap(new FastJobStorage$$ExternalSyntheticLambda12()).filter(new Predicate(str) { // from class: org.thoughtcrime.securesms.jobs.FastJobStorage$$ExternalSyntheticLambda16
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return FastJobStorage.lambda$getSingleLayerOfDependencySpecsThatDependOnJob$14(this.f$0, (DependencySpec) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$getSingleLayerOfDependencySpecsThatDependOnJob$14(String str, DependencySpec dependencySpec) {
        return dependencySpec.getDependsOnJobId().equals(str);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.persistence.JobStorage
    public List<DependencySpec> getAllDependencySpecs() {
        return Stream.of(this.dependenciesByJobId).map(new FastJobStorage$$ExternalSyntheticLambda11()).flatMap(new FastJobStorage$$ExternalSyntheticLambda12()).toList();
    }

    private JobSpec getJobById(String str) {
        for (JobSpec jobSpec : this.jobs) {
            if (jobSpec.getId().equals(str)) {
                return jobSpec;
            }
        }
        String str2 = TAG;
        Log.w(str2, "Was looking for job with ID JOB::" + str + ", but it doesn't exist in memory!");
        return null;
    }
}
