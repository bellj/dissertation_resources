package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.whispersystems.signalservice.internal.EmptyResponse;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes4.dex */
public class DonationReceiptRedemptionJob extends BaseJob {
    public static final String DATA_ERROR_SOURCE;
    public static final String DATA_GIFT_MESSAGE_ID;
    public static final String DATA_PRIMARY;
    public static final String INPUT_RECEIPT_CREDENTIAL_PRESENTATION;
    public static final String KEY;
    private static final long NO_ID;
    public static final String SUBSCRIPTION_QUEUE;
    private static final String TAG = Log.tag(DonationReceiptRedemptionJob.class);
    private final DonationErrorSource errorSource;
    private final long giftMessageId;
    private final boolean makePrimary;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public static DonationReceiptRedemptionJob createJobForSubscription(DonationErrorSource donationErrorSource) {
        return new DonationReceiptRedemptionJob(-1, false, donationErrorSource, new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(SUBSCRIPTION_QUEUE).setMaxAttempts(-1).setMaxInstancesForQueue(1).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
    }

    public static DonationReceiptRedemptionJob createJobForBoost() {
        return new DonationReceiptRedemptionJob(-1, false, DonationErrorSource.BOOST, new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue("BoostReceiptRedemption").setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
    }

    public static JobManager.Chain createJobChainForGift(long j, boolean z) {
        DonationErrorSource donationErrorSource = DonationErrorSource.GIFT_REDEMPTION;
        Job.Parameters.Builder addConstraint = new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY);
        DonationReceiptRedemptionJob donationReceiptRedemptionJob = new DonationReceiptRedemptionJob(j, z, donationErrorSource, addConstraint.setQueue("GiftReceiptRedemption-" + j).setMaxAttempts(-1).setLifespan(TimeUnit.DAYS.toMillis(1)).build());
        RefreshOwnProfileJob refreshOwnProfileJob = new RefreshOwnProfileJob();
        return ApplicationDependencies.getJobManager().startChain(donationReceiptRedemptionJob).then(refreshOwnProfileJob).then(new MultiDeviceProfileContentUpdateJob());
    }

    private DonationReceiptRedemptionJob(long j, boolean z, DonationErrorSource donationErrorSource, Job.Parameters parameters) {
        super(parameters);
        this.giftMessageId = j;
        this.makePrimary = z;
        this.errorSource = donationErrorSource;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(DATA_ERROR_SOURCE, this.errorSource.serialize()).putLong(DATA_GIFT_MESSAGE_ID, this.giftMessageId).putBoolean(DATA_PRIMARY, this.makePrimary).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        if (isForSubscription()) {
            Log.d(TAG, "Marking subscription failure", true);
            SignalStore.donationsValues().markSubscriptionRedemptionFailed();
            MultiDeviceSubscriptionSyncRequestJob.enqueue();
        } else if (this.giftMessageId != -1) {
            SignalDatabase.mms().markGiftRedemptionFailed(this.giftMessageId);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        if (this.giftMessageId != -1) {
            SignalDatabase.mms().markGiftRedemptionStarted(this.giftMessageId);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        ReceiptCredentialPresentation presentation = getPresentation();
        if (presentation == null) {
            Log.d(TAG, "No presentation available. Exiting.", true);
            return;
        }
        String str = TAG;
        Log.d(str, "Attempting to redeem token... isForSubscription: " + isForSubscription(), true);
        ServiceResponse<EmptyResponse> blockingGet = ApplicationDependencies.getDonationsService().redeemReceipt(presentation, SignalStore.donationsValues().getDisplayBadgesOnProfile(), this.makePrimary).blockingGet();
        if (blockingGet.getApplicationError().isPresent()) {
            if (blockingGet.getStatus() >= 500) {
                Log.w(str, "Encountered a server exception " + blockingGet.getStatus(), blockingGet.getApplicationError().get(), true);
                throw new RetryableException();
            }
            Log.w(str, "Encountered a non-recoverable exception " + blockingGet.getStatus(), blockingGet.getApplicationError().get(), true);
            DonationError.routeDonationError(this.context, DonationError.genericBadgeRedemptionFailure(this.errorSource));
            throw new IOException(blockingGet.getApplicationError().get());
        } else if (!blockingGet.getExecutionError().isPresent()) {
            Log.i(str, "Successfully redeemed token with response code " + blockingGet.getStatus() + "... isForSubscription: " + isForSubscription(), true);
            if (isForSubscription()) {
                Log.d(str, "Clearing subscription failure", true);
                SignalStore.donationsValues().clearSubscriptionRedemptionFailed();
            } else if (this.giftMessageId != -1) {
                Log.d(str, "Marking gift redemption completed for " + this.giftMessageId);
                SignalDatabase.mms().markGiftRedemptionCompleted(this.giftMessageId);
                MessageDatabase.MarkedMessageInfo incomingMessageViewed = SignalDatabase.mms().setIncomingMessageViewed(this.giftMessageId);
                if (incomingMessageViewed != null) {
                    Log.d(str, "Marked gift message viewed for " + this.giftMessageId);
                    MultiDeviceViewedUpdateJob.enqueue(Collections.singletonList(incomingMessageViewed.getSyncMessageId()));
                }
            }
        } else {
            Log.w(str, "Encountered a retryable exception", blockingGet.getExecutionError().get(), true);
            throw new RetryableException();
        }
    }

    private ReceiptCredentialPresentation getPresentation() throws InvalidInputException, NoSuchMessageException {
        if (this.giftMessageId == -1) {
            return getPresentationFromInputData();
        }
        return getPresentationFromGiftMessage();
    }

    private ReceiptCredentialPresentation getPresentationFromInputData() throws InvalidInputException {
        Data inputData = getInputData();
        if (inputData == null) {
            Log.w(TAG, "No input data. Exiting.", true);
            return null;
        }
        byte[] stringAsBlob = inputData.getStringAsBlob(INPUT_RECEIPT_CREDENTIAL_PRESENTATION);
        if (stringAsBlob != null) {
            return new ReceiptCredentialPresentation(stringAsBlob);
        }
        Log.d(TAG, "No response data. Exiting.", true);
        return null;
    }

    private ReceiptCredentialPresentation getPresentationFromGiftMessage() throws InvalidInputException, NoSuchMessageException {
        MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(this.giftMessageId);
        if (MessageRecordUtil.hasGiftBadge(messageRecord)) {
            GiftBadge requireGiftBadge = MessageRecordUtil.requireGiftBadge(messageRecord);
            if (requireGiftBadge.getRedemptionState() == GiftBadge.RedemptionState.REDEEMED) {
                Log.d(TAG, "Already redeemed this gift badge. Exiting.", true);
                return null;
            }
            String str = TAG;
            Log.d(str, "Attempting redemption  of badge in state " + requireGiftBadge.getRedemptionState().name());
            return new ReceiptCredentialPresentation(requireGiftBadge.getRedemptionToken().toByteArray());
        }
        Log.d(TAG, "No gift badge on message record. Exiting.", true);
        return null;
    }

    private boolean isForSubscription() {
        return Objects.equals(getParameters().getQueue(), SUBSCRIPTION_QUEUE);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryableException;
    }

    /* loaded from: classes4.dex */
    private static final class RetryableException extends Exception {
        private RetryableException() {
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<DonationReceiptRedemptionJob> {
        public DonationReceiptRedemptionJob create(Job.Parameters parameters, Data data) {
            return new DonationReceiptRedemptionJob(data.getLongOrDefault(DonationReceiptRedemptionJob.DATA_GIFT_MESSAGE_ID, -1), data.getBooleanOrDefault(DonationReceiptRedemptionJob.DATA_PRIMARY, false), DonationErrorSource.deserialize(data.getStringOrDefault(DonationReceiptRedemptionJob.DATA_ERROR_SOURCE, DonationErrorSource.UNKNOWN.serialize())), parameters);
        }
    }
}
