package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.IOException;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class ReportSpamJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_THREAD_ID;
    private static final String KEY_TIMESTAMP;
    private static final int MAX_MESSAGE_COUNT;
    private static final String TAG = Log.tag(ReportSpamJob.class);
    private final long threadId;
    private final long timestamp;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public ReportSpamJob(long j, long j2) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setMaxAttempts(5).setQueue(KEY).build(), j, j2);
    }

    private ReportSpamJob(Job.Parameters parameters, long j, long j2) {
        super(parameters);
        this.threadId = j;
        this.timestamp = j2;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("thread_id", this.threadId).putLong("timestamp", this.timestamp).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        if (SignalStore.account().isRegistered()) {
            int i = 0;
            List<MessageDatabase.ReportSpamData> reportSpamMessageServerData = SignalDatabase.mmsSms().getReportSpamMessageServerData(this.threadId, this.timestamp, 3);
            SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
            for (MessageDatabase.ReportSpamData reportSpamData : reportSpamMessageServerData) {
                Optional<ServiceId> serviceId = Recipient.resolved(reportSpamData.getRecipientId()).getServiceId();
                if (!serviceId.isPresent() || serviceId.get().isUnknown()) {
                    String str = TAG;
                    Log.w(str, "Unable to report spam without an ACI for " + reportSpamData.getRecipientId());
                } else {
                    signalServiceAccountManager.reportSpam(serviceId.get(), reportSpamData.getServerGuid());
                    i++;
                }
            }
            String str2 = TAG;
            Log.i(str2, "Reported " + i + " out of " + reportSpamMessageServerData.size() + " messages in thread " + this.threadId + " as spam");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        if (exc instanceof NonSuccessfulResponseCodeException) {
            return ((NonSuccessfulResponseCodeException) exc).is5xx();
        }
        return exc instanceof IOException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        String str = TAG;
        Log.w(str, "Canceling report spam for thread " + this.threadId);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<ReportSpamJob> {
        public ReportSpamJob create(Job.Parameters parameters, Data data) {
            return new ReportSpamJob(parameters, data.getLong("thread_id"), data.getLong("timestamp"));
        }
    }
}
