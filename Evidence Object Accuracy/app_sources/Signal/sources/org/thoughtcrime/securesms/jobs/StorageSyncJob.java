package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.DesugarArrays;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.UnknownStorageIdDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.storage.AccountRecordProcessor;
import org.thoughtcrime.securesms.storage.ContactRecordProcessor;
import org.thoughtcrime.securesms.storage.GroupV1RecordProcessor;
import org.thoughtcrime.securesms.storage.GroupV2RecordProcessor;
import org.thoughtcrime.securesms.storage.StorageKeyGenerator;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.storage.StorageSyncModels;
import org.thoughtcrime.securesms.storage.StorageSyncValidations;
import org.thoughtcrime.securesms.storage.StoryDistributionListRecordProcessor;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.RequestMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.SignalContactRecord;
import org.whispersystems.signalservice.api.storage.SignalGroupV1Record;
import org.whispersystems.signalservice.api.storage.SignalGroupV2Record;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.storage.StorageKey;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.storage.protos.ManifestRecord;

/* loaded from: classes4.dex */
public class StorageSyncJob extends BaseJob {
    public static final String KEY;
    public static final String QUEUE_KEY;
    private static final String TAG = Log.tag(StorageSyncJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public StorageSyncJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(QUEUE_KEY).setMaxInstancesForFactory(2).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(3).build());
    }

    private StorageSyncJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws IOException, RetryLaterException, UntrustedIdentityException {
        if (!SignalStore.kbsValues().hasPin() && !SignalStore.kbsValues().hasOptedOut()) {
            Log.i(TAG, "Doesn't have a PIN. Skipping.");
        } else if (!SignalStore.account().isRegistered()) {
            Log.i(TAG, "Not registered. Skipping.");
        } else if (!Recipient.self().hasE164() || !Recipient.self().hasServiceId()) {
            Log.w(TAG, "Missing E164 or ACI!");
        } else if (SignalStore.internalValues().storageServiceDisabled()) {
            Log.w(TAG, "Storage service has been manually disabled. Skipping.");
        } else {
            try {
                boolean performSync = performSync();
                if (TextSecurePreferences.isMultiDevice(this.context) && performSync) {
                    ApplicationDependencies.getJobManager().add(new MultiDeviceStorageSyncRequestJob());
                }
                SignalStore.storageService().onSyncCompleted();
            } catch (InvalidKeyException e) {
                if (SignalStore.account().isPrimaryDevice()) {
                    Log.w(TAG, "Failed to decrypt remote storage! Force-pushing and syncing the storage key to linked devices.", e);
                    ApplicationDependencies.getJobManager().startChain(new MultiDeviceKeysUpdateJob()).then(new StorageForcePushJob()).then(new MultiDeviceStorageSyncRequestJob()).enqueue();
                    return;
                }
                Log.w(TAG, "Failed to decrypt remote storage! Requesting new keys from primary.", e);
                SignalStore.storageService().clearStorageKeyFromPrimary();
                ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forRequest(RequestMessage.forType(SignalServiceProtos.SyncMessage.Request.Type.KEYS)), UnidentifiedAccessUtil.getAccessForSync(this.context));
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return (exc instanceof PushNetworkException) || (exc instanceof RetryLaterException);
    }

    /* JADX INFO: finally extract failed */
    private boolean performSync() throws IOException, RetryLaterException, InvalidKeyException {
        boolean z;
        Throwable th;
        SignalServiceAccountManager signalServiceAccountManager;
        Stopwatch stopwatch = new Stopwatch("StorageSync");
        SQLiteDatabase rawDatabase = SignalDatabase.getRawDatabase();
        SignalServiceAccountManager signalServiceAccountManager2 = ApplicationDependencies.getSignalServiceAccountManager();
        UnknownStorageIdDatabase unknownStorageIds = SignalDatabase.unknownStorageIds();
        StorageKey orCreateStorageKey = SignalStore.storageService().getOrCreateStorageKey();
        SignalStorageManifest manifest = SignalStore.storageService().getManifest();
        SignalStorageManifest orElse = signalServiceAccountManager2.getStorageManifestIfDifferentVersion(orCreateStorageKey, manifest.getVersion()).orElse(manifest);
        stopwatch.split("remote-manifest");
        Recipient freshSelf = freshSelf();
        if (freshSelf.getStorageServiceId() == null) {
            Log.w(TAG, "No storageId for self. Generating.");
            SignalDatabase.recipients().updateStorageId(freshSelf.getId(), StorageSyncHelper.generateKey());
            freshSelf = freshSelf();
        }
        String str = TAG;
        Log.i(str, "Our version: " + manifest.getVersion() + ", their version: " + orElse.getVersion());
        boolean z2 = true;
        if (orElse.getVersion() > manifest.getVersion()) {
            Log.i(str, "[Remote Sync] Newer manifest version found!");
            StorageSyncHelper.IdDifferenceResult findIdDifference = StorageSyncHelper.findIdDifference(orElse.getStorageIds(), getAllLocalStorageIds(freshSelf));
            if (!findIdDifference.hasTypeMismatches() || !SignalStore.account().isPrimaryDevice()) {
                z = false;
            } else {
                Log.w(str, "[Remote Sync] Found type mismatches in the ID sets! Scheduling a force push after this sync completes.");
                z = true;
            }
            Log.i(str, "[Remote Sync] Pre-Merge ID Difference :: " + findIdDifference);
            stopwatch.split("remote-id-diff");
            if (!findIdDifference.isEmpty()) {
                Log.i(str, "[Remote Sync] Retrieving records for key difference.");
                List<SignalStorageRecord> readStorageRecords = signalServiceAccountManager2.readStorageRecords(orCreateStorageKey, findIdDifference.getRemoteOnlyIds());
                stopwatch.split("remote-records");
                if (readStorageRecords.size() != findIdDifference.getRemoteOnlyIds().size()) {
                    Log.w(str, "[Remote Sync] Could not find all remote-only records! Requested: " + findIdDifference.getRemoteOnlyIds().size() + ", Found: " + readStorageRecords.size() + ". These stragglers should naturally get deleted during the sync.");
                }
                StorageRecordCollection storageRecordCollection = new StorageRecordCollection(readStorageRecords);
                rawDatabase.beginTransaction();
                try {
                    Log.i(str, "[Remote Sync] Remote-Only :: Contacts: " + storageRecordCollection.contacts.size() + ", GV1: " + storageRecordCollection.gv1.size() + ", GV2: " + storageRecordCollection.gv2.size() + ", Account: " + storageRecordCollection.account.size() + ", DLists: " + storageRecordCollection.storyDistributionLists.size());
                    processKnownRecords(this.context, storageRecordCollection);
                    List<SignalStorageRecord> list = storageRecordCollection.unknown;
                    List list2 = Stream.of(findIdDifference.getLocalOnlyIds()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.StorageSyncJob$$ExternalSyntheticLambda0
                        @Override // com.annimon.stream.function.Predicate
                        public final boolean test(Object obj) {
                            return ((StorageId) obj).isUnknown();
                        }
                    }).toList();
                    StringBuilder sb = new StringBuilder();
                    sb.append("[Remote Sync] Unknowns :: ");
                    sb.append(list.size());
                    sb.append(" inserts, ");
                    sb.append(list2.size());
                    sb.append(" deletes");
                    Log.i(str, sb.toString());
                    unknownStorageIds.insert(list);
                    unknownStorageIds.delete(list2);
                    rawDatabase.setTransactionSuccessful();
                } finally {
                    rawDatabase.endTransaction();
                    ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
                    stopwatch.split("remote-merge-transaction");
                }
            } else {
                Log.i(str, "[Remote Sync] Remote version was newer, but there were no remote-only IDs.");
            }
        } else {
            if (orElse.getVersion() < manifest.getVersion()) {
                Log.w(str, "[Remote Sync] Remote version was older. User might have switched accounts.");
            }
            z = false;
        }
        if (orElse != manifest) {
            Log.i(str, "[Remote Sync] Saved new manifest. Now at version: " + orElse.getVersion());
            SignalStore.storageService().setManifest(orElse);
        }
        Log.i(str, "We are up-to-date with the remote storage state.");
        rawDatabase.beginTransaction();
        try {
            Recipient freshSelf2 = freshSelf();
            List<StorageId> allLocalStorageIds = getAllLocalStorageIds(freshSelf2);
            StorageSyncHelper.IdDifferenceResult findIdDifference2 = StorageSyncHelper.findIdDifference(orElse.getStorageIds(), allLocalStorageIds);
            List<SignalStorageRecord> buildLocalStorageRecords = buildLocalStorageRecords(this.context, freshSelf2, findIdDifference2.getLocalOnlyIds());
            List list3 = Stream.of(findIdDifference2.getRemoteOnlyIds()).map(new StorageSyncJob$$ExternalSyntheticLambda1()).toList();
            Log.i(str, "ID Difference :: " + findIdDifference2);
            try {
                StorageSyncHelper.WriteOperationResult writeOperationResult = new StorageSyncHelper.WriteOperationResult(new SignalStorageManifest(orElse.getVersion() + 1, allLocalStorageIds), buildLocalStorageRecords, list3);
                rawDatabase.setTransactionSuccessful();
                rawDatabase.endTransaction();
                stopwatch.split("local-data-transaction");
                if (!writeOperationResult.isEmpty()) {
                    Log.i(str, "We have something to write remotely.");
                    Log.i(str, "WriteOperationResult :: " + writeOperationResult);
                    StorageSyncValidations.validate(writeOperationResult, orElse, z, freshSelf2);
                    signalServiceAccountManager = signalServiceAccountManager2;
                    if (!signalServiceAccountManager.writeStorageRecords(orCreateStorageKey, writeOperationResult.getManifest(), writeOperationResult.getInserts(), writeOperationResult.getDeletes()).isPresent()) {
                        Log.i(str, "Saved new manifest. Now at version: " + writeOperationResult.getManifest().getVersion());
                        SignalStore.storageService().setManifest(writeOperationResult.getManifest());
                        stopwatch.split("remote-write");
                    } else {
                        Log.w(str, "Hit a conflict when trying to resolve the conflict! Retrying.");
                        throw new RetryLaterException();
                    }
                } else {
                    signalServiceAccountManager = signalServiceAccountManager2;
                    Log.i(str, "No remote writes needed. Still at version: " + orElse.getVersion());
                    z2 = false;
                }
                List<Integer> knownTypes = getKnownTypes();
                List<StorageId> allWithTypes = SignalDatabase.unknownStorageIds().getAllWithTypes(knownTypes);
                if (allWithTypes.size() > 0) {
                    Log.i(str, "We have " + allWithTypes.size() + " unknown records that we can now process.");
                    List<SignalStorageRecord> readStorageRecords2 = signalServiceAccountManager.readStorageRecords(orCreateStorageKey, allWithTypes);
                    StorageRecordCollection storageRecordCollection2 = new StorageRecordCollection(readStorageRecords2);
                    Log.i(str, "Found " + readStorageRecords2.size() + " of the known-unknowns remotely.");
                    rawDatabase.beginTransaction();
                    try {
                        processKnownRecords(this.context, storageRecordCollection2);
                        SignalDatabase.unknownStorageIds().getAllWithTypes(knownTypes);
                        rawDatabase.setTransactionSuccessful();
                        rawDatabase.endTransaction();
                        Log.i(str, "Enqueueing a storage sync job to handle any possible merges after applying unknown records.");
                        ApplicationDependencies.getJobManager().add(new StorageSyncJob());
                    } catch (Throwable th2) {
                        rawDatabase.endTransaction();
                        throw th2;
                    }
                }
                stopwatch.split("known-unknowns");
                if (z && SignalStore.account().isPrimaryDevice()) {
                    Log.w(str, "Scheduling a force push.");
                    ApplicationDependencies.getJobManager().add(new StorageForcePushJob());
                }
                stopwatch.stop(str);
                return z2;
            } catch (Throwable th3) {
                th = th3;
                rawDatabase.endTransaction();
                stopwatch.split("local-data-transaction");
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
        }
    }

    private static void processKnownRecords(Context context, StorageRecordCollection storageRecordCollection) throws IOException {
        ContactRecordProcessor contactRecordProcessor = new ContactRecordProcessor(context, freshSelf());
        List<SignalContactRecord> list = storageRecordCollection.contacts;
        StorageKeyGenerator storageKeyGenerator = StorageSyncHelper.KEY_GENERATOR;
        contactRecordProcessor.process(list, storageKeyGenerator);
        new GroupV1RecordProcessor(context).process(storageRecordCollection.gv1, storageKeyGenerator);
        new GroupV2RecordProcessor(context).process(storageRecordCollection.gv2, storageKeyGenerator);
        new AccountRecordProcessor(context, freshSelf()).process(storageRecordCollection.account, storageKeyGenerator);
        if (getKnownTypes().contains(5)) {
            new StoryDistributionListRecordProcessor().process(storageRecordCollection.storyDistributionLists, storageKeyGenerator);
        }
    }

    private static List<StorageId> getAllLocalStorageIds(Recipient recipient) {
        return Util.concatenatedList(SignalDatabase.recipients().getContactStorageSyncIds(), Collections.singletonList(StorageId.forAccount(recipient.getStorageServiceId())), SignalDatabase.unknownStorageIds().getAllUnknownIds());
    }

    private static List<SignalStorageRecord> buildLocalStorageRecords(Context context, Recipient recipient, Collection<StorageId> collection) {
        if (collection.isEmpty()) {
            return Collections.emptyList();
        }
        RecipientDatabase recipients = SignalDatabase.recipients();
        UnknownStorageIdDatabase unknownStorageIds = SignalDatabase.unknownStorageIds();
        ArrayList arrayList = new ArrayList(collection.size());
        for (StorageId storageId : collection) {
            int type = storageId.getType();
            if (type == 1 || type == 2 || type == 3) {
                RecipientRecord byStorageId = recipients.getByStorageId(storageId.getRaw());
                if (byStorageId == null) {
                    throw new MissingRecipientModelError("Missing local recipient model! Type: " + storageId.getType());
                } else if (byStorageId.getGroupType() == RecipientDatabase.GroupType.SIGNAL_V2 && byStorageId.getSyncExtras().getGroupMasterKey() == null) {
                    throw new MissingGv2MasterKeyError();
                } else {
                    arrayList.add(StorageSyncModels.localToRemoteRecord(byStorageId));
                }
            } else if (type != 4) {
                if (type != 5) {
                    SignalStorageRecord byId = unknownStorageIds.getById(storageId.getRaw());
                    if (byId != null) {
                        arrayList.add(byId);
                    } else {
                        throw new MissingUnknownModelError("Missing local unknown model! Type: " + storageId.getType());
                    }
                } else {
                    RecipientRecord byStorageId2 = recipients.getByStorageId(storageId.getRaw());
                    if (byStorageId2 == null) {
                        throw new MissingRecipientModelError("Missing local recipient model! Type: " + storageId.getType());
                    } else if (byStorageId2.getDistributionListId() != null) {
                        arrayList.add(StorageSyncModels.localToRemoteRecord(byStorageId2));
                    } else {
                        throw new MissingRecipientModelError("Missing local recipient model (no DistributionListId)! Type: " + storageId.getType());
                    }
                }
            } else if (Arrays.equals(recipient.getStorageServiceId(), storageId.getRaw())) {
                arrayList.add(StorageSyncHelper.buildAccountRecord(context, recipient));
            } else {
                throw new AssertionError("Local storage ID doesn't match self!");
            }
        }
        return arrayList;
    }

    private static Recipient freshSelf() {
        Recipient.self().live().refresh();
        return Recipient.self();
    }

    private static List<Integer> getKnownTypes() {
        return (List) DesugarArrays.stream(ManifestRecord.Identifier.Type.values()).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.StorageSyncJob$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return StorageSyncJob.lambda$getKnownTypes$0((ManifestRecord.Identifier.Type) obj);
            }
        }).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.jobs.StorageSyncJob$$ExternalSyntheticLambda3
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return StorageSyncJob.lambda$getKnownTypes$1((ManifestRecord.Identifier.Type) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.StorageSyncJob$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StorageSyncJob.lambda$getKnownTypes$2((ManifestRecord.Identifier.Type) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ boolean lambda$getKnownTypes$0(ManifestRecord.Identifier.Type type) {
        return !type.equals(ManifestRecord.Identifier.Type.UNKNOWN) && !type.equals(ManifestRecord.Identifier.Type.UNRECOGNIZED);
    }

    public static /* synthetic */ boolean lambda$getKnownTypes$1(ManifestRecord.Identifier.Type type) {
        return Recipient.self().getStoriesCapability() == Recipient.Capability.SUPPORTED || !type.equals(ManifestRecord.Identifier.Type.STORY_DISTRIBUTION_LIST);
    }

    public static /* synthetic */ Integer lambda$getKnownTypes$2(ManifestRecord.Identifier.Type type) {
        return Integer.valueOf(type.getNumber());
    }

    /* loaded from: classes4.dex */
    public static final class StorageRecordCollection {
        final List<SignalAccountRecord> account = new LinkedList();
        final List<SignalContactRecord> contacts = new LinkedList();
        final List<SignalGroupV1Record> gv1 = new LinkedList();
        final List<SignalGroupV2Record> gv2 = new LinkedList();
        final List<SignalStoryDistributionListRecord> storyDistributionLists = new LinkedList();
        final List<SignalStorageRecord> unknown = new LinkedList();

        StorageRecordCollection(Collection<SignalStorageRecord> collection) {
            for (SignalStorageRecord signalStorageRecord : collection) {
                if (signalStorageRecord.getContact().isPresent()) {
                    this.contacts.add(signalStorageRecord.getContact().get());
                } else if (signalStorageRecord.getGroupV1().isPresent()) {
                    this.gv1.add(signalStorageRecord.getGroupV1().get());
                } else if (signalStorageRecord.getGroupV2().isPresent()) {
                    this.gv2.add(signalStorageRecord.getGroupV2().get());
                } else if (signalStorageRecord.getAccount().isPresent()) {
                    this.account.add(signalStorageRecord.getAccount().get());
                } else if (signalStorageRecord.getStoryDistributionList().isPresent()) {
                    this.storyDistributionLists.add(signalStorageRecord.getStoryDistributionList().get());
                } else if (signalStorageRecord.getId().isUnknown()) {
                    this.unknown.add(signalStorageRecord);
                } else {
                    String str = StorageSyncJob.TAG;
                    Log.w(str, "Bad record! Type is a known value (" + signalStorageRecord.getId().getType() + "), but doesn't have a matching inner record. Dropping it.");
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class MissingGv2MasterKeyError extends Error {
        private MissingGv2MasterKeyError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class MissingRecipientModelError extends Error {
        public MissingRecipientModelError(String str) {
            super(str);
        }
    }

    /* loaded from: classes4.dex */
    public static final class MissingUnknownModelError extends Error {
        public MissingUnknownModelError(String str) {
            super(str);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<StorageSyncJob> {
        public StorageSyncJob create(Job.Parameters parameters, Data data) {
            return new StorageSyncJob(parameters);
        }
    }
}
