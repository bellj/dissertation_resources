package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public final class MarkerJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(MarkerJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public MarkerJob(String str) {
        this(new Job.Parameters.Builder().setQueue(str).build());
    }

    private MarkerJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        Log.i(TAG, String.format("Marker reached in %s queue", getParameters().getQueue()));
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MarkerJob> {
        public MarkerJob create(Job.Parameters parameters, Data data) {
            return new MarkerJob(parameters);
        }
    }
}
