package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.logging.Log;
import org.signal.core.util.tracing.Tracer;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobLogger;
import org.thoughtcrime.securesms.jobmanager.impl.BackoffUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes.dex */
public abstract class BaseJob extends Job {
    private static final String TAG = Log.tag(BaseJob.class);
    private Data outputData;

    protected abstract void onRun() throws Exception;

    protected abstract boolean onShouldRetry(Exception exc);

    protected boolean shouldTrace() {
        return false;
    }

    public BaseJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Job.Result run() {
        if (shouldTrace()) {
            Tracer.getInstance().start(getClass().getSimpleName());
        }
        try {
            try {
                try {
                    onRun();
                    Job.Result success = Job.Result.success(this.outputData);
                    if (shouldTrace()) {
                        Tracer.getInstance().end(getClass().getSimpleName());
                    }
                    return success;
                } catch (RuntimeException e) {
                    Log.e(TAG, "Encountered a fatal exception. Crash imminent.", e);
                    Job.Result fatalFailure = Job.Result.fatalFailure(e);
                    if (shouldTrace()) {
                        Tracer.getInstance().end(getClass().getSimpleName());
                    }
                    return fatalFailure;
                }
            } catch (Exception e2) {
                if (onShouldRetry(e2)) {
                    Log.i(TAG, JobLogger.format(this, "Encountered a retryable exception."), e2);
                    Job.Result retry = Job.Result.retry(getNextRunAttemptBackoff(getRunAttempt() + 1, e2));
                    if (shouldTrace()) {
                        Tracer.getInstance().end(getClass().getSimpleName());
                    }
                    return retry;
                }
                Log.w(TAG, JobLogger.format(this, "Encountered a failing exception."), e2);
                Job.Result failure = Job.Result.failure();
                if (shouldTrace()) {
                    Tracer.getInstance().end(getClass().getSimpleName());
                }
                return failure;
            }
        } catch (Throwable th) {
            if (shouldTrace()) {
                Tracer.getInstance().end(getClass().getSimpleName());
            }
            throw th;
        }
    }

    public long getNextRunAttemptBackoff(int i, Exception exc) {
        return BackoffUtil.exponentialBackoff(i, FeatureFlags.getDefaultMaxBackoff());
    }

    public void setOutputData(Data data) {
        this.outputData = data;
    }

    public void log(String str, String str2) {
        Log.i(str, JobLogger.format(this, str2));
    }

    public void log(String str, String str2, String str3) {
        Log.i(str, JobLogger.format(this, str2, str3));
    }

    public void warn(String str, String str2) {
        warn(str, "", str2, null);
    }

    public void warn(String str, String str2, String str3) {
        warn(str, str2, str3, null);
    }

    public void warn(String str, Throwable th) {
        warn(str, "", th);
    }

    public void warn(String str, String str2, Throwable th) {
        warn(str, "", str2, th);
    }

    public void warn(String str, String str2, String str3, Throwable th) {
        Log.w(str, JobLogger.format(this, str2, str3), th);
    }
}
