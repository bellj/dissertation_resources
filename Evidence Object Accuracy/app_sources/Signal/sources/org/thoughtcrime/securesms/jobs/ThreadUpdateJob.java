package org.thoughtcrime.securesms.jobs;

import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public final class ThreadUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_THREAD_ID;
    private final long threadId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ThreadUpdateJob(long r4) {
        /*
            r3 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "ThreadUpdateJob_"
            r1.append(r2)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            r1 = 2
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxInstancesForQueue(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            r3.<init>(r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.ThreadUpdateJob.<init>(long):void");
    }

    private ThreadUpdateJob(Job.Parameters parameters, long j) {
        super(parameters);
        this.threadId = j;
    }

    public static void enqueue(long j) {
        ApplicationDependencies.getJobManager().add(new ThreadUpdateJob(j));
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("thread_id", this.threadId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        SignalDatabase.threads().update(this.threadId, true);
        ThreadUtil.sleep(1000);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<ThreadUpdateJob> {
        public ThreadUpdateJob create(Job.Parameters parameters, Data data) {
            return new ThreadUpdateJob(parameters, data.getLong("thread_id"));
        }
    }
}
