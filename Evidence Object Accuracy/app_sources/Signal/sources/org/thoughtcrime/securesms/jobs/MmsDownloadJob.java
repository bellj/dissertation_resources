package org.thoughtcrime.securesms.jobs;

import android.net.Uri;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;
import com.google.android.mms.pdu_alt.RetrieveConf;
import j$.util.Optional;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.contactshare.VCardUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.ApnUnavailableException;
import org.thoughtcrime.securesms.mms.CompatMmsConnection;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.MmsRadioException;
import org.thoughtcrime.securesms.mms.PartParser;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class MmsDownloadJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_AUTOMATIC;
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_THREAD_ID;
    private static final String TAG = Log.tag(MmsDownloadJob.class);
    private boolean automatic;
    private long messageId;
    private long threadId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public MmsDownloadJob(long j, long j2, boolean z) {
        this(new Job.Parameters.Builder().setQueue("mms-operation").setMaxAttempts(25).build(), j, j2, z);
    }

    private MmsDownloadJob(Job.Parameters parameters, long j, long j2, boolean z) {
        super(parameters);
        this.messageId = j;
        this.threadId = j2;
        this.automatic = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).putLong("thread_id", this.threadId).putBoolean(KEY_AUTOMATIC, this.automatic).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        if (this.automatic && KeyCachingService.isLocked(this.context)) {
            SignalDatabase.mms().markIncomingNotificationReceived(this.threadId);
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() {
        Throwable e;
        if (SignalStore.account().getE164() != null) {
            MmsDatabase mms = SignalDatabase.mms();
            Optional<MessageDatabase.MmsNotificationInfo> notification = mms.getNotification(this.messageId);
            if (!notification.isPresent()) {
                String str = TAG;
                Log.w(str, "No notification for ID: " + this.messageId);
                return;
            }
            try {
                if (notification.get().getContentLocation() == null) {
                    throw new MmsException("Notification content location was null.");
                } else if (SignalStore.account().isRegistered()) {
                    mms.markDownloadState(this.messageId, 3);
                    String contentLocation = notification.get().getContentLocation();
                    byte[] bArr = new byte[0];
                    try {
                        if (notification.get().getTransactionId() != null) {
                            bArr = notification.get().getTransactionId().getBytes("iso-8859-1");
                        } else {
                            Log.w(TAG, "No transaction ID!");
                        }
                    } catch (UnsupportedEncodingException e2) {
                        Log.w(TAG, e2);
                    }
                    String str2 = TAG;
                    Log.i(str2, "Downloading mms at " + Uri.parse(contentLocation).getHost() + ", subscription ID: " + notification.get().getSubscriptionId());
                    RetrieveConf retrieve = new CompatMmsConnection(this.context).retrieve(contentLocation, bArr, notification.get().getSubscriptionId());
                    if (retrieve != null) {
                        storeRetrievedMms(contentLocation, this.messageId, this.threadId, retrieve, notification.get().getSubscriptionId(), notification.get().getFrom());
                        return;
                    }
                    throw new MmsException("RetrieveConf was null");
                } else {
                    throw new MmsException("Not registered");
                }
            } catch (IOException e3) {
                e = e3;
                Log.w(TAG, e);
                handleDownloadError(this.messageId, this.threadId, 4, this.automatic);
            } catch (ApnUnavailableException e4) {
                Log.w(TAG, e4);
                handleDownloadError(this.messageId, this.threadId, 6, this.automatic);
            } catch (MmsException e5) {
                Log.w(TAG, e5);
                handleDownloadError(this.messageId, this.threadId, 5, this.automatic);
            } catch (MmsRadioException e6) {
                e = e6;
                Log.w(TAG, e);
                handleDownloadError(this.messageId, this.threadId, 4, this.automatic);
            }
        } else {
            throw new NotReadyException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        MmsDatabase mms = SignalDatabase.mms();
        mms.markDownloadState(this.messageId, 4);
        if (this.automatic) {
            mms.markIncomingNotificationReceived(this.threadId);
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(this.threadId));
        }
    }

    private void storeRetrievedMms(String str, long j, long j2, RetrieveConf retrieveConf, int i, RecipientId recipientId) throws MmsException {
        String str2;
        MmsDatabase mms = SignalDatabase.mms();
        Optional empty = Optional.empty();
        HashSet hashSet = new HashSet();
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        RecipientId id = retrieveConf.getFrom() != null ? Recipient.external(this.context, Util.toIsoString(retrieveConf.getFrom().getTextString())).getId() : recipientId != null ? recipientId : null;
        if (retrieveConf.getTo() != null) {
            for (EncodedStringValue encodedStringValue : retrieveConf.getTo()) {
                hashSet.add(Recipient.external(this.context, Util.toIsoString(encodedStringValue.getTextString())).getId());
            }
        }
        if (retrieveConf.getCc() != null) {
            for (EncodedStringValue encodedStringValue2 : retrieveConf.getCc()) {
                hashSet.add(Recipient.external(this.context, Util.toIsoString(encodedStringValue2.getTextString())).getId());
            }
        }
        if (id != null) {
            hashSet.add(id);
        }
        hashSet.add(Recipient.self().getId());
        if (retrieveConf.getBody() != null) {
            String messageText = PartParser.getMessageText(retrieveConf.getBody());
            PduBody supportedMediaParts = PartParser.getSupportedMediaParts(retrieveConf.getBody());
            for (int i2 = 0; i2 < supportedMediaParts.getPartsNum(); i2++) {
                PduPart part = supportedMediaParts.getPart(i2);
                if (part.getData() != null) {
                    if (Util.toIsoString(part.getContentType()).toLowerCase().equals(MediaUtil.VCARD)) {
                        linkedList2.addAll(VCardUtil.parseContacts(new String(part.getData())));
                    } else {
                        linkedList.add(new UriAttachment(BlobProvider.getInstance().forData(part.getData()).createForSingleUseInMemory(), Util.toIsoString(part.getContentType()), 0, (long) part.getData().length, part.getName() != null ? Util.toIsoString(part.getName()) : null, false, false, false, false, null, null, null, null, null));
                    }
                }
            }
            str2 = messageText;
        } else {
            str2 = null;
        }
        if (hashSet.size() > 2) {
            empty = Optional.of(SignalDatabase.groups().getOrCreateMmsGroupForMembers(new ArrayList(hashSet)));
        }
        Optional<MessageDatabase.InsertResult> insertMessageInbox = mms.insertMessageInbox(new IncomingMediaMessage(id, empty, str2, TimeUnit.SECONDS.toMillis(retrieveConf.getDate()), -1, System.currentTimeMillis(), linkedList, i, 0, false, false, false, Optional.of(linkedList2)), str, j2);
        if (insertMessageInbox.isPresent()) {
            mms.deleteMessage(j);
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertMessageInbox.get().getThreadId()));
        }
    }

    private void handleDownloadError(long j, long j2, int i, boolean z) {
        MmsDatabase mms = SignalDatabase.mms();
        mms.markDownloadState(j, (long) i);
        if (z) {
            mms.markIncomingNotificationReceived(j2);
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(j2));
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MmsDownloadJob> {
        public MmsDownloadJob create(Job.Parameters parameters, Data data) {
            return new MmsDownloadJob(parameters, data.getLong("message_id"), data.getLong("thread_id"), data.getBoolean(MmsDownloadJob.KEY_AUTOMATIC));
        }
    }

    /* loaded from: classes4.dex */
    private static class NotReadyException extends RuntimeException {
        private NotReadyException() {
        }
    }
}
