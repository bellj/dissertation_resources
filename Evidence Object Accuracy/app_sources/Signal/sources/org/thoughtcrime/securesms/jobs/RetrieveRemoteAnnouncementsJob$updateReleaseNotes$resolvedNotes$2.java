package org.thoughtcrime.securesms.jobs;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob;
import org.thoughtcrime.securesms.util.LocaleFeatureFlags;

/* compiled from: RetrieveRemoteAnnouncementsJob.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;", "invoke", "(Lorg/thoughtcrime/securesms/jobs/RetrieveRemoteAnnouncementsJob$ReleaseNote;)Ljava/lang/Boolean;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RetrieveRemoteAnnouncementsJob$updateReleaseNotes$resolvedNotes$2 extends Lambda implements Function1<RetrieveRemoteAnnouncementsJob.ReleaseNote, Boolean> {
    public static final RetrieveRemoteAnnouncementsJob$updateReleaseNotes$resolvedNotes$2 INSTANCE = new RetrieveRemoteAnnouncementsJob$updateReleaseNotes$resolvedNotes$2();

    RetrieveRemoteAnnouncementsJob$updateReleaseNotes$resolvedNotes$2() {
        super(1);
    }

    public final Boolean invoke(RetrieveRemoteAnnouncementsJob.ReleaseNote releaseNote) {
        Intrinsics.checkNotNullParameter(releaseNote, "it");
        return Boolean.valueOf(releaseNote.getCountries() == null || LocaleFeatureFlags.shouldShowReleaseNote(releaseNote.getUuid(), releaseNote.getCountries()));
    }
}
