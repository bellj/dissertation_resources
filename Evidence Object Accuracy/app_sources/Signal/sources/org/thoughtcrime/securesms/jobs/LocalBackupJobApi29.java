package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.net.Uri;
import androidx.documentfile.provider.DocumentFile;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.backup.BackupFileIOError;
import org.thoughtcrime.securesms.backup.BackupPassphrase;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.backup.FullBackupExporter;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.service.GenericForegroundService;
import org.thoughtcrime.securesms.service.NotificationController;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public final class LocalBackupJobApi29 extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(LocalBackupJobApi29.class);
    public static final String TEMP_BACKUP_FILE_PREFIX;
    public static final String TEMP_BACKUP_FILE_SUFFIX;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public LocalBackupJobApi29(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        String str = TAG;
        Log.i(str, "Executing backup job...");
        BackupFileIOError.clearNotification(this.context);
        if (BackupUtil.isUserSelectionRequired(this.context)) {
            Uri signalBackupDirectory = SignalStore.settings().getSignalBackupDirectory();
            if (signalBackupDirectory == null || signalBackupDirectory.getPath() == null) {
                throw new IOException("Backup Directory has not been selected!");
            }
            ProgressUpdater progressUpdater = new ProgressUpdater();
            try {
                Context context = this.context;
                NotificationController startForegroundTask = GenericForegroundService.startForegroundTask(context, context.getString(R.string.LocalBackupJob_creating_signal_backup), NotificationChannels.BACKUPS, R.drawable.ic_signal_backup);
                progressUpdater.setNotification(startForegroundTask);
                EventBus.getDefault().register(progressUpdater);
                startForegroundTask.setIndeterminateProgress();
                String str2 = BackupPassphrase.get(this.context);
                DocumentFile fromTreeUri = DocumentFile.fromTreeUri(this.context, signalBackupDirectory);
                Locale locale = Locale.US;
                String format = String.format("signal-%s.backup", new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", locale).format(new Date()));
                if (fromTreeUri == null || !fromTreeUri.canWrite()) {
                    BackupFileIOError.ACCESS_ERROR.postNotification(this.context);
                    throw new IOException("Cannot write to backup directory location.");
                }
                deleteOldTemporaryBackups(fromTreeUri);
                if (fromTreeUri.findFile(format) == null) {
                    String format2 = String.format(locale, "%s%s%s", ".backup", UUID.randomUUID(), ".tmp");
                    DocumentFile createFile = fromTreeUri.createFile(MediaUtil.OCTET, format2);
                    if (createFile != null) {
                        try {
                            if (str2 != null) {
                                try {
                                    Context context2 = this.context;
                                    FullBackupExporter.export(context2, AttachmentSecretProvider.getInstance(context2).getOrCreateAttachmentSecret(), SignalDatabase.getBackupDatabase(), createFile, str2, new FullBackupExporter.BackupCancellationSignal() { // from class: org.thoughtcrime.securesms.jobs.LocalBackupJobApi29$$ExternalSyntheticLambda0
                                        @Override // org.thoughtcrime.securesms.backup.FullBackupExporter.BackupCancellationSignal
                                        public final boolean isCanceled() {
                                            return LocalBackupJobApi29.this.isCanceled();
                                        }
                                    });
                                    if (createFile.renameTo(format)) {
                                        DocumentFile findFile = fromTreeUri.findFile(format2);
                                        if (findFile != null) {
                                            if (findFile.delete()) {
                                                Log.w(str, "Backup failed. Deleted temp file");
                                            } else {
                                                Log.w(str, "Backup failed. Failed to delete temp file " + format2);
                                            }
                                        }
                                        BackupUtil.deleteOldBackups();
                                        startForegroundTask.close();
                                        return;
                                    }
                                    Log.w(str, "Failed to rename temp file");
                                    throw new IOException("Renaming temporary backup file failed!");
                                } catch (FullBackupExporter.BackupCanceledException e) {
                                    Log.w(TAG, "Backup cancelled");
                                    throw e;
                                } catch (IOException e2) {
                                    Log.w(TAG, "Error during backup!", e2);
                                    BackupFileIOError.postNotificationForException(this.context, e2, getRunAttempt());
                                    throw e2;
                                }
                            } else {
                                throw new IOException("Backup password is null");
                            }
                        } catch (Throwable th) {
                            DocumentFile findFile2 = fromTreeUri.findFile(format2);
                            if (findFile2 != null) {
                                if (findFile2.delete()) {
                                    Log.w(TAG, "Backup failed. Deleted temp file");
                                } else {
                                    String str3 = TAG;
                                    Log.w(str3, "Backup failed. Failed to delete temp file " + format2);
                                }
                            }
                            throw th;
                        }
                    } else {
                        throw new IOException("Failed to create temporary backup file.");
                    }
                } else {
                    throw new IOException("Backup file already exists!");
                }
            } finally {
                EventBus.getDefault().unregister(progressUpdater);
                progressUpdater.setNotification(null);
            }
        } else {
            throw new IOException("Wrong backup job!");
        }
    }

    private static void deleteOldTemporaryBackups(DocumentFile documentFile) {
        String name;
        DocumentFile[] listFiles = documentFile.listFiles();
        for (DocumentFile documentFile2 : listFiles) {
            if (documentFile2.isFile() && (name = documentFile2.getName()) != null && name.startsWith(".backup") && name.endsWith(".tmp")) {
                if (documentFile2.delete()) {
                    Log.w(TAG, "Deleted old temporary backup file");
                } else {
                    Log.w(TAG, "Could not delete old temporary backup file");
                }
            }
        }
    }

    /* loaded from: classes.dex */
    private static class ProgressUpdater {
        private NotificationController notification;

        private ProgressUpdater() {
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onEvent(FullBackupBase.BackupEvent backupEvent) {
            if (this.notification == null || backupEvent.getType() != FullBackupBase.BackupEvent.Type.PROGRESS) {
                return;
            }
            if (backupEvent.getEstimatedTotalCount() == 0) {
                this.notification.setIndeterminateProgress();
            } else {
                this.notification.setProgress(100, (long) ((int) backupEvent.getCompletionPercentage()));
            }
        }

        public void setNotification(NotificationController notificationController) {
            this.notification = notificationController;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<LocalBackupJobApi29> {
        public LocalBackupJobApi29 create(Job.Parameters parameters, Data data) {
            return new LocalBackupJobApi29(parameters);
        }
    }
}
