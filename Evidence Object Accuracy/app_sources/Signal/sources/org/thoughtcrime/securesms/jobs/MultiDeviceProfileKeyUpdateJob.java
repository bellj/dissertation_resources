package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.multidevice.ContactsMessage;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceContact;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsOutputStream;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceProfileKeyUpdateJob extends BaseJob {
    public static String KEY;
    private static final String TAG = Log.tag(MultiDeviceProfileKeyUpdateJob.class);

    public MultiDeviceProfileKeyUpdateJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue("MultiDeviceProfileKeyUpdateJob").setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private MultiDeviceProfileKeyUpdateJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device...");
        } else {
            Optional of = Optional.of(ProfileKeyUtil.getSelfProfileKey());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DeviceContactsOutputStream deviceContactsOutputStream = new DeviceContactsOutputStream(byteArrayOutputStream);
            deviceContactsOutputStream.write(new DeviceContact(RecipientUtil.toSignalServiceAddress(this.context, Recipient.self()), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), of, false, Optional.empty(), Optional.empty(), false));
            deviceContactsOutputStream.close();
            ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forContacts(new ContactsMessage(SignalServiceAttachment.newStreamBuilder().withStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())).withContentType(MediaUtil.OCTET).withLength((long) byteArrayOutputStream.toByteArray().length).build(), false)), UnidentifiedAccessUtil.getAccessForSync(this.context));
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (!(exc instanceof ServerRejectedException) && (exc instanceof PushNetworkException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Profile key sync failed!");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceProfileKeyUpdateJob> {
        public MultiDeviceProfileKeyUpdateJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceProfileKeyUpdateJob(parameters);
        }
    }
}
