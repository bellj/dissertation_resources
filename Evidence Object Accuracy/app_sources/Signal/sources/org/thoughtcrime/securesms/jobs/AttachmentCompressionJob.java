package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.media.MediaDataSource;
import android.net.Uri;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.MediaStream;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.service.GenericForegroundService;
import org.thoughtcrime.securesms.service.NotificationController;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.BitmapDecodingException;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.ImageCompressionUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MemoryFileDescriptor;
import org.thoughtcrime.securesms.video.InMemoryTranscoder;
import org.thoughtcrime.securesms.video.StreamingTranscoder;
import org.thoughtcrime.securesms.video.TranscoderCancelationSignal;
import org.thoughtcrime.securesms.video.TranscoderOptions;
import org.thoughtcrime.securesms.video.VideoSourceException;
import org.thoughtcrime.securesms.video.videoconverter.EncodingException;

/* loaded from: classes4.dex */
public final class AttachmentCompressionJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_MMS;
    private static final String KEY_MMS_SUBSCRIPTION_ID;
    private static final String KEY_ROW_ID;
    private static final String KEY_UNIQUE_ID;
    private static final String TAG = Log.tag(AttachmentCompressionJob.class);
    private final AttachmentId attachmentId;
    private final boolean mms;
    private final int mmsSubscriptionId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public static AttachmentCompressionJob fromAttachment(DatabaseAttachment databaseAttachment, boolean z, int i) {
        return new AttachmentCompressionJob(databaseAttachment.getAttachmentId(), MediaUtil.isVideo(databaseAttachment) && MediaConstraints.isVideoTranscodeAvailable(), z, i);
    }

    private AttachmentCompressionJob(AttachmentId attachmentId, boolean z, boolean z2, int i) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).setQueue(z ? "VIDEO_TRANSCODE" : "GENERIC_TRANSCODE").build(), attachmentId, z2, i);
    }

    private AttachmentCompressionJob(Job.Parameters parameters, AttachmentId attachmentId, boolean z, int i) {
        super(parameters);
        this.attachmentId = attachmentId;
        this.mms = z;
        this.mmsSubscriptionId = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong(KEY_ROW_ID, this.attachmentId.getRowId()).putLong("unique_id", this.attachmentId.getUniqueId()).putBoolean("mms", this.mms).putInt(KEY_MMS_SUBSCRIPTION_ID, this.mmsSubscriptionId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        MediaConstraints mediaConstraints;
        String str = TAG;
        Log.d(str, "Running for: " + this.attachmentId);
        AttachmentDatabase attachments = SignalDatabase.attachments();
        DatabaseAttachment attachment = attachments.getAttachment(this.attachmentId);
        if (attachment == null) {
            throw new UndeliverableMessageException("Cannot find the specified attachment.");
        } else if (attachment.getTransformProperties().shouldSkipTransform()) {
            Log.i(str, "Skipping at the direction of the TransformProperties.");
        } else {
            if (this.mms) {
                mediaConstraints = MediaConstraints.getMmsMediaConstraints(this.mmsSubscriptionId);
            } else {
                mediaConstraints = MediaConstraints.getPushMediaConstraints(SentMediaQuality.fromCode(attachment.getTransformProperties().getSentMediaQuality()));
            }
            compress(attachments, mediaConstraints, attachment);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    private void compress(AttachmentDatabase attachmentDatabase, MediaConstraints mediaConstraints, DatabaseAttachment databaseAttachment) throws UndeliverableMessageException {
        try {
            if (databaseAttachment.isSticker()) {
                Log.d(TAG, "Sticker, not compressing.");
            } else if (MediaUtil.isVideo(databaseAttachment)) {
                Log.i(TAG, "Compressing video.");
                if (!mediaConstraints.isSatisfied(this.context, transcodeVideoIfNeededToDatabase(this.context, attachmentDatabase, databaseAttachment, mediaConstraints, EventBus.getDefault(), new TranscoderCancelationSignal() { // from class: org.thoughtcrime.securesms.jobs.AttachmentCompressionJob$$ExternalSyntheticLambda2
                    @Override // org.thoughtcrime.securesms.video.TranscoderCancelationSignal
                    public final boolean isCanceled() {
                        return AttachmentCompressionJob.this.isCanceled();
                    }
                }))) {
                    throw new UndeliverableMessageException("Size constraints could not be met on video!");
                }
            } else if (mediaConstraints.canResize(databaseAttachment)) {
                Log.i(TAG, "Compressing image.");
                MediaStream compressImage = compressImage(this.context, databaseAttachment, mediaConstraints);
                try {
                    attachmentDatabase.updateAttachmentData(databaseAttachment, compressImage, false);
                    if (compressImage != null) {
                        compressImage.close();
                    }
                    attachmentDatabase.markAttachmentAsTransformed(this.attachmentId);
                } catch (Throwable th) {
                    if (compressImage != null) {
                        try {
                            compressImage.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                    }
                    throw th;
                }
            } else if (mediaConstraints.isSatisfied(this.context, databaseAttachment)) {
                Log.i(TAG, "Not compressing.");
                attachmentDatabase.markAttachmentAsTransformed(this.attachmentId);
            } else {
                throw new UndeliverableMessageException("Size constraints could not be met!");
            }
        } catch (IOException | MmsException e) {
            throw new UndeliverableMessageException(e);
        }
    }

    private static DatabaseAttachment transcodeVideoIfNeededToDatabase(Context context, AttachmentDatabase attachmentDatabase, DatabaseAttachment databaseAttachment, MediaConstraints mediaConstraints, EventBus eventBus, TranscoderCancelationSignal transcoderCancelationSignal) throws UndeliverableMessageException {
        Throwable e;
        NotificationController startForegroundTask;
        Throwable th;
        Throwable th2;
        boolean z;
        Throwable th3;
        AttachmentDatabase.TransformProperties transformProperties = databaseAttachment.getTransformProperties();
        if (MediaConstraints.isVideoTranscodeAvailable()) {
            boolean z2 = false;
            try {
                try {
                    startForegroundTask = GenericForegroundService.startForegroundTask(context, context.getString(R.string.AttachmentUploadJob_compressing_video_start));
                } catch (MemoryFileDescriptor.MemoryFileException e2) {
                    e = e2;
                } catch (VideoSourceException e3) {
                    e = e3;
                } catch (EncodingException e4) {
                    e = e4;
                }
                try {
                    startForegroundTask.setIndeterminateProgress();
                    MediaDataSource mediaDataSourceFor = attachmentDatabase.mediaDataSourceFor(databaseAttachment.getAttachmentId());
                    try {
                        if (mediaDataSourceFor != null) {
                            boolean z3 = !transformProperties.isVideoEdited();
                            TranscoderOptions transcoderOptions = null;
                            try {
                                if (transformProperties.isVideoTrim()) {
                                    z = z3;
                                    try {
                                        transcoderOptions = new TranscoderOptions(transformProperties.getVideoTrimStartTimeUs(), transformProperties.getVideoTrimEndTimeUs());
                                    } catch (Throwable th4) {
                                        th3 = th4;
                                        th2 = th3;
                                        z2 = z;
                                        if (mediaDataSourceFor != null) {
                                            mediaDataSourceFor.close();
                                        }
                                        throw th2;
                                    }
                                } else {
                                    z = z3;
                                }
                                try {
                                    try {
                                    } catch (Throwable th5) {
                                        th = th5;
                                        z2 = z;
                                        if (startForegroundTask != null) {
                                            try {
                                                startForegroundTask.close();
                                            } catch (Throwable th6) {
                                                th.addSuppressed(th6);
                                            }
                                        }
                                        throw th;
                                    }
                                } catch (MemoryFileDescriptor.MemoryFileException | VideoSourceException | EncodingException e5) {
                                    e = e5;
                                    z2 = z;
                                    if (databaseAttachment.getSize() > ((long) mediaConstraints.getVideoMaxSize(context))) {
                                        throw new UndeliverableMessageException("Duration not found, attachment too large to skip transcode", e);
                                    } else if (z2) {
                                        Log.w(TAG, "Problem with video source, but video small enough to skip transcode", e);
                                        return databaseAttachment;
                                    } else {
                                        throw new UndeliverableMessageException("Failed to transcode and cannot skip due to editing", e);
                                    }
                                }
                                if (!FeatureFlags.useStreamingVideoMuxer() && MemoryFileDescriptor.supported()) {
                                    InMemoryTranscoder inMemoryTranscoder = new InMemoryTranscoder(context, mediaDataSourceFor, transcoderOptions, (long) mediaConstraints.getCompressedVideoMaxSize(context));
                                    if (inMemoryTranscoder.isTranscodeRequired()) {
                                        Log.i(TAG, "Compressing with android in-memory muxer");
                                        MediaStream transcode = inMemoryTranscoder.transcode(new InMemoryTranscoder.Progress(eventBus, databaseAttachment) { // from class: org.thoughtcrime.securesms.jobs.AttachmentCompressionJob$$ExternalSyntheticLambda1
                                            public final /* synthetic */ EventBus f$1;
                                            public final /* synthetic */ DatabaseAttachment f$2;

                                            {
                                                this.f$1 = r2;
                                                this.f$2 = r3;
                                            }

                                            @Override // org.thoughtcrime.securesms.video.InMemoryTranscoder.Progress
                                            public final void onProgress(int i) {
                                                AttachmentCompressionJob.$r8$lambda$XPNNBR9zjF4EM2L3js0sjsPcghI(NotificationController.this, this.f$1, this.f$2, i);
                                            }
                                        }, transcoderCancelationSignal);
                                        attachmentDatabase.updateAttachmentData(databaseAttachment, transcode, true);
                                        if (transcode != null) {
                                            transcode.close();
                                        }
                                        attachmentDatabase.markAttachmentAsTransformed(databaseAttachment.getAttachmentId());
                                        DatabaseAttachment attachment = attachmentDatabase.getAttachment(databaseAttachment.getAttachmentId());
                                        Objects.requireNonNull(attachment);
                                        inMemoryTranscoder.close();
                                        mediaDataSourceFor.close();
                                        startForegroundTask.close();
                                        return attachment;
                                    }
                                    Log.i(TAG, "Transcode was not required (in-memory transcoder)");
                                    inMemoryTranscoder.close();
                                    mediaDataSourceFor.close();
                                    startForegroundTask.close();
                                    return databaseAttachment;
                                }
                                StreamingTranscoder streamingTranscoder = new StreamingTranscoder(mediaDataSourceFor, transcoderOptions, (long) mediaConstraints.getCompressedVideoMaxSize(context));
                                if (streamingTranscoder.isTranscodeRequired()) {
                                    String str = TAG;
                                    Log.i(str, "Compressing with streaming muxer");
                                    AttachmentSecret orCreateAttachmentSecret = AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret();
                                    File newFile = SignalDatabase.attachments().newFile();
                                    newFile.deleteOnExit();
                                    OutputStream outputStream = (OutputStream) ModernEncryptingPartOutputStream.createFor(orCreateAttachmentSecret, newFile, true).second;
                                    streamingTranscoder.transcode(new StreamingTranscoder.Progress(eventBus, databaseAttachment) { // from class: org.thoughtcrime.securesms.jobs.AttachmentCompressionJob$$ExternalSyntheticLambda0
                                        public final /* synthetic */ EventBus f$1;
                                        public final /* synthetic */ DatabaseAttachment f$2;

                                        {
                                            this.f$1 = r2;
                                            this.f$2 = r3;
                                        }

                                        @Override // org.thoughtcrime.securesms.video.StreamingTranscoder.Progress
                                        public final void onProgress(int i) {
                                            AttachmentCompressionJob.$r8$lambda$uvqKbiytp8yPeDqeI0l5X4B2uq8(NotificationController.this, this.f$1, this.f$2, i);
                                        }
                                    }, outputStream, transcoderCancelationSignal);
                                    if (outputStream != null) {
                                        outputStream.close();
                                    }
                                    MediaStream mediaStream = new MediaStream(ModernDecryptingPartInputStream.createFor(orCreateAttachmentSecret, newFile, 0), "video/mp4", 0, 0);
                                    attachmentDatabase.updateAttachmentData(databaseAttachment, mediaStream, true);
                                    mediaStream.close();
                                    if (!newFile.delete()) {
                                        Log.w(str, "Failed to delete temp file");
                                    }
                                    attachmentDatabase.markAttachmentAsTransformed(databaseAttachment.getAttachmentId());
                                    DatabaseAttachment attachment2 = attachmentDatabase.getAttachment(databaseAttachment.getAttachmentId());
                                    Objects.requireNonNull(attachment2);
                                    mediaDataSourceFor.close();
                                    startForegroundTask.close();
                                    return attachment2;
                                }
                                Log.i(TAG, "Transcode was not required");
                                mediaDataSourceFor.close();
                                startForegroundTask.close();
                                return databaseAttachment;
                            } catch (Throwable th7) {
                                th3 = th7;
                                z = z3;
                            }
                        } else {
                            throw new UndeliverableMessageException("Cannot get media data source for attachment.");
                        }
                    } catch (Throwable th8) {
                        th2 = th8;
                    }
                } catch (Throwable th9) {
                    th = th9;
                }
            } catch (IOException | MmsException e6) {
                throw new UndeliverableMessageException("Failed to transcode", e6);
            }
        } else if (!transformProperties.isVideoEdited()) {
            return databaseAttachment;
        } else {
            throw new UndeliverableMessageException("Video edited, but transcode is not available");
        }
    }

    public static /* synthetic */ void lambda$transcodeVideoIfNeededToDatabase$0(NotificationController notificationController, EventBus eventBus, DatabaseAttachment databaseAttachment, int i) {
        long j = (long) i;
        notificationController.setProgress(100, j);
        eventBus.postSticky(new PartProgressEvent(databaseAttachment, PartProgressEvent.Type.COMPRESSION, 100, j));
    }

    public static /* synthetic */ void lambda$transcodeVideoIfNeededToDatabase$1(NotificationController notificationController, EventBus eventBus, DatabaseAttachment databaseAttachment, int i) {
        long j = (long) i;
        notificationController.setProgress(100, j);
        eventBus.postSticky(new PartProgressEvent(databaseAttachment, PartProgressEvent.Type.COMPRESSION, 100, j));
    }

    private static MediaStream compressImage(Context context, Attachment attachment, MediaConstraints mediaConstraints) throws UndeliverableMessageException {
        Uri uri = attachment.getUri();
        if (uri != null) {
            ImageCompressionUtil.Result result = null;
            try {
                for (int i : mediaConstraints.getImageDimensionTargets(context)) {
                    result = ImageCompressionUtil.compressWithinConstraints(context, attachment.getContentType(), new DecryptableStreamUriLoader.DecryptableUri(uri), i, mediaConstraints.getImageMaxSize(context), mediaConstraints.getImageCompressionQualitySetting(context));
                    if (result != null) {
                        break;
                    }
                }
                if (result != null) {
                    return new MediaStream(new ByteArrayInputStream(result.getData()), result.getMimeType(), result.getWidth(), result.getHeight());
                }
                throw new UndeliverableMessageException("Somehow couldn't meet the constraints!");
            } catch (BitmapDecodingException e) {
                throw new UndeliverableMessageException(e);
            }
        } else {
            throw new UndeliverableMessageException("No attachment URI!");
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<AttachmentCompressionJob> {
        public AttachmentCompressionJob create(Job.Parameters parameters, Data data) {
            return new AttachmentCompressionJob(parameters, new AttachmentId(data.getLong(AttachmentCompressionJob.KEY_ROW_ID), data.getLong("unique_id")), data.getBoolean("mms"), data.getInt(AttachmentCompressionJob.KEY_MMS_SUBSCRIPTION_ID));
        }
    }
}
