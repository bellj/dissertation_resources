package org.thoughtcrime.securesms.jobs;

import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsMapper;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.messages.multidevice.ContactsMessage;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceContact;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsOutputStream;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.api.util.InvalidNumberException;

/* loaded from: classes.dex */
public class MultiDeviceContactUpdateJob extends BaseJob {
    private static final long FULL_SYNC_TIME = TimeUnit.HOURS.toMillis(6);
    public static final String KEY;
    private static final String KEY_FORCE_SYNC;
    private static final String KEY_RECIPIENT;
    private static final String TAG = Log.tag(MultiDeviceContactUpdateJob.class);
    private boolean forceSync;
    private RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* synthetic */ MultiDeviceContactUpdateJob(Job.Parameters parameters, RecipientId recipientId, boolean z, AnonymousClass1 r4) {
        this(parameters, recipientId, z);
    }

    public MultiDeviceContactUpdateJob() {
        this(false);
    }

    public MultiDeviceContactUpdateJob(boolean z) {
        this(null, z);
    }

    public MultiDeviceContactUpdateJob(RecipientId recipientId) {
        this(recipientId, true);
    }

    public MultiDeviceContactUpdateJob(RecipientId recipientId, boolean z) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build(), recipientId, z);
    }

    private MultiDeviceContactUpdateJob(Job.Parameters parameters, RecipientId recipientId, boolean z) {
        super(parameters);
        this.recipientId = recipientId;
        this.forceSync = z;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data.Builder builder = new Data.Builder();
        RecipientId recipientId = this.recipientId;
        return builder.putString("recipient", recipientId != null ? recipientId.serialize() : null).putBoolean(KEY_FORCE_SYNC, this.forceSync).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException, NetworkException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else if (SignalStore.account().isLinkedDevice()) {
            Log.i(TAG, "Not primary device, aborting...");
        } else {
            RecipientId recipientId = this.recipientId;
            if (recipientId == null) {
                generateFullContactUpdate();
            } else {
                generateSingleContactUpdate(recipientId);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00b4 A[Catch: InvalidNumberException -> 0x0108, all -> 0x0106, TryCatch #1 {InvalidNumberException -> 0x0108, blocks: (B:3:0x0008, B:5:0x001d, B:8:0x0041, B:10:0x0077, B:14:0x0080, B:15:0x0086, B:17:0x00b4, B:18:0x00c1, B:19:0x00c5), top: B:28:0x0008, outer: #0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00c1 A[Catch: InvalidNumberException -> 0x0108, all -> 0x0106, TryCatch #1 {InvalidNumberException -> 0x0108, blocks: (B:3:0x0008, B:5:0x001d, B:8:0x0041, B:10:0x0077, B:14:0x0080, B:15:0x0086, B:17:0x00b4, B:18:0x00c1, B:19:0x00c5), top: B:28:0x0008, outer: #0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void generateSingleContactUpdate(org.thoughtcrime.securesms.recipients.RecipientId r21) throws java.io.IOException, org.whispersystems.signalservice.api.crypto.UntrustedIdentityException, org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.NetworkException {
        /*
            r20 = this;
            r7 = r20
            r0 = r21
            org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob$WriteDetails r8 = r20.createTempFile()
            org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsOutputStream r1 = new org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsOutputStream     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.io.OutputStream r2 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.WriteDetails.access$000(r8)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r1.<init>(r2)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.recipients.Recipient r2 = org.thoughtcrime.securesms.recipients.Recipient.resolved(r21)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.database.RecipientDatabase$RegisteredState r3 = r2.getRegistered()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.database.RecipientDatabase$RegisteredState r4 = org.thoughtcrime.securesms.database.RecipientDatabase.RegisteredState.NOT_REGISTERED     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            if (r3 != r4) goto L_0x0041
            java.lang.String r1 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.TAG     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r2.<init>()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r2.append(r0)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.String r0 = " not registered!"
            r2.append(r0)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.String r0 = r2.toString()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.signal.core.util.logging.Log.w(r1, r0)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.providers.BlobProvider r0 = org.thoughtcrime.securesms.providers.BlobProvider.getInstance()
            android.content.Context r1 = r7.context
            android.net.Uri r2 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.WriteDetails.access$100(r8)
            r0.delete(r1, r2)
            return
        L_0x0041:
            org.thoughtcrime.securesms.crypto.storage.SignalServiceDataStoreImpl r3 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getProtocolStore()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.crypto.storage.SignalServiceAccountDataStoreImpl r3 = r3.aci()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore r3 = r3.identities()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.recipients.RecipientId r4 = r2.getId()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r3 = r3.getIdentityRecord(r4)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r14 = r7.getVerifiedMessage(r2, r3)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.database.ThreadDatabase r3 = org.thoughtcrime.securesms.database.SignalDatabase.threads()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.util.Map r3 = r3.getInboxPositions()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.database.ThreadDatabase r4 = org.thoughtcrime.securesms.database.SignalDatabase.threads()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.util.Set r4 = r4.getArchivedRecipients()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.whispersystems.signalservice.api.messages.multidevice.DeviceContact r5 = new org.whispersystems.signalservice.api.messages.multidevice.DeviceContact     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            android.content.Context r6 = r7.context     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.whispersystems.signalservice.api.push.SignalServiceAddress r10 = org.thoughtcrime.securesms.recipients.RecipientUtil.toSignalServiceAddress(r6, r2)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            boolean r6 = r2.isGroup()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            if (r6 != 0) goto L_0x0080
            boolean r6 = r2.isSystemContact()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            if (r6 == 0) goto L_0x007e
            goto L_0x0080
        L_0x007e:
            r6 = 0
            goto L_0x0086
        L_0x0080:
            android.content.Context r6 = r7.context     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.String r6 = r2.getDisplayName(r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
        L_0x0086:
            j$.util.Optional r11 = j$.util.Optional.ofNullable(r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            android.net.Uri r6 = r2.getContactUri()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r12 = r7.getSystemAvatar(r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.conversation.colors.ChatColors r6 = r2.getChatColors()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.color.MaterialColor r6 = org.thoughtcrime.securesms.conversation.colors.ChatColorsMapper.getMaterialColor(r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.String r6 = r6.serialize()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r13 = j$.util.Optional.of(r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            byte[] r6 = r2.getProfileKey()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r15 = org.thoughtcrime.securesms.crypto.ProfileKeyUtil.profileKeyOptional(r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            boolean r16 = r2.isBlocked()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            int r6 = r2.getExpiresInSeconds()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            if (r6 <= 0) goto L_0x00c1
            int r2 = r2.getExpiresInSeconds()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r2 = j$.util.Optional.of(r2)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            goto L_0x00c5
        L_0x00c1:
            j$.util.Optional r2 = j$.util.Optional.empty()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
        L_0x00c5:
            r17 = r2
            java.lang.Object r2 = r3.get(r0)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            j$.util.Optional r18 = j$.util.Optional.ofNullable(r2)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            boolean r19 = r4.contains(r0)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r9 = r5
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r1.write(r5)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r1.close()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.providers.BlobProvider r0 = org.thoughtcrime.securesms.providers.BlobProvider.getInstance()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            android.content.Context r1 = r7.context     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            android.net.Uri r2 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.WriteDetails.access$100(r8)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            long r4 = r0.calculateFileSize(r1, r2)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.whispersystems.signalservice.api.SignalServiceMessageSender r2 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getSignalServiceMessageSender()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            org.thoughtcrime.securesms.providers.BlobProvider r0 = org.thoughtcrime.securesms.providers.BlobProvider.getInstance()     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            android.content.Context r1 = r7.context     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            android.net.Uri r3 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.WriteDetails.access$100(r8)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            java.io.InputStream r3 = r0.getStream(r1, r3)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            r6 = 0
            r1 = r20
            r1.sendUpdate(r2, r3, r4, r6)     // Catch: InvalidNumberException -> 0x0108, all -> 0x0106
            goto L_0x010e
        L_0x0106:
            r0 = move-exception
            goto L_0x011c
        L_0x0108:
            r0 = move-exception
            java.lang.String r1 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.TAG     // Catch: all -> 0x0106
            org.signal.core.util.logging.Log.w(r1, r0)     // Catch: all -> 0x0106
        L_0x010e:
            org.thoughtcrime.securesms.providers.BlobProvider r0 = org.thoughtcrime.securesms.providers.BlobProvider.getInstance()
            android.content.Context r1 = r7.context
            android.net.Uri r2 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.WriteDetails.access$100(r8)
            r0.delete(r1, r2)
            return
        L_0x011c:
            org.thoughtcrime.securesms.providers.BlobProvider r1 = org.thoughtcrime.securesms.providers.BlobProvider.getInstance()
            android.content.Context r2 = r7.context
            android.net.Uri r3 = org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.WriteDetails.access$100(r8)
            r1.delete(r2, r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob.generateSingleContactUpdate(org.thoughtcrime.securesms.recipients.RecipientId):void");
    }

    private void generateFullContactUpdate() throws IOException, UntrustedIdentityException, NetworkException {
        WriteDetails createTempFile;
        boolean isForegrounded = ApplicationDependencies.getAppForegroundObserver().isForegrounded();
        long currentTimeMillis = System.currentTimeMillis() - TextSecurePreferences.getLastFullContactSyncTime(this.context);
        String str = TAG;
        Log.d(str, "Requesting a full contact sync. forced = " + this.forceSync + ", appVisible = " + isForegrounded + ", timeSinceLastSync = " + currentTimeMillis + " ms");
        if (this.forceSync || isForegrounded || currentTimeMillis >= FULL_SYNC_TIME) {
            try {
                TextSecurePreferences.setLastFullContactSyncTime(this.context, System.currentTimeMillis());
                TextSecurePreferences.setNeedsFullContactSync(this.context, false);
                createTempFile = createTempFile();
                try {
                    DeviceContactsOutputStream deviceContactsOutputStream = new DeviceContactsOutputStream(createTempFile.outputStream);
                    List<Recipient> recipientsForMultiDeviceSync = SignalDatabase.recipients().getRecipientsForMultiDeviceSync();
                    Map<RecipientId, Integer> inboxPositions = SignalDatabase.threads().getInboxPositions();
                    Set<RecipientId> archivedRecipients = SignalDatabase.threads().getArchivedRecipients();
                    for (Recipient recipient : recipientsForMultiDeviceSync) {
                        deviceContactsOutputStream.write(new DeviceContact(RecipientUtil.toSignalServiceAddress(this.context, recipient), Optional.ofNullable(recipient.isSystemContact() ? recipient.getDisplayName(this.context) : recipient.getGroupName(this.context)), getSystemAvatar(recipient.getContactUri()), Optional.of(ChatColorsMapper.getMaterialColor(recipient.getChatColors()).serialize()), getVerifiedMessage(recipient, ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipient.getId())), ProfileKeyUtil.profileKeyOptional(recipient.getProfileKey()), recipient.isBlocked(), recipient.getExpiresInSeconds() > 0 ? Optional.of(Integer.valueOf(recipient.getExpiresInSeconds())) : Optional.empty(), Optional.ofNullable(inboxPositions.get(recipient.getId())), archivedRecipients.contains(recipient.getId())));
                    }
                    Recipient self = Recipient.self();
                    if (self.getProfileKey() != null) {
                        deviceContactsOutputStream.write(new DeviceContact(RecipientUtil.toSignalServiceAddress(this.context, self), Optional.empty(), Optional.empty(), Optional.of(ChatColorsMapper.getMaterialColor(self.getChatColors()).serialize()), Optional.empty(), ProfileKeyUtil.profileKeyOptionalOrThrow(self.getProfileKey()), false, self.getExpiresInSeconds() > 0 ? Optional.of(Integer.valueOf(self.getExpiresInSeconds())) : Optional.empty(), Optional.ofNullable(inboxPositions.get(self.getId())), archivedRecipients.contains(self.getId())));
                    }
                    deviceContactsOutputStream.close();
                    sendUpdate(ApplicationDependencies.getSignalServiceMessageSender(), BlobProvider.getInstance().getStream(this.context, createTempFile.uri), BlobProvider.getInstance().calculateFileSize(this.context, createTempFile.uri), true);
                } catch (InvalidNumberException e) {
                    Log.w(TAG, e);
                }
            } finally {
                BlobProvider.getInstance().delete(this.context, createTempFile.uri);
            }
        } else {
            Log.i(str, "App is backgrounded and the last contact sync was too soon (" + currentTimeMillis + " ms ago). Marking that we need a sync. Skipping multi-device contact update...");
            TextSecurePreferences.setNeedsFullContactSync(this.context, true);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        if ((exc instanceof PushNetworkException) || (exc instanceof NetworkException)) {
            return true;
        }
        return false;
    }

    private void sendUpdate(SignalServiceMessageSender signalServiceMessageSender, InputStream inputStream, long j, boolean z) throws UntrustedIdentityException, NetworkException {
        if (j > 0) {
            try {
                signalServiceMessageSender.sendSyncMessage(SignalServiceSyncMessage.forContacts(new ContactsMessage(SignalServiceAttachment.newStreamBuilder().withStream(inputStream).withContentType(MediaUtil.OCTET).withLength(j).withResumableUploadSpec(signalServiceMessageSender.getResumableUploadSpec()).build(), z)), UnidentifiedAccessUtil.getAccessForSync(this.context));
            } catch (IOException e) {
                throw new NetworkException(e);
            }
        } else {
            Log.w(TAG, "Nothing to write!");
        }
    }

    private Optional<SignalServiceAttachmentStream> getSystemAvatar(Uri uri) {
        byte[] blob;
        if (uri == null) {
            return Optional.empty();
        }
        if (!Permissions.hasAny(this.context, "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS")) {
            return Optional.empty();
        }
        try {
            AssetFileDescriptor openAssetFileDescriptor = this.context.getContentResolver().openAssetFileDescriptor(Uri.withAppendedPath(uri, "display_photo"), "r");
            if (openAssetFileDescriptor == null) {
                return Optional.empty();
            }
            return Optional.of(SignalServiceAttachment.newStreamBuilder().withStream(openAssetFileDescriptor.createInputStream()).withContentType("image/*").withLength(openAssetFileDescriptor.getLength()).build());
        } catch (IOException unused) {
            Uri withAppendedPath = Uri.withAppendedPath(uri, "photo");
            if (withAppendedPath == null) {
                return Optional.empty();
            }
            Cursor query = this.context.getContentResolver().query(withAppendedPath, new String[]{"data15", "mimetype"}, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToNext() && (blob = query.getBlob(0)) != null) {
                        Optional<SignalServiceAttachmentStream> of = Optional.of(SignalServiceAttachment.newStreamBuilder().withStream(new ByteArrayInputStream(blob)).withContentType("image/*").withLength((long) blob.length).build());
                        query.close();
                        return of;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            return Optional.empty();
        }
    }

    private Optional<VerifiedMessage> getVerifiedMessage(Recipient recipient, Optional<IdentityRecord> optional) throws InvalidNumberException, IOException {
        VerifiedMessage.VerifiedState verifiedState;
        if (!optional.isPresent()) {
            return Optional.empty();
        }
        SignalServiceAddress signalServiceAddress = RecipientUtil.toSignalServiceAddress(this.context, recipient);
        IdentityKey identityKey = optional.get().getIdentityKey();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[optional.get().getVerifiedStatus().ordinal()];
        if (i == 1) {
            verifiedState = VerifiedMessage.VerifiedState.VERIFIED;
        } else if (i == 2) {
            verifiedState = VerifiedMessage.VerifiedState.UNVERIFIED;
        } else if (i == 3) {
            verifiedState = VerifiedMessage.VerifiedState.DEFAULT;
        } else {
            throw new AssertionError("Unknown state: " + optional.get().getVerifiedStatus());
        }
        return Optional.of(new VerifiedMessage(signalServiceAddress, identityKey, verifiedState, System.currentTimeMillis()));
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus;

        static {
            int[] iArr = new int[IdentityDatabase.VerifiedStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus = iArr;
            try {
                iArr[IdentityDatabase.VerifiedStatus.VERIFIED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[IdentityDatabase.VerifiedStatus.UNVERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[IdentityDatabase.VerifiedStatus.DEFAULT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private WriteDetails createTempFile() throws IOException {
        ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
        return new WriteDetails(BlobProvider.getInstance().forData(new ParcelFileDescriptor.AutoCloseInputStream(createPipe[0]), 0).withFileName("multidevice-contact-update").createForSingleSessionOnDiskAsync(this.context, new BlobProvider.SuccessListener() { // from class: org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.providers.BlobProvider.SuccessListener
            public final void onSuccess() {
                MultiDeviceContactUpdateJob.lambda$createTempFile$0();
            }
        }, new BlobProvider.ErrorListener() { // from class: org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.providers.BlobProvider.ErrorListener
            public final void onError(IOException iOException) {
                MultiDeviceContactUpdateJob.lambda$createTempFile$1(iOException);
            }
        }), new ParcelFileDescriptor.AutoCloseOutputStream(createPipe[1]), null);
    }

    public static /* synthetic */ void lambda$createTempFile$0() {
        Log.i(TAG, "Write successful.");
    }

    public static /* synthetic */ void lambda$createTempFile$1(IOException iOException) {
        Log.w(TAG, "Error during write.", iOException);
    }

    /* loaded from: classes4.dex */
    public static class NetworkException extends Exception {
        public NetworkException(Exception exc) {
            super(exc);
        }
    }

    /* loaded from: classes4.dex */
    public static class WriteDetails {
        private final OutputStream outputStream;
        private final Uri uri;

        /* synthetic */ WriteDetails(Uri uri, OutputStream outputStream, AnonymousClass1 r3) {
            this(uri, outputStream);
        }

        private WriteDetails(Uri uri, OutputStream outputStream) {
            this.uri = uri;
            this.outputStream = outputStream;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceContactUpdateJob> {
        public MultiDeviceContactUpdateJob create(Job.Parameters parameters, Data data) {
            String string = data.getString("recipient");
            return new MultiDeviceContactUpdateJob(parameters, string != null ? RecipientId.from(string) : null, data.getBoolean(MultiDeviceContactUpdateJob.KEY_FORCE_SYNC), null);
        }
    }
}
