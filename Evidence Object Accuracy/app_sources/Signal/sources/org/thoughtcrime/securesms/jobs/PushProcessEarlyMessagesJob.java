package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.ServiceMessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;
import org.thoughtcrime.securesms.messages.MessageContentProcessor;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;

/* compiled from: PushProcessEarlyMessagesJob.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\u0007\b\u0012¢\u0006\u0002\u0010\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0014J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u0010H\u0016¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/PushProcessEarlyMessagesJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "()V", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PushProcessEarlyMessagesJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String TAG = Log.tag(PushProcessEarlyMessagesJob.class);

    public /* synthetic */ PushProcessEarlyMessagesJob(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public /* synthetic */ PushProcessEarlyMessagesJob(Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters);
    }

    @JvmStatic
    public static final void enqueue() {
        Companion.enqueue();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return false;
    }

    private PushProcessEarlyMessagesJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private PushProcessEarlyMessagesJob() {
        /*
            r3 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            r1 = 2
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxInstancesForFactory(r1)
            r1 = 1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setMaxAttempts(r1)
            r1 = -1
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setLifespan(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder()\n        .setMa…MMORTAL)\n        .build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r3.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.PushProcessEarlyMessagesJob.<init>():void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data data = Data.EMPTY;
        Intrinsics.checkNotNullExpressionValue(data, "EMPTY");
        return data;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        Collection<ServiceMessageId> allReferencedIds = ApplicationDependencies.getEarlyMessageCache().getAllReferencedIds();
        Intrinsics.checkNotNullExpressionValue(allReferencedIds, "getEarlyMessageCache().allReferencedIds");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = allReferencedIds.iterator();
        while (true) {
            boolean z = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            ServiceMessageId serviceMessageId = (ServiceMessageId) next;
            if (SignalDatabase.Companion.mmsSms().getMessageFor(serviceMessageId.getSentTimestamp(), serviceMessageId.getSender()) == null) {
                z = false;
            }
            if (z) {
                arrayList.add(next);
            }
        }
        List<ServiceMessageId> list = CollectionsKt___CollectionsKt.sortedWith(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.jobs.PushProcessEarlyMessagesJob$onRun$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return ComparisonsKt__ComparisonsKt.compareValues(Long.valueOf(((ServiceMessageId) t).getSentTimestamp()), Long.valueOf(((ServiceMessageId) t2).getSentTimestamp()));
            }
        });
        if (!list.isEmpty()) {
            String str = TAG;
            Log.i(str, "There are " + list.size() + " items in the early message cache with matches.");
            for (ServiceMessageId serviceMessageId2 : list) {
                Optional<List<SignalServiceContent>> retrieve = ApplicationDependencies.getEarlyMessageCache().retrieve(serviceMessageId2.getSender(), serviceMessageId2.getSentTimestamp());
                Intrinsics.checkNotNullExpressionValue(retrieve, "getEarlyMessageCache().r…sender, id.sentTimestamp)");
                if (retrieve.isPresent()) {
                    for (SignalServiceContent signalServiceContent : retrieve.get()) {
                        String str2 = TAG;
                        Log.i(str2, '[' + serviceMessageId2.getSentTimestamp() + "] Processing early content for " + serviceMessageId2);
                        MessageContentProcessor.forEarlyContent(this.context).process(MessageContentProcessor.MessageState.DECRYPTED_OK, signalServiceContent, null, serviceMessageId2.getSentTimestamp(), -1);
                    }
                } else {
                    String str3 = TAG;
                    Log.w(str3, '[' + serviceMessageId2.getSentTimestamp() + "] Saw " + serviceMessageId2 + " in the cache, but when we went to retrieve it, it was already gone.");
                }
            }
            return;
        }
        Log.i(TAG, "There are no items in the early message cache with matches.");
    }

    /* compiled from: PushProcessEarlyMessagesJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/PushProcessEarlyMessagesJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/PushProcessEarlyMessagesJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushProcessEarlyMessagesJob> {
        public PushProcessEarlyMessagesJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new PushProcessEarlyMessagesJob(parameters, null);
        }
    }

    /* compiled from: PushProcessEarlyMessagesJob.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/PushProcessEarlyMessagesJob$Companion;", "", "()V", "KEY", "", "TAG", "kotlin.jvm.PlatformType", "enqueue", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void enqueue() {
            Object obj;
            JobManager jobManager = ApplicationDependencies.getJobManager();
            Intrinsics.checkNotNullExpressionValue(jobManager, "getJobManager()");
            List<JobSpec> find = jobManager.find(new PushProcessEarlyMessagesJob$Companion$$ExternalSyntheticLambda0());
            Intrinsics.checkNotNullExpressionValue(find, "jobManger.find { it.fact…shProcessMessageJob.KEY }");
            Iterator<T> it = find.iterator();
            if (!it.hasNext()) {
                obj = null;
            } else {
                obj = it.next();
                if (it.hasNext()) {
                    long createTime = ((JobSpec) obj).getCreateTime();
                    do {
                        Object next = it.next();
                        long createTime2 = ((JobSpec) next).getCreateTime();
                        if (createTime < createTime2) {
                            obj = next;
                            createTime = createTime2;
                        }
                    } while (it.hasNext());
                }
            }
            JobSpec jobSpec = (JobSpec) obj;
            String id = jobSpec != null ? jobSpec.getId() : null;
            if (id != null) {
                jobManager.add(new PushProcessEarlyMessagesJob((DefaultConstructorMarker) null), CollectionsKt__CollectionsJVMKt.listOf(id));
            } else {
                jobManager.add(new PushProcessEarlyMessagesJob((DefaultConstructorMarker) null));
            }
        }

        /* renamed from: enqueue$lambda-0 */
        public static final boolean m1925enqueue$lambda0(JobSpec jobSpec) {
            return Intrinsics.areEqual(jobSpec.getFactoryKey(), PushProcessMessageJob.KEY);
        }
    }
}
