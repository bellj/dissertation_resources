package org.thoughtcrime.securesms.jobs;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.payments.proto.PaymentMetaData;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.messages.multidevice.OutgoingPaymentMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public final class MultiDeviceOutgoingPaymentSyncJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_UUID;
    private static final String TAG = Log.tag(MultiDeviceOutgoingPaymentSyncJob.class);
    private final UUID uuid;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public MultiDeviceOutgoingPaymentSyncJob(UUID uuid) {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).build(), uuid);
    }

    private MultiDeviceOutgoingPaymentSyncJob(Job.Parameters parameters, UUID uuid) {
        super(parameters);
        this.uuid = uuid;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("uuid", this.uuid.toString()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        Optional optional;
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else {
            PaymentDatabase.PaymentTransaction payment = SignalDatabase.payments().getPayment(this.uuid);
            if (payment == null) {
                String str = TAG;
                Log.w(str, "Payment not found " + this.uuid);
                return;
            }
            PaymentMetaData.MobileCoinTxoIdentification mobileCoinTxoIdentification = payment.getPaymentMetaData().getMobileCoinTxoIdentification();
            boolean isDefrag = payment.isDefrag();
            if (isDefrag || !payment.getPayee().hasRecipientId()) {
                optional = Optional.empty();
            } else {
                optional = Optional.of(RecipientUtil.getOrFetchServiceId(this.context, Recipient.resolved(payment.getPayee().requireRecipientId())));
            }
            byte[] receipt = payment.getReceipt();
            if (receipt != null) {
                ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forOutgoingPayment(new OutgoingPaymentMessage(optional, payment.getAmount().requireMobileCoin(), payment.getFee().requireMobileCoin(), ByteString.copyFrom(receipt), payment.getBlockIndex(), payment.getTimestamp(), isDefrag ? Optional.empty() : Optional.of(payment.getPayee().requirePublicAddress().serialize()), isDefrag ? Optional.empty() : Optional.of(payment.getNote()), mobileCoinTxoIdentification.getPublicKeyList(), mobileCoinTxoIdentification.getKeyImagesList())), UnidentifiedAccessUtil.getAccessForSync(this.context));
                return;
            }
            throw new AssertionError("Trying to sync payment before sent?");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to sync sent payment!");
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<MultiDeviceOutgoingPaymentSyncJob> {
        public MultiDeviceOutgoingPaymentSyncJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceOutgoingPaymentSyncJob(parameters, UUID.fromString(data.getString("uuid")));
        }
    }
}
