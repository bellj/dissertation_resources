package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import org.whispersystems.signalservice.api.storage.StorageId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StorageSyncJob$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((StorageId) obj).getRaw();
    }
}
