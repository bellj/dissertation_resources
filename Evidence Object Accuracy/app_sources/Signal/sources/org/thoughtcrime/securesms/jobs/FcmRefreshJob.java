package org.thoughtcrime.securesms.jobs;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.GoogleApiAvailability;
import j$.util.Optional;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PlayServicesProblemActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.gcm.FcmUtil;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes.dex */
public class FcmRefreshJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(FcmRefreshJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public FcmRefreshJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxAttempts(3).setLifespan(TimeUnit.HOURS.toMillis(6)).setMaxInstancesForFactory(1).build());
    }

    private FcmRefreshJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws Exception {
        if (SignalStore.account().isFcmEnabled()) {
            String str = TAG;
            Log.i(str, "Reregistering FCM...");
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this.context) != 0) {
                notifyFcmFailure();
                return;
            }
            Optional<String> token = FcmUtil.getToken(this.context);
            if (token.isPresent()) {
                String fcmToken = SignalStore.account().getFcmToken();
                if (!token.get().equals(fcmToken)) {
                    int length = fcmToken != null ? fcmToken.length() : -1;
                    Log.i(str, "Token changed. oldLength: " + length + "  newLength: " + token.get().length());
                } else {
                    Log.i(str, "Token didn't change.");
                }
                ApplicationDependencies.getSignalServiceAccountManager().setGcmId(token);
                SignalStore.account().setFcmToken(token.get());
                return;
            }
            throw new RetryLaterException(new IOException("Failed to retrieve a token."));
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "FCM reregistration failed after retry attempt exhaustion!");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return !(exc instanceof NonSuccessfulResponseCodeException);
    }

    private void notifyFcmFailure() {
        PendingIntent activity = PendingIntent.getActivity(this.context, 1122, new Intent(this.context, PlayServicesProblemActivity.class), SQLiteDatabase.CREATE_IF_NECESSARY);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.context, NotificationChannels.FAILURES);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(this.context.getResources(), R.drawable.ic_action_warning_red));
        builder.setContentTitle(this.context.getString(R.string.GcmRefreshJob_Permanent_Signal_communication_failure));
        builder.setContentText(this.context.getString(R.string.GcmRefreshJob_Signal_was_unable_to_register_with_Google_Play_Services));
        builder.setTicker(this.context.getString(R.string.GcmRefreshJob_Permanent_Signal_communication_failure));
        builder.setVibrate(new long[]{0, 1000});
        builder.setContentIntent(activity);
        ((NotificationManager) this.context.getSystemService("notification")).notify(12, builder.build());
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<FcmRefreshJob> {
        public FcmRefreshJob create(Job.Parameters parameters, Data data) {
            return new FcmRefreshJob(parameters);
        }
    }
}
