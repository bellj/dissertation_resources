package org.thoughtcrime.securesms.jobs;

import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ProfileUtil;

/* loaded from: classes.dex */
public final class ProfileUploadJob extends BaseJob {
    public static final String KEY;
    public static final String QUEUE;
    private static final String TAG = Log.tag(ProfileUploadJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return true;
    }

    public ProfileUploadJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue(QUEUE).setLifespan(TimeUnit.DAYS.toMillis(30)).setMaxAttempts(-1).setMaxInstancesForFactory(2).build());
    }

    private ProfileUploadJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (!SignalStore.account().isRegistered()) {
            Log.w(TAG, "Not registered. Skipping.");
            return;
        }
        ProfileUtil.uploadProfile(this.context);
        Log.i(TAG, "Profile uploaded.");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ProfileUploadJob> {
        public ProfileUploadJob create(Job.Parameters parameters, Data data) {
            return new ProfileUploadJob(parameters);
        }
    }
}
