package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StorageForcePushJob$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ RecipientDatabase f$0;

    public /* synthetic */ StorageForcePushJob$$ExternalSyntheticLambda0(RecipientDatabase recipientDatabase) {
        this.f$0 = recipientDatabase;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return this.f$0.getRecordForSync((RecipientId) obj);
    }
}
