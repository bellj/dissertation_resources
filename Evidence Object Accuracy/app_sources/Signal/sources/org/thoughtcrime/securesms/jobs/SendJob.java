package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.TextSecureExpiredException;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public abstract class SendJob extends BaseJob {
    private static final String TAG = Log.tag(SendJob.class);

    protected abstract void onSend() throws Exception;

    public SendJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public final void onRun() throws Exception {
        if (!SignalStore.misc().isClientDeprecated()) {
            String str = TAG;
            Log.i(str, "Starting message send attempt");
            onSend();
            Log.i(str, "Message send completed");
            return;
        }
        throw new TextSecureExpiredException(String.format("TextSecure expired (build %d, now %d)", Long.valueOf((long) BuildConfig.BUILD_TIMESTAMP), Long.valueOf(System.currentTimeMillis())));
    }

    public static void markAttachmentsUploaded(long j, OutgoingMediaMessage outgoingMediaMessage) {
        LinkedList<Attachment> linkedList = new LinkedList();
        linkedList.addAll(outgoingMediaMessage.getAttachments());
        linkedList.addAll(Stream.of(outgoingMediaMessage.getLinkPreviews()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.SendJob$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SendJob.lambda$markAttachmentsUploaded$0((LinkPreview) obj);
            }
        }).withoutNulls().toList());
        linkedList.addAll(Stream.of(outgoingMediaMessage.getSharedContacts()).map(new MmsDatabase$$ExternalSyntheticLambda2()).withoutNulls().toList());
        if (outgoingMediaMessage.getOutgoingQuote() != null) {
            linkedList.addAll(outgoingMediaMessage.getOutgoingQuote().getAttachments());
        }
        AttachmentDatabase attachments = SignalDatabase.attachments();
        for (Attachment attachment : linkedList) {
            attachments.markAttachmentUploaded(j, attachment);
        }
    }

    public static /* synthetic */ Attachment lambda$markAttachmentsUploaded$0(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().orElse(null);
    }

    public String buildAttachmentString(List<Attachment> list) {
        return Util.join((Collection) ((List) Collection$EL.stream(list).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.jobs.SendJob$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SendJob.lambda$buildAttachmentString$1((Attachment) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList())), ", ");
    }

    public static /* synthetic */ String lambda$buildAttachmentString$1(Attachment attachment) {
        if (attachment instanceof DatabaseAttachment) {
            return ((DatabaseAttachment) attachment).getAttachmentId().toString();
        }
        if (attachment.getUri() != null) {
            return attachment.getUri().toString();
        }
        return attachment.toString();
    }
}
