package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import android.net.Uri;
import com.annimon.stream.IntPair;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.annimon.stream.function.Supplier;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiData;
import org.thoughtcrime.securesms.emoji.EmojiDownloader;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.emoji.EmojiImageRequest;
import org.thoughtcrime.securesms.emoji.EmojiJsonRequest;
import org.thoughtcrime.securesms.emoji.EmojiPageCache;
import org.thoughtcrime.securesms.emoji.EmojiRemote;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.emoji.JumboEmoji;
import org.thoughtcrime.securesms.emoji.ParsedEmojiData;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.AutoDownloadEmojiConstraint;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.FileUtils;
import org.thoughtcrime.securesms.util.ScreenDensity;

/* loaded from: classes.dex */
public class DownloadLatestEmojiDataJob extends BaseJob {
    private static final long INTERVAL_WITHOUT_REMOTE_DOWNLOAD;
    private static final long INTERVAL_WITH_REMOTE_DOWNLOAD;
    public static final String KEY;
    private static final String QUEUE_KEY;
    private static final String TAG = Log.tag(DownloadLatestEmojiDataJob.class);
    private static final String VERSION_DENSITY;
    private static final String VERSION_INT;
    private static final String VERSION_UUID;
    private EmojiFiles.Version targetVersion;

    /* loaded from: classes4.dex */
    public interface Producer<T> {
        T produce();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    static {
        TimeUnit timeUnit = TimeUnit.DAYS;
        INTERVAL_WITHOUT_REMOTE_DOWNLOAD = timeUnit.toMillis(1);
        INTERVAL_WITH_REMOTE_DOWNLOAD = timeUnit.toMillis(7);
        TAG = Log.tag(DownloadLatestEmojiDataJob.class);
    }

    public static void scheduleIfNecessary(Context context) {
        long j;
        if (SignalStore.emojiValues().getNextScheduledImageCheck() <= System.currentTimeMillis()) {
            Log.i(TAG, "Scheduling DownloadLatestEmojiDataJob.");
            ApplicationDependencies.getJobManager().add(new DownloadLatestEmojiDataJob(false));
            if (EmojiFiles.Version.isVersionValid(context, EmojiFiles.Version.readVersion(context))) {
                j = INTERVAL_WITH_REMOTE_DOWNLOAD;
            } else {
                j = INTERVAL_WITHOUT_REMOTE_DOWNLOAD;
            }
            SignalStore.emojiValues().setNextScheduledImageCheck(System.currentTimeMillis() + j);
        }
    }

    public DownloadLatestEmojiDataJob(boolean z) {
        this(new Job.Parameters.Builder().setQueue(QUEUE_KEY).addConstraint(z ? NetworkConstraint.KEY : AutoDownloadEmojiConstraint.KEY).setMaxInstancesForQueue(1).setMaxAttempts(5).setLifespan(TimeUnit.DAYS.toMillis(1)).build(), null);
    }

    public DownloadLatestEmojiDataJob(Job.Parameters parameters, EmojiFiles.Version version) {
        super(parameters);
        this.targetVersion = version;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        String str;
        EmojiFiles.Version readVersion = EmojiFiles.Version.readVersion(this.context);
        int version = readVersion != null ? readVersion.getVersion() : 0;
        int version2 = EmojiRemote.getVersion();
        EmojiFiles.Version version3 = this.targetVersion;
        if (version3 == null) {
            str = getDesiredRemoteBucketForDensity(ScreenDensity.get(this.context));
        } else {
            str = version3.getDensity();
        }
        String str2 = TAG;
        Log.d(str2, "LocalVersion: " + version + ", ServerVersion: " + version2 + ", Bucket: " + str);
        if (str == null) {
            Log.d(str2, "This device has too low a display density to download remote emoji.");
        } else if (version == version2) {
            Log.d(str2, "Already have latest emoji data. Skipping.");
        } else if (version2 > version) {
            Log.d(str2, "New server data detected. Starting download...");
            EmojiFiles.Version version4 = this.targetVersion;
            if (version4 == null || version4.getVersion() != version2) {
                this.targetVersion = new EmojiFiles.Version(version2, UUID.randomUUID(), str);
            }
            if (isCanceled()) {
                Log.w(str2, "Job was cancelled prior to downloading json.");
                return;
            }
            EmojiData downloadJson = downloadJson(this.context, this.targetVersion);
            List<String> densities = downloadJson.getDensities();
            String format = downloadJson.getFormat();
            List list = Stream.of(downloadJson.getDataPages()).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda8
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((EmojiPageModel) obj).getSpriteUri();
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda9
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((Uri) obj).getLastPathSegment();
                }
            }).toList();
            this.targetVersion = new EmojiFiles.Version(this.targetVersion.getVersion(), this.targetVersion.getUuid(), resolveDensity(densities, this.targetVersion.getDensity()));
            if (isCanceled()) {
                Log.w(str2, "Job was cancelled after downloading json.");
                return;
            }
            downloadImages(this.context, this.targetVersion, list, format, new Producer() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda10
                @Override // org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob.Producer
                public final Object produce() {
                    return Boolean.valueOf(DownloadLatestEmojiDataJob.this.isCanceled());
                }
            });
            if (isCanceled()) {
                Log.w(str2, "Job was cancelled during or after downloading images.");
                return;
            }
            clearOldEmojiData(this.context, this.targetVersion);
            markComplete(this.targetVersion);
            EmojiSource.refresh();
            JumboEmoji.updateCurrentVersion(this.context);
        } else {
            Log.d(str2, "Server has an older version than we do. Skipping.");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        if (this.targetVersion == null) {
            return Data.EMPTY;
        }
        return new Data.Builder().putInt(VERSION_INT, this.targetVersion.getVersion()).putString(VERSION_UUID, this.targetVersion.getUuid().toString()).putString(VERSION_DENSITY, this.targetVersion.getDensity()).build();
    }

    private static String getDesiredRemoteBucketForDensity(ScreenDensity screenDensity) {
        return screenDensity.isKnownDensity() ? screenDensity.getBucket() : "xhdpi";
    }

    private static String resolveDensity(List<String> list, String str) {
        if (list.isEmpty()) {
            throw new IllegalStateException("Version does not have any supported densities.");
        } else if (list.contains(str)) {
            Log.d(TAG, "Version supports our density.");
            return str;
        } else {
            String str2 = TAG;
            Log.d(str2, "Version does not support our density.");
            List asList = Arrays.asList("ldpi", "mdpi", "hdpi", "xhdpi", "xxhdpi", "xxxhdpi");
            int indexOf = asList.indexOf(str);
            if (indexOf != -1) {
                return (String) Stream.of(asList).indexed().sorted(new Comparator(indexOf) { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda4
                    public final /* synthetic */ int f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // java.util.Comparator
                    public final int compare(Object obj, Object obj2) {
                        return DownloadLatestEmojiDataJob.lambda$resolveDensity$0(this.f$0, (IntPair) obj, (IntPair) obj2);
                    }
                }).map(new Function() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda5
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return (String) ((IntPair) obj).getSecond();
                    }
                }).filter(new Predicate(list) { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda6
                    public final /* synthetic */ List f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // com.annimon.stream.function.Predicate
                    public final boolean test(Object obj) {
                        return this.f$0.contains((String) obj);
                    }
                }).findFirst().orElseThrow(new Supplier() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda7
                    @Override // com.annimon.stream.function.Supplier
                    public final Object get() {
                        return DownloadLatestEmojiDataJob.lambda$resolveDensity$1();
                    }
                });
            }
            Log.d(str2, "Unknown density. Falling back...");
            if (list.contains("xhdpi")) {
                return "xhdpi";
            }
            return list.get(0);
        }
    }

    public static /* synthetic */ int lambda$resolveDensity$0(int i, IntPair intPair, IntPair intPair2) {
        int compare = Integer.compare(Math.abs(i - intPair.getFirst()), Math.abs(i - intPair2.getFirst()));
        return compare == 0 ? Integer.compare(intPair.getFirst(), intPair2.getFirst()) : compare;
    }

    public static /* synthetic */ IllegalStateException lambda$resolveDensity$1() {
        return new IllegalStateException("No density available.");
    }

    private static byte[] getRemoteImageHash(EmojiFiles.Version version, String str, String str2) {
        return EmojiRemote.getMd5(new EmojiImageRequest(version.getVersion(), version.getDensity(), str, str2));
    }

    private static EmojiData downloadJson(Context context, EmojiFiles.Version version) throws IOException, InvalidEmojiDataJsonException {
        EmojiFiles.NameCollection read = EmojiFiles.NameCollection.read(context, version);
        UUID uUIDForEmojiData = read.getUUIDForEmojiData();
        if (!Arrays.equals(uUIDForEmojiData != null ? EmojiFiles.getMd5(context, version, uUIDForEmojiData) : null, EmojiRemote.getMd5(new EmojiJsonRequest(version.getVersion())))) {
            Log.d(TAG, "Downloading JSON from Remote");
            assertRemoteDownloadConstraints(context);
            EmojiFiles.NameCollection.append(context, read, EmojiDownloader.downloadAndVerifyJsonFromRemote(context, version));
        } else {
            Log.d(TAG, "Already have JSON from remote, skipping download");
        }
        ParsedEmojiData latestEmojiData = EmojiFiles.getLatestEmojiData(context, version);
        if (latestEmojiData != null) {
            return latestEmojiData;
        }
        throw new InvalidEmojiDataJsonException();
    }

    private static void downloadImages(Context context, EmojiFiles.Version version, List<String> list, String str, Producer<Boolean> producer) throws IOException {
        EmojiFiles.NameCollection read = EmojiFiles.NameCollection.read(context, version);
        for (String str2 : list) {
            if (producer.produce().booleanValue()) {
                Log.w(TAG, "Job was cancelled while downloading images.");
                return;
            }
            UUID uUIDForName = read.getUUIDForName(str2);
            byte[] md5 = uUIDForName != null ? EmojiFiles.getMd5(context, version, uUIDForName) : null;
            byte[] remoteImageHash = getRemoteImageHash(version, str2, str);
            if (md5 == null || !Arrays.equals(md5, remoteImageHash)) {
                if (md5 != null) {
                    Log.d(TAG, "Hash mismatch. Deleting data and re-downloading file.");
                    EmojiFiles.delete(context, version, uUIDForName);
                }
                assertRemoteDownloadConstraints(context);
                read = EmojiFiles.NameCollection.append(context, read, EmojiDownloader.downloadAndVerifyImageFromRemote(context, version, version.getDensity(), str2, str));
            } else {
                Log.d(TAG, "Already have Image from remote, skipping download");
            }
        }
    }

    private void markComplete(EmojiFiles.Version version) {
        EmojiFiles.Version.writeVersion(this.context, version);
    }

    private static void assertRemoteDownloadConstraints(Context context) throws IOException {
        if (!AutoDownloadEmojiConstraint.canAutoDownloadEmoji(context)) {
            throw new IOException("Network conditions no longer permit download.");
        }
    }

    private static void clearOldEmojiData(Context context, EmojiFiles.Version version) {
        EmojiFiles.Version readVersion = EmojiFiles.Version.readVersion(context);
        String str = "";
        String uuid = readVersion != null ? readVersion.getUuid().toString() : str;
        if (version != null) {
            str = version.getUuid().toString();
        }
        File[] listFiles = EmojiFiles.getBaseDirectory(context).listFiles();
        if (listFiles == null) {
            Log.d(TAG, "No emoji data to delete.");
            return;
        }
        Log.d(TAG, "Deleting old folders of emoji data");
        Stream.of(listFiles).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((File) obj).isDirectory();
            }
        }).filterNot(new Predicate(uuid) { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return DownloadLatestEmojiDataJob.lambda$clearOldEmojiData$2(this.f$0, (File) obj);
            }
        }).filterNot(new Predicate(str) { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return DownloadLatestEmojiDataJob.lambda$clearOldEmojiData$3(this.f$0, (File) obj);
            }
        }).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                FileUtils.deleteDirectory((File) obj);
            }
        });
        EmojiPageCache.INSTANCE.clear();
        if (readVersion != null) {
            SignalStore.emojiValues().clearJumboEmojiSheets(readVersion.getVersion());
        }
    }

    public static /* synthetic */ boolean lambda$clearOldEmojiData$2(String str, File file) {
        return file.getName().equals(str);
    }

    public static /* synthetic */ boolean lambda$clearOldEmojiData$3(String str, File file) {
        return file.getName().equals(str);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<DownloadLatestEmojiDataJob> {
        public DownloadLatestEmojiDataJob create(Job.Parameters parameters, Data data) {
            return new DownloadLatestEmojiDataJob(parameters, (!data.hasInt(DownloadLatestEmojiDataJob.VERSION_INT) || !data.hasString(DownloadLatestEmojiDataJob.VERSION_UUID) || !data.hasString(DownloadLatestEmojiDataJob.VERSION_DENSITY)) ? null : new EmojiFiles.Version(data.getInt(DownloadLatestEmojiDataJob.VERSION_INT), UUID.fromString(data.getString(DownloadLatestEmojiDataJob.VERSION_UUID)), data.getString(DownloadLatestEmojiDataJob.VERSION_DENSITY)));
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidEmojiDataJsonException extends Exception {
        private InvalidEmojiDataJsonException() {
        }
    }
}
