package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.BackoffUtil;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.payments.Direction;
import org.thoughtcrime.securesms.payments.FailureReason;
import org.thoughtcrime.securesms.payments.PaymentTransactionId;
import org.thoughtcrime.securesms.payments.Payments;
import org.thoughtcrime.securesms.payments.Wallet;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes4.dex */
public final class PaymentTransactionCheckJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_UUID;
    private static final String TAG = Log.tag(PaymentTransactionCheckJob.class);
    private final UUID uuid;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* synthetic */ PaymentTransactionCheckJob(Job.Parameters parameters, UUID uuid, AnonymousClass1 r3) {
        this(parameters, uuid);
    }

    public PaymentTransactionCheckJob(UUID uuid) {
        this(uuid, "Payments");
    }

    public PaymentTransactionCheckJob(UUID uuid, String str) {
        this(new Job.Parameters.Builder().setQueue(str).addConstraint(NetworkConstraint.KEY).setMaxAttempts(-1).build(), uuid);
    }

    private PaymentTransactionCheckJob(Job.Parameters parameters, UUID uuid) {
        super(parameters);
        this.uuid = uuid;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        PaymentDatabase payments = SignalDatabase.payments();
        PaymentDatabase.PaymentTransaction payment = payments.getPayment(this.uuid);
        if (payment == null) {
            String str = TAG;
            Log.w(str, "No payment found for UUID " + this.uuid);
            return;
        }
        Payments payments2 = ApplicationDependencies.getPayments();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Direction[payment.getDirection().ordinal()];
        if (i == 1) {
            String str2 = TAG;
            Log.i(str2, "Checking sent status of " + this.uuid);
            byte[] transaction = payment.getTransaction();
            Objects.requireNonNull(transaction);
            byte[] receipt = payment.getReceipt();
            Objects.requireNonNull(receipt);
            Wallet.TransactionStatusResult sentTransactionStatus = payments2.getWallet().getSentTransactionStatus(new PaymentTransactionId.MobileCoin(transaction, receipt, payment.getFee().requireMobileCoin()));
            int i2 = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Wallet$TransactionStatus[sentTransactionStatus.getTransactionStatus().ordinal()];
            if (i2 == 1) {
                payments.markPaymentSuccessful(this.uuid, sentTransactionStatus.getBlockIndex());
                Log.i(str2, "Marked sent payment successful " + this.uuid);
            } else if (i2 == 2) {
                payments.markPaymentFailed(this.uuid, FailureReason.UNKNOWN);
                Log.i(str2, "Marked sent payment failed " + this.uuid);
            } else if (i2 != 3) {
                throw new AssertionError();
            } else {
                Log.i(str2, "Sent payment still in progress " + this.uuid);
                throw new IncompleteTransactionException(null);
            }
        } else if (i == 2) {
            String str3 = TAG;
            Log.i(str3, "Checking received status of " + this.uuid);
            Wallet wallet = payments2.getWallet();
            byte[] receipt2 = payment.getReceipt();
            Objects.requireNonNull(receipt2);
            Wallet.ReceivedTransactionStatus receivedTransactionStatus = wallet.getReceivedTransactionStatus(receipt2);
            int i3 = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$payments$Wallet$TransactionStatus[receivedTransactionStatus.getStatus().ordinal()];
            if (i3 == 1) {
                payments.markReceivedPaymentSuccessful(this.uuid, receivedTransactionStatus.getAmount(), receivedTransactionStatus.getBlockIndex());
                Log.i(str3, "Marked received payment successful " + this.uuid);
            } else if (i3 == 2) {
                payments.markPaymentFailed(this.uuid, FailureReason.UNKNOWN);
                Log.i(str3, "Marked received payment failed " + this.uuid);
            } else if (i3 != 3) {
                throw new AssertionError();
            } else {
                Log.i(str3, "Received payment still in progress " + this.uuid);
                throw new IncompleteTransactionException(null);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.PaymentTransactionCheckJob$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$Direction;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$payments$Wallet$TransactionStatus;

        static {
            int[] iArr = new int[Direction.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$Direction = iArr;
            try {
                iArr[Direction.SENT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$Direction[Direction.RECEIVED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[Wallet.TransactionStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$payments$Wallet$TransactionStatus = iArr2;
            try {
                iArr2[Wallet.TransactionStatus.COMPLETE.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$Wallet$TransactionStatus[Wallet.TransactionStatus.FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$payments$Wallet$TransactionStatus[Wallet.TransactionStatus.IN_PROGRESS.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public long getNextRunAttemptBackoff(int i, Exception exc) {
        if ((exc instanceof NonSuccessfulResponseCodeException) && ((NonSuccessfulResponseCodeException) exc).is5xx()) {
            return BackoffUtil.exponentialBackoff(i, FeatureFlags.getServerErrorMaxBackoff());
        }
        if (!(exc instanceof IncompleteTransactionException) || i >= 20) {
            return super.getNextRunAttemptBackoff(i, exc);
        }
        return 500;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return (exc instanceof IncompleteTransactionException) || (exc instanceof IOException);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("uuid", this.uuid.toString()).build();
    }

    /* loaded from: classes4.dex */
    private static final class IncompleteTransactionException extends Exception {
        private IncompleteTransactionException() {
        }

        /* synthetic */ IncompleteTransactionException(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PaymentTransactionCheckJob> {
        public PaymentTransactionCheckJob create(Job.Parameters parameters, Data data) {
            return new PaymentTransactionCheckJob(parameters, UUID.fromString(data.getString("uuid")), null);
        }
    }
}
