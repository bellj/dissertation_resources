package org.thoughtcrime.securesms.jobs;

import java.util.Collections;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.StickerPackOperationMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceStickerPackOperationJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_PACK_ID;
    private static final String KEY_PACK_KEY;
    private static final String KEY_TYPE;
    private static final String TAG = Log.tag(MultiDeviceStickerPackOperationJob.class);
    private final String packId;
    private final String packKey;
    private final Type type;

    /* loaded from: classes4.dex */
    public enum Type {
        INSTALL,
        REMOVE
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public MultiDeviceStickerPackOperationJob(String str, String str2, Type type) {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).build(), str, str2, type);
    }

    public MultiDeviceStickerPackOperationJob(Job.Parameters parameters, String str, String str2, Type type) {
        super(parameters);
        this.packId = str;
        this.packKey = str2;
        this.type = type;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_PACK_ID, this.packId).putString(KEY_PACK_KEY, this.packKey).putString("type", this.type.name()).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        StickerPackOperationMessage.Type type;
        if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
            return;
        }
        byte[] fromStringCondensed = Hex.fromStringCondensed(this.packId);
        byte[] fromStringCondensed2 = Hex.fromStringCondensed(this.packKey);
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceStickerPackOperationJob$Type[this.type.ordinal()];
        if (i == 1) {
            type = StickerPackOperationMessage.Type.INSTALL;
        } else if (i == 2) {
            type = StickerPackOperationMessage.Type.REMOVE;
        } else {
            throw new AssertionError("No matching type?");
        }
        ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forStickerPackOperations(Collections.singletonList(new StickerPackOperationMessage(fromStringCondensed, fromStringCondensed2, type))), UnidentifiedAccessUtil.getAccessForSync(this.context));
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.MultiDeviceStickerPackOperationJob$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceStickerPackOperationJob$Type;

        static {
            int[] iArr = new int[Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceStickerPackOperationJob$Type = iArr;
            try {
                iArr[Type.INSTALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$jobs$MultiDeviceStickerPackOperationJob$Type[Type.REMOVE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to sync sticker pack operation!");
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<MultiDeviceStickerPackOperationJob> {
        public MultiDeviceStickerPackOperationJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceStickerPackOperationJob(parameters, data.getString(MultiDeviceStickerPackOperationJob.KEY_PACK_ID), data.getString(MultiDeviceStickerPackOperationJob.KEY_PACK_KEY), Type.valueOf(data.getString("type")));
        }
    }
}
