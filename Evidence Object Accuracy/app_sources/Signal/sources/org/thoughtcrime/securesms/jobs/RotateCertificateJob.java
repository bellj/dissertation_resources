package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.CertificateType;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public final class RotateCertificateJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(RotateCertificateJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
    }

    /* synthetic */ RotateCertificateJob(Job.Parameters parameters, AnonymousClass1 r2) {
        this(parameters);
    }

    public RotateCertificateJob() {
        this(new Job.Parameters.Builder().setQueue("__ROTATE_SENDER_CERTIFICATE__").addConstraint(NetworkConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).setMaxAttempts(-1).build());
    }

    private RotateCertificateJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        byte[] bArr;
        if (!SignalStore.account().isRegistered()) {
            Log.w(TAG, "Not yet registered. Ignoring.");
            return;
        }
        synchronized (RotateCertificateJob.class) {
            SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
            Collection<CertificateType> allCertificateTypes = SignalStore.phoneNumberPrivacy().getAllCertificateTypes();
            String str = TAG;
            Log.i(str, "Rotating these certificates " + allCertificateTypes);
            for (CertificateType certificateType : allCertificateTypes) {
                int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType[certificateType.ordinal()];
                if (i == 1) {
                    bArr = signalServiceAccountManager.getSenderCertificate();
                } else if (i == 2) {
                    bArr = signalServiceAccountManager.getSenderCertificateForPhoneNumberPrivacy();
                } else {
                    throw new AssertionError();
                }
                Log.i(TAG, String.format("Successfully got %s certificate", certificateType));
                SignalStore.certificateValues().setUnidentifiedAccessCertificate(certificateType, bArr);
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.jobs.RotateCertificateJob$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType;

        static {
            int[] iArr = new int[CertificateType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType = iArr;
            try {
                iArr[CertificateType.UUID_AND_E164.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType[CertificateType.UUID_ONLY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to rotate sender certificate!");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RotateCertificateJob> {
        public RotateCertificateJob create(Job.Parameters parameters, Data data) {
            return new RotateCertificateJob(parameters, null);
        }
    }
}
