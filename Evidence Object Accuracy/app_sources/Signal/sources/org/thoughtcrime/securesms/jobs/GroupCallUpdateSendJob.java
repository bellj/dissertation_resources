package org.thoughtcrime.securesms.jobs;

import com.annimon.stream.Stream;
import j$.util.Collection$EL;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class GroupCallUpdateSendJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ERA_ID;
    private static final String KEY_INITIAL_RECIPIENT_COUNT;
    private static final String KEY_RECIPIENTS;
    private static final String KEY_RECIPIENT_ID;
    private static final String TAG = Log.tag(GroupCallUpdateSendJob.class);
    private final String eraId;
    private final int initialRecipientCount;
    private final RecipientId recipientId;
    private final List<RecipientId> recipients;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public static GroupCallUpdateSendJob create(RecipientId recipientId, String str) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.isPushV2Group()) {
            List list = Stream.of(RecipientUtil.getEligibleForSending(Recipient.resolvedList(resolved.getParticipantIds()))).filterNot(new GroupsV1MigrationRepository$$ExternalSyntheticLambda5()).map(new ContactUtil$$ExternalSyntheticLambda3()).toList();
            return new GroupCallUpdateSendJob(recipientId, str, list, list.size(), new Job.Parameters.Builder().setQueue(resolved.getId().toQueueKey()).setLifespan(TimeUnit.MINUTES.toMillis(5)).setMaxAttempts(3).build());
        }
        throw new AssertionError("We have a recipient, but it's not a V2 Group");
    }

    private GroupCallUpdateSendJob(RecipientId recipientId, String str, List<RecipientId> list, int i, Job.Parameters parameters) {
        super(parameters);
        this.recipientId = recipientId;
        this.eraId = str;
        this.recipients = list;
        this.initialRecipientCount = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString("recipient_id", this.recipientId.serialize()).putString(KEY_ERA_ID, this.eraId).putString("recipients", RecipientId.toSerializedList(this.recipients)).putInt(KEY_INITIAL_RECIPIENT_COUNT, this.initialRecipientCount).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        if (Recipient.self().isRegistered()) {
            Recipient resolved = Recipient.resolved(this.recipientId);
            if (resolved.isPushV2Group()) {
                List<Recipient> deliver = deliver(resolved, Stream.of(this.recipients).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList());
                for (Recipient recipient : deliver) {
                    this.recipients.remove(recipient.getId());
                }
                String str = TAG;
                Log.i(str, "Completed now: " + deliver.size() + ", Remaining: " + this.recipients.size());
                if (!this.recipients.isEmpty()) {
                    Log.w(str, "Still need to send to " + this.recipients.size() + " recipients. Retrying.");
                    throw new RetryLaterException();
                }
                return;
            }
            throw new AssertionError("We have a recipient, but it's not a V2 Group");
        }
        throw new NotPushRegisteredException();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        if ((exc instanceof IOException) || (exc instanceof RetryLaterException)) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        if (this.recipients.size() < this.initialRecipientCount) {
            String str = TAG;
            Log.w(str, "Only sent a group update to " + this.recipients.size() + "/" + this.initialRecipientCount + " recipients. Still, it sent to someone, so it stays.");
            return;
        }
        Log.w(TAG, "Failed to send the group update to all recipients!");
    }

    private List<Recipient> deliver(Recipient recipient, List<Recipient> list) throws IOException, UntrustedIdentityException {
        SignalServiceDataMessage.Builder withGroupCallUpdate = SignalServiceDataMessage.newBuilder().withTimestamp(System.currentTimeMillis()).withGroupCallUpdate(new SignalServiceDataMessage.GroupCallUpdate(this.eraId));
        GroupUtil.setDataMessageGroupContext(this.context, withGroupCallUpdate, recipient.requireGroupId().requirePush());
        SignalServiceDataMessage build = withGroupCallUpdate.build();
        List list2 = (List) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.jobs.GroupCallUpdateSendJob$$ExternalSyntheticLambda0
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return GroupCallUpdateSendJob.m1913$r8$lambda$YxvTiG7H35HQaH4X50TiB4EmqQ((Recipient) obj);
            }
        }).collect(Collectors.toList());
        boolean z = list2.size() != list.size();
        List<SendMessageResult> sendUnresendableDataMessage = GroupSendUtil.sendUnresendableDataMessage(this.context, recipient.requireGroupId().requireV2(), list2, false, ContentHint.DEFAULT, build);
        if (z) {
            sendUnresendableDataMessage.add(ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(build));
        }
        return GroupSendJobHelper.getCompletedSends(list, sendUnresendableDataMessage).completed;
    }

    public static /* synthetic */ boolean lambda$deliver$0(Recipient recipient) {
        return !recipient.isSelf();
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<GroupCallUpdateSendJob> {
        public GroupCallUpdateSendJob create(Job.Parameters parameters, Data data) {
            return new GroupCallUpdateSendJob(RecipientId.from(data.getString("recipient_id")), data.getString(GroupCallUpdateSendJob.KEY_ERA_ID), RecipientId.fromSerializedList(data.getString("recipients")), data.getInt(GroupCallUpdateSendJob.KEY_INITIAL_RECIPIENT_COUNT), parameters);
        }
    }
}
