package org.thoughtcrime.securesms.jobs;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.events.ReminderUpdateEvent;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class ServiceOutageDetectionJob extends BaseJob {
    private static final long CHECK_TIME;
    private static final String IP_FAILURE;
    private static final String IP_SUCCESS;
    public static final String KEY;
    private static final String TAG = Log.tag(ServiceOutageDetectionJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public ServiceOutageDetectionJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxAttempts(5).setMaxInstancesForFactory(1).build());
    }

    private ServiceOutageDetectionJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws RetryLaterException {
        String str = TAG;
        Log.i(str, "onRun()");
        if (System.currentTimeMillis() - TextSecurePreferences.getLastOutageCheckTime(this.context) < CHECK_TIME) {
            Log.w(str, "Skipping service outage check. Too soon.");
            return;
        }
        try {
            InetAddress byName = InetAddress.getByName(BuildConfig.SIGNAL_SERVICE_STATUS_URL);
            Log.i(str, "Received outage check address: " + byName.getHostAddress());
            if (IP_SUCCESS.equals(byName.getHostAddress())) {
                Log.i(str, "Service is available.");
                TextSecurePreferences.setServiceOutage(this.context, false);
            } else if (IP_FAILURE.equals(byName.getHostAddress())) {
                Log.w(str, "Service is down.");
                TextSecurePreferences.setServiceOutage(this.context, true);
            } else {
                Log.w(str, "Service status check returned an unrecognized IP address. Could be a weird network state. Prompting retry.");
                throw new RetryLaterException(new Exception("Unrecognized service outage IP address."));
            }
            TextSecurePreferences.setLastOutageCheckTime(this.context, System.currentTimeMillis());
            EventBus.getDefault().post(new ReminderUpdateEvent());
        } catch (UnknownHostException e) {
            throw new RetryLaterException(e);
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryLaterException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.i(TAG, "Service status check could not complete. Assuming success to avoid false positives due to bad network.");
        TextSecurePreferences.setServiceOutage(this.context, false);
        TextSecurePreferences.setLastOutageCheckTime(this.context, System.currentTimeMillis());
        EventBus.getDefault().post(new ReminderUpdateEvent());
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<ServiceOutageDetectionJob> {
        public ServiceOutageDetectionJob create(Job.Parameters parameters, Data data) {
            return new ServiceOutageDetectionJob(parameters);
        }
    }
}
