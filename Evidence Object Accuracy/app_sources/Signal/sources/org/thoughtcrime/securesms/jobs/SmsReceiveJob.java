package org.thoughtcrime.securesms.jobs;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import com.google.android.gms.common.api.Status;
import j$.util.Optional;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.SqlCipherMigrationConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.VerificationCodeParser;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class SmsReceiveJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_PDUS;
    private static final String KEY_SUBSCRIPTION_ID;
    private static final String TAG = Log.tag(SmsReceiveJob.class);
    private Object[] pdus;
    private int subscriptionId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public SmsReceiveJob(Object[] objArr, int i) {
        this(new Job.Parameters.Builder().addConstraint(SqlCipherMigrationConstraint.KEY).setLifespan(TimeUnit.DAYS.toMillis(1)).build(), objArr, i);
    }

    private SmsReceiveJob(Job.Parameters parameters, Object[] objArr, int i) {
        super(parameters);
        this.pdus = objArr;
        this.subscriptionId = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        String[] strArr = new String[this.pdus.length];
        int i = 0;
        while (true) {
            Object[] objArr = this.pdus;
            if (i >= objArr.length) {
                return new Data.Builder().putStringArray(KEY_PDUS, strArr).putInt("subscription_id", this.subscriptionId).build();
            }
            strArr[i] = Base64.encodeBytes((byte[]) objArr[i]);
            i++;
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws MigrationPendingException, RetryLaterException {
        Optional<IncomingTextMessage> assembleMessageFragments = assembleMessageFragments(this.pdus, this.subscriptionId);
        if (SignalStore.account().getE164() == null) {
            String str = TAG;
            Log.i(str, "Received an SMS before we're registered...");
            if (!assembleMessageFragments.isPresent()) {
                Log.w(str, "Received an SMS before registration is complete, but couldn't assemble the message anyway. Ignoring.");
            } else if (VerificationCodeParser.parse(assembleMessageFragments.get().getMessageBody()).isPresent()) {
                Log.i(str, "Received something that looks like a registration SMS. Posting a notification and broadcast.");
                ServiceUtil.getNotificationManager(this.context).notify(NotificationIds.PRE_REGISTRATION_SMS, buildPreRegistrationNotification(this.context, assembleMessageFragments.get()));
                this.context.sendBroadcast(buildSmsRetrieverIntent(assembleMessageFragments.get()));
            } else {
                Log.w(str, "Received an SMS before registration is complete. We'll try again later.");
                throw new RetryLaterException();
            }
        } else if (assembleMessageFragments.isPresent() && SignalStore.account().getE164() != null && assembleMessageFragments.get().getSender().equals(Recipient.self().getId())) {
            Log.w(TAG, "Received an SMS from ourselves! Ignoring.");
        } else if (assembleMessageFragments.isPresent() && !isBlocked(assembleMessageFragments.get())) {
            Optional<MessageDatabase.InsertResult> storeMessage = storeMessage(assembleMessageFragments.get());
            if (storeMessage.isPresent()) {
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(storeMessage.get().getThreadId()));
            }
        } else if (assembleMessageFragments.isPresent()) {
            Log.w(TAG, "Received an SMS from a blocked user. Ignoring.");
        } else {
            Log.w(TAG, "Failed to assemble message fragments!");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return (exc instanceof MigrationPendingException) || (exc instanceof RetryLaterException);
    }

    private boolean isBlocked(IncomingTextMessage incomingTextMessage) {
        if (incomingTextMessage.getSender() != null) {
            return Recipient.resolved(incomingTextMessage.getSender()).isBlocked();
        }
        return false;
    }

    private Optional<MessageDatabase.InsertResult> storeMessage(IncomingTextMessage incomingTextMessage) throws MigrationPendingException {
        SmsDatabase sms = SignalDatabase.sms();
        sms.ensureMigration();
        if (TextSecurePreferences.getNeedsSqlCipherMigration(this.context)) {
            throw new MigrationPendingException();
        } else if (!incomingTextMessage.isSecureMessage()) {
            return sms.insertMessageInbox(incomingTextMessage);
        } else {
            Optional<MessageDatabase.InsertResult> insertMessageInbox = sms.insertMessageInbox(new IncomingTextMessage(incomingTextMessage, ""));
            sms.markAsLegacyVersion(insertMessageInbox.get().getMessageId());
            return insertMessageInbox;
        }
    }

    private Optional<IncomingTextMessage> assembleMessageFragments(Object[] objArr, int i) {
        if (objArr == null) {
            return Optional.empty();
        }
        LinkedList linkedList = new LinkedList();
        for (Object obj : objArr) {
            SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) obj);
            linkedList.add(new IncomingTextMessage(Recipient.external(this.context, createFromPdu.getDisplayOriginatingAddress()).getId(), createFromPdu, i));
        }
        if (linkedList.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new IncomingTextMessage(linkedList));
    }

    private static Notification buildPreRegistrationNotification(Context context, IncomingTextMessage incomingTextMessage) {
        return new NotificationCompat.Builder(context, NotificationChannels.getMessagesChannel(context)).setStyle(new NotificationCompat.MessagingStyle(new Person.Builder().setName(Recipient.resolved(incomingTextMessage.getSender()).getE164().orElse("")).build()).addMessage(new NotificationCompat.MessagingStyle.Message(incomingTextMessage.getMessageBody(), incomingTextMessage.getSentTimestampMillis(), null))).setSmallIcon(R.drawable.ic_notification).build();
    }

    private static Intent buildSmsRetrieverIntent(IncomingTextMessage incomingTextMessage) {
        Intent intent = new Intent("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");
        intent.putExtra("com.google.android.gms.auth.api.phone.EXTRA_STATUS", Status.RESULT_SUCCESS);
        intent.putExtra("com.google.android.gms.auth.api.phone.EXTRA_SMS_MESSAGE", incomingTextMessage.getMessageBody());
        return intent;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<SmsReceiveJob> {
        public SmsReceiveJob create(Job.Parameters parameters, Data data) {
            try {
                int i = data.getInt("subscription_id");
                String[] stringArray = data.getStringArray(SmsReceiveJob.KEY_PDUS);
                Object[] objArr = new Object[stringArray.length];
                for (int i2 = 0; i2 < stringArray.length; i2++) {
                    objArr[i2] = Base64.decode(stringArray[i2]);
                }
                return new SmsReceiveJob(parameters, objArr, i);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class MigrationPendingException extends Exception {
        private MigrationPendingException() {
            SmsReceiveJob.this = r1;
        }
    }
}
