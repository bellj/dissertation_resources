package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.backup.BackupFileIOError;
import org.thoughtcrime.securesms.backup.BackupPassphrase;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.backup.FullBackupExporter;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.database.NoExternalStorageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.impl.ChargingConstraint;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.service.GenericForegroundService;
import org.thoughtcrime.securesms.service.NotificationController;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.StorageUtil;

/* loaded from: classes4.dex */
public final class LocalBackupJob extends BaseJob {
    public static final String KEY;
    public static final String QUEUE;
    private static final String TAG = Log.tag(LocalBackupJob.class);
    public static final String TEMP_BACKUP_FILE_PREFIX;
    public static final String TEMP_BACKUP_FILE_SUFFIX;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return false;
    }

    public static void enqueue(boolean z) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        Job.Parameters.Builder maxAttempts = new Job.Parameters.Builder().setQueue(QUEUE).setMaxInstancesForFactory(1).setMaxAttempts(3);
        if (z) {
            jobManager.cancelAllInQueue(QUEUE);
        } else {
            maxAttempts.addConstraint(ChargingConstraint.KEY);
        }
        if (BackupUtil.isUserSelectionRequired(ApplicationDependencies.getApplication())) {
            jobManager.add(new LocalBackupJobApi29(maxAttempts.build()));
        } else {
            jobManager.add(new LocalBackupJob(maxAttempts.build()));
        }
    }

    private LocalBackupJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws NoExternalStorageException, IOException {
        String str = TAG;
        Log.i(str, "Executing backup job...");
        BackupFileIOError.clearNotification(this.context);
        if (Permissions.hasAll(this.context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            ProgressUpdater progressUpdater = new ProgressUpdater();
            try {
                Context context = this.context;
                NotificationController startForegroundTask = GenericForegroundService.startForegroundTask(context, context.getString(R.string.LocalBackupJob_creating_signal_backup), NotificationChannels.BACKUPS, R.drawable.ic_signal_backup);
                progressUpdater.setNotification(startForegroundTask);
                EventBus.getDefault().register(progressUpdater);
                startForegroundTask.setIndeterminateProgress();
                String str2 = BackupPassphrase.get(this.context);
                File orCreateBackupDirectory = StorageUtil.getOrCreateBackupDirectory();
                File file = new File(orCreateBackupDirectory, String.format("signal-%s.backup", new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US).format(new Date())));
                deleteOldTemporaryBackups(orCreateBackupDirectory);
                if (file.exists()) {
                    throw new IOException("Backup file already exists?");
                } else if (str2 != null) {
                    File createTempFile = File.createTempFile(".backup", ".tmp", orCreateBackupDirectory);
                    try {
                        try {
                            try {
                                Context context2 = this.context;
                                FullBackupExporter.export(context2, AttachmentSecretProvider.getInstance(context2).getOrCreateAttachmentSecret(), SignalDatabase.getBackupDatabase(), createTempFile, str2, new FullBackupExporter.BackupCancellationSignal() { // from class: org.thoughtcrime.securesms.jobs.LocalBackupJob$$ExternalSyntheticLambda0
                                    @Override // org.thoughtcrime.securesms.backup.FullBackupExporter.BackupCancellationSignal
                                    public final boolean isCanceled() {
                                        return LocalBackupJob.this.isCanceled();
                                    }
                                });
                                if (createTempFile.renameTo(file)) {
                                    if (createTempFile.exists()) {
                                        if (createTempFile.delete()) {
                                            Log.w(str, "Backup failed. Deleted temp file");
                                        } else {
                                            Log.w(str, "Backup failed. Failed to delete temp file " + createTempFile);
                                        }
                                    }
                                    BackupUtil.deleteOldBackups();
                                    startForegroundTask.close();
                                    return;
                                }
                                Log.w(str, "Failed to rename temp file");
                                throw new IOException("Renaming temporary backup file failed!");
                            } catch (IOException e) {
                                BackupFileIOError.postNotificationForException(this.context, e, getRunAttempt());
                                throw e;
                            }
                        } catch (FullBackupExporter.BackupCanceledException e2) {
                            Log.w(TAG, "Backup cancelled");
                            throw e2;
                        }
                    } catch (Throwable th) {
                        if (createTempFile.exists()) {
                            if (createTempFile.delete()) {
                                Log.w(TAG, "Backup failed. Deleted temp file");
                            } else {
                                String str3 = TAG;
                                Log.w(str3, "Backup failed. Failed to delete temp file " + createTempFile);
                            }
                        }
                        throw th;
                    }
                } else {
                    throw new IOException("Backup password is null");
                }
            } finally {
                EventBus.getDefault().unregister(progressUpdater);
                progressUpdater.setNotification(null);
            }
        } else {
            throw new IOException("No external storage permission!");
        }
    }

    private static void deleteOldTemporaryBackups(File file) {
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (file2.isFile()) {
                String name = file2.getName();
                if (name.startsWith(".backup") && name.endsWith(".tmp")) {
                    if (file2.delete()) {
                        Log.w(TAG, "Deleted old temporary backup file");
                    } else {
                        Log.w(TAG, "Could not delete old temporary backup file");
                    }
                }
            }
        }
    }

    /* loaded from: classes.dex */
    private static class ProgressUpdater {
        private NotificationController notification;

        private ProgressUpdater() {
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onEvent(FullBackupBase.BackupEvent backupEvent) {
            if (this.notification == null || backupEvent.getType() != FullBackupBase.BackupEvent.Type.PROGRESS) {
                return;
            }
            if (backupEvent.getEstimatedTotalCount() == 0) {
                this.notification.setIndeterminateProgress();
            } else {
                this.notification.setProgress(100, (long) ((int) backupEvent.getCompletionPercentage()));
            }
        }

        public void setNotification(NotificationController notificationController) {
            this.notification = notificationController;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<LocalBackupJob> {
        public LocalBackupJob create(Job.Parameters parameters, Data data) {
            return new LocalBackupJob(parameters);
        }
    }
}
