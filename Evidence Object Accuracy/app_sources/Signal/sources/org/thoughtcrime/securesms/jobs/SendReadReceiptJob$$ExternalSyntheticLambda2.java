package org.thoughtcrime.securesms.jobs;

import j$.util.function.Function;
import org.thoughtcrime.securesms.database.MessageDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SendReadReceiptJob$$ExternalSyntheticLambda2 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((MessageDatabase.MarkedMessageInfo) obj).getMessageId();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
