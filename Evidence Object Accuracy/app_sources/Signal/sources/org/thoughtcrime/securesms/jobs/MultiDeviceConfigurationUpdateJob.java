package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.NotPushRegisteredException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.multidevice.ConfigurationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;

/* loaded from: classes4.dex */
public class MultiDeviceConfigurationUpdateJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_LINK_PREVIEWS_ENABLED;
    private static final String KEY_READ_RECEIPTS_ENABLED;
    private static final String KEY_TYPING_INDICATORS_ENABLED;
    private static final String KEY_UNIDENTIFIED_DELIVERY_INDICATORS_ENABLED;
    private static final String TAG = Log.tag(MultiDeviceConfigurationUpdateJob.class);
    private boolean linkPreviewsEnabled;
    private boolean readReceiptsEnabled;
    private boolean typingIndicatorsEnabled;
    private boolean unidentifiedDeliveryIndicatorsEnabled;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public MultiDeviceConfigurationUpdateJob(boolean z, boolean z2, boolean z3, boolean z4) {
        this(new Job.Parameters.Builder().setQueue("__MULTI_DEVICE_CONFIGURATION_UPDATE_JOB__").addConstraint(NetworkConstraint.KEY).setMaxAttempts(10).build(), z, z2, z3, z4);
    }

    private MultiDeviceConfigurationUpdateJob(Job.Parameters parameters, boolean z, boolean z2, boolean z3, boolean z4) {
        super(parameters);
        this.readReceiptsEnabled = z;
        this.typingIndicatorsEnabled = z2;
        this.unidentifiedDeliveryIndicatorsEnabled = z3;
        this.linkPreviewsEnabled = z4;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putBoolean(KEY_READ_RECEIPTS_ENABLED, this.readReceiptsEnabled).putBoolean(KEY_TYPING_INDICATORS_ENABLED, this.typingIndicatorsEnabled).putBoolean(KEY_UNIDENTIFIED_DELIVERY_INDICATORS_ENABLED, this.unidentifiedDeliveryIndicatorsEnabled).putBoolean(KEY_LINK_PREVIEWS_ENABLED, this.linkPreviewsEnabled).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, UntrustedIdentityException {
        if (!Recipient.self().isRegistered()) {
            throw new NotPushRegisteredException();
        } else if (!TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Not multi device, aborting...");
        } else if (SignalStore.account().isLinkedDevice()) {
            Log.i(TAG, "Not primary device, aborting...");
        } else {
            ApplicationDependencies.getSignalServiceMessageSender().sendSyncMessage(SignalServiceSyncMessage.forConfiguration(new ConfigurationMessage(Optional.of(Boolean.valueOf(this.readReceiptsEnabled)), Optional.of(Boolean.valueOf(this.unidentifiedDeliveryIndicatorsEnabled)), Optional.of(Boolean.valueOf(this.typingIndicatorsEnabled)), Optional.of(Boolean.valueOf(this.linkPreviewsEnabled)))), UnidentifiedAccessUtil.getAccessForSync(this.context));
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        if (exc instanceof ServerRejectedException) {
            return false;
        }
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "**** Failed to synchronize read receipts state!");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<MultiDeviceConfigurationUpdateJob> {
        public MultiDeviceConfigurationUpdateJob create(Job.Parameters parameters, Data data) {
            return new MultiDeviceConfigurationUpdateJob(parameters, data.getBooleanOrDefault(MultiDeviceConfigurationUpdateJob.KEY_READ_RECEIPTS_ENABLED, false), data.getBooleanOrDefault(MultiDeviceConfigurationUpdateJob.KEY_TYPING_INDICATORS_ENABLED, false), data.getBooleanOrDefault(MultiDeviceConfigurationUpdateJob.KEY_UNIDENTIFIED_DELIVERY_INDICATORS_ENABLED, false), data.getBooleanOrDefault(MultiDeviceConfigurationUpdateJob.KEY_LINK_PREVIEWS_ENABLED, false));
        }
    }
}
