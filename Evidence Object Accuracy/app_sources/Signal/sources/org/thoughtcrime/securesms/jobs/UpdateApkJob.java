package org.thoughtcrime.securesms.jobs;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.service.UpdateApkReadyListener;
import org.thoughtcrime.securesms.util.FileUtils;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class UpdateApkJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(UpdateApkJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public UpdateApkJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxAttempts(2).build());
    }

    private UpdateApkJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException, PackageManager.NameNotFoundException {
        String str = TAG;
        Log.i(str, "Checking for APK update...");
        Response execute = new OkHttpClient().newCall(new Request.Builder().url(String.format("%s/latest.json", BuildConfig.NOPLAY_UPDATE_URL)).build()).execute();
        if (execute.isSuccessful()) {
            UpdateDescriptor updateDescriptor = (UpdateDescriptor) JsonUtils.fromJson(execute.body().string(), UpdateDescriptor.class);
            byte[] fromStringCondensed = Hex.fromStringCondensed(updateDescriptor.getDigest());
            Log.i(str, "Got descriptor: " + updateDescriptor);
            if (updateDescriptor.getVersionCode() > getVersionCode()) {
                DownloadStatus downloadStatus = getDownloadStatus(updateDescriptor.getUrl(), fromStringCondensed);
                Log.i(str, "Download status: " + downloadStatus.getStatus());
                if (downloadStatus.getStatus() == DownloadStatus.Status.COMPLETE) {
                    Log.i(str, "Download status complete, notifying...");
                    handleDownloadNotify(downloadStatus.getDownloadId());
                } else if (downloadStatus.getStatus() == DownloadStatus.Status.MISSING) {
                    Log.i(str, "Download status missing, starting download...");
                    handleDownloadStart(updateDescriptor.getUrl(), updateDescriptor.getVersionName(), fromStringCondensed);
                }
            }
        } else {
            throw new IOException("Bad response: " + execute.message());
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Update check failed");
    }

    private int getVersionCode() throws PackageManager.NameNotFoundException {
        return this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionCode;
    }

    private DownloadStatus getDownloadStatus(String str, byte[] bArr) {
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterByStatus(15);
        long updateApkDownloadId = TextSecurePreferences.getUpdateApkDownloadId(this.context);
        byte[] pendingDigest = getPendingDigest(this.context);
        Cursor query2 = ((DownloadManager) this.context.getSystemService("download")).query(query);
        try {
            DownloadStatus downloadStatus = new DownloadStatus(DownloadStatus.Status.MISSING, -1);
            while (query2 != null && query2.moveToNext()) {
                int i = query2.getInt(query2.getColumnIndexOrThrow("status"));
                String string = query2.getString(query2.getColumnIndexOrThrow("uri"));
                long j = query2.getLong(query2.getColumnIndexOrThrow("_id"));
                byte[] digestForDownloadId = getDigestForDownloadId(j);
                if (string != null && string.equals(str) && j == updateApkDownloadId) {
                    if (i == 8 && digestForDownloadId != null && pendingDigest != null && MessageDigest.isEqual(pendingDigest, bArr) && MessageDigest.isEqual(digestForDownloadId, bArr)) {
                        DownloadStatus downloadStatus2 = new DownloadStatus(DownloadStatus.Status.COMPLETE, j);
                        query2.close();
                        return downloadStatus2;
                    } else if (i != 8) {
                        downloadStatus = new DownloadStatus(DownloadStatus.Status.PENDING, j);
                    }
                }
            }
            return downloadStatus;
        } finally {
            if (query2 != null) {
                query2.close();
            }
        }
    }

    private void handleDownloadStart(String str, String str2, byte[] bArr) {
        clearPreviousDownloads(this.context);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
        request.setAllowedNetworkTypes(2);
        request.setTitle("Downloading Signal update");
        request.setDescription("Downloading Signal " + str2);
        request.setVisibleInDownloadsUi(false);
        request.setDestinationInExternalFilesDir(this.context, null, "signal-update.apk");
        request.setNotificationVisibility(2);
        TextSecurePreferences.setUpdateApkDownloadId(this.context, ((DownloadManager) this.context.getSystemService("download")).enqueue(request));
        TextSecurePreferences.setUpdateApkDigest(this.context, Hex.toStringCondensed(bArr));
    }

    private void handleDownloadNotify(long j) {
        Intent intent = new Intent("android.intent.action.DOWNLOAD_COMPLETE");
        intent.putExtra("extra_download_id", j);
        new UpdateApkReadyListener().onReceive(this.context, intent);
    }

    private byte[] getDigestForDownloadId(long j) {
        try {
            FileInputStream fileInputStream = new FileInputStream(((DownloadManager) this.context.getSystemService("download")).openDownloadedFile(j).getFileDescriptor());
            byte[] fileDigest = FileUtils.getFileDigest(fileInputStream);
            fileInputStream.close();
            return fileDigest;
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    private byte[] getPendingDigest(Context context) {
        try {
            String updateApkDigest = TextSecurePreferences.getUpdateApkDigest(context);
            if (updateApkDigest == null) {
                return null;
            }
            return Hex.fromStringCondensed(updateApkDigest);
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    private static void clearPreviousDownloads(Context context) {
        File externalFilesDir = context.getExternalFilesDir(null);
        if (externalFilesDir == null) {
            Log.w(TAG, "Failed to read external files directory.");
            return;
        }
        File[] listFiles = externalFilesDir.listFiles();
        for (File file : listFiles) {
            if (file.getName().startsWith("signal-update") && file.delete()) {
                Log.d(TAG, "Deleted " + file.getName());
            }
        }
    }

    /* loaded from: classes.dex */
    private static class UpdateDescriptor {
        @JsonProperty
        private String sha256sum;
        @JsonProperty
        private String url;
        @JsonProperty
        private int versionCode;
        @JsonProperty
        private String versionName;

        private UpdateDescriptor() {
        }

        public int getVersionCode() {
            return this.versionCode;
        }

        public String getVersionName() {
            return this.versionName;
        }

        public String getUrl() {
            return this.url;
        }

        public String toString() {
            return "[" + this.versionCode + ", " + this.versionName + ", " + this.url + "]";
        }

        public String getDigest() {
            return this.sha256sum;
        }
    }

    /* loaded from: classes4.dex */
    public static class DownloadStatus {
        private final long downloadId;
        private final Status status;

        /* loaded from: classes4.dex */
        public enum Status {
            PENDING,
            COMPLETE,
            MISSING
        }

        DownloadStatus(Status status, long j) {
            this.status = status;
            this.downloadId = j;
        }

        public Status getStatus() {
            return this.status;
        }

        public long getDownloadId() {
            return this.downloadId;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<UpdateApkJob> {
        public UpdateApkJob create(Job.Parameters parameters, Data data) {
            return new UpdateApkJob(parameters);
        }
    }
}
