package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.messages.RestStrategy;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes.dex */
public final class PushNotificationReceiveJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_FOREGROUND_SERVICE_DELAY;
    private static final String TAG = Log.tag(PushNotificationReceiveJob.class);
    private final long foregroundServiceDelayMs;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public PushNotificationReceiveJob() {
        this(-1);
    }

    private PushNotificationReceiveJob(long j) {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).setQueue("__notification_received").setMaxAttempts(3).setMaxInstancesForFactory(1).build(), j);
    }

    private PushNotificationReceiveJob(Job.Parameters parameters, long j) {
        super(parameters);
        this.foregroundServiceDelayMs = j;
    }

    public static Job withDelayedForegroundService(long j) {
        return new PushNotificationReceiveJob(j);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong(KEY_FOREGROUND_SERVICE_DELAY, this.foregroundServiceDelayMs).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws IOException {
        if (ApplicationDependencies.getBackgroundMessageRetriever().retrieveMessages(this.context, this.foregroundServiceDelayMs, new RestStrategy())) {
            Log.i(TAG, "Successfully pulled messages.");
            return;
        }
        throw new PushNetworkException("Failed to pull messages.");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        Log.w(TAG, exc);
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "***** Failed to download pending message!");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushNotificationReceiveJob> {
        public PushNotificationReceiveJob create(Job.Parameters parameters, Data data) {
            return new PushNotificationReceiveJob(parameters, data.getLongOrDefault(PushNotificationReceiveJob.KEY_FOREGROUND_SERVICE_DELAY, -1));
        }
    }
}
