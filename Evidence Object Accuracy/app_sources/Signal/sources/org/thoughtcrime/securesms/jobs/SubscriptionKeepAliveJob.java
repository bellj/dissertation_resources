package org.thoughtcrime.securesms.jobs;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes.dex */
public class SubscriptionKeepAliveJob extends BaseJob {
    private static final long JOB_TIMEOUT = TimeUnit.DAYS.toMillis(3);
    public static final String KEY;
    private static final String TAG = Log.tag(SubscriptionKeepAliveJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public static void enqueueAndTrackTimeIfNecessary() {
        long lastKeepAliveLaunchTime = SignalStore.donationsValues().getLastKeepAliveLaunchTime() + TimeUnit.DAYS.toMillis(3);
        long currentTimeMillis = System.currentTimeMillis();
        if (lastKeepAliveLaunchTime <= currentTimeMillis) {
            enqueueAndTrackTime(currentTimeMillis);
        }
    }

    public static void enqueueAndTrackTime(long j) {
        ApplicationDependencies.getJobManager().add(new SubscriptionKeepAliveJob());
        SignalStore.donationsValues().setLastKeepAliveLaunchTime(j);
    }

    private SubscriptionKeepAliveJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).addConstraint(NetworkConstraint.KEY).setMaxInstancesForQueue(1).setMaxAttempts(-1).setLifespan(JOB_TIMEOUT).build());
    }

    private SubscriptionKeepAliveJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        synchronized (SubscriptionReceiptRequestResponseJob.MUTEX) {
            doRun();
        }
    }

    private void doRun() throws Exception {
        Subscriber subscriber = SignalStore.donationsValues().getSubscriber();
        if (subscriber != null) {
            verifyResponse(ApplicationDependencies.getDonationsService().putSubscription(subscriber.getSubscriberId()).blockingGet());
            String str = TAG;
            Log.i(str, "Successful call to PUT subscription ID", true);
            ServiceResponse<ActiveSubscription> blockingGet = ApplicationDependencies.getDonationsService().getSubscription(subscriber.getSubscriberId()).blockingGet();
            verifyResponse(blockingGet);
            Log.i(str, "Successful call to GET active subscription", true);
            ActiveSubscription activeSubscription = blockingGet.getResult().get();
            if (activeSubscription.getActiveSubscription() == null) {
                Log.i(str, "User does not have a subscription. Exiting.", true);
            } else if (activeSubscription.isFailedPayment()) {
                Log.i(str, "User has a subscription with a failed payment. Marking the payment failure. Status message: " + activeSubscription.getActiveSubscription().getStatus(), true);
                SignalStore.donationsValues().setUnexpectedSubscriptionCancelationChargeFailure(activeSubscription.getChargeFailure());
                SignalStore.donationsValues().setUnexpectedSubscriptionCancelationReason(activeSubscription.getActiveSubscription().getStatus());
                SignalStore.donationsValues().setUnexpectedSubscriptionCancelationTimestamp(activeSubscription.getActiveSubscription().getEndOfCurrentPeriod());
            } else if (!activeSubscription.getActiveSubscription().isActive()) {
                Log.i(str, "User has an inactive subscription. Status message: " + activeSubscription.getActiveSubscription().getStatus() + " Exiting.", true);
            } else if (activeSubscription.getActiveSubscription().getEndOfCurrentPeriod() > SignalStore.donationsValues().getLastEndOfPeriod()) {
                Log.i(str, String.format(Locale.US, "Last end of period change. Requesting receipt refresh. (old: %d to new: %d)", Long.valueOf(SignalStore.donationsValues().getLastEndOfPeriod()), Long.valueOf(activeSubscription.getActiveSubscription().getEndOfCurrentPeriod())), true);
                SubscriptionReceiptRequestResponseJob.createSubscriptionContinuationJobChain(true).enqueue();
            } else {
                Log.i(str, "Subscription is active, and end of current period (remote) is after the latest checked end of period (local). Nothing to do.");
            }
        }
    }

    private <T> void verifyResponse(ServiceResponse<T> serviceResponse) throws Exception {
        if (serviceResponse.getExecutionError().isPresent()) {
            Log.w(TAG, "Failed with an execution error. Scheduling retry.", serviceResponse.getExecutionError().get(), true);
            throw new RetryableException();
        } else if (serviceResponse.getApplicationError().isPresent()) {
            int status = serviceResponse.getStatus();
            if (status == 403 || status == 404) {
                String str = TAG;
                Log.w(str, "Invalid or malformed subscriber id. Status: " + serviceResponse.getStatus(), serviceResponse.getApplicationError().get(), true);
                throw new IOException();
            }
            String str2 = TAG;
            Log.w(str2, "An unknown server error occurred: " + serviceResponse.getStatus(), serviceResponse.getApplicationError().get(), true);
            throw new RetryableException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryableException;
    }

    /* loaded from: classes4.dex */
    public static class RetryableException extends Exception {
        private RetryableException() {
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<SubscriptionKeepAliveJob> {
        public SubscriptionKeepAliveJob create(Job.Parameters parameters, Data data) {
            return new SubscriptionKeepAliveJob(parameters);
        }
    }
}
