package org.thoughtcrime.securesms.jobs;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: RecipientChangedNumberJob.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0002\u0014\u0015B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u000bH\u0014J\u0014\u0010\r\u001a\u00020\u000e2\n\u0010\u000f\u001a\u00060\u0010j\u0002`\u0011H\u0014J\b\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RecipientChangedNumberJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RecipientChangedNumberJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String KEY_RECIPIENT_ID;
    private static final String TAG = Log.tag(RecipientChangedNumberJob.class);
    private final RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RecipientChangedNumberJob(Job.Parameters parameters, RecipientId recipientId) {
        super(parameters);
        Intrinsics.checkNotNullParameter(parameters, "parameters");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.recipientId = recipientId;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RecipientChangedNumberJob(org.thoughtcrime.securesms.recipients.RecipientId r4) {
        /*
            r3 = this;
            java.lang.String r0 = "recipientId"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "RecipientChangedNumberJob_"
            r1.append(r2)
            java.lang.String r2 = r4.toQueueKey()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r0 = r0.build()
            java.lang.String r1 = "Builder().setQueue(\"Reci…d.toQueueKey()}\").build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r3.<init>(r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.RecipientChangedNumberJob.<init>(org.thoughtcrime.securesms.recipients.RecipientId):void");
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data build = new Data.Builder().putString("recipient_id", this.recipientId.serialize()).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n      .putStri…rialize())\n      .build()");
        return build;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        Recipient resolved = Recipient.resolved(this.recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (resolved.isBlocked() || resolved.isGroup() || resolved.isSelf()) {
            String str = TAG;
            Log.i(str, "Number changed but not relevant. blocked: " + resolved.isBlocked() + " isGroup: " + resolved.isGroup() + " isSelf: " + resolved.isSelf());
            return;
        }
        Log.i(TAG, "Writing a number change event.");
        SignalDatabase.Companion.sms().insertNumberChangeMessages(resolved);
    }

    /* compiled from: RecipientChangedNumberJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RecipientChangedNumberJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/RecipientChangedNumberJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<RecipientChangedNumberJob> {
        public RecipientChangedNumberJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            RecipientId from = RecipientId.from(data.getString("recipient_id"));
            Intrinsics.checkNotNullExpressionValue(from, "from(data.getString(KEY_RECIPIENT_ID))");
            return new RecipientChangedNumberJob(parameters, from);
        }
    }

    /* compiled from: RecipientChangedNumberJob.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n \u0007*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/RecipientChangedNumberJob$Companion;", "", "()V", "KEY", "", "KEY_RECIPIENT_ID", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
