package org.thoughtcrime.securesms.jobs;

import j$.util.Optional;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;
import org.thoughtcrime.securesms.transport.InsecureFallbackApprovalException;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;

/* loaded from: classes4.dex */
public class PushTextSendJob extends PushSendJob {
    public static final String KEY;
    private static final String KEY_MESSAGE_ID;
    private static final String TAG = Log.tag(PushTextSendJob.class);
    private final long messageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public PushTextSendJob(long j, Recipient recipient) {
        this(PushSendJob.constructParameters(recipient, false), j);
    }

    private PushTextSendJob(Job.Parameters parameters, long j) {
        super(parameters);
        this.messageId = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putLong("message_id", this.messageId).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onAdded() {
        SignalDatabase.sms().markAsSending(this.messageId);
    }

    @Override // org.thoughtcrime.securesms.jobs.PushSendJob
    public void onPushSend() throws IOException, NoSuchMessageException, UndeliverableMessageException, RetryLaterException {
        RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode;
        SignalLocalMetrics.IndividualMessageSend.onJobStarted(this.messageId);
        ExpiringMessageManager expiringMessageManager = ApplicationDependencies.getExpiringMessageManager();
        SmsDatabase sms = SignalDatabase.sms();
        SmsMessageRecord smsMessage = sms.getSmsMessage(this.messageId);
        if (smsMessage.isPending() || smsMessage.isFailed()) {
            try {
                String str = TAG;
                String valueOf = String.valueOf(smsMessage.getDateSent());
                log(str, valueOf, "Sending message: " + this.messageId + ",  Recipient: " + smsMessage.getRecipient().getId() + ", Thread: " + smsMessage.getThreadId());
                RecipientUtil.shareProfileIfFirstSecureMessage(this.context, smsMessage.getRecipient());
                Recipient resolve = smsMessage.getRecipient().resolve();
                byte[] profileKey = resolve.getProfileKey();
                RecipientDatabase.UnidentifiedAccessMode unidentifiedAccessMode2 = resolve.getUnidentifiedAccessMode();
                boolean deliver = deliver(smsMessage);
                sms.markAsSent(this.messageId, true);
                sms.markUnidentified(this.messageId, deliver);
                if (resolve.isSelf()) {
                    MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(resolve.getId(), smsMessage.getDateSent());
                    SignalDatabase.mmsSms().incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
                    SignalDatabase.mmsSms().incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
                }
                if (deliver && unidentifiedAccessMode2 == RecipientDatabase.UnidentifiedAccessMode.UNKNOWN && profileKey == null) {
                    log(str, String.valueOf(smsMessage.getDateSent()), "Marking recipient as UD-unrestricted following a UD send.");
                    SignalDatabase.recipients().setUnidentifiedAccessMode(resolve.getId(), RecipientDatabase.UnidentifiedAccessMode.UNRESTRICTED);
                } else if (deliver && unidentifiedAccessMode2 == RecipientDatabase.UnidentifiedAccessMode.UNKNOWN) {
                    log(str, String.valueOf(smsMessage.getDateSent()), "Marking recipient as UD-enabled following a UD send.");
                    SignalDatabase.recipients().setUnidentifiedAccessMode(resolve.getId(), RecipientDatabase.UnidentifiedAccessMode.ENABLED);
                } else if (!deliver && unidentifiedAccessMode2 != (unidentifiedAccessMode = RecipientDatabase.UnidentifiedAccessMode.DISABLED)) {
                    log(str, String.valueOf(smsMessage.getDateSent()), "Marking recipient as UD-disabled following a non-UD send.");
                    SignalDatabase.recipients().setUnidentifiedAccessMode(resolve.getId(), unidentifiedAccessMode);
                }
                if (smsMessage.getExpiresIn() > 0) {
                    sms.markExpireStarted(this.messageId);
                    expiringMessageManager.scheduleDeletion(smsMessage.getId(), smsMessage.isMms(), smsMessage.getExpiresIn());
                }
                String valueOf2 = String.valueOf(smsMessage.getDateSent());
                log(str, valueOf2, "Sent message: " + this.messageId);
            } catch (InsecureFallbackApprovalException e) {
                warn(TAG, String.valueOf(smsMessage.getDateSent()), "Failure", e);
                sms.markAsPendingInsecureSmsFallback(smsMessage.getId());
                ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(this.context, smsMessage.getRecipient(), ConversationId.forConversation(smsMessage.getThreadId()));
                ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
            } catch (UntrustedIdentityException e2) {
                warn(TAG, String.valueOf(smsMessage.getDateSent()), "Failure", e2);
                RecipientId id = Recipient.external(this.context, e2.getIdentifier()).getId();
                sms.addMismatchedIdentity(smsMessage.getId(), id, e2.getIdentityKey());
                sms.markAsSentFailed(smsMessage.getId());
                sms.markAsPush(smsMessage.getId());
                RetrieveProfileJob.enqueue(id);
            } catch (ProofRequiredException e3) {
                PushSendJob.handleProofRequiredException(this.context, e3, smsMessage.getRecipient(), smsMessage.getThreadId(), this.messageId, false);
            }
            SignalLocalMetrics.IndividualMessageSend.onJobFinished(this.messageId);
            return;
        }
        String str2 = TAG;
        String valueOf3 = String.valueOf(smsMessage.getDateSent());
        warn(str2, valueOf3, "Message " + this.messageId + " was already sent. Ignoring.");
    }

    @Override // org.thoughtcrime.securesms.jobs.PushSendJob, org.thoughtcrime.securesms.jobmanager.Job
    public void onRetry() {
        SignalLocalMetrics.IndividualMessageSend.cancel(this.messageId);
        super.onRetry();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        SignalDatabase.sms().markAsSentFailed(this.messageId);
        long threadIdForMessage = SignalDatabase.sms().getThreadIdForMessage(this.messageId);
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(threadIdForMessage);
        if (threadIdForMessage != -1 && recipientForThreadId != null) {
            ApplicationDependencies.getMessageNotifier().notifyMessageDeliveryFailed(this.context, recipientForThreadId, ConversationId.forConversation(threadIdForMessage));
        }
    }

    private boolean deliver(SmsMessageRecord smsMessageRecord) throws UntrustedIdentityException, InsecureFallbackApprovalException, UndeliverableMessageException, IOException {
        try {
            rotateSenderCertificateIfNecessary();
            Recipient resolve = smsMessageRecord.getIndividualRecipient().resolve();
            if (!resolve.isUnregistered()) {
                SignalServiceMessageSender signalServiceMessageSender = ApplicationDependencies.getSignalServiceMessageSender();
                SignalServiceAddress signalServiceAddress = RecipientUtil.toSignalServiceAddress(this.context, resolve);
                Optional<byte[]> profileKey = getProfileKey(resolve);
                Optional<UnidentifiedAccessPair> accessFor = UnidentifiedAccessUtil.getAccessFor(this.context, resolve);
                String str = TAG;
                String valueOf = String.valueOf(smsMessageRecord.getDateSent());
                log(str, valueOf, "Have access key to use: " + accessFor.isPresent());
                SignalServiceDataMessage build = SignalServiceDataMessage.newBuilder().withTimestamp(smsMessageRecord.getDateSent()).withBody(smsMessageRecord.getBody()).withExpiration((int) (smsMessageRecord.getExpiresIn() / 1000)).withProfileKey(profileKey.orElse(null)).asEndSessionMessage(smsMessageRecord.isEndSession()).build();
                if (Util.equals(SignalStore.account().getAci(), signalServiceAddress.getServiceId())) {
                    Optional<UnidentifiedAccessPair> accessForSync = UnidentifiedAccessUtil.getAccessForSync(this.context);
                    SignalLocalMetrics.IndividualMessageSend.onDeliveryStarted(this.messageId);
                    SignalDatabase.messageLog().insertIfPossible(resolve.getId(), smsMessageRecord.getDateSent(), signalServiceMessageSender.sendSyncMessage(build), ContentHint.RESENDABLE, new MessageId(this.messageId, false));
                    return accessForSync.isPresent();
                }
                SignalLocalMetrics.IndividualMessageSend.onDeliveryStarted(this.messageId);
                ContentHint contentHint = ContentHint.RESENDABLE;
                SendMessageResult sendDataMessage = signalServiceMessageSender.sendDataMessage(signalServiceAddress, accessFor, contentHint, build, new MetricEventListener(this.messageId));
                SignalDatabase.messageLog().insertIfPossible(resolve.getId(), smsMessageRecord.getDateSent(), sendDataMessage, contentHint, new MessageId(this.messageId, false));
                return sendDataMessage.getSuccess().isUnidentified();
            }
            throw new UndeliverableMessageException(resolve.getId() + " not registered!");
        } catch (ServerRejectedException e) {
            throw new UndeliverableMessageException(e);
        } catch (UnregisteredUserException e2) {
            warn(TAG, "Failure", e2);
            throw new InsecureFallbackApprovalException(e2);
        }
    }

    public static long getMessageId(Data data) {
        return data.getLong("message_id");
    }

    /* loaded from: classes4.dex */
    public static class MetricEventListener implements SignalServiceMessageSender.IndividualSendEvents {
        private final long messageId;

        private MetricEventListener(long j) {
            this.messageId = j;
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents
        public void onMessageEncrypted() {
            SignalLocalMetrics.IndividualMessageSend.onMessageEncrypted(this.messageId);
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents
        public void onMessageSent() {
            SignalLocalMetrics.IndividualMessageSend.onMessageSent(this.messageId);
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents
        public void onSyncMessageSent() {
            SignalLocalMetrics.IndividualMessageSend.onSyncMessageSent(this.messageId);
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PushTextSendJob> {
        public PushTextSendJob create(Job.Parameters parameters, Data data) {
            return new PushTextSendJob(parameters, data.getLong("message_id"));
        }
    }
}
