package org.thoughtcrime.securesms.jobs;

import j$.util.function.Function;
import org.thoughtcrime.securesms.badges.Badges;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class RefreshOwnProfileJob$$ExternalSyntheticLambda11 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return Badges.fromServiceBadge((SignalServiceProfile.Badge) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
