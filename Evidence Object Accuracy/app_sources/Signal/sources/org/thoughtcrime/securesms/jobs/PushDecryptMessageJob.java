package org.thoughtcrime.securesms.jobs;

import android.app.PendingIntent;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import java.util.LinkedList;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.message.SenderKeyDistributionMessage;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.messages.MessageContentProcessor;
import org.thoughtcrime.securesms.messages.MessageDecryptionUtil;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes4.dex */
public final class PushDecryptMessageJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_ENVELOPE;
    private static final String KEY_SMS_MESSAGE_ID;
    public static final String QUEUE;
    public static final String TAG = Log.tag(PushDecryptMessageJob.class);
    private final SignalServiceEnvelope envelope;
    private final long smsMessageId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean shouldTrace() {
        return true;
    }

    public PushDecryptMessageJob(Context context, SignalServiceEnvelope signalServiceEnvelope) {
        this(context, signalServiceEnvelope, -1);
    }

    public PushDecryptMessageJob(Context context, SignalServiceEnvelope signalServiceEnvelope, long j) {
        this(new Job.Parameters.Builder().setQueue(QUEUE).setMaxAttempts(-1).build(), signalServiceEnvelope, j);
        setContext(context);
    }

    private PushDecryptMessageJob(Job.Parameters parameters, SignalServiceEnvelope signalServiceEnvelope, long j) {
        super(parameters);
        this.envelope = signalServiceEnvelope;
        this.smsMessageId = j;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putBlobAsString(KEY_ENVELOPE, this.envelope.serialize()).putLong(KEY_SMS_MESSAGE_ID, this.smsMessageId).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public void onRun() throws RetryLaterException {
        if (!needsMigration()) {
            LinkedList<Job> linkedList = new LinkedList();
            MessageDecryptionUtil.DecryptionResult decrypt = MessageDecryptionUtil.decrypt(this.context, this.envelope);
            if (decrypt.getContent() != null) {
                if (decrypt.getContent().getSenderKeyDistributionMessage().isPresent()) {
                    handleSenderKeyDistributionMessage(decrypt.getContent().getSender(), decrypt.getContent().getSenderDevice(), decrypt.getContent().getSenderKeyDistributionMessage().get());
                }
                linkedList.add(new PushProcessMessageJob(decrypt.getContent(), this.smsMessageId, this.envelope.getTimestamp()));
            } else if (!(decrypt.getException() == null || decrypt.getState() == MessageContentProcessor.MessageState.NOOP)) {
                linkedList.add(new PushProcessMessageJob(decrypt.getState(), decrypt.getException(), this.smsMessageId, this.envelope.getTimestamp()));
            }
            linkedList.addAll(decrypt.getJobs());
            for (Job job : linkedList) {
                ApplicationDependencies.getJobManager().add(job);
            }
            return;
        }
        Log.w(TAG, "Migration is still needed.");
        postMigrationNotification();
        throw new RetryLaterException();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    public boolean onShouldRetry(Exception exc) {
        return exc instanceof RetryLaterException;
    }

    private void handleSenderKeyDistributionMessage(SignalServiceAddress signalServiceAddress, int i, SenderKeyDistributionMessage senderKeyDistributionMessage) {
        Log.i(TAG, "Processing SenderKeyDistributionMessage.");
        ApplicationDependencies.getSignalServiceMessageSender().processSenderKeyDistributionMessage(new SignalProtocolAddress(signalServiceAddress.getIdentifier(), i), senderKeyDistributionMessage);
    }

    private boolean needsMigration() {
        return TextSecurePreferences.getNeedsSqlCipherMigration(this.context);
    }

    private void postMigrationNotification() {
        NotificationManagerCompat from = NotificationManagerCompat.from(this.context);
        Context context = this.context;
        NotificationCompat.Builder contentText = new NotificationCompat.Builder(context, NotificationChannels.getMessagesChannel(context)).setSmallIcon(R.drawable.ic_notification).setPriority(1).setCategory("msg").setContentTitle(this.context.getString(R.string.PushDecryptJob_new_locked_message)).setContentText(this.context.getString(R.string.PushDecryptJob_unlock_to_view_pending_messages));
        Context context2 = this.context;
        from.notify(NotificationIds.LEGACY_SQLCIPHER_MIGRATION, contentText.setContentIntent(PendingIntent.getActivity(context2, 0, MainActivity.clearTop(context2), 0)).setDefaults(3).build());
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PushDecryptMessageJob> {
        public PushDecryptMessageJob create(Job.Parameters parameters, Data data) {
            return new PushDecryptMessageJob(parameters, SignalServiceEnvelope.deserialize(data.getStringAsBlob(PushDecryptMessageJob.KEY_ENVELOPE)), data.getLong(PushDecryptMessageJob.KEY_SMS_MESSAGE_ID));
        }
    }
}
