package org.thoughtcrime.securesms.jobs;

import androidx.core.util.Preconditions;
import java.io.IOException;
import java.util.ArrayList;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.IncomingSticker;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.stickers.BlessedPacks;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.messages.SignalServiceStickerManifest;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class StickerPackDownloadJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_NOTIFY;
    private static final String KEY_PACK_ID;
    private static final String KEY_PACK_KEY;
    private static final String KEY_REFERENCE_PACK;
    private static final String TAG = Log.tag(StickerPackDownloadJob.class);
    private final boolean isReferencePack;
    private final boolean notify;
    private final String packId;
    private final String packKey;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    public static StickerPackDownloadJob forInstall(String str, String str2, boolean z) {
        return new StickerPackDownloadJob(str, str2, false, z);
    }

    public static StickerPackDownloadJob forReference(String str, String str2) {
        return new StickerPackDownloadJob(str, str2, true, true);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private StickerPackDownloadJob(java.lang.String r8, java.lang.String r9, boolean r10, boolean r11) {
        /*
            r7 = this;
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = new org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder
            r0.<init>()
            java.lang.String r1 = "NetworkConstraint"
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.addConstraint(r1)
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS
            r2 = 30
            long r1 = r1.toMillis(r2)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setLifespan(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "StickerPackDownloadJob_"
            r1.append(r2)
            r1.append(r8)
            java.lang.String r1 = r1.toString()
            org.thoughtcrime.securesms.jobmanager.Job$Parameters$Builder r0 = r0.setQueue(r1)
            org.thoughtcrime.securesms.jobmanager.Job$Parameters r2 = r0.build()
            r1 = r7
            r3 = r8
            r4 = r9
            r5 = r10
            r6 = r11
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.StickerPackDownloadJob.<init>(java.lang.String, java.lang.String, boolean, boolean):void");
    }

    private StickerPackDownloadJob(Job.Parameters parameters, String str, String str2, boolean z, boolean z2) {
        super(parameters);
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(str2);
        this.packId = str;
        this.packKey = str2;
        this.isReferencePack = z;
        this.notify = z2;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_PACK_ID, this.packId).putString(KEY_PACK_KEY, this.packKey).putBoolean(KEY_REFERENCE_PACK, this.isReferencePack).putBoolean(KEY_NOTIFY, this.notify).build();
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws IOException, InvalidMessageException {
        if (this.isReferencePack && !SignalDatabase.attachments().containsStickerPackId(this.packId) && !BlessedPacks.contains(this.packId)) {
            Log.w(TAG, "There are no attachments with the requested packId present for this reference pack. Skipping.");
        } else if (!this.isReferencePack || !SignalDatabase.stickers().isPackAvailableAsReference(this.packId)) {
            SignalServiceMessageReceiver signalServiceMessageReceiver = ApplicationDependencies.getSignalServiceMessageReceiver();
            JobManager jobManager = ApplicationDependencies.getJobManager();
            StickerDatabase stickers = SignalDatabase.stickers();
            SignalServiceStickerManifest retrieveStickerManifest = signalServiceMessageReceiver.retrieveStickerManifest(Hex.fromStringCondensed(this.packId), Hex.fromStringCondensed(this.packKey));
            if (retrieveStickerManifest.getStickers().isEmpty()) {
                Log.w(TAG, "No stickers in pack!");
                return;
            }
            if (!this.isReferencePack && stickers.isPackAvailableAsReference(this.packId)) {
                stickers.markPackAsInstalled(this.packId, this.notify);
            }
            SignalServiceStickerManifest.StickerInfo orElse = retrieveStickerManifest.getCover().orElse(retrieveStickerManifest.getStickers().get(0));
            JobManager.Chain startChain = jobManager.startChain(new StickerDownloadJob(new IncomingSticker(this.packId, this.packKey, retrieveStickerManifest.getTitle().orElse(""), retrieveStickerManifest.getAuthor().orElse(""), orElse.getId(), "", orElse.getContentType(), true, !this.isReferencePack), this.notify));
            if (!this.isReferencePack) {
                ArrayList arrayList = new ArrayList(retrieveStickerManifest.getStickers().size());
                for (SignalServiceStickerManifest.StickerInfo stickerInfo : retrieveStickerManifest.getStickers()) {
                    arrayList.add(new StickerDownloadJob(new IncomingSticker(this.packId, this.packKey, retrieveStickerManifest.getTitle().orElse(""), retrieveStickerManifest.getAuthor().orElse(""), stickerInfo.getId(), stickerInfo.getEmoji(), stickerInfo.getContentType(), false, true), this.notify));
                }
                startChain.then(arrayList);
            }
            startChain.enqueue();
        } else {
            Log.i(TAG, "Sticker pack already available for reference. Skipping.");
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof PushNetworkException;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        Log.w(TAG, "Failed to download manifest! Uninstalling pack.");
        SignalDatabase.stickers().uninstallPack(this.packId);
        SignalDatabase.stickers().deleteOrphanedPacks();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<StickerPackDownloadJob> {
        public StickerPackDownloadJob create(Job.Parameters parameters, Data data) {
            return new StickerPackDownloadJob(parameters, data.getString(StickerPackDownloadJob.KEY_PACK_ID), data.getString(StickerPackDownloadJob.KEY_PACK_KEY), data.getBoolean(StickerPackDownloadJob.KEY_REFERENCE_PACK), data.getBoolean(StickerPackDownloadJob.KEY_NOTIFY));
        }
    }
}
