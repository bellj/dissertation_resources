package org.thoughtcrime.securesms.jobs;

import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class GroupCallPeekWorkerJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_GROUP_RECIPIENT_ID;
    private final RecipientId groupRecipientId;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return false;
    }

    public GroupCallPeekWorkerJob(RecipientId recipientId) {
        this(new Job.Parameters.Builder().setQueue(PushProcessMessageJob.getQueueName(recipientId)).setMaxInstancesForQueue(2).build(), recipientId);
    }

    private GroupCallPeekWorkerJob(Job.Parameters parameters, RecipientId recipientId) {
        super(parameters);
        this.groupRecipientId = recipientId;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() {
        ApplicationDependencies.getSignalCallManager().peekGroupCall(this.groupRecipientId);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putString(KEY_GROUP_RECIPIENT_ID, this.groupRecipientId.serialize()).build();
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<GroupCallPeekWorkerJob> {
        public GroupCallPeekWorkerJob create(Job.Parameters parameters, Data data) {
            return new GroupCallPeekWorkerJob(parameters, RecipientId.from(data.getString(GroupCallPeekWorkerJob.KEY_GROUP_RECIPIENT_ID)));
        }
    }
}
