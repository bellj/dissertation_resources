package org.thoughtcrime.securesms.jobs;

import android.content.Context;
import androidx.core.content.ContextCompat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarRenderer;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.transport.RetryLaterException;

/* compiled from: CreateReleaseChannelJob.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0002\u0014\u0015B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\bH\u0014J\u0014\u0010\n\u001a\u00020\u000b2\n\u0010\f\u001a\u00060\rj\u0002`\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/CreateReleaseChannelJob;", "Lorg/thoughtcrime/securesms/jobs/BaseJob;", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "(Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;)V", "getFactoryKey", "", "onFailure", "", "onRun", "onShouldRetry", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "serialize", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "setAvatar", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CreateReleaseChannelJob extends BaseJob {
    public static final Companion Companion = new Companion(null);
    public static final String KEY;
    private static final String TAG = Log.tag(CreateReleaseChannelJob.class);

    public /* synthetic */ CreateReleaseChannelJob(Job.Parameters parameters, DefaultConstructorMarker defaultConstructorMarker) {
        this(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    /* compiled from: CreateReleaseChannelJob.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/CreateReleaseChannelJob$Companion;", "", "()V", "KEY", "", "TAG", "kotlin.jvm.PlatformType", "create", "Lorg/thoughtcrime/securesms/jobs/CreateReleaseChannelJob;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final CreateReleaseChannelJob create() {
            Job.Parameters build = new Job.Parameters.Builder().setQueue(CreateReleaseChannelJob.KEY).setMaxInstancesForFactory(1).setMaxAttempts(3).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder()\n          .set…pts(3)\n          .build()");
            return new CreateReleaseChannelJob(build, null);
        }
    }

    private CreateReleaseChannelJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        Data data = Data.EMPTY;
        Intrinsics.checkNotNullExpressionValue(data, "EMPTY");
        return data;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0065, code lost:
        if ((r1.length() == 0) == true) goto L_0x0069;
     */
    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onRun() {
        /*
            r4 = this;
            org.thoughtcrime.securesms.keyvalue.AccountValues r0 = org.thoughtcrime.securesms.keyvalue.SignalStore.account()
            boolean r0 = r0.isRegistered()
            if (r0 != 0) goto L_0x0012
            java.lang.String r0 = org.thoughtcrime.securesms.jobs.CreateReleaseChannelJob.TAG
            java.lang.String r1 = "Not registered, skipping."
            org.signal.core.util.logging.Log.i(r0, r1)
            return
        L_0x0012:
            org.thoughtcrime.securesms.keyvalue.ReleaseChannelValues r0 = org.thoughtcrime.securesms.keyvalue.SignalStore.releaseChannelValues()
            org.thoughtcrime.securesms.recipients.RecipientId r0 = r0.getReleaseChannelRecipientId()
            if (r0 == 0) goto L_0x0078
            java.lang.String r0 = org.thoughtcrime.securesms.jobs.CreateReleaseChannelJob.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Already created Release Channel recipient "
            r1.append(r2)
            org.thoughtcrime.securesms.keyvalue.ReleaseChannelValues r2 = org.thoughtcrime.securesms.keyvalue.SignalStore.releaseChannelValues()
            org.thoughtcrime.securesms.recipients.RecipientId r2 = r2.getReleaseChannelRecipientId()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.signal.core.util.logging.Log.i(r0, r1)
            org.thoughtcrime.securesms.keyvalue.ReleaseChannelValues r0 = org.thoughtcrime.securesms.keyvalue.SignalStore.releaseChannelValues()
            org.thoughtcrime.securesms.recipients.RecipientId r0 = r0.getReleaseChannelRecipientId()
            kotlin.jvm.internal.Intrinsics.checkNotNull(r0)
            org.thoughtcrime.securesms.recipients.Recipient r0 = org.thoughtcrime.securesms.recipients.Recipient.resolved(r0)
            java.lang.String r1 = "resolved(SignalStore.rel…easeChannelRecipientId!!)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            java.lang.String r1 = r0.getProfileAvatar()
            if (r1 == 0) goto L_0x006b
            java.lang.String r1 = r0.getProfileAvatar()
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0068
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0064
            r1 = 1
            goto L_0x0065
        L_0x0064:
            r1 = 0
        L_0x0065:
            if (r1 != r2) goto L_0x0068
            goto L_0x0069
        L_0x0068:
            r2 = 0
        L_0x0069:
            if (r2 == 0) goto L_0x00a2
        L_0x006b:
            org.thoughtcrime.securesms.recipients.RecipientId r0 = r0.getId()
            java.lang.String r1 = "recipient.id"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r4.setAvatar(r0)
            goto L_0x00a2
        L_0x0078:
            org.thoughtcrime.securesms.database.SignalDatabase$Companion r0 = org.thoughtcrime.securesms.database.SignalDatabase.Companion
            org.thoughtcrime.securesms.database.RecipientDatabase r0 = r0.recipients()
            org.thoughtcrime.securesms.recipients.RecipientId r1 = r0.insertReleaseChannelRecipient()
            org.thoughtcrime.securesms.keyvalue.ReleaseChannelValues r2 = org.thoughtcrime.securesms.keyvalue.SignalStore.releaseChannelValues()
            r2.setReleaseChannelRecipientId(r1)
            java.lang.String r2 = "Signal"
            org.thoughtcrime.securesms.profiles.ProfileName r2 = org.thoughtcrime.securesms.profiles.ProfileName.asGiven(r2)
            java.lang.String r3 = "asGiven(\"Signal\")"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            r0.setProfileName(r1, r2)
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0.setMuted(r1, r2)
            r4.setAvatar(r1)
        L_0x00a2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.jobs.CreateReleaseChannelJob.onRun():void");
    }

    private final void setAvatar(RecipientId recipientId) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AvatarRenderer avatarRenderer = AvatarRenderer.INSTANCE;
        Context context = this.context;
        Intrinsics.checkNotNullExpressionValue(context, "context");
        avatarRenderer.renderAvatar(context, new Avatar.Resource(R.drawable.ic_signal_logo_large, new Avatars.ColorPair(ContextCompat.getColor(this.context, R.color.core_ultramarine), ContextCompat.getColor(this.context, R.color.core_white), "")), new Function1<Media, Unit>(this, recipientId, countDownLatch) { // from class: org.thoughtcrime.securesms.jobs.CreateReleaseChannelJob$setAvatar$1
            final /* synthetic */ RecipientId $id;
            final /* synthetic */ CountDownLatch $latch;
            final /* synthetic */ CreateReleaseChannelJob this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$id = r2;
                this.$latch = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Media media) {
                invoke(media);
                return Unit.INSTANCE;
            }

            public final void invoke(Media media) {
                Intrinsics.checkNotNullParameter(media, "media");
                AvatarHelper.setAvatar(this.this$0.context, this.$id, BlobProvider.getInstance().getStream(this.this$0.context, media.getUri()));
                SignalDatabase.Companion.recipients().setProfileAvatar(this.$id, "local");
                this.$latch.countDown();
            }
        }, new Function1<Throwable, Unit>(countDownLatch) { // from class: org.thoughtcrime.securesms.jobs.CreateReleaseChannelJob$setAvatar$2
            final /* synthetic */ CountDownLatch $latch;

            /* access modifiers changed from: package-private */
            {
                this.$latch = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Log.w(CreateReleaseChannelJob.TAG, th);
                this.$latch.countDown();
            }
        });
        try {
            if (!countDownLatch.await(30, TimeUnit.SECONDS)) {
                throw new RetryLaterException();
            }
        } catch (InterruptedException unused) {
            throw new RetryLaterException();
        }
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        Intrinsics.checkNotNullParameter(exc, "e");
        return exc instanceof RetryLaterException;
    }

    /* compiled from: CreateReleaseChannelJob.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/jobs/CreateReleaseChannelJob$Factory;", "Lorg/thoughtcrime/securesms/jobmanager/Job$Factory;", "Lorg/thoughtcrime/securesms/jobs/CreateReleaseChannelJob;", "()V", "create", "parameters", "Lorg/thoughtcrime/securesms/jobmanager/Job$Parameters;", "data", "Lorg/thoughtcrime/securesms/jobmanager/Data;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<CreateReleaseChannelJob> {
        public CreateReleaseChannelJob create(Job.Parameters parameters, Data data) {
            Intrinsics.checkNotNullParameter(parameters, "parameters");
            Intrinsics.checkNotNullParameter(data, "data");
            return new CreateReleaseChannelJob(parameters, null);
        }
    }
}
