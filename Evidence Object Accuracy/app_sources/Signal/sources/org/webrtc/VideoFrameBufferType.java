package org.webrtc;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
/* loaded from: classes.dex */
public @interface VideoFrameBufferType {
    public static final int I010;
    public static final int I420;
    public static final int I420A;
    public static final int I444;
    public static final int NATIVE;
    public static final int NV12;
}
