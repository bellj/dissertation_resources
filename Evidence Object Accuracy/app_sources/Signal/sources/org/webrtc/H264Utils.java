package org.webrtc;

import java.util.HashMap;
import java.util.Map;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* loaded from: classes5.dex */
public class H264Utils {
    public static VideoCodecInfo DEFAULT_H264_BASELINE_PROFILE_CODEC = new VideoCodecInfo("H264", getDefaultH264Params(false));
    public static VideoCodecInfo DEFAULT_H264_HIGH_PROFILE_CODEC = new VideoCodecInfo("H264", getDefaultH264Params(true));
    public static final String H264_CONSTRAINED_BASELINE_3_1;
    public static final String H264_CONSTRAINED_HIGH_3_1;
    public static final String H264_FMTP_LEVEL_ASYMMETRY_ALLOWED;
    public static final String H264_FMTP_PACKETIZATION_MODE;
    public static final String H264_FMTP_PROFILE_LEVEL_ID;
    public static final String H264_LEVEL_3_1;
    public static final String H264_PROFILE_CONSTRAINED_BASELINE;
    public static final String H264_PROFILE_CONSTRAINED_HIGH;

    private static native boolean nativeIsSameH264Profile(Map<String, String> map, Map<String, String> map2);

    H264Utils() {
    }

    public static Map<String, String> getDefaultH264Params(boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("level-asymmetry-allowed", SubscriptionLevels.BOOST_LEVEL);
        hashMap.put("packetization-mode", SubscriptionLevels.BOOST_LEVEL);
        hashMap.put("profile-level-id", z ? "640c1f" : "42e01f");
        return hashMap;
    }

    public static boolean isSameH264Profile(Map<String, String> map, Map<String, String> map2) {
        return nativeIsSameH264Profile(map, map2);
    }
}
