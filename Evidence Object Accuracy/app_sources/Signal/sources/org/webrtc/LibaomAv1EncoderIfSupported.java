package org.webrtc;

/* loaded from: classes5.dex */
public class LibaomAv1EncoderIfSupported extends WrappedNativeVideoEncoder {
    static native long nativeCreateEncoder();

    /* access modifiers changed from: package-private */
    public static native boolean nativeIsSupported();

    @Override // org.webrtc.WrappedNativeVideoEncoder, org.webrtc.VideoEncoder
    public boolean isHardwareEncoder() {
        return false;
    }

    @Override // org.webrtc.WrappedNativeVideoEncoder, org.webrtc.VideoEncoder
    public long createNativeVideoEncoder() {
        return nativeCreateEncoder();
    }
}
