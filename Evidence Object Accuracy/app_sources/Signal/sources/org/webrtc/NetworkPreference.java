package org.webrtc;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
/* loaded from: classes.dex */
public @interface NetworkPreference {
    public static final int NEUTRAL;
    public static final int NOT_PREFERRED;
}
