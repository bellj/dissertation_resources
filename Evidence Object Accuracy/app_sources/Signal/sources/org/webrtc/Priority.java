package org.webrtc;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
/* loaded from: classes.dex */
public @interface Priority {
    public static final int HIGH;
    public static final int LOW;
    public static final int MEDIUM;
    public static final int VERY_LOW;
}
