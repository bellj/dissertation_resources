package org.webrtc;

import org.webrtc.Camera1Session;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class Camera1Session$2$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Camera1Session.AnonymousClass2 f$0;
    public final /* synthetic */ byte[] f$1;

    public /* synthetic */ Camera1Session$2$$ExternalSyntheticLambda1(Camera1Session.AnonymousClass2 r1, byte[] bArr) {
        this.f$0 = r1;
        this.f$1 = bArr;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onPreviewFrame$1(this.f$1);
    }
}
