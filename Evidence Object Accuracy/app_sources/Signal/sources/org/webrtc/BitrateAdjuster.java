package org.webrtc;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public interface BitrateAdjuster {
    int getAdjustedBitrateBps();

    double getAdjustedFramerateFps();

    void reportEncodedFrame(int i);

    void setTargets(int i, double d);
}
