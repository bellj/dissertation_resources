package org.webrtc;

import java.io.IOException;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public interface MediaCodecWrapperFactory {
    MediaCodecWrapper createByCodecName(String str) throws IOException;
}
