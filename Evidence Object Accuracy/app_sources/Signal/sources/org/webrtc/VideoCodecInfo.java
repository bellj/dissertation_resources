package org.webrtc;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

/* loaded from: classes5.dex */
public class VideoCodecInfo {
    public static final String H264_CONSTRAINED_BASELINE_3_1;
    public static final String H264_CONSTRAINED_HIGH_3_1;
    public static final String H264_FMTP_LEVEL_ASYMMETRY_ALLOWED;
    public static final String H264_FMTP_PACKETIZATION_MODE;
    public static final String H264_FMTP_PROFILE_LEVEL_ID;
    public static final String H264_LEVEL_3_1;
    public static final String H264_PROFILE_CONSTRAINED_BASELINE;
    public static final String H264_PROFILE_CONSTRAINED_HIGH;
    public final String name;
    public final Map<String, String> params;
    @Deprecated
    public final int payload;

    @CalledByNative
    public VideoCodecInfo(String str, Map<String, String> map) {
        this.payload = 0;
        this.name = str;
        this.params = map;
    }

    @Deprecated
    public VideoCodecInfo(int i, String str, Map<String, String> map) {
        this.payload = i;
        this.name = str;
        this.params = map;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof VideoCodecInfo)) {
            return false;
        }
        VideoCodecInfo videoCodecInfo = (VideoCodecInfo) obj;
        if (!this.name.equalsIgnoreCase(videoCodecInfo.name) || !this.params.equals(videoCodecInfo.params)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.name.toUpperCase(Locale.ROOT), this.params});
    }

    public String toString() {
        return "VideoCodec{" + this.name + " " + this.params + "}";
    }

    @CalledByNative
    public String getName() {
        return this.name;
    }

    @CalledByNative
    Map getParams() {
        return this.params;
    }
}
