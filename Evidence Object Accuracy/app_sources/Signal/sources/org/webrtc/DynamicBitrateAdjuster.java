package org.webrtc;

/* loaded from: classes5.dex */
public class DynamicBitrateAdjuster extends BaseBitrateAdjuster {
    private static final double BITRATE_ADJUSTMENT_MAX_SCALE;
    private static final double BITRATE_ADJUSTMENT_SEC;
    private static final int BITRATE_ADJUSTMENT_STEPS;
    private static final double BITS_PER_BYTE;
    private int bitrateAdjustmentScaleExp;
    private double deviationBytes;
    private double timeSinceLastAdjustmentMs;

    @Override // org.webrtc.BaseBitrateAdjuster, org.webrtc.BitrateAdjuster
    public void setTargets(int i, double d) {
        int i2 = this.targetBitrateBps;
        if (i2 > 0 && i < i2) {
            double d2 = this.deviationBytes;
            double d3 = (double) i;
            Double.isNaN(d3);
            double d4 = d2 * d3;
            double d5 = (double) i2;
            Double.isNaN(d5);
            this.deviationBytes = d4 / d5;
        }
        super.setTargets(i, d);
    }

    @Override // org.webrtc.BaseBitrateAdjuster, org.webrtc.BitrateAdjuster
    public void reportEncodedFrame(int i) {
        double d = this.targetFramerateFps;
        if (d != 0.0d) {
            int i2 = this.targetBitrateBps;
            double d2 = (double) i2;
            Double.isNaN(d2);
            double d3 = this.deviationBytes;
            double d4 = (double) i;
            Double.isNaN(d4);
            double d5 = d3 + (d4 - ((d2 / BITS_PER_BYTE) / d));
            this.deviationBytes = d5;
            this.timeSinceLastAdjustmentMs += 1000.0d / d;
            double d6 = (double) i2;
            Double.isNaN(d6);
            double d7 = d6 / BITS_PER_BYTE;
            double d8 = BITRATE_ADJUSTMENT_SEC * d7;
            double min = Math.min(d5, d8);
            this.deviationBytes = min;
            double max = Math.max(min, -d8);
            this.deviationBytes = max;
            if (this.timeSinceLastAdjustmentMs > 3000.0d) {
                if (max > d7) {
                    int i3 = this.bitrateAdjustmentScaleExp - ((int) ((max / d7) + 0.5d));
                    this.bitrateAdjustmentScaleExp = i3;
                    this.bitrateAdjustmentScaleExp = Math.max(i3, -20);
                    this.deviationBytes = d7;
                } else {
                    double d9 = -d7;
                    if (max < d9) {
                        int i4 = this.bitrateAdjustmentScaleExp + ((int) (((-max) / d7) + 0.5d));
                        this.bitrateAdjustmentScaleExp = i4;
                        this.bitrateAdjustmentScaleExp = Math.min(i4, 20);
                        this.deviationBytes = d9;
                    }
                }
                this.timeSinceLastAdjustmentMs = 0.0d;
            }
        }
    }

    private double getBitrateAdjustmentScale() {
        double d = (double) this.bitrateAdjustmentScaleExp;
        Double.isNaN(d);
        return Math.pow(BITRATE_ADJUSTMENT_MAX_SCALE, d / 20.0d);
    }

    @Override // org.webrtc.BaseBitrateAdjuster, org.webrtc.BitrateAdjuster
    public int getAdjustedBitrateBps() {
        double d = (double) this.targetBitrateBps;
        double bitrateAdjustmentScale = getBitrateAdjustmentScale();
        Double.isNaN(d);
        return (int) (d * bitrateAdjustmentScale);
    }
}
