package org.webrtc;

import org.webrtc.VideoDecoder;

/* loaded from: classes5.dex */
class VideoDecoderWrapper {
    /* access modifiers changed from: private */
    public static native void nativeOnDecodedFrame(long j, VideoFrame videoFrame, Integer num, Integer num2);

    VideoDecoderWrapper() {
    }

    @CalledByNative
    static VideoDecoder.Callback createDecoderCallback(long j) {
        return new VideoDecoder.Callback(j) { // from class: org.webrtc.VideoDecoderWrapper$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.webrtc.VideoDecoder.Callback
            public final void onDecodedFrame(VideoFrame videoFrame, Integer num, Integer num2) {
                VideoDecoderWrapper.$r8$lambda$3uw3mxtBT9cCv2VFxBjPGUCviXM(this.f$0, videoFrame, num, num2);
            }
        };
    }
}
