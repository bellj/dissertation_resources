package org.webrtc;

/* loaded from: classes5.dex */
public interface VideoDecoderFactory {

    /* renamed from: org.webrtc.VideoDecoderFactory$-CC */
    /* loaded from: classes5.dex */
    public final /* synthetic */ class CC {
        @CalledByNative
        public static VideoCodecInfo[] $default$getSupportedCodecs(VideoDecoderFactory videoDecoderFactory) {
            return new VideoCodecInfo[0];
        }
    }

    @CalledByNative
    VideoDecoder createDecoder(VideoCodecInfo videoCodecInfo);

    @CalledByNative
    VideoCodecInfo[] getSupportedCodecs();
}
