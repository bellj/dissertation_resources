package org.mp4parser;

import java.util.List;

/* loaded from: classes3.dex */
public interface Container {
    List<Box> getBoxes();
}
