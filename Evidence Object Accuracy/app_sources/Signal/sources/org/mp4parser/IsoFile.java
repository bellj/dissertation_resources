package org.mp4parser;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* loaded from: classes3.dex */
public class IsoFile extends BasicContainer implements Closeable {
    private static Logger LOG = LoggerFactory.getLogger(IsoFile.class);
    private ReadableByteChannel readableByteChannel;

    public static byte[] fourCCtoBytes(String str) {
        byte[] bArr = new byte[4];
        if (str != null) {
            for (int i = 0; i < Math.min(4, str.length()); i++) {
                bArr[i] = (byte) str.charAt(i);
            }
        }
        return bArr;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.readableByteChannel.close();
    }

    @Override // org.mp4parser.BasicContainer, java.lang.Object
    public String toString() {
        return "model(" + this.readableByteChannel.toString() + ")";
    }
}
