package org.mp4parser.streaming;

import java.io.Closeable;
import org.mp4parser.boxes.iso14496.part12.SampleDescriptionBox;
import org.mp4parser.streaming.output.SampleSink;

/* loaded from: classes3.dex */
public interface StreamingTrack extends Closeable {
    void addTrackExtension(TrackExtension trackExtension);

    String getHandler();

    String getLanguage();

    SampleDescriptionBox getSampleDescriptionBox();

    long getTimescale();

    <T extends TrackExtension> T getTrackExtension(Class<T> cls);

    void setSampleSink(SampleSink sampleSink);
}
