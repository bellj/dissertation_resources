package org.mp4parser.streaming;

import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public interface StreamingSample {
    void addSampleExtension(SampleExtension sampleExtension);

    ByteBuffer getContent();

    long getDuration();

    <T extends SampleExtension> T getSampleExtension(Class<T> cls);

    <T extends SampleExtension> T removeSampleExtension(Class<T> cls);
}
