package org.mp4parser.streaming.extensions;

import j$.util.DesugarCollections;
import java.util.HashMap;
import java.util.Map;
import org.mp4parser.streaming.SampleExtension;

/* loaded from: classes3.dex */
public class CompositionTimeSampleExtension implements SampleExtension {
    public static Map<Long, CompositionTimeSampleExtension> pool = DesugarCollections.synchronizedMap(new HashMap());
    private long ctts;

    public static CompositionTimeSampleExtension create(long j) {
        CompositionTimeSampleExtension compositionTimeSampleExtension = pool.get(Long.valueOf(j));
        if (compositionTimeSampleExtension != null) {
            return compositionTimeSampleExtension;
        }
        CompositionTimeSampleExtension compositionTimeSampleExtension2 = new CompositionTimeSampleExtension();
        compositionTimeSampleExtension2.ctts = j;
        pool.put(Long.valueOf(j), compositionTimeSampleExtension2);
        return compositionTimeSampleExtension2;
    }

    public long getCompositionTimeOffset() {
        return this.ctts;
    }

    public String toString() {
        return "ctts=" + this.ctts;
    }
}
