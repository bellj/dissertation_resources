package org.mp4parser.streaming.extensions;

import j$.util.DesugarCollections;
import java.util.HashMap;
import java.util.Map;
import org.mp4parser.streaming.SampleExtension;

/* loaded from: classes3.dex */
public class SampleFlagsSampleExtension implements SampleExtension {
    public static Map<Long, SampleFlagsSampleExtension> pool = DesugarCollections.synchronizedMap(new HashMap());
    private byte isLeading;
    private int sampleDegradationPriority;
    private byte sampleDependsOn;
    private byte sampleHasRedundancy;
    private byte sampleIsDependedOn;
    private boolean sampleIsNonSyncSample;
    private byte samplePaddingValue;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("isLeading=");
        sb.append((int) this.isLeading);
        sb.append(", dependsOn=");
        sb.append((int) this.sampleDependsOn);
        sb.append(", isDependedOn=");
        sb.append((int) this.sampleIsDependedOn);
        sb.append(", hasRedundancy=");
        sb.append((int) this.sampleHasRedundancy);
        sb.append(", paddingValue=");
        sb.append((int) this.samplePaddingValue);
        sb.append(", isSyncSample=");
        sb.append(!this.sampleIsNonSyncSample);
        sb.append(", sampleDegradationPriority=");
        sb.append(this.sampleDegradationPriority);
        return sb.toString();
    }

    public void setSampleDependsOn(int i) {
        this.sampleDependsOn = (byte) i;
    }

    public void setSampleIsDependedOn(int i) {
        this.sampleIsDependedOn = (byte) i;
    }

    public void setSampleIsNonSyncSample(boolean z) {
        this.sampleIsNonSyncSample = z;
    }

    public boolean isSyncSample() {
        return !this.sampleIsNonSyncSample;
    }
}
