package org.mp4parser.streaming.extensions;

import j$.util.DesugarCollections;
import java.util.HashMap;
import java.util.Map;
import org.mp4parser.streaming.TrackExtension;

/* loaded from: classes3.dex */
public class DefaultSampleFlagsTrackExtension implements TrackExtension {
    public static Map<Long, SampleFlagsSampleExtension> pool = DesugarCollections.synchronizedMap(new HashMap());
    private byte isLeading;
    private byte sampleDependsOn;
    private byte sampleHasRedundancy;
    private byte sampleIsDependedOn;
    private boolean sampleIsNonSyncSample;

    public void setIsLeading(int i) {
        this.isLeading = (byte) i;
    }

    public void setSampleDependsOn(int i) {
        this.sampleDependsOn = (byte) i;
    }

    public void setSampleIsDependedOn(int i) {
        this.sampleIsDependedOn = (byte) i;
    }

    public void setSampleHasRedundancy(int i) {
        this.sampleHasRedundancy = (byte) i;
    }

    public void setSampleIsNonSyncSample(boolean z) {
        this.sampleIsNonSyncSample = z;
    }
}
