package org.mp4parser.streaming.extensions;

import org.mp4parser.streaming.TrackExtension;

/* loaded from: classes3.dex */
public class TrackIdTrackExtension implements TrackExtension {
    private long trackId;

    public TrackIdTrackExtension(long j) {
        this.trackId = j;
    }

    public long getTrackId() {
        return this.trackId;
    }
}
