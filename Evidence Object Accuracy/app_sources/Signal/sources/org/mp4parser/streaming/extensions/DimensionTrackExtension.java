package org.mp4parser.streaming.extensions;

import org.mp4parser.streaming.TrackExtension;

/* loaded from: classes3.dex */
public class DimensionTrackExtension implements TrackExtension {
    int height;
    int width;

    public DimensionTrackExtension(int i, int i2) {
        this.width = i;
        this.height = i2;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public String toString() {
        return "width=" + this.width + ", height=" + this.height;
    }
}
