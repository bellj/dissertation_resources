package org.mp4parser.streaming.input.h264.spspps;

import java.io.IOException;
import java.nio.ByteBuffer;
import org.mp4parser.streaming.input.h264.spspps.VUIParameters;

/* loaded from: classes3.dex */
public class SeqParameterSet {
    public int bit_depth_chroma_minus8;
    public int bit_depth_luma_minus8;
    public ChromaFormat chroma_format_idc;
    public boolean constraint_set_0_flag;
    public boolean constraint_set_1_flag;
    public boolean constraint_set_2_flag;
    public boolean constraint_set_3_flag;
    public boolean constraint_set_4_flag;
    public boolean constraint_set_5_flag;
    public boolean delta_pic_order_always_zero_flag;
    public boolean direct_8x8_inference_flag;
    public boolean entropy_coding_mode_flag;
    public boolean field_pic_flag;
    public int frame_crop_bottom_offset;
    public int frame_crop_left_offset;
    public int frame_crop_right_offset;
    public int frame_crop_top_offset;
    public boolean frame_cropping_flag;
    public boolean frame_mbs_only_flag;
    public boolean gaps_in_frame_num_value_allowed_flag;
    public int level_idc;
    public int log2_max_frame_num_minus4;
    public int log2_max_pic_order_cnt_lsb_minus4;
    public boolean mb_adaptive_frame_field_flag;
    public int num_ref_frames;
    public int num_ref_frames_in_pic_order_cnt_cycle;
    public int[] offsetForRefFrame;
    public int offset_for_non_ref_pic;
    public int offset_for_top_to_bottom_field;
    public int pic_height_in_map_units_minus1;
    public int pic_order_cnt_type;
    public int pic_width_in_mbs_minus1;
    public int profile_idc;
    public boolean qpprime_y_zero_transform_bypass_flag;
    public long reserved_zero_2bits;
    public boolean residual_color_transform_flag;
    public ScalingMatrix scalingMatrix;
    public int seq_parameter_set_id;
    public VUIParameters vuiParams;
    public int weighted_bipred_idc;
    public boolean weighted_pred_flag;

    public static SeqParameterSet read(ByteBuffer byteBuffer) throws IOException {
        ByteBufferBitreader byteBufferBitreader = new ByteBufferBitreader(byteBuffer);
        SeqParameterSet seqParameterSet = new SeqParameterSet();
        seqParameterSet.profile_idc = (int) byteBufferBitreader.readNBit(8);
        seqParameterSet.constraint_set_0_flag = byteBufferBitreader.readBool();
        seqParameterSet.constraint_set_1_flag = byteBufferBitreader.readBool();
        seqParameterSet.constraint_set_2_flag = byteBufferBitreader.readBool();
        seqParameterSet.constraint_set_3_flag = byteBufferBitreader.readBool();
        seqParameterSet.constraint_set_4_flag = byteBufferBitreader.readBool();
        seqParameterSet.constraint_set_5_flag = byteBufferBitreader.readBool();
        seqParameterSet.reserved_zero_2bits = byteBufferBitreader.readNBit(2);
        seqParameterSet.level_idc = (int) byteBufferBitreader.readNBit(8);
        seqParameterSet.seq_parameter_set_id = byteBufferBitreader.readUE();
        int i = seqParameterSet.profile_idc;
        if (i == 100 || i == 110 || i == 122 || i == 144) {
            ChromaFormat fromId = ChromaFormat.fromId(byteBufferBitreader.readUE());
            seqParameterSet.chroma_format_idc = fromId;
            if (fromId == ChromaFormat.YUV_444) {
                seqParameterSet.residual_color_transform_flag = byteBufferBitreader.readBool();
            }
            seqParameterSet.bit_depth_luma_minus8 = byteBufferBitreader.readUE();
            seqParameterSet.bit_depth_chroma_minus8 = byteBufferBitreader.readUE();
            seqParameterSet.qpprime_y_zero_transform_bypass_flag = byteBufferBitreader.readBool();
            if (byteBufferBitreader.readBool()) {
                readScalingListMatrix(byteBufferBitreader, seqParameterSet);
            }
        } else {
            seqParameterSet.chroma_format_idc = ChromaFormat.YUV_420;
        }
        seqParameterSet.log2_max_frame_num_minus4 = byteBufferBitreader.readUE();
        int readUE = byteBufferBitreader.readUE();
        seqParameterSet.pic_order_cnt_type = readUE;
        if (readUE == 0) {
            seqParameterSet.log2_max_pic_order_cnt_lsb_minus4 = byteBufferBitreader.readUE();
        } else if (readUE == 1) {
            seqParameterSet.delta_pic_order_always_zero_flag = byteBufferBitreader.readBool();
            seqParameterSet.offset_for_non_ref_pic = byteBufferBitreader.readSE();
            seqParameterSet.offset_for_top_to_bottom_field = byteBufferBitreader.readSE();
            int readUE2 = byteBufferBitreader.readUE();
            seqParameterSet.num_ref_frames_in_pic_order_cnt_cycle = readUE2;
            seqParameterSet.offsetForRefFrame = new int[readUE2];
            for (int i2 = 0; i2 < seqParameterSet.num_ref_frames_in_pic_order_cnt_cycle; i2++) {
                seqParameterSet.offsetForRefFrame[i2] = byteBufferBitreader.readSE();
            }
        }
        seqParameterSet.num_ref_frames = byteBufferBitreader.readUE();
        seqParameterSet.gaps_in_frame_num_value_allowed_flag = byteBufferBitreader.readBool();
        seqParameterSet.pic_width_in_mbs_minus1 = byteBufferBitreader.readUE();
        seqParameterSet.pic_height_in_map_units_minus1 = byteBufferBitreader.readUE();
        boolean readBool = byteBufferBitreader.readBool();
        seqParameterSet.frame_mbs_only_flag = readBool;
        if (!readBool) {
            seqParameterSet.mb_adaptive_frame_field_flag = byteBufferBitreader.readBool();
        }
        seqParameterSet.direct_8x8_inference_flag = byteBufferBitreader.readBool();
        boolean readBool2 = byteBufferBitreader.readBool();
        seqParameterSet.frame_cropping_flag = readBool2;
        if (readBool2) {
            seqParameterSet.frame_crop_left_offset = byteBufferBitreader.readUE();
            seqParameterSet.frame_crop_right_offset = byteBufferBitreader.readUE();
            seqParameterSet.frame_crop_top_offset = byteBufferBitreader.readUE();
            seqParameterSet.frame_crop_bottom_offset = byteBufferBitreader.readUE();
        }
        if (byteBufferBitreader.readBool()) {
            seqParameterSet.vuiParams = ReadVUIParameters(byteBufferBitreader);
        }
        return seqParameterSet;
    }

    private static void readScalingListMatrix(ByteBufferBitreader byteBufferBitreader, SeqParameterSet seqParameterSet) throws IOException {
        seqParameterSet.scalingMatrix = new ScalingMatrix();
        for (int i = 0; i < 8; i++) {
            if (byteBufferBitreader.readBool()) {
                ScalingMatrix scalingMatrix = seqParameterSet.scalingMatrix;
                ScalingList[] scalingListArr = new ScalingList[8];
                scalingMatrix.ScalingList4x4 = scalingListArr;
                ScalingList[] scalingListArr2 = new ScalingList[8];
                scalingMatrix.ScalingList8x8 = scalingListArr2;
                if (i < 6) {
                    scalingListArr[i] = ScalingList.read(byteBufferBitreader, 16);
                } else {
                    scalingListArr2[i - 6] = ScalingList.read(byteBufferBitreader, 64);
                }
            }
        }
    }

    private static VUIParameters ReadVUIParameters(ByteBufferBitreader byteBufferBitreader) throws IOException {
        VUIParameters vUIParameters = new VUIParameters();
        boolean readBool = byteBufferBitreader.readBool();
        vUIParameters.aspect_ratio_info_present_flag = readBool;
        if (readBool) {
            AspectRatio fromValue = AspectRatio.fromValue((int) byteBufferBitreader.readNBit(8));
            vUIParameters.aspect_ratio = fromValue;
            if (fromValue == AspectRatio.Extended_SAR) {
                vUIParameters.sar_width = (int) byteBufferBitreader.readNBit(16);
                vUIParameters.sar_height = (int) byteBufferBitreader.readNBit(16);
            }
        }
        boolean readBool2 = byteBufferBitreader.readBool();
        vUIParameters.overscan_info_present_flag = readBool2;
        if (readBool2) {
            vUIParameters.overscan_appropriate_flag = byteBufferBitreader.readBool();
        }
        boolean readBool3 = byteBufferBitreader.readBool();
        vUIParameters.video_signal_type_present_flag = readBool3;
        if (readBool3) {
            vUIParameters.video_format = (int) byteBufferBitreader.readNBit(3);
            vUIParameters.video_full_range_flag = byteBufferBitreader.readBool();
            boolean readBool4 = byteBufferBitreader.readBool();
            vUIParameters.colour_description_present_flag = readBool4;
            if (readBool4) {
                vUIParameters.colour_primaries = (int) byteBufferBitreader.readNBit(8);
                vUIParameters.transfer_characteristics = (int) byteBufferBitreader.readNBit(8);
                vUIParameters.matrix_coefficients = (int) byteBufferBitreader.readNBit(8);
            }
        }
        boolean readBool5 = byteBufferBitreader.readBool();
        vUIParameters.chroma_loc_info_present_flag = readBool5;
        if (readBool5) {
            vUIParameters.chroma_sample_loc_type_top_field = byteBufferBitreader.readUE();
            vUIParameters.chroma_sample_loc_type_bottom_field = byteBufferBitreader.readUE();
        }
        boolean readBool6 = byteBufferBitreader.readBool();
        vUIParameters.timing_info_present_flag = readBool6;
        if (readBool6) {
            vUIParameters.num_units_in_tick = (int) byteBufferBitreader.readNBit(32);
            vUIParameters.time_scale = (int) byteBufferBitreader.readNBit(32);
            vUIParameters.fixed_frame_rate_flag = byteBufferBitreader.readBool();
        }
        boolean readBool7 = byteBufferBitreader.readBool();
        if (readBool7) {
            vUIParameters.nalHRDParams = readHRDParameters(byteBufferBitreader);
        }
        boolean readBool8 = byteBufferBitreader.readBool();
        if (readBool8) {
            vUIParameters.vclHRDParams = readHRDParameters(byteBufferBitreader);
        }
        if (readBool7 || readBool8) {
            vUIParameters.low_delay_hrd_flag = byteBufferBitreader.readBool();
        }
        vUIParameters.pic_struct_present_flag = byteBufferBitreader.readBool();
        if (byteBufferBitreader.readBool()) {
            VUIParameters.BitstreamRestriction bitstreamRestriction = new VUIParameters.BitstreamRestriction();
            vUIParameters.bitstreamRestriction = bitstreamRestriction;
            bitstreamRestriction.motion_vectors_over_pic_boundaries_flag = byteBufferBitreader.readBool();
            vUIParameters.bitstreamRestriction.max_bytes_per_pic_denom = byteBufferBitreader.readUE();
            vUIParameters.bitstreamRestriction.max_bits_per_mb_denom = byteBufferBitreader.readUE();
            vUIParameters.bitstreamRestriction.log2_max_mv_length_horizontal = byteBufferBitreader.readUE();
            vUIParameters.bitstreamRestriction.log2_max_mv_length_vertical = byteBufferBitreader.readUE();
            vUIParameters.bitstreamRestriction.num_reorder_frames = byteBufferBitreader.readUE();
            vUIParameters.bitstreamRestriction.max_dec_frame_buffering = byteBufferBitreader.readUE();
        }
        return vUIParameters;
    }

    private static HRDParameters readHRDParameters(ByteBufferBitreader byteBufferBitreader) throws IOException {
        HRDParameters hRDParameters = new HRDParameters();
        hRDParameters.cpb_cnt_minus1 = byteBufferBitreader.readUE();
        hRDParameters.bit_rate_scale = (int) byteBufferBitreader.readNBit(4);
        hRDParameters.cpb_size_scale = (int) byteBufferBitreader.readNBit(4);
        int i = hRDParameters.cpb_cnt_minus1;
        hRDParameters.bit_rate_value_minus1 = new int[i + 1];
        hRDParameters.cpb_size_value_minus1 = new int[i + 1];
        hRDParameters.cbr_flag = new boolean[i + 1];
        for (int i2 = 0; i2 <= hRDParameters.cpb_cnt_minus1; i2++) {
            hRDParameters.bit_rate_value_minus1[i2] = byteBufferBitreader.readUE();
            hRDParameters.cpb_size_value_minus1[i2] = byteBufferBitreader.readUE();
            hRDParameters.cbr_flag[i2] = byteBufferBitreader.readBool();
        }
        hRDParameters.initial_cpb_removal_delay_length_minus1 = (int) byteBufferBitreader.readNBit(5);
        hRDParameters.cpb_removal_delay_length_minus1 = (int) byteBufferBitreader.readNBit(5);
        hRDParameters.dpb_output_delay_length_minus1 = (int) byteBufferBitreader.readNBit(5);
        hRDParameters.time_offset_length = (int) byteBufferBitreader.readNBit(5);
        return hRDParameters;
    }

    public String toString() {
        return "SeqParameterSet{ \n        pic_order_cnt_type=" + this.pic_order_cnt_type + ", \n        field_pic_flag=" + this.field_pic_flag + ", \n        delta_pic_order_always_zero_flag=" + this.delta_pic_order_always_zero_flag + ", \n        weighted_pred_flag=" + this.weighted_pred_flag + ", \n        weighted_bipred_idc=" + this.weighted_bipred_idc + ", \n        entropy_coding_mode_flag=" + this.entropy_coding_mode_flag + ", \n        mb_adaptive_frame_field_flag=" + this.mb_adaptive_frame_field_flag + ", \n        direct_8x8_inference_flag=" + this.direct_8x8_inference_flag + ", \n        chroma_format_idc=" + this.chroma_format_idc + ", \n        log2_max_frame_num_minus4=" + this.log2_max_frame_num_minus4 + ", \n        log2_max_pic_order_cnt_lsb_minus4=" + this.log2_max_pic_order_cnt_lsb_minus4 + ", \n        pic_height_in_map_units_minus1=" + this.pic_height_in_map_units_minus1 + ", \n        pic_width_in_mbs_minus1=" + this.pic_width_in_mbs_minus1 + ", \n        bit_depth_luma_minus8=" + this.bit_depth_luma_minus8 + ", \n        bit_depth_chroma_minus8=" + this.bit_depth_chroma_minus8 + ", \n        qpprime_y_zero_transform_bypass_flag=" + this.qpprime_y_zero_transform_bypass_flag + ", \n        profile_idc=" + this.profile_idc + ", \n        constraint_set_0_flag=" + this.constraint_set_0_flag + ", \n        constraint_set_1_flag=" + this.constraint_set_1_flag + ", \n        constraint_set_2_flag=" + this.constraint_set_2_flag + ", \n        constraint_set_3_flag=" + this.constraint_set_3_flag + ", \n        constraint_set_4_flag=" + this.constraint_set_4_flag + ", \n        constraint_set_5_flag=" + this.constraint_set_5_flag + ", \n        level_idc=" + this.level_idc + ", \n        seq_parameter_set_id=" + this.seq_parameter_set_id + ", \n        residual_color_transform_flag=" + this.residual_color_transform_flag + ", \n        offset_for_non_ref_pic=" + this.offset_for_non_ref_pic + ", \n        offset_for_top_to_bottom_field=" + this.offset_for_top_to_bottom_field + ", \n        num_ref_frames=" + this.num_ref_frames + ", \n        gaps_in_frame_num_value_allowed_flag=" + this.gaps_in_frame_num_value_allowed_flag + ", \n        frame_mbs_only_flag=" + this.frame_mbs_only_flag + ", \n        frame_cropping_flag=" + this.frame_cropping_flag + ", \n        frame_crop_left_offset=" + this.frame_crop_left_offset + ", \n        frame_crop_right_offset=" + this.frame_crop_right_offset + ", \n        frame_crop_top_offset=" + this.frame_crop_top_offset + ", \n        frame_crop_bottom_offset=" + this.frame_crop_bottom_offset + ", \n        offsetForRefFrame=" + this.offsetForRefFrame + ", \n        vuiParams=" + this.vuiParams + ", \n        scalingMatrix=" + this.scalingMatrix + ", \n        num_ref_frames_in_pic_order_cnt_cycle=" + this.num_ref_frames_in_pic_order_cnt_cycle + '}';
    }
}
