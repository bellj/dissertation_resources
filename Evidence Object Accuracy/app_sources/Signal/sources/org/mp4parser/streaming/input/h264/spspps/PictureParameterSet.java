package org.mp4parser.streaming.input.h264.spspps;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/* loaded from: classes3.dex */
public class PictureParameterSet {
    public boolean bottom_field_pic_order_in_frame_present_flag;
    public int[] bottom_right;
    public int chroma_qp_index_offset;
    public boolean constrained_intra_pred_flag;
    public boolean deblocking_filter_control_present_flag;
    public boolean entropy_coding_mode_flag;
    public PPSExt extended;
    public int num_ref_idx_l0_active_minus1;
    public int num_ref_idx_l1_active_minus1;
    public int num_slice_groups_minus1;
    public int pic_init_qp_minus26;
    public int pic_init_qs_minus26;
    public int pic_parameter_set_id;
    public boolean redundant_pic_cnt_present_flag;
    public int[] run_length_minus1;
    public int seq_parameter_set_id;
    public boolean slice_group_change_direction_flag;
    public int slice_group_change_rate_minus1;
    public int[] slice_group_id;
    public int slice_group_map_type;
    public int[] top_left;
    public int weighted_bipred_idc;
    public boolean weighted_pred_flag;

    public static PictureParameterSet read(ByteBuffer byteBuffer) throws IOException {
        ByteBufferBitreader byteBufferBitreader = new ByteBufferBitreader(byteBuffer);
        PictureParameterSet pictureParameterSet = new PictureParameterSet();
        pictureParameterSet.pic_parameter_set_id = byteBufferBitreader.readUE();
        pictureParameterSet.seq_parameter_set_id = byteBufferBitreader.readUE();
        pictureParameterSet.entropy_coding_mode_flag = byteBufferBitreader.readBool();
        pictureParameterSet.bottom_field_pic_order_in_frame_present_flag = byteBufferBitreader.readBool();
        int readUE = byteBufferBitreader.readUE();
        pictureParameterSet.num_slice_groups_minus1 = readUE;
        if (readUE > 0) {
            int readUE2 = byteBufferBitreader.readUE();
            pictureParameterSet.slice_group_map_type = readUE2;
            int i = pictureParameterSet.num_slice_groups_minus1;
            pictureParameterSet.top_left = new int[i + 1];
            pictureParameterSet.bottom_right = new int[i + 1];
            pictureParameterSet.run_length_minus1 = new int[i + 1];
            if (readUE2 == 0) {
                for (int i2 = 0; i2 <= pictureParameterSet.num_slice_groups_minus1; i2++) {
                    pictureParameterSet.run_length_minus1[i2] = byteBufferBitreader.readUE();
                }
            } else if (readUE2 == 2) {
                for (int i3 = 0; i3 < pictureParameterSet.num_slice_groups_minus1; i3++) {
                    pictureParameterSet.top_left[i3] = byteBufferBitreader.readUE();
                    pictureParameterSet.bottom_right[i3] = byteBufferBitreader.readUE();
                }
            } else {
                int i4 = 3;
                if (readUE2 == 3 || readUE2 == 4 || readUE2 == 5) {
                    pictureParameterSet.slice_group_change_direction_flag = byteBufferBitreader.readBool();
                    pictureParameterSet.slice_group_change_rate_minus1 = byteBufferBitreader.readUE();
                } else if (readUE2 == 6) {
                    if (i + 1 <= 4) {
                        i4 = i + 1 > 2 ? 2 : 1;
                    }
                    int readUE3 = byteBufferBitreader.readUE();
                    pictureParameterSet.slice_group_id = new int[readUE3 + 1];
                    for (int i5 = 0; i5 <= readUE3; i5++) {
                        pictureParameterSet.slice_group_id[i5] = (int) byteBufferBitreader.readNBit(i4);
                    }
                }
            }
        }
        pictureParameterSet.num_ref_idx_l0_active_minus1 = byteBufferBitreader.readUE();
        pictureParameterSet.num_ref_idx_l1_active_minus1 = byteBufferBitreader.readUE();
        pictureParameterSet.weighted_pred_flag = byteBufferBitreader.readBool();
        pictureParameterSet.weighted_bipred_idc = (int) byteBufferBitreader.readNBit(2);
        pictureParameterSet.pic_init_qp_minus26 = byteBufferBitreader.readSE();
        pictureParameterSet.pic_init_qs_minus26 = byteBufferBitreader.readSE();
        pictureParameterSet.chroma_qp_index_offset = byteBufferBitreader.readSE();
        pictureParameterSet.deblocking_filter_control_present_flag = byteBufferBitreader.readBool();
        pictureParameterSet.constrained_intra_pred_flag = byteBufferBitreader.readBool();
        pictureParameterSet.redundant_pic_cnt_present_flag = byteBufferBitreader.readBool();
        if (byteBufferBitreader.moreRBSPData()) {
            PPSExt pPSExt = new PPSExt();
            pictureParameterSet.extended = pPSExt;
            pPSExt.transform_8x8_mode_flag = byteBufferBitreader.readBool();
            if (byteBufferBitreader.readBool()) {
                for (int i6 = 0; i6 < ((pictureParameterSet.extended.transform_8x8_mode_flag ? 1 : 0) * 2) + 6; i6++) {
                    if (byteBufferBitreader.readBool()) {
                        ScalingMatrix scalingMatrix = pictureParameterSet.extended.scalindMatrix;
                        ScalingList[] scalingListArr = new ScalingList[8];
                        scalingMatrix.ScalingList4x4 = scalingListArr;
                        ScalingList[] scalingListArr2 = new ScalingList[8];
                        scalingMatrix.ScalingList8x8 = scalingListArr2;
                        if (i6 < 6) {
                            scalingListArr[i6] = ScalingList.read(byteBufferBitreader, 16);
                        } else {
                            scalingListArr2[i6 - 6] = ScalingList.read(byteBufferBitreader, 64);
                        }
                    }
                }
            }
            pictureParameterSet.extended.second_chroma_qp_index_offset = byteBufferBitreader.readSE();
        }
        return pictureParameterSet;
    }

    public int hashCode() {
        int i;
        int i2 = 1231;
        int hashCode = (((((((((Arrays.hashCode(this.bottom_right) + 31) * 31) + this.chroma_qp_index_offset) * 31) + (this.constrained_intra_pred_flag ? 1231 : 1237)) * 31) + (this.deblocking_filter_control_present_flag ? 1231 : 1237)) * 31) + (this.entropy_coding_mode_flag ? 1231 : 1237)) * 31;
        PPSExt pPSExt = this.extended;
        if (pPSExt == null) {
            i = 0;
        } else {
            i = pPSExt.hashCode();
        }
        int hashCode2 = (((((((((((((((((((((((((((((((((hashCode + i) * 31) + this.num_ref_idx_l0_active_minus1) * 31) + this.num_ref_idx_l1_active_minus1) * 31) + this.num_slice_groups_minus1) * 31) + this.pic_init_qp_minus26) * 31) + this.pic_init_qs_minus26) * 31) + (this.bottom_field_pic_order_in_frame_present_flag ? 1231 : 1237)) * 31) + this.pic_parameter_set_id) * 31) + (this.redundant_pic_cnt_present_flag ? 1231 : 1237)) * 31) + Arrays.hashCode(this.run_length_minus1)) * 31) + this.seq_parameter_set_id) * 31) + (this.slice_group_change_direction_flag ? 1231 : 1237)) * 31) + this.slice_group_change_rate_minus1) * 31) + Arrays.hashCode(this.slice_group_id)) * 31) + this.slice_group_map_type) * 31) + Arrays.hashCode(this.top_left)) * 31) + this.weighted_bipred_idc) * 31;
        if (!this.weighted_pred_flag) {
            i2 = 1237;
        }
        return hashCode2 + i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PictureParameterSet pictureParameterSet = (PictureParameterSet) obj;
        if (!Arrays.equals(this.bottom_right, pictureParameterSet.bottom_right) || this.chroma_qp_index_offset != pictureParameterSet.chroma_qp_index_offset || this.constrained_intra_pred_flag != pictureParameterSet.constrained_intra_pred_flag || this.deblocking_filter_control_present_flag != pictureParameterSet.deblocking_filter_control_present_flag || this.entropy_coding_mode_flag != pictureParameterSet.entropy_coding_mode_flag) {
            return false;
        }
        PPSExt pPSExt = this.extended;
        if (pPSExt == null) {
            if (pictureParameterSet.extended != null) {
                return false;
            }
        } else if (!pPSExt.equals(pictureParameterSet.extended)) {
            return false;
        }
        return this.num_ref_idx_l0_active_minus1 == pictureParameterSet.num_ref_idx_l0_active_minus1 && this.num_ref_idx_l1_active_minus1 == pictureParameterSet.num_ref_idx_l1_active_minus1 && this.num_slice_groups_minus1 == pictureParameterSet.num_slice_groups_minus1 && this.pic_init_qp_minus26 == pictureParameterSet.pic_init_qp_minus26 && this.pic_init_qs_minus26 == pictureParameterSet.pic_init_qs_minus26 && this.bottom_field_pic_order_in_frame_present_flag == pictureParameterSet.bottom_field_pic_order_in_frame_present_flag && this.pic_parameter_set_id == pictureParameterSet.pic_parameter_set_id && this.redundant_pic_cnt_present_flag == pictureParameterSet.redundant_pic_cnt_present_flag && Arrays.equals(this.run_length_minus1, pictureParameterSet.run_length_minus1) && this.seq_parameter_set_id == pictureParameterSet.seq_parameter_set_id && this.slice_group_change_direction_flag == pictureParameterSet.slice_group_change_direction_flag && this.slice_group_change_rate_minus1 == pictureParameterSet.slice_group_change_rate_minus1 && Arrays.equals(this.slice_group_id, pictureParameterSet.slice_group_id) && this.slice_group_map_type == pictureParameterSet.slice_group_map_type && Arrays.equals(this.top_left, pictureParameterSet.top_left) && this.weighted_bipred_idc == pictureParameterSet.weighted_bipred_idc && this.weighted_pred_flag == pictureParameterSet.weighted_pred_flag;
    }

    public String toString() {
        return "PictureParameterSet{\n       entropy_coding_mode_flag=" + this.entropy_coding_mode_flag + ",\n       num_ref_idx_l0_active_minus1=" + this.num_ref_idx_l0_active_minus1 + ",\n       num_ref_idx_l1_active_minus1=" + this.num_ref_idx_l1_active_minus1 + ",\n       slice_group_change_rate_minus1=" + this.slice_group_change_rate_minus1 + ",\n       pic_parameter_set_id=" + this.pic_parameter_set_id + ",\n       seq_parameter_set_id=" + this.seq_parameter_set_id + ",\n       pic_order_present_flag=" + this.bottom_field_pic_order_in_frame_present_flag + ",\n       num_slice_groups_minus1=" + this.num_slice_groups_minus1 + ",\n       slice_group_map_type=" + this.slice_group_map_type + ",\n       weighted_pred_flag=" + this.weighted_pred_flag + ",\n       weighted_bipred_idc=" + this.weighted_bipred_idc + ",\n       pic_init_qp_minus26=" + this.pic_init_qp_minus26 + ",\n       pic_init_qs_minus26=" + this.pic_init_qs_minus26 + ",\n       chroma_qp_index_offset=" + this.chroma_qp_index_offset + ",\n       deblocking_filter_control_present_flag=" + this.deblocking_filter_control_present_flag + ",\n       constrained_intra_pred_flag=" + this.constrained_intra_pred_flag + ",\n       redundant_pic_cnt_present_flag=" + this.redundant_pic_cnt_present_flag + ",\n       top_left=" + this.top_left + ",\n       bottom_right=" + this.bottom_right + ",\n       run_length_minus1=" + this.run_length_minus1 + ",\n       slice_group_change_direction_flag=" + this.slice_group_change_direction_flag + ",\n       slice_group_id=" + this.slice_group_id + ",\n       extended=" + this.extended + '}';
    }

    /* loaded from: classes3.dex */
    public static class PPSExt {
        public boolean[] pic_scaling_list_present_flag;
        public ScalingMatrix scalindMatrix = new ScalingMatrix();
        public int second_chroma_qp_index_offset;
        public boolean transform_8x8_mode_flag;

        public String toString() {
            return "PPSExt{transform_8x8_mode_flag=" + this.transform_8x8_mode_flag + ", scalindMatrix=" + this.scalindMatrix + ", second_chroma_qp_index_offset=" + this.second_chroma_qp_index_offset + ", pic_scaling_list_present_flag=" + this.pic_scaling_list_present_flag + '}';
        }
    }
}
