package org.mp4parser.streaming.input.h264.spspps;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public class ByteBufferBitreader {
    ByteBuffer buffer;
    private int currentByte = get();
    int nBit;
    private int nextByte = get();

    public ByteBufferBitreader(ByteBuffer byteBuffer) {
        this.buffer = byteBuffer;
    }

    public int get() {
        try {
            byte b = this.buffer.get();
            return b < 0 ? b + 256 : b;
        } catch (BufferUnderflowException unused) {
            return -1;
        }
    }

    public int read1Bit() throws IOException {
        if (this.nBit == 8) {
            advance();
            if (this.currentByte == -1) {
                return -1;
            }
        }
        int i = this.currentByte;
        int i2 = this.nBit;
        int i3 = (i >> (7 - i2)) & 1;
        this.nBit = i2 + 1;
        return i3;
    }

    private void advance() throws IOException {
        this.currentByte = this.nextByte;
        this.nextByte = get();
        this.nBit = 0;
    }

    public int readUE() throws IOException {
        int i = 0;
        while (read1Bit() == 0) {
            i++;
        }
        if (i > 0) {
            return (int) (((long) ((1 << i) - 1)) + readNBit(i));
        }
        return 0;
    }

    public long readNBit(int i) throws IOException {
        if (i <= 64) {
            long j = 0;
            for (int i2 = 0; i2 < i; i2++) {
                j = (j << 1) | ((long) read1Bit());
            }
            return j;
        }
        throw new IllegalArgumentException("Can not readByte more then 64 bit");
    }

    public boolean readBool() throws IOException {
        return read1Bit() != 0;
    }

    public int readSE() throws IOException {
        int readUE = readUE();
        int i = readUE & 1;
        return ((readUE >> 1) + i) * ((i << 1) - 1);
    }

    public boolean moreRBSPData() throws IOException {
        if (this.nBit == 8) {
            advance();
        }
        int i = 1 << ((8 - this.nBit) - 1);
        int i2 = this.currentByte;
        boolean z = (((i << 1) - 1) & i2) == i;
        if (i2 == -1 || (this.nextByte == -1 && z)) {
            return false;
        }
        return true;
    }
}
