package org.mp4parser.streaming.input.h264.spspps;

/* loaded from: classes3.dex */
public class AspectRatio {
    public static final AspectRatio Extended_SAR = new AspectRatio(255);
    private int value;

    private AspectRatio(int i) {
        this.value = i;
    }

    public static AspectRatio fromValue(int i) {
        AspectRatio aspectRatio = Extended_SAR;
        if (i == aspectRatio.value) {
            return aspectRatio;
        }
        return new AspectRatio(i);
    }

    public String toString() {
        return "AspectRatio{value=" + this.value + '}';
    }
}
