package org.mp4parser.streaming.input;

import java.util.HashMap;
import org.mp4parser.boxes.iso14496.part12.TrackHeaderBox;
import org.mp4parser.streaming.StreamingTrack;
import org.mp4parser.streaming.TrackExtension;
import org.mp4parser.streaming.output.SampleSink;

/* loaded from: classes3.dex */
public abstract class AbstractStreamingTrack implements StreamingTrack {
    protected SampleSink sampleSink;
    protected TrackHeaderBox tkhd;
    protected HashMap<Class<? extends TrackExtension>, TrackExtension> trackExtensions = new HashMap<>();

    public AbstractStreamingTrack() {
        TrackHeaderBox trackHeaderBox = new TrackHeaderBox();
        this.tkhd = trackHeaderBox;
        trackHeaderBox.setTrackId(1);
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public void setSampleSink(SampleSink sampleSink) {
        this.sampleSink = sampleSink;
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public <T extends TrackExtension> T getTrackExtension(Class<T> cls) {
        return (T) this.trackExtensions.get(cls);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.util.HashMap<java.lang.Class<? extends org.mp4parser.streaming.TrackExtension>, org.mp4parser.streaming.TrackExtension> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.mp4parser.streaming.StreamingTrack
    public void addTrackExtension(TrackExtension trackExtension) {
        this.trackExtensions.put(trackExtension.getClass(), trackExtension);
    }

    public void removeTrackExtension(Class<? extends TrackExtension> cls) {
        this.trackExtensions.remove(cls);
    }
}
