package org.mp4parser.streaming.input.h264.spspps;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes3.dex */
public class SliceHeader {
    public boolean bottom_field_flag = false;
    public int colour_plane_id;
    public int delta_pic_order_cnt_0;
    public int delta_pic_order_cnt_1;
    public int delta_pic_order_cnt_bottom;
    public boolean field_pic_flag = false;
    public int first_mb_in_slice;
    public int frame_num;
    public int idr_pic_id = -1;
    public int pic_order_cnt_lsb;
    public int pic_parameter_set_id;
    public PictureParameterSet pps;
    public SliceType slice_type;
    public SeqParameterSet sps;

    /* loaded from: classes3.dex */
    public enum SliceType {
        P,
        B,
        I,
        SP,
        SI
    }

    public SliceHeader(ByteBuffer byteBuffer, Map<Integer, SeqParameterSet> map, Map<Integer, PictureParameterSet> map2, boolean z) {
        try {
            byteBuffer.position(1);
            ByteBufferBitreader byteBufferBitreader = new ByteBufferBitreader(byteBuffer);
            this.first_mb_in_slice = byteBufferBitreader.readUE();
            switch (byteBufferBitreader.readUE()) {
                case 0:
                case 5:
                    this.slice_type = SliceType.P;
                    break;
                case 1:
                case 6:
                    this.slice_type = SliceType.B;
                    break;
                case 2:
                case 7:
                    this.slice_type = SliceType.I;
                    break;
                case 3:
                case 8:
                    this.slice_type = SliceType.SP;
                    break;
                case 4:
                case 9:
                    this.slice_type = SliceType.SI;
                    break;
            }
            int readUE = byteBufferBitreader.readUE();
            this.pic_parameter_set_id = readUE;
            PictureParameterSet pictureParameterSet = map2.get(Integer.valueOf(readUE));
            this.pps = pictureParameterSet;
            String str = "";
            if (pictureParameterSet == null) {
                Iterator<Integer> it = map2.keySet().iterator();
                while (it.hasNext()) {
                    str = str + it.next() + ", ";
                }
                throw new RuntimeException("PPS with ids " + str + " available but not " + this.pic_parameter_set_id);
            }
            SeqParameterSet seqParameterSet = map.get(Integer.valueOf(pictureParameterSet.seq_parameter_set_id));
            this.sps = seqParameterSet;
            if (seqParameterSet == null) {
                Iterator<Integer> it2 = map.keySet().iterator();
                while (it2.hasNext()) {
                    str = str + it2.next() + ", ";
                }
                throw new RuntimeException("SPS with ids " + str + " available but not " + this.pps.seq_parameter_set_id);
            }
            if (seqParameterSet.residual_color_transform_flag) {
                this.colour_plane_id = (int) byteBufferBitreader.readNBit(2);
            }
            this.frame_num = (int) byteBufferBitreader.readNBit(this.sps.log2_max_frame_num_minus4 + 4);
            if (!this.sps.frame_mbs_only_flag) {
                boolean readBool = byteBufferBitreader.readBool();
                this.field_pic_flag = readBool;
                if (readBool) {
                    this.bottom_field_flag = byteBufferBitreader.readBool();
                }
            }
            if (z) {
                this.idr_pic_id = byteBufferBitreader.readUE();
            }
            SeqParameterSet seqParameterSet2 = this.sps;
            if (seqParameterSet2.pic_order_cnt_type == 0) {
                this.pic_order_cnt_lsb = (int) byteBufferBitreader.readNBit(seqParameterSet2.log2_max_pic_order_cnt_lsb_minus4 + 4);
                if (this.pps.bottom_field_pic_order_in_frame_present_flag && !this.field_pic_flag) {
                    this.delta_pic_order_cnt_bottom = byteBufferBitreader.readSE();
                }
            }
            SeqParameterSet seqParameterSet3 = this.sps;
            if (seqParameterSet3.pic_order_cnt_type == 1 && !seqParameterSet3.delta_pic_order_always_zero_flag) {
                this.delta_pic_order_cnt_0 = byteBufferBitreader.readSE();
                if (this.pps.bottom_field_pic_order_in_frame_present_flag && !this.field_pic_flag) {
                    this.delta_pic_order_cnt_1 = byteBufferBitreader.readSE();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String toString() {
        return "SliceHeader{first_mb_in_slice=" + this.first_mb_in_slice + ", slice_type=" + this.slice_type + ", pic_parameter_set_id=" + this.pic_parameter_set_id + ", colour_plane_id=" + this.colour_plane_id + ", frame_num=" + this.frame_num + ", field_pic_flag=" + this.field_pic_flag + ", bottom_field_flag=" + this.bottom_field_flag + ", idr_pic_id=" + this.idr_pic_id + ", pic_order_cnt_lsb=" + this.pic_order_cnt_lsb + ", delta_pic_order_cnt_bottom=" + this.delta_pic_order_cnt_bottom + '}';
    }
}
