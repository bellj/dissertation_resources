package org.mp4parser.streaming.input.h264.spspps;

import java.util.Arrays;
import java.util.List;

/* loaded from: classes3.dex */
public class ScalingMatrix {
    public ScalingList[] ScalingList4x4;
    public ScalingList[] ScalingList8x8;

    public String toString() {
        List list;
        StringBuilder sb = new StringBuilder();
        sb.append("ScalingMatrix{ScalingList4x4=");
        ScalingList[] scalingListArr = this.ScalingList4x4;
        List list2 = null;
        if (scalingListArr == null) {
            list = null;
        } else {
            list = Arrays.asList(scalingListArr);
        }
        sb.append(list);
        sb.append("\n, ScalingList8x8=");
        ScalingList[] scalingListArr2 = this.ScalingList8x8;
        if (scalingListArr2 != null) {
            list2 = Arrays.asList(scalingListArr2);
        }
        sb.append(list2);
        sb.append("\n");
        sb.append('}');
        return sb.toString();
    }
}
