package org.mp4parser.streaming.input;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import org.mp4parser.streaming.SampleExtension;
import org.mp4parser.streaming.StreamingSample;

/* loaded from: classes3.dex */
public class StreamingSampleImpl implements StreamingSample {
    private long duration;
    private ByteBuffer s;
    private HashMap<Class<? extends SampleExtension>, SampleExtension> sampleExtensions = new HashMap<>();

    public StreamingSampleImpl(ByteBuffer byteBuffer, long j) {
        this.s = byteBuffer.duplicate();
        this.duration = j;
    }

    public StreamingSampleImpl(List<ByteBuffer> list, long j) {
        this.duration = j;
        int i = 0;
        for (ByteBuffer byteBuffer : list) {
            i = i + 4 + byteBuffer.limit();
        }
        this.s = ByteBuffer.allocate(i);
        for (ByteBuffer byteBuffer2 : list) {
            this.s.put((byte) ((byteBuffer2.limit() & -16777216) >> 24));
            this.s.put((byte) ((byteBuffer2.limit() & 16711680) >> 16));
            this.s.put((byte) ((byteBuffer2.limit() & 65280) >> 8));
            this.s.put((byte) (byteBuffer2.limit() & 255));
            this.s.put((ByteBuffer) byteBuffer2.rewind());
        }
    }

    @Override // org.mp4parser.streaming.StreamingSample
    public ByteBuffer getContent() {
        return this.s;
    }

    @Override // org.mp4parser.streaming.StreamingSample
    public long getDuration() {
        return this.duration;
    }

    @Override // org.mp4parser.streaming.StreamingSample
    public <T extends SampleExtension> T getSampleExtension(Class<T> cls) {
        return (T) this.sampleExtensions.get(cls);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.util.HashMap<java.lang.Class<? extends org.mp4parser.streaming.SampleExtension>, org.mp4parser.streaming.SampleExtension> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.mp4parser.streaming.StreamingSample
    public void addSampleExtension(SampleExtension sampleExtension) {
        this.sampleExtensions.put(sampleExtension.getClass(), sampleExtension);
    }

    @Override // org.mp4parser.streaming.StreamingSample
    public <T extends SampleExtension> T removeSampleExtension(Class<T> cls) {
        return (T) this.sampleExtensions.remove(cls);
    }
}
