package org.mp4parser.streaming.output.mp4;

import java.util.LinkedList;
import org.mp4parser.Box;
import org.mp4parser.boxes.iso14496.part12.DataEntryUrlBox;
import org.mp4parser.boxes.iso14496.part12.DataInformationBox;
import org.mp4parser.boxes.iso14496.part12.DataReferenceBox;
import org.mp4parser.boxes.iso14496.part12.FileTypeBox;
import org.mp4parser.boxes.iso14496.part12.HandlerBox;
import org.mp4parser.boxes.iso14496.part12.HintMediaHeaderBox;
import org.mp4parser.boxes.iso14496.part12.MediaBox;
import org.mp4parser.boxes.iso14496.part12.MediaInformationBox;
import org.mp4parser.boxes.iso14496.part12.NullMediaHeaderBox;
import org.mp4parser.boxes.iso14496.part12.SampleSizeBox;
import org.mp4parser.boxes.iso14496.part12.SampleTableBox;
import org.mp4parser.boxes.iso14496.part12.SampleToChunkBox;
import org.mp4parser.boxes.iso14496.part12.SoundMediaHeaderBox;
import org.mp4parser.boxes.iso14496.part12.StaticChunkOffsetBox;
import org.mp4parser.boxes.iso14496.part12.SubtitleMediaHeaderBox;
import org.mp4parser.boxes.iso14496.part12.TimeToSampleBox;
import org.mp4parser.boxes.iso14496.part12.TrackBox;
import org.mp4parser.boxes.iso14496.part12.TrackHeaderBox;
import org.mp4parser.boxes.iso14496.part12.VideoMediaHeaderBox;
import org.mp4parser.streaming.StreamingTrack;
import org.mp4parser.streaming.extensions.DimensionTrackExtension;
import org.mp4parser.streaming.extensions.TrackIdTrackExtension;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public abstract class DefaultBoxes {
    protected abstract Box createMdhd(StreamingTrack streamingTrack);

    protected abstract Box createMvhd();

    public Box createFtyp() {
        LinkedList linkedList = new LinkedList();
        linkedList.add("isom");
        linkedList.add("iso2");
        linkedList.add("avc1");
        linkedList.add("iso6");
        linkedList.add("mp41");
        return new FileTypeBox("isom", 512, linkedList);
    }

    protected Box createMdiaHdlr(StreamingTrack streamingTrack) {
        HandlerBox handlerBox = new HandlerBox();
        handlerBox.setHandlerType(streamingTrack.getHandler());
        return handlerBox;
    }

    public Box createMdia(StreamingTrack streamingTrack) {
        MediaBox mediaBox = new MediaBox();
        mediaBox.addBox(createMdhd(streamingTrack));
        mediaBox.addBox(createMdiaHdlr(streamingTrack));
        mediaBox.addBox(createMinf(streamingTrack));
        return mediaBox;
    }

    protected Box createMinf(StreamingTrack streamingTrack) {
        MediaInformationBox mediaInformationBox = new MediaInformationBox();
        if (streamingTrack.getHandler().equals("vide")) {
            mediaInformationBox.addBox(new VideoMediaHeaderBox());
        } else if (streamingTrack.getHandler().equals("soun")) {
            mediaInformationBox.addBox(new SoundMediaHeaderBox());
        } else if (streamingTrack.getHandler().equals(DraftDatabase.Draft.TEXT)) {
            mediaInformationBox.addBox(new NullMediaHeaderBox());
        } else if (streamingTrack.getHandler().equals("subt")) {
            mediaInformationBox.addBox(new SubtitleMediaHeaderBox());
        } else if (streamingTrack.getHandler().equals("hint")) {
            mediaInformationBox.addBox(new HintMediaHeaderBox());
        } else if (streamingTrack.getHandler().equals("sbtl")) {
            mediaInformationBox.addBox(new NullMediaHeaderBox());
        }
        mediaInformationBox.addBox(createDinf());
        mediaInformationBox.addBox(createStbl(streamingTrack));
        return mediaInformationBox;
    }

    protected Box createStbl(StreamingTrack streamingTrack) {
        SampleTableBox sampleTableBox = new SampleTableBox();
        sampleTableBox.addBox(streamingTrack.getSampleDescriptionBox());
        sampleTableBox.addBox(new TimeToSampleBox());
        sampleTableBox.addBox(new SampleToChunkBox());
        sampleTableBox.addBox(new SampleSizeBox());
        sampleTableBox.addBox(new StaticChunkOffsetBox());
        return sampleTableBox;
    }

    protected DataInformationBox createDinf() {
        DataInformationBox dataInformationBox = new DataInformationBox();
        DataReferenceBox dataReferenceBox = new DataReferenceBox();
        dataInformationBox.addBox(dataReferenceBox);
        DataEntryUrlBox dataEntryUrlBox = new DataEntryUrlBox();
        dataEntryUrlBox.setFlags(1);
        dataReferenceBox.addBox(dataEntryUrlBox);
        return dataInformationBox;
    }

    protected Box createTrak(StreamingTrack streamingTrack) {
        TrackBox trackBox = new TrackBox();
        trackBox.addBox(createTkhd(streamingTrack));
        trackBox.addBox(createMdia(streamingTrack));
        return trackBox;
    }

    public Box createTkhd(StreamingTrack streamingTrack) {
        TrackHeaderBox trackHeaderBox = new TrackHeaderBox();
        trackHeaderBox.setTrackId(((TrackIdTrackExtension) streamingTrack.getTrackExtension(TrackIdTrackExtension.class)).getTrackId());
        DimensionTrackExtension dimensionTrackExtension = (DimensionTrackExtension) streamingTrack.getTrackExtension(DimensionTrackExtension.class);
        if (dimensionTrackExtension != null) {
            trackHeaderBox.setHeight((double) dimensionTrackExtension.getHeight());
            trackHeaderBox.setWidth((double) dimensionTrackExtension.getWidth());
        }
        return trackHeaderBox;
    }
}
