package org.mp4parser.streaming.output;

import java.io.Closeable;
import java.io.IOException;
import org.mp4parser.streaming.StreamingSample;
import org.mp4parser.streaming.StreamingTrack;

/* loaded from: classes3.dex */
public interface SampleSink extends Closeable {
    void acceptSample(StreamingSample streamingSample, StreamingTrack streamingTrack) throws IOException;
}
