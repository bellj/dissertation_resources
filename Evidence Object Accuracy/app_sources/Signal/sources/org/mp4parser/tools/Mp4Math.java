package org.mp4parser.tools;

/* loaded from: classes3.dex */
public class Mp4Math {
    public static long gcd(long j, long j2) {
        while (true) {
            j = j2;
            if (j <= 0) {
                return j;
            }
            j2 = j % j;
        }
    }

    public static long lcm(long j, long j2) {
        return j * (j2 / gcd(j, j2));
    }

    public static long lcm(long[] jArr) {
        long j = jArr[0];
        for (int i = 1; i < jArr.length; i++) {
            j = lcm(j, jArr[i]);
        }
        return j;
    }
}
