package org.mp4parser.support;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import org.mp4parser.BasicContainer;
import org.mp4parser.ParsableBox;
import org.mp4parser.tools.IsoTypeWriter;
import org.thoughtcrime.securesms.database.MmsSmsColumns;

/* loaded from: classes3.dex */
public class AbstractContainerBox extends BasicContainer implements ParsableBox {
    protected boolean largeBox;
    protected String type;

    public AbstractContainerBox(String str) {
        this.type = str;
    }

    @Override // org.mp4parser.Box
    public long getSize() {
        long containerSize = getContainerSize();
        return containerSize + ((long) ((this.largeBox || 8 + containerSize >= MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION) ? 16 : 8));
    }

    @Override // org.mp4parser.Box
    public String getType() {
        return this.type;
    }

    public ByteBuffer getHeader() {
        ByteBuffer byteBuffer;
        if (this.largeBox || getSize() >= MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION) {
            byte[] bArr = new byte[16];
            bArr[3] = 1;
            bArr[4] = this.type.getBytes()[0];
            bArr[5] = this.type.getBytes()[1];
            bArr[6] = this.type.getBytes()[2];
            bArr[7] = this.type.getBytes()[3];
            byteBuffer = ByteBuffer.wrap(bArr);
            byteBuffer.position(8);
            IsoTypeWriter.writeUInt64(byteBuffer, getSize());
        } else {
            byte[] bArr2 = new byte[8];
            bArr2[4] = this.type.getBytes()[0];
            bArr2[5] = this.type.getBytes()[1];
            bArr2[6] = this.type.getBytes()[2];
            bArr2[7] = this.type.getBytes()[3];
            byteBuffer = ByteBuffer.wrap(bArr2);
            IsoTypeWriter.writeUInt32(byteBuffer, getSize());
        }
        byteBuffer.rewind();
        return byteBuffer;
    }

    @Override // org.mp4parser.Box
    public void getBox(WritableByteChannel writableByteChannel) throws IOException {
        writableByteChannel.write(getHeader());
        writeContainer(writableByteChannel);
    }
}
