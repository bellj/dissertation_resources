package org.mp4parser.support;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import org.mp4parser.IsoFile;
import org.mp4parser.ParsableBox;
import org.mp4parser.tools.CastUtils;
import org.mp4parser.tools.IsoTypeWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes3.dex */
public abstract class AbstractBox implements ParsableBox {
    private static Logger LOG = LoggerFactory.getLogger(AbstractBox.class);
    protected ByteBuffer content;
    private ByteBuffer deadBytes = null;
    boolean isParsed;
    protected String type;
    private byte[] userType;

    protected abstract void _parseDetails(ByteBuffer byteBuffer);

    protected abstract void getContent(ByteBuffer byteBuffer);

    protected abstract long getContentSize();

    public AbstractBox(String str) {
        this.type = str;
        this.isParsed = true;
    }

    @Override // org.mp4parser.Box
    public void getBox(WritableByteChannel writableByteChannel) throws IOException {
        if (this.isParsed) {
            ByteBuffer allocate = ByteBuffer.allocate(CastUtils.l2i(getSize()));
            getHeader(allocate);
            getContent(allocate);
            ByteBuffer byteBuffer = this.deadBytes;
            if (byteBuffer != null) {
                byteBuffer.rewind();
                while (this.deadBytes.remaining() > 0) {
                    allocate.put(this.deadBytes);
                }
            }
            writableByteChannel.write((ByteBuffer) allocate.rewind());
            return;
        }
        int i = 16;
        int i2 = isSmallBox() ? 8 : 16;
        if (!RecipientDatabase.SERVICE_ID.equals(getType())) {
            i = 0;
        }
        ByteBuffer allocate2 = ByteBuffer.allocate(i2 + i);
        getHeader(allocate2);
        writableByteChannel.write((ByteBuffer) allocate2.rewind());
        writableByteChannel.write((ByteBuffer) this.content.position(0));
    }

    public final synchronized void parseDetails() {
        LOG.debug("parsing details of {}", getType());
        ByteBuffer byteBuffer = this.content;
        if (byteBuffer != null) {
            this.isParsed = true;
            byteBuffer.rewind();
            _parseDetails(byteBuffer);
            if (byteBuffer.remaining() > 0) {
                this.deadBytes = byteBuffer.slice();
            }
            this.content = null;
        }
    }

    @Override // org.mp4parser.Box
    public long getSize() {
        long contentSize = this.isParsed ? getContentSize() : (long) this.content.limit();
        int i = 0;
        long j = contentSize + ((long) ((contentSize >= 4294967288L ? 8 : 0) + 8 + (RecipientDatabase.SERVICE_ID.equals(getType()) ? 16 : 0)));
        ByteBuffer byteBuffer = this.deadBytes;
        if (byteBuffer != null) {
            i = byteBuffer.limit();
        }
        return j + ((long) i);
    }

    @Override // org.mp4parser.Box
    public String getType() {
        return this.type;
    }

    public byte[] getUserType() {
        return this.userType;
    }

    public boolean isParsed() {
        return this.isParsed;
    }

    private boolean isSmallBox() {
        int i = RecipientDatabase.SERVICE_ID.equals(getType()) ? 24 : 8;
        if (!this.isParsed) {
            return ((long) (this.content.limit() + i)) < MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION;
        }
        long contentSize = getContentSize();
        ByteBuffer byteBuffer = this.deadBytes;
        return (contentSize + ((long) (byteBuffer != null ? byteBuffer.limit() : 0))) + ((long) i) < MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION;
    }

    private void getHeader(ByteBuffer byteBuffer) {
        if (isSmallBox()) {
            IsoTypeWriter.writeUInt32(byteBuffer, getSize());
            byteBuffer.put(IsoFile.fourCCtoBytes(getType()));
        } else {
            IsoTypeWriter.writeUInt32(byteBuffer, 1);
            byteBuffer.put(IsoFile.fourCCtoBytes(getType()));
            IsoTypeWriter.writeUInt64(byteBuffer, getSize());
        }
        if (RecipientDatabase.SERVICE_ID.equals(getType())) {
            byteBuffer.put(getUserType());
        }
    }
}
