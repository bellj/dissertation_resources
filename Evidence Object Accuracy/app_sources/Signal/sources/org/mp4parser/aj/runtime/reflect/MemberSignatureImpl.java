package org.mp4parser.aj.runtime.reflect;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public abstract class MemberSignatureImpl extends SignatureImpl {
    public MemberSignatureImpl(int i, String str, Class cls) {
        super(i, str, cls);
    }
}
