package org.mp4parser.aj.runtime.reflect;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public abstract class CodeSignatureImpl extends MemberSignatureImpl {
    Class[] exceptionTypes;
    String[] parameterNames;
    Class[] parameterTypes;

    public CodeSignatureImpl(int i, String str, Class cls, Class[] clsArr, String[] strArr, Class[] clsArr2) {
        super(i, str, cls);
        this.parameterTypes = clsArr;
        this.parameterNames = strArr;
        this.exceptionTypes = clsArr2;
    }

    public Class[] getParameterTypes() {
        if (this.parameterTypes == null) {
            this.parameterTypes = extractTypes(3);
        }
        return this.parameterTypes;
    }

    public Class[] getExceptionTypes() {
        if (this.exceptionTypes == null) {
            this.exceptionTypes = extractTypes(5);
        }
        return this.exceptionTypes;
    }
}
