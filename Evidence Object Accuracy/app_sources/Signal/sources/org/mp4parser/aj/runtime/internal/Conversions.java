package org.mp4parser.aj.runtime.internal;

/* loaded from: classes3.dex */
public final class Conversions {
    public static Object intObject(int i) {
        return new Integer(i);
    }

    public static Object longObject(long j) {
        return new Long(j);
    }

    public static Object doubleObject(double d) {
        return new Double(d);
    }
}
