package org.mp4parser.aj.lang;

/* loaded from: classes3.dex */
public interface JoinPoint {

    /* loaded from: classes3.dex */
    public interface StaticPart {
        String toString();
    }

    Object getTarget();
}
