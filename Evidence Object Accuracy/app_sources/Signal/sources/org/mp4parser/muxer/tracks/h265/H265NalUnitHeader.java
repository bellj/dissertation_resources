package org.mp4parser.muxer.tracks.h265;

/* loaded from: classes3.dex */
public class H265NalUnitHeader {
    public int forbiddenZeroFlag;
    public int nalUnitType;
    public int nuhLayerId;
    public int nuhTemporalIdPlusOne;
}
