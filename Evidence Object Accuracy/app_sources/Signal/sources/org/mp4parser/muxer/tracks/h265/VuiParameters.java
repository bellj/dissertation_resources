package org.mp4parser.muxer.tracks.h265;

import java.io.IOException;
import org.mp4parser.muxer.tracks.h264.parsing.read.CAVLCReader;

/* loaded from: classes3.dex */
public class VuiParameters {
    public int aspect_ratio_idc;
    public boolean aspect_ratio_info_present_flag;
    public boolean colour_description_present_flag;
    public int colour_primaries;
    public int matrix_coeffs;
    public int min_spatial_segmentation_idc;
    public int sar_height;
    public int sar_width;
    public int transfer_characteristics;
    public int video_format;
    public boolean video_full_range_flag;
    public boolean video_signal_type_present_flag;
    public long vui_num_units_in_tick;
    public long vui_time_scale;
    public boolean vui_timing_info_present_flag = false;

    public VuiParameters(int i, CAVLCReader cAVLCReader) throws IOException {
        boolean readBool = cAVLCReader.readBool("aspect_ratio_info_present_flag");
        this.aspect_ratio_info_present_flag = readBool;
        if (readBool) {
            int readU = cAVLCReader.readU(8, "aspect_ratio_idc");
            this.aspect_ratio_idc = readU;
            if (readU == 255) {
                this.sar_width = cAVLCReader.readU(16, "sar_width");
                this.sar_height = cAVLCReader.readU(16, "sar_height");
            }
        }
        if (cAVLCReader.readBool("overscan_info_present_flag")) {
            cAVLCReader.readBool("overscan_appropriate_flag");
        }
        boolean readBool2 = cAVLCReader.readBool("video_signal_type_present_flag");
        this.video_signal_type_present_flag = readBool2;
        if (readBool2) {
            this.video_format = cAVLCReader.readU(3, "video_format");
            this.video_full_range_flag = cAVLCReader.readBool("video_full_range_flag");
            boolean readBool3 = cAVLCReader.readBool("colour_description_present_flag");
            this.colour_description_present_flag = readBool3;
            if (readBool3) {
                this.colour_primaries = cAVLCReader.readU(8, "colour_primaries");
                this.transfer_characteristics = cAVLCReader.readU(8, "transfer_characteristics");
                this.matrix_coeffs = cAVLCReader.readU(8, "matrix_coeffs");
            }
        }
        if (cAVLCReader.readBool("chroma_loc_info_present_flag")) {
            cAVLCReader.readUE("chroma_sample_loc_type_top_field");
            cAVLCReader.readUE("chroma_sample_loc_type_bottom_field");
        }
        cAVLCReader.readBool("neutral_chroma_indication_flag");
        cAVLCReader.readBool("field_seq_flag");
        cAVLCReader.readBool("frame_field_info_present_flag");
        if (cAVLCReader.readBool("default_display_window_flag")) {
            cAVLCReader.readUE("def_disp_win_left_offset");
            cAVLCReader.readUE("def_disp_win_right_offset");
            cAVLCReader.readUE("def_disp_win_top_offset");
            cAVLCReader.readUE("def_disp_win_bottom_offset");
        }
        boolean readBool4 = cAVLCReader.readBool("vui_timing_info_present_flag");
        this.vui_timing_info_present_flag = readBool4;
        if (readBool4) {
            this.vui_num_units_in_tick = cAVLCReader.readNBit(32, "vui_num_units_in_tick");
            this.vui_time_scale = cAVLCReader.readNBit(32, "vui_time_scale");
            if (cAVLCReader.readBool("vui_poc_proportional_to_timing_flag")) {
                cAVLCReader.readUE("vui_num_ticks_poc_diff_one_minus1");
            }
            if (cAVLCReader.readBool("vui_hrd_parameters_present_flag")) {
                new HrdParameters(true, i, cAVLCReader);
            }
        }
        if (cAVLCReader.readBool("bitstream_restriction_flag")) {
            cAVLCReader.readBool("tiles_fixed_structure_flag");
            cAVLCReader.readBool("motion_vectors_over_pic_boundaries_flag");
            cAVLCReader.readBool("restricted_ref_pic_lists_flag");
            this.min_spatial_segmentation_idc = cAVLCReader.readUE("min_spatial_segmentation_idc");
            cAVLCReader.readUE("max_bytes_per_pic_denom");
            cAVLCReader.readUE("max_bits_per_min_cu_denom");
            cAVLCReader.readUE("log2_max_mv_length_horizontal");
            cAVLCReader.readUE("log2_max_mv_length_vertical");
        }
    }
}
