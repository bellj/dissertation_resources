package org.mp4parser.muxer.tracks.h265;

import java.io.IOException;
import org.mp4parser.muxer.tracks.h264.parsing.read.CAVLCReader;

/* loaded from: classes3.dex */
public class SubLayerHrdParameters {
    public SubLayerHrdParameters(int i, int[] iArr, boolean z, CAVLCReader cAVLCReader) throws IOException {
        int i2 = iArr[i];
        int i3 = i2 + 1;
        int[] iArr2 = new int[i3];
        int[] iArr3 = new int[i3];
        int[] iArr4 = new int[i3];
        int[] iArr5 = new int[i3];
        boolean[] zArr = new boolean[i3];
        for (int i4 = 0; i4 <= i2; i4++) {
            iArr2[i4] = cAVLCReader.readUE("bit_rate_value_minus1[i]");
            iArr3[i4] = cAVLCReader.readUE("cpb_size_value_minus1[i]");
            if (z) {
                iArr4[i4] = cAVLCReader.readUE("cpb_size_du_value_minus1[i]");
                iArr5[i4] = cAVLCReader.readUE("bit_rate_du_value_minus1[i]");
            }
            zArr[i4] = cAVLCReader.readBool("cbr_flag[i]");
        }
    }
}
