package org.mp4parser.muxer.tracks.h265;

/* loaded from: classes3.dex */
public class HrdParameters {
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HrdParameters(boolean r11, int r12, org.mp4parser.muxer.tracks.h264.parsing.read.CAVLCReader r13) throws java.io.IOException {
        /*
            r10 = this;
            r10.<init>()
            r0 = 0
            if (r11 == 0) goto L_0x0057
            java.lang.String r11 = "nal_hrd_parameters_present_flag"
            boolean r11 = r13.readBool(r11)
            java.lang.String r1 = "vcl_hrd_parameters_present_flag"
            boolean r1 = r13.readBool(r1)
            if (r11 != 0) goto L_0x0016
            if (r1 == 0) goto L_0x0059
        L_0x0016:
            java.lang.String r2 = "sub_pic_hrd_params_present_flag"
            boolean r2 = r13.readBool(r2)
            r3 = 5
            if (r2 == 0) goto L_0x0035
            r4 = 8
            java.lang.String r5 = "tick_divisor_minus2"
            r13.readU(r4, r5)
            java.lang.String r4 = "du_cpb_removal_delay_increment_length_minus1"
            r13.readU(r3, r4)
            java.lang.String r4 = "sub_pic_cpb_params_in_pic_timing_sei_flag"
            r13.readBool(r4)
            java.lang.String r4 = "dpb_output_delay_du_length_minus1"
            r13.readU(r3, r4)
        L_0x0035:
            r4 = 4
            java.lang.String r5 = "bit_rate_scale"
            r13.readU(r4, r5)
            java.lang.String r5 = "cpb_size_scale"
            r13.readU(r4, r5)
            if (r2 == 0) goto L_0x0047
            java.lang.String r5 = "cpb_size_du_scale"
            r13.readU(r4, r5)
        L_0x0047:
            java.lang.String r4 = "initial_cpb_removal_delay_length_minus1"
            r13.readU(r3, r4)
            java.lang.String r4 = "au_cpb_removal_delay_length_minus1"
            r13.readU(r3, r4)
            java.lang.String r4 = "dpb_output_delay_length_minus1"
            r13.readU(r3, r4)
            goto L_0x005a
        L_0x0057:
            r11 = 0
            r1 = 0
        L_0x0059:
            r2 = 0
        L_0x005a:
            int r3 = r12 + 1
            boolean[] r4 = new boolean[r3]
            boolean[] r5 = new boolean[r3]
            int[] r6 = new int[r3]
            boolean[] r7 = new boolean[r3]
            int[] r3 = new int[r3]
        L_0x0066:
            if (r0 > r12) goto L_0x00aa
            java.lang.String r8 = "fixed_pic_rate_general_flag[i]"
            boolean r9 = r13.readBool(r8)
            r4[r0] = r9
            if (r9 != 0) goto L_0x0078
            boolean r8 = r13.readBool(r8)
            r5[r0] = r8
        L_0x0078:
            boolean r8 = r5[r0]
            if (r8 == 0) goto L_0x0085
            java.lang.String r8 = "elemental_duration_in_tc_minus1[i]"
            int r8 = r13.readUE(r8)
            r6[r0] = r8
            goto L_0x008d
        L_0x0085:
            java.lang.String r8 = "low_delay_hrd_flag[i]"
            boolean r8 = r13.readBool(r8)
            r7[r0] = r8
        L_0x008d:
            boolean r8 = r7[r0]
            if (r8 != 0) goto L_0x0099
            java.lang.String r8 = "cpb_cnt_minus1[i]"
            int r8 = r13.readUE(r8)
            r3[r0] = r8
        L_0x0099:
            if (r11 == 0) goto L_0x00a0
            org.mp4parser.muxer.tracks.h265.SubLayerHrdParameters r8 = new org.mp4parser.muxer.tracks.h265.SubLayerHrdParameters
            r8.<init>(r0, r3, r2, r13)
        L_0x00a0:
            if (r1 == 0) goto L_0x00a7
            org.mp4parser.muxer.tracks.h265.SubLayerHrdParameters r8 = new org.mp4parser.muxer.tracks.h265.SubLayerHrdParameters
            r8.<init>(r0, r3, r2, r13)
        L_0x00a7:
            int r0 = r0 + 1
            goto L_0x0066
        L_0x00aa:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mp4parser.muxer.tracks.h265.HrdParameters.<init>(boolean, int, org.mp4parser.muxer.tracks.h264.parsing.read.CAVLCReader):void");
    }
}
