package org.mp4parser.boxes.iso14496.part12;

import org.mp4parser.support.AbstractFullBox;

/* loaded from: classes3.dex */
public abstract class AbstractMediaHeaderBox extends AbstractFullBox {
    public AbstractMediaHeaderBox(String str) {
        super(str);
    }
}
