package org.mp4parser.boxes.iso14496.part12;

import org.mp4parser.support.AbstractContainerBox;

/* loaded from: classes3.dex */
public class TrackBox extends AbstractContainerBox {
    public TrackBox() {
        super("trak");
    }
}
