package org.mp4parser.boxes.sampleentry;

import org.mp4parser.support.AbstractContainerBox;

/* loaded from: classes3.dex */
public abstract class AbstractSampleEntry extends AbstractContainerBox {
    protected int dataReferenceIndex = 1;

    public AbstractSampleEntry(String str) {
        super(str);
    }

    public void setDataReferenceIndex(int i) {
        this.dataReferenceIndex = i;
    }
}
