package org.apache.http.client.methods;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.LinkedList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntityHC4;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.HeaderGroup;
import org.apache.http.util.Args;

/* loaded from: classes3.dex */
public class RequestBuilder {
    private RequestConfig config;
    private HttpEntity entity;
    private HeaderGroup headergroup;
    private String method;
    private LinkedList<NameValuePair> parameters;
    private URI uri;
    private ProtocolVersion version;

    RequestBuilder(String str) {
        this.method = str;
    }

    RequestBuilder() {
        this(null);
    }

    public static RequestBuilder copy(HttpRequest httpRequest) {
        Args.notNull(httpRequest, "HTTP request");
        return new RequestBuilder().doCopy(httpRequest);
    }

    private RequestBuilder doCopy(HttpRequest httpRequest) {
        if (httpRequest == null) {
            return this;
        }
        this.method = httpRequest.getRequestLine().getMethod();
        this.version = httpRequest.getRequestLine().getProtocolVersion();
        if (httpRequest instanceof HttpUriRequest) {
            this.uri = ((HttpUriRequest) httpRequest).getURI();
        } else {
            this.uri = URI.create(httpRequest.getRequestLine().getUri());
        }
        if (this.headergroup == null) {
            this.headergroup = new HeaderGroup();
        }
        this.headergroup.clear();
        this.headergroup.setHeaders(httpRequest.getAllHeaders());
        if (httpRequest instanceof HttpEntityEnclosingRequest) {
            this.entity = ((HttpEntityEnclosingRequest) httpRequest).getEntity();
        } else {
            this.entity = null;
        }
        if (httpRequest instanceof Configurable) {
            this.config = ((Configurable) httpRequest).getConfig();
        } else {
            this.config = null;
        }
        this.parameters = null;
        return this;
    }

    public RequestBuilder setUri(URI uri) {
        this.uri = uri;
        return this;
    }

    public HttpUriRequest build() {
        HttpRequestBaseHC4 httpRequestBaseHC4;
        URI uri = this.uri;
        if (uri == null) {
            uri = URI.create("/");
        }
        UrlEncodedFormEntityHC4 urlEncodedFormEntityHC4 = this.entity;
        LinkedList<NameValuePair> linkedList = this.parameters;
        if (linkedList != null && !linkedList.isEmpty()) {
            if (urlEncodedFormEntityHC4 != null || (!"POST".equalsIgnoreCase(this.method) && !"PUT".equalsIgnoreCase(this.method))) {
                try {
                    uri = new URIBuilder(uri).addParameters(this.parameters).build();
                } catch (URISyntaxException unused) {
                }
            } else {
                urlEncodedFormEntityHC4 = new UrlEncodedFormEntityHC4(this.parameters, Charset.forName("ISO-8859-1"));
            }
        }
        if (urlEncodedFormEntityHC4 == null) {
            httpRequestBaseHC4 = new InternalRequest(this.method);
        } else {
            InternalEntityEclosingRequest internalEntityEclosingRequest = new InternalEntityEclosingRequest(this.method);
            internalEntityEclosingRequest.setEntity(urlEncodedFormEntityHC4);
            httpRequestBaseHC4 = internalEntityEclosingRequest;
        }
        httpRequestBaseHC4.setProtocolVersion(this.version);
        httpRequestBaseHC4.setURI(uri);
        HeaderGroup headerGroup = this.headergroup;
        if (headerGroup != null) {
            httpRequestBaseHC4.setHeaders(headerGroup.getAllHeaders());
        }
        httpRequestBaseHC4.setConfig(this.config);
        return httpRequestBaseHC4;
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class InternalRequest extends HttpRequestBaseHC4 {
        private final String method;

        InternalRequest(String str) {
            this.method = str;
        }

        @Override // org.apache.http.client.methods.HttpRequestBaseHC4
        public String getMethod() {
            return this.method;
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class InternalEntityEclosingRequest extends HttpEntityEnclosingRequestBaseHC4 {
        private final String method;

        InternalEntityEclosingRequest(String str) {
            this.method = str;
        }

        @Override // org.apache.http.client.methods.HttpRequestBaseHC4
        public String getMethod() {
            return this.method;
        }
    }
}
