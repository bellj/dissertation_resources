package org.apache.http.client.methods;

import java.net.URI;

/* loaded from: classes3.dex */
public class HttpGetHC4 extends HttpRequestBaseHC4 {
    @Override // org.apache.http.client.methods.HttpRequestBaseHC4
    public String getMethod() {
        return "GET";
    }

    public HttpGetHC4(URI uri) {
        setURI(uri);
    }

    public HttpGetHC4(String str) {
        setURI(URI.create(str));
    }
}
