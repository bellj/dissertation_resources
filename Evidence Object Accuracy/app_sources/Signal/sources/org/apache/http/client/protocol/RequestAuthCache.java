package org.apache.http.client.protocol;

import android.util.Log;
import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthProtocolState;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthStateHC4;
import org.apache.http.auth.Credentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.routing.RouteInfo;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.Args;

/* loaded from: classes3.dex */
public class RequestAuthCache implements HttpRequestInterceptor {
    public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
        AuthScheme authScheme;
        AuthScheme authScheme2;
        Args.notNull(httpRequest, "HTTP request");
        Args.notNull(httpContext, "HTTP context");
        HttpClientContext adapt = HttpClientContext.adapt(httpContext);
        AuthCache authCache = adapt.getAuthCache();
        if (authCache != null) {
            CredentialsProvider credentialsProvider = adapt.getCredentialsProvider();
            if (credentialsProvider != null) {
                RouteInfo httpRoute = adapt.getHttpRoute();
                if (httpRoute != null) {
                    HttpHost targetHost = adapt.getTargetHost();
                    if (targetHost != null) {
                        if (targetHost.getPort() < 0) {
                            targetHost = new HttpHost(targetHost.getHostName(), httpRoute.getTargetHost().getPort(), targetHost.getSchemeName());
                        }
                        AuthStateHC4 targetAuthState = adapt.getTargetAuthState();
                        if (!(targetAuthState == null || targetAuthState.getState() != AuthProtocolState.UNCHALLENGED || (authScheme2 = authCache.get(targetHost)) == null)) {
                            doPreemptiveAuth(targetHost, authScheme2, targetAuthState, credentialsProvider);
                        }
                        HttpHost proxyHost = httpRoute.getProxyHost();
                        AuthStateHC4 proxyAuthState = adapt.getProxyAuthState();
                        if (proxyHost != null && proxyAuthState != null && proxyAuthState.getState() == AuthProtocolState.UNCHALLENGED && (authScheme = authCache.get(proxyHost)) != null) {
                            doPreemptiveAuth(proxyHost, authScheme, proxyAuthState, credentialsProvider);
                        }
                    } else if (Log.isLoggable("HttpClient", 3)) {
                        Log.d("HttpClient", "Target host not set in the context");
                    }
                } else if (Log.isLoggable("HttpClient", 3)) {
                    Log.d("HttpClient", "Route info not set in the context");
                }
            } else if (Log.isLoggable("HttpClient", 3)) {
                Log.d("HttpClient", "Credentials provider not set in the context");
            }
        } else if (Log.isLoggable("HttpClient", 3)) {
            Log.d("HttpClient", "Auth cache not set in the context");
        }
    }

    private void doPreemptiveAuth(HttpHost httpHost, AuthScheme authScheme, AuthStateHC4 authStateHC4, CredentialsProvider credentialsProvider) {
        String schemeName = authScheme.getSchemeName();
        if (Log.isLoggable("HttpClient", 3)) {
            Log.d("HttpClient", "Re-using cached '" + schemeName + "' auth scheme for " + httpHost);
        }
        Credentials credentials = credentialsProvider.getCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort(), AuthScope.ANY_REALM, schemeName));
        if (credentials != null) {
            if ("BASIC".equalsIgnoreCase(authScheme.getSchemeName())) {
                authStateHC4.setState(AuthProtocolState.CHALLENGED);
            } else {
                authStateHC4.setState(AuthProtocolState.SUCCESS);
            }
            authStateHC4.update(authScheme, credentials);
        } else if (Log.isLoggable("HttpClient", 3)) {
            Log.d("HttpClient", "No credentials for preemptive authentication");
        }
    }
}
