package org.apache.http.client.protocol;

import android.util.Log;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.config.Lookup;
import org.apache.http.conn.routing.RouteInfo;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.CookieSpec;
import org.apache.http.cookie.CookieSpecProvider;
import org.apache.http.cookie.SetCookie2;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.Args;
import org.apache.http.util.TextUtils;

/* loaded from: classes3.dex */
public class RequestAddCookiesHC4 implements HttpRequestInterceptor {
    public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
        URI uri;
        Header versionHeader;
        Args.notNull(httpRequest, "HTTP request");
        Args.notNull(httpContext, "HTTP context");
        if (!httpRequest.getRequestLine().getMethod().equalsIgnoreCase("CONNECT")) {
            HttpContext adapt = HttpClientContext.adapt(httpContext);
            CookieStore cookieStore = adapt.getCookieStore();
            if (cookieStore != null) {
                Lookup<CookieSpecProvider> cookieSpecRegistry = adapt.getCookieSpecRegistry();
                if (cookieSpecRegistry != null) {
                    HttpHost targetHost = adapt.getTargetHost();
                    if (targetHost != null) {
                        RouteInfo httpRoute = adapt.getHttpRoute();
                        if (httpRoute != null) {
                            String cookieSpec = adapt.getRequestConfig().getCookieSpec();
                            if (cookieSpec == null) {
                                cookieSpec = "best-match";
                            }
                            if (Log.isLoggable("HttpClient", 3)) {
                                Log.d("HttpClient", "CookieSpec selected: " + cookieSpec);
                            }
                            String str = null;
                            if (httpRequest instanceof HttpUriRequest) {
                                uri = ((HttpUriRequest) httpRequest).getURI();
                            } else {
                                try {
                                    uri = new URI(httpRequest.getRequestLine().getUri());
                                } catch (URISyntaxException unused) {
                                    uri = null;
                                }
                            }
                            if (uri != null) {
                                str = uri.getPath();
                            }
                            String hostName = targetHost.getHostName();
                            int port = targetHost.getPort();
                            if (port < 0) {
                                port = httpRoute.getTargetHost().getPort();
                            }
                            boolean z = false;
                            if (port < 0) {
                                port = 0;
                            }
                            if (TextUtils.isEmpty(str)) {
                                str = "/";
                            }
                            CookieOrigin cookieOrigin = new CookieOrigin(hostName, port, str, httpRoute.isSecure());
                            CookieSpecProvider lookup = cookieSpecRegistry.lookup(cookieSpec);
                            if (lookup != null) {
                                CookieSpec create = lookup.create(adapt);
                                ArrayList<Cookie> arrayList = new ArrayList(cookieStore.getCookies());
                                ArrayList<Cookie> arrayList2 = new ArrayList();
                                Date date = new Date();
                                for (Cookie cookie : arrayList) {
                                    if (!cookie.isExpired(date)) {
                                        if (create.match(cookie, cookieOrigin)) {
                                            if (Log.isLoggable("HttpClient", 3)) {
                                                Log.d("HttpClient", "Cookie " + cookie + " match " + cookieOrigin);
                                            }
                                            arrayList2.add(cookie);
                                        }
                                    } else if (Log.isLoggable("HttpClient", 3)) {
                                        Log.d("HttpClient", "Cookie " + cookie + " expired");
                                    }
                                }
                                if (!arrayList2.isEmpty()) {
                                    for (Header header : create.formatCookies(arrayList2)) {
                                        httpRequest.addHeader(header);
                                    }
                                }
                                int version = create.getVersion();
                                if (version > 0) {
                                    for (Cookie cookie2 : arrayList2) {
                                        if (version != cookie2.getVersion() || !(cookie2 instanceof SetCookie2)) {
                                            z = true;
                                        }
                                    }
                                    if (z && (versionHeader = create.getVersionHeader()) != null) {
                                        httpRequest.addHeader(versionHeader);
                                    }
                                }
                                httpContext.setAttribute("http.cookie-spec", create);
                                httpContext.setAttribute("http.cookie-origin", cookieOrigin);
                                return;
                            }
                            throw new HttpException("Unsupported cookie policy: " + cookieSpec);
                        } else if (Log.isLoggable("HttpClient", 3)) {
                            Log.d("HttpClient", "Connection route not set in the context");
                        }
                    } else if (Log.isLoggable("HttpClient", 3)) {
                        Log.d("HttpClient", "Target host not set in the context");
                    }
                } else if (Log.isLoggable("HttpClient", 3)) {
                    Log.d("HttpClient", "CookieSpec registry not specified in HTTP context");
                }
            } else if (Log.isLoggable("HttpClient", 3)) {
                Log.d("HttpClient", "Cookie store not specified in HTTP context");
            }
        }
    }
}
