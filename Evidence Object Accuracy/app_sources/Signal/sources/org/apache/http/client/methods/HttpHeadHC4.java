package org.apache.http.client.methods;

import java.net.URI;

/* loaded from: classes3.dex */
public class HttpHeadHC4 extends HttpRequestBaseHC4 {
    @Override // org.apache.http.client.methods.HttpRequestBaseHC4
    public String getMethod() {
        return "HEAD";
    }

    public HttpHeadHC4(URI uri) {
        setURI(uri);
    }
}
