package org.apache.http.client.methods;

import java.net.URI;

/* loaded from: classes3.dex */
public class HttpPostHC4 extends HttpEntityEnclosingRequestBaseHC4 {
    @Override // org.apache.http.client.methods.HttpRequestBaseHC4
    public String getMethod() {
        return "POST";
    }

    public HttpPostHC4(String str) {
        setURI(URI.create(str));
    }
}
