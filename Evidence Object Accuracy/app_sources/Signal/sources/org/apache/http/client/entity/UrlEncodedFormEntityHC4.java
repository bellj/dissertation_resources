package org.apache.http.client.entity;

import java.nio.charset.Charset;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtilsHC4;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntityHC4;

/* loaded from: classes3.dex */
public class UrlEncodedFormEntityHC4 extends StringEntityHC4 {
    public UrlEncodedFormEntityHC4(Iterable<? extends NameValuePair> iterable, Charset charset) {
        super(URLEncodedUtilsHC4.format(iterable, charset != null ? charset : Charset.forName("ISO-8859-1")), ContentType.create("application/x-www-form-urlencoded", charset));
    }
}
