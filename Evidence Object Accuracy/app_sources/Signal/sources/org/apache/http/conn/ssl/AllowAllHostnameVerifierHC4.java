package org.apache.http.conn.ssl;

/* loaded from: classes3.dex */
public class AllowAllHostnameVerifierHC4 extends AbstractVerifierHC4 {
    @Override // java.lang.Object
    public final String toString() {
        return "ALLOW_ALL";
    }

    @Override // org.apache.http.conn.ssl.X509HostnameVerifier
    public final void verify(String str, String[] strArr, String[] strArr2) {
    }
}
