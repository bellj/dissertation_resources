package org.apache.http.conn;

import java.io.IOException;

/* loaded from: classes3.dex */
public class UnsupportedSchemeException extends IOException {
    public UnsupportedSchemeException(String str) {
        super(str);
    }
}
