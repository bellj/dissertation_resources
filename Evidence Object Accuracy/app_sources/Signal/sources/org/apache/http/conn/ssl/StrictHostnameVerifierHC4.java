package org.apache.http.conn.ssl;

import javax.net.ssl.SSLException;

/* loaded from: classes3.dex */
public class StrictHostnameVerifierHC4 extends AbstractVerifierHC4 {
    @Override // java.lang.Object
    public final String toString() {
        return "STRICT";
    }

    @Override // org.apache.http.conn.ssl.X509HostnameVerifier
    public final void verify(String str, String[] strArr, String[] strArr2) throws SSLException {
        verify(str, strArr, strArr2, true);
    }
}
