package org.apache.http.entity;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Locale;
import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeaderValueFormatterHC4;
import org.apache.http.util.Args;
import org.apache.http.util.CharArrayBuffer;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes3.dex */
public final class ContentType implements Serializable {
    public static final ContentType APPLICATION_ATOM_XML;
    public static final ContentType APPLICATION_FORM_URLENCODED;
    public static final ContentType APPLICATION_JSON = create("application/json", Consts.UTF_8);
    public static final ContentType APPLICATION_OCTET_STREAM;
    public static final ContentType APPLICATION_SVG_XML;
    public static final ContentType APPLICATION_XHTML_XML;
    public static final ContentType APPLICATION_XML;
    public static final ContentType DEFAULT_BINARY;
    public static final ContentType DEFAULT_TEXT;
    public static final ContentType MULTIPART_FORM_DATA;
    public static final ContentType TEXT_HTML;
    public static final ContentType TEXT_PLAIN;
    public static final ContentType TEXT_XML;
    public static final ContentType WILDCARD = create(MediaUtil.UNKNOWN, null);
    private final Charset charset;
    private final String mimeType;
    private final NameValuePair[] params = null;

    static {
        Charset charset = Consts.ISO_8859_1;
        APPLICATION_ATOM_XML = create("application/atom+xml", charset);
        APPLICATION_FORM_URLENCODED = create("application/x-www-form-urlencoded", charset);
        APPLICATION_JSON = create("application/json", Consts.UTF_8);
        ContentType create = create(MediaUtil.OCTET, null);
        APPLICATION_OCTET_STREAM = create;
        APPLICATION_SVG_XML = create("application/svg+xml", charset);
        APPLICATION_XHTML_XML = create("application/xhtml+xml", charset);
        APPLICATION_XML = create("application/xml", charset);
        MULTIPART_FORM_DATA = create("multipart/form-data", charset);
        TEXT_HTML = create("text/html", charset);
        ContentType create2 = create("text/plain", charset);
        TEXT_PLAIN = create2;
        TEXT_XML = create("text/xml", charset);
        WILDCARD = create(MediaUtil.UNKNOWN, null);
        DEFAULT_TEXT = create2;
        DEFAULT_BINARY = create;
    }

    ContentType(String str, Charset charset) {
        this.mimeType = str;
        this.charset = charset;
    }

    public Charset getCharset() {
        return this.charset;
    }

    @Override // java.lang.Object
    public String toString() {
        CharArrayBuffer charArrayBuffer = new CharArrayBuffer(64);
        charArrayBuffer.append(this.mimeType);
        if (this.params != null) {
            charArrayBuffer.append("; ");
            BasicHeaderValueFormatterHC4.INSTANCE.formatParameters(charArrayBuffer, this.params, false);
        } else if (this.charset != null) {
            charArrayBuffer.append("; charset=");
            charArrayBuffer.append(this.charset.name());
        }
        return charArrayBuffer.toString();
    }

    private static boolean valid(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == ',' || charAt == ';') {
                return false;
            }
        }
        return true;
    }

    public static ContentType create(String str, Charset charset) {
        String lowerCase = ((String) Args.notBlank(str, "MIME type")).toLowerCase(Locale.US);
        Args.check(valid(lowerCase), "MIME type may not contain reserved characters");
        return new ContentType(lowerCase, charset);
    }
}
