package org.apache.http.entity;

import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

/* loaded from: classes3.dex */
public abstract class AbstractHttpEntityHC4 implements HttpEntity {
    protected boolean chunked;
    protected Header contentEncoding;
    protected Header contentType;

    @Deprecated
    public void consumeContent() throws IOException {
    }

    public Header getContentType() {
        return this.contentType;
    }

    public Header getContentEncoding() {
        return this.contentEncoding;
    }

    public boolean isChunked() {
        return this.chunked;
    }

    public void setContentType(Header header) {
        this.contentType = header;
    }

    public void setContentType(String str) {
        setContentType((Header) (str != null ? new BasicHeader("Content-Type", str) : null));
    }

    public void setContentEncoding(Header header) {
        this.contentEncoding = header;
    }

    public void setChunked(boolean z) {
        this.chunked = z;
    }
}
