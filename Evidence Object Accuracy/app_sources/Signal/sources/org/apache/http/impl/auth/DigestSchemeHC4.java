package org.apache.http.impl.auth;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Locale;
import java.util.StringTokenizer;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.message.BasicHeaderValueFormatterHC4;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.message.BufferedHeader;
import org.apache.http.protocol.BasicHttpContextHC4;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.Args;
import org.apache.http.util.CharArrayBuffer;
import org.apache.http.util.EncodingUtils;

/* loaded from: classes3.dex */
public class DigestSchemeHC4 extends RFC2617SchemeHC4 {
    private static final char[] HEXADECIMAL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private String a1;
    private String a2;
    private String cnonce;
    private boolean complete;
    private String lastNonce;
    private long nounceCount;

    public String getSchemeName() {
        return "digest";
    }

    public boolean isConnectionBased() {
        return false;
    }

    public DigestSchemeHC4(Charset charset) {
        super(charset);
        this.complete = false;
    }

    public DigestSchemeHC4() {
        this(Consts.ASCII);
    }

    @Override // org.apache.http.impl.auth.AuthSchemeBaseHC4
    public void processChallenge(Header header) throws MalformedChallengeException {
        super.processChallenge(header);
        this.complete = true;
    }

    public boolean isComplete() {
        if ("true".equalsIgnoreCase(getParameter("stale"))) {
            return false;
        }
        return this.complete;
    }

    @Deprecated
    public Header authenticate(Credentials credentials, HttpRequest httpRequest) throws AuthenticationException {
        return authenticate(credentials, httpRequest, new BasicHttpContextHC4());
    }

    @Override // org.apache.http.impl.auth.AuthSchemeBaseHC4, org.apache.http.auth.ContextAwareAuthScheme
    public Header authenticate(Credentials credentials, HttpRequest httpRequest, HttpContext httpContext) throws AuthenticationException {
        Args.notNull(credentials, "Credentials");
        Args.notNull(httpRequest, "HTTP request");
        if (getParameter("realm") == null) {
            throw new AuthenticationException("missing realm in challenge");
        } else if (getParameter("nonce") != null) {
            getParameters().put("methodname", httpRequest.getRequestLine().getMethod());
            getParameters().put("uri", httpRequest.getRequestLine().getUri());
            if (getParameter("charset") == null) {
                getParameters().put("charset", getCredentialsCharset(httpRequest));
            }
            return createDigestHeader(credentials, httpRequest);
        } else {
            throw new AuthenticationException("missing nonce in challenge");
        }
    }

    private static MessageDigest createMessageDigest(String str) throws UnsupportedDigestAlgorithmException {
        try {
            return MessageDigest.getInstance(str);
        } catch (Exception unused) {
            throw new UnsupportedDigestAlgorithmException("Unsupported algorithm in HTTP Digest authentication: " + str);
        }
    }

    private Header createDigestHeader(Credentials credentials, HttpRequest httpRequest) throws AuthenticationException {
        String str;
        char c;
        String str2;
        String str3;
        MessageDigest messageDigest;
        String str4;
        String str5;
        String str6;
        String str7;
        char c2;
        String parameter = getParameter("uri");
        String parameter2 = getParameter("realm");
        String parameter3 = getParameter("nonce");
        String parameter4 = getParameter("opaque");
        String parameter5 = getParameter("methodname");
        String parameter6 = getParameter("algorithm");
        if (parameter6 == null) {
            parameter6 = "MD5";
        }
        HashSet hashSet = new HashSet(8);
        String str8 = "MD5";
        String parameter7 = getParameter("qop");
        if (parameter7 != null) {
            str = "qop";
            for (StringTokenizer stringTokenizer = new StringTokenizer(parameter7, ","); stringTokenizer.hasMoreTokens(); stringTokenizer = stringTokenizer) {
                hashSet.add(stringTokenizer.nextToken().trim().toLowerCase(Locale.ENGLISH));
            }
            c = (!(httpRequest instanceof HttpEntityEnclosingRequest) || !hashSet.contains("auth-int")) ? hashSet.contains("auth") ? (char) 2 : 65535 : 1;
        } else {
            str = "qop";
            c = 0;
        }
        if (c != 65535) {
            String parameter8 = getParameter("charset");
            if (parameter8 == null) {
                parameter8 = "ISO-8859-1";
            }
            if (parameter6.equalsIgnoreCase("MD5-sess")) {
                str2 = "auth-int";
            } else {
                str2 = "auth-int";
                str8 = parameter6;
            }
            try {
                MessageDigest createMessageDigest = createMessageDigest(str8);
                String name = credentials.getUserPrincipal().getName();
                String password = credentials.getPassword();
                if (parameter3.equals(this.lastNonce)) {
                    str3 = parameter;
                    this.nounceCount++;
                } else {
                    str3 = parameter;
                    this.nounceCount = 1;
                    this.cnonce = null;
                    this.lastNonce = parameter3;
                }
                StringBuilder sb = new StringBuilder(256);
                Formatter formatter = new Formatter(sb, Locale.US);
                formatter.format("%08x", Long.valueOf(this.nounceCount));
                formatter.close();
                String sb2 = sb.toString();
                if (this.cnonce == null) {
                    this.cnonce = createCnonce();
                }
                this.a1 = null;
                this.a2 = null;
                if (parameter6.equalsIgnoreCase("MD5-sess")) {
                    sb.setLength(0);
                    sb.append(name);
                    sb.append(':');
                    sb.append(parameter2);
                    sb.append(':');
                    sb.append(password);
                    messageDigest = createMessageDigest;
                    String encode = encode(messageDigest.digest(EncodingUtils.getBytes(sb.toString(), parameter8)));
                    sb.setLength(0);
                    sb.append(encode);
                    sb.append(':');
                    sb.append(parameter3);
                    sb.append(':');
                    sb.append(this.cnonce);
                    this.a1 = sb.toString();
                } else {
                    messageDigest = createMessageDigest;
                    sb.setLength(0);
                    sb.append(name);
                    sb.append(':');
                    sb.append(parameter2);
                    sb.append(':');
                    sb.append(password);
                    this.a1 = sb.toString();
                }
                String encode2 = encode(messageDigest.digest(EncodingUtils.getBytes(this.a1, parameter8)));
                if (c == 2) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(parameter5);
                    sb3.append(':');
                    str5 = str3;
                    sb3.append(str5);
                    this.a2 = sb3.toString();
                    str4 = "auth";
                } else {
                    str5 = str3;
                    if (c == 1) {
                        HttpEntity entity = httpRequest instanceof HttpEntityEnclosingRequest ? ((HttpEntityEnclosingRequest) httpRequest).getEntity() : null;
                        if (entity == null || entity.isRepeatable()) {
                            str4 = "auth";
                            HttpEntityDigester httpEntityDigester = new HttpEntityDigester(messageDigest);
                            if (entity != null) {
                                try {
                                    entity.writeTo(httpEntityDigester);
                                } catch (IOException e) {
                                    throw new AuthenticationException("I/O error reading entity content", e);
                                }
                            }
                            httpEntityDigester.close();
                            this.a2 = parameter5 + ':' + str5 + ':' + encode(httpEntityDigester.getDigest());
                            c2 = c;
                        } else {
                            str4 = "auth";
                            if (hashSet.contains(str4)) {
                                this.a2 = parameter5 + ':' + str5;
                                c2 = 2;
                            } else {
                                throw new AuthenticationException("Qop auth-int cannot be used with a non-repeatable entity");
                            }
                        }
                        c = c2;
                    } else {
                        str4 = "auth";
                        this.a2 = parameter5 + ':' + str5;
                    }
                }
                String encode3 = encode(messageDigest.digest(EncodingUtils.getBytes(this.a2, parameter8)));
                if (c == 0) {
                    sb.setLength(0);
                    sb.append(encode2);
                    sb.append(':');
                    sb.append(parameter3);
                    sb.append(':');
                    sb.append(encode3);
                    str6 = sb.toString();
                } else {
                    sb.setLength(0);
                    sb.append(encode2);
                    sb.append(':');
                    sb.append(parameter3);
                    sb.append(':');
                    sb.append(sb2);
                    sb.append(':');
                    sb.append(this.cnonce);
                    sb.append(':');
                    sb.append(c == 1 ? str2 : str4);
                    sb.append(':');
                    sb.append(encode3);
                    str6 = sb.toString();
                }
                String encode4 = encode(messageDigest.digest(EncodingUtils.getAsciiBytes(str6)));
                CharArrayBuffer charArrayBuffer = new CharArrayBuffer(128);
                if (isProxy()) {
                    charArrayBuffer.append("Proxy-Authorization");
                } else {
                    charArrayBuffer.append("Authorization");
                }
                charArrayBuffer.append(": Digest ");
                ArrayList arrayList = new ArrayList(20);
                arrayList.add(new BasicNameValuePair("username", name));
                arrayList.add(new BasicNameValuePair("realm", parameter2));
                arrayList.add(new BasicNameValuePair("nonce", parameter3));
                arrayList.add(new BasicNameValuePair("uri", str5));
                arrayList.add(new BasicNameValuePair("response", encode4));
                if (c != 0) {
                    if (c == 1) {
                        str4 = str2;
                    }
                    str7 = str;
                    arrayList.add(new BasicNameValuePair(str7, str4));
                    arrayList.add(new BasicNameValuePair("nc", sb2));
                    arrayList.add(new BasicNameValuePair("cnonce", this.cnonce));
                } else {
                    str7 = str;
                }
                arrayList.add(new BasicNameValuePair("algorithm", parameter6));
                if (parameter4 != null) {
                    arrayList.add(new BasicNameValuePair("opaque", parameter4));
                }
                for (int i = 0; i < arrayList.size(); i++) {
                    NameValuePair nameValuePair = (BasicNameValuePair) arrayList.get(i);
                    if (i > 0) {
                        charArrayBuffer.append(", ");
                    }
                    String name2 = nameValuePair.getName();
                    BasicHeaderValueFormatterHC4.INSTANCE.formatNameValuePair(charArrayBuffer, nameValuePair, !("nc".equals(name2) || str7.equals(name2) || "algorithm".equals(name2)));
                }
                return new BufferedHeader(charArrayBuffer);
            } catch (UnsupportedDigestAlgorithmException unused) {
                throw new AuthenticationException("Unsuppported digest algorithm: " + str8);
            }
        } else {
            throw new AuthenticationException("None of the qop methods is supported: " + parameter7);
        }
    }

    static String encode(byte[] bArr) {
        int length = bArr.length;
        char[] cArr = new char[length * 2];
        for (int i = 0; i < length; i++) {
            byte b = bArr[i];
            int i2 = i * 2;
            char[] cArr2 = HEXADECIMAL;
            cArr[i2] = cArr2[(b & 240) >> 4];
            cArr[i2 + 1] = cArr2[b & 15];
        }
        return new String(cArr);
    }

    public static String createCnonce() {
        byte[] bArr = new byte[8];
        new SecureRandom().nextBytes(bArr);
        return encode(bArr);
    }

    @Override // org.apache.http.impl.auth.AuthSchemeBaseHC4
    public String toString() {
        return "DIGEST [complete=" + this.complete + ", nonce=" + this.lastNonce + ", nc=" + this.nounceCount + "]";
    }
}
