package org.apache.http.impl.conn;

import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseFactory;
import org.apache.http.config.MessageConstraints;
import org.apache.http.impl.DefaultHttpResponseFactoryHC4;
import org.apache.http.impl.io.AbstractMessageParserHC4;
import org.apache.http.io.SessionInputBuffer;
import org.apache.http.message.LineParser;
import org.apache.http.util.CharArrayBuffer;

/* loaded from: classes3.dex */
public class DefaultHttpResponseParser extends AbstractMessageParserHC4<HttpResponse> {
    private final CharArrayBuffer lineBuf;
    private final HttpResponseFactory responseFactory;

    protected boolean reject(CharArrayBuffer charArrayBuffer, int i) {
        return false;
    }

    public DefaultHttpResponseParser(SessionInputBuffer sessionInputBuffer, LineParser lineParser, HttpResponseFactory httpResponseFactory, MessageConstraints messageConstraints) {
        super(sessionInputBuffer, lineParser, messageConstraints);
        this.responseFactory = httpResponseFactory == null ? DefaultHttpResponseFactoryHC4.INSTANCE : httpResponseFactory;
        this.lineBuf = new CharArrayBuffer(128);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0078, code lost:
        throw new org.apache.http.ProtocolException("The server failed to respond with a valid HTTP response");
     */
    @Override // org.apache.http.impl.io.AbstractMessageParserHC4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.http.HttpResponse parseHead(org.apache.http.io.SessionInputBuffer r8) throws java.io.IOException, org.apache.http.HttpException {
        /*
            r7 = this;
            r0 = 0
            r1 = 0
        L_0x0002:
            org.apache.http.util.CharArrayBuffer r2 = r7.lineBuf
            r2.clear()
            org.apache.http.util.CharArrayBuffer r2 = r7.lineBuf
            int r2 = r8.readLine(r2)
            r3 = -1
            if (r2 != r3) goto L_0x001b
            if (r1 == 0) goto L_0x0013
            goto L_0x001b
        L_0x0013:
            org.apache.http.NoHttpResponseException r8 = new org.apache.http.NoHttpResponseException
            java.lang.String r0 = "The target server failed to respond"
            r8.<init>(r0)
            throw r8
        L_0x001b:
            org.apache.http.message.ParserCursor r4 = new org.apache.http.message.ParserCursor
            org.apache.http.util.CharArrayBuffer r5 = r7.lineBuf
            int r5 = r5.length()
            r4.<init>(r0, r5)
            org.apache.http.message.LineParser r5 = r7.lineParser
            org.apache.http.util.CharArrayBuffer r6 = r7.lineBuf
            boolean r5 = r5.hasProtocolVersion(r6, r4)
            if (r5 == 0) goto L_0x0040
            org.apache.http.message.LineParser r8 = r7.lineParser
            org.apache.http.util.CharArrayBuffer r0 = r7.lineBuf
            org.apache.http.StatusLine r8 = r8.parseStatusLine(r0, r4)
            org.apache.http.HttpResponseFactory r0 = r7.responseFactory
            r1 = 0
            org.apache.http.HttpResponse r8 = r0.newHttpResponse(r8, r1)
            return r8
        L_0x0040:
            if (r2 == r3) goto L_0x0070
            org.apache.http.util.CharArrayBuffer r2 = r7.lineBuf
            boolean r2 = r7.reject(r2, r1)
            if (r2 != 0) goto L_0x0070
            r2 = 3
            java.lang.String r3 = "HttpClient"
            boolean r2 = android.util.Log.isLoggable(r3, r2)
            if (r2 == 0) goto L_0x006d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Garbage in response: "
            r2.append(r4)
            org.apache.http.util.CharArrayBuffer r4 = r7.lineBuf
            java.lang.String r4 = r4.toString()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r3, r2)
        L_0x006d:
            int r1 = r1 + 1
            goto L_0x0002
        L_0x0070:
            org.apache.http.ProtocolException r8 = new org.apache.http.ProtocolException
            java.lang.String r0 = "The server failed to respond with a valid HTTP response"
            r8.<init>(r0)
            goto L_0x0079
        L_0x0078:
            throw r8
        L_0x0079:
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(org.apache.http.io.SessionInputBuffer):org.apache.http.HttpResponse");
    }
}
