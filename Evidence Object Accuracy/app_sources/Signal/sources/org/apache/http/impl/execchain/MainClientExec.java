package org.apache.http.impl.execchain;

import android.util.Log;
import java.io.IOException;
import org.apache.http.ConnectionReuseStrategy;
import org.apache.http.HttpClientConnection;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthStateHC4;
import org.apache.http.client.AuthenticationStrategy;
import org.apache.http.client.UserTokenHandler;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.protocol.RequestClientConnControl;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.BasicRouteDirector;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRouteDirector;
import org.apache.http.conn.routing.RouteTracker;
import org.apache.http.impl.auth.HttpAuthenticator;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.ImmutableHttpProcessor;
import org.apache.http.protocol.RequestTargetHostHC4;
import org.apache.http.util.Args;

/* loaded from: classes3.dex */
public class MainClientExec implements ClientExecChain {
    private final HttpAuthenticator authenticator = new HttpAuthenticator();
    private final HttpClientConnectionManager connManager;
    private final ConnectionKeepAliveStrategy keepAliveStrategy;
    private final AuthenticationStrategy proxyAuthStrategy;
    private final HttpProcessor proxyHttpProcessor = new ImmutableHttpProcessor(new RequestTargetHostHC4(), new RequestClientConnControl());
    private final HttpRequestExecutor requestExecutor;
    private final ConnectionReuseStrategy reuseStrategy;
    private final HttpRouteDirector routeDirector = new BasicRouteDirector();
    private final AuthenticationStrategy targetAuthStrategy;
    private final UserTokenHandler userTokenHandler;

    public MainClientExec(HttpRequestExecutor httpRequestExecutor, HttpClientConnectionManager httpClientConnectionManager, ConnectionReuseStrategy connectionReuseStrategy, ConnectionKeepAliveStrategy connectionKeepAliveStrategy, AuthenticationStrategy authenticationStrategy, AuthenticationStrategy authenticationStrategy2, UserTokenHandler userTokenHandler) {
        Args.notNull(httpRequestExecutor, "HTTP request executor");
        Args.notNull(httpClientConnectionManager, "Client connection manager");
        Args.notNull(connectionReuseStrategy, "Connection reuse strategy");
        Args.notNull(connectionKeepAliveStrategy, "Connection keep alive strategy");
        Args.notNull(authenticationStrategy, "Target authentication strategy");
        Args.notNull(authenticationStrategy2, "Proxy authentication strategy");
        Args.notNull(userTokenHandler, "User token handler");
        this.requestExecutor = httpRequestExecutor;
        this.connManager = httpClientConnectionManager;
        this.reuseStrategy = connectionReuseStrategy;
        this.keepAliveStrategy = connectionKeepAliveStrategy;
        this.targetAuthStrategy = authenticationStrategy;
        this.proxyAuthStrategy = authenticationStrategy2;
        this.userTokenHandler = userTokenHandler;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00ee, code lost:
        if (r31.isAborted() != false) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00f6, code lost:
        throw new org.apache.http.impl.execchain.RequestAbortedException(r5);
     */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x030b A[Catch: ConnectionShutdownException -> 0x00cf, HttpException -> 0x0341, IOException -> 0x033e, RuntimeException -> 0x033b, TryCatch #3 {ConnectionShutdownException -> 0x00cf, blocks: (B:39:0x00bc, B:53:0x00d9, B:56:0x00e0, B:57:0x00e7, B:59:0x00ea, B:62:0x00f1, B:63:0x00f6, B:64:0x00f7, B:66:0x00fd, B:68:0x0104, B:70:0x010b, B:80:0x0148, B:83:0x014e, B:85:0x0154, B:86:0x015b, B:89:0x0171, B:92:0x0179, B:101:0x0193, B:104:0x019a, B:105:0x01a1, B:107:0x01a4, B:109:0x01aa, B:110:0x01c2, B:112:0x01c8, B:114:0x01ce, B:115:0x01e6, B:117:0x01ed, B:119:0x01f3, B:121:0x01f9, B:123:0x01ff, B:124:0x0217, B:125:0x021c, B:127:0x022a, B:131:0x023a, B:133:0x025c, B:135:0x0277, B:137:0x027b, B:138:0x0282, B:139:0x028b, B:141:0x02a3, B:143:0x02ad, B:145:0x02b2, B:147:0x02bd, B:149:0x02c3, B:151:0x02cd, B:153:0x02d4, B:154:0x02d9, B:155:0x02dc, B:157:0x02e2, B:159:0x02e8, B:161:0x02f2, B:163:0x02f9, B:164:0x02fe, B:165:0x0301, B:167:0x030b, B:168:0x030e, B:170:0x0314, B:171:0x0317, B:174:0x032f, B:183:0x0348, B:184:0x034b, B:186:0x0351, B:189:0x0358, B:191:0x035e), top: B:216:0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0314 A[Catch: ConnectionShutdownException -> 0x00cf, HttpException -> 0x0341, IOException -> 0x033e, RuntimeException -> 0x033b, TryCatch #3 {ConnectionShutdownException -> 0x00cf, blocks: (B:39:0x00bc, B:53:0x00d9, B:56:0x00e0, B:57:0x00e7, B:59:0x00ea, B:62:0x00f1, B:63:0x00f6, B:64:0x00f7, B:66:0x00fd, B:68:0x0104, B:70:0x010b, B:80:0x0148, B:83:0x014e, B:85:0x0154, B:86:0x015b, B:89:0x0171, B:92:0x0179, B:101:0x0193, B:104:0x019a, B:105:0x01a1, B:107:0x01a4, B:109:0x01aa, B:110:0x01c2, B:112:0x01c8, B:114:0x01ce, B:115:0x01e6, B:117:0x01ed, B:119:0x01f3, B:121:0x01f9, B:123:0x01ff, B:124:0x0217, B:125:0x021c, B:127:0x022a, B:131:0x023a, B:133:0x025c, B:135:0x0277, B:137:0x027b, B:138:0x0282, B:139:0x028b, B:141:0x02a3, B:143:0x02ad, B:145:0x02b2, B:147:0x02bd, B:149:0x02c3, B:151:0x02cd, B:153:0x02d4, B:154:0x02d9, B:155:0x02dc, B:157:0x02e2, B:159:0x02e8, B:161:0x02f2, B:163:0x02f9, B:164:0x02fe, B:165:0x0301, B:167:0x030b, B:168:0x030e, B:170:0x0314, B:171:0x0317, B:174:0x032f, B:183:0x0348, B:184:0x034b, B:186:0x0351, B:189:0x0358, B:191:0x035e), top: B:216:0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x0317 A[SYNTHETIC] */
    @Override // org.apache.http.impl.execchain.ClientExecChain
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.http.client.methods.CloseableHttpResponse execute(org.apache.http.conn.routing.HttpRoute r28, org.apache.http.client.methods.HttpRequestWrapper r29, org.apache.http.client.protocol.HttpClientContext r30, org.apache.http.client.methods.HttpExecutionAware r31) throws java.io.IOException, org.apache.http.HttpException {
        /*
        // Method dump skipped, instructions count: 949
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.execchain.MainClientExec.execute(org.apache.http.conn.routing.HttpRoute, org.apache.http.client.methods.HttpRequestWrapper, org.apache.http.client.protocol.HttpClientContext, org.apache.http.client.methods.HttpExecutionAware):org.apache.http.client.methods.CloseableHttpResponse");
    }

    void establishRoute(AuthStateHC4 authStateHC4, HttpClientConnection httpClientConnection, HttpRoute httpRoute, HttpRequest httpRequest, HttpClientContext httpClientContext) throws HttpException, IOException {
        int nextStep;
        int connectTimeout = httpClientContext.getRequestConfig().getConnectTimeout();
        RouteTracker routeTracker = new RouteTracker(httpRoute);
        do {
            HttpRoute route = routeTracker.toRoute();
            nextStep = this.routeDirector.nextStep(httpRoute, route);
            int i = 0;
            switch (nextStep) {
                case -1:
                    throw new HttpException("Unable to establish route: planned = " + httpRoute + "; current = " + route);
                case 0:
                    this.connManager.routeComplete(httpClientConnection, httpRoute, httpClientContext);
                    continue;
                case 1:
                    HttpClientConnectionManager httpClientConnectionManager = this.connManager;
                    if (connectTimeout > 0) {
                        i = connectTimeout;
                    }
                    httpClientConnectionManager.connect(httpClientConnection, httpRoute, i, httpClientContext);
                    routeTracker.connectTarget(httpRoute.isSecure());
                    continue;
                case 2:
                    this.connManager.connect(httpClientConnection, httpRoute, connectTimeout > 0 ? connectTimeout : 0, httpClientContext);
                    routeTracker.connectProxy(httpRoute.getProxyHost(), false);
                    continue;
                case 3:
                    boolean createTunnelToTarget = createTunnelToTarget(authStateHC4, httpClientConnection, httpRoute, httpRequest, httpClientContext);
                    if (Log.isLoggable("HttpClient", 3)) {
                        Log.d("HttpClient", "Tunnel to target created.");
                    }
                    routeTracker.tunnelTarget(createTunnelToTarget);
                    continue;
                case 4:
                    int hopCount = route.getHopCount() - 1;
                    boolean createTunnelToProxy = createTunnelToProxy(httpRoute, hopCount, httpClientContext);
                    if (Log.isLoggable("HttpClient", 3)) {
                        Log.d("HttpClient", "Tunnel to proxy created.");
                    }
                    routeTracker.tunnelProxy(httpRoute.getHopTarget(hopCount), createTunnelToProxy);
                    continue;
                case 5:
                    this.connManager.upgrade(httpClientConnection, httpRoute, httpClientContext);
                    routeTracker.layerProtocol(httpRoute.isSecure());
                    continue;
                default:
                    throw new IllegalStateException("Unknown step indicator " + nextStep + " from RouteDirector.");
            }
        } while (nextStep > 0);
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Throwable, org.apache.http.impl.execchain.TunnelRefusedException] */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0093, code lost:
        if (r16.reuseStrategy.keepAlive(r7, r21) == false) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009c, code lost:
        if (android.util.Log.isLoggable("HttpClient", 3) == false) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009e, code lost:
        android.util.Log.d("HttpClient", "Connection kept alive");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a3, code lost:
        org.apache.http.util.EntityUtilsHC4.consume(r7.getEntity());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ab, code lost:
        r18.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean createTunnelToTarget(org.apache.http.auth.AuthStateHC4 r17, org.apache.http.HttpClientConnection r18, org.apache.http.conn.routing.HttpRoute r19, org.apache.http.HttpRequest r20, org.apache.http.client.protocol.HttpClientContext r21) throws org.apache.http.HttpException, java.io.IOException {
        /*
        // Method dump skipped, instructions count: 268
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.execchain.MainClientExec.createTunnelToTarget(org.apache.http.auth.AuthStateHC4, org.apache.http.HttpClientConnection, org.apache.http.conn.routing.HttpRoute, org.apache.http.HttpRequest, org.apache.http.client.protocol.HttpClientContext):boolean");
    }

    private boolean createTunnelToProxy(HttpRoute httpRoute, int i, HttpClientContext httpClientContext) throws HttpException {
        throw new HttpException("Proxy chains are not supported.");
    }

    private boolean needAuthentication(AuthStateHC4 authStateHC4, AuthStateHC4 authStateHC42, HttpRoute httpRoute, HttpResponse httpResponse, HttpClientContext httpClientContext) {
        if (!httpClientContext.getRequestConfig().isAuthenticationEnabled()) {
            return false;
        }
        HttpHost targetHost = httpClientContext.getTargetHost();
        if (targetHost == null) {
            targetHost = httpRoute.getTargetHost();
        }
        if (targetHost.getPort() < 0) {
            targetHost = new HttpHost(targetHost.getHostName(), httpRoute.getTargetHost().getPort(), targetHost.getSchemeName());
        }
        boolean isAuthenticationRequested = this.authenticator.isAuthenticationRequested(targetHost, httpResponse, this.targetAuthStrategy, authStateHC4, httpClientContext);
        HttpHost proxyHost = httpRoute.getProxyHost();
        if (proxyHost == null) {
            proxyHost = httpRoute.getTargetHost();
        }
        boolean isAuthenticationRequested2 = this.authenticator.isAuthenticationRequested(proxyHost, httpResponse, this.proxyAuthStrategy, authStateHC42, httpClientContext);
        if (isAuthenticationRequested) {
            return this.authenticator.handleAuthChallenge(targetHost, httpResponse, this.targetAuthStrategy, authStateHC4, httpClientContext);
        }
        if (isAuthenticationRequested2) {
            return this.authenticator.handleAuthChallenge(proxyHost, httpResponse, this.proxyAuthStrategy, authStateHC42, httpClientContext);
        }
        return false;
    }
}
