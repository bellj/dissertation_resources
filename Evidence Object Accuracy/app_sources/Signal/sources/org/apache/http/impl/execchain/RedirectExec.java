package org.apache.http.impl.execchain;

import android.util.Log;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthStateHC4;
import org.apache.http.client.RedirectException;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpExecutionAware;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIUtilsHC4;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.util.Args;
import org.apache.http.util.EntityUtilsHC4;

/* loaded from: classes3.dex */
public class RedirectExec implements ClientExecChain {
    private final RedirectStrategy redirectStrategy;
    private final ClientExecChain requestExecutor;
    private final HttpRoutePlanner routePlanner;

    public RedirectExec(ClientExecChain clientExecChain, HttpRoutePlanner httpRoutePlanner, RedirectStrategy redirectStrategy) {
        Args.notNull(clientExecChain, "HTTP client request executor");
        Args.notNull(httpRoutePlanner, "HTTP route planner");
        Args.notNull(redirectStrategy, "HTTP redirect strategy");
        this.requestExecutor = clientExecChain;
        this.routePlanner = httpRoutePlanner;
        this.redirectStrategy = redirectStrategy;
    }

    /* JADX INFO: finally extract failed */
    @Override // org.apache.http.impl.execchain.ClientExecChain
    public CloseableHttpResponse execute(HttpRoute httpRoute, HttpRequestWrapper httpRequestWrapper, HttpClientContext httpClientContext, HttpExecutionAware httpExecutionAware) throws IOException, HttpException {
        HttpResponse execute;
        AuthScheme authScheme;
        Args.notNull(httpRoute, "HTTP route");
        Args.notNull(httpRequestWrapper, "HTTP request");
        Args.notNull(httpClientContext, "HTTP context");
        List<URI> redirectLocations = httpClientContext.getRedirectLocations();
        if (redirectLocations != null) {
            redirectLocations.clear();
        }
        RequestConfig requestConfig = httpClientContext.getRequestConfig();
        int maxRedirects = requestConfig.getMaxRedirects() > 0 ? requestConfig.getMaxRedirects() : 50;
        int i = 0;
        HttpEntityEnclosingRequest httpEntityEnclosingRequest = httpRequestWrapper;
        while (true) {
            execute = this.requestExecutor.execute(httpRoute, httpEntityEnclosingRequest, httpClientContext, httpExecutionAware);
            try {
                if (!requestConfig.isRedirectsEnabled() || !this.redirectStrategy.isRedirected(httpEntityEnclosingRequest, execute, httpClientContext)) {
                    break;
                } else if (i < maxRedirects) {
                    i++;
                    HttpUriRequest redirect = this.redirectStrategy.getRedirect(httpEntityEnclosingRequest, execute, httpClientContext);
                    if (!redirect.headerIterator().hasNext()) {
                        redirect.setHeaders(httpRequestWrapper.getOriginal().getAllHeaders());
                    }
                    httpEntityEnclosingRequest = HttpRequestWrapper.wrap(redirect);
                    if (httpEntityEnclosingRequest instanceof HttpEntityEnclosingRequest) {
                        RequestEntityProxy.enhance(httpEntityEnclosingRequest);
                    }
                    URI uri = httpEntityEnclosingRequest.getURI();
                    HttpHost extractHost = URIUtilsHC4.extractHost(uri);
                    if (extractHost != null) {
                        if (!httpRoute.getTargetHost().equals(extractHost)) {
                            AuthStateHC4 targetAuthState = httpClientContext.getTargetAuthState();
                            if (targetAuthState != null) {
                                if (Log.isLoggable("HttpClient", 3)) {
                                    Log.d("HttpClient", "Resetting target auth state");
                                }
                                targetAuthState.reset();
                            }
                            AuthStateHC4 proxyAuthState = httpClientContext.getProxyAuthState();
                            if (!(proxyAuthState == null || (authScheme = proxyAuthState.getAuthScheme()) == null || !authScheme.isConnectionBased())) {
                                if (Log.isLoggable("HttpClient", 3)) {
                                    Log.d("HttpClient", "Resetting proxy auth state");
                                }
                                proxyAuthState.reset();
                            }
                        }
                        httpRoute = this.routePlanner.determineRoute(extractHost, httpEntityEnclosingRequest, httpClientContext);
                        if (Log.isLoggable("HttpClient", 3)) {
                            Log.d("HttpClient", "Redirecting to '" + uri + "' via " + httpRoute);
                        }
                        EntityUtilsHC4.consume(execute.getEntity());
                        execute.close();
                    } else {
                        throw new ProtocolException("Redirect URI does not specify a valid host name: " + uri);
                    }
                } else {
                    throw new RedirectException("Maximum redirects (" + maxRedirects + ") exceeded");
                }
            } catch (IOException e) {
                execute.close();
                throw e;
            } catch (HttpException e2) {
                try {
                    try {
                        EntityUtilsHC4.consume(execute.getEntity());
                    } catch (IOException e3) {
                        if (Log.isLoggable("HttpClient", 3)) {
                            Log.d("HttpClient", "I/O error while releasing connection", e3);
                        }
                        execute.close();
                        throw e2;
                    }
                    execute.close();
                    throw e2;
                } catch (Throwable th) {
                    execute.close();
                    throw th;
                }
            } catch (RuntimeException e4) {
                execute.close();
                throw e4;
            }
        }
        return execute;
    }
}
