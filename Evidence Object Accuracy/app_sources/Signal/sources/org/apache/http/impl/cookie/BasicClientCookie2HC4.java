package org.apache.http.impl.cookie;

import java.util.Date;
import org.apache.http.cookie.SetCookie2;

/* loaded from: classes3.dex */
public class BasicClientCookie2HC4 extends BasicClientCookieHC4 implements SetCookie2 {
    private String commentURL;
    private boolean discard;
    private int[] ports;

    public BasicClientCookie2HC4(String str, String str2) {
        super(str, str2);
    }

    @Override // org.apache.http.impl.cookie.BasicClientCookieHC4
    public int[] getPorts() {
        return this.ports;
    }

    public void setPorts(int[] iArr) {
        this.ports = iArr;
    }

    @Override // org.apache.http.impl.cookie.BasicClientCookieHC4
    public String getCommentURL() {
        return this.commentURL;
    }

    public void setCommentURL(String str) {
        this.commentURL = str;
    }

    public void setDiscard(boolean z) {
        this.discard = z;
    }

    @Override // org.apache.http.impl.cookie.BasicClientCookieHC4
    public boolean isPersistent() {
        return !this.discard && super.isPersistent();
    }

    @Override // org.apache.http.impl.cookie.BasicClientCookieHC4
    public boolean isExpired(Date date) {
        return this.discard || super.isExpired(date);
    }

    @Override // org.apache.http.impl.cookie.BasicClientCookieHC4, java.lang.Object
    public Object clone() throws CloneNotSupportedException {
        BasicClientCookie2HC4 basicClientCookie2HC4 = (BasicClientCookie2HC4) super.clone();
        int[] iArr = this.ports;
        if (iArr != null) {
            basicClientCookie2HC4.ports = (int[]) iArr.clone();
        }
        return basicClientCookie2HC4;
    }
}
