package org.apache.http.io;

/* loaded from: classes3.dex */
public interface BufferInfo {
    int length();
}
