package org.apache.http.protocol;

import j$.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import org.apache.http.util.Args;

/* loaded from: classes3.dex */
public class BasicHttpContextHC4 implements HttpContext {
    private final Map<String, Object> map;
    private final HttpContext parentContext;

    public BasicHttpContextHC4() {
        this(null);
    }

    public BasicHttpContextHC4(HttpContext httpContext) {
        this.map = new ConcurrentHashMap();
        this.parentContext = httpContext;
    }

    public Object getAttribute(String str) {
        HttpContext httpContext;
        Args.notNull(str, "Id");
        Object obj = this.map.get(str);
        return (obj != null || (httpContext = this.parentContext) == null) ? obj : httpContext.getAttribute(str);
    }

    public void setAttribute(String str, Object obj) {
        Args.notNull(str, "Id");
        if (obj != null) {
            this.map.put(str, obj);
        } else {
            this.map.remove(str);
        }
    }

    public Object removeAttribute(String str) {
        Args.notNull(str, "Id");
        return this.map.remove(str);
    }

    public String toString() {
        return this.map.toString();
    }
}
