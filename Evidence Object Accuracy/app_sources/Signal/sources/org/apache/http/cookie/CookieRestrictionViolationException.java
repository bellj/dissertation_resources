package org.apache.http.cookie;

/* loaded from: classes3.dex */
public class CookieRestrictionViolationException extends MalformedCookieException {
    public CookieRestrictionViolationException(String str) {
        super(str);
    }
}
