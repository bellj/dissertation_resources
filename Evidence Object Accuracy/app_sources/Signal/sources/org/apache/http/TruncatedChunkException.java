package org.apache.http;

/* loaded from: classes3.dex */
public class TruncatedChunkException extends MalformedChunkCodingException {
    public TruncatedChunkException(String str) {
        super(str);
    }
}
