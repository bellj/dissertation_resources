package org.apache.http;

import java.nio.charset.Charset;

/* loaded from: classes3.dex */
public final class Consts {
    public static final Charset ASCII = Charset.forName("US-ASCII");
    public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName("UTF-8");
}
