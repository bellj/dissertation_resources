package org.apache.http;

import java.io.IOException;

/* loaded from: classes3.dex */
public class MessageConstraintException extends IOException {
    public MessageConstraintException(String str) {
        super(str);
    }
}
