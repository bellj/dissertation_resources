package org.apache.http.util;

import java.util.Map;
import org.thoughtcrime.securesms.database.MentionUtil;

/* loaded from: classes3.dex */
public class VersionInfoHC4 {
    private final String infoClassloader;
    private final String infoModule;
    private final String infoPackage;
    private final String infoRelease;
    private final String infoTimestamp;

    protected VersionInfoHC4(String str, String str2, String str3, String str4, String str5) {
        Args.notNull(str, "Package identifier");
        this.infoPackage = str;
        this.infoModule = str2 == null ? "UNAVAILABLE" : str2;
        this.infoRelease = str3 == null ? "UNAVAILABLE" : str3;
        this.infoTimestamp = str4 == null ? "UNAVAILABLE" : str4;
        this.infoClassloader = str5 == null ? "UNAVAILABLE" : str5;
    }

    public final String getRelease() {
        return this.infoRelease;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.infoPackage.length() + 20 + this.infoModule.length() + this.infoRelease.length() + this.infoTimestamp.length() + this.infoClassloader.length());
        sb.append("VersionInfo(");
        sb.append(this.infoPackage);
        sb.append(':');
        sb.append(this.infoModule);
        if (!"UNAVAILABLE".equals(this.infoRelease)) {
            sb.append(':');
            sb.append(this.infoRelease);
        }
        if (!"UNAVAILABLE".equals(this.infoTimestamp)) {
            sb.append(':');
            sb.append(this.infoTimestamp);
        }
        sb.append(')');
        if (!"UNAVAILABLE".equals(this.infoClassloader)) {
            sb.append(MentionUtil.MENTION_STARTER);
            sb.append(this.infoClassloader);
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.http.util.VersionInfoHC4 loadVersionInfo(java.lang.String r4, java.lang.ClassLoader r5) {
        /*
            java.lang.String r0 = "Package identifier"
            org.apache.http.util.Args.notNull(r4, r0)
            if (r5 == 0) goto L_0x0008
            goto L_0x0010
        L_0x0008:
            java.lang.Thread r5 = java.lang.Thread.currentThread()
            java.lang.ClassLoader r5 = r5.getContextClassLoader()
        L_0x0010:
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: IOException -> 0x004a
            r1.<init>()     // Catch: IOException -> 0x004a
            r2 = 46
            r3 = 47
            java.lang.String r2 = r4.replace(r2, r3)     // Catch: IOException -> 0x004a
            r1.append(r2)     // Catch: IOException -> 0x004a
            java.lang.String r2 = "/"
            r1.append(r2)     // Catch: IOException -> 0x004a
            java.lang.String r2 = "version.properties"
            r1.append(r2)     // Catch: IOException -> 0x004a
            java.lang.String r1 = r1.toString()     // Catch: IOException -> 0x004a
            java.io.InputStream r1 = r5.getResourceAsStream(r1)     // Catch: IOException -> 0x004a
            if (r1 == 0) goto L_0x0048
            java.util.Properties r2 = new java.util.Properties     // Catch: all -> 0x0043
            r2.<init>()     // Catch: all -> 0x0043
            r2.load(r1)     // Catch: all -> 0x0043
            r1.close()     // Catch: IOException -> 0x0041
            goto L_0x004c
        L_0x0041:
            goto L_0x004c
        L_0x0043:
            r2 = move-exception
            r1.close()     // Catch: IOException -> 0x004a
            throw r2     // Catch: IOException -> 0x004a
        L_0x0048:
            r2 = r0
            goto L_0x004c
        L_0x004a:
            goto L_0x0048
        L_0x004c:
            if (r2 == 0) goto L_0x0052
            org.apache.http.util.VersionInfoHC4 r0 = fromMap(r4, r2, r5)
        L_0x0052:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.util.VersionInfoHC4.loadVersionInfo(java.lang.String, java.lang.ClassLoader):org.apache.http.util.VersionInfoHC4");
    }

    protected static VersionInfoHC4 fromMap(String str, Map<?, ?> map, ClassLoader classLoader) {
        String str2;
        String str3;
        String str4;
        Args.notNull(str, "Package identifier");
        String str5 = null;
        if (map != null) {
            String str6 = (String) map.get("info.module");
            if (str6 != null && str6.length() < 1) {
                str6 = null;
            }
            String str7 = (String) map.get("info.release");
            if (str7 != null && (str7.length() < 1 || str7.equals("${pom.version}"))) {
                str7 = null;
            }
            String str8 = (String) map.get("info.timestamp");
            str2 = (str8 == null || (str8.length() >= 1 && !str8.equals("${mvn.timestamp}"))) ? str8 : null;
            str4 = str6;
            str3 = str7;
        } else {
            str4 = null;
            str3 = null;
            str2 = null;
        }
        if (classLoader != null) {
            str5 = classLoader.toString();
        }
        return new VersionInfoHC4(str, str4, str3, str2, str5);
    }
}
