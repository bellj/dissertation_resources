package org.whispersystems.util;

/* loaded from: classes5.dex */
public final class FlagUtil {
    public static int toBinaryFlag(int i) {
        return 1 << (i - 1);
    }

    private FlagUtil() {
    }
}
