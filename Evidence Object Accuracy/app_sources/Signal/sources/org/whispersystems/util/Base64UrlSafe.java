package org.whispersystems.util;

import java.io.IOException;

/* loaded from: classes5.dex */
public final class Base64UrlSafe {
    private Base64UrlSafe() {
    }

    public static byte[] decode(String str) throws IOException {
        return Base64.decode(str, 16);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000d, code lost:
        if (r0 != 3) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] decodePaddingAgnostic(java.lang.String r2) throws java.io.IOException {
        /*
            int r0 = r2.length()
            int r0 = r0 % 4
            r1 = 1
            if (r0 == r1) goto L_0x0022
            r1 = 2
            if (r0 == r1) goto L_0x0010
            r1 = 3
            if (r0 == r1) goto L_0x0022
            goto L_0x0033
        L_0x0010:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r2)
            java.lang.String r2 = "=="
            r0.append(r2)
            java.lang.String r2 = r0.toString()
            goto L_0x0033
        L_0x0022:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r2)
            java.lang.String r2 = "="
            r0.append(r2)
            java.lang.String r2 = r0.toString()
        L_0x0033:
            byte[] r2 = decode(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.util.Base64UrlSafe.decodePaddingAgnostic(java.lang.String):byte[]");
    }

    public static String encodeBytes(byte[] bArr) {
        try {
            return Base64.encodeBytes(bArr, 16);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static String encodeBytesWithoutPadding(byte[] bArr) {
        return encodeBytes(bArr).replace("=", "");
    }
}
