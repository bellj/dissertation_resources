package org.whispersystems.util;

import java.nio.charset.StandardCharsets;

/* loaded from: classes5.dex */
public final class StringUtil {
    private StringUtil() {
    }

    public static byte[] utf8(String str) {
        return str.getBytes(StandardCharsets.UTF_8);
    }
}
