package org.whispersystems.util;

/* loaded from: classes5.dex */
public final class ByteArrayUtil {
    private ByteArrayUtil() {
    }

    public static byte[] xor(byte[] bArr, byte[] bArr2) {
        if (bArr.length == bArr2.length) {
            byte[] bArr3 = new byte[bArr.length];
            for (int length = bArr.length - 1; length >= 0; length--) {
                bArr3[length] = (byte) (bArr[length] ^ bArr2[length]);
            }
            return bArr3;
        }
        throw new AssertionError("XOR length mismatch");
    }

    public static byte[] concat(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[bArr.length + bArr2.length];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }
}
