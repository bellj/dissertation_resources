package org.whispersystems.signalservice.api.messages.multidevice;

import java.util.List;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class BlockedListMessage {
    private final List<SignalServiceAddress> addresses;
    private final List<byte[]> groupIds;

    public BlockedListMessage(List<SignalServiceAddress> list, List<byte[]> list2) {
        this.addresses = list;
        this.groupIds = list2;
    }

    public List<SignalServiceAddress> getAddresses() {
        return this.addresses;
    }

    public List<byte[]> getGroupIds() {
        return this.groupIds;
    }
}
