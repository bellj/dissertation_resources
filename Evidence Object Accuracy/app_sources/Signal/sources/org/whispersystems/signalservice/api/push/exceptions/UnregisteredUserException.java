package org.whispersystems.signalservice.api.push.exceptions;

import java.io.IOException;

/* loaded from: classes5.dex */
public class UnregisteredUserException extends IOException {
    private final String e164number;

    public UnregisteredUserException(String str, Exception exc) {
        super(exc);
        this.e164number = str;
    }

    public String getE164Number() {
        return this.e164number;
    }
}
