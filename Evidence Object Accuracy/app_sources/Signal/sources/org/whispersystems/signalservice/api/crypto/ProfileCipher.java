package org.whispersystems.signalservice.api.crypto;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class ProfileCipher {
    private static final int ABOUT_PADDED_LENGTH_1;
    private static final int ABOUT_PADDED_LENGTH_2;
    private static final int ABOUT_PADDED_LENGTH_3;
    public static final int EMOJI_PADDED_LENGTH;
    public static final int ENCRYPTION_OVERHEAD;
    public static final int MAX_POSSIBLE_ABOUT_LENGTH;
    public static final int MAX_POSSIBLE_NAME_LENGTH;
    private static final int NAME_PADDED_LENGTH_1;
    private static final int NAME_PADDED_LENGTH_2;
    public static final int PAYMENTS_ADDRESS_BASE64_FIELD_SIZE;
    public static final int PAYMENTS_ADDRESS_CONTENT_SIZE;
    private final ProfileKey key;

    public ProfileCipher(ProfileKey profileKey) {
        this.key = profileKey;
    }

    public byte[] encrypt(byte[] bArr, int i) {
        try {
            byte[] bArr2 = new byte[i];
            if (bArr.length <= i) {
                System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                byte[] secretBytes = Util.getSecretBytes(12);
                Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                instance.init(1, new SecretKeySpec(this.key.serialize(), "AES"), new GCMParameterSpec(128, secretBytes));
                byte[] combine = ByteUtil.combine(secretBytes, instance.doFinal(bArr2));
                if (combine.length == i + 28) {
                    return combine;
                }
                throw new AssertionError(String.format(Locale.US, "Wrong output length %d != padded length %d + %d", Integer.valueOf(combine.length), Integer.valueOf(i), 28));
            }
            throw new IllegalArgumentException("Input is too long: " + new String(bArr));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] decrypt(byte[] bArr) throws InvalidCiphertextException {
        Object e;
        Exception e2;
        try {
            if (bArr.length >= 29) {
                byte[] bArr2 = new byte[12];
                System.arraycopy(bArr, 0, bArr2, 0, 12);
                Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                instance.init(2, new SecretKeySpec(this.key.serialize(), "AES"), new GCMParameterSpec(128, bArr2));
                return instance.doFinal(bArr, 12, bArr.length - 12);
            }
            throw new InvalidCiphertextException("Too short: " + bArr.length);
        } catch (InvalidAlgorithmParameterException e3) {
            e = e3;
            throw new AssertionError(e);
        } catch (InvalidKeyException e4) {
            e2 = e4;
            throw new InvalidCiphertextException(e2);
        } catch (NoSuchAlgorithmException e5) {
            e = e5;
            throw new AssertionError(e);
        } catch (BadPaddingException e6) {
            e2 = e6;
            throw new InvalidCiphertextException(e2);
        } catch (IllegalBlockSizeException e7) {
            e = e7;
            throw new AssertionError(e);
        } catch (NoSuchPaddingException e8) {
            e = e8;
            throw new AssertionError(e);
        }
    }

    public byte[] encryptString(String str, int i) {
        return encrypt(str.getBytes(StandardCharsets.UTF_8), i);
    }

    public String decryptString(byte[] bArr) throws InvalidCiphertextException {
        int i;
        byte[] decrypt = decrypt(bArr);
        int length = decrypt.length;
        while (true) {
            length--;
            if (length < 0) {
                i = 0;
                break;
            } else if (decrypt[length] != 0) {
                i = length + 1;
                break;
            }
        }
        byte[] bArr2 = new byte[i];
        System.arraycopy(decrypt, 0, bArr2, 0, i);
        return new String(bArr2);
    }

    public byte[] encryptWithLength(byte[] bArr, int i) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[bArr.length + 4]);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        wrap.putInt(bArr.length);
        wrap.put(bArr);
        return encrypt(wrap.array(), i);
    }

    public byte[] decryptWithLength(byte[] bArr) throws InvalidCiphertextException, IOException {
        byte[] decrypt = decrypt(bArr);
        int length = decrypt.length - 4;
        ByteBuffer wrap = ByteBuffer.wrap(decrypt);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        int i = wrap.getInt();
        if (i > length) {
            throw new IOException("Encoded length exceeds content length");
        } else if (i >= 0) {
            byte[] bArr2 = new byte[i];
            wrap.get(bArr2);
            return bArr2;
        } else {
            throw new IOException("Encoded length is less than 0");
        }
    }

    public boolean verifyUnidentifiedAccess(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        try {
            if (bArr.length == 0) {
                return false;
            }
            byte[] deriveAccessKeyFrom = UnidentifiedAccess.deriveAccessKeyFrom(this.key);
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(deriveAccessKeyFrom, "HmacSHA256"));
            return MessageDigest.isEqual(bArr, instance.doFinal(new byte[32]));
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static int getTargetNameLength(String str) {
        return str.getBytes(StandardCharsets.UTF_8).length <= 53 ? 53 : 257;
    }

    public static int getTargetAboutLength(String str) {
        int length = str.getBytes(StandardCharsets.UTF_8).length;
        if (length <= 128) {
            return 128;
        }
        if (length < ABOUT_PADDED_LENGTH_2) {
            return ABOUT_PADDED_LENGTH_2;
        }
        return 512;
    }
}
