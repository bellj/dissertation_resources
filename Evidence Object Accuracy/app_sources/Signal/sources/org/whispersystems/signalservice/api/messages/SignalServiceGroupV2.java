package org.whispersystems.signalservice.api.messages;

import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public final class SignalServiceGroupV2 {
    private final GroupMasterKey masterKey;
    private final int revision;
    private final byte[] signedGroupChange;

    private SignalServiceGroupV2(Builder builder) {
        this.masterKey = builder.masterKey;
        this.revision = builder.revision;
        this.signedGroupChange = builder.signedGroupChange != null ? (byte[]) builder.signedGroupChange.clone() : null;
    }

    public static SignalServiceGroupV2 fromProtobuf(SignalServiceProtos.GroupContextV2 groupContextV2) {
        try {
            Builder newBuilder = newBuilder(new GroupMasterKey(groupContextV2.getMasterKey().toByteArray()));
            if (groupContextV2.hasGroupChange() && !groupContextV2.getGroupChange().isEmpty()) {
                newBuilder.withSignedGroupChange(groupContextV2.getGroupChange().toByteArray());
            }
            return newBuilder.withRevision(groupContextV2.getRevision()).build();
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public GroupMasterKey getMasterKey() {
        return this.masterKey;
    }

    public int getRevision() {
        return this.revision;
    }

    public byte[] getSignedGroupChange() {
        return this.signedGroupChange;
    }

    public boolean hasSignedGroupChange() {
        byte[] bArr = this.signedGroupChange;
        return bArr != null && bArr.length > 0;
    }

    public static Builder newBuilder(GroupMasterKey groupMasterKey) {
        return new Builder(groupMasterKey);
    }

    /* loaded from: classes5.dex */
    public static class Builder {
        private final GroupMasterKey masterKey;
        private int revision;
        private byte[] signedGroupChange;

        private Builder(GroupMasterKey groupMasterKey) {
            if (groupMasterKey != null) {
                this.masterKey = groupMasterKey;
                return;
            }
            throw new IllegalArgumentException();
        }

        public Builder withRevision(int i) {
            this.revision = i;
            return this;
        }

        public Builder withSignedGroupChange(byte[] bArr) {
            this.signedGroupChange = bArr;
            return this;
        }

        public SignalServiceGroupV2 build() {
            return new SignalServiceGroupV2(this);
        }
    }
}
