package org.whispersystems.signalservice.api.crypto;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* loaded from: classes5.dex */
public final class CryptoUtil {
    private static final String HMAC_SHA256;

    private CryptoUtil() {
    }

    public static byte[] hmacSha256(byte[] bArr, byte[] bArr2) {
        try {
            Mac instance = Mac.getInstance(HMAC_SHA256);
            instance.init(new SecretKeySpec(bArr, HMAC_SHA256));
            return instance.doFinal(bArr2);
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] sha256(byte[] bArr) {
        try {
            return MessageDigest.getInstance("SHA-256").digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
