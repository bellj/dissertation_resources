package org.whispersystems.signalservice.api.messages;

import java.util.List;

/* loaded from: classes5.dex */
public class SignalServiceReceiptMessage {
    private final List<Long> timestamps;
    private final Type type;
    private final long when;

    /* loaded from: classes5.dex */
    public enum Type {
        UNKNOWN,
        DELIVERY,
        READ,
        VIEWED
    }

    public SignalServiceReceiptMessage(Type type, List<Long> list, long j) {
        this.type = type;
        this.timestamps = list;
        this.when = j;
    }

    public Type getType() {
        return this.type;
    }

    public List<Long> getTimestamps() {
        return this.timestamps;
    }

    public long getWhen() {
        return this.when;
    }

    public boolean isDeliveryReceipt() {
        return this.type == Type.DELIVERY;
    }

    public boolean isReadReceipt() {
        return this.type == Type.READ;
    }

    public boolean isViewedReceipt() {
        return this.type == Type.VIEWED;
    }
}
