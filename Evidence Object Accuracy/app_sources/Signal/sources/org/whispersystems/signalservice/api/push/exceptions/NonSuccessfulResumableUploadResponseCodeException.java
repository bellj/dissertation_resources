package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class NonSuccessfulResumableUploadResponseCodeException extends NonSuccessfulResponseCodeException {
    public NonSuccessfulResumableUploadResponseCodeException(int i, String str) {
        super(i, str);
    }
}
