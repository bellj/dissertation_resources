package org.whispersystems.signalservice.api.push;

import com.google.protobuf.ByteString;
import java.util.UUID;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public final class ACI extends ServiceId {
    public static ACI from(UUID uuid) {
        return new ACI(uuid);
    }

    public static ACI from(ServiceId serviceId) {
        return new ACI(serviceId.uuid());
    }

    public static ACI fromNullable(ServiceId serviceId) {
        if (serviceId != null) {
            return new ACI(serviceId.uuid());
        }
        return null;
    }

    public static ACI parseOrThrow(String str) {
        return from(UUID.fromString(str));
    }

    public static ACI parseOrNull(String str) {
        UUID parseOrNull = UuidUtil.parseOrNull(str);
        if (parseOrNull != null) {
            return from(parseOrNull);
        }
        return null;
    }

    private ACI(UUID uuid) {
        super(uuid);
    }

    @Override // org.whispersystems.signalservice.api.push.ServiceId
    public ByteString toByteString() {
        return UuidUtil.toByteString(this.uuid);
    }

    @Override // org.whispersystems.signalservice.api.push.ServiceId
    public byte[] toByteArray() {
        return UuidUtil.toByteArray(this.uuid);
    }
}
