package org.whispersystems.signalservice.api.services;

import com.google.protobuf.ByteString;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.signal.cdsi.proto.ClientRequest;
import org.signal.cdsi.proto.ClientResponse;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.services.CdsiV2Service;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;

/* loaded from: classes5.dex */
public final class CdsiV2Service {
    private static final UUID EMPTY_UUID = new UUID(0, 0);
    private static final int RESPONSE_ITEM_SIZE;
    private static final String TAG;
    private final CdsiSocket cdshSocket;

    public CdsiV2Service(SignalServiceConfiguration signalServiceConfiguration, String str) {
        this.cdshSocket = new CdsiSocket(signalServiceConfiguration, str);
    }

    public Single<ServiceResponse<Response>> getRegisteredUsers(String str, String str2, Request request, Consumer<byte[]> consumer) {
        return this.cdshSocket.connect(str, str2, buildClientRequest(request), consumer).map(new Function() { // from class: org.whispersystems.signalservice.api.services.CdsiV2Service$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return CdsiV2Service.parseEntries((ClientResponse) obj);
            }
        }).collect(Collectors.toList()).flatMap(new Function() { // from class: org.whispersystems.signalservice.api.services.CdsiV2Service$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return CdsiV2Service.lambda$getRegisteredUsers$0((List) obj);
            }
        }).map(new Function() { // from class: org.whispersystems.signalservice.api.services.CdsiV2Service$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ServiceResponse.forResult((CdsiV2Service.Response) obj, 200, null);
            }
        }).onErrorReturn(new Function() { // from class: org.whispersystems.signalservice.api.services.CdsiV2Service$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return CdsiV2Service.lambda$getRegisteredUsers$2((Throwable) obj);
            }
        });
    }

    public static /* synthetic */ SingleSource lambda$getRegisteredUsers$0(List list) throws Throwable {
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            hashMap.putAll(((Response) it.next()).getResults());
        }
        return Single.just(new Response(hashMap));
    }

    public static /* synthetic */ ServiceResponse lambda$getRegisteredUsers$2(Throwable th) throws Throwable {
        if (th instanceof NonSuccessfulResponseCodeException) {
            return ServiceResponse.forApplicationError(th, ((NonSuccessfulResponseCodeException) th).getCode(), null);
        }
        return ServiceResponse.forUnknownError(th);
    }

    public static Response parseEntries(ClientResponse clientResponse) {
        HashMap hashMap = new HashMap();
        ByteBuffer asReadOnlyByteBuffer = clientResponse.getE164PniAciTriples().asReadOnlyByteBuffer();
        while (asReadOnlyByteBuffer.remaining() >= 40) {
            String str = "+" + asReadOnlyByteBuffer.getLong();
            UUID uuid = new UUID(asReadOnlyByteBuffer.getLong(), asReadOnlyByteBuffer.getLong());
            UUID uuid2 = new UUID(asReadOnlyByteBuffer.getLong(), asReadOnlyByteBuffer.getLong());
            UUID uuid3 = EMPTY_UUID;
            if (!uuid.equals(uuid3)) {
                hashMap.put(str, new ResponseItem(PNI.from(uuid), Optional.ofNullable(uuid2.equals(uuid3) ? null : ACI.from(uuid2))));
            }
        }
        return new Response(hashMap);
    }

    private static ClientRequest buildClientRequest(Request request) {
        ClientRequest.Builder aciUakPairs = ClientRequest.newBuilder().setPrevE164S(toByteString(parseAndSortE164Strings(request.previousE164s))).setNewE164S(toByteString(parseAndSortE164Strings(request.newE164s))).setDiscardE164S(toByteString(parseAndSortE164Strings(request.removedE164s))).setAciUakPairs(toByteString(request.serviceIds));
        if (request.token != null) {
            aciUakPairs.setToken(ByteString.copyFrom(request.token));
        }
        return aciUakPairs.build();
    }

    private static ByteString toByteString(List<Long> list) {
        ByteString.Output newOutput = ByteString.newOutput();
        for (Long l : list) {
            try {
                newOutput.write(ByteUtil.longToByteArray(l.longValue()));
            } catch (IOException e) {
                throw new AssertionError("Failed to write long to ByteString", e);
            }
        }
        return newOutput.toByteString();
    }

    private static ByteString toByteString(Map<ServiceId, ProfileKey> map) {
        ByteString.Output newOutput = ByteString.newOutput();
        for (Map.Entry<ServiceId, ProfileKey> entry : map.entrySet()) {
            try {
                newOutput.write(UuidUtil.toByteArray(entry.getKey().uuid()));
                newOutput.write(UnidentifiedAccess.deriveAccessKeyFrom(entry.getValue()));
            } catch (IOException e) {
                throw new AssertionError("Failed to write long to ByteString", e);
            }
        }
        return newOutput.toByteString();
    }

    private static List<Long> parseAndSortE164Strings(Collection<String> collection) {
        return (List) Collection$EL.stream(collection).map(new j$.util.function.Function() { // from class: org.whispersystems.signalservice.api.services.CdsiV2Service$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(Long.parseLong((String) obj));
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).sorted().collect(Collectors.toList());
    }

    /* loaded from: classes5.dex */
    public static final class Request {
        private final Set<String> newE164s;
        private final Set<String> previousE164s;
        private final Set<String> removedE164s;
        private final Map<ServiceId, ProfileKey> serviceIds;
        private final byte[] token;

        public Request(Set<String> set, Set<String> set2, Map<ServiceId, ProfileKey> map, Optional<byte[]> optional) {
            if (set.size() <= 0 || optional.isPresent()) {
                this.previousE164s = set;
                this.newE164s = set2;
                this.removedE164s = Collections.emptySet();
                this.serviceIds = map;
                this.token = optional.orElse(null);
                return;
            }
            throw new IllegalArgumentException("You must have a token if you have previousE164s!");
        }

        public int totalE164s() {
            return (this.previousE164s.size() + this.newE164s.size()) - this.removedE164s.size();
        }

        public int serviceIdSize() {
            return this.previousE164s.size() + this.newE164s.size() + this.removedE164s.size() + this.serviceIds.size();
        }
    }

    /* loaded from: classes5.dex */
    public static final class Response {
        private final Map<String, ResponseItem> results;

        public Response(Map<String, ResponseItem> map) {
            this.results = map;
        }

        public Map<String, ResponseItem> getResults() {
            return this.results;
        }
    }

    /* loaded from: classes5.dex */
    public static final class ResponseItem {
        private final Optional<ACI> aci;
        private final PNI pni;

        public ResponseItem(PNI pni, Optional<ACI> optional) {
            this.pni = pni;
            this.aci = optional;
        }

        public PNI getPni() {
            return this.pni;
        }

        public Optional<ACI> getAci() {
            return this.aci;
        }

        public boolean hasAci() {
            return this.aci.isPresent();
        }
    }
}
