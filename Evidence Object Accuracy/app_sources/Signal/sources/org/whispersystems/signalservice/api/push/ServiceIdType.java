package org.whispersystems.signalservice.api.push;

import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes5.dex */
public enum ServiceIdType {
    ACI("aci"),
    PNI(RecipientDatabase.PNI_COLUMN);
    
    private final String queryParamName;

    ServiceIdType(String str) {
        this.queryParamName = str;
    }

    public String queryParam() {
        return this.queryParamName;
    }
}
