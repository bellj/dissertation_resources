package org.whispersystems.signalservice.api.groupsv2;

import java.util.UUID;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;

/* loaded from: classes5.dex */
public final class UuidProfileKey {
    private final ProfileKey profileKey;
    private final UUID uuid;

    public UuidProfileKey(UUID uuid, ProfileKey profileKey) {
        this.uuid = uuid;
        this.profileKey = profileKey;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public ProfileKey getProfileKey() {
        return this.profileKey;
    }
}
