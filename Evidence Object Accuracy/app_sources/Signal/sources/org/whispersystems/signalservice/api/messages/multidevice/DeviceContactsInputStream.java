package org.whispersystems.signalservice.api.messages.multidevice;

import java.io.InputStream;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class DeviceContactsInputStream extends ChunkedInputStream {
    private static final String TAG;

    public DeviceContactsInputStream(InputStream inputStream) {
        super(inputStream);
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x017d  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x018b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.whispersystems.signalservice.api.messages.multidevice.DeviceContact read() throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 424
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsInputStream.read():org.whispersystems.signalservice.api.messages.multidevice.DeviceContact");
    }

    /* renamed from: org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsInputStream$1 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$Verified$State;

        static {
            int[] iArr = new int[SignalServiceProtos.Verified.State.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$Verified$State = iArr;
            try {
                iArr[SignalServiceProtos.Verified.State.VERIFIED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$Verified$State[SignalServiceProtos.Verified.State.UNVERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$Verified$State[SignalServiceProtos.Verified.State.DEFAULT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }
}
