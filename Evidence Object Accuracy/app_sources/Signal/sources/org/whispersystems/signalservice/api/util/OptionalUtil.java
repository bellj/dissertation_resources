package org.whispersystems.signalservice.api.util;

import com.google.protobuf.ByteString;
import j$.util.DesugarArrays;
import j$.util.Optional;
import java.util.Arrays;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord$$ExternalSyntheticLambda3;

/* loaded from: classes5.dex */
public final class OptionalUtil {
    private OptionalUtil() {
    }

    @SafeVarargs
    public static <E> Optional<E> or(Optional<E>... optionalArr) {
        return (Optional) DesugarArrays.stream(optionalArr).filter(new MediaMmsMessageRecord$$ExternalSyntheticLambda3()).findFirst().orElse(Optional.empty());
    }

    public static boolean byteArrayEquals(Optional<byte[]> optional, Optional<byte[]> optional2) {
        if (optional.isPresent() != optional2.isPresent()) {
            return false;
        }
        if (optional.isPresent()) {
            return Arrays.equals(optional.get(), optional2.get());
        }
        return true;
    }

    public static int byteArrayHashCode(Optional<byte[]> optional) {
        if (optional.isPresent()) {
            return Arrays.hashCode(optional.get());
        }
        return 0;
    }

    public static Optional<String> absentIfEmpty(String str) {
        if (str == null || str.length() == 0) {
            return Optional.empty();
        }
        return Optional.of(str);
    }

    public static Optional<byte[]> absentIfEmpty(ByteString byteString) {
        if (byteString == null || byteString.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(byteString.toByteArray());
    }
}
