package org.whispersystems.signalservice.api.payments;

/* loaded from: classes5.dex */
public abstract class Currency {
    public abstract String getCurrencyCode();

    public abstract int getDecimalPrecision();

    public abstract Formatter getFormatter(FormatterOptions formatterOptions);

    private Currency() {
    }

    /* loaded from: classes5.dex */
    public static class CryptoCurrency extends Currency {
        private final String currencyCode;
        private final int decimalPrecision;

        CryptoCurrency(String str, int i) {
            super();
            this.currencyCode = str;
            this.decimalPrecision = i;
        }

        @Override // org.whispersystems.signalservice.api.payments.Currency
        public String getCurrencyCode() {
            return this.currencyCode;
        }

        @Override // org.whispersystems.signalservice.api.payments.Currency
        public int getDecimalPrecision() {
            return this.decimalPrecision;
        }

        @Override // org.whispersystems.signalservice.api.payments.Currency
        public Formatter getFormatter(FormatterOptions formatterOptions) {
            return Formatter.forMoney(this, formatterOptions);
        }
    }

    /* loaded from: classes5.dex */
    private static class FiatCurrency extends Currency {
        private final java.util.Currency javaCurrency;

        private FiatCurrency(java.util.Currency currency) {
            super();
            this.javaCurrency = currency;
        }

        @Override // org.whispersystems.signalservice.api.payments.Currency
        public String getCurrencyCode() {
            return this.javaCurrency.getCurrencyCode();
        }

        @Override // org.whispersystems.signalservice.api.payments.Currency
        public int getDecimalPrecision() {
            return this.javaCurrency.getDefaultFractionDigits();
        }

        @Override // org.whispersystems.signalservice.api.payments.Currency
        public Formatter getFormatter(FormatterOptions formatterOptions) {
            return Formatter.forFiat(this.javaCurrency, formatterOptions);
        }
    }

    public static Currency fromJavaCurrency(java.util.Currency currency) {
        return new FiatCurrency(currency);
    }

    public static Currency fromCodeAndPrecision(String str, int i) {
        return new CryptoCurrency(str, i);
    }
}
