package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class DeviceContact {
    private final SignalServiceAddress address;
    private final boolean archived;
    private final Optional<SignalServiceAttachmentStream> avatar;
    private final boolean blocked;
    private final Optional<String> color;
    private final Optional<Integer> expirationTimer;
    private final Optional<Integer> inboxPosition;
    private final Optional<String> name;
    private final Optional<ProfileKey> profileKey;
    private final Optional<VerifiedMessage> verified;

    public DeviceContact(SignalServiceAddress signalServiceAddress, Optional<String> optional, Optional<SignalServiceAttachmentStream> optional2, Optional<String> optional3, Optional<VerifiedMessage> optional4, Optional<ProfileKey> optional5, boolean z, Optional<Integer> optional6, Optional<Integer> optional7, boolean z2) {
        this.address = signalServiceAddress;
        this.name = optional;
        this.avatar = optional2;
        this.color = optional3;
        this.verified = optional4;
        this.profileKey = optional5;
        this.blocked = z;
        this.expirationTimer = optional6;
        this.inboxPosition = optional7;
        this.archived = z2;
    }

    public Optional<SignalServiceAttachmentStream> getAvatar() {
        return this.avatar;
    }

    public Optional<String> getName() {
        return this.name;
    }

    public SignalServiceAddress getAddress() {
        return this.address;
    }

    public Optional<String> getColor() {
        return this.color;
    }

    public Optional<VerifiedMessage> getVerified() {
        return this.verified;
    }

    public Optional<ProfileKey> getProfileKey() {
        return this.profileKey;
    }

    public boolean isBlocked() {
        return this.blocked;
    }

    public Optional<Integer> getExpirationTimer() {
        return this.expirationTimer;
    }

    public Optional<Integer> getInboxPosition() {
        return this.inboxPosition;
    }

    public boolean isArchived() {
        return this.archived;
    }
}
