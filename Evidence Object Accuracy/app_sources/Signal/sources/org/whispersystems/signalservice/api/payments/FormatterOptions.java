package org.whispersystems.signalservice.api.payments;

import java.util.Locale;

/* loaded from: classes5.dex */
public final class FormatterOptions {
    public final boolean alwaysPositive;
    public final boolean alwaysPrefixWithSign;
    public final Locale locale;
    public final int maximumFractionDigits;
    public final boolean withSpaceBeforeUnit;
    public final boolean withUnit;

    FormatterOptions(Builder builder) {
        this.locale = builder.locale;
        this.alwaysPositive = builder.alwaysPositive;
        this.alwaysPrefixWithSign = builder.alwaysPrefixWithSign;
        this.withSpaceBeforeUnit = builder.withSpaceBeforeUnit;
        this.withUnit = builder.withUnit;
        this.maximumFractionDigits = builder.maximumFractionDigits;
    }

    public static FormatterOptions defaults() {
        return builder().build();
    }

    public static FormatterOptions defaults(Locale locale) {
        return builder(locale).build();
    }

    public static Builder builder() {
        return builder(Locale.getDefault());
    }

    public static Builder builder(Locale locale) {
        return new Builder(locale);
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        private boolean alwaysPositive;
        private boolean alwaysPrefixWithSign;
        private final Locale locale;
        private int maximumFractionDigits;
        private boolean withSpaceBeforeUnit;
        private boolean withUnit;

        private Builder(Locale locale) {
            this.alwaysPositive = false;
            this.alwaysPrefixWithSign = false;
            this.withSpaceBeforeUnit = true;
            this.withUnit = true;
            this.maximumFractionDigits = Integer.MAX_VALUE;
            this.locale = locale;
        }

        public Builder alwaysPositive() {
            this.alwaysPositive = true;
            return this;
        }

        public Builder alwaysPrefixWithSign() {
            this.alwaysPrefixWithSign = true;
            return this;
        }

        public Builder withoutSpaceBeforeUnit() {
            this.withSpaceBeforeUnit = false;
            return this;
        }

        public Builder withoutUnit() {
            this.withUnit = false;
            return this;
        }

        public Builder withMaximumFractionDigits(int i) {
            this.maximumFractionDigits = Math.max(i, 0);
            return this;
        }

        public FormatterOptions build() {
            return new FormatterOptions(this);
        }
    }
}
