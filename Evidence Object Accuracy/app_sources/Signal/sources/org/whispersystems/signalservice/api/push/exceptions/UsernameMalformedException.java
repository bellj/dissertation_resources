package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class UsernameMalformedException extends NonSuccessfulResponseCodeException {
    public UsernameMalformedException() {
        super(400);
    }
}
