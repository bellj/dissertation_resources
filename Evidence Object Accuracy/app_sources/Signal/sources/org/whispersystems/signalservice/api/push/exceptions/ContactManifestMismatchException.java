package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class ContactManifestMismatchException extends ConflictException {
    private final byte[] responseBody;

    public ContactManifestMismatchException(byte[] bArr) {
        this.responseBody = bArr;
    }

    public byte[] getResponseBody() {
        return this.responseBody;
    }
}
