package org.whispersystems.signalservice.api.messages.shared;

import j$.util.Optional;
import java.util.LinkedList;
import java.util.List;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;

/* loaded from: classes5.dex */
public class SharedContact {
    private final Optional<List<PostalAddress>> address;
    private final Optional<Avatar> avatar;
    private final Optional<List<Email>> email;
    private final Name name;
    private final Optional<String> organization;
    private final Optional<List<Phone>> phone;

    public SharedContact(Name name, Optional<Avatar> optional, Optional<List<Phone>> optional2, Optional<List<Email>> optional3, Optional<List<PostalAddress>> optional4, Optional<String> optional5) {
        this.name = name;
        this.avatar = optional;
        this.phone = optional2;
        this.email = optional3;
        this.address = optional4;
        this.organization = optional5;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Name getName() {
        return this.name;
    }

    public Optional<Avatar> getAvatar() {
        return this.avatar;
    }

    public Optional<List<Phone>> getPhone() {
        return this.phone;
    }

    public Optional<List<Email>> getEmail() {
        return this.email;
    }

    public Optional<List<PostalAddress>> getAddress() {
        return this.address;
    }

    public Optional<String> getOrganization() {
        return this.organization;
    }

    /* loaded from: classes5.dex */
    public static class Avatar {
        private final SignalServiceAttachment attachment;
        private final boolean isProfile;

        public Avatar(SignalServiceAttachment signalServiceAttachment, boolean z) {
            this.attachment = signalServiceAttachment;
            this.isProfile = z;
        }

        public SignalServiceAttachment getAttachment() {
            return this.attachment;
        }

        public boolean isProfile() {
            return this.isProfile;
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        /* loaded from: classes5.dex */
        public static class Builder {
            private SignalServiceAttachment attachment;
            private boolean isProfile;

            public Builder withAttachment(SignalServiceAttachment signalServiceAttachment) {
                this.attachment = signalServiceAttachment;
                return this;
            }

            public Builder withProfileFlag(boolean z) {
                this.isProfile = z;
                return this;
            }

            public Avatar build() {
                return new Avatar(this.attachment, this.isProfile);
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class Name {
        private final Optional<String> display;
        private final Optional<String> family;
        private final Optional<String> given;
        private final Optional<String> middle;
        private final Optional<String> prefix;
        private final Optional<String> suffix;

        public Name(Optional<String> optional, Optional<String> optional2, Optional<String> optional3, Optional<String> optional4, Optional<String> optional5, Optional<String> optional6) {
            this.display = optional;
            this.given = optional2;
            this.family = optional3;
            this.prefix = optional4;
            this.suffix = optional5;
            this.middle = optional6;
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Optional<String> getDisplay() {
            return this.display;
        }

        public Optional<String> getGiven() {
            return this.given;
        }

        public Optional<String> getFamily() {
            return this.family;
        }

        public Optional<String> getPrefix() {
            return this.prefix;
        }

        public Optional<String> getSuffix() {
            return this.suffix;
        }

        public Optional<String> getMiddle() {
            return this.middle;
        }

        /* loaded from: classes5.dex */
        public static class Builder {
            private String display;
            private String family;
            private String given;
            private String middle;
            private String prefix;
            private String suffix;

            public Builder setDisplay(String str) {
                this.display = str;
                return this;
            }

            public Builder setGiven(String str) {
                this.given = str;
                return this;
            }

            public Builder setFamily(String str) {
                this.family = str;
                return this;
            }

            public Builder setPrefix(String str) {
                this.prefix = str;
                return this;
            }

            public Builder setSuffix(String str) {
                this.suffix = str;
                return this;
            }

            public Builder setMiddle(String str) {
                this.middle = str;
                return this;
            }

            public Name build() {
                return new Name(Optional.ofNullable(this.display), Optional.ofNullable(this.given), Optional.ofNullable(this.family), Optional.ofNullable(this.prefix), Optional.ofNullable(this.suffix), Optional.ofNullable(this.middle));
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class Phone {
        private final Optional<String> label;
        private final Type type;
        private final String value;

        /* loaded from: classes5.dex */
        public enum Type {
            HOME,
            WORK,
            MOBILE,
            CUSTOM
        }

        public Phone(String str, Type type, Optional<String> optional) {
            this.value = str;
            this.type = type;
            this.label = optional;
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public String getValue() {
            return this.value;
        }

        public Type getType() {
            return this.type;
        }

        public Optional<String> getLabel() {
            return this.label;
        }

        /* loaded from: classes5.dex */
        public static class Builder {
            private String label;
            private Type type;
            private String value;

            public Builder setValue(String str) {
                this.value = str;
                return this;
            }

            public Builder setType(Type type) {
                this.type = type;
                return this;
            }

            public Builder setLabel(String str) {
                this.label = str;
                return this;
            }

            public Phone build() {
                return new Phone(this.value, this.type, Optional.ofNullable(this.label));
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class Email {
        private final Optional<String> label;
        private final Type type;
        private final String value;

        /* loaded from: classes5.dex */
        public enum Type {
            HOME,
            WORK,
            MOBILE,
            CUSTOM
        }

        public Email(String str, Type type, Optional<String> optional) {
            this.value = str;
            this.type = type;
            this.label = optional;
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public String getValue() {
            return this.value;
        }

        public Type getType() {
            return this.type;
        }

        public Optional<String> getLabel() {
            return this.label;
        }

        /* loaded from: classes5.dex */
        public static class Builder {
            private String label;
            private Type type;
            private String value;

            public Builder setValue(String str) {
                this.value = str;
                return this;
            }

            public Builder setType(Type type) {
                this.type = type;
                return this;
            }

            public Builder setLabel(String str) {
                this.label = str;
                return this;
            }

            public Email build() {
                return new Email(this.value, this.type, Optional.ofNullable(this.label));
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class PostalAddress {
        private final Optional<String> city;
        private final Optional<String> country;
        private final Optional<String> label;
        private final Optional<String> neighborhood;
        private final Optional<String> pobox;
        private final Optional<String> postcode;
        private final Optional<String> region;
        private final Optional<String> street;
        private final Type type;

        /* loaded from: classes5.dex */
        public enum Type {
            HOME,
            WORK,
            CUSTOM
        }

        public PostalAddress(Type type, Optional<String> optional, Optional<String> optional2, Optional<String> optional3, Optional<String> optional4, Optional<String> optional5, Optional<String> optional6, Optional<String> optional7, Optional<String> optional8) {
            this.type = type;
            this.label = optional;
            this.street = optional2;
            this.pobox = optional3;
            this.neighborhood = optional4;
            this.city = optional5;
            this.region = optional6;
            this.postcode = optional7;
            this.country = optional8;
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Type getType() {
            return this.type;
        }

        public Optional<String> getLabel() {
            return this.label;
        }

        public Optional<String> getStreet() {
            return this.street;
        }

        public Optional<String> getPobox() {
            return this.pobox;
        }

        public Optional<String> getNeighborhood() {
            return this.neighborhood;
        }

        public Optional<String> getCity() {
            return this.city;
        }

        public Optional<String> getRegion() {
            return this.region;
        }

        public Optional<String> getPostcode() {
            return this.postcode;
        }

        public Optional<String> getCountry() {
            return this.country;
        }

        /* loaded from: classes5.dex */
        public static class Builder {
            private String city;
            private String country;
            private String label;
            private String neighborhood;
            private String pobox;
            private String postcode;
            private String region;
            private String street;
            private Type type;

            public Builder setType(Type type) {
                this.type = type;
                return this;
            }

            public Builder setLabel(String str) {
                this.label = str;
                return this;
            }

            public Builder setStreet(String str) {
                this.street = str;
                return this;
            }

            public Builder setPobox(String str) {
                this.pobox = str;
                return this;
            }

            public Builder setNeighborhood(String str) {
                this.neighborhood = str;
                return this;
            }

            public Builder setCity(String str) {
                this.city = str;
                return this;
            }

            public Builder setRegion(String str) {
                this.region = str;
                return this;
            }

            public Builder setPostcode(String str) {
                this.postcode = str;
                return this;
            }

            public Builder setCountry(String str) {
                this.country = str;
                return this;
            }

            public PostalAddress build() {
                return new PostalAddress(this.type, Optional.ofNullable(this.label), Optional.ofNullable(this.street), Optional.ofNullable(this.pobox), Optional.ofNullable(this.neighborhood), Optional.ofNullable(this.city), Optional.ofNullable(this.region), Optional.ofNullable(this.postcode), Optional.ofNullable(this.country));
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class Builder {
        private List<PostalAddress> address = new LinkedList();
        private Avatar avatar;
        private List<Email> email = new LinkedList();
        private Name name;
        private String organization;
        private List<Phone> phone = new LinkedList();

        public Builder setName(Name name) {
            this.name = name;
            return this;
        }

        public Builder withOrganization(String str) {
            this.organization = str;
            return this;
        }

        public Builder setAvatar(Avatar avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder withPhone(Phone phone) {
            this.phone.add(phone);
            return this;
        }

        public Builder withPhones(List<Phone> list) {
            this.phone.addAll(list);
            return this;
        }

        public Builder withEmail(Email email) {
            this.email.add(email);
            return this;
        }

        public Builder withEmails(List<Email> list) {
            this.email.addAll(list);
            return this;
        }

        public Builder withAddress(PostalAddress postalAddress) {
            this.address.add(postalAddress);
            return this;
        }

        public Builder withAddresses(List<PostalAddress> list) {
            this.address.addAll(list);
            return this;
        }

        public SharedContact build() {
            return new SharedContact(this.name, Optional.ofNullable(this.avatar), this.phone.isEmpty() ? Optional.empty() : Optional.of(this.phone), this.email.isEmpty() ? Optional.empty() : Optional.of(this.email), this.address.isEmpty() ? Optional.empty() : Optional.of(this.address), Optional.ofNullable(this.organization));
        }
    }
}
