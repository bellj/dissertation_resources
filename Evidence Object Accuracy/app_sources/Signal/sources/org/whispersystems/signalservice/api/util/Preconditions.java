package org.whispersystems.signalservice.api.util;

/* loaded from: classes5.dex */
public final class Preconditions {
    private Preconditions() {
    }

    public static void checkArgument(boolean z) {
        checkArgument(z, "Condition must be true!");
    }

    public static void checkArgument(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void checkState(boolean z) {
        checkState(z, "Condition must be true!");
    }

    public static void checkState(boolean z, String str) {
        if (!z) {
            throw new IllegalStateException(str);
        }
    }

    public static <E> E checkNotNull(E e) {
        return (E) checkNotNull(e, "Must not be null!");
    }

    public static <E> E checkNotNull(E e, String str) {
        if (e != null) {
            return e;
        }
        throw new NullPointerException(str);
    }
}
