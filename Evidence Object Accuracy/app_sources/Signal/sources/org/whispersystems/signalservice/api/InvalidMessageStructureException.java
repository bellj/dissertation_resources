package org.whispersystems.signalservice.api;

import j$.util.Optional;

/* loaded from: classes5.dex */
public final class InvalidMessageStructureException extends Exception {
    private final Optional<Integer> device;
    private final Optional<String> sender;

    public InvalidMessageStructureException(String str) {
        super(str);
        this.sender = Optional.empty();
        this.device = Optional.empty();
    }

    public InvalidMessageStructureException(String str, String str2, int i) {
        super(str);
        this.sender = Optional.ofNullable(str2);
        this.device = Optional.of(Integer.valueOf(i));
    }

    public InvalidMessageStructureException(Exception exc, String str, int i) {
        super(exc);
        this.sender = Optional.ofNullable(str);
        this.device = Optional.of(Integer.valueOf(i));
    }

    public InvalidMessageStructureException(Exception exc) {
        super(exc);
        this.sender = Optional.empty();
        this.device = Optional.empty();
    }

    public Optional<String> getSender() {
        return this.sender;
    }

    public Optional<Integer> getDevice() {
        return this.device;
    }
}
