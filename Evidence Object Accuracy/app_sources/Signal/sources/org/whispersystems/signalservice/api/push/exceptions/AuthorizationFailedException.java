package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class AuthorizationFailedException extends NonSuccessfulResponseCodeException {
    public AuthorizationFailedException(int i, String str) {
        super(i, str);
    }
}
