package org.whispersystems.signalservice.api.push;

import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public class ServiceId {
    public static final ServiceId UNKNOWN = from(UuidUtil.UNKNOWN_UUID);
    protected final UUID uuid;

    public ServiceId(UUID uuid) {
        this.uuid = uuid;
    }

    public static ServiceId from(UUID uuid) {
        return new ServiceId(uuid);
    }

    public static ServiceId parseOrThrow(String str) {
        return from(UUID.fromString(str));
    }

    public static ServiceId parseOrThrow(byte[] bArr) {
        return from(UuidUtil.parseOrThrow(bArr));
    }

    public static ServiceId parseOrNull(String str) {
        UUID parseOrNull = UuidUtil.parseOrNull(str);
        if (parseOrNull != null) {
            return from(parseOrNull);
        }
        return null;
    }

    public static ServiceId parseOrNull(byte[] bArr) {
        UUID parseOrNull = UuidUtil.parseOrNull(bArr);
        if (parseOrNull != null) {
            return from(parseOrNull);
        }
        return null;
    }

    public static ServiceId parseOrUnknown(String str) {
        ServiceId parseOrNull = parseOrNull(str);
        return parseOrNull != null ? parseOrNull : UNKNOWN;
    }

    public static ServiceId fromByteString(ByteString byteString) {
        return parseOrThrow(byteString.toByteArray());
    }

    public static ServiceId fromByteStringOrNull(ByteString byteString) {
        UUID fromByteStringOrNull = UuidUtil.fromByteStringOrNull(byteString);
        if (fromByteStringOrNull != null) {
            return from(fromByteStringOrNull);
        }
        return null;
    }

    public static ServiceId fromByteStringOrUnknown(ByteString byteString) {
        ServiceId fromByteStringOrNull = fromByteStringOrNull(byteString);
        return fromByteStringOrNull != null ? fromByteStringOrNull : UNKNOWN;
    }

    public UUID uuid() {
        return this.uuid;
    }

    public boolean isUnknown() {
        return this.uuid.equals(UNKNOWN.uuid);
    }

    public SignalProtocolAddress toProtocolAddress(int i) {
        return new SignalProtocolAddress(this.uuid.toString(), i);
    }

    public ByteString toByteString() {
        return UuidUtil.toByteString(this.uuid);
    }

    public byte[] toByteArray() {
        return UuidUtil.toByteArray(this.uuid);
    }

    public static List<ServiceId> filterKnown(Collection<ServiceId> collection) {
        return (List) Collection$EL.stream(collection).filter(new Predicate() { // from class: org.whispersystems.signalservice.api.push.ServiceId$$ExternalSyntheticLambda0
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ServiceId.lambda$filterKnown$0((ServiceId) obj);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ boolean lambda$filterKnown$0(ServiceId serviceId) {
        return !serviceId.equals(UNKNOWN);
    }

    public String toString() {
        return this.uuid.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ServiceId)) {
            return false;
        }
        return Objects.equals(this.uuid, ((ServiceId) obj).uuid);
    }

    public int hashCode() {
        return this.uuid.hashCode();
    }
}
