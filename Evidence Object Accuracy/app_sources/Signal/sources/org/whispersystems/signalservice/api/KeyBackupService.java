package org.whispersystems.signalservice.api;

import androidx.recyclerview.widget.ItemTouchHelper;
import java.io.IOException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.util.Locale;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.api.kbs.HashedPin;
import org.whispersystems.signalservice.api.kbs.KbsData;
import org.whispersystems.signalservice.api.kbs.MasterKey;
import org.whispersystems.signalservice.internal.contacts.crypto.KeyBackupCipher;
import org.whispersystems.signalservice.internal.contacts.crypto.Quote;
import org.whispersystems.signalservice.internal.contacts.crypto.RemoteAttestation;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedQuoteException;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.BackupResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponse;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.RemoteAttestationUtil;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public final class KeyBackupService {
    private static final String TAG;
    private final String enclaveName;
    private final KeyStore iasKeyStore;
    private final int maxTries;
    private final String mrenclave;
    private final PushServiceSocket pushServiceSocket;
    private final byte[] serviceId;

    /* loaded from: classes5.dex */
    public interface HashSession {
        byte[] hashSalt();
    }

    /* loaded from: classes5.dex */
    public interface PinChangeSession extends HashSession {
        void disableRegistrationLock() throws IOException;

        void enableRegistrationLock(MasterKey masterKey) throws IOException;

        void removePin() throws IOException, UnauthenticatedResponseException;

        KbsPinData setPin(HashedPin hashedPin, MasterKey masterKey) throws IOException, UnauthenticatedResponseException;
    }

    /* loaded from: classes5.dex */
    public interface RestoreSession extends HashSession {
        KbsPinData restorePin(HashedPin hashedPin) throws UnauthenticatedResponseException, IOException, KeyBackupServicePinException, KeyBackupSystemNoDataException, InvalidKeyException;
    }

    public KeyBackupService(KeyStore keyStore, String str, byte[] bArr, String str2, PushServiceSocket pushServiceSocket, int i) {
        this.iasKeyStore = keyStore;
        this.enclaveName = str;
        this.serviceId = bArr;
        this.mrenclave = str2;
        this.pushServiceSocket = pushServiceSocket;
        this.maxTries = i;
    }

    public PinChangeSession newPinChangeSession() throws IOException {
        return newSession(this.pushServiceSocket.getKeyBackupServiceAuthorization(), null);
    }

    public PinChangeSession newPinChangeSession(TokenResponse tokenResponse) throws IOException {
        return newSession(this.pushServiceSocket.getKeyBackupServiceAuthorization(), tokenResponse);
    }

    public TokenResponse getToken(String str) throws IOException {
        return this.pushServiceSocket.getKeyBackupServiceToken(str, this.enclaveName);
    }

    public String getAuthorization() throws IOException {
        return this.pushServiceSocket.getKeyBackupServiceAuthorization();
    }

    public RestoreSession newRegistrationSession(String str, TokenResponse tokenResponse) throws IOException {
        return newSession(str, tokenResponse);
    }

    private Session newSession(String str, TokenResponse tokenResponse) throws IOException {
        if (tokenResponse == null) {
            tokenResponse = this.pushServiceSocket.getKeyBackupServiceToken(str, this.enclaveName);
        }
        return new Session(str, tokenResponse);
    }

    /* loaded from: classes5.dex */
    public class Session implements RestoreSession, PinChangeSession {
        private final String authorization;
        private final TokenResponse currentToken;

        Session(String str, TokenResponse tokenResponse) {
            KeyBackupService.this = r1;
            this.authorization = str;
            this.currentToken = tokenResponse;
        }

        @Override // org.whispersystems.signalservice.api.KeyBackupService.HashSession
        public byte[] hashSalt() {
            return this.currentToken.getBackupId();
        }

        @Override // org.whispersystems.signalservice.api.KeyBackupService.RestoreSession
        public KbsPinData restorePin(HashedPin hashedPin) throws UnauthenticatedResponseException, IOException, KeyBackupServicePinException, KeyBackupSystemNoDataException, InvalidKeyException {
            SecureRandom secureRandom = new SecureRandom();
            TokenResponse tokenResponse = this.currentToken;
            int i = 0;
            while (true) {
                i++;
                try {
                    return restorePin(hashedPin, tokenResponse);
                } catch (TokenException e) {
                    TokenResponse token = e.getToken();
                    if (e instanceof KeyBackupServicePinException) {
                        throw ((KeyBackupServicePinException) e);
                    } else if (!e.isCanAutomaticallyRetry() || i >= 5) {
                        throw new UnauthenticatedResponseException("Token mismatch, expended all automatic retries");
                    } else {
                        int i2 = (1 << (i - 1)) * ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION;
                        Util.sleep((long) (i2 + secureRandom.nextInt(i2)));
                        tokenResponse = token;
                    }
                }
            }
            throw new UnauthenticatedResponseException("Token mismatch, expended all automatic retries");
        }

        private KbsPinData restorePin(HashedPin hashedPin, TokenResponse tokenResponse) throws UnauthenticatedResponseException, IOException, TokenException, KeyBackupSystemNoDataException, InvalidKeyException {
            try {
                int tries = tokenResponse.getTries();
                RemoteAttestation andVerifyRemoteAttestation = getAndVerifyRemoteAttestation();
                RestoreResponse keyRestoreResponse = KeyBackupCipher.getKeyRestoreResponse(KeyBackupService.this.pushServiceSocket.putKbsData(this.authorization, KeyBackupCipher.createKeyRestoreRequest(hashedPin.getKbsAccessKey(), tokenResponse, andVerifyRemoteAttestation, KeyBackupService.this.serviceId), andVerifyRemoteAttestation.getCookies(), KeyBackupService.this.enclaveName), andVerifyRemoteAttestation);
                if (keyRestoreResponse.hasToken()) {
                    tokenResponse = new TokenResponse(tokenResponse.getBackupId(), keyRestoreResponse.getToken().toByteArray(), keyRestoreResponse.getTries());
                }
                String str = KeyBackupService.TAG;
                Log.i(str, "Restore " + keyRestoreResponse.getStatus());
                int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status[keyRestoreResponse.getStatus().ordinal()];
                if (i == 1) {
                    return new KbsPinData(hashedPin.decryptKbsDataIVCipherText(keyRestoreResponse.getData().toByteArray()).getMasterKey(), tokenResponse);
                }
                if (i == 2) {
                    Log.i(KeyBackupService.TAG, "Restore PIN_MISMATCH");
                    throw new KeyBackupServicePinException(tokenResponse);
                } else if (i == 3) {
                    Log.i(KeyBackupService.TAG, "Restore TOKEN_MISMATCH");
                    boolean z = tries == keyRestoreResponse.getTries();
                    Log.i(KeyBackupService.TAG, String.format(Locale.US, "Token MISMATCH %d %d", Integer.valueOf(tries), Integer.valueOf(keyRestoreResponse.getTries())));
                    throw new TokenException(tokenResponse, z);
                } else if (i == 4) {
                    Log.i(KeyBackupService.TAG, "Restore OK! No data though");
                    throw new KeyBackupSystemNoDataException();
                } else if (i != 5) {
                    throw new AssertionError("Unexpected case");
                } else {
                    throw new UnauthenticatedResponseException("Key is not valid yet, clock mismatch");
                }
            } catch (InvalidCiphertextException e) {
                throw new UnauthenticatedResponseException(e);
            }
        }

        private RemoteAttestation getAndVerifyRemoteAttestation() throws UnauthenticatedResponseException, IOException, InvalidKeyException {
            try {
                return RemoteAttestationUtil.getAndVerifyRemoteAttestation(KeyBackupService.this.pushServiceSocket, PushServiceSocket.ClientSet.KeyBackup, KeyBackupService.this.iasKeyStore, KeyBackupService.this.enclaveName, KeyBackupService.this.mrenclave, this.authorization);
            } catch (SignatureException | InvalidCiphertextException | Quote.InvalidQuoteFormatException | UnauthenticatedQuoteException e) {
                throw new UnauthenticatedResponseException(e);
            }
        }

        @Override // org.whispersystems.signalservice.api.KeyBackupService.PinChangeSession
        public KbsPinData setPin(HashedPin hashedPin, MasterKey masterKey) throws IOException, UnauthenticatedResponseException {
            KbsData createNewKbsData = hashedPin.createNewKbsData(masterKey);
            return new KbsPinData(masterKey, putKbsData(createNewKbsData.getKbsAccessKey(), createNewKbsData.getCipherText(), KeyBackupService.this.enclaveName, this.currentToken));
        }

        @Override // org.whispersystems.signalservice.api.KeyBackupService.PinChangeSession
        public void removePin() throws IOException, UnauthenticatedResponseException {
            try {
                RemoteAttestation andVerifyRemoteAttestation = getAndVerifyRemoteAttestation();
                KeyBackupCipher.getKeyDeleteResponseStatus(KeyBackupService.this.pushServiceSocket.putKbsData(this.authorization, KeyBackupCipher.createKeyDeleteRequest(this.currentToken, andVerifyRemoteAttestation, KeyBackupService.this.serviceId), andVerifyRemoteAttestation.getCookies(), KeyBackupService.this.enclaveName), andVerifyRemoteAttestation);
            } catch (InvalidKeyException | InvalidCiphertextException e) {
                throw new UnauthenticatedResponseException(e);
            }
        }

        @Override // org.whispersystems.signalservice.api.KeyBackupService.PinChangeSession
        public void enableRegistrationLock(MasterKey masterKey) throws IOException {
            KeyBackupService.this.pushServiceSocket.setRegistrationLockV2(masterKey.deriveRegistrationLock());
        }

        @Override // org.whispersystems.signalservice.api.KeyBackupService.PinChangeSession
        public void disableRegistrationLock() throws IOException {
            KeyBackupService.this.pushServiceSocket.disableRegistrationLockV2();
        }

        private TokenResponse putKbsData(byte[] bArr, byte[] bArr2, String str, TokenResponse tokenResponse) throws IOException, UnauthenticatedResponseException {
            try {
                RemoteAttestation andVerifyRemoteAttestation = getAndVerifyRemoteAttestation();
                BackupResponse keyBackupResponse = KeyBackupCipher.getKeyBackupResponse(KeyBackupService.this.pushServiceSocket.putKbsData(this.authorization, KeyBackupCipher.createKeyBackupRequest(bArr, bArr2, tokenResponse, andVerifyRemoteAttestation, KeyBackupService.this.serviceId, KeyBackupService.this.maxTries), andVerifyRemoteAttestation.getCookies(), str), andVerifyRemoteAttestation);
                BackupResponse.Status status = keyBackupResponse.getStatus();
                int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$BackupResponse$Status[status.ordinal()];
                if (i == 1) {
                    return keyBackupResponse.hasToken() ? new TokenResponse(tokenResponse.getBackupId(), keyBackupResponse.getToken().toByteArray(), KeyBackupService.this.maxTries) : tokenResponse;
                }
                if (i == 2) {
                    throw new UnauthenticatedResponseException("Already exists");
                } else if (i != 3) {
                    throw new AssertionError("Unknown response status " + status);
                } else {
                    throw new UnauthenticatedResponseException("Key is not valid yet, clock mismatch");
                }
            } catch (InvalidKeyException | InvalidCiphertextException e) {
                throw new UnauthenticatedResponseException(e);
            }
        }
    }

    /* renamed from: org.whispersystems.signalservice.api.KeyBackupService$1 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$BackupResponse$Status;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status;

        static {
            int[] iArr = new int[BackupResponse.Status.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$BackupResponse$Status = iArr;
            try {
                iArr[BackupResponse.Status.OK.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$BackupResponse$Status[BackupResponse.Status.ALREADY_EXISTS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$BackupResponse$Status[BackupResponse.Status.NOT_YET_VALID.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[RestoreResponse.Status.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status = iArr2;
            try {
                iArr2[RestoreResponse.Status.OK.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status[RestoreResponse.Status.PIN_MISMATCH.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status[RestoreResponse.Status.TOKEN_MISMATCH.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status[RestoreResponse.Status.MISSING.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$keybackup$protos$RestoreResponse$Status[RestoreResponse.Status.NOT_YET_VALID.ordinal()] = 5;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }
}
