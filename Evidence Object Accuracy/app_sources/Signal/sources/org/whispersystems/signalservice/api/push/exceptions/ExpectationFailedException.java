package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class ExpectationFailedException extends NonSuccessfulResponseCodeException {
    public ExpectationFailedException() {
        super(417);
    }
}
