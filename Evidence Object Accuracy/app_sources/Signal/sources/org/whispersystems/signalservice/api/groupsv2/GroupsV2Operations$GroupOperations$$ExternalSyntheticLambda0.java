package org.whispersystems.signalservice.api.groupsv2;

import j$.util.function.ToLongFunction;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda0 implements ToLongFunction {
    @Override // j$.util.function.ToLongFunction
    public final long applyAsLong(Object obj) {
        return ((DecryptedBannedMember) obj).getTimestamp();
    }
}
