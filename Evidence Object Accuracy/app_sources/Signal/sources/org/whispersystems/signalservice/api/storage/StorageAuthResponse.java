package org.whispersystems.signalservice.api.storage;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class StorageAuthResponse {
    @JsonProperty
    private String password;
    @JsonProperty
    private String username;

    public StorageAuthResponse() {
    }

    public StorageAuthResponse(String str, String str2) {
        this.username = str;
        this.password = str2;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }
}
