package org.whispersystems.signalservice.api.services;

import com.google.protobuf.ByteString;
import io.reactivex.rxjava3.core.Single;
import j$.util.Optional;
import j$.util.function.Function;
import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Objects;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.ServiceResponseProcessor;
import org.whispersystems.signalservice.internal.push.GroupMismatchedDevices;
import org.whispersystems.signalservice.internal.push.GroupStaleDevices;
import org.whispersystems.signalservice.internal.push.OutgoingPushMessageList;
import org.whispersystems.signalservice.internal.push.SendGroupMessageResponse;
import org.whispersystems.signalservice.internal.push.SendMessageResponse;
import org.whispersystems.signalservice.internal.push.exceptions.GroupMismatchedDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupStaleDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.InvalidUnidentifiedAccessHeaderException;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper;
import org.whispersystems.signalservice.internal.websocket.ErrorMapper;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public class MessagingService {
    private final SignalWebSocket signalWebSocket;

    public MessagingService(SignalWebSocket signalWebSocket) {
        this.signalWebSocket = signalWebSocket;
    }

    public Single<ServiceResponse<SendMessageResponse>> send(OutgoingPushMessageList outgoingPushMessageList, Optional<UnidentifiedAccess> optional) {
        AnonymousClass1 r0 = new LinkedList<String>() { // from class: org.whispersystems.signalservice.api.services.MessagingService.1
            {
                add("content-type:application/json");
            }
        };
        WebSocketProtos.WebSocketRequestMessage.Builder verb = WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(new SecureRandom().nextLong()).setVerb("PUT");
        Object[] objArr = {outgoingPushMessageList.getDestination()};
        ResponseMapper build = DefaultResponseMapper.extend(SendMessageResponse.class).withResponseMapper(new DefaultResponseMapper.CustomResponseMapper() { // from class: org.whispersystems.signalservice.api.services.MessagingService$$ExternalSyntheticLambda5
            @Override // org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper.CustomResponseMapper
            public final ServiceResponse map(int i, String str, Function function, boolean z) {
                return MessagingService.lambda$send$0(i, str, function, z);
            }
        }).withCustomError(404, new ErrorMapper() { // from class: org.whispersystems.signalservice.api.services.MessagingService$$ExternalSyntheticLambda6
            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public /* synthetic */ Throwable parseError(int i) {
                return ErrorMapper.CC.$default$parseError(this, i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public final Throwable parseError(int i, String str, Function function) {
                return MessagingService.lambda$send$1(OutgoingPushMessageList.this, i, str, function);
            }
        }).build();
        Single<WebsocketResponse> request = this.signalWebSocket.request(verb.setPath(String.format("/v1/messages/%s", objArr)).addAllHeaders(r0).setBody(ByteString.copyFrom(JsonUtil.toJson(outgoingPushMessageList).getBytes())).build(), optional);
        Objects.requireNonNull(build);
        return request.map(new MessagingService$$ExternalSyntheticLambda4(build)).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    public static /* synthetic */ ServiceResponse lambda$send$0(int i, String str, Function function, boolean z) throws MalformedResponseException {
        SendMessageResponse sendMessageResponse;
        if (Util.isEmpty(str)) {
            sendMessageResponse = new SendMessageResponse(false, z);
        } else {
            sendMessageResponse = (SendMessageResponse) JsonUtil.fromJsonResponse(str, SendMessageResponse.class);
        }
        sendMessageResponse.setSentUnidentfied(z);
        return ServiceResponse.forResult(sendMessageResponse, i, str);
    }

    public static /* synthetic */ Throwable lambda$send$1(OutgoingPushMessageList outgoingPushMessageList, int i, String str, Function function) throws MalformedResponseException {
        return new UnregisteredUserException(outgoingPushMessageList.getDestination(), new NotFoundException("not found"));
    }

    public Single<ServiceResponse<SendGroupMessageResponse>> sendToGroup(byte[] bArr, byte[] bArr2, long j, boolean z) {
        Single<WebsocketResponse> request = this.signalWebSocket.request(WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(new SecureRandom().nextLong()).setVerb("PUT").setPath(String.format(Locale.US, "/v1/messages/multi_recipient?ts=%s&online=%s", Long.valueOf(j), Boolean.valueOf(z))).addAllHeaders(new LinkedList<String>(bArr2) { // from class: org.whispersystems.signalservice.api.services.MessagingService.2
            final /* synthetic */ byte[] val$joinedUnidentifiedAccess;

            {
                this.val$joinedUnidentifiedAccess = r3;
                add("content-type:application/vnd.signal-messenger.mrm");
                add("Unidentified-Access-Key:" + Base64.encodeBytes(r3));
            }
        }).setBody(ByteString.copyFrom(bArr)).build());
        ResponseMapper build = DefaultResponseMapper.extend(SendGroupMessageResponse.class).withCustomError(401, new ErrorMapper() { // from class: org.whispersystems.signalservice.api.services.MessagingService$$ExternalSyntheticLambda0
            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public /* synthetic */ Throwable parseError(int i) {
                return ErrorMapper.CC.$default$parseError(this, i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public final Throwable parseError(int i, String str, Function function) {
                return MessagingService.lambda$sendToGroup$2(i, str, function);
            }
        }).withCustomError(404, new ErrorMapper() { // from class: org.whispersystems.signalservice.api.services.MessagingService$$ExternalSyntheticLambda1
            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public /* synthetic */ Throwable parseError(int i) {
                return ErrorMapper.CC.$default$parseError(this, i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public final Throwable parseError(int i, String str, Function function) {
                return MessagingService.lambda$sendToGroup$3(i, str, function);
            }
        }).withCustomError(409, new ErrorMapper() { // from class: org.whispersystems.signalservice.api.services.MessagingService$$ExternalSyntheticLambda2
            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public /* synthetic */ Throwable parseError(int i) {
                return ErrorMapper.CC.$default$parseError(this, i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public final Throwable parseError(int i, String str, Function function) {
                return MessagingService.lambda$sendToGroup$4(i, str, function);
            }
        }).withCustomError(410, new ErrorMapper() { // from class: org.whispersystems.signalservice.api.services.MessagingService$$ExternalSyntheticLambda3
            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public /* synthetic */ Throwable parseError(int i) {
                return ErrorMapper.CC.$default$parseError(this, i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
            public final Throwable parseError(int i, String str, Function function) {
                return MessagingService.lambda$sendToGroup$5(i, str, function);
            }
        }).build();
        Objects.requireNonNull(build);
        return request.map(new MessagingService$$ExternalSyntheticLambda4(build)).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    public static /* synthetic */ Throwable lambda$sendToGroup$2(int i, String str, Function function) throws MalformedResponseException {
        return new InvalidUnidentifiedAccessHeaderException();
    }

    public static /* synthetic */ Throwable lambda$sendToGroup$3(int i, String str, Function function) throws MalformedResponseException {
        return new NotFoundException("At least one unregistered user in message send.");
    }

    public static /* synthetic */ Throwable lambda$sendToGroup$4(int i, String str, Function function) throws MalformedResponseException {
        return new GroupMismatchedDevicesException((GroupMismatchedDevices[]) JsonUtil.fromJsonResponse(str, GroupMismatchedDevices[].class));
    }

    public static /* synthetic */ Throwable lambda$sendToGroup$5(int i, String str, Function function) throws MalformedResponseException {
        return new GroupStaleDevicesException((GroupStaleDevices[]) JsonUtil.fromJsonResponse(str, GroupStaleDevices[].class));
    }

    /* loaded from: classes5.dex */
    public static class SendResponseProcessor<T> extends ServiceResponseProcessor<T> {
        public SendResponseProcessor(ServiceResponse<T> serviceResponse) {
            super(serviceResponse);
        }
    }
}
