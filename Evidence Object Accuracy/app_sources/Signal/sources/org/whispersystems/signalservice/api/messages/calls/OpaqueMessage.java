package org.whispersystems.signalservice.api.messages.calls;

import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class OpaqueMessage {
    private final byte[] opaque;
    private final Urgency urgency;

    public OpaqueMessage(byte[] bArr, Urgency urgency) {
        this.opaque = bArr;
        this.urgency = urgency == null ? Urgency.DROPPABLE : urgency;
    }

    public byte[] getOpaque() {
        return this.opaque;
    }

    public Urgency getUrgency() {
        return this.urgency;
    }

    /* loaded from: classes5.dex */
    public enum Urgency {
        DROPPABLE,
        HANDLE_IMMEDIATELY;

        public SignalServiceProtos.CallMessage.Opaque.Urgency toProto() {
            if (this == HANDLE_IMMEDIATELY) {
                return SignalServiceProtos.CallMessage.Opaque.Urgency.HANDLE_IMMEDIATELY;
            }
            return SignalServiceProtos.CallMessage.Opaque.Urgency.DROPPABLE;
        }
    }
}
