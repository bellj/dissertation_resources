package org.whispersystems.signalservice.api.storage;

import java.util.Arrays;
import org.whispersystems.signalservice.api.crypto.CryptoUtil;
import org.whispersystems.util.Base64;
import org.whispersystems.util.StringUtil;

/* loaded from: classes5.dex */
public final class StorageKey {
    private static final int LENGTH;
    private final byte[] key;

    public StorageKey(byte[] bArr) {
        if (bArr.length == 32) {
            this.key = bArr;
            return;
        }
        throw new AssertionError();
    }

    public StorageManifestKey deriveManifestKey(long j) {
        return new StorageManifestKey(derive("Manifest_" + j));
    }

    public StorageItemKey deriveItemKey(byte[] bArr) {
        return new StorageItemKey(derive("Item_" + Base64.encodeBytes(bArr)));
    }

    private byte[] derive(String str) {
        return CryptoUtil.hmacSha256(this.key, StringUtil.utf8(str));
    }

    public byte[] serialize() {
        return (byte[]) this.key.clone();
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != StorageKey.class) {
            return false;
        }
        return Arrays.equals(((StorageKey) obj).key, this.key);
    }

    public int hashCode() {
        return Arrays.hashCode(this.key);
    }
}
