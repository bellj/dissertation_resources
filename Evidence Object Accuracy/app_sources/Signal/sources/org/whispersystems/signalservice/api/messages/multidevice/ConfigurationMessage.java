package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class ConfigurationMessage {
    private final Optional<Boolean> linkPreviews;
    private final Optional<Boolean> readReceipts;
    private final Optional<Boolean> typingIndicators;
    private final Optional<Boolean> unidentifiedDeliveryIndicators;

    public ConfigurationMessage(Optional<Boolean> optional, Optional<Boolean> optional2, Optional<Boolean> optional3, Optional<Boolean> optional4) {
        this.readReceipts = optional;
        this.unidentifiedDeliveryIndicators = optional2;
        this.typingIndicators = optional3;
        this.linkPreviews = optional4;
    }

    public Optional<Boolean> getReadReceipts() {
        return this.readReceipts;
    }

    public Optional<Boolean> getUnidentifiedDeliveryIndicators() {
        return this.unidentifiedDeliveryIndicators;
    }

    public Optional<Boolean> getTypingIndicators() {
        return this.typingIndicators;
    }

    public Optional<Boolean> getLinkPreviews() {
        return this.linkPreviews;
    }
}
