package org.whispersystems.signalservice.api.groupsv2;

import com.google.protobuf.ByteString;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.signal.storageservice.protos.groups.local.DecryptedApproveMember;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.signal.storageservice.protos.groups.local.DecryptedString;

/* loaded from: classes5.dex */
public final class GroupChangeReconstruct {
    public static DecryptedGroupChange reconstructGroupChange(DecryptedGroup decryptedGroup, DecryptedGroup decryptedGroup2) {
        DecryptedGroupChange.Builder revision = DecryptedGroupChange.newBuilder().setRevision(decryptedGroup2.getRevision());
        if (!decryptedGroup.getTitle().equals(decryptedGroup2.getTitle())) {
            revision.setNewTitle(DecryptedString.newBuilder().setValue(decryptedGroup2.getTitle()));
        }
        if (!decryptedGroup.getDescription().equals(decryptedGroup2.getDescription())) {
            revision.setNewDescription(DecryptedString.newBuilder().setValue(decryptedGroup2.getDescription()));
        }
        if (!decryptedGroup.getIsAnnouncementGroup().equals(decryptedGroup2.getIsAnnouncementGroup())) {
            revision.setNewIsAnnouncementGroup(decryptedGroup2.getIsAnnouncementGroup());
        }
        if (!decryptedGroup.getAvatar().equals(decryptedGroup2.getAvatar())) {
            revision.setNewAvatar(DecryptedString.newBuilder().setValue(decryptedGroup2.getAvatar()));
        }
        if (!decryptedGroup.getDisappearingMessagesTimer().equals(decryptedGroup2.getDisappearingMessagesTimer())) {
            revision.setNewTimer(decryptedGroup2.getDisappearingMessagesTimer());
        }
        if (!decryptedGroup.getAccessControl().getAttributes().equals(decryptedGroup2.getAccessControl().getAttributes())) {
            revision.setNewAttributeAccess(decryptedGroup2.getAccessControl().getAttributes());
        }
        if (!decryptedGroup.getAccessControl().getMembers().equals(decryptedGroup2.getAccessControl().getMembers())) {
            revision.setNewMemberAccess(decryptedGroup2.getAccessControl().getMembers());
        }
        Set<ByteString> membersToSetOfUuids = membersToSetOfUuids(decryptedGroup.getMembersList());
        Set<ByteString> membersToSetOfUuids2 = membersToSetOfUuids(decryptedGroup2.getMembersList());
        Set<ByteString> pendingMembersToSetOfUuids = pendingMembersToSetOfUuids(decryptedGroup.getPendingMembersList());
        Set<ByteString> pendingMembersToSetOfUuids2 = pendingMembersToSetOfUuids(decryptedGroup2.getPendingMembersList());
        Set<ByteString> requestingMembersToSetOfUuids = requestingMembersToSetOfUuids(decryptedGroup.getRequestingMembersList());
        Set<ByteString> requestingMembersToSetOfUuids2 = requestingMembersToSetOfUuids(decryptedGroup2.getRequestingMembersList());
        Set<ByteString> bannedMembersToSetOfUuids = bannedMembersToSetOfUuids(decryptedGroup.getBannedMembersList());
        Set<ByteString> bannedMembersToSetOfUuids2 = bannedMembersToSetOfUuids(decryptedGroup2.getBannedMembersList());
        Set subtract = subtract(pendingMembersToSetOfUuids, pendingMembersToSetOfUuids2);
        Set subtract2 = subtract(requestingMembersToSetOfUuids, requestingMembersToSetOfUuids2);
        Set subtract3 = subtract(pendingMembersToSetOfUuids2, pendingMembersToSetOfUuids);
        Set subtract4 = subtract(requestingMembersToSetOfUuids2, requestingMembersToSetOfUuids);
        Set subtract5 = subtract(membersToSetOfUuids, membersToSetOfUuids2);
        Set subtract6 = subtract(membersToSetOfUuids2, membersToSetOfUuids);
        Set<ByteString> subtract7 = subtract(bannedMembersToSetOfUuids, bannedMembersToSetOfUuids2);
        Set<ByteString> subtract8 = subtract(bannedMembersToSetOfUuids2, bannedMembersToSetOfUuids);
        Set intersect = intersect(subtract6, subtract);
        Set intersect2 = intersect(subtract6, subtract2);
        Set<DecryptedMember> intersectByUUID = intersectByUUID(decryptedGroup2.getMembersList(), intersect);
        Set<DecryptedMember> intersectByUUID2 = intersectByUUID(decryptedGroup2.getMembersList(), intersect2);
        Set<DecryptedMember> intersectByUUID3 = intersectByUUID(decryptedGroup2.getMembersList(), subtract(subtract6, intersect, intersect2));
        Set<DecryptedPendingMember> intersectPendingByUUID = intersectPendingByUUID(decryptedGroup.getPendingMembersList(), subtract(subtract, intersect));
        Set<DecryptedRequestingMember> intersectRequestingByUUID = intersectRequestingByUUID(decryptedGroup.getRequestingMembersList(), subtract(subtract2, intersect2));
        for (DecryptedMember decryptedMember : intersectByUUID(decryptedGroup.getMembersList(), subtract5)) {
            revision.addDeleteMembers(decryptedMember.getUuid());
        }
        for (DecryptedMember decryptedMember2 : intersectByUUID3) {
            revision.addNewMembers(decryptedMember2);
        }
        for (DecryptedMember decryptedMember3 : intersectByUUID) {
            revision.addPromotePendingMembers(decryptedMember3);
        }
        for (DecryptedPendingMember decryptedPendingMember : intersectPendingByUUID) {
            revision.addDeletePendingMembers(DecryptedPendingMemberRemoval.newBuilder().setUuid(decryptedPendingMember.getUuid()).setUuidCipherText(decryptedPendingMember.getUuidCipherText()));
        }
        for (DecryptedPendingMember decryptedPendingMember2 : intersectPendingByUUID(decryptedGroup2.getPendingMembersList(), subtract3)) {
            revision.addNewPendingMembers(decryptedPendingMember2);
        }
        Set<DecryptedMember> intersectByUUID4 = intersectByUUID(subtract(decryptedGroup2.getMembersList(), decryptedGroup.getMembersList()), intersect(membersToSetOfUuids, membersToSetOfUuids2));
        Map<ByteString, DecryptedMember> uuidMap = uuidMap(decryptedGroup.getMembersList());
        Map<ByteString, DecryptedBannedMember> bannedUuidMap = bannedUuidMap(decryptedGroup2.getBannedMembersList());
        for (DecryptedMember decryptedMember4 : intersectByUUID4) {
            DecryptedMember decryptedMember5 = uuidMap.get(decryptedMember4.getUuid());
            if (decryptedMember5.getRole() != decryptedMember4.getRole()) {
                revision.addModifyMemberRoles(DecryptedModifyMemberRole.newBuilder().setUuid(decryptedMember4.getUuid()).setRole(decryptedMember4.getRole()));
            }
            if (!decryptedMember5.getProfileKey().equals(decryptedMember4.getProfileKey())) {
                revision.addModifiedProfileKeys(decryptedMember4);
            }
        }
        if (!decryptedGroup.getAccessControl().getAddFromInviteLink().equals(decryptedGroup2.getAccessControl().getAddFromInviteLink())) {
            revision.setNewInviteLinkAccess(decryptedGroup2.getAccessControl().getAddFromInviteLink());
        }
        for (DecryptedRequestingMember decryptedRequestingMember : intersectRequestingByUUID(decryptedGroup2.getRequestingMembersList(), subtract4)) {
            revision.addNewRequestingMembers(decryptedRequestingMember);
        }
        for (DecryptedRequestingMember decryptedRequestingMember2 : intersectRequestingByUUID) {
            revision.addDeleteRequestingMembers(decryptedRequestingMember2.getUuid());
        }
        for (DecryptedMember decryptedMember6 : intersectByUUID2) {
            revision.addPromoteRequestingMembers(DecryptedApproveMember.newBuilder().setUuid(decryptedMember6.getUuid()).setRole(decryptedMember6.getRole()));
        }
        if (!decryptedGroup.getInviteLinkPassword().equals(decryptedGroup2.getInviteLinkPassword())) {
            revision.setNewInviteLinkPassword(decryptedGroup2.getInviteLinkPassword());
        }
        for (ByteString byteString : subtract7) {
            revision.addDeleteBannedMembers(DecryptedBannedMember.newBuilder().setUuid(byteString).build());
        }
        for (ByteString byteString2 : subtract8) {
            DecryptedBannedMember.Builder uuid = DecryptedBannedMember.newBuilder().setUuid(byteString2);
            DecryptedBannedMember decryptedBannedMember = bannedUuidMap.get(byteString2);
            if (decryptedBannedMember != null) {
                uuid.setTimestamp(decryptedBannedMember.getTimestamp());
            }
            revision.addNewBannedMembers(uuid);
        }
        return revision.build();
    }

    private static Map<ByteString, DecryptedMember> uuidMap(List<DecryptedMember> list) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(list.size());
        for (DecryptedMember decryptedMember : list) {
            linkedHashMap.put(decryptedMember.getUuid(), decryptedMember);
        }
        return linkedHashMap;
    }

    private static Map<ByteString, DecryptedBannedMember> bannedUuidMap(List<DecryptedBannedMember> list) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(list.size());
        for (DecryptedBannedMember decryptedBannedMember : list) {
            linkedHashMap.put(decryptedBannedMember.getUuid(), decryptedBannedMember);
        }
        return linkedHashMap;
    }

    private static Set<DecryptedMember> intersectByUUID(Collection<DecryptedMember> collection, Set<ByteString> set) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedMember decryptedMember : collection) {
            if (set.contains(decryptedMember.getUuid())) {
                linkedHashSet.add(decryptedMember);
            }
        }
        return linkedHashSet;
    }

    private static Set<DecryptedPendingMember> intersectPendingByUUID(Collection<DecryptedPendingMember> collection, Set<ByteString> set) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedPendingMember decryptedPendingMember : collection) {
            if (set.contains(decryptedPendingMember.getUuid())) {
                linkedHashSet.add(decryptedPendingMember);
            }
        }
        return linkedHashSet;
    }

    private static Set<DecryptedRequestingMember> intersectRequestingByUUID(Collection<DecryptedRequestingMember> collection, Set<ByteString> set) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedRequestingMember decryptedRequestingMember : collection) {
            if (set.contains(decryptedRequestingMember.getUuid())) {
                linkedHashSet.add(decryptedRequestingMember);
            }
        }
        return linkedHashSet;
    }

    private static Set<ByteString> pendingMembersToSetOfUuids(Collection<DecryptedPendingMember> collection) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedPendingMember decryptedPendingMember : collection) {
            linkedHashSet.add(decryptedPendingMember.getUuid());
        }
        return linkedHashSet;
    }

    private static Set<ByteString> requestingMembersToSetOfUuids(Collection<DecryptedRequestingMember> collection) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedRequestingMember decryptedRequestingMember : collection) {
            linkedHashSet.add(decryptedRequestingMember.getUuid());
        }
        return linkedHashSet;
    }

    private static Set<ByteString> membersToSetOfUuids(Collection<DecryptedMember> collection) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedMember decryptedMember : collection) {
            linkedHashSet.add(decryptedMember.getUuid());
        }
        return linkedHashSet;
    }

    private static Set<ByteString> bannedMembersToSetOfUuids(Collection<DecryptedBannedMember> collection) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection.size());
        for (DecryptedBannedMember decryptedBannedMember : collection) {
            linkedHashSet.add(decryptedBannedMember.getUuid());
        }
        return linkedHashSet;
    }

    private static <T> Set<T> subtract(Collection<T> collection, Collection<T> collection2) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection);
        linkedHashSet.removeAll(collection2);
        return linkedHashSet;
    }

    private static <T> Set<T> subtract(Collection<T> collection, Collection<T> collection2, Collection<T> collection3) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection);
        linkedHashSet.removeAll(collection2);
        linkedHashSet.removeAll(collection3);
        return linkedHashSet;
    }

    private static <T> Set<T> intersect(Collection<T> collection, Collection<T> collection2) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection);
        linkedHashSet.retainAll(collection2);
        return linkedHashSet;
    }
}
