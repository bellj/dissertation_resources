package org.whispersystems.signalservice.api.groupsv2;

import com.google.protobuf.ByteString;
import java.util.ArrayList;
import java.util.List;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;

/* loaded from: classes5.dex */
public final class DecryptedGroupChangeActionsBuilderChangeSetModifier implements ChangeSetModifier {
    private final DecryptedGroupChange.Builder result;

    public DecryptedGroupChangeActionsBuilderChangeSetModifier(DecryptedGroupChange.Builder builder) {
        this.result = builder;
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddMembers(int i) {
        this.result.removeNewMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void moveAddToPromote(int i) {
        this.result.removeNewMembers(i);
        this.result.addPromotePendingMembers(this.result.getNewMembersList().get(i));
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeleteMembers(int i) {
        this.result.clearDeleteMembers().addAllDeleteMembers(removeIndexFromByteStringList(this.result.getDeleteMembersList(), i));
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeModifyMemberRoles(int i) {
        this.result.removeModifyMemberRoles(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeModifyMemberProfileKeys(int i) {
        this.result.removeModifiedProfileKeys(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddPendingMembers(int i) {
        this.result.removeNewPendingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeletePendingMembers(int i) {
        this.result.removeDeletePendingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removePromotePendingMembers(int i) {
        this.result.removePromotePendingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyTitle() {
        this.result.clearNewTitle();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAvatar() {
        this.result.clearNewAvatar();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyDisappearingMessagesTimer() {
        this.result.clearNewTimer();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAttributesAccess() {
        this.result.clearNewAttributeAccess();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyMemberAccess() {
        this.result.clearNewMemberAccess();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAddFromInviteLinkAccess() {
        this.result.clearNewInviteLinkAccess();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddRequestingMembers(int i) {
        this.result.removeNewRequestingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void moveAddRequestingMembersToPromote(int i) {
        DecryptedRequestingMember decryptedRequestingMember = this.result.getNewRequestingMembersList().get(i);
        this.result.removeNewRequestingMembers(i);
        this.result.addPromotePendingMembers(0, DecryptedMember.newBuilder().setUuid(decryptedRequestingMember.getUuid()).setProfileKey(decryptedRequestingMember.getProfileKey()).setRole(Member.Role.DEFAULT).build());
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeleteRequestingMembers(int i) {
        this.result.clearDeleteRequestingMembers().addAllDeleteRequestingMembers(removeIndexFromByteStringList(this.result.getDeleteRequestingMembersList(), i));
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removePromoteRequestingMembers(int i) {
        this.result.removePromoteRequestingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyDescription() {
        this.result.clearNewDescription();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAnnouncementsOnly() {
        this.result.clearNewIsAnnouncementGroup();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddBannedMembers(int i) {
        this.result.removeNewBannedMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeleteBannedMembers(int i) {
        this.result.removeDeleteBannedMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removePromotePendingPniAciMembers(int i) {
        this.result.removePromotePendingPniAciMembers(i);
    }

    private static List<ByteString> removeIndexFromByteStringList(List<ByteString> list, int i) {
        ArrayList arrayList = new ArrayList(list);
        arrayList.remove(i);
        return arrayList;
    }
}
