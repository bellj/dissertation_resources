package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class SignalServiceTypingMessage {
    private final Action action;
    private final Optional<byte[]> groupId;
    private final long timestamp;

    /* loaded from: classes5.dex */
    public enum Action {
        UNKNOWN,
        STARTED,
        STOPPED
    }

    public SignalServiceTypingMessage(Action action, long j, Optional<byte[]> optional) {
        this.action = action;
        this.timestamp = j;
        this.groupId = optional;
    }

    public Action getAction() {
        return this.action;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public Optional<byte[]> getGroupId() {
        return this.groupId;
    }

    public boolean isTypingStarted() {
        return this.action == Action.STARTED;
    }

    public boolean isTypingStopped() {
        return this.action == Action.STOPPED;
    }
}
