package org.whispersystems.signalservice.api;

import java.io.Closeable;

/* loaded from: classes5.dex */
public interface SignalSessionLock {

    /* loaded from: classes5.dex */
    public interface Lock extends Closeable {
        @Override // java.io.Closeable, java.lang.AutoCloseable
        void close();
    }

    Lock acquire();
}
