package org.whispersystems.signalservice.api.util;

/* loaded from: classes5.dex */
public interface SleepTimer {
    void sleep(long j) throws InterruptedException;
}
