package org.whispersystems.signalservice.api.messages.calls;

/* loaded from: classes5.dex */
public class BusyMessage {
    private final long id;

    public BusyMessage(long j) {
        this.id = j;
    }

    public long getId() {
        return this.id;
    }
}
