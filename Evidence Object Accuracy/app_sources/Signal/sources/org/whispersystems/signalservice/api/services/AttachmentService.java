package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.core.Single;
import java.security.SecureRandom;
import java.util.Objects;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.ServiceResponseProcessor;
import org.whispersystems.signalservice.internal.push.AttachmentV2UploadAttributes;
import org.whispersystems.signalservice.internal.push.AttachmentV3UploadAttributes;
import org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;

/* loaded from: classes5.dex */
public final class AttachmentService {
    private final SignalWebSocket signalWebSocket;

    public AttachmentService(SignalWebSocket signalWebSocket) {
        this.signalWebSocket = signalWebSocket;
    }

    public Single<ServiceResponse<AttachmentV2UploadAttributes>> getAttachmentV2UploadAttributes() {
        Single<WebsocketResponse> request = this.signalWebSocket.request(WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(new SecureRandom().nextLong()).setVerb("GET").setPath("/v2/attachments/form/upload").build());
        DefaultResponseMapper defaultResponseMapper = DefaultResponseMapper.getDefault(AttachmentV2UploadAttributes.class);
        Objects.requireNonNull(defaultResponseMapper);
        return request.map(new AttachmentService$$ExternalSyntheticLambda0(defaultResponseMapper)).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    public Single<ServiceResponse<AttachmentV3UploadAttributes>> getAttachmentV3UploadAttributes() {
        Single<WebsocketResponse> request = this.signalWebSocket.request(WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(new SecureRandom().nextLong()).setVerb("GET").setPath("/v3/attachments/form/upload").build());
        DefaultResponseMapper defaultResponseMapper = DefaultResponseMapper.getDefault(AttachmentV3UploadAttributes.class);
        Objects.requireNonNull(defaultResponseMapper);
        return request.map(new AttachmentService$$ExternalSyntheticLambda0(defaultResponseMapper)).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    /* loaded from: classes5.dex */
    public static class AttachmentAttributesResponseProcessor<T> extends ServiceResponseProcessor<T> {
        public AttachmentAttributesResponseProcessor(ServiceResponse<T> serviceResponse) {
            super(serviceResponse);
        }
    }
}
