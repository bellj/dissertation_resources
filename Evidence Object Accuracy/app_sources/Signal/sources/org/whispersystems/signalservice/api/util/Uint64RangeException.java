package org.whispersystems.signalservice.api.util;

/* loaded from: classes5.dex */
public final class Uint64RangeException extends Exception {
    public Uint64RangeException(String str) {
        super(str);
    }
}
