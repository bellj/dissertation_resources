package org.whispersystems.signalservice.api.crypto;

import java.util.List;
import org.signal.libsignal.metadata.InvalidMetadataMessageException;
import org.signal.libsignal.metadata.InvalidMetadataVersionException;
import org.signal.libsignal.metadata.ProtocolDuplicateMessageException;
import org.signal.libsignal.metadata.ProtocolInvalidKeyException;
import org.signal.libsignal.metadata.ProtocolInvalidKeyIdException;
import org.signal.libsignal.metadata.ProtocolInvalidMessageException;
import org.signal.libsignal.metadata.ProtocolInvalidVersionException;
import org.signal.libsignal.metadata.ProtocolLegacyMessageException;
import org.signal.libsignal.metadata.ProtocolNoSessionException;
import org.signal.libsignal.metadata.ProtocolUntrustedIdentityException;
import org.signal.libsignal.metadata.SealedSessionCipher;
import org.signal.libsignal.metadata.SelfSendException;
import org.signal.libsignal.metadata.certificate.CertificateValidator;
import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidRegistrationIdException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes5.dex */
public class SignalSealedSessionCipher {
    private final SealedSessionCipher cipher;
    private final SignalSessionLock lock;

    public SignalSealedSessionCipher(SignalSessionLock signalSessionLock, SealedSessionCipher sealedSessionCipher) {
        this.lock = signalSessionLock;
        this.cipher = sealedSessionCipher;
    }

    public byte[] encrypt(SignalProtocolAddress signalProtocolAddress, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) throws InvalidKeyException, UntrustedIdentityException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            byte[] encrypt = this.cipher.encrypt(signalProtocolAddress, unidentifiedSenderMessageContent);
            if (acquire != null) {
                acquire.close();
            }
            return encrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public byte[] multiRecipientEncrypt(List<SignalProtocolAddress> list, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) throws InvalidKeyException, UntrustedIdentityException, NoSessionException, InvalidRegistrationIdException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            byte[] multiRecipientEncrypt = this.cipher.multiRecipientEncrypt(list, unidentifiedSenderMessageContent);
            if (acquire != null) {
                acquire.close();
            }
            return multiRecipientEncrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public SealedSessionCipher.DecryptionResult decrypt(CertificateValidator certificateValidator, byte[] bArr, long j) throws InvalidMetadataMessageException, InvalidMetadataVersionException, ProtocolInvalidMessageException, ProtocolInvalidKeyException, ProtocolNoSessionException, ProtocolLegacyMessageException, ProtocolInvalidVersionException, ProtocolDuplicateMessageException, ProtocolInvalidKeyIdException, ProtocolUntrustedIdentityException, SelfSendException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            SealedSessionCipher.DecryptionResult decrypt = this.cipher.decrypt(certificateValidator, bArr, j);
            if (acquire != null) {
                acquire.close();
            }
            return decrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public int getSessionVersion(SignalProtocolAddress signalProtocolAddress) {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            int sessionVersion = this.cipher.getSessionVersion(signalProtocolAddress);
            if (acquire != null) {
                acquire.close();
            }
            return sessionVersion;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public int getRemoteRegistrationId(SignalProtocolAddress signalProtocolAddress) {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            int remoteRegistrationId = this.cipher.getRemoteRegistrationId(signalProtocolAddress);
            if (acquire != null) {
                acquire.close();
            }
            return remoteRegistrationId;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
