package org.whispersystems.signalservice.api.kbs;

import java.util.Arrays;
import org.whispersystems.signalservice.api.crypto.HmacSIV;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;

/* loaded from: classes5.dex */
public final class HashedPin {
    private final byte[] K;
    private final byte[] kbsAccessKey;

    private HashedPin(byte[] bArr, byte[] bArr2) {
        this.K = bArr;
        this.kbsAccessKey = bArr2;
    }

    public static HashedPin fromArgon2Hash(byte[] bArr) {
        if (bArr.length == 64) {
            return new HashedPin(Arrays.copyOfRange(bArr, 0, 32), Arrays.copyOfRange(bArr, 32, 64));
        }
        throw new AssertionError();
    }

    public KbsData createNewKbsData(MasterKey masterKey) {
        return new KbsData(masterKey, this.kbsAccessKey, HmacSIV.encrypt(this.K, masterKey.serialize()));
    }

    public KbsData decryptKbsDataIVCipherText(byte[] bArr) throws InvalidCiphertextException {
        return new KbsData(new MasterKey(HmacSIV.decrypt(this.K, bArr)), this.kbsAccessKey, bArr);
    }

    public byte[] getKbsAccessKey() {
        return this.kbsAccessKey;
    }
}
