package org.whispersystems.signalservice.api.crypto;

/* loaded from: classes5.dex */
public class InvalidCiphertextException extends Exception {
    public InvalidCiphertextException(Exception exc) {
        super(exc);
    }

    public InvalidCiphertextException(String str) {
        super(str);
    }
}
