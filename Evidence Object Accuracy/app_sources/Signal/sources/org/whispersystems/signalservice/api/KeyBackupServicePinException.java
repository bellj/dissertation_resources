package org.whispersystems.signalservice.api;

import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes5.dex */
public final class KeyBackupServicePinException extends TokenException {
    private final int triesRemaining;

    @Override // org.whispersystems.signalservice.api.TokenException
    public /* bridge */ /* synthetic */ TokenResponse getToken() {
        return super.getToken();
    }

    @Override // org.whispersystems.signalservice.api.TokenException
    public /* bridge */ /* synthetic */ boolean isCanAutomaticallyRetry() {
        return super.isCanAutomaticallyRetry();
    }

    public KeyBackupServicePinException(TokenResponse tokenResponse) {
        super(tokenResponse, false);
        this.triesRemaining = tokenResponse.getTries();
    }

    public int getTriesRemaining() {
        return this.triesRemaining;
    }
}
