package org.whispersystems.signalservice.api.messages.multidevice;

import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes5.dex */
public class ReadMessage {
    private final ServiceId sender;
    private final long timestamp;

    public ReadMessage(ServiceId serviceId, long j) {
        this.sender = serviceId;
        this.timestamp = j;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public ServiceId getSender() {
        return this.sender;
    }
}
