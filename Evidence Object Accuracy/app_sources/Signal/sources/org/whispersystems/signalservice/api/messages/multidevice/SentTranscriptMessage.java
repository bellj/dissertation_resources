package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Map;
import j$.util.Optional;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessageRecipient;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class SentTranscriptMessage {
    private final Optional<SignalServiceAddress> destination;
    private final long expirationStartTimestamp;
    private final boolean isRecipientUpdate;
    private final Optional<SignalServiceDataMessage> message;
    private final Set<ServiceId> recipients;
    private final Optional<SignalServiceStoryMessage> storyMessage;
    private final Set<SignalServiceStoryMessageRecipient> storyMessageRecipients;
    private final long timestamp;
    private final Map<ServiceId, Boolean> unidentifiedStatusBySid;

    public SentTranscriptMessage(Optional<SignalServiceAddress> optional, long j, Optional<SignalServiceDataMessage> optional2, long j2, Map<ServiceId, Boolean> map, boolean z, Optional<SignalServiceStoryMessage> optional3, Set<SignalServiceStoryMessageRecipient> set) {
        this.destination = optional;
        this.timestamp = j;
        this.message = optional2;
        this.expirationStartTimestamp = j2;
        this.unidentifiedStatusBySid = new HashMap(map);
        this.recipients = map.keySet();
        this.isRecipientUpdate = z;
        this.storyMessage = optional3;
        this.storyMessageRecipients = set;
    }

    public Optional<SignalServiceAddress> getDestination() {
        return this.destination;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public long getExpirationStartTimestamp() {
        return this.expirationStartTimestamp;
    }

    public Optional<SignalServiceDataMessage> getDataMessage() {
        return this.message;
    }

    public Optional<SignalServiceStoryMessage> getStoryMessage() {
        return this.storyMessage;
    }

    public Set<SignalServiceStoryMessageRecipient> getStoryMessageRecipients() {
        return this.storyMessageRecipients;
    }

    public boolean isUnidentified(ServiceId serviceId) {
        return ((Boolean) Map.EL.getOrDefault(this.unidentifiedStatusBySid, serviceId, Boolean.FALSE)).booleanValue();
    }

    public Set<ServiceId> getRecipients() {
        return this.recipients;
    }

    public boolean isRecipientUpdate() {
        return this.isRecipientUpdate;
    }
}
