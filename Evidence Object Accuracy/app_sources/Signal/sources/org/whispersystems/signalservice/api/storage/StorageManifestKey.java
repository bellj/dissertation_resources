package org.whispersystems.signalservice.api.storage;

import java.util.Arrays;

/* loaded from: classes5.dex */
public final class StorageManifestKey implements StorageCipherKey {
    private static final int LENGTH;
    private final byte[] key;

    public StorageManifestKey(byte[] bArr) {
        if (bArr.length == 32) {
            this.key = bArr;
            return;
        }
        throw new AssertionError();
    }

    @Override // org.whispersystems.signalservice.api.storage.StorageCipherKey
    public byte[] serialize() {
        return (byte[]) this.key.clone();
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != StorageManifestKey.class) {
            return false;
        }
        return Arrays.equals(((StorageManifestKey) obj).key, this.key);
    }

    public int hashCode() {
        return Arrays.hashCode(this.key);
    }
}
