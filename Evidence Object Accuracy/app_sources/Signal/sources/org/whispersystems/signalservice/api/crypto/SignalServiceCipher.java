package org.whispersystems.signalservice.api.crypto;

import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import java.util.Collections;
import java.util.List;
import org.signal.libsignal.metadata.InvalidMetadataMessageException;
import org.signal.libsignal.metadata.InvalidMetadataVersionException;
import org.signal.libsignal.metadata.ProtocolDuplicateMessageException;
import org.signal.libsignal.metadata.ProtocolInvalidKeyException;
import org.signal.libsignal.metadata.ProtocolInvalidKeyIdException;
import org.signal.libsignal.metadata.ProtocolInvalidMessageException;
import org.signal.libsignal.metadata.ProtocolInvalidVersionException;
import org.signal.libsignal.metadata.ProtocolLegacyMessageException;
import org.signal.libsignal.metadata.ProtocolNoSessionException;
import org.signal.libsignal.metadata.ProtocolUntrustedIdentityException;
import org.signal.libsignal.metadata.SealedSessionCipher;
import org.signal.libsignal.metadata.SelfSendException;
import org.signal.libsignal.metadata.certificate.CertificateValidator;
import org.signal.libsignal.metadata.certificate.SenderCertificate;
import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.DuplicateMessageException;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidRegistrationIdException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SessionCipher;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.signal.libsignal.protocol.groups.GroupCipher;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.message.PlaintextContent;
import org.signal.libsignal.protocol.message.PreKeySignalMessage;
import org.signal.libsignal.protocol.message.SignalMessage;
import org.whispersystems.signalservice.api.InvalidMessageStructureException;
import org.whispersystems.signalservice.api.SignalServiceAccountDataStore;
import org.whispersystems.signalservice.api.SignalSessionLock;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.messages.SignalServiceMetadata;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.push.OutgoingPushMessage;
import org.whispersystems.signalservice.internal.push.PushTransportDetails;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.push.UnsupportedDataMessageException;
import org.whispersystems.signalservice.internal.serialize.SignalServiceAddressProtobufSerializer;
import org.whispersystems.signalservice.internal.serialize.SignalServiceMetadataProtobufSerializer;
import org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProto;

/* loaded from: classes5.dex */
public class SignalServiceCipher {
    private static final String TAG;
    private final CertificateValidator certificateValidator;
    private final SignalServiceAddress localAddress;
    private final int localDeviceId;
    private final SignalSessionLock sessionLock;
    private final SignalServiceAccountDataStore signalProtocolStore;

    public SignalServiceCipher(SignalServiceAddress signalServiceAddress, int i, SignalServiceAccountDataStore signalServiceAccountDataStore, SignalSessionLock signalSessionLock, CertificateValidator certificateValidator) {
        this.signalProtocolStore = signalServiceAccountDataStore;
        this.sessionLock = signalSessionLock;
        this.localAddress = signalServiceAddress;
        this.localDeviceId = i;
        this.certificateValidator = certificateValidator;
    }

    public byte[] encryptForGroup(DistributionId distributionId, List<SignalProtocolAddress> list, SenderCertificate senderCertificate, byte[] bArr, ContentHint contentHint, Optional<byte[]> optional) throws NoSessionException, UntrustedIdentityException, InvalidKeyException, InvalidRegistrationIdException {
        PushTransportDetails pushTransportDetails = new PushTransportDetails();
        return new SignalSealedSessionCipher(this.sessionLock, new SealedSessionCipher(this.signalProtocolStore, this.localAddress.getServiceId().uuid(), this.localAddress.getNumber().orElse(null), this.localDeviceId)).multiRecipientEncrypt(list, new UnidentifiedSenderMessageContent(new SignalGroupCipher(this.sessionLock, new GroupCipher(this.signalProtocolStore, new SignalProtocolAddress(this.localAddress.getIdentifier(), this.localDeviceId))).encrypt(distributionId.asUuid(), pushTransportDetails.getPaddedMessageBody(bArr)), senderCertificate, contentHint.getType(), optional));
    }

    public OutgoingPushMessage encrypt(SignalProtocolAddress signalProtocolAddress, Optional<UnidentifiedAccess> optional, EnvelopeContent envelopeContent) throws UntrustedIdentityException, InvalidKeyException {
        if (optional.isPresent()) {
            return envelopeContent.processSealedSender(new SignalSessionCipher(this.sessionLock, new SessionCipher(this.signalProtocolStore, signalProtocolAddress)), new SignalSealedSessionCipher(this.sessionLock, new SealedSessionCipher(this.signalProtocolStore, this.localAddress.getServiceId().uuid(), this.localAddress.getNumber().orElse(null), this.localDeviceId)), signalProtocolAddress, optional.get().getUnidentifiedCertificate());
        }
        return envelopeContent.processUnsealedSender(new SignalSessionCipher(this.sessionLock, new SessionCipher(this.signalProtocolStore, signalProtocolAddress)), signalProtocolAddress);
    }

    public SignalServiceContent decrypt(SignalServiceEnvelope signalServiceEnvelope) throws InvalidMetadataMessageException, InvalidMetadataVersionException, ProtocolInvalidKeyIdException, ProtocolLegacyMessageException, ProtocolUntrustedIdentityException, ProtocolNoSessionException, ProtocolInvalidVersionException, ProtocolInvalidMessageException, ProtocolInvalidKeyException, ProtocolDuplicateMessageException, SelfSendException, UnsupportedDataMessageException, InvalidMessageStructureException {
        try {
            if (signalServiceEnvelope.hasLegacyMessage()) {
                Plaintext decrypt = decrypt(signalServiceEnvelope, signalServiceEnvelope.getLegacyMessage());
                return SignalServiceContent.createFromProto(SignalServiceContentProto.newBuilder().setLocalAddress(SignalServiceAddressProtobufSerializer.toProtobuf(this.localAddress)).setMetadata(SignalServiceMetadataProtobufSerializer.toProtobuf(decrypt.metadata)).setLegacyDataMessage(SignalServiceProtos.DataMessage.parseFrom(decrypt.getData())).build());
            } else if (!signalServiceEnvelope.hasContent()) {
                return null;
            } else {
                Plaintext decrypt2 = decrypt(signalServiceEnvelope, signalServiceEnvelope.getContent());
                return SignalServiceContent.createFromProto(SignalServiceContentProto.newBuilder().setLocalAddress(SignalServiceAddressProtobufSerializer.toProtobuf(this.localAddress)).setMetadata(SignalServiceMetadataProtobufSerializer.toProtobuf(decrypt2.metadata)).setContent(SignalServiceProtos.Content.parseFrom(decrypt2.getData())).build());
            }
        } catch (InvalidProtocolBufferException e) {
            throw new InvalidMetadataMessageException(e);
        }
    }

    private Plaintext decrypt(SignalServiceEnvelope signalServiceEnvelope, byte[] bArr) throws InvalidMetadataMessageException, InvalidMetadataVersionException, ProtocolDuplicateMessageException, ProtocolUntrustedIdentityException, ProtocolLegacyMessageException, ProtocolInvalidKeyException, ProtocolInvalidVersionException, ProtocolInvalidMessageException, ProtocolInvalidKeyIdException, ProtocolNoSessionException, SelfSendException, InvalidMessageStructureException {
        SignalServiceMetadata signalServiceMetadata;
        byte[] bArr2;
        boolean z;
        try {
            if (!signalServiceEnvelope.hasSourceUuid() && !signalServiceEnvelope.isUnidentifiedSender()) {
                throw new InvalidMessageStructureException("Non-UD envelope is missing a UUID!");
            }
            if (signalServiceEnvelope.isPreKeySignalMessage()) {
                SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(signalServiceEnvelope.getSourceUuid().get(), signalServiceEnvelope.getSourceDevice());
                bArr2 = new SignalSessionCipher(this.sessionLock, new SessionCipher(this.signalProtocolStore, signalProtocolAddress)).decrypt(new PreKeySignalMessage(bArr));
                signalServiceMetadata = new SignalServiceMetadata(signalServiceEnvelope.getSourceAddress(), signalServiceEnvelope.getSourceDevice(), signalServiceEnvelope.getTimestamp(), signalServiceEnvelope.getServerReceivedTimestamp(), signalServiceEnvelope.getServerDeliveredTimestamp(), false, signalServiceEnvelope.getServerGuid(), Optional.empty(), signalServiceEnvelope.getDestinationUuid());
                this.signalProtocolStore.clearSenderKeySharedWith(Collections.singleton(signalProtocolAddress));
            } else if (signalServiceEnvelope.isSignalMessage()) {
                bArr2 = new SignalSessionCipher(this.sessionLock, new SessionCipher(this.signalProtocolStore, new SignalProtocolAddress(signalServiceEnvelope.getSourceUuid().get(), signalServiceEnvelope.getSourceDevice()))).decrypt(new SignalMessage(bArr));
                signalServiceMetadata = new SignalServiceMetadata(signalServiceEnvelope.getSourceAddress(), signalServiceEnvelope.getSourceDevice(), signalServiceEnvelope.getTimestamp(), signalServiceEnvelope.getServerReceivedTimestamp(), signalServiceEnvelope.getServerDeliveredTimestamp(), false, signalServiceEnvelope.getServerGuid(), Optional.empty(), signalServiceEnvelope.getDestinationUuid());
            } else if (signalServiceEnvelope.isPlaintextContent()) {
                bArr2 = new PlaintextContent(bArr).getBody();
                signalServiceMetadata = new SignalServiceMetadata(signalServiceEnvelope.getSourceAddress(), signalServiceEnvelope.getSourceDevice(), signalServiceEnvelope.getTimestamp(), signalServiceEnvelope.getServerReceivedTimestamp(), signalServiceEnvelope.getServerDeliveredTimestamp(), false, signalServiceEnvelope.getServerGuid(), Optional.empty(), signalServiceEnvelope.getDestinationUuid());
            } else if (signalServiceEnvelope.isUnidentifiedSender()) {
                SealedSessionCipher.DecryptionResult decrypt = new SignalSealedSessionCipher(this.sessionLock, new SealedSessionCipher(this.signalProtocolStore, this.localAddress.getServiceId().uuid(), this.localAddress.getNumber().orElse(null), this.localDeviceId)).decrypt(this.certificateValidator, bArr, signalServiceEnvelope.getServerReceivedTimestamp());
                SignalServiceAddress signalServiceAddress = new SignalServiceAddress(ACI.parseOrThrow(decrypt.getSenderUuid()), decrypt.getSenderE164());
                Optional<byte[]> groupId = decrypt.getGroupId();
                if (signalServiceEnvelope.hasSourceUuid()) {
                    String str = TAG;
                    Log.w(str, "[" + signalServiceEnvelope.getTimestamp() + "] Received a UD-encrypted message sent over an identified channel. Marking as needsReceipt=false");
                    z = false;
                } else {
                    z = true;
                }
                if (decrypt.getCiphertextMessageType() == 3) {
                    this.signalProtocolStore.clearSenderKeySharedWith(Collections.singleton(new SignalProtocolAddress(decrypt.getSenderUuid(), decrypt.getDeviceId())));
                }
                byte[] paddedMessage = decrypt.getPaddedMessage();
                signalServiceMetadata = new SignalServiceMetadata(signalServiceAddress, decrypt.getDeviceId(), signalServiceEnvelope.getTimestamp(), signalServiceEnvelope.getServerReceivedTimestamp(), signalServiceEnvelope.getServerDeliveredTimestamp(), z, signalServiceEnvelope.getServerGuid(), groupId, signalServiceEnvelope.getDestinationUuid());
                bArr2 = paddedMessage;
            } else {
                throw new InvalidMetadataMessageException("Unknown type: " + signalServiceEnvelope.getType());
            }
            return new Plaintext(signalServiceMetadata, new PushTransportDetails().getStrippedPaddingMessageBody(bArr2));
        } catch (DuplicateMessageException e) {
            throw new ProtocolDuplicateMessageException(e, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (InvalidKeyException e2) {
            throw new ProtocolInvalidKeyException(e2, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (InvalidKeyIdException e3) {
            throw new ProtocolInvalidKeyIdException(e3, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (InvalidMessageException e4) {
            throw new ProtocolInvalidMessageException(e4, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (InvalidVersionException e5) {
            throw new ProtocolInvalidVersionException(e5, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (LegacyMessageException e6) {
            throw new ProtocolLegacyMessageException(e6, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (NoSessionException e7) {
            throw new ProtocolNoSessionException(e7, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        } catch (UntrustedIdentityException e8) {
            throw new ProtocolUntrustedIdentityException(e8, signalServiceEnvelope.getSourceIdentifier(), signalServiceEnvelope.getSourceDevice());
        }
    }

    /* loaded from: classes5.dex */
    public static class Plaintext {
        private final byte[] data;
        private final SignalServiceMetadata metadata;

        private Plaintext(SignalServiceMetadata signalServiceMetadata, byte[] bArr) {
            this.metadata = signalServiceMetadata;
            this.data = bArr;
        }

        public SignalServiceMetadata getMetadata() {
            return this.metadata;
        }

        public byte[] getData() {
            return this.data;
        }
    }
}
