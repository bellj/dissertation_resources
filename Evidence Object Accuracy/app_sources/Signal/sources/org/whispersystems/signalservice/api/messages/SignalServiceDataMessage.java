package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import java.util.LinkedList;
import java.util.List;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class SignalServiceDataMessage {
    private final Optional<List<SignalServiceAttachment>> attachments;
    private final Optional<String> body;
    private final Optional<List<SharedContact>> contacts;
    private final boolean endSession;
    private final boolean expirationUpdate;
    private final int expiresInSeconds;
    private final Optional<GiftBadge> giftBadge;
    private final Optional<SignalServiceGroupContext> group;
    private final Optional<GroupCallUpdate> groupCallUpdate;
    private final Optional<List<Mention>> mentions;
    private final Optional<Payment> payment;
    private final Optional<List<SignalServicePreview>> previews;
    private final Optional<byte[]> profileKey;
    private final boolean profileKeyUpdate;
    private final Optional<Quote> quote;
    private final Optional<Reaction> reaction;
    private final Optional<RemoteDelete> remoteDelete;
    private final Optional<Sticker> sticker;
    private final Optional<StoryContext> storyContext;
    private final long timestamp;
    private final boolean viewOnce;

    public SignalServiceDataMessage(long j, SignalServiceGroup signalServiceGroup, SignalServiceGroupV2 signalServiceGroupV2, List<SignalServiceAttachment> list, String str, boolean z, int i, boolean z2, byte[] bArr, boolean z3, Quote quote, List<SharedContact> list2, List<SignalServicePreview> list3, List<Mention> list4, Sticker sticker, boolean z4, Reaction reaction, RemoteDelete remoteDelete, GroupCallUpdate groupCallUpdate, Payment payment, StoryContext storyContext, GiftBadge giftBadge) {
        try {
            this.group = SignalServiceGroupContext.createOptional(signalServiceGroup, signalServiceGroupV2);
            this.timestamp = j;
            this.body = OptionalUtil.absentIfEmpty(str);
            this.endSession = z;
            this.expiresInSeconds = i;
            this.expirationUpdate = z2;
            this.profileKey = Optional.ofNullable(bArr);
            this.profileKeyUpdate = z3;
            this.quote = Optional.ofNullable(quote);
            this.sticker = Optional.ofNullable(sticker);
            this.viewOnce = z4;
            this.reaction = Optional.ofNullable(reaction);
            this.remoteDelete = Optional.ofNullable(remoteDelete);
            this.groupCallUpdate = Optional.ofNullable(groupCallUpdate);
            this.payment = Optional.ofNullable(payment);
            this.storyContext = Optional.ofNullable(storyContext);
            this.giftBadge = Optional.ofNullable(giftBadge);
            if (list == null || list.isEmpty()) {
                this.attachments = Optional.empty();
            } else {
                this.attachments = Optional.of(list);
            }
            if (list2 == null || list2.isEmpty()) {
                this.contacts = Optional.empty();
            } else {
                this.contacts = Optional.of(list2);
            }
            if (list3 == null || list3.isEmpty()) {
                this.previews = Optional.empty();
            } else {
                this.previews = Optional.of(list3);
            }
            if (list4 == null || list4.isEmpty()) {
                this.mentions = Optional.empty();
            } else {
                this.mentions = Optional.of(list4);
            }
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public Optional<List<SignalServiceAttachment>> getAttachments() {
        return this.attachments;
    }

    public Optional<String> getBody() {
        return this.body;
    }

    public Optional<SignalServiceGroupContext> getGroupContext() {
        return this.group;
    }

    public boolean isEndSession() {
        return this.endSession;
    }

    public boolean isExpirationUpdate() {
        return this.expirationUpdate;
    }

    public boolean isProfileKeyUpdate() {
        return this.profileKeyUpdate;
    }

    public boolean isGroupV1Update() {
        return this.group.isPresent() && this.group.get().getGroupV1().isPresent() && this.group.get().getGroupV1().get().getType() != SignalServiceGroup.Type.DELIVER;
    }

    public boolean isGroupV2Message() {
        return this.group.isPresent() && this.group.get().getGroupV2().isPresent();
    }

    public boolean isGroupV2Update() {
        return isGroupV2Message() && this.group.get().getGroupV2().get().hasSignedGroupChange() && !hasRenderableContent();
    }

    public boolean isEmptyGroupV2Message() {
        return isGroupV2Message() && !isGroupV2Update() && !hasRenderableContent();
    }

    public boolean hasRenderableContent() {
        return this.attachments.isPresent() || this.body.isPresent() || this.quote.isPresent() || this.contacts.isPresent() || this.previews.isPresent() || this.mentions.isPresent() || this.sticker.isPresent() || this.reaction.isPresent() || this.remoteDelete.isPresent();
    }

    public int getExpiresInSeconds() {
        return this.expiresInSeconds;
    }

    public Optional<byte[]> getProfileKey() {
        return this.profileKey;
    }

    public Optional<Quote> getQuote() {
        return this.quote;
    }

    public Optional<List<SharedContact>> getSharedContacts() {
        return this.contacts;
    }

    public Optional<List<SignalServicePreview>> getPreviews() {
        return this.previews;
    }

    public Optional<List<Mention>> getMentions() {
        return this.mentions;
    }

    public Optional<Sticker> getSticker() {
        return this.sticker;
    }

    public boolean isViewOnce() {
        return this.viewOnce;
    }

    public Optional<Reaction> getReaction() {
        return this.reaction;
    }

    public Optional<RemoteDelete> getRemoteDelete() {
        return this.remoteDelete;
    }

    public Optional<GroupCallUpdate> getGroupCallUpdate() {
        return this.groupCallUpdate;
    }

    public Optional<Payment> getPayment() {
        return this.payment;
    }

    public Optional<StoryContext> getStoryContext() {
        return this.storyContext;
    }

    public Optional<GiftBadge> getGiftBadge() {
        return this.giftBadge;
    }

    public Optional<byte[]> getGroupId() {
        return Optional.ofNullable((!getGroupContext().isPresent() || !getGroupContext().get().getGroupV2().isPresent()) ? null : GroupSecretParams.deriveFromMasterKey(getGroupContext().get().getGroupV2().get().getMasterKey()).getPublicParams().getGroupIdentifier().serialize());
    }

    /* loaded from: classes5.dex */
    public static class Builder {
        private List<SignalServiceAttachment> attachments;
        private String body;
        private boolean endSession;
        private boolean expirationUpdate;
        private int expiresInSeconds;
        private GiftBadge giftBadge;
        private SignalServiceGroup group;
        private GroupCallUpdate groupCallUpdate;
        private SignalServiceGroupV2 groupV2;
        private List<Mention> mentions;
        private Payment payment;
        private List<SignalServicePreview> previews;
        private byte[] profileKey;
        private boolean profileKeyUpdate;
        private Quote quote;
        private Reaction reaction;
        private RemoteDelete remoteDelete;
        private List<SharedContact> sharedContacts;
        private Sticker sticker;
        private StoryContext storyContext;
        private long timestamp;
        private boolean viewOnce;

        private Builder() {
            this.attachments = new LinkedList();
            this.sharedContacts = new LinkedList();
            this.previews = new LinkedList();
            this.mentions = new LinkedList();
        }

        public Builder withTimestamp(long j) {
            this.timestamp = j;
            return this;
        }

        public Builder asGroupMessage(SignalServiceGroup signalServiceGroup) {
            if (this.groupV2 == null) {
                this.group = signalServiceGroup;
                return this;
            }
            throw new AssertionError("Can not contain both V1 and V2 group contexts.");
        }

        public Builder asGroupMessage(SignalServiceGroupV2 signalServiceGroupV2) {
            if (this.group == null) {
                this.groupV2 = signalServiceGroupV2;
                return this;
            }
            throw new AssertionError("Can not contain both V1 and V2 group contexts.");
        }

        public Builder withAttachment(SignalServiceAttachment signalServiceAttachment) {
            this.attachments.add(signalServiceAttachment);
            return this;
        }

        public Builder withAttachments(List<SignalServiceAttachment> list) {
            this.attachments.addAll(list);
            return this;
        }

        public Builder withBody(String str) {
            this.body = str;
            return this;
        }

        public Builder asEndSessionMessage() {
            return asEndSessionMessage(true);
        }

        public Builder asEndSessionMessage(boolean z) {
            this.endSession = z;
            return this;
        }

        public Builder asExpirationUpdate() {
            return asExpirationUpdate(true);
        }

        public Builder asExpirationUpdate(boolean z) {
            this.expirationUpdate = z;
            return this;
        }

        public Builder withExpiration(int i) {
            this.expiresInSeconds = i;
            return this;
        }

        public Builder withProfileKey(byte[] bArr) {
            this.profileKey = bArr;
            return this;
        }

        public Builder asProfileKeyUpdate(boolean z) {
            this.profileKeyUpdate = z;
            return this;
        }

        public Builder withQuote(Quote quote) {
            this.quote = quote;
            return this;
        }

        public Builder withSharedContact(SharedContact sharedContact) {
            this.sharedContacts.add(sharedContact);
            return this;
        }

        public Builder withSharedContacts(List<SharedContact> list) {
            this.sharedContacts.addAll(list);
            return this;
        }

        public Builder withPreviews(List<SignalServicePreview> list) {
            this.previews.addAll(list);
            return this;
        }

        public Builder withMentions(List<Mention> list) {
            this.mentions.addAll(list);
            return this;
        }

        public Builder withSticker(Sticker sticker) {
            this.sticker = sticker;
            return this;
        }

        public Builder withViewOnce(boolean z) {
            this.viewOnce = z;
            return this;
        }

        public Builder withReaction(Reaction reaction) {
            this.reaction = reaction;
            return this;
        }

        public Builder withRemoteDelete(RemoteDelete remoteDelete) {
            this.remoteDelete = remoteDelete;
            return this;
        }

        public Builder withGroupCallUpdate(GroupCallUpdate groupCallUpdate) {
            this.groupCallUpdate = groupCallUpdate;
            return this;
        }

        public Builder withPayment(Payment payment) {
            this.payment = payment;
            return this;
        }

        public Builder withStoryContext(StoryContext storyContext) {
            this.storyContext = storyContext;
            return this;
        }

        public Builder withGiftBadge(GiftBadge giftBadge) {
            this.giftBadge = giftBadge;
            return this;
        }

        public SignalServiceDataMessage build() {
            if (this.timestamp == 0) {
                this.timestamp = System.currentTimeMillis();
            }
            return new SignalServiceDataMessage(this.timestamp, this.group, this.groupV2, this.attachments, this.body, this.endSession, this.expiresInSeconds, this.expirationUpdate, this.profileKey, this.profileKeyUpdate, this.quote, this.sharedContacts, this.previews, this.mentions, this.sticker, this.viewOnce, this.reaction, this.remoteDelete, this.groupCallUpdate, this.payment, this.storyContext, this.giftBadge);
        }
    }

    /* loaded from: classes5.dex */
    public static class Quote {
        private final List<QuotedAttachment> attachments;
        private final ServiceId author;
        private final long id;
        private final List<Mention> mentions;
        private final String text;
        private final Type type;

        public Quote(long j, ServiceId serviceId, String str, List<QuotedAttachment> list, List<Mention> list2, Type type) {
            this.id = j;
            this.author = serviceId;
            this.text = str;
            this.attachments = list;
            this.mentions = list2;
            this.type = type;
        }

        public long getId() {
            return this.id;
        }

        public ServiceId getAuthor() {
            return this.author;
        }

        public String getText() {
            return this.text;
        }

        public List<QuotedAttachment> getAttachments() {
            return this.attachments;
        }

        public List<Mention> getMentions() {
            return this.mentions;
        }

        public Type getType() {
            return this.type;
        }

        /* loaded from: classes5.dex */
        public enum Type {
            NORMAL(SignalServiceProtos.DataMessage.Quote.Type.NORMAL),
            GIFT_BADGE(SignalServiceProtos.DataMessage.Quote.Type.GIFT_BADGE);
            
            private final SignalServiceProtos.DataMessage.Quote.Type protoType;

            Type(SignalServiceProtos.DataMessage.Quote.Type type) {
                this.protoType = type;
            }

            public SignalServiceProtos.DataMessage.Quote.Type getProtoType() {
                return this.protoType;
            }

            public static Type fromProto(SignalServiceProtos.DataMessage.Quote.Type type) {
                Type[] values = values();
                for (Type type2 : values) {
                    if (type2.protoType == type) {
                        return type2;
                    }
                }
                return NORMAL;
            }
        }

        /* loaded from: classes5.dex */
        public static class QuotedAttachment {
            private final String contentType;
            private final String fileName;
            private final SignalServiceAttachment thumbnail;

            public QuotedAttachment(String str, String str2, SignalServiceAttachment signalServiceAttachment) {
                this.contentType = str;
                this.fileName = str2;
                this.thumbnail = signalServiceAttachment;
            }

            public String getContentType() {
                return this.contentType;
            }

            public String getFileName() {
                return this.fileName;
            }

            public SignalServiceAttachment getThumbnail() {
                return this.thumbnail;
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class Sticker {
        private final SignalServiceAttachment attachment;
        private final String emoji;
        private final byte[] packId;
        private final byte[] packKey;
        private final int stickerId;

        public Sticker(byte[] bArr, byte[] bArr2, int i, String str, SignalServiceAttachment signalServiceAttachment) {
            this.packId = bArr;
            this.packKey = bArr2;
            this.stickerId = i;
            this.emoji = str;
            this.attachment = signalServiceAttachment;
        }

        public byte[] getPackId() {
            return this.packId;
        }

        public byte[] getPackKey() {
            return this.packKey;
        }

        public int getStickerId() {
            return this.stickerId;
        }

        public String getEmoji() {
            return this.emoji;
        }

        public SignalServiceAttachment getAttachment() {
            return this.attachment;
        }
    }

    /* loaded from: classes5.dex */
    public static class Reaction {
        private final String emoji;
        private final boolean remove;
        private final ServiceId targetAuthor;
        private final long targetSentTimestamp;

        public Reaction(String str, boolean z, ServiceId serviceId, long j) {
            this.emoji = str;
            this.remove = z;
            this.targetAuthor = serviceId;
            this.targetSentTimestamp = j;
        }

        public String getEmoji() {
            return this.emoji;
        }

        public boolean isRemove() {
            return this.remove;
        }

        public ServiceId getTargetAuthor() {
            return this.targetAuthor;
        }

        public long getTargetSentTimestamp() {
            return this.targetSentTimestamp;
        }
    }

    /* loaded from: classes5.dex */
    public static class RemoteDelete {
        private final long targetSentTimestamp;

        public RemoteDelete(long j) {
            this.targetSentTimestamp = j;
        }

        public long getTargetSentTimestamp() {
            return this.targetSentTimestamp;
        }
    }

    /* loaded from: classes5.dex */
    public static class Mention {
        private final int length;
        private final ServiceId serviceId;
        private final int start;

        public Mention(ServiceId serviceId, int i, int i2) {
            this.serviceId = serviceId;
            this.start = i;
            this.length = i2;
        }

        public ServiceId getServiceId() {
            return this.serviceId;
        }

        public int getStart() {
            return this.start;
        }

        public int getLength() {
            return this.length;
        }
    }

    /* loaded from: classes5.dex */
    public static class GroupCallUpdate {
        private final String eraId;

        public GroupCallUpdate(String str) {
            this.eraId = str;
        }

        public String getEraId() {
            return this.eraId;
        }
    }

    /* loaded from: classes5.dex */
    public static class PaymentNotification {
        private final String note;
        private final byte[] receipt;

        public PaymentNotification(byte[] bArr, String str) {
            this.receipt = bArr;
            this.note = str;
        }

        public byte[] getReceipt() {
            return this.receipt;
        }

        public String getNote() {
            return this.note;
        }
    }

    /* loaded from: classes5.dex */
    public static class Payment {
        private final Optional<PaymentNotification> paymentNotification;

        public Payment(PaymentNotification paymentNotification) {
            this.paymentNotification = Optional.of(paymentNotification);
        }

        public Optional<PaymentNotification> getPaymentNotification() {
            return this.paymentNotification;
        }
    }

    /* loaded from: classes5.dex */
    public static class StoryContext {
        private final ServiceId authorServiceId;
        private final long sentTimestamp;

        public StoryContext(ServiceId serviceId, long j) {
            this.authorServiceId = serviceId;
            this.sentTimestamp = j;
        }

        public ServiceId getAuthorServiceId() {
            return this.authorServiceId;
        }

        public long getSentTimestamp() {
            return this.sentTimestamp;
        }
    }

    /* loaded from: classes5.dex */
    public static class GiftBadge {
        private final ReceiptCredentialPresentation receiptCredentialPresentation;

        public GiftBadge(ReceiptCredentialPresentation receiptCredentialPresentation) {
            this.receiptCredentialPresentation = receiptCredentialPresentation;
        }

        public ReceiptCredentialPresentation getReceiptCredentialPresentation() {
            return this.receiptCredentialPresentation;
        }
    }
}
