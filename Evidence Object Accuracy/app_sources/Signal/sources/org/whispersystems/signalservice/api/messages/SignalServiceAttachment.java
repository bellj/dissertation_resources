package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec;

/* loaded from: classes5.dex */
public abstract class SignalServiceAttachment {
    private final String contentType;

    /* loaded from: classes5.dex */
    public interface ProgressListener {
        void onAttachmentProgress(long j, long j2);
    }

    public abstract boolean isPointer();

    public abstract boolean isStream();

    public SignalServiceAttachment(String str) {
        this.contentType = str;
    }

    public String getContentType() {
        return this.contentType;
    }

    public SignalServiceAttachmentStream asStream() {
        return (SignalServiceAttachmentStream) this;
    }

    public SignalServiceAttachmentPointer asPointer() {
        return (SignalServiceAttachmentPointer) this;
    }

    public static Builder newStreamBuilder() {
        return new Builder();
    }

    public static SignalServiceAttachmentStream emptyStream(String str) {
        return new SignalServiceAttachmentStream(new ByteArrayInputStream(new byte[0]), str, 0, Optional.empty(), false, false, false, null, null);
    }

    /* loaded from: classes5.dex */
    public static class Builder {
        private String blurHash;
        private boolean borderless;
        private CancelationSignal cancelationSignal;
        private String caption;
        private String contentType;
        private String fileName;
        private boolean gif;
        private int height;
        private InputStream inputStream;
        private long length;
        private ProgressListener listener;
        private ResumableUploadSpec resumableUploadSpec;
        private long uploadTimestamp;
        private boolean voiceNote;
        private int width;

        private Builder() {
        }

        public Builder withStream(InputStream inputStream) {
            this.inputStream = inputStream;
            return this;
        }

        public Builder withContentType(String str) {
            this.contentType = str;
            return this;
        }

        public Builder withLength(long j) {
            this.length = j;
            return this;
        }

        public Builder withFileName(String str) {
            this.fileName = str;
            return this;
        }

        public Builder withListener(ProgressListener progressListener) {
            this.listener = progressListener;
            return this;
        }

        public Builder withCancelationSignal(CancelationSignal cancelationSignal) {
            this.cancelationSignal = cancelationSignal;
            return this;
        }

        public Builder withVoiceNote(boolean z) {
            this.voiceNote = z;
            return this;
        }

        public Builder withBorderless(boolean z) {
            this.borderless = z;
            return this;
        }

        public Builder withGif(boolean z) {
            this.gif = z;
            return this;
        }

        public Builder withWidth(int i) {
            this.width = i;
            return this;
        }

        public Builder withHeight(int i) {
            this.height = i;
            return this;
        }

        public Builder withCaption(String str) {
            this.caption = str;
            return this;
        }

        public Builder withBlurHash(String str) {
            this.blurHash = str;
            return this;
        }

        public Builder withUploadTimestamp(long j) {
            this.uploadTimestamp = j;
            return this;
        }

        public Builder withResumableUploadSpec(ResumableUploadSpec resumableUploadSpec) {
            this.resumableUploadSpec = resumableUploadSpec;
            return this;
        }

        public SignalServiceAttachmentStream build() {
            InputStream inputStream = this.inputStream;
            if (inputStream != null) {
                String str = this.contentType;
                if (str != null) {
                    long j = this.length;
                    if (j != 0) {
                        return new SignalServiceAttachmentStream(inputStream, str, j, Optional.ofNullable(this.fileName), this.voiceNote, this.borderless, this.gif, Optional.empty(), this.width, this.height, this.uploadTimestamp, Optional.ofNullable(this.caption), Optional.ofNullable(this.blurHash), this.listener, this.cancelationSignal, Optional.ofNullable(this.resumableUploadSpec));
                    }
                    throw new IllegalArgumentException("No length specified!");
                }
                throw new IllegalArgumentException("No content type specified!");
            }
            throw new IllegalArgumentException("Must specify stream!");
        }
    }
}
