package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;
import org.whispersystems.signalservice.api.storage.StorageKey;

/* loaded from: classes5.dex */
public class KeysMessage {
    private final Optional<StorageKey> storageService;

    public KeysMessage(Optional<StorageKey> optional) {
        this.storageService = optional;
    }

    public Optional<StorageKey> getStorageService() {
        return this.storageService;
    }
}
