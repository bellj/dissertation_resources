package org.whispersystems.signalservice.api.messages.multidevice;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class DeviceGroupsOutputStream extends ChunkedOutputStream {
    public DeviceGroupsOutputStream(OutputStream outputStream) {
        super(outputStream);
    }

    public void write(DeviceGroup deviceGroup) throws IOException {
        writeGroupDetails(deviceGroup);
        writeAvatarImage(deviceGroup);
    }

    public void close() throws IOException {
        this.out.close();
    }

    private void writeAvatarImage(DeviceGroup deviceGroup) throws IOException {
        if (deviceGroup.getAvatar().isPresent()) {
            writeStream(deviceGroup.getAvatar().get().getInputStream());
        }
    }

    private void writeGroupDetails(DeviceGroup deviceGroup) throws IOException {
        SignalServiceProtos.GroupDetails.Builder newBuilder = SignalServiceProtos.GroupDetails.newBuilder();
        newBuilder.setId(ByteString.copyFrom(deviceGroup.getId()));
        if (deviceGroup.getName().isPresent()) {
            newBuilder.setName(deviceGroup.getName().get());
        }
        if (deviceGroup.getAvatar().isPresent()) {
            SignalServiceProtos.GroupDetails.Avatar.Builder newBuilder2 = SignalServiceProtos.GroupDetails.Avatar.newBuilder();
            newBuilder2.setContentType(deviceGroup.getAvatar().get().getContentType());
            newBuilder2.setLength((int) deviceGroup.getAvatar().get().getLength());
            newBuilder.setAvatar(newBuilder2);
        }
        if (deviceGroup.getExpirationTimer().isPresent()) {
            newBuilder.setExpireTimer(deviceGroup.getExpirationTimer().get().intValue());
        }
        if (deviceGroup.getColor().isPresent()) {
            newBuilder.setColor(deviceGroup.getColor().get());
        }
        ArrayList arrayList = new ArrayList(deviceGroup.getMembers().size());
        ArrayList arrayList2 = new ArrayList(deviceGroup.getMembers().size());
        for (SignalServiceAddress signalServiceAddress : deviceGroup.getMembers()) {
            if (signalServiceAddress.getNumber().isPresent()) {
                arrayList2.add(signalServiceAddress.getNumber().get());
                SignalServiceProtos.GroupDetails.Member.Builder newBuilder3 = SignalServiceProtos.GroupDetails.Member.newBuilder();
                newBuilder3.setE164(signalServiceAddress.getNumber().get());
                arrayList.add(newBuilder3.build());
            }
        }
        newBuilder.addAllMembers(arrayList);
        newBuilder.addAllMembersE164(arrayList2);
        newBuilder.setActive(deviceGroup.isActive());
        newBuilder.setBlocked(deviceGroup.isBlocked());
        newBuilder.setArchived(deviceGroup.isArchived());
        if (deviceGroup.getInboxPosition().isPresent()) {
            newBuilder.setInboxPosition(deviceGroup.getInboxPosition().get().intValue());
        }
        byte[] byteArray = ((SignalServiceProtos.GroupDetails) newBuilder.build()).toByteArray();
        writeVarint32(byteArray.length);
        this.out.write(byteArray);
    }
}
