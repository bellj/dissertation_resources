package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class ImpossiblePhoneNumberException extends NonSuccessfulResponseCodeException {
    public ImpossiblePhoneNumberException() {
        super(400);
    }
}
