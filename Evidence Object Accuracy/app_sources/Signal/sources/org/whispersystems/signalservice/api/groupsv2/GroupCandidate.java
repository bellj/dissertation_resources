package org.whispersystems.signalservice.api.groupsv2;

import j$.util.Optional;
import j$.util.function.Function;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.whispersystems.signalservice.api.util.ExpiringProfileCredentialUtil;

/* loaded from: classes5.dex */
public final class GroupCandidate {
    private final Optional<ExpiringProfileKeyCredential> expiringProfileKeyCredential;
    private final UUID uuid;

    public GroupCandidate(UUID uuid, Optional<ExpiringProfileKeyCredential> optional) {
        this.uuid = uuid;
        this.expiringProfileKeyCredential = optional;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public Optional<ExpiringProfileKeyCredential> getExpiringProfileKeyCredential() {
        return this.expiringProfileKeyCredential;
    }

    public ExpiringProfileKeyCredential requireExpiringProfileKeyCredential() {
        if (this.expiringProfileKeyCredential.isPresent()) {
            return this.expiringProfileKeyCredential.get();
        }
        throw new IllegalStateException("no profile key credential");
    }

    public boolean hasValidProfileKeyCredential() {
        return ((Boolean) this.expiringProfileKeyCredential.map(new Function() { // from class: org.whispersystems.signalservice.api.groupsv2.GroupCandidate$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(ExpiringProfileCredentialUtil.isValid((ExpiringProfileKeyCredential) obj));
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Boolean.FALSE)).booleanValue();
    }

    public static Set<GroupCandidate> withoutExpiringProfileKeyCredentials(Set<GroupCandidate> set) {
        HashSet hashSet = new HashSet(set.size());
        for (GroupCandidate groupCandidate : set) {
            hashSet.add(groupCandidate.withoutExpiringProfileKeyCredential());
        }
        return hashSet;
    }

    public GroupCandidate withoutExpiringProfileKeyCredential() {
        return this.expiringProfileKeyCredential.isPresent() ? new GroupCandidate(this.uuid, Optional.empty()) : this;
    }

    public GroupCandidate withExpiringProfileKeyCredential(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        return new GroupCandidate(this.uuid, Optional.of(expiringProfileKeyCredential));
    }

    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == GroupCandidate.class && ((GroupCandidate) obj).uuid == this.uuid) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.uuid.hashCode();
    }
}
