package org.whispersystems.signalservice.api.messages.multidevice;

import org.signal.libsignal.protocol.IdentityKey;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class VerifiedMessage {
    private final SignalServiceAddress destination;
    private final IdentityKey identityKey;
    private final long timestamp;
    private final VerifiedState verified;

    /* loaded from: classes5.dex */
    public enum VerifiedState {
        DEFAULT,
        VERIFIED,
        UNVERIFIED
    }

    public VerifiedMessage(SignalServiceAddress signalServiceAddress, IdentityKey identityKey, VerifiedState verifiedState, long j) {
        this.destination = signalServiceAddress;
        this.identityKey = identityKey;
        this.verified = verifiedState;
        this.timestamp = j;
    }

    public SignalServiceAddress getDestination() {
        return this.destination;
    }

    public IdentityKey getIdentityKey() {
        return this.identityKey;
    }

    public VerifiedState getVerified() {
        return this.verified;
    }

    public long getTimestamp() {
        return this.timestamp;
    }
}
