package org.whispersystems.signalservice.api.push;

import java.util.UUID;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public final class PNI extends ServiceId {
    public static PNI from(UUID uuid) {
        return new PNI(uuid);
    }

    public static PNI parseOrNull(String str) {
        UUID parseOrNull = UuidUtil.parseOrNull(str);
        if (parseOrNull != null) {
            return from(parseOrNull);
        }
        return null;
    }

    public static PNI parseOrThrow(String str) {
        return from(UUID.fromString(str));
    }

    private PNI(UUID uuid) {
        super(uuid);
    }
}
