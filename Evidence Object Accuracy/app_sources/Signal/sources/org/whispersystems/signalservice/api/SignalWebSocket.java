package org.whispersystems.signalservice.api;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import j$.util.Optional;
import java.io.IOException;
import java.util.Objects;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;
import org.whispersystems.signalservice.api.websocket.WebSocketFactory;
import org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException;
import org.whispersystems.signalservice.internal.websocket.WebSocketConnection;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public final class SignalWebSocket {
    private static final String SERVER_DELIVERED_TIMESTAMP_HEADER;
    private static final String TAG;
    private boolean canConnect;
    private WebSocketConnection unidentifiedWebSocket;
    private final BehaviorSubject<WebSocketConnectionState> unidentifiedWebSocketState;
    private CompositeDisposable unidentifiedWebSocketStateDisposable = new CompositeDisposable();
    private WebSocketConnection webSocket;
    private final WebSocketFactory webSocketFactory;
    private final BehaviorSubject<WebSocketConnectionState> webSocketState;
    private CompositeDisposable webSocketStateDisposable = new CompositeDisposable();

    /* loaded from: classes5.dex */
    public interface MessageReceivedCallback {
        void onMessage(SignalServiceEnvelope signalServiceEnvelope);
    }

    public SignalWebSocket(WebSocketFactory webSocketFactory) {
        this.webSocketFactory = webSocketFactory;
        WebSocketConnectionState webSocketConnectionState = WebSocketConnectionState.DISCONNECTED;
        this.webSocketState = BehaviorSubject.createDefault(webSocketConnectionState);
        this.unidentifiedWebSocketState = BehaviorSubject.createDefault(webSocketConnectionState);
    }

    public Observable<WebSocketConnectionState> getWebSocketState() {
        return this.webSocketState;
    }

    public Observable<WebSocketConnectionState> getUnidentifiedWebSocketState() {
        return this.unidentifiedWebSocketState;
    }

    public synchronized void connect() {
        this.canConnect = true;
        try {
            getWebSocket();
            getUnidentifiedWebSocket();
        } catch (WebSocketUnavailableException e) {
            throw new AssertionError(e);
        }
    }

    public synchronized void disconnect() {
        this.canConnect = false;
        disconnectIdentified();
        disconnectUnidentified();
    }

    public synchronized void forceNewWebSockets() {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Forcing new WebSockets  identified: ");
        WebSocketConnection webSocketConnection = this.webSocket;
        sb.append(webSocketConnection != null ? webSocketConnection.getName() : "[null]");
        sb.append(" unidentified: ");
        WebSocketConnection webSocketConnection2 = this.unidentifiedWebSocket;
        sb.append(webSocketConnection2 != null ? webSocketConnection2.getName() : "[null]");
        sb.append(" canConnect: ");
        sb.append(this.canConnect);
        Log.i(str, sb.toString());
        disconnectIdentified();
        disconnectUnidentified();
    }

    private void disconnectIdentified() {
        if (this.webSocket != null) {
            this.webSocketStateDisposable.dispose();
            this.webSocket.disconnect();
            this.webSocket = null;
            if (!this.webSocketState.getValue().isFailure()) {
                this.webSocketState.onNext(WebSocketConnectionState.DISCONNECTED);
            }
        }
    }

    private void disconnectUnidentified() {
        if (this.unidentifiedWebSocket != null) {
            this.unidentifiedWebSocketStateDisposable.dispose();
            this.unidentifiedWebSocket.disconnect();
            this.unidentifiedWebSocket = null;
            if (!this.unidentifiedWebSocketState.getValue().isFailure()) {
                this.unidentifiedWebSocketState.onNext(WebSocketConnectionState.DISCONNECTED);
            }
        }
    }

    private synchronized WebSocketConnection getWebSocket() throws WebSocketUnavailableException {
        if (this.canConnect) {
            WebSocketConnection webSocketConnection = this.webSocket;
            if (webSocketConnection == null || webSocketConnection.isDead()) {
                this.webSocketStateDisposable.dispose();
                this.webSocket = this.webSocketFactory.createWebSocket();
                this.webSocketStateDisposable = new CompositeDisposable();
                Observable<WebSocketConnectionState> observeOn = this.webSocket.connect().subscribeOn(Schedulers.computation()).observeOn(Schedulers.computation());
                BehaviorSubject<WebSocketConnectionState> behaviorSubject = this.webSocketState;
                Objects.requireNonNull(behaviorSubject);
                this.webSocketStateDisposable.add(observeOn.subscribe(new SignalWebSocket$$ExternalSyntheticLambda0(behaviorSubject)));
            }
        } else {
            throw new WebSocketUnavailableException();
        }
        return this.webSocket;
    }

    private synchronized WebSocketConnection getUnidentifiedWebSocket() throws WebSocketUnavailableException {
        if (this.canConnect) {
            WebSocketConnection webSocketConnection = this.unidentifiedWebSocket;
            if (webSocketConnection == null || webSocketConnection.isDead()) {
                this.unidentifiedWebSocketStateDisposable.dispose();
                this.unidentifiedWebSocket = this.webSocketFactory.createUnidentifiedWebSocket();
                this.unidentifiedWebSocketStateDisposable = new CompositeDisposable();
                Observable<WebSocketConnectionState> observeOn = this.unidentifiedWebSocket.connect().subscribeOn(Schedulers.computation()).observeOn(Schedulers.computation());
                BehaviorSubject<WebSocketConnectionState> behaviorSubject = this.unidentifiedWebSocketState;
                Objects.requireNonNull(behaviorSubject);
                this.unidentifiedWebSocketStateDisposable.add(observeOn.subscribe(new SignalWebSocket$$ExternalSyntheticLambda0(behaviorSubject)));
            }
        } else {
            throw new WebSocketUnavailableException();
        }
        return this.unidentifiedWebSocket;
    }

    public synchronized void sendKeepAlive() throws IOException {
        if (this.canConnect) {
            try {
                getWebSocket().sendKeepAlive();
                getUnidentifiedWebSocket().sendKeepAlive();
            } catch (WebSocketUnavailableException e) {
                throw new AssertionError(e);
            }
        }
    }

    public Single<WebsocketResponse> request(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage) {
        try {
            return getWebSocket().sendRequest(webSocketRequestMessage);
        } catch (IOException e) {
            return Single.error(e);
        }
    }

    public Single<WebsocketResponse> request(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage, Optional<UnidentifiedAccess> optional) {
        if (!optional.isPresent()) {
            return request(webSocketRequestMessage);
        }
        WebSocketProtos.WebSocketRequestMessage.Builder newBuilder = WebSocketProtos.WebSocketRequestMessage.newBuilder(webSocketRequestMessage);
        try {
            return getUnidentifiedWebSocket().sendRequest(newBuilder.addHeaders("Unidentified-Access-Key:" + Base64.encodeBytes(optional.get().getUnidentifiedAccessKey())).build()).flatMap(new Function(webSocketRequestMessage) { // from class: org.whispersystems.signalservice.api.SignalWebSocket$$ExternalSyntheticLambda1
                public final /* synthetic */ WebSocketProtos.WebSocketRequestMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return SignalWebSocket.this.lambda$request$0(this.f$1, (WebsocketResponse) obj);
                }
            }).onErrorResumeNext(new Function(webSocketRequestMessage) { // from class: org.whispersystems.signalservice.api.SignalWebSocket$$ExternalSyntheticLambda2
                public final /* synthetic */ WebSocketProtos.WebSocketRequestMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return SignalWebSocket.this.lambda$request$1(this.f$1, (Throwable) obj);
                }
            });
        } catch (IOException e) {
            return Single.error(e);
        }
    }

    public /* synthetic */ SingleSource lambda$request$0(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage, WebsocketResponse websocketResponse) throws Throwable {
        if (websocketResponse.getStatus() == 401) {
            return request(webSocketRequestMessage);
        }
        return Single.just(websocketResponse);
    }

    public /* synthetic */ SingleSource lambda$request$1(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage, Throwable th) throws Throwable {
        return request(webSocketRequestMessage);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0012, code lost:
        r5 = findHeader(r0);
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if (r5.isPresent() == false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        r2 = java.lang.Long.parseLong(r5.get());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0029, code lost:
        org.signal.libsignal.protocol.logging.Log.w(org.whispersystems.signalservice.api.SignalWebSocket.TAG, "Failed to parse X-Signal-Timestamp");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public j$.util.Optional<org.whispersystems.signalservice.api.messages.SignalServiceEnvelope> readOrEmpty(long r5, org.whispersystems.signalservice.api.SignalWebSocket.MessageReceivedCallback r7) throws java.util.concurrent.TimeoutException, org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException, java.io.IOException {
        /*
            r4 = this;
        L_0x0000:
            org.whispersystems.signalservice.internal.websocket.WebSocketConnection r0 = r4.getWebSocket()
            org.whispersystems.signalservice.internal.websocket.WebSocketProtos$WebSocketRequestMessage r0 = r0.readRequest(r5)
            org.whispersystems.signalservice.internal.websocket.WebSocketProtos$WebSocketResponseMessage r1 = createWebSocketResponse(r0)
            boolean r2 = isSignalServiceEnvelope(r0)     // Catch: all -> 0x005f
            if (r2 == 0) goto L_0x004c
            j$.util.Optional r5 = findHeader(r0)     // Catch: all -> 0x005f
            r2 = 0
            boolean r6 = r5.isPresent()     // Catch: all -> 0x005f
            if (r6 == 0) goto L_0x0030
            java.lang.Object r5 = r5.get()     // Catch: NumberFormatException -> 0x0029, all -> 0x005f
            java.lang.String r5 = (java.lang.String) r5     // Catch: NumberFormatException -> 0x0029, all -> 0x005f
            long r2 = java.lang.Long.parseLong(r5)     // Catch: NumberFormatException -> 0x0029, all -> 0x005f
            goto L_0x0030
        L_0x0029:
            java.lang.String r5 = org.whispersystems.signalservice.api.SignalWebSocket.TAG     // Catch: all -> 0x005f
            java.lang.String r6 = "Failed to parse X-Signal-Timestamp"
            org.signal.libsignal.protocol.logging.Log.w(r5, r6)     // Catch: all -> 0x005f
        L_0x0030:
            org.whispersystems.signalservice.api.messages.SignalServiceEnvelope r5 = new org.whispersystems.signalservice.api.messages.SignalServiceEnvelope     // Catch: all -> 0x005f
            com.google.protobuf.ByteString r6 = r0.getBody()     // Catch: all -> 0x005f
            byte[] r6 = r6.toByteArray()     // Catch: all -> 0x005f
            r5.<init>(r6, r2)     // Catch: all -> 0x005f
            r7.onMessage(r5)     // Catch: all -> 0x005f
            j$.util.Optional r5 = j$.util.Optional.of(r5)     // Catch: all -> 0x005f
        L_0x0044:
            org.whispersystems.signalservice.internal.websocket.WebSocketConnection r6 = r4.getWebSocket()
            r6.sendResponse(r1)
            return r5
        L_0x004c:
            boolean r0 = isSocketEmptyRequest(r0)     // Catch: all -> 0x005f
            if (r0 == 0) goto L_0x0057
            j$.util.Optional r5 = j$.util.Optional.empty()     // Catch: all -> 0x005f
            goto L_0x0044
        L_0x0057:
            org.whispersystems.signalservice.internal.websocket.WebSocketConnection r0 = r4.getWebSocket()
            r0.sendResponse(r1)
            goto L_0x0000
        L_0x005f:
            r5 = move-exception
            org.whispersystems.signalservice.internal.websocket.WebSocketConnection r6 = r4.getWebSocket()
            r6.sendResponse(r1)
            goto L_0x0069
        L_0x0068:
            throw r5
        L_0x0069:
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.SignalWebSocket.readOrEmpty(long, org.whispersystems.signalservice.api.SignalWebSocket$MessageReceivedCallback):j$.util.Optional");
    }

    private static boolean isSignalServiceEnvelope(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage) {
        return "PUT".equals(webSocketRequestMessage.getVerb()) && "/api/v1/message".equals(webSocketRequestMessage.getPath());
    }

    private static boolean isSocketEmptyRequest(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage) {
        return "PUT".equals(webSocketRequestMessage.getVerb()) && "/api/v1/queue/empty".equals(webSocketRequestMessage.getPath());
    }

    private static WebSocketProtos.WebSocketResponseMessage createWebSocketResponse(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage) {
        if (isSignalServiceEnvelope(webSocketRequestMessage)) {
            return WebSocketProtos.WebSocketResponseMessage.newBuilder().setId(webSocketRequestMessage.getId()).setStatus(200).setMessage("OK").build();
        }
        return WebSocketProtos.WebSocketResponseMessage.newBuilder().setId(webSocketRequestMessage.getId()).setStatus(400).setMessage("Unknown").build();
    }

    private static Optional<String> findHeader(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage) {
        if (webSocketRequestMessage.getHeadersCount() == 0) {
            return Optional.empty();
        }
        for (String str : webSocketRequestMessage.getHeadersList()) {
            if (str.startsWith(SERVER_DELIVERED_TIMESTAMP_HEADER)) {
                String[] split = str.split(":");
                if (split.length == 2 && split[0].trim().toLowerCase().equals(SERVER_DELIVERED_TIMESTAMP_HEADER.toLowerCase())) {
                    return Optional.of(split[1].trim());
                }
            }
        }
        return Optional.empty();
    }
}
