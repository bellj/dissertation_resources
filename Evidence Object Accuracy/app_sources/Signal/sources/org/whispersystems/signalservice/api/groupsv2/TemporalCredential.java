package org.whispersystems.signalservice.api.groupsv2;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class TemporalCredential {
    @JsonProperty
    private byte[] credential;
    @JsonProperty
    private long redemptionTime;

    public byte[] getCredential() {
        return this.credential;
    }

    public long getRedemptionTime() {
        return this.redemptionTime;
    }
}
