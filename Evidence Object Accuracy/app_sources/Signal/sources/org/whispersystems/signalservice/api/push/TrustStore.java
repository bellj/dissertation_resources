package org.whispersystems.signalservice.api.push;

import java.io.InputStream;

/* loaded from: classes5.dex */
public interface TrustStore {
    InputStream getKeyStoreInputStream();

    String getKeyStorePassword();
}
