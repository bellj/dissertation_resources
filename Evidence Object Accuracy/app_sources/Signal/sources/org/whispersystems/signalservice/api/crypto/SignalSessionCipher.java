package org.whispersystems.signalservice.api.crypto;

import org.signal.libsignal.protocol.DuplicateMessageException;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SessionCipher;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.signal.libsignal.protocol.message.PreKeySignalMessage;
import org.signal.libsignal.protocol.message.SignalMessage;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes5.dex */
public class SignalSessionCipher {
    private final SessionCipher cipher;
    private final SignalSessionLock lock;

    public SignalSessionCipher(SignalSessionLock signalSessionLock, SessionCipher sessionCipher) {
        this.lock = signalSessionLock;
        this.cipher = sessionCipher;
    }

    public CiphertextMessage encrypt(byte[] bArr) throws UntrustedIdentityException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            CiphertextMessage encrypt = this.cipher.encrypt(bArr);
            if (acquire != null) {
                acquire.close();
            }
            return encrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public byte[] decrypt(PreKeySignalMessage preKeySignalMessage) throws DuplicateMessageException, LegacyMessageException, InvalidMessageException, InvalidKeyIdException, InvalidKeyException, UntrustedIdentityException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            byte[] decrypt = this.cipher.decrypt(preKeySignalMessage);
            if (acquire != null) {
                acquire.close();
            }
            return decrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public byte[] decrypt(SignalMessage signalMessage) throws InvalidMessageException, InvalidVersionException, DuplicateMessageException, LegacyMessageException, NoSessionException, UntrustedIdentityException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            byte[] decrypt = this.cipher.decrypt(signalMessage);
            if (acquire != null) {
                acquire.close();
            }
            return decrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public int getRemoteRegistrationId() {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            int remoteRegistrationId = this.cipher.getRemoteRegistrationId();
            if (acquire != null) {
                acquire.close();
            }
            return remoteRegistrationId;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public int getSessionVersion() {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            int sessionVersion = this.cipher.getSessionVersion();
            if (acquire != null) {
                acquire.close();
            }
            return sessionVersion;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
