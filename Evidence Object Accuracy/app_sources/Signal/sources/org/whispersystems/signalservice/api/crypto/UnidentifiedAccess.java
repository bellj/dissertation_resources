package org.whispersystems.signalservice.api.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.metadata.certificate.InvalidCertificateException;
import org.signal.libsignal.metadata.certificate.SenderCertificate;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;

/* loaded from: classes5.dex */
public class UnidentifiedAccess {
    private final byte[] unidentifiedAccessKey;
    private final SenderCertificate unidentifiedCertificate;

    public UnidentifiedAccess(byte[] bArr, byte[] bArr2) throws InvalidCertificateException {
        this.unidentifiedAccessKey = bArr;
        this.unidentifiedCertificate = new SenderCertificate(bArr2);
    }

    public byte[] getUnidentifiedAccessKey() {
        return this.unidentifiedAccessKey;
    }

    public SenderCertificate getUnidentifiedCertificate() {
        return this.unidentifiedCertificate;
    }

    public static byte[] deriveAccessKeyFrom(ProfileKey profileKey) {
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(1, new SecretKeySpec(profileKey.serialize(), "AES"), new GCMParameterSpec(128, new byte[12]));
            return ByteUtil.trim(instance.doFinal(new byte[16]), 16);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }
}
