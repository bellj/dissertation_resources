package org.whispersystems.signalservice.api;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.api.account.AccountAttributes;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.api.crypto.ProfileCipher;
import org.whispersystems.signalservice.api.crypto.ProfileCipherOutputStream;
import org.whispersystems.signalservice.api.groupsv2.ClientZkOperations;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Api;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Operations;
import org.whispersystems.signalservice.api.messages.calls.TurnServerInfo;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceInfo;
import org.whispersystems.signalservice.api.messages.multidevice.VerifyDeviceResponse;
import org.whispersystems.signalservice.api.payments.CurrencyConversions;
import org.whispersystems.signalservice.api.profiles.AvatarUploadParams;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfileWrite;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.ServiceIdType;
import org.whispersystems.signalservice.api.push.SignedPreKeyEntity;
import org.whispersystems.signalservice.api.push.exceptions.NoContentException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.services.CdsiV2Service;
import org.whispersystems.signalservice.api.storage.SignalStorageCipher;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.SignalStorageModels;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.storage.StorageKey;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.api.util.StreamDetails;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.contacts.crypto.ContactDiscoveryCipher;
import org.whispersystems.signalservice.internal.contacts.crypto.Quote;
import org.whispersystems.signalservice.internal.contacts.crypto.RemoteAttestation;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedQuoteException;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;
import org.whispersystems.signalservice.internal.crypto.PrimaryProvisioningCipher;
import org.whispersystems.signalservice.internal.push.AuthCredentials;
import org.whispersystems.signalservice.internal.push.CdsiAuthResponse;
import org.whispersystems.signalservice.internal.push.ProfileAvatarData;
import org.whispersystems.signalservice.internal.push.ProvisioningProtos;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.RemoteAttestationUtil;
import org.whispersystems.signalservice.internal.push.RemoteConfigResponse;
import org.whispersystems.signalservice.internal.push.RequestVerificationCodeResponse;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;
import org.whispersystems.signalservice.internal.push.WhoAmIResponse;
import org.whispersystems.signalservice.internal.push.http.ProfileCipherOutputStreamFactory;
import org.whispersystems.signalservice.internal.storage.protos.ManifestRecord;
import org.whispersystems.signalservice.internal.storage.protos.ReadOperation;
import org.whispersystems.signalservice.internal.storage.protos.StorageItem;
import org.whispersystems.signalservice.internal.storage.protos.StorageManifest;
import org.whispersystems.signalservice.internal.storage.protos.WriteOperation;
import org.whispersystems.signalservice.internal.util.StaticCredentialsProvider;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public class SignalServiceAccountManager {
    private static final int STORAGE_READ_MAX_ITEMS;
    private static final String TAG;
    private final SignalServiceConfiguration configuration;
    private final CredentialsProvider credentials;
    private final GroupsV2Operations groupsV2Operations;
    private final PushServiceSocket pushServiceSocket;
    private final String userAgent;

    public SignalServiceAccountManager(SignalServiceConfiguration signalServiceConfiguration, ACI aci, PNI pni, String str, int i, String str2, String str3, boolean z, int i2) {
        this(signalServiceConfiguration, new StaticCredentialsProvider(aci, pni, str, i, str2), str3, new GroupsV2Operations(ClientZkOperations.create(signalServiceConfiguration), i2), z);
    }

    public SignalServiceAccountManager(SignalServiceConfiguration signalServiceConfiguration, CredentialsProvider credentialsProvider, String str, GroupsV2Operations groupsV2Operations, boolean z) {
        this.groupsV2Operations = groupsV2Operations;
        this.pushServiceSocket = new PushServiceSocket(signalServiceConfiguration, credentialsProvider, str, groupsV2Operations.getProfileOperations(), z);
        this.credentials = credentialsProvider;
        this.userAgent = str;
        this.configuration = signalServiceConfiguration;
    }

    public byte[] getSenderCertificate() throws IOException {
        return this.pushServiceSocket.getSenderCertificate();
    }

    public byte[] getSenderCertificateForPhoneNumberPrivacy() throws IOException {
        return this.pushServiceSocket.getUuidOnlySenderCertificate();
    }

    public void removeRegistrationLockV1() throws IOException {
        this.pushServiceSocket.removeRegistrationLockV1();
    }

    public WhoAmIResponse getWhoAmI() throws IOException {
        return this.pushServiceSocket.getWhoAmI();
    }

    public KeyBackupService getKeyBackupService(KeyStore keyStore, String str, byte[] bArr, String str2, int i) {
        return new KeyBackupService(keyStore, str, bArr, str2, this.pushServiceSocket, i);
    }

    public void setGcmId(Optional<String> optional) throws IOException {
        if (optional.isPresent()) {
            this.pushServiceSocket.registerGcmId(optional.get());
        } else {
            this.pushServiceSocket.unregisterGcmId();
        }
    }

    public void requestRegistrationPushChallenge(String str, String str2) throws IOException {
        this.pushServiceSocket.requestPushChallenge(str, str2);
    }

    public ServiceResponse<RequestVerificationCodeResponse> requestSmsVerificationCode(boolean z, Optional<String> optional, Optional<String> optional2, Optional<String> optional3) {
        try {
            this.pushServiceSocket.requestSmsVerificationCode(z, optional, optional2);
            return ServiceResponse.forResult(new RequestVerificationCodeResponse(optional3), 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    public ServiceResponse<RequestVerificationCodeResponse> requestVoiceVerificationCode(Locale locale, Optional<String> optional, Optional<String> optional2, Optional<String> optional3) {
        try {
            this.pushServiceSocket.requestVoiceVerificationCode(locale, optional, optional2);
            return ServiceResponse.forResult(new RequestVerificationCodeResponse(optional3), 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    public ServiceResponse<VerifyAccountResponse> verifyAccount(String str, int i, boolean z, byte[] bArr, boolean z2, AccountAttributes.Capabilities capabilities, boolean z3) {
        try {
            return ServiceResponse.forResult(this.pushServiceSocket.verifyAccountCode(str, null, i, z, null, null, bArr, z2, capabilities, z3), 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    public ServiceResponse<VerifyAccountResponse> verifyAccountWithRegistrationLockPin(String str, int i, boolean z, String str2, byte[] bArr, boolean z2, AccountAttributes.Capabilities capabilities, boolean z3) {
        try {
            return ServiceResponse.forResult(this.pushServiceSocket.verifyAccountCode(str, null, i, z, null, str2, bArr, z2, capabilities, z3), 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    public VerifyDeviceResponse verifySecondaryDevice(String str, int i, boolean z, byte[] bArr, boolean z2, AccountAttributes.Capabilities capabilities, boolean z3, byte[] bArr2) throws IOException {
        return this.pushServiceSocket.verifySecondaryDevice(str, new AccountAttributes(null, i, z, null, null, bArr, z2, capabilities, z3, Base64.encodeBytes(bArr2)));
    }

    public ServiceResponse<VerifyAccountResponse> changeNumber(String str, String str2, String str3) {
        try {
            return ServiceResponse.forResult(this.pushServiceSocket.changeNumber(str, str2, str3), 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    public void setAccountAttributes(String str, int i, boolean z, String str2, String str3, byte[] bArr, boolean z2, AccountAttributes.Capabilities capabilities, boolean z3, byte[] bArr2) throws IOException {
        this.pushServiceSocket.setAccountAttributes(str, i, z, str2, str3, bArr, z2, capabilities, z3, bArr2);
    }

    public void setPreKeys(ServiceIdType serviceIdType, IdentityKey identityKey, SignedPreKeyRecord signedPreKeyRecord, List<PreKeyRecord> list) throws IOException {
        this.pushServiceSocket.registerPreKeys(serviceIdType, identityKey, signedPreKeyRecord, list);
    }

    public int getPreKeysCount(ServiceIdType serviceIdType) throws IOException {
        return this.pushServiceSocket.getAvailablePreKeys(serviceIdType);
    }

    public void setSignedPreKey(ServiceIdType serviceIdType, SignedPreKeyRecord signedPreKeyRecord) throws IOException {
        this.pushServiceSocket.setCurrentSignedPreKey(serviceIdType, signedPreKeyRecord);
    }

    public SignedPreKeyEntity getSignedPreKey(ServiceIdType serviceIdType) throws IOException {
        return this.pushServiceSocket.getCurrentSignedPreKey(serviceIdType);
    }

    public boolean isIdentifierRegistered(ServiceId serviceId) throws IOException {
        return this.pushServiceSocket.isIdentifierRegistered(serviceId);
    }

    public Map<String, ACI> getRegisteredUsers(KeyStore keyStore, Set<String> set, String str) throws IOException, Quote.InvalidQuoteFormatException, UnauthenticatedQuoteException, SignatureException, UnauthenticatedResponseException, InvalidKeyException {
        if (set.isEmpty()) {
            return Collections.emptyMap();
        }
        try {
            String contactDiscoveryAuthorization = this.pushServiceSocket.getContactDiscoveryAuthorization();
            Map<String, RemoteAttestation> andVerifyMultiRemoteAttestation = RemoteAttestationUtil.getAndVerifyMultiRemoteAttestation(this.pushServiceSocket, PushServiceSocket.ClientSet.ContactDiscovery, keyStore, str, str, contactDiscoveryAuthorization);
            ArrayList<String> arrayList = new ArrayList(set.size());
            for (String str2 : set) {
                arrayList.add(str2.substring(1));
            }
            List<String> cookies = andVerifyMultiRemoteAttestation.values().iterator().next().getCookies();
            byte[] discoveryResponseData = ContactDiscoveryCipher.getDiscoveryResponseData(this.pushServiceSocket.getContactDiscoveryRegisteredUsers(contactDiscoveryAuthorization, ContactDiscoveryCipher.createDiscoveryRequest(arrayList, andVerifyMultiRemoteAttestation), cookies, str), andVerifyMultiRemoteAttestation.values());
            HashMap hashMap = new HashMap(arrayList.size());
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(discoveryResponseData));
            for (String str3 : arrayList) {
                long readLong = dataInputStream.readLong();
                long readLong2 = dataInputStream.readLong();
                if (readLong != 0 || readLong2 != 0) {
                    hashMap.put('+' + str3, ACI.from(new UUID(readLong, readLong2)));
                }
            }
            return hashMap;
        } catch (InvalidCiphertextException e) {
            throw new UnauthenticatedResponseException(e);
        }
    }

    public CdsiV2Service.Response getRegisteredUsersWithCdsi(Set<String> set, Set<String> set2, Map<ServiceId, ProfileKey> map, Optional<byte[]> optional, String str, Consumer<byte[]> consumer) throws IOException {
        CdsiAuthResponse cdsiAuth = this.pushServiceSocket.getCdsiAuth();
        try {
            ServiceResponse<CdsiV2Service.Response> blockingGet = new CdsiV2Service(this.configuration, str).getRegisteredUsers(cdsiAuth.getUsername(), cdsiAuth.getPassword(), new CdsiV2Service.Request(set, set2, map, optional), consumer).blockingGet();
            if (blockingGet.getResult().isPresent()) {
                return blockingGet.getResult().get();
            }
            if (blockingGet.getApplicationError().isPresent()) {
                throw new IOException(blockingGet.getApplicationError().get());
            } else if (blockingGet.getExecutionError().isPresent()) {
                throw new IOException(blockingGet.getExecutionError().get());
            } else {
                throw new IOException("Missing result!");
            }
        } catch (Exception e) {
            throw new RuntimeException("Unexpected exception when retrieving registered users!", e);
        }
    }

    public Optional<SignalStorageManifest> getStorageManifest(StorageKey storageKey) throws IOException {
        try {
            return Optional.of(SignalStorageModels.remoteToLocalStorageManifest(this.pushServiceSocket.getStorageManifest(this.pushServiceSocket.getStorageAuth()), storageKey));
        } catch (InvalidKeyException | NotFoundException e) {
            Log.w(TAG, "Error while fetching manifest.", e);
            return Optional.empty();
        }
    }

    public long getStorageManifestVersion() throws IOException {
        try {
            return this.pushServiceSocket.getStorageManifest(this.pushServiceSocket.getStorageAuth()).getVersion();
        } catch (NotFoundException unused) {
            return 0;
        }
    }

    public Optional<SignalStorageManifest> getStorageManifestIfDifferentVersion(StorageKey storageKey, long j) throws IOException, InvalidKeyException {
        try {
            StorageManifest storageManifestIfDifferentVersion = this.pushServiceSocket.getStorageManifestIfDifferentVersion(this.pushServiceSocket.getStorageAuth(), j);
            if (!storageManifestIfDifferentVersion.getValue().isEmpty()) {
                return Optional.of(SignalStorageModels.remoteToLocalStorageManifest(storageManifestIfDifferentVersion, storageKey));
            }
            Log.w(TAG, "Got an empty storage manifest!");
            return Optional.empty();
        } catch (NoContentException unused) {
            return Optional.empty();
        }
    }

    public List<SignalStorageRecord> readStorageRecords(StorageKey storageKey, List<StorageId> list) throws IOException, InvalidKeyException {
        if (list.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        LinkedList<ReadOperation> linkedList = new LinkedList();
        ReadOperation.Builder newBuilder = ReadOperation.newBuilder();
        for (StorageId storageId : list) {
            hashMap.put(ByteString.copyFrom(storageId.getRaw()), Integer.valueOf(storageId.getType()));
            if (newBuilder.getReadKeyCount() >= 1000) {
                Log.i(TAG, "Going over max read items. Starting a new read operation.");
                linkedList.add(newBuilder.build());
                newBuilder = ReadOperation.newBuilder();
            }
            if (StorageId.isKnownType(storageId.getType())) {
                newBuilder.addReadKey(ByteString.copyFrom(storageId.getRaw()));
            } else {
                arrayList.add(SignalStorageRecord.forUnknown(storageId));
            }
        }
        if (newBuilder.getReadKeyCount() > 0) {
            linkedList.add(newBuilder.build());
        }
        String str = TAG;
        Log.i(str, "Reading " + list.size() + " items split over " + linkedList.size() + " page(s).");
        String storageAuth = this.pushServiceSocket.getStorageAuth();
        for (ReadOperation readOperation : linkedList) {
            for (StorageItem storageItem : this.pushServiceSocket.readStorageItems(storageAuth, readOperation).getItemsList()) {
                Integer num = (Integer) hashMap.get(storageItem.getKey());
                if (num != null) {
                    arrayList.add(SignalStorageModels.remoteToLocalStorageRecord(storageItem, num.intValue(), storageKey));
                } else {
                    Log.w(TAG, "No type found! Skipping.");
                }
            }
        }
        return arrayList;
    }

    public Optional<SignalStorageManifest> resetStorageRecords(StorageKey storageKey, SignalStorageManifest signalStorageManifest, List<SignalStorageRecord> list) throws IOException, InvalidKeyException {
        return writeStorageRecords(storageKey, signalStorageManifest, list, Collections.emptyList(), true);
    }

    public Optional<SignalStorageManifest> writeStorageRecords(StorageKey storageKey, SignalStorageManifest signalStorageManifest, List<SignalStorageRecord> list, List<byte[]> list2) throws IOException, InvalidKeyException {
        return writeStorageRecords(storageKey, signalStorageManifest, list, list2, false);
    }

    private Optional<SignalStorageManifest> writeStorageRecords(StorageKey storageKey, SignalStorageManifest signalStorageManifest, List<SignalStorageRecord> list, List<byte[]> list2, boolean z) throws IOException, InvalidKeyException {
        ManifestRecord.Builder version = ManifestRecord.newBuilder().setVersion(signalStorageManifest.getVersion());
        for (StorageId storageId : signalStorageManifest.getStorageIds()) {
            version.addIdentifiers(ManifestRecord.Identifier.newBuilder().setRaw(ByteString.copyFrom(storageId.getRaw())).setTypeValue(storageId.getType()).build());
        }
        String storageAuth = this.pushServiceSocket.getStorageAuth();
        byte[] encrypt = SignalStorageCipher.encrypt(storageKey.deriveManifestKey(signalStorageManifest.getVersion()), version.build().toByteArray());
        WriteOperation.Builder manifest = WriteOperation.newBuilder().setManifest(StorageManifest.newBuilder().setVersion(signalStorageManifest.getVersion()).setValue(ByteString.copyFrom(encrypt)).build());
        for (SignalStorageRecord signalStorageRecord : list) {
            manifest.addInsertItem(SignalStorageModels.localToRemoteStorageRecord(signalStorageRecord, storageKey));
        }
        if (z) {
            manifest.setClearAll(true);
        } else {
            for (byte[] bArr : list2) {
                manifest.addDeleteKey(ByteString.copyFrom(bArr));
            }
        }
        Optional<StorageManifest> writeStorageContacts = this.pushServiceSocket.writeStorageContacts(storageAuth, manifest.build());
        if (!writeStorageContacts.isPresent()) {
            return Optional.empty();
        }
        ManifestRecord parseFrom = ManifestRecord.parseFrom(SignalStorageCipher.decrypt(storageKey.deriveManifestKey(writeStorageContacts.get().getVersion()), writeStorageContacts.get().getValue().toByteArray()));
        ArrayList arrayList = new ArrayList(parseFrom.getIdentifiersCount());
        for (ManifestRecord.Identifier identifier : parseFrom.getIdentifiersList()) {
            arrayList.add(StorageId.forType(identifier.getRaw().toByteArray(), identifier.getTypeValue()));
        }
        return Optional.of(new SignalStorageManifest(parseFrom.getVersion(), arrayList));
    }

    public Map<String, Object> getRemoteConfig() throws IOException {
        RemoteConfigResponse remoteConfig = this.pushServiceSocket.getRemoteConfig();
        HashMap hashMap = new HashMap();
        for (RemoteConfigResponse.Config config : remoteConfig.getConfig()) {
            hashMap.put(config.getName(), config.getValue() != null ? config.getValue() : Boolean.valueOf(config.isEnabled()));
        }
        return hashMap;
    }

    public String getNewDeviceVerificationCode() throws IOException {
        return this.pushServiceSocket.getNewDeviceVerificationCode();
    }

    public void addDevice(String str, ECPublicKey eCPublicKey, IdentityKeyPair identityKeyPair, IdentityKeyPair identityKeyPair2, ProfileKey profileKey, String str2) throws InvalidKeyException, IOException {
        String e164 = this.credentials.getE164();
        ACI aci = this.credentials.getAci();
        PNI pni = this.credentials.getPni();
        boolean z = false;
        Preconditions.checkArgument(e164 != null, "Missing e164!");
        Preconditions.checkArgument(aci != null, "Missing ACI!");
        if (pni != null) {
            z = true;
        }
        Preconditions.checkArgument(z, "Missing PNI!");
        this.pushServiceSocket.sendProvisioningMessage(str, new PrimaryProvisioningCipher(eCPublicKey).encrypt(ProvisioningProtos.ProvisionMessage.newBuilder().setAciIdentityKeyPublic(ByteString.copyFrom(identityKeyPair.getPublicKey().serialize())).setAciIdentityKeyPrivate(ByteString.copyFrom(identityKeyPair.getPrivateKey().serialize())).setPniIdentityKeyPublic(ByteString.copyFrom(identityKeyPair2.getPublicKey().serialize())).setPniIdentityKeyPrivate(ByteString.copyFrom(identityKeyPair2.getPrivateKey().serialize())).setAci(aci.toString()).setPni(pni.toString()).setNumber(e164).setProfileKey(ByteString.copyFrom(profileKey.serialize())).setProvisioningCode(str2).setProvisioningVersion(1).build()));
    }

    public List<DeviceInfo> getDevices() throws IOException {
        return this.pushServiceSocket.getDevices();
    }

    public void removeDevice(long j) throws IOException {
        this.pushServiceSocket.removeDevice(j);
    }

    public TurnServerInfo getTurnServerInfo() throws IOException {
        return this.pushServiceSocket.getTurnServerInfo();
    }

    public void checkNetworkConnection() throws IOException {
        this.pushServiceSocket.pingStorageService();
    }

    public CurrencyConversions getCurrencyConversions() throws IOException {
        return this.pushServiceSocket.getCurrencyConversions();
    }

    public void reportSpam(ServiceId serviceId, String str) throws IOException {
        this.pushServiceSocket.reportSpam(serviceId, str);
    }

    public Optional<String> setVersionedProfile(ACI aci, ProfileKey profileKey, String str, String str2, String str3, Optional<SignalServiceProtos.PaymentAddress> optional, AvatarUploadParams avatarUploadParams, List<String> list) throws IOException {
        String str4 = str == null ? "" : str;
        ProfileCipher profileCipher = new ProfileCipher(profileKey);
        byte[] encryptString = profileCipher.encryptString(str4, ProfileCipher.getTargetNameLength(str4));
        byte[] encryptString2 = profileCipher.encryptString(str2, ProfileCipher.getTargetAboutLength(str2));
        byte[] encryptString3 = profileCipher.encryptString(str3, 32);
        ProfileAvatarData profileAvatarData = null;
        byte[] bArr = (byte[]) optional.map(new Function() { // from class: org.whispersystems.signalservice.api.SignalServiceAccountManager$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SignalServiceAccountManager.lambda$setVersionedProfile$0(ProfileCipher.this, (SignalServiceProtos.PaymentAddress) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null);
        StreamDetails streamDetails = avatarUploadParams.stream;
        if (streamDetails != null && !avatarUploadParams.keepTheSame) {
            profileAvatarData = new ProfileAvatarData(streamDetails.getStream(), ProfileCipherOutputStream.getCiphertextLength(avatarUploadParams.stream.getLength()), avatarUploadParams.stream.getContentType(), new ProfileCipherOutputStreamFactory(profileKey));
        }
        return this.pushServiceSocket.writeProfile(new SignalServiceProfileWrite(profileKey.getProfileKeyVersion(aci.uuid()).serialize(), encryptString, encryptString2, encryptString3, bArr, avatarUploadParams.hasAvatar, avatarUploadParams.keepTheSame, profileKey.getCommitment(aci.uuid()).serialize(), list), profileAvatarData);
    }

    public static /* synthetic */ byte[] lambda$setVersionedProfile$0(ProfileCipher profileCipher, SignalServiceProtos.PaymentAddress paymentAddress) {
        return profileCipher.encryptWithLength(paymentAddress.toByteArray(), ProfileCipher.PAYMENTS_ADDRESS_CONTENT_SIZE);
    }

    public Optional<ExpiringProfileKeyCredential> resolveProfileKeyCredential(ServiceId serviceId, ProfileKey profileKey, Locale locale) throws NonSuccessfulResponseCodeException, PushNetworkException {
        Exception e;
        try {
            return this.pushServiceSocket.retrieveVersionedProfileAndCredential(serviceId.uuid(), profileKey, Optional.empty(), locale).get(10, TimeUnit.SECONDS).getExpiringProfileKeyCredential();
        } catch (InterruptedException e2) {
            e = e2;
            throw new PushNetworkException(e);
        } catch (ExecutionException e3) {
            if (e3.getCause() instanceof NonSuccessfulResponseCodeException) {
                throw ((NonSuccessfulResponseCodeException) e3.getCause());
            } else if (e3.getCause() instanceof PushNetworkException) {
                throw ((PushNetworkException) e3.getCause());
            } else {
                throw new PushNetworkException(e3);
            }
        } catch (TimeoutException e4) {
            e = e4;
            throw new PushNetworkException(e);
        }
    }

    public void setUsername(String str) throws IOException {
        this.pushServiceSocket.setUsername(str);
    }

    public void deleteUsername() throws IOException {
        this.pushServiceSocket.deleteUsername();
    }

    public void deleteAccount() throws IOException {
        this.pushServiceSocket.deleteAccount();
    }

    public void requestRateLimitPushChallenge() throws IOException {
        this.pushServiceSocket.requestRateLimitPushChallenge();
    }

    public void submitRateLimitPushChallenge(String str) throws IOException {
        this.pushServiceSocket.submitRateLimitPushChallenge(str);
    }

    public void submitRateLimitRecaptchaChallenge(String str, String str2) throws IOException {
        this.pushServiceSocket.submitRateLimitRecaptchaChallenge(str, str2);
    }

    public void setSoTimeoutMillis(long j) {
        this.pushServiceSocket.setSoTimeoutMillis(j);
    }

    public void cancelInFlightRequests() {
        this.pushServiceSocket.cancelInFlightRequests();
    }

    private String createDirectoryServerToken(String str, boolean z) {
        try {
            String encodeBytesWithoutPadding = Base64.encodeBytesWithoutPadding(Util.trim(MessageDigest.getInstance("SHA1").digest(str.getBytes()), 10));
            return z ? encodeBytesWithoutPadding.replace('+', '-').replace('/', '_') : encodeBytesWithoutPadding;
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    private Map<String, String> createDirectoryServerTokenMap(Collection<String> collection) {
        HashMap hashMap = new HashMap(collection.size());
        for (String str : collection) {
            hashMap.put(createDirectoryServerToken(str, false), str);
        }
        return hashMap;
    }

    public GroupsV2Api getGroupsV2Api() {
        return new GroupsV2Api(this.pushServiceSocket, this.groupsV2Operations);
    }

    public AuthCredentials getPaymentsAuthorization() throws IOException {
        return this.pushServiceSocket.getPaymentsAuthorization();
    }
}
