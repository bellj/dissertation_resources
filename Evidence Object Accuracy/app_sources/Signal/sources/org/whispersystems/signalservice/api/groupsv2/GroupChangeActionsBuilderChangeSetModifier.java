package org.whispersystems.signalservice.api.groupsv2;

import org.signal.storageservice.protos.groups.GroupChange;

/* loaded from: classes5.dex */
public final class GroupChangeActionsBuilderChangeSetModifier implements ChangeSetModifier {
    private final GroupChange.Actions.Builder result;

    public GroupChangeActionsBuilderChangeSetModifier(GroupChange.Actions.Builder builder) {
        this.result = builder;
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddMembers(int i) {
        this.result.removeAddMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void moveAddToPromote(int i) {
        this.result.removeAddMembers(i);
        this.result.addPromotePendingMembers(GroupChange.Actions.PromotePendingMemberAction.newBuilder().setPresentation(this.result.getAddMembersList().get(i).getAdded().getPresentation()));
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeleteMembers(int i) {
        this.result.removeDeleteMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeModifyMemberRoles(int i) {
        this.result.removeModifyMemberRoles(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeModifyMemberProfileKeys(int i) {
        this.result.removeModifyMemberProfileKeys(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddPendingMembers(int i) {
        this.result.removeAddPendingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeletePendingMembers(int i) {
        this.result.removeDeletePendingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removePromotePendingMembers(int i) {
        this.result.removePromotePendingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyTitle() {
        this.result.clearModifyTitle();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAvatar() {
        this.result.clearModifyAvatar();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyDisappearingMessagesTimer() {
        this.result.clearModifyDisappearingMessagesTimer();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAttributesAccess() {
        this.result.clearModifyAttributesAccess();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyMemberAccess() {
        this.result.clearModifyMemberAccess();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAddFromInviteLinkAccess() {
        this.result.clearModifyAddFromInviteLinkAccess();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddRequestingMembers(int i) {
        this.result.removeAddRequestingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void moveAddRequestingMembersToPromote(int i) {
        this.result.removeAddRequestingMembers(i);
        this.result.addPromotePendingMembers(0, GroupChange.Actions.PromotePendingMemberAction.newBuilder().setPresentation(this.result.getAddRequestingMembersList().get(i).getAdded().getPresentation()));
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeleteRequestingMembers(int i) {
        this.result.removeDeleteRequestingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removePromoteRequestingMembers(int i) {
        this.result.removePromoteRequestingMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyDescription() {
        this.result.clearModifyDescription();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void clearModifyAnnouncementsOnly() {
        this.result.clearModifyAnnouncementsOnly();
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeAddBannedMembers(int i) {
        this.result.removeAddBannedMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removeDeleteBannedMembers(int i) {
        this.result.removeDeleteBannedMembers(i);
    }

    @Override // org.whispersystems.signalservice.api.groupsv2.ChangeSetModifier
    public void removePromotePendingPniAciMembers(int i) {
        this.result.removePromotePendingPniAciMembers(i);
    }
}
