package org.whispersystems.signalservice.api;

import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes5.dex */
public class TokenException extends Exception {
    private final boolean canAutomaticallyRetry;
    private final TokenResponse nextToken;

    public TokenException(TokenResponse tokenResponse, boolean z) {
        this.nextToken = tokenResponse;
        this.canAutomaticallyRetry = z;
    }

    public TokenResponse getToken() {
        return this.nextToken;
    }

    public boolean isCanAutomaticallyRetry() {
        return this.canAutomaticallyRetry;
    }
}
