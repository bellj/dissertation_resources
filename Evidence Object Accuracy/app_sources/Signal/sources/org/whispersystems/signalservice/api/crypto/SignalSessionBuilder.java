package org.whispersystems.signalservice.api.crypto;

import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.SessionBuilder;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.signal.libsignal.protocol.state.PreKeyBundle;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes5.dex */
public class SignalSessionBuilder {
    private final SessionBuilder builder;
    private final SignalSessionLock lock;

    public SignalSessionBuilder(SignalSessionLock signalSessionLock, SessionBuilder sessionBuilder) {
        this.lock = signalSessionLock;
        this.builder = sessionBuilder;
    }

    public void process(PreKeyBundle preKeyBundle) throws InvalidKeyException, UntrustedIdentityException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            this.builder.process(preKeyBundle);
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
