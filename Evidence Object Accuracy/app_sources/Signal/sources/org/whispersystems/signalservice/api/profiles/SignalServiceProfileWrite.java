package org.whispersystems.signalservice.api.profiles;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public class SignalServiceProfileWrite {
    @JsonProperty
    private byte[] about;
    @JsonProperty
    private byte[] aboutEmoji;
    @JsonProperty
    private boolean avatar;
    @JsonProperty
    private List<String> badgeIds;
    @JsonProperty
    private byte[] commitment;
    @JsonProperty
    private byte[] name;
    @JsonProperty
    private byte[] paymentAddress;
    @JsonProperty
    private boolean sameAvatar;
    @JsonProperty
    private String version;

    @JsonCreator
    public SignalServiceProfileWrite() {
    }

    public SignalServiceProfileWrite(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, boolean z, boolean z2, byte[] bArr5, List<String> list) {
        this.version = str;
        this.name = bArr;
        this.about = bArr2;
        this.aboutEmoji = bArr3;
        this.paymentAddress = bArr4;
        this.avatar = z;
        this.sameAvatar = z2;
        this.commitment = bArr5;
        this.badgeIds = list;
    }

    public boolean hasAvatar() {
        return this.avatar;
    }
}
