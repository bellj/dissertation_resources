package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class ConflictException extends NonSuccessfulResponseCodeException {
    public ConflictException() {
        super(409, "Conflict");
    }
}
