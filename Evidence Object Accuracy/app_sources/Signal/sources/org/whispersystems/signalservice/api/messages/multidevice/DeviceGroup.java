package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;
import java.util.List;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class DeviceGroup {
    private final boolean active;
    private final boolean archived;
    private final Optional<SignalServiceAttachmentStream> avatar;
    private final boolean blocked;
    private final Optional<String> color;
    private final Optional<Integer> expirationTimer;
    private final byte[] id;
    private final Optional<Integer> inboxPosition;
    private final List<SignalServiceAddress> members;
    private final Optional<String> name;

    public DeviceGroup(byte[] bArr, Optional<String> optional, List<SignalServiceAddress> list, Optional<SignalServiceAttachmentStream> optional2, boolean z, Optional<Integer> optional3, Optional<String> optional4, boolean z2, Optional<Integer> optional5, boolean z3) {
        this.id = bArr;
        this.name = optional;
        this.members = list;
        this.avatar = optional2;
        this.active = z;
        this.expirationTimer = optional3;
        this.color = optional4;
        this.blocked = z2;
        this.inboxPosition = optional5;
        this.archived = z3;
    }

    public Optional<SignalServiceAttachmentStream> getAvatar() {
        return this.avatar;
    }

    public Optional<String> getName() {
        return this.name;
    }

    public byte[] getId() {
        return this.id;
    }

    public List<SignalServiceAddress> getMembers() {
        return this.members;
    }

    public boolean isActive() {
        return this.active;
    }

    public Optional<Integer> getExpirationTimer() {
        return this.expirationTimer;
    }

    public Optional<String> getColor() {
        return this.color;
    }

    public boolean isBlocked() {
        return this.blocked;
    }

    public Optional<Integer> getInboxPosition() {
        return this.inboxPosition;
    }

    public boolean isArchived() {
        return this.archived;
    }
}
