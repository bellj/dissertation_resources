package org.whispersystems.signalservice.api.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class ContactTokenDetails {
    @JsonProperty
    private String number;
    @JsonProperty
    private String relay;
    @JsonProperty
    private String token;
    @JsonProperty
    private boolean video;
    @JsonProperty
    private boolean voice;

    public String getToken() {
        return this.token;
    }

    public String getRelay() {
        return this.relay;
    }

    public boolean isVoice() {
        return this.voice;
    }

    public boolean isVideo() {
        return this.video;
    }

    public void setNumber(String str) {
        this.number = str;
    }

    public String getNumber() {
        return this.number;
    }
}
