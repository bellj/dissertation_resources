package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class NoContentException extends NonSuccessfulResponseCodeException {
    public NoContentException(String str) {
        super(204, str);
    }
}
