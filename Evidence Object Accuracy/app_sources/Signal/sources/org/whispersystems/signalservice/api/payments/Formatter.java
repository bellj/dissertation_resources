package org.whispersystems.signalservice.api.payments;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;

/* loaded from: classes5.dex */
public abstract class Formatter {
    protected final FormatterOptions formatterOptions;

    public abstract String format(BigDecimal bigDecimal);

    private Formatter(FormatterOptions formatterOptions) {
        this.formatterOptions = formatterOptions;
    }

    public static Formatter forMoney(Currency currency, FormatterOptions formatterOptions) {
        return new CryptoFormatter(currency, formatterOptions);
    }

    public static Formatter forFiat(Currency currency, FormatterOptions formatterOptions) {
        return new FiatFormatter(currency, formatterOptions);
    }

    /* loaded from: classes5.dex */
    public static final class FiatFormatter extends Formatter {
        private final Currency javaCurrency;

        private FiatFormatter(Currency currency, FormatterOptions formatterOptions) {
            super(formatterOptions);
            this.javaCurrency = currency;
        }

        @Override // org.whispersystems.signalservice.api.payments.Formatter
        public String format(BigDecimal bigDecimal) {
            NumberFormat numberFormat;
            if (this.formatterOptions.alwaysPositive) {
                bigDecimal = bigDecimal.abs();
            }
            StringBuilder sb = new StringBuilder();
            FormatterOptions formatterOptions = this.formatterOptions;
            if (formatterOptions.withUnit) {
                numberFormat = NumberFormat.getCurrencyInstance(formatterOptions.locale);
                numberFormat.setCurrency(this.javaCurrency);
            } else {
                numberFormat = NumberFormat.getNumberInstance(formatterOptions.locale);
                numberFormat.setMinimumFractionDigits(this.javaCurrency.getDefaultFractionDigits());
            }
            numberFormat.setMaximumFractionDigits(Math.min(this.javaCurrency.getDefaultFractionDigits(), this.formatterOptions.maximumFractionDigits));
            sb.append(numberFormat.format(bigDecimal));
            return sb.toString();
        }
    }

    /* loaded from: classes5.dex */
    public static final class CryptoFormatter extends Formatter {
        private final Currency currency;

        private CryptoFormatter(Currency currency, FormatterOptions formatterOptions) {
            super(formatterOptions);
            this.currency = currency;
        }

        @Override // org.whispersystems.signalservice.api.payments.Formatter
        public String format(BigDecimal bigDecimal) {
            NumberFormat numberInstance = NumberFormat.getNumberInstance(this.formatterOptions.locale);
            if (this.formatterOptions.alwaysPositive) {
                bigDecimal = bigDecimal.abs();
            }
            StringBuilder sb = new StringBuilder();
            numberInstance.setMaximumFractionDigits(Math.min(this.currency.getDecimalPrecision(), this.formatterOptions.maximumFractionDigits));
            if (bigDecimal.signum() == 1 && this.formatterOptions.alwaysPrefixWithSign) {
                sb.append("+");
            }
            sb.append(numberInstance.format(bigDecimal));
            FormatterOptions formatterOptions = this.formatterOptions;
            if (formatterOptions.withSpaceBeforeUnit && formatterOptions.withUnit) {
                sb.append(" ");
            }
            if (this.formatterOptions.withUnit) {
                sb.append(this.currency.getCurrencyCode());
            }
            return sb.toString();
        }
    }
}
