package org.whispersystems.signalservice.api.groupsv2;

/* loaded from: classes5.dex */
public interface ChangeSetModifier {
    void clearModifyAddFromInviteLinkAccess();

    void clearModifyAnnouncementsOnly();

    void clearModifyAttributesAccess();

    void clearModifyAvatar();

    void clearModifyDescription();

    void clearModifyDisappearingMessagesTimer();

    void clearModifyMemberAccess();

    void clearModifyTitle();

    void moveAddRequestingMembersToPromote(int i);

    void moveAddToPromote(int i);

    void removeAddBannedMembers(int i);

    void removeAddMembers(int i);

    void removeAddPendingMembers(int i);

    void removeAddRequestingMembers(int i);

    void removeDeleteBannedMembers(int i);

    void removeDeleteMembers(int i);

    void removeDeletePendingMembers(int i);

    void removeDeleteRequestingMembers(int i);

    void removeModifyMemberProfileKeys(int i);

    void removeModifyMemberRoles(int i);

    void removePromotePendingMembers(int i);

    void removePromotePendingPniAciMembers(int i);

    void removePromoteRequestingMembers(int i);
}
