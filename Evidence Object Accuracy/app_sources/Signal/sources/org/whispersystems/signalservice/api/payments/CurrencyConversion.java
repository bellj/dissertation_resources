package org.whispersystems.signalservice.api.payments;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/* loaded from: classes.dex */
public final class CurrencyConversion {
    @JsonProperty
    private String base;
    @JsonProperty
    private Map<String, Double> conversions;

    public String getBase() {
        return this.base;
    }

    public Map<String, Double> getConversions() {
        return this.conversions;
    }
}
