package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import java.util.List;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class SignalServiceGroup {
    private final Optional<SignalServiceAttachment> avatar;
    private final byte[] groupId;
    private final Optional<List<SignalServiceAddress>> members;
    private final Optional<String> name;
    private final Type type;

    /* loaded from: classes5.dex */
    public enum Type {
        UNKNOWN,
        UPDATE,
        DELIVER,
        QUIT,
        REQUEST_INFO
    }

    public SignalServiceGroup(byte[] bArr) {
        this(Type.DELIVER, bArr, null, null, null);
    }

    public SignalServiceGroup(Type type, byte[] bArr, String str, List<SignalServiceAddress> list, SignalServiceAttachment signalServiceAttachment) {
        this.type = type;
        this.groupId = bArr;
        this.name = Optional.ofNullable(str);
        this.members = Optional.ofNullable(list);
        this.avatar = Optional.ofNullable(signalServiceAttachment);
    }

    public byte[] getGroupId() {
        return this.groupId;
    }

    public Type getType() {
        return this.type;
    }

    public Optional<String> getName() {
        return this.name;
    }

    public Optional<List<SignalServiceAddress>> getMembers() {
        return this.members;
    }

    public Optional<SignalServiceAttachment> getAvatar() {
        return this.avatar;
    }

    public static Builder newUpdateBuilder() {
        return new Builder(Type.UPDATE);
    }

    public static Builder newBuilder(Type type) {
        return new Builder(type);
    }

    /* loaded from: classes5.dex */
    public static class Builder {
        private SignalServiceAttachment avatar;
        private byte[] id;
        private List<SignalServiceAddress> members;
        private String name;
        private Type type;

        private Builder(Type type) {
            this.type = type;
        }

        public Builder withId(byte[] bArr) {
            this.id = bArr;
            return this;
        }

        public Builder withName(String str) {
            this.name = str;
            return this;
        }

        public Builder withMembers(List<SignalServiceAddress> list) {
            this.members = list;
            return this;
        }

        public Builder withAvatar(SignalServiceAttachment signalServiceAttachment) {
            this.avatar = signalServiceAttachment;
            return this;
        }

        public SignalServiceGroup build() {
            byte[] bArr = this.id;
            if (bArr != null) {
                Type type = this.type;
                if (type != Type.UPDATE || this.name != null || this.members != null || this.avatar != null) {
                    return new SignalServiceGroup(type, bArr, this.name, this.members, this.avatar);
                }
                throw new IllegalArgumentException("Group update with no updates!");
            }
            throw new IllegalArgumentException("No group ID specified!");
        }
    }
}
