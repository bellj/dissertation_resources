package org.whispersystems.signalservice.api.push.exceptions;

import java.io.IOException;

/* loaded from: classes5.dex */
public class ResumeLocationInvalidException extends IOException {
    public ResumeLocationInvalidException() {
    }

    public ResumeLocationInvalidException(String str) {
        super(str);
    }
}
