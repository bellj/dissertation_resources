package org.whispersystems.signalservice.api.crypto;

import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class AttachmentCipherOutputStream extends DigestingOutputStream {
    private final Cipher cipher;
    private final Mac mac;

    public AttachmentCipherOutputStream(byte[] bArr, byte[] bArr2, OutputStream outputStream) throws IOException {
        super(outputStream);
        try {
            Cipher initializeCipher = initializeCipher();
            this.cipher = initializeCipher;
            Mac initializeMac = initializeMac();
            this.mac = initializeMac;
            byte[][] split = Util.split(bArr, 32, 32);
            if (bArr2 == null) {
                initializeCipher.init(1, new SecretKeySpec(split[0], "AES"));
            } else {
                initializeCipher.init(1, new SecretKeySpec(split[0], "AES"), new IvParameterSpec(bArr2));
            }
            initializeMac.init(new SecretKeySpec(split[1], "HmacSHA256"));
            initializeMac.update(initializeCipher.getIV());
            super.write(initializeCipher.getIV());
        } catch (InvalidAlgorithmParameterException | InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        byte[] update = this.cipher.update(bArr, i, i2);
        if (update != null) {
            this.mac.update(update);
            super.write(update);
        }
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream
    public void write(int i) {
        throw new AssertionError("NYI");
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream, java.io.Flushable
    public void flush() throws IOException {
        try {
            byte[] doFinal = this.cipher.doFinal();
            byte[] doFinal2 = this.mac.doFinal(doFinal);
            super.write(doFinal);
            super.write(doFinal2);
            super.flush();
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new AssertionError(e);
        }
    }

    public static long getCiphertextLength(long j) {
        return (((j / 16) + 1) * 16) + 16 + 32;
    }

    private Mac initializeMac() {
        try {
            return Mac.getInstance("HmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    private Cipher initializeCipher() {
        try {
            return Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }
}
