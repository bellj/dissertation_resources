package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class SignalServiceStoryMessage {
    private final Optional<Boolean> allowsReplies;
    private final Optional<SignalServiceAttachment> fileAttachment;
    private final Optional<SignalServiceGroupV2> groupContext;
    private final Optional<byte[]> profileKey;
    private final Optional<SignalServiceTextAttachment> textAttachment;

    private SignalServiceStoryMessage(byte[] bArr, SignalServiceGroupV2 signalServiceGroupV2, SignalServiceAttachment signalServiceAttachment, SignalServiceTextAttachment signalServiceTextAttachment, boolean z) {
        this.profileKey = Optional.ofNullable(bArr);
        this.groupContext = Optional.ofNullable(signalServiceGroupV2);
        this.fileAttachment = Optional.ofNullable(signalServiceAttachment);
        this.textAttachment = Optional.ofNullable(signalServiceTextAttachment);
        this.allowsReplies = Optional.of(Boolean.valueOf(z));
    }

    public static SignalServiceStoryMessage forFileAttachment(byte[] bArr, SignalServiceGroupV2 signalServiceGroupV2, SignalServiceAttachment signalServiceAttachment, boolean z) {
        return new SignalServiceStoryMessage(bArr, signalServiceGroupV2, signalServiceAttachment, null, z);
    }

    public static SignalServiceStoryMessage forTextAttachment(byte[] bArr, SignalServiceGroupV2 signalServiceGroupV2, SignalServiceTextAttachment signalServiceTextAttachment, boolean z) {
        return new SignalServiceStoryMessage(bArr, signalServiceGroupV2, null, signalServiceTextAttachment, z);
    }

    public Optional<byte[]> getProfileKey() {
        return this.profileKey;
    }

    public Optional<SignalServiceGroupV2> getGroupContext() {
        return this.groupContext;
    }

    public Optional<SignalServiceAttachment> getFileAttachment() {
        return this.fileAttachment;
    }

    public Optional<SignalServiceTextAttachment> getTextAttachment() {
        return this.textAttachment;
    }

    public Optional<Boolean> getAllowsReplies() {
        return this.allowsReplies;
    }
}
