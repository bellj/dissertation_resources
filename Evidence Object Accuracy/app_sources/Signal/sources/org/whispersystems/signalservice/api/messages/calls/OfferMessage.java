package org.whispersystems.signalservice.api.messages.calls;

import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class OfferMessage {
    private final long id;
    private final byte[] opaque;
    private final String sdp;
    private final Type type;

    public OfferMessage(long j, String str, Type type, byte[] bArr) {
        this.id = j;
        this.sdp = str;
        this.type = type;
        this.opaque = bArr;
    }

    public String getSdp() {
        return this.sdp;
    }

    public long getId() {
        return this.id;
    }

    public Type getType() {
        return this.type;
    }

    public byte[] getOpaque() {
        return this.opaque;
    }

    /* loaded from: classes5.dex */
    public enum Type {
        AUDIO_CALL("audio_call", SignalServiceProtos.CallMessage.Offer.Type.OFFER_AUDIO_CALL),
        VIDEO_CALL("video_call", SignalServiceProtos.CallMessage.Offer.Type.OFFER_VIDEO_CALL);
        
        private final String code;
        private final SignalServiceProtos.CallMessage.Offer.Type protoType;

        Type(String str, SignalServiceProtos.CallMessage.Offer.Type type) {
            this.code = str;
            this.protoType = type;
        }

        public String getCode() {
            return this.code;
        }

        public SignalServiceProtos.CallMessage.Offer.Type getProtoType() {
            return this.protoType;
        }

        public static Type fromProto(SignalServiceProtos.CallMessage.Offer.Type type) {
            Type[] values = values();
            for (Type type2 : values) {
                if (type2.getProtoType().equals(type)) {
                    return type2;
                }
            }
            throw new IllegalArgumentException("Unexpected type: " + type.name());
        }

        public static Type fromCode(String str) {
            Type[] values = values();
            for (Type type : values) {
                if (type.getCode().equals(str)) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Unexpected code: " + str);
        }
    }
}
