package org.whispersystems.signalservice.api.crypto;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/* loaded from: classes5.dex */
public class SkippingOutputStream extends FilterOutputStream {
    private long toSkip;

    public SkippingOutputStream(long j, OutputStream outputStream) {
        super(outputStream);
        this.toSkip = j;
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream
    public void write(int i) throws IOException {
        long j = this.toSkip;
        if (j > 0) {
            this.toSkip = j - 1;
        } else {
            ((FilterOutputStream) this).out.write(i);
        }
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        bArr.getClass();
        if (i < 0 || i > bArr.length || i2 < 0 || (i3 = i2 + i) > bArr.length || i3 < 0) {
            throw new IndexOutOfBoundsException();
        }
        long j = this.toSkip;
        if (j > 0) {
            long j2 = (long) i2;
            if (j2 <= j) {
                this.toSkip = j - j2;
                return;
            }
            ((FilterOutputStream) this).out.write(bArr, i + ((int) j), i2 - ((int) j));
            this.toSkip = 0;
            return;
        }
        ((FilterOutputStream) this).out.write(bArr, i, i2);
    }
}
