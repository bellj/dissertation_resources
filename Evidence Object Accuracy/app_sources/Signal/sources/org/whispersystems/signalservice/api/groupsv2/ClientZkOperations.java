package org.whispersystems.signalservice.api.groupsv2;

import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.ServerPublicParams;
import org.signal.libsignal.zkgroup.auth.ClientZkAuthOperations;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.receipts.ClientZkReceiptOperations;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;

/* loaded from: classes5.dex */
public final class ClientZkOperations {
    private final ClientZkAuthOperations clientZkAuthOperations;
    private final ClientZkProfileOperations clientZkProfileOperations;
    private final ClientZkReceiptOperations clientZkReceiptOperations;
    private final ServerPublicParams serverPublicParams;

    public ClientZkOperations(ServerPublicParams serverPublicParams) {
        this.serverPublicParams = serverPublicParams;
        this.clientZkAuthOperations = new ClientZkAuthOperations(serverPublicParams);
        this.clientZkProfileOperations = new ClientZkProfileOperations(serverPublicParams);
        this.clientZkReceiptOperations = new ClientZkReceiptOperations(serverPublicParams);
    }

    public static ClientZkOperations create(SignalServiceConfiguration signalServiceConfiguration) {
        try {
            return new ClientZkOperations(new ServerPublicParams(signalServiceConfiguration.getZkGroupServerPublicParams()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public ClientZkAuthOperations getAuthOperations() {
        return this.clientZkAuthOperations;
    }

    public ClientZkProfileOperations getProfileOperations() {
        return this.clientZkProfileOperations;
    }

    public ClientZkReceiptOperations getReceiptOperations() {
        return this.clientZkReceiptOperations;
    }

    public ServerPublicParams getServerPublicParams() {
        return this.serverPublicParams;
    }
}
