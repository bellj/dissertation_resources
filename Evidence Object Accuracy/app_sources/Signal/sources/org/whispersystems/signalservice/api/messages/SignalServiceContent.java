package org.whispersystems.signalservice.api.messages;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.signal.libsignal.metadata.ProtocolInvalidKeyException;
import org.signal.libsignal.metadata.ProtocolInvalidMessageException;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.message.DecryptionErrorMessage;
import org.signal.libsignal.protocol.message.SenderKeyDistributionMessage;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.whispersystems.signalservice.api.InvalidMessageStructureException;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupV2;
import org.whispersystems.signalservice.api.messages.SignalServiceReceiptMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceTypingMessage;
import org.whispersystems.signalservice.api.messages.calls.AnswerMessage;
import org.whispersystems.signalservice.api.messages.calls.BusyMessage;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;
import org.whispersystems.signalservice.api.messages.calls.IceUpdateMessage;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.messages.calls.OpaqueMessage;
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage;
import org.whispersystems.signalservice.api.messages.multidevice.BlockedListMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ConfigurationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ContactsMessage;
import org.whispersystems.signalservice.api.messages.multidevice.KeysMessage;
import org.whispersystems.signalservice.api.messages.multidevice.MessageRequestResponseMessage;
import org.whispersystems.signalservice.api.messages.multidevice.OutgoingPaymentMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ReadMessage;
import org.whispersystems.signalservice.api.messages.multidevice.RequestMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SentTranscriptMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.StickerPackOperationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewOnceOpenMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewedMessage;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.StorageKey;
import org.whispersystems.signalservice.api.util.AttachmentPointerUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.push.UnsupportedDataMessageException;
import org.whispersystems.signalservice.internal.push.UnsupportedDataMessageProtocolVersionException;
import org.whispersystems.signalservice.internal.serialize.SignalServiceAddressProtobufSerializer;
import org.whispersystems.signalservice.internal.serialize.SignalServiceMetadataProtobufSerializer;
import org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProto;

/* loaded from: classes5.dex */
public final class SignalServiceContent {
    private static final String TAG;
    private final Optional<SignalServiceCallMessage> callMessage;
    private final Optional<DecryptionErrorMessage> decryptionErrorMessage;
    private final String destinationUuid;
    private final Optional<byte[]> groupId;
    private final Optional<SignalServiceDataMessage> message;
    private final boolean needsReceipt;
    private final Optional<SignalServiceReceiptMessage> readMessage;
    private final SignalServiceAddress sender;
    private final int senderDevice;
    private final Optional<SenderKeyDistributionMessage> senderKeyDistributionMessage;
    private final SignalServiceContentProto serializedState;
    private final long serverDeliveredTimestamp;
    private final long serverReceivedTimestamp;
    private final String serverUuid;
    private final Optional<SignalServiceStoryMessage> storyMessage;
    private final Optional<SignalServiceSyncMessage> synchronizeMessage;
    private final long timestamp;
    private final Optional<SignalServiceTypingMessage> typingMessage;

    private SignalServiceContent(SignalServiceDataMessage signalServiceDataMessage, Optional<SenderKeyDistributionMessage> optional, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional2, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional2;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.ofNullable(signalServiceDataMessage);
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.empty();
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = optional;
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(SignalServiceSyncMessage signalServiceSyncMessage, Optional<SenderKeyDistributionMessage> optional, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional2, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional2;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.ofNullable(signalServiceSyncMessage);
        this.callMessage = Optional.empty();
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = optional;
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(SignalServiceCallMessage signalServiceCallMessage, Optional<SenderKeyDistributionMessage> optional, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional2, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional2;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.of(signalServiceCallMessage);
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = optional;
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(SignalServiceReceiptMessage signalServiceReceiptMessage, Optional<SenderKeyDistributionMessage> optional, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional2, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional2;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.empty();
        this.readMessage = Optional.of(signalServiceReceiptMessage);
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = optional;
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(DecryptionErrorMessage decryptionErrorMessage, Optional<SenderKeyDistributionMessage> optional, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional2, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional2;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.empty();
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = optional;
        this.decryptionErrorMessage = Optional.of(decryptionErrorMessage);
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(SignalServiceTypingMessage signalServiceTypingMessage, Optional<SenderKeyDistributionMessage> optional, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional2, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional2;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.empty();
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.of(signalServiceTypingMessage);
        this.senderKeyDistributionMessage = optional;
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(SenderKeyDistributionMessage senderKeyDistributionMessage, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.empty();
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = Optional.of(senderKeyDistributionMessage);
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.empty();
    }

    private SignalServiceContent(SignalServiceStoryMessage signalServiceStoryMessage, SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional, String str2, SignalServiceContentProto signalServiceContentProto) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverUuid = str;
        this.groupId = optional;
        this.destinationUuid = str2;
        this.serializedState = signalServiceContentProto;
        this.message = Optional.empty();
        this.synchronizeMessage = Optional.empty();
        this.callMessage = Optional.empty();
        this.readMessage = Optional.empty();
        this.typingMessage = Optional.empty();
        this.senderKeyDistributionMessage = Optional.empty();
        this.decryptionErrorMessage = Optional.empty();
        this.storyMessage = Optional.of(signalServiceStoryMessage);
    }

    public Optional<SignalServiceDataMessage> getDataMessage() {
        return this.message;
    }

    public Optional<SignalServiceSyncMessage> getSyncMessage() {
        return this.synchronizeMessage;
    }

    public Optional<SignalServiceCallMessage> getCallMessage() {
        return this.callMessage;
    }

    public Optional<SignalServiceReceiptMessage> getReceiptMessage() {
        return this.readMessage;
    }

    public Optional<SignalServiceTypingMessage> getTypingMessage() {
        return this.typingMessage;
    }

    public Optional<SignalServiceStoryMessage> getStoryMessage() {
        return this.storyMessage;
    }

    public Optional<SenderKeyDistributionMessage> getSenderKeyDistributionMessage() {
        return this.senderKeyDistributionMessage;
    }

    public Optional<DecryptionErrorMessage> getDecryptionErrorMessage() {
        return this.decryptionErrorMessage;
    }

    public SignalServiceAddress getSender() {
        return this.sender;
    }

    public int getSenderDevice() {
        return this.senderDevice;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public long getServerReceivedTimestamp() {
        return this.serverReceivedTimestamp;
    }

    public long getServerDeliveredTimestamp() {
        return this.serverDeliveredTimestamp;
    }

    public boolean isNeedsReceipt() {
        return this.needsReceipt;
    }

    public String getServerUuid() {
        return this.serverUuid;
    }

    public Optional<byte[]> getGroupId() {
        return this.groupId;
    }

    public String getDestinationUuid() {
        return this.destinationUuid;
    }

    public byte[] serialize() {
        return this.serializedState.toByteArray();
    }

    public static SignalServiceContent deserialize(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            return createFromProto(SignalServiceContentProto.parseFrom(bArr));
        } catch (InvalidProtocolBufferException | ProtocolInvalidKeyException | ProtocolInvalidMessageException | InvalidMessageStructureException | UnsupportedDataMessageException e) {
            throw new AssertionError(e);
        }
    }

    public static SignalServiceContent createFromProto(SignalServiceContentProto signalServiceContentProto) throws ProtocolInvalidMessageException, ProtocolInvalidKeyException, UnsupportedDataMessageException, InvalidMessageStructureException {
        SignalServiceMetadata fromProtobuf = SignalServiceMetadataProtobufSerializer.fromProtobuf(signalServiceContentProto.getMetadata());
        SignalServiceAddress fromProtobuf2 = SignalServiceAddressProtobufSerializer.fromProtobuf(signalServiceContentProto.getLocalAddress());
        if (signalServiceContentProto.getDataCase() == SignalServiceContentProto.DataCase.LEGACYDATAMESSAGE) {
            return new SignalServiceContent(createSignalServiceMessage(fromProtobuf, signalServiceContentProto.getLegacyDataMessage()), Optional.empty(), fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), fromProtobuf.isNeedsReceipt(), fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (signalServiceContentProto.getDataCase() != SignalServiceContentProto.DataCase.CONTENT) {
            return null;
        }
        SignalServiceProtos.Content content = signalServiceContentProto.getContent();
        Optional empty = Optional.empty();
        if (content.hasSenderKeyDistributionMessage()) {
            try {
                empty = Optional.of(new SenderKeyDistributionMessage(content.getSenderKeyDistributionMessage().toByteArray()));
            } catch (InvalidKeyException | InvalidMessageException | InvalidVersionException | LegacyMessageException e) {
                Log.w(TAG, "Failed to parse SenderKeyDistributionMessage!", e);
            }
        }
        if (content.hasDataMessage()) {
            return new SignalServiceContent(createSignalServiceMessage(fromProtobuf, content.getDataMessage()), empty, fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), fromProtobuf.isNeedsReceipt(), fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (content.hasSyncMessage() && fromProtobuf2.matches(fromProtobuf.getSender())) {
            return new SignalServiceContent(createSynchronizeMessage(fromProtobuf, content.getSyncMessage()), empty, fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), fromProtobuf.isNeedsReceipt(), fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (content.hasCallMessage()) {
            return new SignalServiceContent(createCallMessage(content.getCallMessage()), empty, fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), fromProtobuf.isNeedsReceipt(), fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (content.hasReceiptMessage()) {
            return new SignalServiceContent(createReceiptMessage(fromProtobuf, content.getReceiptMessage()), empty, fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), fromProtobuf.isNeedsReceipt(), fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (content.hasTypingMessage()) {
            return new SignalServiceContent(createTypingMessage(fromProtobuf, content.getTypingMessage()), (Optional<SenderKeyDistributionMessage>) empty, fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), false, fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (content.hasDecryptionErrorMessage()) {
            return new SignalServiceContent(createDecryptionErrorMessage(fromProtobuf, content.getDecryptionErrorMessage()), empty, fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), fromProtobuf.isNeedsReceipt(), fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (empty.isPresent()) {
            return new SignalServiceContent((SenderKeyDistributionMessage) empty.get(), fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), false, fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        if (content.hasStoryMessage()) {
            return new SignalServiceContent(createStoryMessage(content.getStoryMessage()), fromProtobuf.getSender(), fromProtobuf.getSenderDevice(), fromProtobuf.getTimestamp(), fromProtobuf.getServerReceivedTimestamp(), fromProtobuf.getServerDeliveredTimestamp(), false, fromProtobuf.getServerGuid(), fromProtobuf.getGroupId(), fromProtobuf.getDestinationUuid(), signalServiceContentProto);
        }
        return null;
    }

    private static SignalServiceDataMessage createSignalServiceMessage(SignalServiceMetadata signalServiceMetadata, SignalServiceProtos.DataMessage dataMessage) throws UnsupportedDataMessageException, InvalidMessageStructureException {
        SignalServiceGroupV2 createGroupV2Info = createGroupV2Info(dataMessage);
        byte[] bArr = null;
        try {
            Optional<SignalServiceGroupContext> createOptional = SignalServiceGroupContext.createOptional(null, createGroupV2Info);
            LinkedList linkedList = new LinkedList();
            boolean z = true;
            boolean z2 = (dataMessage.getFlags() & 1) != 0;
            boolean z3 = (dataMessage.getFlags() & 2) != 0;
            boolean z4 = (dataMessage.getFlags() & 4) != 0;
            if (createGroupV2Info == null) {
                z = false;
            }
            SignalServiceDataMessage.Quote createQuote = createQuote(dataMessage, z);
            List<SharedContact> createSharedContacts = createSharedContacts(dataMessage);
            List<SignalServicePreview> createPreviews = createPreviews(dataMessage);
            List<SignalServiceDataMessage.Mention> createMentions = createMentions(dataMessage.getBodyRangesList(), dataMessage.getBody(), z);
            SignalServiceDataMessage.Sticker createSticker = createSticker(dataMessage);
            SignalServiceDataMessage.Reaction createReaction = createReaction(dataMessage);
            SignalServiceDataMessage.RemoteDelete createRemoteDelete = createRemoteDelete(dataMessage);
            SignalServiceDataMessage.GroupCallUpdate createGroupCallUpdate = createGroupCallUpdate(dataMessage);
            SignalServiceDataMessage.StoryContext createStoryContext = createStoryContext(dataMessage);
            SignalServiceDataMessage.GiftBadge createGiftBadge = createGiftBadge(dataMessage);
            if (dataMessage.getRequiredProtocolVersion() <= 7) {
                SignalServiceDataMessage.Payment createPayment = createPayment(dataMessage);
                int requiredProtocolVersion = dataMessage.getRequiredProtocolVersion();
                SignalServiceProtos.DataMessage.ProtocolVersion protocolVersion = SignalServiceProtos.DataMessage.ProtocolVersion.CURRENT;
                if (requiredProtocolVersion <= protocolVersion.getNumber()) {
                    for (SignalServiceProtos.AttachmentPointer attachmentPointer : dataMessage.getAttachmentsList()) {
                        linkedList.add(createAttachmentPointer(attachmentPointer));
                    }
                    if (!dataMessage.hasTimestamp() || dataMessage.getTimestamp() == signalServiceMetadata.getTimestamp()) {
                        long timestamp = signalServiceMetadata.getTimestamp();
                        String body = dataMessage.hasBody() ? dataMessage.getBody() : null;
                        int expireTimer = dataMessage.getExpireTimer();
                        if (dataMessage.hasProfileKey()) {
                            bArr = dataMessage.getProfileKey().toByteArray();
                        }
                        return new SignalServiceDataMessage(timestamp, null, createGroupV2Info, linkedList, body, z2, expireTimer, z3, bArr, z4, createQuote, createSharedContacts, createPreviews, createMentions, createSticker, dataMessage.getIsViewOnce(), createReaction, createRemoteDelete, createGroupCallUpdate, createPayment, createStoryContext, createGiftBadge);
                    }
                    throw new InvalidMessageStructureException("Timestamps don't match: " + dataMessage.getTimestamp() + " vs " + signalServiceMetadata.getTimestamp(), signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice());
                }
                throw new UnsupportedDataMessageProtocolVersionException(protocolVersion.getNumber(), dataMessage.getRequiredProtocolVersion(), signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice(), createOptional);
            }
            throw new UnsupportedDataMessageProtocolVersionException(7, dataMessage.getRequiredProtocolVersion(), signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice(), createOptional);
        } catch (InvalidMessageException e) {
            throw new InvalidMessageStructureException(e);
        }
    }

    private static SignalServiceSyncMessage createSynchronizeMessage(SignalServiceMetadata signalServiceMetadata, SignalServiceProtos.SyncMessage syncMessage) throws ProtocolInvalidKeyException, UnsupportedDataMessageException, InvalidMessageStructureException {
        MessageRequestResponseMessage.Type type;
        MessageRequestResponseMessage messageRequestResponseMessage;
        StickerPackOperationMessage.Type type2;
        VerifiedMessage.VerifiedState verifiedState;
        Optional optional;
        Boolean bool = null;
        if (syncMessage.hasSent()) {
            HashMap hashMap = new HashMap();
            SignalServiceProtos.SyncMessage.Sent sent = syncMessage.getSent();
            Optional of = sent.hasMessage() ? Optional.of(createSignalServiceMessage(signalServiceMetadata, sent.getMessage())) : Optional.empty();
            Optional of2 = sent.hasStoryMessage() ? Optional.of(createStoryMessage(sent.getStoryMessage())) : Optional.empty();
            if (SignalServiceAddress.isValidAddress(sent.getDestinationUuid())) {
                optional = Optional.of(new SignalServiceAddress(ServiceId.parseOrThrow(sent.getDestinationUuid())));
            } else {
                optional = Optional.empty();
            }
            Set set = (Set) Collection$EL.stream(sent.getStoryMessageRecipientsList()).map(new Function() { // from class: org.whispersystems.signalservice.api.messages.SignalServiceContent$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return SignalServiceContent.createSignalServiceStoryMessageRecipient((SignalServiceProtos.SyncMessage.Sent.StoryMessageRecipient) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toSet());
            if (optional.isPresent() || of.flatMap(new Function() { // from class: org.whispersystems.signalservice.api.messages.SignalServiceContent$$ExternalSyntheticLambda1
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ((SignalServiceDataMessage) obj).getGroupContext();
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).isPresent() || of2.flatMap(new Function() { // from class: org.whispersystems.signalservice.api.messages.SignalServiceContent$$ExternalSyntheticLambda2
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ((SignalServiceStoryMessage) obj).getGroupContext();
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).isPresent()) {
                for (SignalServiceProtos.SyncMessage.Sent.UnidentifiedDeliveryStatus unidentifiedDeliveryStatus : sent.getUnidentifiedStatusList()) {
                    if (SignalServiceAddress.isValidAddress(unidentifiedDeliveryStatus.getDestinationUuid(), null)) {
                        hashMap.put(ServiceId.parseOrNull(unidentifiedDeliveryStatus.getDestinationUuid()), Boolean.valueOf(unidentifiedDeliveryStatus.getUnidentified()));
                    } else {
                        Log.w(TAG, "Encountered an invalid UnidentifiedDeliveryStatus in a SentTranscript! Ignoring.");
                    }
                }
                return SignalServiceSyncMessage.forSentTranscript(new SentTranscriptMessage(optional, sent.getTimestamp(), of, sent.getExpirationStartTimestamp(), hashMap, sent.getIsRecipientUpdate(), of2, set));
            }
            throw new InvalidMessageStructureException("SyncMessage missing both destination and group ID!");
        } else if (syncMessage.hasRequest()) {
            return SignalServiceSyncMessage.forRequest(new RequestMessage(syncMessage.getRequest()));
        } else {
            if (syncMessage.getReadList().size() > 0) {
                LinkedList linkedList = new LinkedList();
                for (SignalServiceProtos.SyncMessage.Read read : syncMessage.getReadList()) {
                    ServiceId parseOrNull = ServiceId.parseOrNull(read.getSenderUuid());
                    if (parseOrNull != null) {
                        linkedList.add(new ReadMessage(parseOrNull, read.getTimestamp()));
                    } else {
                        Log.w(TAG, "Encountered an invalid ReadMessage! Ignoring.");
                    }
                }
                return SignalServiceSyncMessage.forRead(linkedList);
            } else if (syncMessage.getViewedList().size() > 0) {
                LinkedList linkedList2 = new LinkedList();
                for (SignalServiceProtos.SyncMessage.Viewed viewed : syncMessage.getViewedList()) {
                    ServiceId parseOrNull2 = ServiceId.parseOrNull(viewed.getSenderUuid());
                    if (parseOrNull2 != null) {
                        linkedList2.add(new ViewedMessage(parseOrNull2, viewed.getTimestamp()));
                    } else {
                        Log.w(TAG, "Encountered an invalid ReadMessage! Ignoring.");
                    }
                }
                return SignalServiceSyncMessage.forViewed(linkedList2);
            } else if (syncMessage.hasViewOnceOpen()) {
                ServiceId parseOrNull3 = ServiceId.parseOrNull(syncMessage.getViewOnceOpen().getSenderUuid());
                if (parseOrNull3 != null) {
                    return SignalServiceSyncMessage.forViewOnceOpen(new ViewOnceOpenMessage(parseOrNull3, syncMessage.getViewOnceOpen().getTimestamp()));
                }
                throw new InvalidMessageStructureException("ViewOnceOpen message has no sender!");
            } else if (syncMessage.hasVerified()) {
                if (SignalServiceAddress.isValidAddress(syncMessage.getVerified().getDestinationUuid())) {
                    try {
                        SignalServiceProtos.Verified verified = syncMessage.getVerified();
                        SignalServiceAddress signalServiceAddress = new SignalServiceAddress(ServiceId.parseOrThrow(verified.getDestinationUuid()));
                        IdentityKey identityKey = new IdentityKey(verified.getIdentityKey().toByteArray(), 0);
                        if (verified.getState() == SignalServiceProtos.Verified.State.DEFAULT) {
                            verifiedState = VerifiedMessage.VerifiedState.DEFAULT;
                        } else if (verified.getState() == SignalServiceProtos.Verified.State.VERIFIED) {
                            verifiedState = VerifiedMessage.VerifiedState.VERIFIED;
                        } else if (verified.getState() == SignalServiceProtos.Verified.State.UNVERIFIED) {
                            verifiedState = VerifiedMessage.VerifiedState.UNVERIFIED;
                        } else {
                            throw new InvalidMessageStructureException("Unknown state: " + verified.getState().getNumber(), signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice());
                        }
                        return SignalServiceSyncMessage.forVerified(new VerifiedMessage(signalServiceAddress, identityKey, verifiedState, System.currentTimeMillis()));
                    } catch (InvalidKeyException e) {
                        throw new ProtocolInvalidKeyException(e, signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice());
                    }
                } else {
                    throw new InvalidMessageStructureException("Verified message has no sender!");
                }
            } else if (syncMessage.getStickerPackOperationList().size() > 0) {
                LinkedList linkedList3 = new LinkedList();
                for (SignalServiceProtos.SyncMessage.StickerPackOperation stickerPackOperation : syncMessage.getStickerPackOperationList()) {
                    byte[] byteArray = stickerPackOperation.hasPackId() ? stickerPackOperation.getPackId().toByteArray() : null;
                    byte[] byteArray2 = stickerPackOperation.hasPackKey() ? stickerPackOperation.getPackKey().toByteArray() : null;
                    if (stickerPackOperation.hasType()) {
                        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$StickerPackOperation$Type[stickerPackOperation.getType().ordinal()];
                        if (i == 1) {
                            type2 = StickerPackOperationMessage.Type.INSTALL;
                        } else if (i == 2) {
                            type2 = StickerPackOperationMessage.Type.REMOVE;
                        }
                        linkedList3.add(new StickerPackOperationMessage(byteArray, byteArray2, type2));
                    }
                    type2 = null;
                    linkedList3.add(new StickerPackOperationMessage(byteArray, byteArray2, type2));
                }
                return SignalServiceSyncMessage.forStickerPackOperations(linkedList3);
            } else if (syncMessage.hasBlocked()) {
                List<String> numbersList = syncMessage.getBlocked().getNumbersList();
                List<String> uuidsList = syncMessage.getBlocked().getUuidsList();
                ArrayList arrayList = new ArrayList(numbersList.size() + uuidsList.size());
                ArrayList arrayList2 = new ArrayList(syncMessage.getBlocked().getGroupIdsList().size());
                for (String str : uuidsList) {
                    Optional<SignalServiceAddress> fromRaw = SignalServiceAddress.fromRaw(str, null);
                    if (fromRaw.isPresent()) {
                        arrayList.add(fromRaw.get());
                    }
                }
                for (ByteString byteString : syncMessage.getBlocked().getGroupIdsList()) {
                    arrayList2.add(byteString.toByteArray());
                }
                return SignalServiceSyncMessage.forBlocked(new BlockedListMessage(arrayList, arrayList2));
            } else if (syncMessage.hasConfiguration()) {
                Boolean valueOf = syncMessage.getConfiguration().hasReadReceipts() ? Boolean.valueOf(syncMessage.getConfiguration().getReadReceipts()) : null;
                Boolean valueOf2 = syncMessage.getConfiguration().hasUnidentifiedDeliveryIndicators() ? Boolean.valueOf(syncMessage.getConfiguration().getUnidentifiedDeliveryIndicators()) : null;
                Boolean valueOf3 = syncMessage.getConfiguration().hasTypingIndicators() ? Boolean.valueOf(syncMessage.getConfiguration().getTypingIndicators()) : null;
                if (syncMessage.getConfiguration().hasLinkPreviews()) {
                    bool = Boolean.valueOf(syncMessage.getConfiguration().getLinkPreviews());
                }
                return SignalServiceSyncMessage.forConfiguration(new ConfigurationMessage(Optional.ofNullable(valueOf), Optional.ofNullable(valueOf2), Optional.ofNullable(valueOf3), Optional.ofNullable(bool)));
            } else {
                if (syncMessage.hasFetchLatest() && syncMessage.getFetchLatest().hasType()) {
                    int i2 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$FetchLatest$Type[syncMessage.getFetchLatest().getType().ordinal()];
                    if (i2 == 1) {
                        return SignalServiceSyncMessage.forFetchLatest(SignalServiceSyncMessage.FetchType.LOCAL_PROFILE);
                    }
                    if (i2 == 2) {
                        return SignalServiceSyncMessage.forFetchLatest(SignalServiceSyncMessage.FetchType.STORAGE_MANIFEST);
                    }
                    if (i2 == 3) {
                        return SignalServiceSyncMessage.forFetchLatest(SignalServiceSyncMessage.FetchType.SUBSCRIPTION_STATUS);
                    }
                }
                if (syncMessage.hasMessageRequestResponse()) {
                    int i3 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$MessageRequestResponse$Type[syncMessage.getMessageRequestResponse().getType().ordinal()];
                    if (i3 == 1) {
                        type = MessageRequestResponseMessage.Type.ACCEPT;
                    } else if (i3 == 2) {
                        type = MessageRequestResponseMessage.Type.DELETE;
                    } else if (i3 == 3) {
                        type = MessageRequestResponseMessage.Type.BLOCK;
                    } else if (i3 != 4) {
                        type = MessageRequestResponseMessage.Type.UNKNOWN;
                    } else {
                        type = MessageRequestResponseMessage.Type.BLOCK_AND_DELETE;
                    }
                    if (syncMessage.getMessageRequestResponse().hasGroupId()) {
                        messageRequestResponseMessage = MessageRequestResponseMessage.forGroup(syncMessage.getMessageRequestResponse().getGroupId().toByteArray(), type);
                    } else {
                        ServiceId parseOrNull4 = ServiceId.parseOrNull(syncMessage.getMessageRequestResponse().getThreadUuid());
                        if (parseOrNull4 != null) {
                            messageRequestResponseMessage = MessageRequestResponseMessage.forIndividual(parseOrNull4, type);
                        } else {
                            throw new InvalidMessageStructureException("Message request response has an invalid thread identifier!");
                        }
                    }
                    return SignalServiceSyncMessage.forMessageRequestResponse(messageRequestResponseMessage);
                } else if (syncMessage.hasOutgoingPayment()) {
                    SignalServiceProtos.SyncMessage.OutgoingPayment outgoingPayment = syncMessage.getOutgoingPayment();
                    if (AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$OutgoingPayment$PaymentDetailCase[outgoingPayment.getPaymentDetailCase().ordinal()] != 1) {
                        return SignalServiceSyncMessage.empty();
                    }
                    SignalServiceProtos.SyncMessage.OutgoingPayment.MobileCoin mobileCoin = outgoingPayment.getMobileCoin();
                    Money.MobileCoin picoMobileCoin = Money.picoMobileCoin(mobileCoin.getAmountPicoMob());
                    Money.MobileCoin picoMobileCoin2 = Money.picoMobileCoin(mobileCoin.getFeePicoMob());
                    ByteString recipientAddress = mobileCoin.getRecipientAddress();
                    return SignalServiceSyncMessage.forOutgoingPayment(new OutgoingPaymentMessage(Optional.ofNullable(ServiceId.parseOrNull(outgoingPayment.getRecipientUuid())), picoMobileCoin, picoMobileCoin2, mobileCoin.getReceipt(), mobileCoin.getLedgerBlockIndex(), mobileCoin.getLedgerBlockTimestamp(), recipientAddress.isEmpty() ? Optional.empty() : Optional.of(recipientAddress.toByteArray()), Optional.of(outgoingPayment.getNote()), mobileCoin.getOutputPublicKeysList(), mobileCoin.getSpentKeyImagesList()));
                } else if (syncMessage.hasKeys() && syncMessage.getKeys().hasStorageService()) {
                    return SignalServiceSyncMessage.forKeys(new KeysMessage(Optional.of(new StorageKey(syncMessage.getKeys().getStorageService().toByteArray()))));
                } else {
                    if (syncMessage.hasContacts()) {
                        return SignalServiceSyncMessage.forContacts(new ContactsMessage(createAttachmentPointer(syncMessage.getContacts().getBlob()), syncMessage.getContacts().getComplete()));
                    }
                    return SignalServiceSyncMessage.empty();
                }
            }
        }
    }

    public static SignalServiceStoryMessageRecipient createSignalServiceStoryMessageRecipient(SignalServiceProtos.SyncMessage.Sent.StoryMessageRecipient storyMessageRecipient) {
        return new SignalServiceStoryMessageRecipient(new SignalServiceAddress(ServiceId.parseOrThrow(storyMessageRecipient.getDestinationUuid())), storyMessageRecipient.getDistributionListIdsList(), storyMessageRecipient.getIsAllowedToReply());
    }

    private static SignalServiceCallMessage createCallMessage(SignalServiceProtos.CallMessage callMessage) {
        boolean multiRing = callMessage.getMultiRing();
        byte[] bArr = null;
        Integer valueOf = callMessage.hasDestinationDeviceId() ? Integer.valueOf(callMessage.getDestinationDeviceId()) : null;
        if (callMessage.hasOffer()) {
            SignalServiceProtos.CallMessage.Offer offer = callMessage.getOffer();
            long id = offer.getId();
            String sdp = offer.hasSdp() ? offer.getSdp() : null;
            OfferMessage.Type fromProto = OfferMessage.Type.fromProto(offer.getType());
            if (offer.hasOpaque()) {
                bArr = offer.getOpaque().toByteArray();
            }
            return SignalServiceCallMessage.forOffer(new OfferMessage(id, sdp, fromProto, bArr), multiRing, valueOf);
        } else if (callMessage.hasAnswer()) {
            SignalServiceProtos.CallMessage.Answer answer = callMessage.getAnswer();
            long id2 = answer.getId();
            String sdp2 = answer.hasSdp() ? answer.getSdp() : null;
            if (answer.hasOpaque()) {
                bArr = answer.getOpaque().toByteArray();
            }
            return SignalServiceCallMessage.forAnswer(new AnswerMessage(id2, sdp2, bArr), multiRing, valueOf);
        } else if (callMessage.getIceUpdateCount() > 0) {
            LinkedList linkedList = new LinkedList();
            for (SignalServiceProtos.CallMessage.IceUpdate iceUpdate : callMessage.getIceUpdateList()) {
                linkedList.add(new IceUpdateMessage(iceUpdate.getId(), iceUpdate.hasOpaque() ? iceUpdate.getOpaque().toByteArray() : null, iceUpdate.hasSdp() ? iceUpdate.getSdp() : null));
            }
            return SignalServiceCallMessage.forIceUpdates(linkedList, multiRing, valueOf);
        } else if (callMessage.hasLegacyHangup()) {
            SignalServiceProtos.CallMessage.Hangup legacyHangup = callMessage.getLegacyHangup();
            return SignalServiceCallMessage.forHangup(new HangupMessage(legacyHangup.getId(), HangupMessage.Type.fromProto(legacyHangup.getType()), legacyHangup.getDeviceId(), callMessage.hasLegacyHangup()), multiRing, valueOf);
        } else if (callMessage.hasHangup()) {
            SignalServiceProtos.CallMessage.Hangup hangup = callMessage.getHangup();
            return SignalServiceCallMessage.forHangup(new HangupMessage(hangup.getId(), HangupMessage.Type.fromProto(hangup.getType()), hangup.getDeviceId(), callMessage.hasLegacyHangup()), multiRing, valueOf);
        } else if (callMessage.hasBusy()) {
            return SignalServiceCallMessage.forBusy(new BusyMessage(callMessage.getBusy().getId()), multiRing, valueOf);
        } else {
            if (callMessage.hasOpaque()) {
                return SignalServiceCallMessage.forOpaque(new OpaqueMessage(callMessage.getOpaque().getData().toByteArray(), null), multiRing, valueOf);
            }
            return SignalServiceCallMessage.empty();
        }
    }

    private static SignalServiceReceiptMessage createReceiptMessage(SignalServiceMetadata signalServiceMetadata, SignalServiceProtos.ReceiptMessage receiptMessage) {
        SignalServiceReceiptMessage.Type type;
        if (receiptMessage.getType() == SignalServiceProtos.ReceiptMessage.Type.DELIVERY) {
            type = SignalServiceReceiptMessage.Type.DELIVERY;
        } else if (receiptMessage.getType() == SignalServiceProtos.ReceiptMessage.Type.READ) {
            type = SignalServiceReceiptMessage.Type.READ;
        } else if (receiptMessage.getType() == SignalServiceProtos.ReceiptMessage.Type.VIEWED) {
            type = SignalServiceReceiptMessage.Type.VIEWED;
        } else {
            type = SignalServiceReceiptMessage.Type.UNKNOWN;
        }
        return new SignalServiceReceiptMessage(type, receiptMessage.getTimestampList(), signalServiceMetadata.getTimestamp());
    }

    private static DecryptionErrorMessage createDecryptionErrorMessage(SignalServiceMetadata signalServiceMetadata, ByteString byteString) throws InvalidMessageStructureException {
        try {
            return new DecryptionErrorMessage(byteString.toByteArray());
        } catch (InvalidMessageException e) {
            throw new InvalidMessageStructureException(e, signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice());
        }
    }

    private static SignalServiceTypingMessage createTypingMessage(SignalServiceMetadata signalServiceMetadata, SignalServiceProtos.TypingMessage typingMessage) throws InvalidMessageStructureException {
        SignalServiceTypingMessage.Action action;
        Optional optional;
        if (typingMessage.getAction() == SignalServiceProtos.TypingMessage.Action.STARTED) {
            action = SignalServiceTypingMessage.Action.STARTED;
        } else if (typingMessage.getAction() == SignalServiceProtos.TypingMessage.Action.STOPPED) {
            action = SignalServiceTypingMessage.Action.STOPPED;
        } else {
            action = SignalServiceTypingMessage.Action.UNKNOWN;
        }
        if (!typingMessage.hasTimestamp() || typingMessage.getTimestamp() == signalServiceMetadata.getTimestamp()) {
            long timestamp = typingMessage.getTimestamp();
            if (typingMessage.hasGroupId()) {
                optional = Optional.of(typingMessage.getGroupId().toByteArray());
            } else {
                optional = Optional.empty();
            }
            return new SignalServiceTypingMessage(action, timestamp, optional);
        }
        throw new InvalidMessageStructureException("Timestamps don't match: " + typingMessage.getTimestamp() + " vs " + signalServiceMetadata.getTimestamp(), signalServiceMetadata.getSender().getIdentifier(), signalServiceMetadata.getSenderDevice());
    }

    private static SignalServiceStoryMessage createStoryMessage(SignalServiceProtos.StoryMessage storyMessage) throws InvalidMessageStructureException {
        byte[] byteArray = storyMessage.hasProfileKey() ? storyMessage.getProfileKey().toByteArray() : null;
        if (storyMessage.hasFileAttachment()) {
            return SignalServiceStoryMessage.forFileAttachment(byteArray, createGroupV2Info(storyMessage), createAttachmentPointer(storyMessage.getFileAttachment()), storyMessage.getAllowsReplies());
        }
        return SignalServiceStoryMessage.forTextAttachment(byteArray, createGroupV2Info(storyMessage), createTextAttachment(storyMessage.getTextAttachment()), storyMessage.getAllowsReplies());
    }

    private static SignalServiceDataMessage.Quote createQuote(SignalServiceProtos.DataMessage dataMessage, boolean z) throws InvalidMessageStructureException {
        if (!dataMessage.hasQuote()) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        for (SignalServiceProtos.DataMessage.Quote.QuotedAttachment quotedAttachment : dataMessage.getQuote().getAttachmentsList()) {
            linkedList.add(new SignalServiceDataMessage.Quote.QuotedAttachment(quotedAttachment.getContentType(), quotedAttachment.getFileName(), quotedAttachment.hasThumbnail() ? createAttachmentPointer(quotedAttachment.getThumbnail()) : null));
        }
        ServiceId parseOrNull = ServiceId.parseOrNull(dataMessage.getQuote().getAuthorUuid());
        if (parseOrNull != null) {
            return new SignalServiceDataMessage.Quote(dataMessage.getQuote().getId(), parseOrNull, dataMessage.getQuote().getText(), linkedList, createMentions(dataMessage.getQuote().getBodyRangesList(), dataMessage.getQuote().getText(), z), SignalServiceDataMessage.Quote.Type.fromProto(dataMessage.getQuote().getType()));
        }
        Log.w(TAG, "Quote was missing an author! Returning null.");
        return null;
    }

    private static List<SignalServicePreview> createPreviews(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (dataMessage.getPreviewCount() <= 0) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        for (SignalServiceProtos.Preview preview : dataMessage.getPreviewList()) {
            linkedList.add(createPreview(preview));
        }
        return linkedList;
    }

    private static SignalServicePreview createPreview(SignalServiceProtos.Preview preview) throws InvalidMessageStructureException {
        return new SignalServicePreview(preview.getUrl(), preview.getTitle(), preview.getDescription(), preview.getDate(), Optional.ofNullable(preview.hasImage() ? createAttachmentPointer(preview.getImage()) : null));
    }

    private static List<SignalServiceDataMessage.Mention> createMentions(List<SignalServiceProtos.DataMessage.BodyRange> list, String str, boolean z) throws InvalidMessageStructureException {
        if (list == null || list.isEmpty() || str == null) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        for (SignalServiceProtos.DataMessage.BodyRange bodyRange : list) {
            if (bodyRange.hasMentionUuid()) {
                try {
                    linkedList.add(new SignalServiceDataMessage.Mention(ServiceId.parseOrThrow(bodyRange.getMentionUuid()), bodyRange.getStart(), bodyRange.getLength()));
                } catch (IllegalArgumentException unused) {
                    throw new InvalidMessageStructureException("Invalid body range!");
                }
            }
        }
        if (linkedList.size() > 0 && !z) {
            Log.w(TAG, "Mentions received in non-GV2 message");
        }
        return linkedList;
    }

    private static SignalServiceDataMessage.Sticker createSticker(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (!dataMessage.hasSticker() || !dataMessage.getSticker().hasPackId() || !dataMessage.getSticker().hasPackKey() || !dataMessage.getSticker().hasStickerId() || !dataMessage.getSticker().hasData()) {
            return null;
        }
        SignalServiceProtos.DataMessage.Sticker sticker = dataMessage.getSticker();
        return new SignalServiceDataMessage.Sticker(sticker.getPackId().toByteArray(), sticker.getPackKey().toByteArray(), sticker.getStickerId(), sticker.getEmoji(), createAttachmentPointer(sticker.getData()));
    }

    private static SignalServiceDataMessage.Reaction createReaction(SignalServiceProtos.DataMessage dataMessage) {
        if (!dataMessage.hasReaction() || !dataMessage.getReaction().hasEmoji() || !dataMessage.getReaction().hasTargetAuthorUuid() || !dataMessage.getReaction().hasTargetSentTimestamp()) {
            return null;
        }
        SignalServiceProtos.DataMessage.Reaction reaction = dataMessage.getReaction();
        ServiceId parseOrNull = ServiceId.parseOrNull(reaction.getTargetAuthorUuid());
        if (parseOrNull != null) {
            return new SignalServiceDataMessage.Reaction(reaction.getEmoji(), reaction.getRemove(), parseOrNull, reaction.getTargetSentTimestamp());
        }
        Log.w(TAG, "Cannot parse author UUID on reaction");
        return null;
    }

    private static SignalServiceDataMessage.RemoteDelete createRemoteDelete(SignalServiceProtos.DataMessage dataMessage) {
        if (!dataMessage.hasDelete() || !dataMessage.getDelete().hasTargetSentTimestamp()) {
            return null;
        }
        return new SignalServiceDataMessage.RemoteDelete(dataMessage.getDelete().getTargetSentTimestamp());
    }

    private static SignalServiceDataMessage.GroupCallUpdate createGroupCallUpdate(SignalServiceProtos.DataMessage dataMessage) {
        if (!dataMessage.hasGroupCallUpdate()) {
            return null;
        }
        return new SignalServiceDataMessage.GroupCallUpdate(dataMessage.getGroupCallUpdate().getEraId());
    }

    private static SignalServiceDataMessage.Payment createPayment(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (!dataMessage.hasPayment()) {
            return null;
        }
        SignalServiceProtos.DataMessage.Payment payment = dataMessage.getPayment();
        if (AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$ItemCase[payment.getItemCase().ordinal()] == 1) {
            return new SignalServiceDataMessage.Payment(createPaymentNotification(payment));
        }
        throw new InvalidMessageStructureException("Unknown payment item");
    }

    private static SignalServiceDataMessage.StoryContext createStoryContext(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (!dataMessage.hasStoryContext()) {
            return null;
        }
        ServiceId parseOrNull = ServiceId.parseOrNull(dataMessage.getStoryContext().getAuthorUuid());
        if (parseOrNull != null) {
            return new SignalServiceDataMessage.StoryContext(parseOrNull, dataMessage.getStoryContext().getSentTimestamp());
        }
        throw new InvalidMessageStructureException("Invalid author ACI!");
    }

    private static SignalServiceDataMessage.GiftBadge createGiftBadge(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (!dataMessage.hasGiftBadge()) {
            return null;
        }
        if (dataMessage.getGiftBadge().hasReceiptCredentialPresentation()) {
            try {
                return new SignalServiceDataMessage.GiftBadge(new ReceiptCredentialPresentation(dataMessage.getGiftBadge().getReceiptCredentialPresentation().toByteArray()));
            } catch (InvalidInputException e) {
                throw new InvalidMessageStructureException(e);
            }
        } else {
            throw new InvalidMessageStructureException("GiftBadge does not contain a receipt credential presentation!");
        }
    }

    private static SignalServiceDataMessage.PaymentNotification createPaymentNotification(SignalServiceProtos.DataMessage.Payment payment) throws InvalidMessageStructureException {
        if (!payment.hasNotification() || payment.getNotification().getTransactionCase() != SignalServiceProtos.DataMessage.Payment.Notification.TransactionCase.MOBILECOIN) {
            throw new InvalidMessageStructureException("Badly-formatted payment notification!");
        }
        SignalServiceProtos.DataMessage.Payment.Notification notification = payment.getNotification();
        return new SignalServiceDataMessage.PaymentNotification(notification.getMobileCoin().getReceipt().toByteArray(), notification.getNote());
    }

    private static List<SharedContact> createSharedContacts(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (dataMessage.getContactCount() <= 0) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        for (SignalServiceProtos.DataMessage.Contact contact : dataMessage.getContactList()) {
            SharedContact.Builder name = SharedContact.newBuilder().setName(SharedContact.Name.newBuilder().setDisplay(contact.getName().getDisplayName()).setFamily(contact.getName().getFamilyName()).setGiven(contact.getName().getGivenName()).setMiddle(contact.getName().getMiddleName()).setPrefix(contact.getName().getPrefix()).setSuffix(contact.getName().getSuffix()).build());
            if (contact.getAddressCount() > 0) {
                for (SignalServiceProtos.DataMessage.Contact.PostalAddress postalAddress : contact.getAddressList()) {
                    SharedContact.PostalAddress.Type type = SharedContact.PostalAddress.Type.HOME;
                    int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$PostalAddress$Type[postalAddress.getType().ordinal()];
                    if (i == 1) {
                        type = SharedContact.PostalAddress.Type.WORK;
                    } else if (i != 2 && i == 3) {
                        type = SharedContact.PostalAddress.Type.CUSTOM;
                    }
                    name.withAddress(SharedContact.PostalAddress.newBuilder().setCity(postalAddress.getCity()).setCountry(postalAddress.getCountry()).setLabel(postalAddress.getLabel()).setNeighborhood(postalAddress.getNeighborhood()).setPobox(postalAddress.getPobox()).setPostcode(postalAddress.getPostcode()).setRegion(postalAddress.getRegion()).setStreet(postalAddress.getStreet()).setType(type).build());
                }
            }
            if (contact.getNumberCount() > 0) {
                for (SignalServiceProtos.DataMessage.Contact.Phone phone : contact.getNumberList()) {
                    SharedContact.Phone.Type type2 = SharedContact.Phone.Type.HOME;
                    int i2 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Phone$Type[phone.getType().ordinal()];
                    if (i2 != 1) {
                        if (i2 == 2) {
                            type2 = SharedContact.Phone.Type.WORK;
                        } else if (i2 == 3) {
                            type2 = SharedContact.Phone.Type.MOBILE;
                        } else if (i2 == 4) {
                            type2 = SharedContact.Phone.Type.CUSTOM;
                        }
                    }
                    name.withPhone(SharedContact.Phone.newBuilder().setLabel(phone.getLabel()).setType(type2).setValue(phone.getValue()).build());
                }
            }
            if (contact.getEmailCount() > 0) {
                for (SignalServiceProtos.DataMessage.Contact.Email email : contact.getEmailList()) {
                    SharedContact.Email.Type type3 = SharedContact.Email.Type.HOME;
                    int i3 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Email$Type[email.getType().ordinal()];
                    if (i3 != 1) {
                        if (i3 == 2) {
                            type3 = SharedContact.Email.Type.WORK;
                        } else if (i3 == 3) {
                            type3 = SharedContact.Email.Type.MOBILE;
                        } else if (i3 == 4) {
                            type3 = SharedContact.Email.Type.CUSTOM;
                        }
                    }
                    name.withEmail(SharedContact.Email.newBuilder().setLabel(email.getLabel()).setType(type3).setValue(email.getValue()).build());
                }
            }
            if (contact.hasAvatar()) {
                name.setAvatar(SharedContact.Avatar.newBuilder().withAttachment(createAttachmentPointer(contact.getAvatar().getAvatar())).withProfileFlag(contact.getAvatar().getIsProfile()).build());
            }
            if (contact.hasOrganization()) {
                name.withOrganization(contact.getOrganization());
            }
            linkedList.add(name.build());
        }
        return linkedList;
    }

    private static SignalServiceAttachmentPointer createAttachmentPointer(SignalServiceProtos.AttachmentPointer attachmentPointer) throws InvalidMessageStructureException {
        return AttachmentPointerUtil.createSignalAttachmentPointer(attachmentPointer);
    }

    /* renamed from: org.whispersystems.signalservice.api.messages.SignalServiceContent$1 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Email$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Phone$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$PostalAddress$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$ItemCase;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$FetchLatest$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$MessageRequestResponse$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$OutgoingPayment$PaymentDetailCase;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$StickerPackOperation$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style;

        static {
            int[] iArr = new int[SignalServiceProtos.TextAttachment.Style.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style = iArr;
            try {
                iArr[SignalServiceProtos.TextAttachment.Style.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style[SignalServiceProtos.TextAttachment.Style.REGULAR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style[SignalServiceProtos.TextAttachment.Style.BOLD.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style[SignalServiceProtos.TextAttachment.Style.SERIF.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style[SignalServiceProtos.TextAttachment.Style.SCRIPT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style[SignalServiceProtos.TextAttachment.Style.CONDENSED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[SignalServiceProtos.DataMessage.Contact.Email.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Email$Type = iArr2;
            try {
                iArr2[SignalServiceProtos.DataMessage.Contact.Email.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Email$Type[SignalServiceProtos.DataMessage.Contact.Email.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Email$Type[SignalServiceProtos.DataMessage.Contact.Email.Type.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Email$Type[SignalServiceProtos.DataMessage.Contact.Email.Type.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr3 = new int[SignalServiceProtos.DataMessage.Contact.Phone.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Phone$Type = iArr3;
            try {
                iArr3[SignalServiceProtos.DataMessage.Contact.Phone.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Phone$Type[SignalServiceProtos.DataMessage.Contact.Phone.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Phone$Type[SignalServiceProtos.DataMessage.Contact.Phone.Type.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$Phone$Type[SignalServiceProtos.DataMessage.Contact.Phone.Type.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused14) {
            }
            int[] iArr4 = new int[SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$PostalAddress$Type = iArr4;
            try {
                iArr4[SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.WORK.ordinal()] = 1;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$PostalAddress$Type[SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.HOME.ordinal()] = 2;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Contact$PostalAddress$Type[SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.CUSTOM.ordinal()] = 3;
            } catch (NoSuchFieldError unused17) {
            }
            int[] iArr5 = new int[SignalServiceProtos.DataMessage.Payment.ItemCase.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$ItemCase = iArr5;
            try {
                iArr5[SignalServiceProtos.DataMessage.Payment.ItemCase.NOTIFICATION.ordinal()] = 1;
            } catch (NoSuchFieldError unused18) {
            }
            int[] iArr6 = new int[SignalServiceProtos.SyncMessage.OutgoingPayment.PaymentDetailCase.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$OutgoingPayment$PaymentDetailCase = iArr6;
            try {
                iArr6[SignalServiceProtos.SyncMessage.OutgoingPayment.PaymentDetailCase.MOBILECOIN.ordinal()] = 1;
            } catch (NoSuchFieldError unused19) {
            }
            int[] iArr7 = new int[SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$MessageRequestResponse$Type = iArr7;
            try {
                iArr7[SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.ACCEPT.ordinal()] = 1;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$MessageRequestResponse$Type[SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$MessageRequestResponse$Type[SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.BLOCK.ordinal()] = 3;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$MessageRequestResponse$Type[SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.BLOCK_AND_DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused23) {
            }
            int[] iArr8 = new int[SignalServiceProtos.SyncMessage.FetchLatest.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$FetchLatest$Type = iArr8;
            try {
                iArr8[SignalServiceProtos.SyncMessage.FetchLatest.Type.LOCAL_PROFILE.ordinal()] = 1;
            } catch (NoSuchFieldError unused24) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$FetchLatest$Type[SignalServiceProtos.SyncMessage.FetchLatest.Type.STORAGE_MANIFEST.ordinal()] = 2;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$FetchLatest$Type[SignalServiceProtos.SyncMessage.FetchLatest.Type.SUBSCRIPTION_STATUS.ordinal()] = 3;
            } catch (NoSuchFieldError unused26) {
            }
            int[] iArr9 = new int[SignalServiceProtos.SyncMessage.StickerPackOperation.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$StickerPackOperation$Type = iArr9;
            try {
                iArr9[SignalServiceProtos.SyncMessage.StickerPackOperation.Type.INSTALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused27) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$SyncMessage$StickerPackOperation$Type[SignalServiceProtos.SyncMessage.StickerPackOperation.Type.REMOVE.ordinal()] = 2;
            } catch (NoSuchFieldError unused28) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment createTextAttachment(org.whispersystems.signalservice.internal.push.SignalServiceProtos.TextAttachment r9) throws org.whispersystems.signalservice.api.InvalidMessageStructureException {
        /*
            boolean r0 = r9.hasTextStyle()
            r1 = 0
            if (r0 == 0) goto L_0x0029
            int[] r0 = org.whispersystems.signalservice.api.messages.SignalServiceContent.AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$TextAttachment$Style
            org.whispersystems.signalservice.internal.push.SignalServiceProtos$TextAttachment$Style r2 = r9.getTextStyle()
            int r2 = r2.ordinal()
            r0 = r0[r2]
            switch(r0) {
                case 1: goto L_0x0026;
                case 2: goto L_0x0023;
                case 3: goto L_0x0020;
                case 4: goto L_0x001d;
                case 5: goto L_0x001a;
                case 6: goto L_0x0017;
                default: goto L_0x0016;
            }
        L_0x0016:
            goto L_0x0029
        L_0x0017:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Style r0 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.Style.CONDENSED
            goto L_0x002a
        L_0x001a:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Style r0 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.Style.SCRIPT
            goto L_0x002a
        L_0x001d:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Style r0 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.Style.SERIF
            goto L_0x002a
        L_0x0020:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Style r0 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.Style.BOLD
            goto L_0x002a
        L_0x0023:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Style r0 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.Style.REGULAR
            goto L_0x002a
        L_0x0026:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Style r0 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.Style.DEFAULT
            goto L_0x002a
        L_0x0029:
            r0 = r1
        L_0x002a:
            boolean r2 = r9.hasText()
            if (r2 == 0) goto L_0x0035
            java.lang.String r2 = r9.getText()
            goto L_0x0036
        L_0x0035:
            r2 = r1
        L_0x0036:
            j$.util.Optional r3 = j$.util.Optional.ofNullable(r2)
            boolean r2 = r9.hasTextForegroundColor()
            if (r2 == 0) goto L_0x0049
            int r2 = r9.getTextForegroundColor()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            goto L_0x004a
        L_0x0049:
            r2 = r1
        L_0x004a:
            j$.util.Optional r5 = j$.util.Optional.ofNullable(r2)
            boolean r2 = r9.hasTextBackgroundColor()
            if (r2 == 0) goto L_0x005d
            int r2 = r9.getTextBackgroundColor()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            goto L_0x005e
        L_0x005d:
            r2 = r1
        L_0x005e:
            j$.util.Optional r6 = j$.util.Optional.ofNullable(r2)
            boolean r2 = r9.hasPreview()
            if (r2 == 0) goto L_0x0071
            org.whispersystems.signalservice.internal.push.SignalServiceProtos$Preview r2 = r9.getPreview()
            org.whispersystems.signalservice.api.messages.SignalServicePreview r2 = createPreview(r2)
            goto L_0x0072
        L_0x0071:
            r2 = r1
        L_0x0072:
            j$.util.Optional r7 = j$.util.Optional.ofNullable(r2)
            boolean r2 = r9.hasGradient()
            if (r2 == 0) goto L_0x00c8
            org.whispersystems.signalservice.internal.push.SignalServiceProtos$TextAttachment$Gradient r9 = r9.getGradient()
            boolean r2 = r9.hasStartColor()
            if (r2 == 0) goto L_0x008f
            int r2 = r9.getStartColor()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            goto L_0x0090
        L_0x008f:
            r2 = r1
        L_0x0090:
            boolean r4 = r9.hasEndColor()
            if (r4 == 0) goto L_0x009f
            int r4 = r9.getEndColor()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            goto L_0x00a0
        L_0x009f:
            r4 = r1
        L_0x00a0:
            boolean r8 = r9.hasAngle()
            if (r8 == 0) goto L_0x00ae
            int r9 = r9.getAngle()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r9)
        L_0x00ae:
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Gradient r8 = new org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment$Gradient
            j$.util.Optional r9 = j$.util.Optional.ofNullable(r2)
            j$.util.Optional r2 = j$.util.Optional.ofNullable(r4)
            j$.util.Optional r1 = j$.util.Optional.ofNullable(r1)
            r8.<init>(r9, r2, r1)
            j$.util.Optional r4 = j$.util.Optional.ofNullable(r0)
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment r9 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.forGradientBackground(r3, r4, r5, r6, r7, r8)
            return r9
        L_0x00c8:
            j$.util.Optional r4 = j$.util.Optional.ofNullable(r0)
            int r8 = r9.getColor()
            org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment r9 = org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment.forSolidBackground(r3, r4, r5, r6, r7, r8)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.messages.SignalServiceContent.createTextAttachment(org.whispersystems.signalservice.internal.push.SignalServiceProtos$TextAttachment):org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment");
    }

    private static SignalServiceGroupV2 createGroupV2Info(SignalServiceProtos.StoryMessage storyMessage) throws InvalidMessageStructureException {
        if (!storyMessage.hasGroup()) {
            return null;
        }
        return createGroupV2Info(storyMessage.getGroup());
    }

    private static SignalServiceGroupV2 createGroupV2Info(SignalServiceProtos.DataMessage dataMessage) throws InvalidMessageStructureException {
        if (!dataMessage.hasGroupV2()) {
            return null;
        }
        return createGroupV2Info(dataMessage.getGroupV2());
    }

    private static SignalServiceGroupV2 createGroupV2Info(SignalServiceProtos.GroupContextV2 groupContextV2) throws InvalidMessageStructureException {
        if (groupContextV2 == null) {
            return null;
        }
        if (!groupContextV2.hasMasterKey()) {
            throw new InvalidMessageStructureException("No GV2 master key on message");
        } else if (groupContextV2.hasRevision()) {
            try {
                SignalServiceGroupV2.Builder withRevision = SignalServiceGroupV2.newBuilder(new GroupMasterKey(groupContextV2.getMasterKey().toByteArray())).withRevision(groupContextV2.getRevision());
                if (groupContextV2.hasGroupChange() && !groupContextV2.getGroupChange().isEmpty()) {
                    withRevision.withSignedGroupChange(groupContextV2.getGroupChange().toByteArray());
                }
                return withRevision.build();
            } catch (InvalidInputException unused) {
                throw new InvalidMessageStructureException("Invalid GV2 input!");
            }
        } else {
            throw new InvalidMessageStructureException("No GV2 revision on message");
        }
    }
}
