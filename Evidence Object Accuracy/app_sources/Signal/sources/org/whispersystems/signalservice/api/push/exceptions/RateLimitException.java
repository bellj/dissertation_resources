package org.whispersystems.signalservice.api.push.exceptions;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class RateLimitException extends NonSuccessfulResponseCodeException {
    private final Optional<Long> retryAfterMilliseconds;

    public RateLimitException(int i, String str) {
        this(i, str, Optional.empty());
    }

    public RateLimitException(int i, String str, Optional<Long> optional) {
        super(i, str);
        this.retryAfterMilliseconds = optional;
    }

    public Optional<Long> getRetryAfterMilliseconds() {
        return this.retryAfterMilliseconds;
    }
}
