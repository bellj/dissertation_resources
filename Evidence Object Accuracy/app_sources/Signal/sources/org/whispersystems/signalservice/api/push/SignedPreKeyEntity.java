package org.whispersystems.signalservice.api.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.whispersystems.signalservice.internal.push.PreKeyEntity;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public class SignedPreKeyEntity extends PreKeyEntity {
    @JsonProperty
    @JsonDeserialize(using = ByteArrayDeserializer.class)
    @JsonSerialize(using = ByteArraySerializer.class)
    private byte[] signature;

    public SignedPreKeyEntity() {
    }

    public SignedPreKeyEntity(int i, ECPublicKey eCPublicKey, byte[] bArr) {
        super(i, eCPublicKey);
        this.signature = bArr;
    }

    public byte[] getSignature() {
        return this.signature;
    }

    /* loaded from: classes5.dex */
    private static class ByteArraySerializer extends JsonSerializer<byte[]> {
        private ByteArraySerializer() {
        }

        public void serialize(byte[] bArr, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Base64.encodeBytesWithoutPadding(bArr));
        }
    }

    /* loaded from: classes5.dex */
    private static class ByteArrayDeserializer extends JsonDeserializer<byte[]> {
        private ByteArrayDeserializer() {
        }

        public byte[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return Base64.decodeWithoutPadding(jsonParser.getValueAsString());
        }
    }
}
