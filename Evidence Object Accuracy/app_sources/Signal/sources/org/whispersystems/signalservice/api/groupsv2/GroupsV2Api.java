package org.whispersystems.signalservice.api.groupsv2;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.auth.AuthCredentialWithPniResponse;
import org.signal.libsignal.zkgroup.auth.ClientZkAuthOperations;
import org.signal.libsignal.zkgroup.groups.ClientZkGroupCipher;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.signal.storageservice.protos.groups.AvatarUploadAttributes;
import org.signal.storageservice.protos.groups.Group;
import org.signal.storageservice.protos.groups.GroupAttributeBlob;
import org.signal.storageservice.protos.groups.GroupChange;
import org.signal.storageservice.protos.groups.GroupChanges;
import org.signal.storageservice.protos.groups.GroupExternalCredential;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;
import org.whispersystems.signalservice.api.groupsv2.GroupHistoryPage;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Operations;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.exceptions.ForbiddenException;

/* loaded from: classes5.dex */
public class GroupsV2Api {
    private final GroupsV2Operations groupsOperations;
    private final PushServiceSocket socket;

    public GroupsV2Api(PushServiceSocket pushServiceSocket, GroupsV2Operations groupsV2Operations) {
        this.socket = pushServiceSocket;
        this.groupsOperations = groupsV2Operations;
    }

    public HashMap<Long, AuthCredentialWithPniResponse> getCredentials(long j) throws IOException {
        return parseCredentialResponse(this.socket.retrieveGroupsV2Credentials(j));
    }

    public GroupsV2AuthorizationString getGroupsV2AuthorizationString(ServiceId serviceId, ServiceId serviceId2, long j, GroupSecretParams groupSecretParams, AuthCredentialWithPniResponse authCredentialWithPniResponse) throws VerificationFailedException {
        ClientZkAuthOperations authOperations = this.groupsOperations.getAuthOperations();
        return new GroupsV2AuthorizationString(groupSecretParams, authOperations.createAuthCredentialPresentation(new SecureRandom(), groupSecretParams, authOperations.receiveAuthCredentialWithPni(serviceId.uuid(), serviceId2.uuid(), j, authCredentialWithPniResponse)));
    }

    public void putNewGroup(GroupsV2Operations.NewGroup newGroup, GroupsV2AuthorizationString groupsV2AuthorizationString) throws IOException {
        Group newGroupMessage = newGroup.getNewGroupMessage();
        if (newGroup.getAvatar().isPresent()) {
            newGroupMessage = Group.newBuilder(newGroupMessage).setAvatar(uploadAvatar(newGroup.getAvatar().get(), newGroup.getGroupSecretParams(), groupsV2AuthorizationString)).build();
        }
        this.socket.putNewGroupsV2Group(newGroupMessage, groupsV2AuthorizationString);
    }

    public PartialDecryptedGroup getPartialDecryptedGroup(GroupSecretParams groupSecretParams, GroupsV2AuthorizationString groupsV2AuthorizationString) throws IOException, InvalidGroupStateException, VerificationFailedException {
        return this.groupsOperations.forGroup(groupSecretParams).partialDecryptGroup(this.socket.getGroupsV2Group(groupsV2AuthorizationString));
    }

    public DecryptedGroup getGroup(GroupSecretParams groupSecretParams, GroupsV2AuthorizationString groupsV2AuthorizationString) throws IOException, InvalidGroupStateException, VerificationFailedException {
        return this.groupsOperations.forGroup(groupSecretParams).decryptGroup(this.socket.getGroupsV2Group(groupsV2AuthorizationString));
    }

    public GroupHistoryPage getGroupHistoryPage(GroupSecretParams groupSecretParams, int i, GroupsV2AuthorizationString groupsV2AuthorizationString, boolean z) throws IOException, InvalidGroupStateException, VerificationFailedException {
        PushServiceSocket.GroupHistory groupsV2GroupHistory = this.socket.getGroupsV2GroupHistory(i, groupsV2AuthorizationString, 4, z);
        ArrayList arrayList = new ArrayList(groupsV2GroupHistory.getGroupChanges().getGroupChangesList().size());
        GroupsV2Operations.GroupOperations forGroup = this.groupsOperations.forGroup(groupSecretParams);
        for (GroupChanges.GroupChangeState groupChangeState : groupsV2GroupHistory.getGroupChanges().getGroupChangesList()) {
            arrayList.add(new DecryptedGroupHistoryEntry(groupChangeState.hasGroupState() ? Optional.of(forGroup.decryptGroup(groupChangeState.getGroupState())) : Optional.empty(), groupChangeState.hasGroupChange() ? forGroup.decryptChange(groupChangeState.getGroupChange(), false) : Optional.empty()));
        }
        return new GroupHistoryPage(arrayList, GroupHistoryPage.PagingData.fromGroup(groupsV2GroupHistory));
    }

    public DecryptedGroupJoinInfo getGroupJoinInfo(GroupSecretParams groupSecretParams, Optional<byte[]> optional, GroupsV2AuthorizationString groupsV2AuthorizationString) throws IOException, GroupLinkNotActiveException {
        try {
            return this.groupsOperations.forGroup(groupSecretParams).decryptGroupJoinInfo(this.socket.getGroupJoinInfo(optional, groupsV2AuthorizationString));
        } catch (ForbiddenException e) {
            throw new GroupLinkNotActiveException(null, e.getReason());
        }
    }

    public String uploadAvatar(byte[] bArr, GroupSecretParams groupSecretParams, GroupsV2AuthorizationString groupsV2AuthorizationString) throws IOException {
        AvatarUploadAttributes groupsV2AvatarUploadForm = this.socket.getGroupsV2AvatarUploadForm(groupsV2AuthorizationString.toString());
        try {
            this.socket.uploadGroupV2Avatar(new ClientZkGroupCipher(groupSecretParams).encryptBlob(GroupAttributeBlob.newBuilder().setAvatar(ByteString.copyFrom(bArr)).build().toByteArray()), groupsV2AvatarUploadForm);
            return groupsV2AvatarUploadForm.getKey();
        } catch (VerificationFailedException e) {
            throw new AssertionError(e);
        }
    }

    public GroupChange patchGroup(GroupChange.Actions actions, GroupsV2AuthorizationString groupsV2AuthorizationString, Optional<byte[]> optional) throws IOException {
        return this.socket.patchGroupsV2Group(actions, groupsV2AuthorizationString.toString(), optional);
    }

    public GroupExternalCredential getGroupExternalCredential(GroupsV2AuthorizationString groupsV2AuthorizationString) throws IOException {
        return this.socket.getGroupExternalCredential(groupsV2AuthorizationString);
    }

    private static HashMap<Long, AuthCredentialWithPniResponse> parseCredentialResponse(CredentialResponse credentialResponse) throws IOException {
        HashMap<Long, AuthCredentialWithPniResponse> hashMap = new HashMap<>();
        TemporalCredential[] credentials = credentialResponse.getCredentials();
        for (TemporalCredential temporalCredential : credentials) {
            try {
                hashMap.put(Long.valueOf(temporalCredential.getRedemptionTime()), new AuthCredentialWithPniResponse(temporalCredential.getCredential()));
            } catch (InvalidInputException e) {
                throw new IOException(e);
            }
        }
        return hashMap;
    }
}
