package org.whispersystems.signalservice.api.groupsv2;

import java.util.UUID;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyCredential;

/* loaded from: classes5.dex */
public final class UuidProfileKeyCredential {
    private final ProfileKeyCredential profileKeyCredential;
    private final UUID uuid;

    public UuidProfileKeyCredential(UUID uuid, ProfileKeyCredential profileKeyCredential) {
        this.uuid = uuid;
        this.profileKeyCredential = profileKeyCredential;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public ProfileKeyCredential getProfileKeyCredential() {
        return this.profileKeyCredential;
    }
}
