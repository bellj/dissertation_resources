package org.whispersystems.signalservice.api.groupsv2;

import com.google.protobuf.ByteString;
import java.util.HashMap;
import java.util.List;
import org.signal.storageservice.protos.groups.GroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedApproveMember;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;

/* loaded from: classes5.dex */
public final class GroupChangeUtil {
    private GroupChangeUtil() {
    }

    public static boolean changeIsEmpty(GroupChange.Actions actions) {
        return actions.getAddMembersCount() == 0 && actions.getDeleteMembersCount() == 0 && actions.getModifyMemberRolesCount() == 0 && actions.getModifyMemberProfileKeysCount() == 0 && actions.getAddPendingMembersCount() == 0 && actions.getDeletePendingMembersCount() == 0 && actions.getPromotePendingMembersCount() == 0 && !actions.hasModifyTitle() && !actions.hasModifyAvatar() && !actions.hasModifyDisappearingMessagesTimer() && !actions.hasModifyAttributesAccess() && !actions.hasModifyMemberAccess() && !actions.hasModifyAddFromInviteLinkAccess() && actions.getAddRequestingMembersCount() == 0 && actions.getDeleteRequestingMembersCount() == 0 && actions.getPromoteRequestingMembersCount() == 0 && !actions.hasModifyInviteLinkPassword() && !actions.hasModifyDescription() && !actions.hasModifyAnnouncementsOnly() && actions.getAddBannedMembersCount() == 0 && actions.getDeleteBannedMembersCount() == 0 && actions.getPromotePendingPniAciMembersCount() == 0;
    }

    public static GroupChange.Actions.Builder resolveConflict(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, GroupChange.Actions actions) {
        GroupChange.Actions.Builder newBuilder = GroupChange.Actions.newBuilder(actions);
        resolveConflict(decryptedGroup, decryptedGroupChange, new GroupChangeActionsBuilderChangeSetModifier(newBuilder));
        return newBuilder;
    }

    public static DecryptedGroupChange.Builder resolveConflict(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) {
        DecryptedGroupChange.Builder newBuilder = DecryptedGroupChange.newBuilder(decryptedGroupChange);
        resolveConflict(decryptedGroup, decryptedGroupChange, new DecryptedGroupChangeActionsBuilderChangeSetModifier(newBuilder));
        return newBuilder;
    }

    private static void resolveConflict(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        HashMap hashMap = new HashMap(decryptedGroup.getMembersCount());
        HashMap hashMap2 = new HashMap(decryptedGroup.getPendingMembersCount());
        HashMap hashMap3 = new HashMap(decryptedGroup.getMembersCount());
        HashMap hashMap4 = new HashMap(decryptedGroup.getBannedMembersCount());
        for (DecryptedMember decryptedMember : decryptedGroup.getMembersList()) {
            hashMap.put(decryptedMember.getUuid(), decryptedMember);
        }
        for (DecryptedPendingMember decryptedPendingMember : decryptedGroup.getPendingMembersList()) {
            hashMap2.put(decryptedPendingMember.getUuid(), decryptedPendingMember);
        }
        for (DecryptedRequestingMember decryptedRequestingMember : decryptedGroup.getRequestingMembersList()) {
            hashMap3.put(decryptedRequestingMember.getUuid(), decryptedRequestingMember);
        }
        for (DecryptedBannedMember decryptedBannedMember : decryptedGroup.getBannedMembersList()) {
            hashMap4.put(decryptedBannedMember.getUuid(), decryptedBannedMember);
        }
        resolveField3AddMembers(decryptedGroupChange, changeSetModifier, hashMap, hashMap2);
        resolveField4DeleteMembers(decryptedGroupChange, changeSetModifier, hashMap);
        resolveField5ModifyMemberRoles(decryptedGroupChange, changeSetModifier, hashMap);
        resolveField6ModifyProfileKeys(decryptedGroupChange, changeSetModifier, hashMap);
        resolveField7AddPendingMembers(decryptedGroupChange, changeSetModifier, hashMap, hashMap2);
        resolveField8DeletePendingMembers(decryptedGroupChange, changeSetModifier, hashMap2);
        resolveField9PromotePendingMembers(decryptedGroupChange, changeSetModifier, hashMap2);
        resolveField10ModifyTitle(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField11ModifyAvatar(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField12modifyDisappearingMessagesTimer(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField13modifyAttributesAccess(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField14modifyAttributesAccess(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField15modifyAddFromInviteLinkAccess(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField16AddRequestingMembers(decryptedGroupChange, changeSetModifier, hashMap, hashMap2);
        resolveField17DeleteMembers(decryptedGroupChange, changeSetModifier, hashMap3);
        resolveField18PromoteRequestingMembers(decryptedGroupChange, changeSetModifier, hashMap3);
        resolveField20ModifyDescription(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField21ModifyAnnouncementsOnly(decryptedGroup, decryptedGroupChange, changeSetModifier);
        resolveField22AddBannedMembers(decryptedGroupChange, changeSetModifier, hashMap4);
        resolveField23DeleteBannedMembers(decryptedGroupChange, changeSetModifier, hashMap4);
        resolveField24PromotePendingPniAciMembers(decryptedGroupChange, changeSetModifier, hashMap);
    }

    private static void resolveField3AddMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap, HashMap<ByteString, DecryptedPendingMember> hashMap2) {
        List<DecryptedMember> newMembersList = decryptedGroupChange.getNewMembersList();
        for (int size = newMembersList.size() - 1; size >= 0; size--) {
            DecryptedMember decryptedMember = newMembersList.get(size);
            if (hashMap.containsKey(decryptedMember.getUuid())) {
                changeSetModifier.removeAddMembers(size);
            } else if (hashMap2.containsKey(decryptedMember.getUuid())) {
                changeSetModifier.moveAddToPromote(size);
            }
        }
    }

    private static void resolveField4DeleteMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap) {
        List<ByteString> deleteMembersList = decryptedGroupChange.getDeleteMembersList();
        for (int size = deleteMembersList.size() - 1; size >= 0; size--) {
            if (!hashMap.containsKey(deleteMembersList.get(size))) {
                changeSetModifier.removeDeleteMembers(size);
            }
        }
    }

    private static void resolveField5ModifyMemberRoles(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap) {
        List<DecryptedModifyMemberRole> modifyMemberRolesList = decryptedGroupChange.getModifyMemberRolesList();
        for (int size = modifyMemberRolesList.size() - 1; size >= 0; size--) {
            DecryptedModifyMemberRole decryptedModifyMemberRole = modifyMemberRolesList.get(size);
            DecryptedMember decryptedMember = hashMap.get(decryptedModifyMemberRole.getUuid());
            if (decryptedMember == null || decryptedMember.getRole() == decryptedModifyMemberRole.getRole()) {
                changeSetModifier.removeModifyMemberRoles(size);
            }
        }
    }

    private static void resolveField6ModifyProfileKeys(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap) {
        List<DecryptedMember> modifiedProfileKeysList = decryptedGroupChange.getModifiedProfileKeysList();
        for (int size = modifiedProfileKeysList.size() - 1; size >= 0; size--) {
            DecryptedMember decryptedMember = modifiedProfileKeysList.get(size);
            DecryptedMember decryptedMember2 = hashMap.get(decryptedMember.getUuid());
            if (decryptedMember2 == null || decryptedMember.getProfileKey().equals(decryptedMember2.getProfileKey())) {
                changeSetModifier.removeModifyMemberProfileKeys(size);
            }
        }
    }

    private static void resolveField7AddPendingMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap, HashMap<ByteString, DecryptedPendingMember> hashMap2) {
        List<DecryptedPendingMember> newPendingMembersList = decryptedGroupChange.getNewPendingMembersList();
        for (int size = newPendingMembersList.size() - 1; size >= 0; size--) {
            DecryptedPendingMember decryptedPendingMember = newPendingMembersList.get(size);
            if (hashMap.containsKey(decryptedPendingMember.getUuid()) || hashMap2.containsKey(decryptedPendingMember.getUuid())) {
                changeSetModifier.removeAddPendingMembers(size);
            }
        }
    }

    private static void resolveField8DeletePendingMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedPendingMember> hashMap) {
        List<DecryptedPendingMemberRemoval> deletePendingMembersList = decryptedGroupChange.getDeletePendingMembersList();
        for (int size = deletePendingMembersList.size() - 1; size >= 0; size--) {
            if (!hashMap.containsKey(deletePendingMembersList.get(size).getUuid())) {
                changeSetModifier.removeDeletePendingMembers(size);
            }
        }
    }

    private static void resolveField9PromotePendingMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedPendingMember> hashMap) {
        List<DecryptedMember> promotePendingMembersList = decryptedGroupChange.getPromotePendingMembersList();
        for (int size = promotePendingMembersList.size() - 1; size >= 0; size--) {
            if (!hashMap.containsKey(promotePendingMembersList.get(size).getUuid())) {
                changeSetModifier.removePromotePendingMembers(size);
            }
        }
    }

    private static void resolveField10ModifyTitle(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.hasNewTitle() && decryptedGroupChange.getNewTitle().getValue().equals(decryptedGroup.getTitle())) {
            changeSetModifier.clearModifyTitle();
        }
    }

    private static void resolveField11ModifyAvatar(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.hasNewAvatar() && decryptedGroupChange.getNewAvatar().getValue().equals(decryptedGroup.getAvatar())) {
            changeSetModifier.clearModifyAvatar();
        }
    }

    private static void resolveField12modifyDisappearingMessagesTimer(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.hasNewTimer() && decryptedGroupChange.getNewTimer().getDuration() == decryptedGroup.getDisappearingMessagesTimer().getDuration()) {
            changeSetModifier.clearModifyDisappearingMessagesTimer();
        }
    }

    private static void resolveField13modifyAttributesAccess(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.getNewAttributeAccess() == decryptedGroup.getAccessControl().getAttributes()) {
            changeSetModifier.clearModifyAttributesAccess();
        }
    }

    private static void resolveField14modifyAttributesAccess(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.getNewMemberAccess() == decryptedGroup.getAccessControl().getMembers()) {
            changeSetModifier.clearModifyMemberAccess();
        }
    }

    private static void resolveField15modifyAddFromInviteLinkAccess(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.getNewInviteLinkAccess() == decryptedGroup.getAccessControl().getAddFromInviteLink()) {
            changeSetModifier.clearModifyAddFromInviteLinkAccess();
        }
    }

    private static void resolveField16AddRequestingMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap, HashMap<ByteString, DecryptedPendingMember> hashMap2) {
        List<DecryptedRequestingMember> newRequestingMembersList = decryptedGroupChange.getNewRequestingMembersList();
        for (int size = newRequestingMembersList.size() - 1; size >= 0; size--) {
            DecryptedRequestingMember decryptedRequestingMember = newRequestingMembersList.get(size);
            if (hashMap.containsKey(decryptedRequestingMember.getUuid())) {
                changeSetModifier.removeAddRequestingMembers(size);
            } else if (hashMap2.containsKey(decryptedRequestingMember.getUuid())) {
                changeSetModifier.moveAddRequestingMembersToPromote(size);
            }
        }
    }

    private static void resolveField17DeleteMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedRequestingMember> hashMap) {
        List<ByteString> deleteRequestingMembersList = decryptedGroupChange.getDeleteRequestingMembersList();
        for (int size = deleteRequestingMembersList.size() - 1; size >= 0; size--) {
            if (!hashMap.containsKey(deleteRequestingMembersList.get(size))) {
                changeSetModifier.removeDeleteRequestingMembers(size);
            }
        }
    }

    private static void resolveField18PromoteRequestingMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedRequestingMember> hashMap) {
        List<DecryptedApproveMember> promoteRequestingMembersList = decryptedGroupChange.getPromoteRequestingMembersList();
        for (int size = promoteRequestingMembersList.size() - 1; size >= 0; size--) {
            if (!hashMap.containsKey(promoteRequestingMembersList.get(size).getUuid())) {
                changeSetModifier.removePromoteRequestingMembers(size);
            }
        }
    }

    private static void resolveField20ModifyDescription(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.hasNewDescription() && decryptedGroupChange.getNewDescription().getValue().equals(decryptedGroup.getDescription())) {
            changeSetModifier.clearModifyDescription();
        }
    }

    private static void resolveField21ModifyAnnouncementsOnly(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier) {
        if (decryptedGroupChange.getNewIsAnnouncementGroup().equals(decryptedGroup.getIsAnnouncementGroup())) {
            changeSetModifier.clearModifyAnnouncementsOnly();
        }
    }

    private static void resolveField22AddBannedMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedBannedMember> hashMap) {
        List<DecryptedBannedMember> newBannedMembersList = decryptedGroupChange.getNewBannedMembersList();
        for (int size = newBannedMembersList.size() - 1; size >= 0; size--) {
            if (hashMap.containsKey(newBannedMembersList.get(size).getUuid())) {
                changeSetModifier.removeAddBannedMembers(size);
            }
        }
    }

    private static void resolveField23DeleteBannedMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedBannedMember> hashMap) {
        List<DecryptedBannedMember> deleteBannedMembersList = decryptedGroupChange.getDeleteBannedMembersList();
        for (int size = deleteBannedMembersList.size() - 1; size >= 0; size--) {
            if (!hashMap.containsKey(deleteBannedMembersList.get(size).getUuid())) {
                changeSetModifier.removeDeleteBannedMembers(size);
            }
        }
    }

    private static void resolveField24PromotePendingPniAciMembers(DecryptedGroupChange decryptedGroupChange, ChangeSetModifier changeSetModifier, HashMap<ByteString, DecryptedMember> hashMap) {
        List<DecryptedMember> promotePendingPniAciMembersList = decryptedGroupChange.getPromotePendingPniAciMembersList();
        for (int size = promotePendingPniAciMembersList.size() - 1; size >= 0; size--) {
            if (hashMap.containsKey(promotePendingPniAciMembersList.get(size).getUuid())) {
                changeSetModifier.removePromotePendingPniAciMembers(size);
            }
        }
    }
}
