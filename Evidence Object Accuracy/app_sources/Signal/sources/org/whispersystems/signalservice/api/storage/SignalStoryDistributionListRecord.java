package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.signal.libsignal.protocol.logging.Log;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.storage.StorageSyncModels$$ExternalSyntheticLambda0;
import org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda6;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.ProtoUtil;
import org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecord;

/* loaded from: classes5.dex */
public class SignalStoryDistributionListRecord implements SignalRecord {
    private static final String TAG;
    private final boolean hasUnknownFields;
    private final StorageId id;
    private final StoryDistributionListRecord proto;
    private final List<SignalServiceAddress> recipients;

    public SignalStoryDistributionListRecord(StorageId storageId, StoryDistributionListRecord storyDistributionListRecord) {
        this.id = storageId;
        this.proto = storyDistributionListRecord;
        this.hasUnknownFields = ProtoUtil.hasUnknownFields(storyDistributionListRecord);
        this.recipients = (List) Collection$EL.stream(storyDistributionListRecord.getRecipientUuidsList()).map(new Function() { // from class: org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ServiceId.parseOrNull((String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).filter(new Predicate() { // from class: org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord$$ExternalSyntheticLambda1
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((ServiceId) obj);
            }
        }).map(new StorageSyncModels$$ExternalSyntheticLambda0()).collect(Collectors.toList());
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public StorageId getId() {
        return this.id;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public SignalStorageRecord asStorageRecord() {
        return SignalStorageRecord.forStoryDistributionList(this);
    }

    public StoryDistributionListRecord toProto() {
        return this.proto;
    }

    public byte[] serializeUnknownFields() {
        if (this.hasUnknownFields) {
            return this.proto.toByteArray();
        }
        return null;
    }

    public byte[] getIdentifier() {
        return this.proto.getIdentifier().toByteArray();
    }

    public String getName() {
        return this.proto.getName();
    }

    public List<SignalServiceAddress> getRecipients() {
        return this.recipients;
    }

    public long getDeletedAtTimestamp() {
        return this.proto.getDeletedAtTimestamp();
    }

    public boolean allowsReplies() {
        return this.proto.getAllowsReplies();
    }

    public boolean isBlockList() {
        return this.proto.getIsBlockList();
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public String describeDiff(SignalRecord signalRecord) {
        if (signalRecord instanceof SignalStoryDistributionListRecord) {
            SignalStoryDistributionListRecord signalStoryDistributionListRecord = (SignalStoryDistributionListRecord) signalRecord;
            LinkedList linkedList = new LinkedList();
            if (!Arrays.equals(this.id.getRaw(), signalStoryDistributionListRecord.id.getRaw())) {
                linkedList.add("ID");
            }
            if (!Arrays.equals(getIdentifier(), signalStoryDistributionListRecord.getIdentifier())) {
                linkedList.add("Identifier");
            }
            if (!Objects.equals(getName(), signalStoryDistributionListRecord.getName())) {
                linkedList.add("Name");
            }
            if (!Objects.equals(this.recipients, signalStoryDistributionListRecord.recipients)) {
                linkedList.add("RecipientUuids");
            }
            if (getDeletedAtTimestamp() != signalStoryDistributionListRecord.getDeletedAtTimestamp()) {
                linkedList.add("DeletedAtTimestamp");
            }
            if (allowsReplies() != signalStoryDistributionListRecord.allowsReplies()) {
                linkedList.add("AllowsReplies");
            }
            if (isBlockList() != signalStoryDistributionListRecord.isBlockList()) {
                linkedList.add("BlockList");
            }
            return linkedList.toString();
        }
        return "Different class. " + getClass().getSimpleName() + " | " + signalRecord.getClass().getSimpleName();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SignalStoryDistributionListRecord signalStoryDistributionListRecord = (SignalStoryDistributionListRecord) obj;
        if (!this.id.equals(signalStoryDistributionListRecord.id) || !this.proto.equals(signalStoryDistributionListRecord.proto)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.proto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        private final StoryDistributionListRecord.Builder builder;
        private final StorageId id;

        public Builder(byte[] bArr, byte[] bArr2) {
            this.id = StorageId.forStoryDistributionList(bArr);
            if (bArr2 != null) {
                this.builder = parseUnknowns(bArr2);
            } else {
                this.builder = StoryDistributionListRecord.newBuilder();
            }
        }

        public Builder setIdentifier(byte[] bArr) {
            this.builder.setIdentifier(ByteString.copyFrom(bArr));
            return this;
        }

        public Builder setName(String str) {
            this.builder.setName(str);
            return this;
        }

        public Builder setRecipients(List<SignalServiceAddress> list) {
            this.builder.clearRecipientUuids();
            this.builder.addAllRecipientUuids((Iterable) Collection$EL.stream(list).map(new SignalServiceMessageSender$$ExternalSyntheticLambda6()).collect(Collectors.toList()));
            return this;
        }

        public Builder setDeletedAtTimestamp(long j) {
            this.builder.setDeletedAtTimestamp(j);
            return this;
        }

        public Builder setAllowsReplies(boolean z) {
            this.builder.setAllowsReplies(z);
            return this;
        }

        public Builder setIsBlockList(boolean z) {
            this.builder.setIsBlockList(z);
            return this;
        }

        public SignalStoryDistributionListRecord build() {
            return new SignalStoryDistributionListRecord(this.id, this.builder.build());
        }

        private static StoryDistributionListRecord.Builder parseUnknowns(byte[] bArr) {
            try {
                return StoryDistributionListRecord.parseFrom(bArr).toBuilder();
            } catch (InvalidProtocolBufferException e) {
                Log.w(SignalStoryDistributionListRecord.TAG, "Failed to combine unknown fields!", e);
                return StoryDistributionListRecord.newBuilder();
            }
        }
    }
}
