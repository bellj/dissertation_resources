package org.whispersystems.signalservice.api.messages.multidevice;

import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes5.dex */
public class ViewOnceOpenMessage {
    private final ServiceId sender;
    private final long timestamp;

    public ViewOnceOpenMessage(ServiceId serviceId, long j) {
        this.sender = serviceId;
        this.timestamp = j;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public ServiceId getSender() {
        return this.sender;
    }
}
