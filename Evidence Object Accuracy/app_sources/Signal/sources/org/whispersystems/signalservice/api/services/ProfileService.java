package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyCredentialRequestContext;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.ServiceResponseProcessor;
import org.whispersystems.signalservice.internal.push.IdentityCheckRequest;
import org.whispersystems.signalservice.internal.push.IdentityCheckResponse;
import org.whispersystems.signalservice.internal.push.http.AcceptLanguagesUtil;
import org.whispersystems.signalservice.internal.util.Hex;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;

/* loaded from: classes5.dex */
public final class ProfileService {
    private static final String TAG;
    private final ClientZkProfileOperations clientZkProfileOperations;
    private final SignalServiceMessageReceiver receiver;
    private final SignalWebSocket signalWebSocket;

    public ProfileService(ClientZkProfileOperations clientZkProfileOperations, SignalServiceMessageReceiver signalServiceMessageReceiver, SignalWebSocket signalWebSocket) {
        this.clientZkProfileOperations = clientZkProfileOperations;
        this.receiver = signalServiceMessageReceiver;
        this.signalWebSocket = signalWebSocket;
    }

    public Single<ServiceResponse<ProfileAndCredential>> getProfile(SignalServiceAddress signalServiceAddress, Optional<ProfileKey> optional, Optional<UnidentifiedAccess> optional2, SignalServiceProfile.RequestType requestType, Locale locale) {
        ServiceId serviceId = signalServiceAddress.getServiceId();
        SecureRandom secureRandom = new SecureRandom();
        WebSocketProtos.WebSocketRequestMessage.Builder verb = WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(secureRandom.nextLong()).setVerb("GET");
        ProfileKeyCredentialRequestContext profileKeyCredentialRequestContext = null;
        if (optional.isPresent()) {
            String serialize = optional.get().getProfileKeyVersion(serviceId.uuid()).serialize();
            if (requestType == SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL) {
                profileKeyCredentialRequestContext = this.clientZkProfileOperations.createProfileKeyCredentialRequestContext(secureRandom, serviceId.uuid(), optional.get());
                verb.setPath(String.format("/v1/profile/%s/%s/%s?credentialType=expiringProfileKey", serviceId, serialize, Hex.toStringCondensed(profileKeyCredentialRequestContext.getRequest().serialize())));
            } else {
                verb.setPath(String.format("/v1/profile/%s/%s", serviceId, serialize));
            }
        } else {
            verb.setPath(String.format("/v1/profile/%s", signalServiceAddress.getIdentifier()));
        }
        verb.addHeaders(AcceptLanguagesUtil.getAcceptLanguageHeader(locale));
        ResponseMapper build = DefaultResponseMapper.extend(ProfileAndCredential.class).withResponseMapper(new ProfileResponseMapper(requestType, profileKeyCredentialRequestContext)).build();
        Single<WebsocketResponse> request = this.signalWebSocket.request(verb.build(), optional2);
        Objects.requireNonNull(build);
        return request.map(new MessagingService$$ExternalSyntheticLambda4(build)).onErrorResumeNext(new Function(signalServiceAddress, optional, optional2, requestType, locale) { // from class: org.whispersystems.signalservice.api.services.ProfileService$$ExternalSyntheticLambda5
            public final /* synthetic */ SignalServiceAddress f$1;
            public final /* synthetic */ Optional f$2;
            public final /* synthetic */ Optional f$3;
            public final /* synthetic */ SignalServiceProfile.RequestType f$4;
            public final /* synthetic */ Locale f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileService.this.lambda$getProfile$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (Throwable) obj);
            }
        }).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    public /* synthetic */ SingleSource lambda$getProfile$0(SignalServiceAddress signalServiceAddress, Optional optional, Optional optional2, SignalServiceProfile.RequestType requestType, Locale locale, Throwable th) throws Throwable {
        return getProfileRestFallback(signalServiceAddress, optional, optional2, requestType, locale);
    }

    public Single<ServiceResponse<IdentityCheckResponse>> performIdentityCheck(Map<ServiceId, IdentityKey> map, Optional<UnidentifiedAccess> optional) {
        IdentityCheckRequest identityCheckRequest = new IdentityCheckRequest((List) Collection$EL.stream(map.entrySet()).map(new j$.util.function.Function() { // from class: org.whispersystems.signalservice.api.services.ProfileService$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ProfileService.lambda$performIdentityCheck$1((Map.Entry) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()));
        WebSocketProtos.WebSocketRequestMessage.Builder body = WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(new SecureRandom().nextLong()).setVerb("POST").setPath("/v1/profile/identity_check/batch").addAllHeaders(Collections.singleton("content-type:application/json")).setBody(JsonUtil.toJsonByteString(identityCheckRequest));
        DefaultResponseMapper defaultResponseMapper = DefaultResponseMapper.getDefault(IdentityCheckResponse.class);
        Single<WebsocketResponse> request = this.signalWebSocket.request(body.build(), optional);
        Objects.requireNonNull(defaultResponseMapper);
        return request.map(new MessagingService$$ExternalSyntheticLambda4(defaultResponseMapper)).onErrorResumeNext(new io.reactivex.rxjava3.functions.Function(identityCheckRequest, optional, defaultResponseMapper) { // from class: org.whispersystems.signalservice.api.services.ProfileService$$ExternalSyntheticLambda1
            public final /* synthetic */ IdentityCheckRequest f$1;
            public final /* synthetic */ Optional f$2;
            public final /* synthetic */ ResponseMapper f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileService.this.lambda$performIdentityCheck$2(this.f$1, this.f$2, this.f$3, (Throwable) obj);
            }
        }).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    public static /* synthetic */ IdentityCheckRequest.AciFingerprintPair lambda$performIdentityCheck$1(Map.Entry entry) {
        return new IdentityCheckRequest.AciFingerprintPair((ServiceId) entry.getKey(), (IdentityKey) entry.getValue());
    }

    public /* synthetic */ SingleSource lambda$performIdentityCheck$2(IdentityCheckRequest identityCheckRequest, Optional optional, ResponseMapper responseMapper, Throwable th) throws Throwable {
        return performIdentityCheckRestFallback(identityCheckRequest, optional, responseMapper);
    }

    private Single<ServiceResponse<ProfileAndCredential>> getProfileRestFallback(SignalServiceAddress signalServiceAddress, Optional<ProfileKey> optional, Optional<UnidentifiedAccess> optional2, SignalServiceProfile.RequestType requestType, Locale locale) {
        return Single.fromFuture(this.receiver.retrieveProfile(signalServiceAddress, optional, optional2, requestType, locale), 10, TimeUnit.SECONDS).onErrorResumeNext(new io.reactivex.rxjava3.functions.Function(signalServiceAddress, optional, requestType, locale) { // from class: org.whispersystems.signalservice.api.services.ProfileService$$ExternalSyntheticLambda3
            public final /* synthetic */ SignalServiceAddress f$1;
            public final /* synthetic */ Optional f$2;
            public final /* synthetic */ SignalServiceProfile.RequestType f$3;
            public final /* synthetic */ Locale f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileService.this.lambda$getProfileRestFallback$3(this.f$1, this.f$2, this.f$3, this.f$4, (Throwable) obj);
            }
        }).map(new io.reactivex.rxjava3.functions.Function() { // from class: org.whispersystems.signalservice.api.services.ProfileService$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ServiceResponse.forResult((ProfileAndCredential) obj, 0, null);
            }
        });
    }

    public /* synthetic */ SingleSource lambda$getProfileRestFallback$3(SignalServiceAddress signalServiceAddress, Optional optional, SignalServiceProfile.RequestType requestType, Locale locale, Throwable th) throws Throwable {
        return Single.fromFuture(this.receiver.retrieveProfile(signalServiceAddress, optional, Optional.empty(), requestType, locale), 10, TimeUnit.SECONDS);
    }

    private Single<ServiceResponse<IdentityCheckResponse>> performIdentityCheckRestFallback(IdentityCheckRequest identityCheckRequest, Optional<UnidentifiedAccess> optional, ResponseMapper<IdentityCheckResponse> responseMapper) {
        return this.receiver.performIdentityCheck(identityCheckRequest, optional, responseMapper).onErrorResumeNext(new io.reactivex.rxjava3.functions.Function(identityCheckRequest, responseMapper) { // from class: org.whispersystems.signalservice.api.services.ProfileService$$ExternalSyntheticLambda2
            public final /* synthetic */ IdentityCheckRequest f$1;
            public final /* synthetic */ ResponseMapper f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ProfileService.this.lambda$performIdentityCheckRestFallback$5(this.f$1, this.f$2, (Throwable) obj);
            }
        });
    }

    public /* synthetic */ SingleSource lambda$performIdentityCheckRestFallback$5(IdentityCheckRequest identityCheckRequest, ResponseMapper responseMapper, Throwable th) throws Throwable {
        return this.receiver.performIdentityCheck(identityCheckRequest, Optional.empty(), responseMapper);
    }

    /* loaded from: classes5.dex */
    public class ProfileResponseMapper implements DefaultResponseMapper.CustomResponseMapper<ProfileAndCredential> {
        private final ProfileKeyCredentialRequestContext requestContext;
        private final SignalServiceProfile.RequestType requestType;

        public ProfileResponseMapper(SignalServiceProfile.RequestType requestType, ProfileKeyCredentialRequestContext profileKeyCredentialRequestContext) {
            ProfileService.this = r1;
            this.requestType = requestType;
            this.requestContext = profileKeyCredentialRequestContext;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper.CustomResponseMapper
        public ServiceResponse<ProfileAndCredential> map(int i, String str, j$.util.function.Function<String, String> function, boolean z) throws MalformedResponseException {
            try {
                SignalServiceProfile signalServiceProfile = (SignalServiceProfile) JsonUtil.fromJsonResponse(str, SignalServiceProfile.class);
                ExpiringProfileKeyCredential expiringProfileKeyCredential = null;
                if (!(this.requestContext == null || signalServiceProfile.getExpiringProfileKeyCredentialResponse() == null)) {
                    expiringProfileKeyCredential = ProfileService.this.clientZkProfileOperations.receiveExpiringProfileKeyCredential(this.requestContext, signalServiceProfile.getExpiringProfileKeyCredentialResponse());
                }
                return ServiceResponse.forResult(new ProfileAndCredential(signalServiceProfile, this.requestType, Optional.ofNullable(expiringProfileKeyCredential)), i, str);
            } catch (VerificationFailedException e) {
                return ServiceResponse.forApplicationError(e, i, str);
            }
        }
    }

    /* loaded from: classes5.dex */
    public static final class ProfileResponseProcessor extends ServiceResponseProcessor<ProfileAndCredential> {
        public ProfileResponseProcessor(ServiceResponse<ProfileAndCredential> serviceResponse) {
            super(serviceResponse);
        }

        public <T> Pair<T, ProfileAndCredential> getResult(T t) {
            return new Pair<>(t, getResult());
        }

        @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
        public boolean notFound() {
            return super.notFound();
        }

        @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
        public boolean genericIoError() {
            return super.genericIoError();
        }

        @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
        public Throwable getError() {
            return super.getError();
        }
    }
}
