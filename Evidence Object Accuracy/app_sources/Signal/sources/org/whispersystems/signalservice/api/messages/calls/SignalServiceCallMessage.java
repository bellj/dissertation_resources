package org.whispersystems.signalservice.api.messages.calls;

import j$.util.Optional;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes5.dex */
public class SignalServiceCallMessage {
    private final Optional<AnswerMessage> answerMessage;
    private final Optional<BusyMessage> busyMessage;
    private final Optional<Integer> destinationDeviceId;
    private final Optional<byte[]> groupId;
    private final Optional<HangupMessage> hangupMessage;
    private final Optional<List<IceUpdateMessage>> iceUpdateMessages;
    private final boolean isMultiRing;
    private final Optional<OfferMessage> offerMessage;
    private final Optional<OpaqueMessage> opaqueMessage;
    private final Optional<Long> timestamp;

    private SignalServiceCallMessage(Optional<OfferMessage> optional, Optional<AnswerMessage> optional2, Optional<List<IceUpdateMessage>> optional3, Optional<HangupMessage> optional4, Optional<BusyMessage> optional5, Optional<OpaqueMessage> optional6, boolean z, Optional<Integer> optional7) {
        this(optional, optional2, optional3, optional4, optional5, optional6, z, optional7, Optional.empty(), Optional.empty());
    }

    private SignalServiceCallMessage(Optional<OfferMessage> optional, Optional<AnswerMessage> optional2, Optional<List<IceUpdateMessage>> optional3, Optional<HangupMessage> optional4, Optional<BusyMessage> optional5, Optional<OpaqueMessage> optional6, boolean z, Optional<Integer> optional7, Optional<byte[]> optional8, Optional<Long> optional9) {
        this.offerMessage = optional;
        this.answerMessage = optional2;
        this.iceUpdateMessages = optional3;
        this.hangupMessage = optional4;
        this.busyMessage = optional5;
        this.opaqueMessage = optional6;
        this.isMultiRing = z;
        this.destinationDeviceId = optional7;
        this.groupId = optional8;
        this.timestamp = optional9;
    }

    public static SignalServiceCallMessage forOffer(OfferMessage offerMessage, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.of(offerMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forAnswer(AnswerMessage answerMessage, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.empty(), Optional.of(answerMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forIceUpdates(List<IceUpdateMessage> list, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.of(list), Optional.empty(), Optional.empty(), Optional.empty(), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forIceUpdate(IceUpdateMessage iceUpdateMessage, boolean z, Integer num) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(iceUpdateMessage);
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.of(linkedList), Optional.empty(), Optional.empty(), Optional.empty(), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forHangup(HangupMessage hangupMessage, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(hangupMessage), Optional.empty(), Optional.empty(), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forBusy(BusyMessage busyMessage, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(busyMessage), Optional.empty(), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forOpaque(OpaqueMessage opaqueMessage, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(opaqueMessage), z, Optional.ofNullable(num));
    }

    public static SignalServiceCallMessage forOutgoingGroupOpaque(byte[] bArr, long j, OpaqueMessage opaqueMessage, boolean z, Integer num) {
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(opaqueMessage), z, Optional.ofNullable(num), Optional.of(bArr), Optional.of(Long.valueOf(j)));
    }

    public static SignalServiceCallMessage empty() {
        return new SignalServiceCallMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), false, Optional.empty());
    }

    public Optional<List<IceUpdateMessage>> getIceUpdateMessages() {
        return this.iceUpdateMessages;
    }

    public Optional<AnswerMessage> getAnswerMessage() {
        return this.answerMessage;
    }

    public Optional<OfferMessage> getOfferMessage() {
        return this.offerMessage;
    }

    public Optional<HangupMessage> getHangupMessage() {
        return this.hangupMessage;
    }

    public Optional<BusyMessage> getBusyMessage() {
        return this.busyMessage;
    }

    public Optional<OpaqueMessage> getOpaqueMessage() {
        return this.opaqueMessage;
    }

    public boolean isMultiRing() {
        return this.isMultiRing;
    }

    public Optional<Integer> getDestinationDeviceId() {
        return this.destinationDeviceId;
    }

    public Optional<byte[]> getGroupId() {
        return this.groupId;
    }

    public Optional<Long> getTimestamp() {
        return this.timestamp;
    }
}
