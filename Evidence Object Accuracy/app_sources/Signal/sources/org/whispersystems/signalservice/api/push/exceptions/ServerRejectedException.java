package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class ServerRejectedException extends NonSuccessfulResponseCodeException {
    public ServerRejectedException() {
        super(508);
    }
}
