package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* loaded from: classes5.dex */
public class SignalServiceStickerManifest {
    private final Optional<String> author;
    private final Optional<StickerInfo> cover;
    private final List<StickerInfo> stickers;
    private final Optional<String> title;

    public SignalServiceStickerManifest(String str, String str2, StickerInfo stickerInfo, List<StickerInfo> list) {
        this.title = Optional.ofNullable(str);
        this.author = Optional.ofNullable(str2);
        this.cover = Optional.ofNullable(stickerInfo);
        this.stickers = list == null ? Collections.emptyList() : new ArrayList<>(list);
    }

    public Optional<String> getTitle() {
        return this.title;
    }

    public Optional<String> getAuthor() {
        return this.author;
    }

    public Optional<StickerInfo> getCover() {
        return this.cover;
    }

    public List<StickerInfo> getStickers() {
        return this.stickers;
    }

    /* loaded from: classes5.dex */
    public static final class StickerInfo {
        private final String contentType;
        private final String emoji;
        private final int id;

        public StickerInfo(int i, String str, String str2) {
            this.id = i;
            this.emoji = str;
            this.contentType = str2;
        }

        public int getId() {
            return this.id;
        }

        public String getEmoji() {
            return this.emoji;
        }

        public String getContentType() {
            return this.contentType;
        }
    }
}
