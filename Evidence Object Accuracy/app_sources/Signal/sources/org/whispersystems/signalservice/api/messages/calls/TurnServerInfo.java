package org.whispersystems.signalservice.api.messages.calls;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public class TurnServerInfo {
    @JsonProperty
    private String password;
    @JsonProperty
    private List<String> urls;
    @JsonProperty
    private String username;

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public List<String> getUrls() {
        return this.urls;
    }
}
