package org.whispersystems.signalservice.api.profiles;

import j$.util.Optional;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;

/* loaded from: classes5.dex */
public final class ProfileAndCredential {
    private final Optional<ExpiringProfileKeyCredential> expiringProfileKeyCredential;
    private final SignalServiceProfile profile;
    private final SignalServiceProfile.RequestType requestType;

    public ProfileAndCredential(SignalServiceProfile signalServiceProfile, SignalServiceProfile.RequestType requestType, Optional<ExpiringProfileKeyCredential> optional) {
        this.profile = signalServiceProfile;
        this.requestType = requestType;
        this.expiringProfileKeyCredential = optional;
    }

    public SignalServiceProfile getProfile() {
        return this.profile;
    }

    public SignalServiceProfile.RequestType getRequestType() {
        return this.requestType;
    }

    public Optional<ExpiringProfileKeyCredential> getExpiringProfileKeyCredential() {
        return this.expiringProfileKeyCredential;
    }
}
