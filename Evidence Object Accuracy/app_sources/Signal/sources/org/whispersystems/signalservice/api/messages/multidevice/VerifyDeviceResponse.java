package org.whispersystems.signalservice.api.messages.multidevice;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;

/* loaded from: classes.dex */
public class VerifyDeviceResponse {
    @JsonProperty
    private int deviceId;
    @JsonProperty
    private UUID pni;
    @JsonProperty
    private UUID uuid;

    public VerifyDeviceResponse() {
    }

    public VerifyDeviceResponse(UUID uuid, UUID uuid2, int i) {
        this.uuid = uuid;
        this.pni = uuid2;
        this.deviceId = i;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public UUID getPni() {
        return this.pni;
    }

    public int getDeviceId() {
        return this.deviceId;
    }
}
