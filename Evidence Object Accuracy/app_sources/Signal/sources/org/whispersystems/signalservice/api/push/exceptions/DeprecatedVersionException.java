package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class DeprecatedVersionException extends NonSuccessfulResponseCodeException {
    public DeprecatedVersionException() {
        super(499);
    }
}
