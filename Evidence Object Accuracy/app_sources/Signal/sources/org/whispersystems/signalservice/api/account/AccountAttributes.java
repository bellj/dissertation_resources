package org.whispersystems.signalservice.api.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class AccountAttributes {
    @JsonProperty
    private Capabilities capabilities;
    @JsonProperty
    private boolean discoverableByPhoneNumber;
    @JsonProperty
    private boolean fetchesMessages;
    @JsonProperty
    private String name;
    @JsonProperty
    private String pin;
    @JsonProperty
    private int registrationId;
    @JsonProperty
    private String registrationLock;
    @JsonProperty
    private String signalingKey;
    @JsonProperty
    private byte[] unidentifiedAccessKey;
    @JsonProperty
    private boolean unrestrictedUnidentifiedAccess;
    @JsonProperty
    private boolean video;
    @JsonProperty
    private boolean voice;

    public AccountAttributes(String str, int i, boolean z, String str2, String str3, byte[] bArr, boolean z2, Capabilities capabilities, boolean z3, String str4) {
        this.signalingKey = str;
        this.registrationId = i;
        this.voice = true;
        this.video = true;
        this.fetchesMessages = z;
        this.pin = str2;
        this.registrationLock = str3;
        this.unidentifiedAccessKey = bArr;
        this.unrestrictedUnidentifiedAccess = z2;
        this.capabilities = capabilities;
        this.discoverableByPhoneNumber = z3;
        this.name = str4;
    }

    public AccountAttributes() {
    }

    public String getSignalingKey() {
        return this.signalingKey;
    }

    public int getRegistrationId() {
        return this.registrationId;
    }

    public boolean isVoice() {
        return this.voice;
    }

    public boolean isVideo() {
        return this.video;
    }

    public boolean isFetchesMessages() {
        return this.fetchesMessages;
    }

    public String getPin() {
        return this.pin;
    }

    public String getRegistrationLock() {
        return this.registrationLock;
    }

    public byte[] getUnidentifiedAccessKey() {
        return this.unidentifiedAccessKey;
    }

    public boolean isUnrestrictedUnidentifiedAccess() {
        return this.unrestrictedUnidentifiedAccess;
    }

    public boolean isDiscoverableByPhoneNumber() {
        return this.discoverableByPhoneNumber;
    }

    public Capabilities getCapabilities() {
        return this.capabilities;
    }

    public String getName() {
        return this.name;
    }

    /* loaded from: classes.dex */
    public static class Capabilities {
        @JsonProperty
        private boolean announcementGroup;
        @JsonProperty
        private boolean changeNumber;
        @JsonProperty
        private boolean giftBadges;
        @JsonProperty("gv1-migration")
        private boolean gv1Migration;
        @JsonProperty("gv2-3")
        private boolean gv2;
        @JsonProperty
        private boolean senderKey;
        @JsonProperty
        private boolean storage;
        @JsonProperty
        private boolean stories;
        @JsonProperty
        private boolean uuid;

        @JsonCreator
        public Capabilities() {
        }

        public Capabilities(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
            this.uuid = z;
            this.gv2 = z2;
            this.storage = z3;
            this.gv1Migration = z4;
            this.senderKey = z5;
            this.announcementGroup = z6;
            this.changeNumber = z7;
            this.stories = z8;
            this.giftBadges = z9;
        }

        public boolean isUuid() {
            return this.uuid;
        }

        public boolean isGv2() {
            return this.gv2;
        }

        public boolean isStorage() {
            return this.storage;
        }

        public boolean isGv1Migration() {
            return this.gv1Migration;
        }

        public boolean isSenderKey() {
            return this.senderKey;
        }

        public boolean isAnnouncementGroup() {
            return this.announcementGroup;
        }

        public boolean isChangeNumber() {
            return this.changeNumber;
        }

        public boolean isStories() {
            return this.stories;
        }

        public boolean isGiftBadges() {
            return this.giftBadges;
        }
    }
}
