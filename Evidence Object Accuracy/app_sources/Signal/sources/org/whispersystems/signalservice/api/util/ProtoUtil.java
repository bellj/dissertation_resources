package org.whispersystems.signalservice.api.util;

import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.UnknownFieldSetLite;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import org.signal.libsignal.protocol.logging.Log;

/* loaded from: classes5.dex */
public final class ProtoUtil {
    private static final String DEFAULT_INSTANCE;
    private static final String TAG;

    private ProtoUtil() {
    }

    public static boolean hasUnknownFields(GeneratedMessageLite generatedMessageLite) {
        try {
            List<GeneratedMessageLite> innerProtos = getInnerProtos(generatedMessageLite);
            innerProtos.add(generatedMessageLite);
            for (GeneratedMessageLite generatedMessageLite2 : innerProtos) {
                Field declaredField = GeneratedMessageLite.class.getDeclaredField("unknownFields");
                declaredField.setAccessible(true);
                UnknownFieldSetLite unknownFieldSetLite = (UnknownFieldSetLite) declaredField.get(generatedMessageLite2);
                if (unknownFieldSetLite != null && unknownFieldSetLite.getSerializedSize() > 0) {
                    return true;
                }
            }
            return false;
        } catch (IllegalAccessException | NoSuchFieldException unused) {
            Log.w(TAG, "Failed to read proto private fields! Assuming no unknown fields.");
            return false;
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [com.google.protobuf.AbstractMessageLite$Builder] */
    public static <Proto extends GeneratedMessageLite> Proto combineWithUnknownFields(Proto proto, byte[] bArr) throws InvalidProtocolBufferException {
        return (Proto) ((GeneratedMessageLite) proto.newBuilderForType().mergeFrom(bArr).mergeFrom(proto).build());
    }

    private static List<GeneratedMessageLite> getInnerProtos(GeneratedMessageLite generatedMessageLite) {
        LinkedList linkedList = new LinkedList();
        try {
            Field[] declaredFields = generatedMessageLite.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                if (!field.getName().equals(DEFAULT_INSTANCE) && GeneratedMessageLite.class.isAssignableFrom(field.getType())) {
                    field.setAccessible(true);
                    GeneratedMessageLite generatedMessageLite2 = (GeneratedMessageLite) field.get(generatedMessageLite);
                    if (generatedMessageLite2 != null) {
                        linkedList.add(generatedMessageLite2);
                        linkedList.addAll(getInnerProtos(generatedMessageLite2));
                    }
                }
            }
        } catch (IllegalAccessException e) {
            Log.w(TAG, "Failed to get inner protos!", e);
        }
        return linkedList;
    }
}
