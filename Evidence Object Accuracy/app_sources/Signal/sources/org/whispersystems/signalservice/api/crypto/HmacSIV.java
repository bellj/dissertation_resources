package org.whispersystems.signalservice.api.crypto;

import java.util.Arrays;
import org.whispersystems.util.ByteArrayUtil;
import org.whispersystems.util.StringUtil;

/* loaded from: classes5.dex */
public final class HmacSIV {
    private static final byte[] AUTH_BYTES = StringUtil.utf8("auth");
    private static final byte[] ENC_BYTES = StringUtil.utf8("enc");

    public static byte[] encrypt(byte[] bArr, byte[] bArr2) {
        if (bArr.length != 32) {
            throw new AssertionError("K was wrong length");
        } else if (bArr2.length == 32) {
            byte[] hmacSha256 = CryptoUtil.hmacSha256(bArr, AUTH_BYTES);
            byte[] hmacSha2562 = CryptoUtil.hmacSha256(bArr, ENC_BYTES);
            byte[] copyOfRange = Arrays.copyOfRange(CryptoUtil.hmacSha256(hmacSha256, bArr2), 0, 16);
            return ByteArrayUtil.concat(copyOfRange, ByteArrayUtil.xor(CryptoUtil.hmacSha256(hmacSha2562, copyOfRange), bArr2));
        } else {
            throw new AssertionError("M was wrong length");
        }
    }

    public static byte[] decrypt(byte[] bArr, byte[] bArr2) throws InvalidCiphertextException {
        if (bArr.length != 32) {
            throw new AssertionError("K was wrong length");
        } else if (bArr2.length == 48) {
            byte[] copyOfRange = Arrays.copyOfRange(bArr2, 0, 16);
            byte[] copyOfRange2 = Arrays.copyOfRange(bArr2, 16, 48);
            byte[] hmacSha256 = CryptoUtil.hmacSha256(bArr, AUTH_BYTES);
            byte[] xor = ByteArrayUtil.xor(CryptoUtil.hmacSha256(CryptoUtil.hmacSha256(bArr, ENC_BYTES), copyOfRange), copyOfRange2);
            if (Arrays.equals(copyOfRange, Arrays.copyOfRange(CryptoUtil.hmacSha256(hmacSha256, xor), 0, 16))) {
                return xor;
            }
            throw new InvalidCiphertextException("IV was incorrect");
        } else {
            throw new InvalidCiphertextException("IVC was wrong length");
        }
    }
}
