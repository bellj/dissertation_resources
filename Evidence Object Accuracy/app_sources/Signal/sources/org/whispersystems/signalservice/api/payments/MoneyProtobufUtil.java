package org.whispersystems.signalservice.api.payments;

import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.util.Uint64RangeException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public final class MoneyProtobufUtil {
    public static SignalServiceProtos.DataMessage.Payment.Amount moneyToPaymentAmount(Money money) {
        SignalServiceProtos.DataMessage.Payment.Amount.Builder newBuilder = SignalServiceProtos.DataMessage.Payment.Amount.newBuilder();
        if (money instanceof Money.MobileCoin) {
            try {
                newBuilder.setMobileCoin(SignalServiceProtos.DataMessage.Payment.Amount.MobileCoin.newBuilder().setPicoMob(money.requireMobileCoin().toPicoMobUint64()));
                return newBuilder.build();
            } catch (Uint64RangeException e) {
                throw new AssertionError(e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: org.whispersystems.signalservice.api.payments.MoneyProtobufUtil$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$Amount$AmountCase;

        static {
            int[] iArr = new int[SignalServiceProtos.DataMessage.Payment.Amount.AmountCase.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$Amount$AmountCase = iArr;
            try {
                iArr[SignalServiceProtos.DataMessage.Payment.Amount.AmountCase.MOBILECOIN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$Amount$AmountCase[SignalServiceProtos.DataMessage.Payment.Amount.AmountCase.AMOUNT_NOT_SET.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public static Money paymentAmountToMoney(SignalServiceProtos.DataMessage.Payment.Amount amount) throws UnsupportedCurrencyException {
        if (AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$DataMessage$Payment$Amount$AmountCase[amount.getAmountCase().ordinal()] == 1) {
            return Money.picoMobileCoin(amount.getMobileCoin().getPicoMob());
        }
        throw new UnsupportedCurrencyException();
    }
}
