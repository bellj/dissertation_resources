package org.whispersystems.signalservice.api.groupsv2;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.local.DecryptedApproveMember;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.signal.storageservice.protos.groups.local.EnabledState;
import org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda2;
import org.whispersystems.signalservice.api.push.ServiceIds;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public final class DecryptedGroupUtil {
    private static final String TAG;

    public static ArrayList<UUID> toUuidList(Collection<DecryptedMember> collection) {
        ArrayList<UUID> arrayList = new ArrayList<>(collection.size());
        for (DecryptedMember decryptedMember : collection) {
            arrayList.add(toUuid(decryptedMember));
        }
        return arrayList;
    }

    public static ArrayList<UUID> membersToUuidList(Collection<DecryptedMember> collection) {
        ArrayList<UUID> arrayList = new ArrayList<>(collection.size());
        for (DecryptedMember decryptedMember : collection) {
            UUID uuid = toUuid(decryptedMember);
            if (!UuidUtil.UNKNOWN_UUID.equals(uuid)) {
                arrayList.add(uuid);
            }
        }
        return arrayList;
    }

    public static Set<ByteString> membersToUuidByteStringSet(Collection<DecryptedMember> collection) {
        HashSet hashSet = new HashSet(collection.size());
        for (DecryptedMember decryptedMember : collection) {
            hashSet.add(decryptedMember.getUuid());
        }
        return hashSet;
    }

    public static ArrayList<UUID> pendingToUuidList(Collection<DecryptedPendingMember> collection) {
        ArrayList<UUID> arrayList = new ArrayList<>(collection.size());
        for (DecryptedPendingMember decryptedPendingMember : collection) {
            arrayList.add(toUuid(decryptedPendingMember));
        }
        return arrayList;
    }

    public static ArrayList<UUID> removedMembersUuidList(DecryptedGroupChange decryptedGroupChange) {
        List<ByteString> deleteMembersList = decryptedGroupChange.getDeleteMembersList();
        ArrayList<UUID> arrayList = new ArrayList<>(deleteMembersList.size());
        for (ByteString byteString : deleteMembersList) {
            UUID uuid = toUuid(byteString);
            if (!UuidUtil.UNKNOWN_UUID.equals(uuid)) {
                arrayList.add(uuid);
            }
        }
        return arrayList;
    }

    public static ArrayList<UUID> removedPendingMembersUuidList(DecryptedGroupChange decryptedGroupChange) {
        List<DecryptedPendingMemberRemoval> deletePendingMembersList = decryptedGroupChange.getDeletePendingMembersList();
        ArrayList<UUID> arrayList = new ArrayList<>(deletePendingMembersList.size());
        for (DecryptedPendingMemberRemoval decryptedPendingMemberRemoval : deletePendingMembersList) {
            UUID uuid = toUuid(decryptedPendingMemberRemoval.getUuid());
            if (!UuidUtil.UNKNOWN_UUID.equals(uuid)) {
                arrayList.add(uuid);
            }
        }
        return arrayList;
    }

    public static ArrayList<UUID> removedRequestingMembersUuidList(DecryptedGroupChange decryptedGroupChange) {
        List<ByteString> deleteRequestingMembersList = decryptedGroupChange.getDeleteRequestingMembersList();
        ArrayList<UUID> arrayList = new ArrayList<>(deleteRequestingMembersList.size());
        for (ByteString byteString : deleteRequestingMembersList) {
            UUID uuid = toUuid(byteString);
            if (!UuidUtil.UNKNOWN_UUID.equals(uuid)) {
                arrayList.add(uuid);
            }
        }
        return arrayList;
    }

    public static Set<UUID> bannedMembersToUuidSet(Collection<DecryptedBannedMember> collection) {
        HashSet hashSet = new HashSet(collection.size());
        for (DecryptedBannedMember decryptedBannedMember : collection) {
            UUID uuid = toUuid(decryptedBannedMember);
            if (!UuidUtil.UNKNOWN_UUID.equals(uuid)) {
                hashSet.add(uuid);
            }
        }
        return hashSet;
    }

    public static UUID toUuid(DecryptedMember decryptedMember) {
        return toUuid(decryptedMember.getUuid());
    }

    public static UUID toUuid(DecryptedPendingMember decryptedPendingMember) {
        return toUuid(decryptedPendingMember.getUuid());
    }

    public static UUID toUuid(DecryptedBannedMember decryptedBannedMember) {
        return toUuid(decryptedBannedMember.getUuid());
    }

    private static UUID toUuid(ByteString byteString) {
        return UuidUtil.fromByteStringOrUnknown(byteString);
    }

    public static Optional<UUID> editorUuid(DecryptedGroupChange decryptedGroupChange) {
        return Optional.ofNullable(decryptedGroupChange != null ? UuidUtil.fromByteStringOrNull(decryptedGroupChange.getEditor()) : null);
    }

    public static Optional<DecryptedMember> findMemberByUuid(Collection<DecryptedMember> collection, UUID uuid) {
        ByteString byteString = UuidUtil.toByteString(uuid);
        for (DecryptedMember decryptedMember : collection) {
            if (byteString.equals(decryptedMember.getUuid())) {
                return Optional.of(decryptedMember);
            }
        }
        return Optional.empty();
    }

    public static Optional<DecryptedPendingMember> findPendingByUuid(Collection<DecryptedPendingMember> collection, UUID uuid) {
        ByteString byteString = UuidUtil.toByteString(uuid);
        for (DecryptedPendingMember decryptedPendingMember : collection) {
            if (byteString.equals(decryptedPendingMember.getUuid())) {
                return Optional.of(decryptedPendingMember);
            }
        }
        return Optional.empty();
    }

    public static Optional<DecryptedPendingMember> findPendingByServiceIds(Collection<DecryptedPendingMember> collection, ServiceIds serviceIds) {
        for (DecryptedPendingMember decryptedPendingMember : collection) {
            if (serviceIds.matches(decryptedPendingMember.getUuid())) {
                return Optional.of(decryptedPendingMember);
            }
        }
        return Optional.empty();
    }

    private static int findPendingIndexByUuidCipherText(List<DecryptedPendingMember> list, ByteString byteString) {
        for (int i = 0; i < list.size(); i++) {
            if (byteString.equals(list.get(i).getUuidCipherText())) {
                return i;
            }
        }
        return -1;
    }

    private static int findPendingIndexByUuid(List<DecryptedPendingMember> list, ByteString byteString) {
        for (int i = 0; i < list.size(); i++) {
            if (byteString.equals(list.get(i).getUuid())) {
                return i;
            }
        }
        return -1;
    }

    public static Optional<DecryptedRequestingMember> findRequestingByUuid(Collection<DecryptedRequestingMember> collection, UUID uuid) {
        ByteString byteString = UuidUtil.toByteString(uuid);
        for (DecryptedRequestingMember decryptedRequestingMember : collection) {
            if (byteString.equals(decryptedRequestingMember.getUuid())) {
                return Optional.of(decryptedRequestingMember);
            }
        }
        return Optional.empty();
    }

    public static Optional<DecryptedRequestingMember> findRequestingByServiceIds(Collection<DecryptedRequestingMember> collection, ServiceIds serviceIds) {
        for (DecryptedRequestingMember decryptedRequestingMember : collection) {
            if (serviceIds.matches(decryptedRequestingMember.getUuid())) {
                return Optional.of(decryptedRequestingMember);
            }
        }
        return Optional.empty();
    }

    public static boolean isPendingOrRequesting(DecryptedGroup decryptedGroup, ServiceIds serviceIds) {
        return findPendingByServiceIds(decryptedGroup.getPendingMembersList(), serviceIds).isPresent() || findRequestingByServiceIds(decryptedGroup.getRequestingMembersList(), serviceIds).isPresent();
    }

    public static boolean isRequesting(DecryptedGroup decryptedGroup, UUID uuid) {
        return findRequestingByUuid(decryptedGroup.getRequestingMembersList(), uuid).isPresent();
    }

    public static DecryptedGroup removeMember(DecryptedGroup decryptedGroup, UUID uuid, int i) {
        DecryptedGroup.Builder newBuilder = DecryptedGroup.newBuilder(decryptedGroup);
        ByteString byteString = UuidUtil.toByteString(uuid);
        ArrayList arrayList = new ArrayList(newBuilder.getMembersList());
        Iterator it = arrayList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            if (byteString.equals(((DecryptedMember) it.next()).getUuid())) {
                it.remove();
                z = true;
            }
        }
        return z ? newBuilder.clearMembers().addAllMembers(arrayList).setRevision(i).build() : decryptedGroup;
    }

    public static DecryptedGroup apply(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) throws NotAbleToApplyGroupV2ChangeException {
        if (decryptedGroupChange.getRevision() == decryptedGroup.getRevision() + 1) {
            return applyWithoutRevisionCheck(decryptedGroup, decryptedGroupChange);
        }
        throw new NotAbleToApplyGroupV2ChangeException();
    }

    public static DecryptedGroup applyWithoutRevisionCheck(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) throws NotAbleToApplyGroupV2ChangeException {
        DecryptedGroup.Builder revision = DecryptedGroup.newBuilder(decryptedGroup).setRevision(decryptedGroupChange.getRevision());
        applyAddMemberAction(revision, decryptedGroupChange.getNewMembersList());
        applyDeleteMemberActions(revision, decryptedGroupChange.getDeleteMembersList());
        applyModifyMemberRoleActions(revision, decryptedGroupChange.getModifyMemberRolesList());
        applyModifyMemberProfileKeyActions(revision, decryptedGroupChange.getModifiedProfileKeysList());
        applyAddPendingMemberActions(revision, decryptedGroupChange.getNewPendingMembersList());
        applyDeletePendingMemberActions(revision, decryptedGroupChange.getDeletePendingMembersList());
        applyPromotePendingMemberActions(revision, decryptedGroupChange.getPromotePendingMembersList());
        applyModifyTitleAction(revision, decryptedGroupChange);
        applyModifyDescriptionAction(revision, decryptedGroupChange);
        applyModifyIsAnnouncementGroupAction(revision, decryptedGroupChange);
        applyModifyAvatarAction(revision, decryptedGroupChange);
        applyModifyDisappearingMessagesTimerAction(revision, decryptedGroupChange);
        applyModifyAttributesAccessControlAction(revision, decryptedGroupChange);
        applyModifyMembersAccessControlAction(revision, decryptedGroupChange);
        applyModifyAddFromInviteLinkAccessControlAction(revision, decryptedGroupChange);
        applyAddRequestingMembers(revision, decryptedGroupChange.getNewRequestingMembersList());
        applyDeleteRequestingMembers(revision, decryptedGroupChange.getDeleteRequestingMembersList());
        applyPromoteRequestingMemberActions(revision, decryptedGroupChange.getPromoteRequestingMembersList());
        applyInviteLinkPassword(revision, decryptedGroupChange);
        applyAddBannedMembersActions(revision, decryptedGroupChange.getNewBannedMembersList());
        applyDeleteBannedMembersActions(revision, decryptedGroupChange.getDeleteBannedMembersList());
        applyPromotePendingPniAciMemberActions(revision, decryptedGroupChange.getPromotePendingPniAciMembersList());
        return revision.build();
    }

    private static void applyAddMemberAction(DecryptedGroup.Builder builder, List<DecryptedMember> list) {
        if (!list.isEmpty()) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (DecryptedMember decryptedMember : builder.getMembersList()) {
                linkedHashMap.put(decryptedMember.getUuid(), decryptedMember);
            }
            for (DecryptedMember decryptedMember2 : list) {
                linkedHashMap.put(decryptedMember2.getUuid(), decryptedMember2);
            }
            builder.clearMembers();
            builder.addAllMembers(linkedHashMap.values());
            removePendingAndRequestingMembersNowInGroup(builder);
        }
    }

    protected static void applyDeleteMemberActions(DecryptedGroup.Builder builder, List<ByteString> list) {
        for (ByteString byteString : list) {
            int indexOfUuid = indexOfUuid(builder.getMembersList(), byteString);
            if (indexOfUuid == -1) {
                Log.w(TAG, "Deleted member on change not found in group");
            } else {
                builder.removeMembers(indexOfUuid);
            }
        }
    }

    private static void applyModifyMemberRoleActions(DecryptedGroup.Builder builder, List<DecryptedModifyMemberRole> list) throws NotAbleToApplyGroupV2ChangeException {
        for (DecryptedModifyMemberRole decryptedModifyMemberRole : list) {
            int indexOfUuid = indexOfUuid(builder.getMembersList(), decryptedModifyMemberRole.getUuid());
            if (indexOfUuid != -1) {
                Member.Role role = decryptedModifyMemberRole.getRole();
                ensureKnownRole(role);
                builder.setMembers(indexOfUuid, DecryptedMember.newBuilder(builder.getMembers(indexOfUuid)).setRole(role));
            } else {
                throw new NotAbleToApplyGroupV2ChangeException();
            }
        }
    }

    private static void applyModifyMemberProfileKeyActions(DecryptedGroup.Builder builder, List<DecryptedMember> list) throws NotAbleToApplyGroupV2ChangeException {
        for (DecryptedMember decryptedMember : list) {
            int indexOfUuid = indexOfUuid(builder.getMembersList(), decryptedMember.getUuid());
            if (indexOfUuid != -1) {
                builder.setMembers(indexOfUuid, withNewProfileKey(builder.getMembers(indexOfUuid), decryptedMember.getProfileKey()));
            } else {
                throw new NotAbleToApplyGroupV2ChangeException();
            }
        }
    }

    private static void applyAddPendingMemberActions(DecryptedGroup.Builder builder, List<DecryptedPendingMember> list) throws NotAbleToApplyGroupV2ChangeException {
        Set<ByteString> memberUuidSet = getMemberUuidSet(builder.getMembersList());
        Set<ByteString> pendingMemberCipherTextSet = getPendingMemberCipherTextSet(builder.getPendingMembersList());
        for (DecryptedPendingMember decryptedPendingMember : list) {
            if (memberUuidSet.contains(decryptedPendingMember.getUuid())) {
                throw new NotAbleToApplyGroupV2ChangeException();
            } else if (!pendingMemberCipherTextSet.contains(decryptedPendingMember.getUuidCipherText())) {
                builder.addPendingMembers(decryptedPendingMember);
            }
        }
    }

    protected static void applyDeletePendingMemberActions(DecryptedGroup.Builder builder, List<DecryptedPendingMemberRemoval> list) {
        for (DecryptedPendingMemberRemoval decryptedPendingMemberRemoval : list) {
            int findPendingIndexByUuidCipherText = findPendingIndexByUuidCipherText(builder.getPendingMembersList(), decryptedPendingMemberRemoval.getUuidCipherText());
            if (findPendingIndexByUuidCipherText == -1) {
                Log.w(TAG, "Deleted pending member on change not found in group");
            } else {
                builder.removePendingMembers(findPendingIndexByUuidCipherText);
            }
        }
    }

    protected static void applyPromotePendingMemberActions(DecryptedGroup.Builder builder, List<DecryptedMember> list) throws NotAbleToApplyGroupV2ChangeException {
        for (DecryptedMember decryptedMember : list) {
            int findPendingIndexByUuid = findPendingIndexByUuid(builder.getPendingMembersList(), decryptedMember.getUuid());
            if (findPendingIndexByUuid != -1) {
                builder.removePendingMembers(findPendingIndexByUuid);
                builder.addMembers(decryptedMember);
            } else {
                throw new NotAbleToApplyGroupV2ChangeException();
            }
        }
    }

    protected static void applyModifyTitleAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.hasNewTitle()) {
            builder.setTitle(decryptedGroupChange.getNewTitle().getValue());
        }
    }

    protected static void applyModifyDescriptionAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.hasNewDescription()) {
            builder.setDescription(decryptedGroupChange.getNewDescription().getValue());
        }
    }

    protected static void applyModifyIsAnnouncementGroupAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.getNewIsAnnouncementGroup() != EnabledState.UNKNOWN) {
            builder.setIsAnnouncementGroup(decryptedGroupChange.getNewIsAnnouncementGroup());
        }
    }

    protected static void applyModifyAvatarAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.hasNewAvatar()) {
            builder.setAvatar(decryptedGroupChange.getNewAvatar().getValue());
        }
    }

    protected static void applyModifyDisappearingMessagesTimerAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.hasNewTimer()) {
            builder.setDisappearingMessagesTimer(decryptedGroupChange.getNewTimer());
        }
    }

    protected static void applyModifyAttributesAccessControlAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.getNewAttributeAccess() != AccessControl.AccessRequired.UNKNOWN) {
            builder.setAccessControl(AccessControl.newBuilder(builder.getAccessControl()).setAttributesValue(decryptedGroupChange.getNewAttributeAccessValue()));
        }
    }

    protected static void applyModifyMembersAccessControlAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.getNewMemberAccess() != AccessControl.AccessRequired.UNKNOWN) {
            builder.setAccessControl(AccessControl.newBuilder(builder.getAccessControl()).setMembersValue(decryptedGroupChange.getNewMemberAccessValue()));
        }
    }

    protected static void applyModifyAddFromInviteLinkAccessControlAction(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        AccessControl.AccessRequired newInviteLinkAccess = decryptedGroupChange.getNewInviteLinkAccess();
        if (newInviteLinkAccess != AccessControl.AccessRequired.UNKNOWN) {
            builder.setAccessControl(AccessControl.newBuilder(builder.getAccessControl()).setAddFromInviteLink(newInviteLinkAccess));
        }
    }

    private static void applyAddRequestingMembers(DecryptedGroup.Builder builder, List<DecryptedRequestingMember> list) {
        builder.addAllRequestingMembers(list);
    }

    private static void applyDeleteRequestingMembers(DecryptedGroup.Builder builder, List<ByteString> list) {
        for (ByteString byteString : list) {
            int indexOfUuidInRequestingList = indexOfUuidInRequestingList(builder.getRequestingMembersList(), byteString);
            if (indexOfUuidInRequestingList == -1) {
                Log.w(TAG, "Deleted member on change not found in group");
            } else {
                builder.removeRequestingMembers(indexOfUuidInRequestingList);
            }
        }
    }

    private static void applyPromoteRequestingMemberActions(DecryptedGroup.Builder builder, List<DecryptedApproveMember> list) throws NotAbleToApplyGroupV2ChangeException {
        for (DecryptedApproveMember decryptedApproveMember : list) {
            int indexOfUuidInRequestingList = indexOfUuidInRequestingList(builder.getRequestingMembersList(), decryptedApproveMember.getUuid());
            if (indexOfUuidInRequestingList == -1) {
                Log.w(TAG, "Deleted member on change not found in group");
            } else {
                DecryptedRequestingMember requestingMembers = builder.getRequestingMembers(indexOfUuidInRequestingList);
                Member.Role role = decryptedApproveMember.getRole();
                ensureKnownRole(role);
                builder.removeRequestingMembers(indexOfUuidInRequestingList).addMembers(DecryptedMember.newBuilder().setUuid(decryptedApproveMember.getUuid()).setProfileKey(requestingMembers.getProfileKey()).setRole(role));
            }
        }
    }

    private static void applyInviteLinkPassword(DecryptedGroup.Builder builder, DecryptedGroupChange decryptedGroupChange) {
        if (!decryptedGroupChange.getNewInviteLinkPassword().isEmpty()) {
            builder.setInviteLinkPassword(decryptedGroupChange.getNewInviteLinkPassword());
        }
    }

    private static void applyAddBannedMembersActions(DecryptedGroup.Builder builder, List<DecryptedBannedMember> list) {
        Set<ByteString> bannedMemberUuidSet = getBannedMemberUuidSet(builder.getBannedMembersList());
        for (DecryptedBannedMember decryptedBannedMember : list) {
            if (bannedMemberUuidSet.contains(decryptedBannedMember.getUuid())) {
                Log.w(TAG, "Banned member already in banned list");
            } else {
                builder.addBannedMembers(decryptedBannedMember);
            }
        }
    }

    private static void applyDeleteBannedMembersActions(DecryptedGroup.Builder builder, List<DecryptedBannedMember> list) {
        for (DecryptedBannedMember decryptedBannedMember : list) {
            int indexOfUuidInBannedMemberList = indexOfUuidInBannedMemberList(builder.getBannedMembersList(), decryptedBannedMember.getUuid());
            if (indexOfUuidInBannedMemberList == -1) {
                Log.w(TAG, "Deleted banned member on change not found in banned list");
            } else {
                builder.removeBannedMembers(indexOfUuidInBannedMemberList);
            }
        }
    }

    protected static void applyPromotePendingPniAciMemberActions(DecryptedGroup.Builder builder, List<DecryptedMember> list) throws NotAbleToApplyGroupV2ChangeException {
        for (DecryptedMember decryptedMember : list) {
            int findPendingIndexByUuid = findPendingIndexByUuid(builder.getPendingMembersList(), decryptedMember.getPni());
            if (findPendingIndexByUuid != -1) {
                builder.removePendingMembers(findPendingIndexByUuid);
                builder.addMembers(decryptedMember);
            } else {
                throw new NotAbleToApplyGroupV2ChangeException();
            }
        }
    }

    private static DecryptedMember withNewProfileKey(DecryptedMember decryptedMember, ByteString byteString) {
        return DecryptedMember.newBuilder(decryptedMember).setProfileKey(byteString).build();
    }

    private static Set<ByteString> getMemberUuidSet(List<DecryptedMember> list) {
        HashSet hashSet = new HashSet(list.size());
        for (DecryptedMember decryptedMember : list) {
            hashSet.add(decryptedMember.getUuid());
        }
        return hashSet;
    }

    private static Set<ByteString> getPendingMemberCipherTextSet(List<DecryptedPendingMember> list) {
        HashSet hashSet = new HashSet(list.size());
        for (DecryptedPendingMember decryptedPendingMember : list) {
            hashSet.add(decryptedPendingMember.getUuidCipherText());
        }
        return hashSet;
    }

    private static Set<ByteString> getBannedMemberUuidSet(List<DecryptedBannedMember> list) {
        HashSet hashSet = new HashSet(list.size());
        for (DecryptedBannedMember decryptedBannedMember : list) {
            hashSet.add(decryptedBannedMember.getUuid());
        }
        return hashSet;
    }

    private static void removePendingAndRequestingMembersNowInGroup(DecryptedGroup.Builder builder) {
        Set<ByteString> membersToUuidByteStringSet = membersToUuidByteStringSet(builder.getMembersList());
        for (int pendingMembersCount = builder.getPendingMembersCount() - 1; pendingMembersCount >= 0; pendingMembersCount--) {
            if (membersToUuidByteStringSet.contains(builder.getPendingMembers(pendingMembersCount).getUuid())) {
                builder.removePendingMembers(pendingMembersCount);
            }
        }
        for (int requestingMembersCount = builder.getRequestingMembersCount() - 1; requestingMembersCount >= 0; requestingMembersCount--) {
            if (membersToUuidByteStringSet.contains(builder.getRequestingMembers(requestingMembersCount).getUuid())) {
                builder.removeRequestingMembers(requestingMembersCount);
            }
        }
    }

    private static void ensureKnownRole(Member.Role role) throws NotAbleToApplyGroupV2ChangeException {
        if (role != Member.Role.ADMINISTRATOR && role != Member.Role.DEFAULT) {
            throw new NotAbleToApplyGroupV2ChangeException();
        }
    }

    private static int indexOfUuid(List<DecryptedMember> list, ByteString byteString) {
        for (int i = 0; i < list.size(); i++) {
            if (byteString.equals(list.get(i).getUuid())) {
                return i;
            }
        }
        return -1;
    }

    private static int indexOfUuidInRequestingList(List<DecryptedRequestingMember> list, ByteString byteString) {
        for (int i = 0; i < list.size(); i++) {
            if (byteString.equals(list.get(i).getUuid())) {
                return i;
            }
        }
        return -1;
    }

    private static int indexOfUuidInBannedMemberList(List<DecryptedBannedMember> list, ByteString byteString) {
        for (int i = 0; i < list.size(); i++) {
            if (byteString.equals(list.get(i).getUuid())) {
                return i;
            }
        }
        return -1;
    }

    public static Optional<UUID> findInviter(List<DecryptedPendingMember> list, UUID uuid) {
        return Optional.ofNullable((UUID) findPendingByUuid(list, uuid).map(new Function() { // from class: org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((DecryptedPendingMember) obj).getAddedByUuid();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda2()).orElse(null));
    }

    public static boolean changeIsEmpty(DecryptedGroupChange decryptedGroupChange) {
        return decryptedGroupChange.getModifiedProfileKeysCount() == 0 && changeIsEmptyExceptForProfileKeyChanges(decryptedGroupChange);
    }

    public static boolean changeIsEmptyExceptForProfileKeyChanges(DecryptedGroupChange decryptedGroupChange) {
        return decryptedGroupChange.getNewMembersCount() == 0 && decryptedGroupChange.getDeleteMembersCount() == 0 && decryptedGroupChange.getModifyMemberRolesCount() == 0 && decryptedGroupChange.getNewPendingMembersCount() == 0 && decryptedGroupChange.getDeletePendingMembersCount() == 0 && decryptedGroupChange.getPromotePendingMembersCount() == 0 && !decryptedGroupChange.hasNewTitle() && !decryptedGroupChange.hasNewAvatar() && !decryptedGroupChange.hasNewTimer() && isEmpty(decryptedGroupChange.getNewAttributeAccess()) && isEmpty(decryptedGroupChange.getNewMemberAccess()) && isEmpty(decryptedGroupChange.getNewInviteLinkAccess()) && decryptedGroupChange.getNewRequestingMembersCount() == 0 && decryptedGroupChange.getDeleteRequestingMembersCount() == 0 && decryptedGroupChange.getPromoteRequestingMembersCount() == 0 && decryptedGroupChange.getNewInviteLinkPassword().size() == 0 && !decryptedGroupChange.hasNewDescription() && isEmpty(decryptedGroupChange.getNewIsAnnouncementGroup()) && decryptedGroupChange.getNewBannedMembersCount() == 0 && decryptedGroupChange.getDeleteBannedMembersCount() == 0 && decryptedGroupChange.getPromotePendingPniAciMembersCount() == 0;
    }

    public static boolean changeIsEmptyExceptForBanChangesAndOptionalProfileKeyChanges(DecryptedGroupChange decryptedGroupChange) {
        return !(decryptedGroupChange.getNewBannedMembersCount() == 0 && decryptedGroupChange.getDeleteBannedMembersCount() == 0) && decryptedGroupChange.getNewMembersCount() == 0 && decryptedGroupChange.getDeleteMembersCount() == 0 && decryptedGroupChange.getModifyMemberRolesCount() == 0 && decryptedGroupChange.getNewPendingMembersCount() == 0 && decryptedGroupChange.getDeletePendingMembersCount() == 0 && decryptedGroupChange.getPromotePendingMembersCount() == 0 && !decryptedGroupChange.hasNewTitle() && !decryptedGroupChange.hasNewAvatar() && !decryptedGroupChange.hasNewTimer() && isEmpty(decryptedGroupChange.getNewAttributeAccess()) && isEmpty(decryptedGroupChange.getNewMemberAccess()) && isEmpty(decryptedGroupChange.getNewInviteLinkAccess()) && decryptedGroupChange.getNewRequestingMembersCount() == 0 && decryptedGroupChange.getDeleteRequestingMembersCount() == 0 && decryptedGroupChange.getPromoteRequestingMembersCount() == 0 && decryptedGroupChange.getNewInviteLinkPassword().size() == 0 && !decryptedGroupChange.hasNewDescription() && isEmpty(decryptedGroupChange.getNewIsAnnouncementGroup()) && decryptedGroupChange.getPromotePendingPniAciMembersCount() == 0;
    }

    static boolean isEmpty(AccessControl.AccessRequired accessRequired) {
        return accessRequired == AccessControl.AccessRequired.UNKNOWN;
    }

    static boolean isEmpty(EnabledState enabledState) {
        return enabledState == EnabledState.UNKNOWN;
    }

    public static boolean changeIsSilent(DecryptedGroupChange decryptedGroupChange) {
        return changeIsEmptyExceptForProfileKeyChanges(decryptedGroupChange) || changeIsEmptyExceptForBanChangesAndOptionalProfileKeyChanges(decryptedGroupChange);
    }
}
