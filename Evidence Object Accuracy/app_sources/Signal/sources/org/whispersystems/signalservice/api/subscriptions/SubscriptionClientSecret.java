package org.whispersystems.signalservice.api.subscriptions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public final class SubscriptionClientSecret {
    private final String clientSecret;
    private final String id;

    @JsonCreator
    public SubscriptionClientSecret(@JsonProperty("clientSecret") String str) {
        this.id = str.replaceFirst("_secret.*", "");
        this.clientSecret = str;
    }

    public String getId() {
        return this.id;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }
}
