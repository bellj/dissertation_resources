package org.whispersystems.signalservice.api.push;

import java.util.Objects;
import java.util.UUID;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public final class DistributionId {
    public static final DistributionId MY_STORY = from("00000000-0000-0000-0000-000000000000");
    private final UUID uuid;

    public static DistributionId from(String str) {
        return new DistributionId(UuidUtil.parseOrThrow(str));
    }

    public static DistributionId from(UUID uuid) {
        return new DistributionId(uuid);
    }

    public static DistributionId create() {
        return new DistributionId(UUID.randomUUID());
    }

    private DistributionId(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID asUuid() {
        return this.uuid;
    }

    public String toString() {
        return this.uuid.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DistributionId.class != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.uuid, ((DistributionId) obj).uuid);
    }

    public int hashCode() {
        return Objects.hash(this.uuid);
    }
}
