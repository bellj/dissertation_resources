package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;
import java.util.LinkedList;
import java.util.List;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class SignalServiceSyncMessage {
    private final Optional<BlockedListMessage> blockedList;
    private final Optional<ConfigurationMessage> configuration;
    private final Optional<ContactsMessage> contacts;
    private final Optional<FetchType> fetchType;
    private final Optional<SignalServiceAttachment> groups;
    private final Optional<KeysMessage> keys;
    private final Optional<MessageRequestResponseMessage> messageRequestResponse;
    private final Optional<OutgoingPaymentMessage> outgoingPaymentMessage;
    private final Optional<SignalServiceProtos.SyncMessage.PniIdentity> pniIdentity;
    private final Optional<List<ReadMessage>> reads;
    private final Optional<RequestMessage> request;
    private final Optional<SentTranscriptMessage> sent;
    private final Optional<List<StickerPackOperationMessage>> stickerPackOperations;
    private final Optional<VerifiedMessage> verified;
    private final Optional<ViewOnceOpenMessage> viewOnceOpen;
    private final Optional<List<ViewedMessage>> views;

    /* loaded from: classes5.dex */
    public enum FetchType {
        LOCAL_PROFILE,
        STORAGE_MANIFEST,
        SUBSCRIPTION_STATUS
    }

    private SignalServiceSyncMessage(Optional<SentTranscriptMessage> optional, Optional<ContactsMessage> optional2, Optional<SignalServiceAttachment> optional3, Optional<BlockedListMessage> optional4, Optional<RequestMessage> optional5, Optional<List<ReadMessage>> optional6, Optional<ViewOnceOpenMessage> optional7, Optional<VerifiedMessage> optional8, Optional<ConfigurationMessage> optional9, Optional<List<StickerPackOperationMessage>> optional10, Optional<FetchType> optional11, Optional<KeysMessage> optional12, Optional<MessageRequestResponseMessage> optional13, Optional<OutgoingPaymentMessage> optional14, Optional<List<ViewedMessage>> optional15, Optional<SignalServiceProtos.SyncMessage.PniIdentity> optional16) {
        this.sent = optional;
        this.contacts = optional2;
        this.groups = optional3;
        this.blockedList = optional4;
        this.request = optional5;
        this.reads = optional6;
        this.viewOnceOpen = optional7;
        this.verified = optional8;
        this.configuration = optional9;
        this.stickerPackOperations = optional10;
        this.fetchType = optional11;
        this.keys = optional12;
        this.messageRequestResponse = optional13;
        this.outgoingPaymentMessage = optional14;
        this.views = optional15;
        this.pniIdentity = optional16;
    }

    public static SignalServiceSyncMessage forSentTranscript(SentTranscriptMessage sentTranscriptMessage) {
        return new SignalServiceSyncMessage(Optional.of(sentTranscriptMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forContacts(ContactsMessage contactsMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.of(contactsMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forGroups(SignalServiceAttachment signalServiceAttachment) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.of(signalServiceAttachment), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forRequest(RequestMessage requestMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(requestMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forRead(List<ReadMessage> list) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(list), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forViewed(List<ViewedMessage> list) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(list), Optional.empty());
    }

    public static SignalServiceSyncMessage forViewOnceOpen(ViewOnceOpenMessage viewOnceOpenMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(viewOnceOpenMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forRead(ReadMessage readMessage) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(readMessage);
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(linkedList), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forVerified(VerifiedMessage verifiedMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(verifiedMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forBlocked(BlockedListMessage blockedListMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(blockedListMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forConfiguration(ConfigurationMessage configurationMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(configurationMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forStickerPackOperations(List<StickerPackOperationMessage> list) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(list), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forFetchLatest(FetchType fetchType) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(fetchType), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forKeys(KeysMessage keysMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(keysMessage), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forMessageRequestResponse(MessageRequestResponseMessage messageRequestResponseMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(messageRequestResponseMessage), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forOutgoingPayment(OutgoingPaymentMessage outgoingPaymentMessage) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(outgoingPaymentMessage), Optional.empty(), Optional.empty());
    }

    public static SignalServiceSyncMessage forPniIdentity(SignalServiceProtos.SyncMessage.PniIdentity pniIdentity) {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(pniIdentity));
    }

    public static SignalServiceSyncMessage empty() {
        return new SignalServiceSyncMessage(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public Optional<SentTranscriptMessage> getSent() {
        return this.sent;
    }

    public Optional<SignalServiceAttachment> getGroups() {
        return this.groups;
    }

    public Optional<ContactsMessage> getContacts() {
        return this.contacts;
    }

    public Optional<RequestMessage> getRequest() {
        return this.request;
    }

    public Optional<List<ReadMessage>> getRead() {
        return this.reads;
    }

    public Optional<ViewOnceOpenMessage> getViewOnceOpen() {
        return this.viewOnceOpen;
    }

    public Optional<BlockedListMessage> getBlockedList() {
        return this.blockedList;
    }

    public Optional<VerifiedMessage> getVerified() {
        return this.verified;
    }

    public Optional<ConfigurationMessage> getConfiguration() {
        return this.configuration;
    }

    public Optional<List<StickerPackOperationMessage>> getStickerPackOperations() {
        return this.stickerPackOperations;
    }

    public Optional<FetchType> getFetchType() {
        return this.fetchType;
    }

    public Optional<KeysMessage> getKeys() {
        return this.keys;
    }

    public Optional<MessageRequestResponseMessage> getMessageRequestResponse() {
        return this.messageRequestResponse;
    }

    public Optional<OutgoingPaymentMessage> getOutgoingPaymentMessage() {
        return this.outgoingPaymentMessage;
    }

    public Optional<List<ViewedMessage>> getViewed() {
        return this.views;
    }

    public Optional<SignalServiceProtos.SyncMessage.PniIdentity> getPniIdentity() {
        return this.pniIdentity;
    }
}
