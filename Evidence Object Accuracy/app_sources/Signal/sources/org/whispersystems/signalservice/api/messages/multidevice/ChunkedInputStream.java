package org.whispersystems.signalservice.api.messages.multidevice;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes5.dex */
public class ChunkedInputStream {
    protected final InputStream in;

    public ChunkedInputStream(InputStream inputStream) {
        this.in = inputStream;
    }

    public long readRawVarint32() throws IOException {
        long j = 0;
        for (int i = 0; i < 32; i += 7) {
            int read = this.in.read();
            if (read < 0) {
                return -1;
            }
            byte b = (byte) read;
            j |= ((long) (b & Byte.MAX_VALUE)) << i;
            if ((b & 128) == 0) {
                return j;
            }
        }
        throw new IOException("Malformed varint!");
    }

    /* loaded from: classes5.dex */
    public static final class LimitedInputStream extends InputStream {
        private final InputStream in;
        private long left;
        private long mark = -1;

        @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        public LimitedInputStream(InputStream inputStream, long j) {
            this.in = inputStream;
            this.left = j;
        }

        @Override // java.io.InputStream
        public int available() throws IOException {
            return (int) Math.min((long) this.in.available(), this.left);
        }

        @Override // java.io.InputStream
        public synchronized void mark(int i) {
            this.in.mark(i);
            this.mark = this.left;
        }

        @Override // java.io.InputStream
        public int read() throws IOException {
            if (this.left == 0) {
                return -1;
            }
            int read = this.in.read();
            if (read != -1) {
                this.left--;
            }
            return read;
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            long j = this.left;
            if (j == 0) {
                return -1;
            }
            int read = this.in.read(bArr, i, (int) Math.min((long) i2, j));
            if (read != -1) {
                this.left -= (long) read;
            }
            return read;
        }

        @Override // java.io.InputStream
        public synchronized void reset() throws IOException {
            if (!this.in.markSupported()) {
                throw new IOException("Mark not supported");
            } else if (this.mark != -1) {
                this.in.reset();
                this.left = this.mark;
            } else {
                throw new IOException("Mark not set");
            }
        }

        @Override // java.io.InputStream
        public long skip(long j) throws IOException {
            long skip = this.in.skip(Math.min(j, this.left));
            this.left -= skip;
            return skip;
        }
    }
}
