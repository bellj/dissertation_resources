package org.whispersystems.signalservice.api.groupsv2;

import j$.util.Optional;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;

/* loaded from: classes5.dex */
public final class DecryptedGroupHistoryEntry {
    private final Optional<DecryptedGroupChange> change;
    private final Optional<DecryptedGroup> group;

    public DecryptedGroupHistoryEntry(Optional<DecryptedGroup> optional, Optional<DecryptedGroupChange> optional2) throws InvalidGroupStateException {
        if (!optional.isPresent() || !optional2.isPresent() || optional.get().getRevision() == optional2.get().getRevision()) {
            this.group = optional;
            this.change = optional2;
            return;
        }
        throw new InvalidGroupStateException();
    }

    public Optional<DecryptedGroup> getGroup() {
        return this.group;
    }

    public Optional<DecryptedGroupChange> getChange() {
        return this.change;
    }
}
