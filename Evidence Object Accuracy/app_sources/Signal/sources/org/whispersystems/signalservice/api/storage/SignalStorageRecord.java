package org.whispersystems.signalservice.api.storage;

import j$.util.Optional;
import java.util.Objects;

/* loaded from: classes5.dex */
public class SignalStorageRecord implements SignalRecord {
    private final Optional<SignalAccountRecord> account;
    private final Optional<SignalContactRecord> contact;
    private final Optional<SignalGroupV1Record> groupV1;
    private final Optional<SignalGroupV2Record> groupV2;
    private final StorageId id;
    private final Optional<SignalStoryDistributionListRecord> storyDistributionList;

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public SignalStorageRecord asStorageRecord() {
        return this;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public String describeDiff(SignalRecord signalRecord) {
        return "Diffs not supported.";
    }

    public static SignalStorageRecord forStoryDistributionList(SignalStoryDistributionListRecord signalStoryDistributionListRecord) {
        return forStoryDistributionList(signalStoryDistributionListRecord.getId(), signalStoryDistributionListRecord);
    }

    public static SignalStorageRecord forStoryDistributionList(StorageId storageId, SignalStoryDistributionListRecord signalStoryDistributionListRecord) {
        return new SignalStorageRecord(storageId, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(signalStoryDistributionListRecord));
    }

    public static SignalStorageRecord forContact(SignalContactRecord signalContactRecord) {
        return forContact(signalContactRecord.getId(), signalContactRecord);
    }

    public static SignalStorageRecord forContact(StorageId storageId, SignalContactRecord signalContactRecord) {
        return new SignalStorageRecord(storageId, Optional.of(signalContactRecord), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalStorageRecord forGroupV1(SignalGroupV1Record signalGroupV1Record) {
        return forGroupV1(signalGroupV1Record.getId(), signalGroupV1Record);
    }

    public static SignalStorageRecord forGroupV1(StorageId storageId, SignalGroupV1Record signalGroupV1Record) {
        return new SignalStorageRecord(storageId, Optional.empty(), Optional.of(signalGroupV1Record), Optional.empty(), Optional.empty(), Optional.empty());
    }

    public static SignalStorageRecord forGroupV2(SignalGroupV2Record signalGroupV2Record) {
        return forGroupV2(signalGroupV2Record.getId(), signalGroupV2Record);
    }

    public static SignalStorageRecord forGroupV2(StorageId storageId, SignalGroupV2Record signalGroupV2Record) {
        return new SignalStorageRecord(storageId, Optional.empty(), Optional.empty(), Optional.of(signalGroupV2Record), Optional.empty(), Optional.empty());
    }

    public static SignalStorageRecord forAccount(SignalAccountRecord signalAccountRecord) {
        return forAccount(signalAccountRecord.getId(), signalAccountRecord);
    }

    public static SignalStorageRecord forAccount(StorageId storageId, SignalAccountRecord signalAccountRecord) {
        return new SignalStorageRecord(storageId, Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(signalAccountRecord), Optional.empty());
    }

    public static SignalStorageRecord forUnknown(StorageId storageId) {
        return new SignalStorageRecord(storageId, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    private SignalStorageRecord(StorageId storageId, Optional<SignalContactRecord> optional, Optional<SignalGroupV1Record> optional2, Optional<SignalGroupV2Record> optional3, Optional<SignalAccountRecord> optional4, Optional<SignalStoryDistributionListRecord> optional5) {
        this.id = storageId;
        this.contact = optional;
        this.groupV1 = optional2;
        this.groupV2 = optional3;
        this.account = optional4;
        this.storyDistributionList = optional5;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public StorageId getId() {
        return this.id;
    }

    public int getType() {
        return this.id.getType();
    }

    public Optional<SignalContactRecord> getContact() {
        return this.contact;
    }

    public Optional<SignalGroupV1Record> getGroupV1() {
        return this.groupV1;
    }

    public Optional<SignalGroupV2Record> getGroupV2() {
        return this.groupV2;
    }

    public Optional<SignalAccountRecord> getAccount() {
        return this.account;
    }

    public Optional<SignalStoryDistributionListRecord> getStoryDistributionList() {
        return this.storyDistributionList;
    }

    public boolean isUnknown() {
        return !this.contact.isPresent() && !this.groupV1.isPresent() && !this.groupV2.isPresent() && !this.account.isPresent() && !this.storyDistributionList.isPresent();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SignalStorageRecord signalStorageRecord = (SignalStorageRecord) obj;
        if (!Objects.equals(this.id, signalStorageRecord.id) || !Objects.equals(this.contact, signalStorageRecord.contact) || !Objects.equals(this.groupV1, signalStorageRecord.groupV1) || !Objects.equals(this.groupV2, signalStorageRecord.groupV2) || !Objects.equals(this.storyDistributionList, signalStorageRecord.storyDistributionList)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.contact, this.groupV1, this.groupV2, this.storyDistributionList);
    }
}
