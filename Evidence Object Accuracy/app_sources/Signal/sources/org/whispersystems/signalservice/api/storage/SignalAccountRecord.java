package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.api.util.ProtoUtil;
import org.whispersystems.signalservice.internal.storage.protos.AccountRecord;
import org.whispersystems.signalservice.internal.storage.protos.Payments;

/* loaded from: classes5.dex */
public final class SignalAccountRecord implements SignalRecord {
    private static final String TAG;
    private final Optional<String> avatarUrlPath;
    private final List<String> defaultReactions;
    private final Optional<String> familyName;
    private final Optional<String> givenName;
    private final boolean hasUnknownFields;
    private final StorageId id;
    private final Payments payments;
    private final List<PinnedConversation> pinnedConversations;
    private final Optional<byte[]> profileKey;
    private final AccountRecord proto;
    private final Subscriber subscriber;

    public SignalAccountRecord(StorageId storageId, AccountRecord accountRecord) {
        this.id = storageId;
        this.proto = accountRecord;
        this.hasUnknownFields = ProtoUtil.hasUnknownFields(accountRecord);
        this.givenName = OptionalUtil.absentIfEmpty(accountRecord.getGivenName());
        this.familyName = OptionalUtil.absentIfEmpty(accountRecord.getFamilyName());
        this.profileKey = OptionalUtil.absentIfEmpty(accountRecord.getProfileKey());
        this.avatarUrlPath = OptionalUtil.absentIfEmpty(accountRecord.getAvatarUrlPath());
        this.pinnedConversations = new ArrayList(accountRecord.getPinnedConversationsCount());
        this.payments = new Payments(accountRecord.getPayments().getEnabled(), OptionalUtil.absentIfEmpty(accountRecord.getPayments().getEntropy()));
        this.defaultReactions = new ArrayList(accountRecord.getPreferredReactionEmojiList());
        this.subscriber = new Subscriber(accountRecord.getSubscriberCurrencyCode(), accountRecord.getSubscriberId().toByteArray());
        for (AccountRecord.PinnedConversation pinnedConversation : accountRecord.getPinnedConversationsList()) {
            this.pinnedConversations.add(PinnedConversation.fromRemote(pinnedConversation));
        }
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public StorageId getId() {
        return this.id;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public SignalStorageRecord asStorageRecord() {
        return SignalStorageRecord.forAccount(this);
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public String describeDiff(SignalRecord signalRecord) {
        if (signalRecord instanceof SignalAccountRecord) {
            SignalAccountRecord signalAccountRecord = (SignalAccountRecord) signalRecord;
            LinkedList linkedList = new LinkedList();
            if (!Arrays.equals(this.id.getRaw(), signalAccountRecord.id.getRaw())) {
                linkedList.add("ID");
            }
            if (!Objects.equals(this.givenName, signalAccountRecord.givenName)) {
                linkedList.add("GivenName");
            }
            if (!Objects.equals(this.familyName, signalAccountRecord.familyName)) {
                linkedList.add("FamilyName");
            }
            if (!OptionalUtil.byteArrayEquals(this.profileKey, signalAccountRecord.profileKey)) {
                linkedList.add("ProfileKey");
            }
            if (!Objects.equals(this.avatarUrlPath, signalAccountRecord.avatarUrlPath)) {
                linkedList.add("AvatarUrlPath");
            }
            if (!Objects.equals(Boolean.valueOf(isNoteToSelfArchived()), Boolean.valueOf(signalAccountRecord.isNoteToSelfArchived()))) {
                linkedList.add("NoteToSelfArchived");
            }
            if (!Objects.equals(Boolean.valueOf(isNoteToSelfForcedUnread()), Boolean.valueOf(signalAccountRecord.isNoteToSelfForcedUnread()))) {
                linkedList.add("NoteToSelfForcedUnread");
            }
            if (!Objects.equals(Boolean.valueOf(isReadReceiptsEnabled()), Boolean.valueOf(signalAccountRecord.isReadReceiptsEnabled()))) {
                linkedList.add("ReadReceipts");
            }
            if (!Objects.equals(Boolean.valueOf(isTypingIndicatorsEnabled()), Boolean.valueOf(signalAccountRecord.isTypingIndicatorsEnabled()))) {
                linkedList.add("TypingIndicators");
            }
            if (!Objects.equals(Boolean.valueOf(isSealedSenderIndicatorsEnabled()), Boolean.valueOf(signalAccountRecord.isSealedSenderIndicatorsEnabled()))) {
                linkedList.add("SealedSenderIndicators");
            }
            if (!Objects.equals(Boolean.valueOf(isLinkPreviewsEnabled()), Boolean.valueOf(signalAccountRecord.isLinkPreviewsEnabled()))) {
                linkedList.add("LinkPreviews");
            }
            if (!Objects.equals(getPhoneNumberSharingMode(), signalAccountRecord.getPhoneNumberSharingMode())) {
                linkedList.add("PhoneNumberSharingMode");
            }
            if (!Objects.equals(Boolean.valueOf(isPhoneNumberUnlisted()), Boolean.valueOf(signalAccountRecord.isPhoneNumberUnlisted()))) {
                linkedList.add("PhoneNumberUnlisted");
            }
            if (!Objects.equals(this.pinnedConversations, signalAccountRecord.pinnedConversations)) {
                linkedList.add("PinnedConversations");
            }
            if (!Objects.equals(Boolean.valueOf(isPreferContactAvatars()), Boolean.valueOf(signalAccountRecord.isPreferContactAvatars()))) {
                linkedList.add("PreferContactAvatars");
            }
            if (!Objects.equals(this.payments, signalAccountRecord.payments)) {
                linkedList.add("Payments");
            }
            if (getUniversalExpireTimer() != signalAccountRecord.getUniversalExpireTimer()) {
                linkedList.add("UniversalExpireTimer");
            }
            if (!Objects.equals(Boolean.valueOf(isPrimarySendsSms()), Boolean.valueOf(signalAccountRecord.isPrimarySendsSms()))) {
                linkedList.add("PrimarySendsSms");
            }
            if (!Objects.equals(getE164(), signalAccountRecord.getE164())) {
                linkedList.add("E164");
            }
            if (!Objects.equals(getDefaultReactions(), signalAccountRecord.getDefaultReactions())) {
                linkedList.add("DefaultReactions");
            }
            if (!Objects.equals(Boolean.valueOf(hasUnknownFields()), Boolean.valueOf(signalAccountRecord.hasUnknownFields()))) {
                linkedList.add("UnknownFields");
            }
            if (!Objects.equals(getSubscriber(), signalAccountRecord.getSubscriber())) {
                linkedList.add("Subscriber");
            }
            if (!Objects.equals(Boolean.valueOf(isDisplayBadgesOnProfile()), Boolean.valueOf(signalAccountRecord.isDisplayBadgesOnProfile()))) {
                linkedList.add("DisplayBadgesOnProfile");
            }
            if (!Objects.equals(Boolean.valueOf(isSubscriptionManuallyCancelled()), Boolean.valueOf(signalAccountRecord.isSubscriptionManuallyCancelled()))) {
                linkedList.add("SubscriptionManuallyCancelled");
            }
            return linkedList.toString();
        }
        return "Different class. " + SignalAccountRecord.class.getSimpleName() + " | " + signalRecord.getClass().getSimpleName();
    }

    public boolean hasUnknownFields() {
        return this.hasUnknownFields;
    }

    public byte[] serializeUnknownFields() {
        if (this.hasUnknownFields) {
            return this.proto.toByteArray();
        }
        return null;
    }

    public Optional<String> getGivenName() {
        return this.givenName;
    }

    public Optional<String> getFamilyName() {
        return this.familyName;
    }

    public Optional<byte[]> getProfileKey() {
        return this.profileKey;
    }

    public Optional<String> getAvatarUrlPath() {
        return this.avatarUrlPath;
    }

    public boolean isNoteToSelfArchived() {
        return this.proto.getNoteToSelfArchived();
    }

    public boolean isNoteToSelfForcedUnread() {
        return this.proto.getNoteToSelfMarkedUnread();
    }

    public boolean isReadReceiptsEnabled() {
        return this.proto.getReadReceipts();
    }

    public boolean isTypingIndicatorsEnabled() {
        return this.proto.getTypingIndicators();
    }

    public boolean isSealedSenderIndicatorsEnabled() {
        return this.proto.getSealedSenderIndicators();
    }

    public boolean isLinkPreviewsEnabled() {
        return this.proto.getLinkPreviews();
    }

    public AccountRecord.PhoneNumberSharingMode getPhoneNumberSharingMode() {
        return this.proto.getPhoneNumberSharingMode();
    }

    public boolean isPhoneNumberUnlisted() {
        return this.proto.getUnlistedPhoneNumber();
    }

    public List<PinnedConversation> getPinnedConversations() {
        return this.pinnedConversations;
    }

    public boolean isPreferContactAvatars() {
        return this.proto.getPreferContactAvatars();
    }

    public Payments getPayments() {
        return this.payments;
    }

    public int getUniversalExpireTimer() {
        return this.proto.getUniversalExpireTimer();
    }

    public boolean isPrimarySendsSms() {
        return this.proto.getPrimarySendsSms();
    }

    public String getE164() {
        return this.proto.getE164();
    }

    public List<String> getDefaultReactions() {
        return this.defaultReactions;
    }

    public Subscriber getSubscriber() {
        return this.subscriber;
    }

    public boolean isDisplayBadgesOnProfile() {
        return this.proto.getDisplayBadgesOnProfile();
    }

    public boolean isSubscriptionManuallyCancelled() {
        return this.proto.getSubscriptionManuallyCancelled();
    }

    public AccountRecord toProto() {
        return this.proto;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SignalAccountRecord.class != obj.getClass()) {
            return false;
        }
        SignalAccountRecord signalAccountRecord = (SignalAccountRecord) obj;
        if (!this.id.equals(signalAccountRecord.id) || !this.proto.equals(signalAccountRecord.proto)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.proto);
    }

    /* loaded from: classes5.dex */
    public static class PinnedConversation {
        private final Optional<SignalServiceAddress> contact;
        private final Optional<byte[]> groupV1Id;
        private final Optional<byte[]> groupV2MasterKey;

        private PinnedConversation(Optional<SignalServiceAddress> optional, Optional<byte[]> optional2, Optional<byte[]> optional3) {
            this.contact = optional;
            this.groupV1Id = optional2;
            this.groupV2MasterKey = optional3;
        }

        public static PinnedConversation forContact(SignalServiceAddress signalServiceAddress) {
            return new PinnedConversation(Optional.of(signalServiceAddress), Optional.empty(), Optional.empty());
        }

        public static PinnedConversation forGroupV1(byte[] bArr) {
            return new PinnedConversation(Optional.empty(), Optional.of(bArr), Optional.empty());
        }

        public static PinnedConversation forGroupV2(byte[] bArr) {
            return new PinnedConversation(Optional.empty(), Optional.empty(), Optional.of(bArr));
        }

        private static PinnedConversation forEmpty() {
            return new PinnedConversation(Optional.empty(), Optional.empty(), Optional.empty());
        }

        static PinnedConversation fromRemote(AccountRecord.PinnedConversation pinnedConversation) {
            if (pinnedConversation.hasContact()) {
                ServiceId parseOrNull = ServiceId.parseOrNull(pinnedConversation.getContact().getUuid());
                if (parseOrNull != null) {
                    return forContact(new SignalServiceAddress(parseOrNull, pinnedConversation.getContact().getE164()));
                }
                String str = SignalAccountRecord.TAG;
                Log.w(str, "Bad serviceId on pinned contact! Length: " + pinnedConversation.getContact().getUuid());
                return forEmpty();
            } else if (!pinnedConversation.getLegacyGroupId().isEmpty()) {
                return forGroupV1(pinnedConversation.getLegacyGroupId().toByteArray());
            } else {
                if (!pinnedConversation.getGroupMasterKey().isEmpty()) {
                    return forGroupV2(pinnedConversation.getGroupMasterKey().toByteArray());
                }
                return forEmpty();
            }
        }

        public Optional<SignalServiceAddress> getContact() {
            return this.contact;
        }

        public Optional<byte[]> getGroupV1Id() {
            return this.groupV1Id;
        }

        public Optional<byte[]> getGroupV2MasterKey() {
            return this.groupV2MasterKey;
        }

        public boolean isValid() {
            return this.contact.isPresent() || this.groupV1Id.isPresent() || this.groupV2MasterKey.isPresent();
        }

        public AccountRecord.PinnedConversation toRemote() {
            if (this.contact.isPresent()) {
                AccountRecord.PinnedConversation.Contact.Builder newBuilder = AccountRecord.PinnedConversation.Contact.newBuilder();
                newBuilder.setUuid(this.contact.get().getServiceId().toString());
                if (this.contact.get().getNumber().isPresent()) {
                    newBuilder.setE164(this.contact.get().getNumber().get());
                }
                return AccountRecord.PinnedConversation.newBuilder().setContact(newBuilder.build()).build();
            } else if (this.groupV1Id.isPresent()) {
                return AccountRecord.PinnedConversation.newBuilder().setLegacyGroupId(ByteString.copyFrom(this.groupV1Id.get())).build();
            } else {
                if (this.groupV2MasterKey.isPresent()) {
                    return AccountRecord.PinnedConversation.newBuilder().setGroupMasterKey(ByteString.copyFrom(this.groupV2MasterKey.get())).build();
                }
                return AccountRecord.PinnedConversation.newBuilder().build();
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            PinnedConversation pinnedConversation = (PinnedConversation) obj;
            if (!this.contact.equals(pinnedConversation.contact) || !this.groupV1Id.equals(pinnedConversation.groupV1Id) || !this.groupV2MasterKey.equals(pinnedConversation.groupV2MasterKey)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hash(this.contact, this.groupV1Id, this.groupV2MasterKey);
        }
    }

    /* loaded from: classes5.dex */
    public static class Subscriber {
        private final Optional<String> currencyCode;
        private final Optional<byte[]> id;

        public Subscriber(String str, byte[] bArr) {
            if (str == null || bArr == null || bArr.length != 32) {
                this.currencyCode = Optional.empty();
                this.id = Optional.empty();
                return;
            }
            this.currencyCode = Optional.of(str);
            this.id = Optional.of(bArr);
        }

        public Optional<String> getCurrencyCode() {
            return this.currencyCode;
        }

        public Optional<byte[]> getId() {
            return this.id;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Subscriber subscriber = (Subscriber) obj;
            if (!Objects.equals(this.currencyCode, subscriber.currencyCode) || !OptionalUtil.byteArrayEquals(this.id, subscriber.id)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hash(this.currencyCode, this.id);
        }
    }

    /* loaded from: classes5.dex */
    public static class Payments {
        private static final String TAG;
        private final boolean enabled;
        private final Optional<byte[]> entropy;

        public Payments(boolean z, Optional<byte[]> optional) {
            byte[] bArr = null;
            byte[] orElse = optional.orElse(null);
            if (orElse == null || orElse.length == 32) {
                bArr = orElse;
            } else {
                String str = TAG;
                Log.w(str, "Blocked entropy of length " + orElse.length);
            }
            Optional<byte[]> ofNullable = Optional.ofNullable(bArr);
            this.entropy = ofNullable;
            this.enabled = z && ofNullable.isPresent();
        }

        public boolean isEnabled() {
            return this.enabled;
        }

        public Optional<byte[]> getEntropy() {
            return this.entropy;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Payments payments = (Payments) obj;
            if (this.enabled != payments.enabled || !OptionalUtil.byteArrayEquals(this.entropy, payments.entropy)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hash(Boolean.valueOf(this.enabled), this.entropy);
        }
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        private final AccountRecord.Builder builder;
        private final StorageId id;

        public Builder(byte[] bArr, byte[] bArr2) {
            this.id = StorageId.forAccount(bArr);
            if (bArr2 != null) {
                this.builder = parseUnknowns(bArr2);
            } else {
                this.builder = AccountRecord.newBuilder();
            }
        }

        public Builder setGivenName(String str) {
            AccountRecord.Builder builder = this.builder;
            if (str == null) {
                str = "";
            }
            builder.setGivenName(str);
            return this;
        }

        public Builder setFamilyName(String str) {
            AccountRecord.Builder builder = this.builder;
            if (str == null) {
                str = "";
            }
            builder.setFamilyName(str);
            return this;
        }

        public Builder setProfileKey(byte[] bArr) {
            this.builder.setProfileKey(bArr == null ? ByteString.EMPTY : ByteString.copyFrom(bArr));
            return this;
        }

        public Builder setAvatarUrlPath(String str) {
            AccountRecord.Builder builder = this.builder;
            if (str == null) {
                str = "";
            }
            builder.setAvatarUrlPath(str);
            return this;
        }

        public Builder setNoteToSelfArchived(boolean z) {
            this.builder.setNoteToSelfArchived(z);
            return this;
        }

        public Builder setNoteToSelfForcedUnread(boolean z) {
            this.builder.setNoteToSelfMarkedUnread(z);
            return this;
        }

        public Builder setReadReceiptsEnabled(boolean z) {
            this.builder.setReadReceipts(z);
            return this;
        }

        public Builder setTypingIndicatorsEnabled(boolean z) {
            this.builder.setTypingIndicators(z);
            return this;
        }

        public Builder setSealedSenderIndicatorsEnabled(boolean z) {
            this.builder.setSealedSenderIndicators(z);
            return this;
        }

        public Builder setLinkPreviewsEnabled(boolean z) {
            this.builder.setLinkPreviews(z);
            return this;
        }

        public Builder setPhoneNumberSharingMode(AccountRecord.PhoneNumberSharingMode phoneNumberSharingMode) {
            this.builder.setPhoneNumberSharingMode(phoneNumberSharingMode);
            return this;
        }

        public Builder setUnlistedPhoneNumber(boolean z) {
            this.builder.setUnlistedPhoneNumber(z);
            return this;
        }

        public Builder setPinnedConversations(List<PinnedConversation> list) {
            this.builder.clearPinnedConversations();
            for (PinnedConversation pinnedConversation : list) {
                this.builder.addPinnedConversations(pinnedConversation.toRemote());
            }
            return this;
        }

        public Builder setPreferContactAvatars(boolean z) {
            this.builder.setPreferContactAvatars(z);
            return this;
        }

        public Builder setPayments(boolean z, byte[] bArr) {
            Payments.Builder newBuilder = org.whispersystems.signalservice.internal.storage.protos.Payments.newBuilder();
            boolean z2 = true;
            boolean z3 = bArr != null && bArr.length == 32;
            if (!z || !z3) {
                z2 = false;
            }
            newBuilder.setEnabled(z2);
            if (z3) {
                newBuilder.setEntropy(ByteString.copyFrom(bArr));
            }
            this.builder.setPayments(newBuilder);
            return this;
        }

        public Builder setUniversalExpireTimer(int i) {
            this.builder.setUniversalExpireTimer(i);
            return this;
        }

        public Builder setPrimarySendsSms(boolean z) {
            this.builder.setPrimarySendsSms(z);
            return this;
        }

        public Builder setE164(String str) {
            this.builder.setE164(str);
            return this;
        }

        public Builder setDefaultReactions(List<String> list) {
            this.builder.clearPreferredReactionEmoji();
            this.builder.addAllPreferredReactionEmoji(list);
            return this;
        }

        public Builder setSubscriber(Subscriber subscriber) {
            if (!subscriber.id.isPresent() || !subscriber.currencyCode.isPresent()) {
                this.builder.clearSubscriberId();
                this.builder.clearSubscriberCurrencyCode();
            } else {
                this.builder.setSubscriberId(ByteString.copyFrom((byte[]) subscriber.id.get()));
                this.builder.setSubscriberCurrencyCode((String) subscriber.currencyCode.get());
            }
            return this;
        }

        public Builder setDisplayBadgesOnProfile(boolean z) {
            this.builder.setDisplayBadgesOnProfile(z);
            return this;
        }

        public Builder setSubscriptionManuallyCancelled(boolean z) {
            this.builder.setSubscriptionManuallyCancelled(z);
            return this;
        }

        private static AccountRecord.Builder parseUnknowns(byte[] bArr) {
            try {
                return AccountRecord.parseFrom(bArr).toBuilder();
            } catch (InvalidProtocolBufferException e) {
                Log.w(SignalAccountRecord.TAG, "Failed to combine unknown fields!", e);
                return AccountRecord.newBuilder();
            }
        }

        public SignalAccountRecord build() {
            return new SignalAccountRecord(this.id, this.builder.build());
        }
    }
}
