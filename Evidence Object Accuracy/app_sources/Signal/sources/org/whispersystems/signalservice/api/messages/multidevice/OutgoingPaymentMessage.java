package org.whispersystems.signalservice.api.messages.multidevice;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.util.List;
import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes5.dex */
public final class OutgoingPaymentMessage {
    private final Optional<byte[]> address;
    private final Money.MobileCoin amount;
    private final long blockIndex;
    private final long blockTimestamp;
    private final Money.MobileCoin fee;
    private final List<ByteString> keyImages;
    private final Optional<String> note;
    private final List<ByteString> publicKeys;
    private final ByteString receipt;
    private final Optional<ServiceId> recipient;

    public OutgoingPaymentMessage(Optional<ServiceId> optional, Money.MobileCoin mobileCoin, Money.MobileCoin mobileCoin2, ByteString byteString, long j, long j2, Optional<byte[]> optional2, Optional<String> optional3, List<ByteString> list, List<ByteString> list2) {
        this.recipient = optional;
        this.amount = mobileCoin;
        this.fee = mobileCoin2;
        this.receipt = byteString;
        this.blockIndex = j;
        this.blockTimestamp = j2;
        this.address = optional2;
        this.note = optional3;
        this.publicKeys = list;
        this.keyImages = list2;
    }

    public Optional<ServiceId> getRecipient() {
        return this.recipient;
    }

    public Money.MobileCoin getAmount() {
        return this.amount;
    }

    public ByteString getReceipt() {
        return this.receipt;
    }

    public Money.MobileCoin getFee() {
        return this.fee;
    }

    public long getBlockIndex() {
        return this.blockIndex;
    }

    public long getBlockTimestamp() {
        return this.blockTimestamp;
    }

    public Optional<byte[]> getAddress() {
        return this.address;
    }

    public Optional<String> getNote() {
        return this.note;
    }

    public List<ByteString> getPublicKeys() {
        return this.publicKeys;
    }

    public List<ByteString> getKeyImages() {
        return this.keyImages;
    }
}
