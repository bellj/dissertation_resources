package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import j$.util.function.Consumer;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.WebSocket;
import org.signal.cdsi.proto.ClientRequest;
import org.signal.cdsi.proto.ClientResponse;
import org.signal.libsignal.cds2.Cds2Client;
import org.signal.libsignal.protocol.util.Pair;
import org.whispersystems.signalservice.api.push.TrustStore;
import org.whispersystems.signalservice.api.util.Tls12SocketFactory;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.util.BlacklistingTrustManager;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public final class CdsiSocket {
    private static final byte[] CERTIFICATE = "-----BEGIN CERTIFICATE-----\n      MIICjzCCAjSgAwIBAgIUImUM1lqdNInzg7SVUr9QGzknBqwwCgYIKoZIzj0EAwIw\n      aDEaMBgGA1UEAwwRSW50ZWwgU0dYIFJvb3QgQ0ExGjAYBgNVBAoMEUludGVsIENv\n      cnBvcmF0aW9uMRQwEgYDVQQHDAtTYW50YSBDbGFyYTELMAkGA1UECAwCQ0ExCzAJ\n      BgNVBAYTAlVTMB4XDTE4MDUyMTEwNDUxMFoXDTQ5MTIzMTIzNTk1OVowaDEaMBgG\n      A1UEAwwRSW50ZWwgU0dYIFJvb3QgQ0ExGjAYBgNVBAoMEUludGVsIENvcnBvcmF0\n      aW9uMRQwEgYDVQQHDAtTYW50YSBDbGFyYTELMAkGA1UECAwCQ0ExCzAJBgNVBAYT\n      AlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEC6nEwMDIYZOj/iPWsCzaEKi7\n      1OiOSLRFhWGjbnBVJfVnkY4u3IjkDYYL0MxO4mqsyYjlBalTVYxFP2sJBK5zlKOB\n      uzCBuDAfBgNVHSMEGDAWgBQiZQzWWp00ifODtJVSv1AbOScGrDBSBgNVHR8ESzBJ\n      MEegRaBDhkFodHRwczovL2NlcnRpZmljYXRlcy50cnVzdGVkc2VydmljZXMuaW50\n      ZWwuY29tL0ludGVsU0dYUm9vdENBLmRlcjAdBgNVHQ4EFgQUImUM1lqdNInzg7SV\n      Ur9QGzknBqwwDgYDVR0PAQH/BAQDAgEGMBIGA1UdEwEB/wQIMAYBAf8CAQEwCgYI\n      KoZIzj0EAwIDSQAwRgIhAOW/5QkR+S9CiSDcNoowLuPRLsWGf/Yi7GSX94BgwTwg\n      AiEA4J0lrHoMs+Xo5o/sX6O9QWxHRAvZUGOdRQ7cvqRXaqI=\n      -----END CERTIFICATE-----".getBytes(StandardCharsets.UTF_8);
    private static final String TAG;
    private final String baseUrl;
    private Cds2Client client;
    private final String mrEnclave;
    private final OkHttpClient okhttp;

    /* loaded from: classes5.dex */
    public enum Stage {
        INIT,
        WAITING_FOR_CONNECTION,
        WAITING_FOR_HANDSHAKE,
        WAITING_FOR_TOKEN,
        WAITING_TO_INITIALIZE,
        WAITING_FOR_RESPONSE,
        CLOSED,
        FAILED
    }

    public CdsiSocket(SignalServiceConfiguration signalServiceConfiguration, String str) {
        this.baseUrl = signalServiceConfiguration.getSignalCdsiUrls()[0].getUrl();
        this.mrEnclave = str;
        Pair<SSLSocketFactory, X509TrustManager> createTlsSocketFactory = createTlsSocketFactory(signalServiceConfiguration.getSignalCdsiUrls()[0].getTrustStore());
        OkHttpClient.Builder connectionSpecs = new OkHttpClient.Builder().sslSocketFactory(new Tls12SocketFactory(createTlsSocketFactory.first()), createTlsSocketFactory.second()).connectionSpecs(Util.immutableList(ConnectionSpec.RESTRICTED_TLS));
        TimeUnit timeUnit = TimeUnit.SECONDS;
        this.okhttp = connectionSpecs.readTimeout(30, timeUnit).connectTimeout(30, timeUnit).build();
    }

    public Observable<ClientResponse> connect(String str, String str2, ClientRequest clientRequest, Consumer<byte[]> consumer) {
        return Observable.create(new ObservableOnSubscribe(str, str2, clientRequest, consumer) { // from class: org.whispersystems.signalservice.api.services.CdsiSocket$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ ClientRequest f$3;
            public final /* synthetic */ Consumer f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                CdsiSocket.$r8$lambda$zY6lyD76L2fvI6yHUenQjBtPB20(CdsiSocket.this, this.f$1, this.f$2, this.f$3, this.f$4, observableEmitter);
            }
        });
    }

    public /* synthetic */ void lambda$connect$1(String str, String str2, final ClientRequest clientRequest, final Consumer consumer, final ObservableEmitter observableEmitter) throws Throwable {
        final AtomicReference atomicReference = new AtomicReference(Stage.WAITING_TO_INITIALIZE);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.whispersystems.signalservice.api.services.CdsiSocket$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                CdsiSocket.m3362$r8$lambda$FBsa2UNQaIwX3wRh7UeKlbAq6w(WebSocket.this);
            }
        });
    }

    /* renamed from: org.whispersystems.signalservice.api.services.CdsiSocket$2 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage;

        static {
            int[] iArr = new int[Stage.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage = iArr;
            try {
                iArr[Stage.INIT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage[Stage.WAITING_FOR_CONNECTION.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage[Stage.WAITING_FOR_HANDSHAKE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage[Stage.WAITING_FOR_TOKEN.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage[Stage.WAITING_FOR_RESPONSE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage[Stage.CLOSED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$services$CdsiSocket$Stage[Stage.FAILED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    private static String basicAuth(String str, String str2) {
        return "Basic " + Base64.encodeBytes((str + ":" + str2).getBytes(StandardCharsets.UTF_8));
    }

    private static Pair<SSLSocketFactory, X509TrustManager> createTlsSocketFactory(TrustStore trustStore) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            TrustManager[] createFor = BlacklistingTrustManager.createFor(trustStore);
            instance.init(null, createFor, null);
            return new Pair<>(instance.getSocketFactory(), (X509TrustManager) createFor[0]);
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
