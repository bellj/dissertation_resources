package org.whispersystems.signalservice.api.push;

import com.google.protobuf.ByteString;
import java.util.Objects;
import java.util.UUID;

/* loaded from: classes5.dex */
public final class ServiceIds {
    private final ACI aci;
    private ByteString aciByteString;
    private final PNI pni;
    private ByteString pniByteString;

    public ServiceIds(ACI aci, PNI pni) {
        this.aci = aci;
        this.pni = pni;
    }

    public ACI getAci() {
        return this.aci;
    }

    public PNI getPni() {
        return this.pni;
    }

    public PNI requirePni() {
        PNI pni = this.pni;
        Objects.requireNonNull(pni);
        return pni;
    }

    public boolean matches(UUID uuid) {
        PNI pni;
        return uuid.equals(this.aci.uuid()) || ((pni = this.pni) != null && uuid.equals(pni.uuid()));
    }

    public boolean matches(ByteString byteString) {
        PNI pni;
        if (this.aciByteString == null) {
            this.aciByteString = this.aci.toByteString();
        }
        if (this.pniByteString == null && (pni = this.pni) != null) {
            this.pniByteString = pni.toByteString();
        }
        return byteString.equals(this.aciByteString) || byteString.equals(this.pniByteString);
    }
}
