package org.whispersystems.signalservice.api.util;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/* loaded from: classes5.dex */
public final class UuidUtil {
    public static final UUID UNKNOWN_UUID = new UUID(0, 0);
    private static final Pattern UUID_PATTERN = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", 2);

    private UuidUtil() {
    }

    public static Optional<UUID> parse(String str) {
        return Optional.ofNullable(parseOrNull(str));
    }

    public static UUID parseOrNull(String str) {
        if (isUuid(str)) {
            return parseOrThrow(str);
        }
        return null;
    }

    public static UUID parseOrUnknown(String str) {
        return (str == null || str.isEmpty()) ? UNKNOWN_UUID : parseOrThrow(str);
    }

    public static UUID parseOrThrow(String str) {
        return UUID.fromString(str);
    }

    public static UUID parseOrThrow(byte[] bArr) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        return new UUID(wrap.getLong(), wrap.getLong());
    }

    public static boolean isUuid(String str) {
        return str != null && UUID_PATTERN.matcher(str).matches();
    }

    public static byte[] toByteArray(UUID uuid) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }

    public static ByteString toByteString(UUID uuid) {
        return ByteString.copyFrom(toByteArray(uuid));
    }

    public static UUID fromByteString(ByteString byteString) {
        return parseOrThrow(byteString.toByteArray());
    }

    public static UUID fromByteStringOrNull(ByteString byteString) {
        return parseOrNull(byteString.toByteArray());
    }

    public static UUID fromByteStringOrUnknown(ByteString byteString) {
        UUID fromByteStringOrNull = fromByteStringOrNull(byteString);
        return fromByteStringOrNull != null ? fromByteStringOrNull : UNKNOWN_UUID;
    }

    public static UUID parseOrNull(byte[] bArr) {
        if (bArr == null || bArr.length != 16) {
            return null;
        }
        return parseOrThrow(bArr);
    }

    public static List<UUID> fromByteStrings(Collection<ByteString> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (ByteString byteString : collection) {
            arrayList.add(fromByteString(byteString));
        }
        return arrayList;
    }

    public static List<UUID> filterKnown(Collection<UUID> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (UUID uuid : collection) {
            if (!UNKNOWN_UUID.equals(uuid)) {
                arrayList.add(uuid);
            }
        }
        return arrayList;
    }
}
