package org.whispersystems.signalservice.api.websocket;

import org.whispersystems.signalservice.internal.websocket.WebSocketConnection;

/* loaded from: classes5.dex */
public interface WebSocketFactory {
    WebSocketConnection createUnidentifiedWebSocket();

    WebSocketConnection createWebSocket();
}
