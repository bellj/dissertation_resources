package org.whispersystems.signalservice.api;

import io.reactivex.rxjava3.core.Single;
import j$.util.Optional;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.api.crypto.AttachmentCipherInputStream;
import org.whispersystems.signalservice.api.crypto.ProfileCipherInputStream;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.messages.SignalServiceStickerManifest;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.MissingConfigurationException;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.push.IdentityCheckRequest;
import org.whispersystems.signalservice.internal.push.IdentityCheckResponse;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.SignalServiceEnvelopeEntity;
import org.whispersystems.signalservice.internal.push.SignalServiceMessagesResult;
import org.whispersystems.signalservice.internal.sticker.StickerProtos;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers;
import org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;

/* loaded from: classes5.dex */
public class SignalServiceMessageReceiver {
    private final PushServiceSocket socket;

    /* loaded from: classes5.dex */
    public interface MessageReceivedCallback {
        void onMessage(SignalServiceEnvelope signalServiceEnvelope);
    }

    /* loaded from: classes5.dex */
    public static class NullMessageReceivedCallback implements MessageReceivedCallback {
        @Override // org.whispersystems.signalservice.api.SignalServiceMessageReceiver.MessageReceivedCallback
        public void onMessage(SignalServiceEnvelope signalServiceEnvelope) {
        }
    }

    public SignalServiceMessageReceiver(SignalServiceConfiguration signalServiceConfiguration, CredentialsProvider credentialsProvider, String str, ClientZkProfileOperations clientZkProfileOperations, boolean z) {
        this.socket = new PushServiceSocket(signalServiceConfiguration, credentialsProvider, str, clientZkProfileOperations, z);
    }

    public InputStream retrieveAttachment(SignalServiceAttachmentPointer signalServiceAttachmentPointer, File file, long j) throws IOException, InvalidMessageException, MissingConfigurationException {
        return retrieveAttachment(signalServiceAttachmentPointer, file, j, null);
    }

    public ListenableFuture<ProfileAndCredential> retrieveProfile(SignalServiceAddress signalServiceAddress, Optional<ProfileKey> optional, Optional<UnidentifiedAccess> optional2, SignalServiceProfile.RequestType requestType, Locale locale) {
        ServiceId serviceId = signalServiceAddress.getServiceId();
        if (!optional.isPresent()) {
            return FutureTransformers.map(this.socket.retrieveProfile(signalServiceAddress, optional2, locale), new FutureTransformers.Transformer() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageReceiver$$ExternalSyntheticLambda1
                @Override // org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers.Transformer
                public final Object transform(Object obj) {
                    return SignalServiceMessageReceiver.lambda$retrieveProfile$1((SignalServiceProfile) obj);
                }
            });
        }
        if (requestType == SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL) {
            return this.socket.retrieveVersionedProfileAndCredential(serviceId.uuid(), optional.get(), optional2, locale);
        }
        return FutureTransformers.map(this.socket.retrieveVersionedProfile(serviceId.uuid(), optional.get(), optional2, locale), new FutureTransformers.Transformer() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageReceiver$$ExternalSyntheticLambda0
            @Override // org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers.Transformer
            public final Object transform(Object obj) {
                return SignalServiceMessageReceiver.lambda$retrieveProfile$0((SignalServiceProfile) obj);
            }
        });
    }

    public static /* synthetic */ ProfileAndCredential lambda$retrieveProfile$0(SignalServiceProfile signalServiceProfile) throws Exception {
        return new ProfileAndCredential(signalServiceProfile, SignalServiceProfile.RequestType.PROFILE, Optional.empty());
    }

    public static /* synthetic */ ProfileAndCredential lambda$retrieveProfile$1(SignalServiceProfile signalServiceProfile) throws Exception {
        return new ProfileAndCredential(signalServiceProfile, SignalServiceProfile.RequestType.PROFILE, Optional.empty());
    }

    public SignalServiceProfile retrieveProfileByUsername(String str, Optional<UnidentifiedAccess> optional, Locale locale) throws IOException {
        return this.socket.retrieveProfileByUsername(str, optional, locale);
    }

    public InputStream retrieveProfileAvatar(String str, File file, ProfileKey profileKey, long j) throws IOException {
        this.socket.retrieveProfileAvatar(str, file, j);
        return new ProfileCipherInputStream(new FileInputStream(file), profileKey);
    }

    public FileInputStream retrieveGroupsV2ProfileAvatar(String str, File file, long j) throws IOException {
        this.socket.retrieveProfileAvatar(str, file, j);
        return new FileInputStream(file);
    }

    public Single<ServiceResponse<IdentityCheckResponse>> performIdentityCheck(IdentityCheckRequest identityCheckRequest, Optional<UnidentifiedAccess> optional, ResponseMapper<IdentityCheckResponse> responseMapper) {
        return this.socket.performIdentityCheck(identityCheckRequest, optional, responseMapper);
    }

    public InputStream retrieveAttachment(SignalServiceAttachmentPointer signalServiceAttachmentPointer, File file, long j, SignalServiceAttachment.ProgressListener progressListener) throws IOException, InvalidMessageException, MissingConfigurationException {
        if (signalServiceAttachmentPointer.getDigest().isPresent()) {
            this.socket.retrieveAttachment(signalServiceAttachmentPointer.getCdnNumber(), signalServiceAttachmentPointer.getRemoteId(), file, j, progressListener);
            return AttachmentCipherInputStream.createForAttachment(file, (long) signalServiceAttachmentPointer.getSize().orElse(0).intValue(), signalServiceAttachmentPointer.getKey(), signalServiceAttachmentPointer.getDigest().get());
        }
        throw new InvalidMessageException("No attachment digest!");
    }

    public InputStream retrieveSticker(byte[] bArr, byte[] bArr2, int i) throws IOException, InvalidMessageException {
        return AttachmentCipherInputStream.createForStickerData(this.socket.retrieveSticker(bArr, i), bArr2);
    }

    public SignalServiceStickerManifest retrieveStickerManifest(byte[] bArr, byte[] bArr2) throws IOException, InvalidMessageException {
        InputStream createForStickerData = AttachmentCipherInputStream.createForStickerData(this.socket.retrieveStickerManifest(bArr), bArr2);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Util.copy(createForStickerData, byteArrayOutputStream);
        StickerProtos.Pack parseFrom = StickerProtos.Pack.parseFrom(byteArrayOutputStream.toByteArray());
        ArrayList arrayList = new ArrayList(parseFrom.getStickersCount());
        SignalServiceStickerManifest.StickerInfo stickerInfo = parseFrom.hasCover() ? new SignalServiceStickerManifest.StickerInfo(parseFrom.getCover().getId(), parseFrom.getCover().getEmoji(), parseFrom.getCover().getContentType()) : null;
        for (StickerProtos.Pack.Sticker sticker : parseFrom.getStickersList()) {
            arrayList.add(new SignalServiceStickerManifest.StickerInfo(sticker.getId(), sticker.getEmoji(), sticker.getContentType()));
        }
        return new SignalServiceStickerManifest(parseFrom.getTitle(), parseFrom.getAuthor(), stickerInfo, arrayList);
    }

    public List<SignalServiceEnvelope> retrieveMessages() throws IOException {
        return retrieveMessages(new NullMessageReceivedCallback());
    }

    public List<SignalServiceEnvelope> retrieveMessages(MessageReceivedCallback messageReceivedCallback) throws IOException {
        SignalServiceEnvelope signalServiceEnvelope;
        LinkedList linkedList = new LinkedList();
        SignalServiceMessagesResult messages = this.socket.getMessages();
        for (SignalServiceEnvelopeEntity signalServiceEnvelopeEntity : messages.getEnvelopes()) {
            if (!signalServiceEnvelopeEntity.hasSource() || signalServiceEnvelopeEntity.getSourceDevice() <= 0) {
                signalServiceEnvelope = new SignalServiceEnvelope(signalServiceEnvelopeEntity.getType(), signalServiceEnvelopeEntity.getTimestamp(), signalServiceEnvelopeEntity.getMessage(), signalServiceEnvelopeEntity.getContent(), signalServiceEnvelopeEntity.getServerTimestamp(), messages.getServerDeliveredTimestamp(), signalServiceEnvelopeEntity.getServerUuid(), signalServiceEnvelopeEntity.getDestinationUuid());
            } else {
                signalServiceEnvelope = new SignalServiceEnvelope(signalServiceEnvelopeEntity.getType(), Optional.of(new SignalServiceAddress(ServiceId.parseOrThrow(signalServiceEnvelopeEntity.getSourceUuid()), signalServiceEnvelopeEntity.getSourceE164())), signalServiceEnvelopeEntity.getSourceDevice(), signalServiceEnvelopeEntity.getTimestamp(), signalServiceEnvelopeEntity.getMessage(), signalServiceEnvelopeEntity.getContent(), signalServiceEnvelopeEntity.getServerTimestamp(), messages.getServerDeliveredTimestamp(), signalServiceEnvelopeEntity.getServerUuid(), signalServiceEnvelopeEntity.getDestinationUuid());
            }
            messageReceivedCallback.onMessage(signalServiceEnvelope);
            linkedList.add(signalServiceEnvelope);
            if (signalServiceEnvelope.hasServerGuid()) {
                this.socket.acknowledgeMessage(signalServiceEnvelope.getServerGuid());
            } else {
                this.socket.acknowledgeMessage(signalServiceEnvelopeEntity.getSourceE164(), signalServiceEnvelopeEntity.getTimestamp());
            }
        }
        return linkedList;
    }

    public void setSoTimeoutMillis(long j) {
        this.socket.setSoTimeoutMillis(j);
    }
}
