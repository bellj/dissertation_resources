package org.whispersystems.signalservice.api.storage;

import java.util.Arrays;
import java.util.Objects;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.internal.storage.protos.ManifestRecord;

/* loaded from: classes5.dex */
public class StorageId {
    private final byte[] raw;
    private final int type;

    public static StorageId forContact(byte[] bArr) {
        return new StorageId(1, (byte[]) Preconditions.checkNotNull(bArr));
    }

    public static StorageId forGroupV1(byte[] bArr) {
        return new StorageId(2, (byte[]) Preconditions.checkNotNull(bArr));
    }

    public static StorageId forGroupV2(byte[] bArr) {
        return new StorageId(3, (byte[]) Preconditions.checkNotNull(bArr));
    }

    public static StorageId forStoryDistributionList(byte[] bArr) {
        return new StorageId(5, (byte[]) Preconditions.checkNotNull(bArr));
    }

    public static StorageId forAccount(byte[] bArr) {
        return new StorageId(4, (byte[]) Preconditions.checkNotNull(bArr));
    }

    public static StorageId forType(byte[] bArr, int i) {
        return new StorageId(i, bArr);
    }

    public boolean isUnknown() {
        return !isKnownType(this.type);
    }

    private StorageId(int i, byte[] bArr) {
        this.type = i;
        this.raw = bArr;
    }

    public int getType() {
        return this.type;
    }

    public byte[] getRaw() {
        return this.raw;
    }

    public StorageId withNewBytes(byte[] bArr) {
        return new StorageId(this.type, bArr);
    }

    public static boolean isKnownType(int i) {
        ManifestRecord.Identifier.Type[] values = ManifestRecord.Identifier.Type.values();
        for (ManifestRecord.Identifier.Type type : values) {
            if (type != ManifestRecord.Identifier.Type.UNRECOGNIZED && type.getNumber() == i) {
                return true;
            }
        }
        return false;
    }

    public static int largestKnownType() {
        ManifestRecord.Identifier.Type[] values = ManifestRecord.Identifier.Type.values();
        int i = 0;
        for (ManifestRecord.Identifier.Type type : values) {
            if (type != ManifestRecord.Identifier.Type.UNRECOGNIZED) {
                i = Math.max(type.getNumber(), i);
            }
        }
        return i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        StorageId storageId = (StorageId) obj;
        if (this.type != storageId.type || !Arrays.equals(this.raw, storageId.raw)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Objects.hash(Integer.valueOf(this.type)) * 31) + Arrays.hashCode(this.raw);
    }
}
