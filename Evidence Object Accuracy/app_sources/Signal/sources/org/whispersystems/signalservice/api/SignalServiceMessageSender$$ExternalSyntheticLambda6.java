package org.whispersystems.signalservice.api;

import j$.util.function.Function;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class SignalServiceMessageSender$$ExternalSyntheticLambda6 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((SignalServiceAddress) obj).getIdentifier();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
