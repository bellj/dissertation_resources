package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec;

/* loaded from: classes5.dex */
public class SignalServiceAttachmentStream extends SignalServiceAttachment implements Closeable {
    private final Optional<String> blurHash;
    private final boolean borderless;
    private final CancelationSignal cancelationSignal;
    private final Optional<String> caption;
    private final Optional<String> fileName;
    private final boolean gif;
    private final int height;
    private final InputStream inputStream;
    private final long length;
    private final SignalServiceAttachment.ProgressListener listener;
    private final Optional<byte[]> preview;
    private final Optional<ResumableUploadSpec> resumableUploadSpec;
    private final long uploadTimestamp;
    private final boolean voiceNote;
    private final int width;

    @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment
    public boolean isPointer() {
        return false;
    }

    @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment
    public boolean isStream() {
        return true;
    }

    public SignalServiceAttachmentStream(InputStream inputStream, String str, long j, Optional<String> optional, boolean z, boolean z2, boolean z3, SignalServiceAttachment.ProgressListener progressListener, CancelationSignal cancelationSignal) {
        this(inputStream, str, j, optional, z, z2, z3, Optional.empty(), 0, 0, System.currentTimeMillis(), Optional.empty(), Optional.empty(), progressListener, cancelationSignal, Optional.empty());
    }

    public SignalServiceAttachmentStream(InputStream inputStream, String str, long j, Optional<String> optional, boolean z, boolean z2, boolean z3, Optional<byte[]> optional2, int i, int i2, long j2, Optional<String> optional3, Optional<String> optional4, SignalServiceAttachment.ProgressListener progressListener, CancelationSignal cancelationSignal, Optional<ResumableUploadSpec> optional5) {
        super(str);
        this.inputStream = inputStream;
        this.length = j;
        this.fileName = optional;
        this.listener = progressListener;
        this.voiceNote = z;
        this.borderless = z2;
        this.gif = z3;
        this.preview = optional2;
        this.width = i;
        this.height = i2;
        this.uploadTimestamp = j2;
        this.caption = optional3;
        this.blurHash = optional4;
        this.cancelationSignal = cancelationSignal;
        this.resumableUploadSpec = optional5;
    }

    public InputStream getInputStream() {
        return this.inputStream;
    }

    public long getLength() {
        return this.length;
    }

    public Optional<String> getFileName() {
        return this.fileName;
    }

    public SignalServiceAttachment.ProgressListener getListener() {
        return this.listener;
    }

    public CancelationSignal getCancelationSignal() {
        return this.cancelationSignal;
    }

    public Optional<byte[]> getPreview() {
        return this.preview;
    }

    public boolean getVoiceNote() {
        return this.voiceNote;
    }

    public boolean isBorderless() {
        return this.borderless;
    }

    public boolean isGif() {
        return this.gif;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Optional<String> getCaption() {
        return this.caption;
    }

    public Optional<String> getBlurHash() {
        return this.blurHash;
    }

    public long getUploadTimestamp() {
        return this.uploadTimestamp;
    }

    public Optional<ResumableUploadSpec> getResumableUploadSpec() {
        return this.resumableUploadSpec;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.inputStream.close();
    }
}
