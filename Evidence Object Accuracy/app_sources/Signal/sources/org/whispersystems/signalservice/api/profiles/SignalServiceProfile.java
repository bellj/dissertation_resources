package org.whispersystems.signalservice.api.profiles;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.math.BigDecimal;
import java.util.List;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredentialResponse;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes.dex */
public class SignalServiceProfile {
    private static final String TAG;
    @JsonProperty
    private String about;
    @JsonProperty
    private String aboutEmoji;
    @JsonProperty
    private String avatar;
    @JsonProperty
    private List<Badge> badges;
    @JsonProperty
    private Capabilities capabilities;
    @JsonProperty
    private byte[] credential;
    @JsonProperty
    private String identityKey;
    @JsonProperty
    private String name;
    @JsonProperty
    private byte[] paymentAddress;
    @JsonIgnore
    private RequestType requestType;
    @JsonProperty
    private String unidentifiedAccess;
    @JsonProperty
    private boolean unrestrictedUnidentifiedAccess;
    @JsonProperty
    @JsonDeserialize(using = JsonUtil.ServiceIdDeserializer.class)
    @JsonSerialize(using = JsonUtil.ServiceIdSerializer.class)
    private ServiceId uuid;

    /* loaded from: classes5.dex */
    public enum RequestType {
        PROFILE,
        PROFILE_AND_CREDENTIAL
    }

    public String getIdentityKey() {
        return this.identityKey;
    }

    public String getName() {
        return this.name;
    }

    public String getAbout() {
        return this.about;
    }

    public String getAboutEmoji() {
        return this.aboutEmoji;
    }

    public byte[] getPaymentAddress() {
        return this.paymentAddress;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public String getUnidentifiedAccess() {
        return this.unidentifiedAccess;
    }

    public boolean isUnrestrictedUnidentifiedAccess() {
        return this.unrestrictedUnidentifiedAccess;
    }

    public Capabilities getCapabilities() {
        return this.capabilities;
    }

    public List<Badge> getBadges() {
        return this.badges;
    }

    public ServiceId getServiceId() {
        return this.uuid;
    }

    public RequestType getRequestType() {
        return this.requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    /* loaded from: classes.dex */
    public static class Badge {
        @JsonProperty
        private String category;
        @JsonProperty
        private String description;
        @JsonProperty
        private long duration;
        @JsonProperty
        private BigDecimal expiration;
        @JsonProperty
        private String id;
        @JsonProperty
        private String name;
        @JsonProperty
        private List<String> sprites6;
        @JsonProperty
        private boolean visible;

        public String getId() {
            return this.id;
        }

        public String getCategory() {
            return this.category;
        }

        public String getName() {
            return this.name;
        }

        public String getDescription() {
            return this.description;
        }

        public List<String> getSprites6() {
            return this.sprites6;
        }

        public BigDecimal getExpiration() {
            return this.expiration;
        }

        public boolean isVisible() {
            return this.visible;
        }

        public long getDuration() {
            return this.duration;
        }
    }

    /* loaded from: classes.dex */
    public static class Capabilities {
        @JsonProperty
        private boolean announcementGroup;
        @JsonProperty
        private boolean changeNumber;
        @JsonProperty
        private boolean giftBadges;
        @JsonProperty("gv1-migration")
        private boolean gv1Migration;
        @JsonProperty
        private boolean senderKey;
        @JsonProperty
        private boolean storage;
        @JsonProperty
        private boolean stories;

        @JsonCreator
        public Capabilities() {
        }

        public Capabilities(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            this.storage = z;
            this.gv1Migration = z2;
            this.senderKey = z3;
            this.announcementGroup = z4;
            this.changeNumber = z5;
            this.stories = z6;
            this.giftBadges = z7;
        }

        public boolean isStorage() {
            return this.storage;
        }

        public boolean isGv1Migration() {
            return this.gv1Migration;
        }

        public boolean isSenderKey() {
            return this.senderKey;
        }

        public boolean isAnnouncementGroup() {
            return this.announcementGroup;
        }

        public boolean isChangeNumber() {
            return this.changeNumber;
        }

        public boolean isStories() {
            return this.stories;
        }

        public boolean isGiftBadges() {
            return this.giftBadges;
        }
    }

    public ExpiringProfileKeyCredentialResponse getExpiringProfileKeyCredentialResponse() {
        byte[] bArr = this.credential;
        if (bArr == null) {
            return null;
        }
        try {
            return new ExpiringProfileKeyCredentialResponse(bArr);
        } catch (InvalidInputException e) {
            Log.w(TAG, e);
            return null;
        }
    }
}
