package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;

/* loaded from: classes5.dex */
public final class SignalServiceGroupContext {
    private final Optional<SignalServiceGroup> groupV1;
    private final Optional<SignalServiceGroupV2> groupV2;

    private SignalServiceGroupContext(SignalServiceGroup signalServiceGroup) {
        this.groupV1 = Optional.of(signalServiceGroup);
        this.groupV2 = Optional.empty();
    }

    private SignalServiceGroupContext(SignalServiceGroupV2 signalServiceGroupV2) {
        this.groupV1 = Optional.empty();
        this.groupV2 = Optional.of(signalServiceGroupV2);
    }

    public Optional<SignalServiceGroup> getGroupV1() {
        return this.groupV1;
    }

    public Optional<SignalServiceGroupV2> getGroupV2() {
        return this.groupV2;
    }

    public static Optional<SignalServiceGroupContext> createOptional(SignalServiceGroup signalServiceGroup, SignalServiceGroupV2 signalServiceGroupV2) throws InvalidMessageException {
        return Optional.ofNullable(create(signalServiceGroup, signalServiceGroupV2));
    }

    public static SignalServiceGroupContext create(SignalServiceGroup signalServiceGroup, SignalServiceGroupV2 signalServiceGroupV2) throws InvalidMessageException {
        if (signalServiceGroup == null && signalServiceGroupV2 == null) {
            return null;
        }
        if (signalServiceGroup != null && signalServiceGroupV2 != null) {
            throw new InvalidMessageException("Message cannot have both V1 and V2 group contexts.");
        } else if (signalServiceGroup != null) {
            return new SignalServiceGroupContext(signalServiceGroup);
        } else {
            return new SignalServiceGroupContext(signalServiceGroupV2);
        }
    }

    public SignalServiceGroup.Type getGroupV1Type() {
        if (this.groupV1.isPresent()) {
            return this.groupV1.get().getType();
        }
        return null;
    }
}
