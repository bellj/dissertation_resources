package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class RemoteAttestationResponseExpiredException extends NonSuccessfulResponseCodeException {
    public RemoteAttestationResponseExpiredException(String str) {
        super(409, str);
    }
}
