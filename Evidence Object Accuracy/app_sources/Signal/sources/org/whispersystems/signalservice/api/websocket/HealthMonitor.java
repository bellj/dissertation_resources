package org.whispersystems.signalservice.api.websocket;

/* loaded from: classes5.dex */
public interface HealthMonitor {
    void onKeepAliveResponse(long j, boolean z);

    void onMessageError(int i, boolean z);
}
