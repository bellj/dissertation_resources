package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public final class SignalServiceMetadata {
    private final String destinationUuid;
    private final Optional<byte[]> groupId;
    private final boolean needsReceipt;
    private final SignalServiceAddress sender;
    private final int senderDevice;
    private final long serverDeliveredTimestamp;
    private final String serverGuid;
    private final long serverReceivedTimestamp;
    private final long timestamp;

    public SignalServiceMetadata(SignalServiceAddress signalServiceAddress, int i, long j, long j2, long j3, boolean z, String str, Optional<byte[]> optional, String str2) {
        this.sender = signalServiceAddress;
        this.senderDevice = i;
        this.timestamp = j;
        this.serverReceivedTimestamp = j2;
        this.serverDeliveredTimestamp = j3;
        this.needsReceipt = z;
        this.serverGuid = str;
        this.groupId = optional;
        this.destinationUuid = str2 == null ? "" : str2;
    }

    public SignalServiceAddress getSender() {
        return this.sender;
    }

    public int getSenderDevice() {
        return this.senderDevice;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public long getServerReceivedTimestamp() {
        return this.serverReceivedTimestamp;
    }

    public long getServerDeliveredTimestamp() {
        return this.serverDeliveredTimestamp;
    }

    public boolean isNeedsReceipt() {
        return this.needsReceipt;
    }

    public String getServerGuid() {
        return this.serverGuid;
    }

    public Optional<byte[]> getGroupId() {
        return this.groupId;
    }

    public String getDestinationUuid() {
        return this.destinationUuid;
    }
}
