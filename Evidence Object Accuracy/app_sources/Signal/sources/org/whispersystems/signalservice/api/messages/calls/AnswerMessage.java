package org.whispersystems.signalservice.api.messages.calls;

/* loaded from: classes5.dex */
public class AnswerMessage {
    private final long id;
    private final byte[] opaque;
    private final String sdp;

    public AnswerMessage(long j, String str, byte[] bArr) {
        this.id = j;
        this.sdp = str;
        this.opaque = bArr;
    }

    public String getSdp() {
        return this.sdp;
    }

    public long getId() {
        return this.id;
    }

    public byte[] getOpaque() {
        return this.opaque;
    }
}
