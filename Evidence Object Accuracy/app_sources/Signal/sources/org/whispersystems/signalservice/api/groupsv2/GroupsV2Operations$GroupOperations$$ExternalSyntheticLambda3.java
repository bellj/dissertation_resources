package org.whispersystems.signalservice.api.groupsv2;

import j$.util.function.Predicate;
import java.util.Set;
import java.util.UUID;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda3 implements Predicate {
    public final /* synthetic */ Set f$0;

    public /* synthetic */ GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda3(Set set) {
        this.f$0 = set;
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate and(Predicate predicate) {
        return Predicate.CC.$default$and(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate negate() {
        return Predicate.CC.$default$negate(this);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate or(Predicate predicate) {
        return Predicate.CC.$default$or(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.contains((UUID) obj);
    }
}
