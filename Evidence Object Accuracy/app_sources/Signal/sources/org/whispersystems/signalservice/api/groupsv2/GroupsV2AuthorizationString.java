package org.whispersystems.signalservice.api.groupsv2;

import okhttp3.Credentials;
import org.signal.libsignal.zkgroup.auth.AuthCredentialPresentation;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.whispersystems.signalservice.internal.util.Hex;

/* loaded from: classes5.dex */
public final class GroupsV2AuthorizationString {
    private final String authString;

    public GroupsV2AuthorizationString(GroupSecretParams groupSecretParams, AuthCredentialPresentation authCredentialPresentation) {
        this.authString = Credentials.basic(Hex.toStringCondensed(groupSecretParams.getPublicParams().serialize()), Hex.toStringCondensed(authCredentialPresentation.serialize()));
    }

    public String toString() {
        return this.authString;
    }
}
