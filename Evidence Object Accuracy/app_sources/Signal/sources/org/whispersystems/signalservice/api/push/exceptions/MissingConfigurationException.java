package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public final class MissingConfigurationException extends Exception {
    public MissingConfigurationException(String str) {
        super(str);
    }
}
