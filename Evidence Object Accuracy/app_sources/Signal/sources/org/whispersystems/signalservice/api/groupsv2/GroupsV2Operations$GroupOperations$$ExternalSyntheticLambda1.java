package org.whispersystems.signalservice.api.groupsv2;

import j$.util.function.Function;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda1 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((DecryptedBannedMember) obj).getUuid();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
