package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class NotFoundException extends NonSuccessfulResponseCodeException {
    public NotFoundException(String str) {
        super(404, str);
    }
}
