package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class SignalServiceAttachmentPointer extends SignalServiceAttachment {
    private final Optional<String> blurHash;
    private final boolean borderless;
    private final Optional<String> caption;
    private final int cdnNumber;
    private final Optional<byte[]> digest;
    private final Optional<String> fileName;
    private final boolean gif;
    private final int height;
    private final byte[] key;
    private final Optional<byte[]> preview;
    private final SignalServiceAttachmentRemoteId remoteId;
    private final Optional<Integer> size;
    private final long uploadTimestamp;
    private final boolean voiceNote;
    private final int width;

    @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment
    public boolean isPointer() {
        return true;
    }

    @Override // org.whispersystems.signalservice.api.messages.SignalServiceAttachment
    public boolean isStream() {
        return false;
    }

    public SignalServiceAttachmentPointer(int i, SignalServiceAttachmentRemoteId signalServiceAttachmentRemoteId, String str, byte[] bArr, Optional<Integer> optional, Optional<byte[]> optional2, int i2, int i3, Optional<byte[]> optional3, Optional<String> optional4, boolean z, boolean z2, boolean z3, Optional<String> optional5, Optional<String> optional6, long j) {
        super(str);
        this.cdnNumber = i;
        this.remoteId = signalServiceAttachmentRemoteId;
        this.key = bArr;
        this.size = optional;
        this.preview = optional2;
        this.width = i2;
        this.height = i3;
        this.digest = optional3;
        this.fileName = optional4;
        this.voiceNote = z;
        this.borderless = z2;
        this.caption = optional5;
        this.blurHash = optional6;
        this.uploadTimestamp = j;
        this.gif = z3;
    }

    public int getCdnNumber() {
        return this.cdnNumber;
    }

    public SignalServiceAttachmentRemoteId getRemoteId() {
        return this.remoteId;
    }

    public byte[] getKey() {
        return this.key;
    }

    public Optional<Integer> getSize() {
        return this.size;
    }

    public Optional<String> getFileName() {
        return this.fileName;
    }

    public Optional<byte[]> getPreview() {
        return this.preview;
    }

    public Optional<byte[]> getDigest() {
        return this.digest;
    }

    public boolean getVoiceNote() {
        return this.voiceNote;
    }

    public boolean isBorderless() {
        return this.borderless;
    }

    public boolean isGif() {
        return this.gif;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Optional<String> getCaption() {
        return this.caption;
    }

    public Optional<String> getBlurHash() {
        return this.blurHash;
    }

    public long getUploadTimestamp() {
        return this.uploadTimestamp;
    }
}
