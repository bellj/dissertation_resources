package org.whispersystems.signalservice.api.util;

import java.math.BigInteger;
import org.signal.libsignal.protocol.util.ByteUtil;

/* loaded from: classes5.dex */
public final class Uint64Util {
    private static final BigInteger MAX_UINT64 = uint64ToBigInteger(-1);

    private Uint64Util() {
    }

    public static BigInteger uint64ToBigInteger(long j) {
        if (j < 0) {
            return new BigInteger(1, ByteUtil.longToByteArray(j));
        }
        return BigInteger.valueOf(j);
    }

    public static long bigIntegerToUInt64(BigInteger bigInteger) throws Uint64RangeException {
        if (bigInteger.signum() < 0) {
            throw new Uint64RangeException("BigInteger out of uint64 range (negative)");
        } else if (bigInteger.compareTo(MAX_UINT64) <= 0) {
            return bigInteger.longValue();
        } else {
            throw new Uint64RangeException("BigInteger out of uint64 range (> MAX)");
        }
    }
}
