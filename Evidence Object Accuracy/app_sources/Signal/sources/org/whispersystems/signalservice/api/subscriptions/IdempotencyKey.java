package org.whispersystems.signalservice.api.subscriptions;

import java.security.SecureRandom;
import java.util.Arrays;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.util.Base64UrlSafe;

/* loaded from: classes5.dex */
public final class IdempotencyKey {
    private static final int SIZE;
    private final byte[] bytes;

    private IdempotencyKey(byte[] bArr) {
        this.bytes = bArr;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public String serialize() {
        return Base64UrlSafe.encodeBytes(this.bytes);
    }

    public static IdempotencyKey fromBytes(byte[] bArr) {
        Preconditions.checkArgument(bArr.length == 16);
        return new IdempotencyKey(bArr);
    }

    public static IdempotencyKey generate() {
        byte[] bArr = new byte[16];
        new SecureRandom().nextBytes(bArr);
        return new IdempotencyKey(bArr);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || IdempotencyKey.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.bytes, ((IdempotencyKey) obj).bytes);
    }

    public int hashCode() {
        return Arrays.hashCode(this.bytes);
    }
}
