package org.whispersystems.signalservice.api.crypto;

import org.signal.libsignal.protocol.IdentityKey;

/* loaded from: classes5.dex */
public class UntrustedIdentityException extends Exception {
    private final String identifier;
    private final IdentityKey identityKey;

    public UntrustedIdentityException(String str, String str2, IdentityKey identityKey) {
        super(str);
        this.identifier = str2;
        this.identityKey = identityKey;
    }

    public UntrustedIdentityException(UntrustedIdentityException untrustedIdentityException) {
        this(untrustedIdentityException.getMessage(), untrustedIdentityException.getIdentifier(), untrustedIdentityException.getIdentityKey());
    }

    public IdentityKey getIdentityKey() {
        return this.identityKey;
    }

    public String getIdentifier() {
        return this.identifier;
    }
}
