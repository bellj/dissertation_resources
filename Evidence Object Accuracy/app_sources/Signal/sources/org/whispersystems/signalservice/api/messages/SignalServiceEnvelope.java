package org.whispersystems.signalservice.api.messages;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import java.io.IOException;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProto;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public class SignalServiceEnvelope {
    private static final String TAG;
    private final SignalServiceProtos.Envelope envelope;
    private final long serverDeliveredTimestamp;

    public SignalServiceEnvelope(String str, long j) throws IOException {
        this(Base64.decode(str), j);
    }

    public SignalServiceEnvelope(byte[] bArr, long j) throws IOException {
        this.envelope = SignalServiceProtos.Envelope.parseFrom(bArr);
        this.serverDeliveredTimestamp = j;
    }

    public SignalServiceEnvelope(int i, Optional<SignalServiceAddress> optional, int i2, long j, byte[] bArr, byte[] bArr2, long j2, long j3, String str, String str2) {
        SignalServiceProtos.Envelope.Builder destinationUuid = SignalServiceProtos.Envelope.newBuilder().setType(SignalServiceProtos.Envelope.Type.valueOf(i)).setSourceDevice(i2).setTimestamp(j).setServerTimestamp(j2).setDestinationUuid(str2);
        if (optional.isPresent()) {
            destinationUuid.setSourceUuid(optional.get().getServiceId().toString());
            if (optional.get().getNumber().isPresent()) {
                destinationUuid.setSourceE164(optional.get().getNumber().get());
            }
        }
        if (str != null) {
            destinationUuid.setServerGuid(str);
        }
        if (bArr != null) {
            destinationUuid.setLegacyMessage(ByteString.copyFrom(bArr));
        }
        if (bArr2 != null) {
            destinationUuid.setContent(ByteString.copyFrom(bArr2));
        }
        this.envelope = destinationUuid.build();
        this.serverDeliveredTimestamp = j3;
    }

    public SignalServiceEnvelope(int i, long j, byte[] bArr, byte[] bArr2, long j2, long j3, String str, String str2) {
        SignalServiceProtos.Envelope.Builder destinationUuid = SignalServiceProtos.Envelope.newBuilder().setType(SignalServiceProtos.Envelope.Type.valueOf(i)).setTimestamp(j).setServerTimestamp(j2).setDestinationUuid(str2);
        if (str != null) {
            destinationUuid.setServerGuid(str);
        }
        if (bArr != null) {
            destinationUuid.setLegacyMessage(ByteString.copyFrom(bArr));
        }
        if (bArr2 != null) {
            destinationUuid.setContent(ByteString.copyFrom(bArr2));
        }
        this.envelope = destinationUuid.build();
        this.serverDeliveredTimestamp = j3;
    }

    public SignalServiceEnvelope withoutE164() {
        return deserialize(serializeToProto().clearSourceE164().build().toByteArray());
    }

    public String getServerGuid() {
        return this.envelope.getServerGuid();
    }

    public boolean hasServerGuid() {
        return this.envelope.hasServerGuid();
    }

    public boolean hasSourceUuid() {
        return this.envelope.hasSourceUuid();
    }

    public Optional<String> getSourceE164() {
        return Optional.ofNullable(this.envelope.getSourceE164());
    }

    public Optional<String> getSourceUuid() {
        return Optional.ofNullable(this.envelope.getSourceUuid());
    }

    public String getSourceIdentifier() {
        return (String) OptionalUtil.or(getSourceUuid(), getSourceE164()).orElse(null);
    }

    public boolean hasSourceDevice() {
        return this.envelope.hasSourceDevice();
    }

    public boolean hasSourceE164() {
        return this.envelope.hasSourceE164();
    }

    public int getSourceDevice() {
        return this.envelope.getSourceDevice();
    }

    public SignalServiceAddress getSourceAddress() {
        return new SignalServiceAddress(ACI.parseOrNull(this.envelope.getSourceUuid()), this.envelope.getSourceE164());
    }

    public int getType() {
        return this.envelope.getType().getNumber();
    }

    public long getTimestamp() {
        return this.envelope.getTimestamp();
    }

    public long getServerReceivedTimestamp() {
        return this.envelope.getServerTimestamp();
    }

    public long getServerDeliveredTimestamp() {
        return this.serverDeliveredTimestamp;
    }

    public boolean hasLegacyMessage() {
        return this.envelope.hasLegacyMessage();
    }

    public byte[] getLegacyMessage() {
        return this.envelope.getLegacyMessage().toByteArray();
    }

    public boolean hasContent() {
        return this.envelope.hasContent();
    }

    public byte[] getContent() {
        return this.envelope.getContent().toByteArray();
    }

    public boolean isSignalMessage() {
        return this.envelope.getType().getNumber() == 1;
    }

    public boolean isPreKeySignalMessage() {
        return this.envelope.getType().getNumber() == 3;
    }

    public boolean isReceipt() {
        return this.envelope.getType().getNumber() == 5;
    }

    public boolean isUnidentifiedSender() {
        return this.envelope.getType().getNumber() == 6;
    }

    public boolean isPlaintextContent() {
        return this.envelope.getType().getNumber() == 8;
    }

    public boolean hasDestinationUuid() {
        return this.envelope.hasDestinationUuid() && UuidUtil.isUuid(this.envelope.getDestinationUuid());
    }

    public String getDestinationUuid() {
        return this.envelope.getDestinationUuid();
    }

    private SignalServiceEnvelopeProto.Builder serializeToProto() {
        SignalServiceEnvelopeProto.Builder serverDeliveredTimestamp = SignalServiceEnvelopeProto.newBuilder().setType(getType()).setDeviceId(getSourceDevice()).setTimestamp(getTimestamp()).setServerReceivedTimestamp(getServerReceivedTimestamp()).setServerDeliveredTimestamp(getServerDeliveredTimestamp());
        if (getSourceUuid().isPresent()) {
            serverDeliveredTimestamp.setSourceUuid(getSourceUuid().get());
        }
        if (getSourceE164().isPresent()) {
            serverDeliveredTimestamp.setSourceE164(getSourceE164().get());
        }
        if (hasLegacyMessage()) {
            serverDeliveredTimestamp.setLegacyMessage(ByteString.copyFrom(getLegacyMessage()));
        }
        if (hasContent()) {
            serverDeliveredTimestamp.setContent(ByteString.copyFrom(getContent()));
        }
        if (hasServerGuid()) {
            serverDeliveredTimestamp.setServerGuid(getServerGuid());
        }
        if (hasDestinationUuid()) {
            serverDeliveredTimestamp.setDestinationUuid(getDestinationUuid().toString());
        }
        return serverDeliveredTimestamp;
    }

    public byte[] serialize() {
        return serializeToProto().build().toByteArray();
    }

    public static SignalServiceEnvelope deserialize(byte[] bArr) {
        SignalServiceEnvelopeProto signalServiceEnvelopeProto;
        byte[] bArr2 = null;
        try {
            signalServiceEnvelopeProto = SignalServiceEnvelopeProto.parseFrom(bArr);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            signalServiceEnvelopeProto = null;
        }
        int type = signalServiceEnvelopeProto.getType();
        Optional<SignalServiceAddress> fromRaw = SignalServiceAddress.fromRaw(signalServiceEnvelopeProto.getSourceUuid(), signalServiceEnvelopeProto.getSourceE164());
        int deviceId = signalServiceEnvelopeProto.getDeviceId();
        long timestamp = signalServiceEnvelopeProto.getTimestamp();
        byte[] byteArray = signalServiceEnvelopeProto.hasLegacyMessage() ? signalServiceEnvelopeProto.getLegacyMessage().toByteArray() : null;
        if (signalServiceEnvelopeProto.hasContent()) {
            bArr2 = signalServiceEnvelopeProto.getContent().toByteArray();
        }
        return new SignalServiceEnvelope(type, fromRaw, deviceId, timestamp, byteArray, bArr2, signalServiceEnvelopeProto.getServerReceivedTimestamp(), signalServiceEnvelopeProto.getServerDeliveredTimestamp(), signalServiceEnvelopeProto.getServerGuid(), signalServiceEnvelopeProto.getDestinationUuid());
    }
}
