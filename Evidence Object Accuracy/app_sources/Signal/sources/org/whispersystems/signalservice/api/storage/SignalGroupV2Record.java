package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.whispersystems.signalservice.api.util.ProtoUtil;
import org.whispersystems.signalservice.internal.storage.protos.GroupV2Record;

/* loaded from: classes5.dex */
public final class SignalGroupV2Record implements SignalRecord {
    private static final String TAG;
    private final boolean hasUnknownFields;
    private final StorageId id;
    private final byte[] masterKey;
    private final GroupV2Record proto;

    public SignalGroupV2Record(StorageId storageId, GroupV2Record groupV2Record) {
        this.id = storageId;
        this.proto = groupV2Record;
        this.hasUnknownFields = ProtoUtil.hasUnknownFields(groupV2Record);
        this.masterKey = groupV2Record.getMasterKey().toByteArray();
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public StorageId getId() {
        return this.id;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public SignalStorageRecord asStorageRecord() {
        return SignalStorageRecord.forGroupV2(this);
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public String describeDiff(SignalRecord signalRecord) {
        if (signalRecord instanceof SignalGroupV2Record) {
            SignalGroupV2Record signalGroupV2Record = (SignalGroupV2Record) signalRecord;
            LinkedList linkedList = new LinkedList();
            if (!Arrays.equals(this.id.getRaw(), signalGroupV2Record.id.getRaw())) {
                linkedList.add("ID");
            }
            if (!Arrays.equals(getMasterKeyBytes(), signalGroupV2Record.getMasterKeyBytes())) {
                linkedList.add("MasterKey");
            }
            if (!Objects.equals(Boolean.valueOf(isBlocked()), Boolean.valueOf(signalGroupV2Record.isBlocked()))) {
                linkedList.add("Blocked");
            }
            if (!Objects.equals(Boolean.valueOf(isProfileSharingEnabled()), Boolean.valueOf(signalGroupV2Record.isProfileSharingEnabled()))) {
                linkedList.add("ProfileSharing");
            }
            if (!Objects.equals(Boolean.valueOf(isArchived()), Boolean.valueOf(signalGroupV2Record.isArchived()))) {
                linkedList.add("Archived");
            }
            if (!Objects.equals(Boolean.valueOf(isForcedUnread()), Boolean.valueOf(signalGroupV2Record.isForcedUnread()))) {
                linkedList.add("ForcedUnread");
            }
            if (!Objects.equals(Long.valueOf(getMuteUntil()), Long.valueOf(signalGroupV2Record.getMuteUntil()))) {
                linkedList.add("MuteUntil");
            }
            if (!Objects.equals(Boolean.valueOf(notifyForMentionsWhenMuted()), Boolean.valueOf(signalGroupV2Record.notifyForMentionsWhenMuted()))) {
                linkedList.add("NotifyForMentionsWhenMuted");
            }
            if (shouldHideStory() != signalGroupV2Record.shouldHideStory()) {
                linkedList.add("HideStory");
            }
            if (!Objects.equals(Boolean.valueOf(hasUnknownFields()), Boolean.valueOf(signalGroupV2Record.hasUnknownFields()))) {
                linkedList.add("UnknownFields");
            }
            return linkedList.toString();
        }
        return "Different class. " + SignalGroupV2Record.class.getSimpleName() + " | " + signalRecord.getClass().getSimpleName();
    }

    public boolean hasUnknownFields() {
        return this.hasUnknownFields;
    }

    public byte[] serializeUnknownFields() {
        if (this.hasUnknownFields) {
            return this.proto.toByteArray();
        }
        return null;
    }

    public byte[] getMasterKeyBytes() {
        return this.masterKey;
    }

    public GroupMasterKey getMasterKeyOrThrow() {
        try {
            return new GroupMasterKey(this.masterKey);
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public boolean isBlocked() {
        return this.proto.getBlocked();
    }

    public boolean isProfileSharingEnabled() {
        return this.proto.getWhitelisted();
    }

    public boolean isArchived() {
        return this.proto.getArchived();
    }

    public boolean isForcedUnread() {
        return this.proto.getMarkedUnread();
    }

    public long getMuteUntil() {
        return this.proto.getMutedUntilTimestamp();
    }

    public boolean notifyForMentionsWhenMuted() {
        return !this.proto.getDontNotifyForMentionsIfMuted();
    }

    public boolean shouldHideStory() {
        return this.proto.getHideStory();
    }

    public GroupV2Record toProto() {
        return this.proto;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SignalGroupV2Record.class != obj.getClass()) {
            return false;
        }
        SignalGroupV2Record signalGroupV2Record = (SignalGroupV2Record) obj;
        if (!this.id.equals(signalGroupV2Record.id) || !this.proto.equals(signalGroupV2Record.proto)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.proto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        private final GroupV2Record.Builder builder;
        private final StorageId id;

        public Builder(byte[] bArr, GroupMasterKey groupMasterKey, byte[] bArr2) {
            this(bArr, groupMasterKey.serialize(), bArr2);
        }

        public Builder(byte[] bArr, byte[] bArr2, byte[] bArr3) {
            this.id = StorageId.forGroupV2(bArr);
            if (bArr3 != null) {
                this.builder = parseUnknowns(bArr3);
            } else {
                this.builder = GroupV2Record.newBuilder();
            }
            this.builder.setMasterKey(ByteString.copyFrom(bArr2));
        }

        public Builder setBlocked(boolean z) {
            this.builder.setBlocked(z);
            return this;
        }

        public Builder setProfileSharingEnabled(boolean z) {
            this.builder.setWhitelisted(z);
            return this;
        }

        public Builder setArchived(boolean z) {
            this.builder.setArchived(z);
            return this;
        }

        public Builder setForcedUnread(boolean z) {
            this.builder.setMarkedUnread(z);
            return this;
        }

        public Builder setMuteUntil(long j) {
            this.builder.setMutedUntilTimestamp(j);
            return this;
        }

        public Builder setNotifyForMentionsWhenMuted(boolean z) {
            this.builder.setDontNotifyForMentionsIfMuted(!z);
            return this;
        }

        public Builder setHideStory(boolean z) {
            this.builder.setHideStory(z);
            return this;
        }

        private static GroupV2Record.Builder parseUnknowns(byte[] bArr) {
            try {
                return GroupV2Record.parseFrom(bArr).toBuilder();
            } catch (InvalidProtocolBufferException e) {
                Log.w(SignalGroupV2Record.TAG, "Failed to combine unknown fields!", e);
                return GroupV2Record.newBuilder();
            }
        }

        public SignalGroupV2Record build() {
            return new SignalGroupV2Record(this.id, this.builder.build());
        }
    }
}
