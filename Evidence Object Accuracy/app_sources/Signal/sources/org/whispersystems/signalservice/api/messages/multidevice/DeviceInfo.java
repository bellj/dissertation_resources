package org.whispersystems.signalservice.api.messages.multidevice;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class DeviceInfo {
    @JsonProperty
    private long created;
    @JsonProperty
    private long id;
    @JsonProperty
    private long lastSeen;
    @JsonProperty
    private String name;

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public long getCreated() {
        return this.created;
    }

    public long getLastSeen() {
        return this.lastSeen;
    }
}
