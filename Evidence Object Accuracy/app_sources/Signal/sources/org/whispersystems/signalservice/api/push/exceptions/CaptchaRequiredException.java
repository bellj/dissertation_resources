package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class CaptchaRequiredException extends NonSuccessfulResponseCodeException {
    public CaptchaRequiredException() {
        super(402);
    }
}
