package org.whispersystems.signalservice.api;

import org.signal.libsignal.protocol.state.SignalProtocolStore;

/* loaded from: classes5.dex */
public interface SignalServiceAccountDataStore extends SignalProtocolStore, SignalServiceSessionStore, SignalServiceSenderKeyStore {
    boolean isMultiDevice();
}
