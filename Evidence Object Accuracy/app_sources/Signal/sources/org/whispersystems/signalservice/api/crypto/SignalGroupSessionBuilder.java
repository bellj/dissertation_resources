package org.whispersystems.signalservice.api.crypto;

import java.util.UUID;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.GroupSessionBuilder;
import org.signal.libsignal.protocol.message.SenderKeyDistributionMessage;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes5.dex */
public class SignalGroupSessionBuilder {
    private final GroupSessionBuilder builder;
    private final SignalSessionLock lock;

    public SignalGroupSessionBuilder(SignalSessionLock signalSessionLock, GroupSessionBuilder groupSessionBuilder) {
        this.lock = signalSessionLock;
        this.builder = groupSessionBuilder;
    }

    public void process(SignalProtocolAddress signalProtocolAddress, SenderKeyDistributionMessage senderKeyDistributionMessage) {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            this.builder.process(signalProtocolAddress, senderKeyDistributionMessage);
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public SenderKeyDistributionMessage create(SignalProtocolAddress signalProtocolAddress, UUID uuid) {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            SenderKeyDistributionMessage create = this.builder.create(signalProtocolAddress, uuid);
            if (acquire != null) {
                acquire.close();
            }
            return create;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
