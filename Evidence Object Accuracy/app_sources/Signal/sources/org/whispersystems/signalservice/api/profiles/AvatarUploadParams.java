package org.whispersystems.signalservice.api.profiles;

import org.whispersystems.signalservice.api.util.StreamDetails;

/* loaded from: classes5.dex */
public class AvatarUploadParams {
    public final boolean hasAvatar;
    public final boolean keepTheSame;
    public final StreamDetails stream;

    public static AvatarUploadParams unchanged(boolean z) {
        return new AvatarUploadParams(true, z, null);
    }

    public static AvatarUploadParams forAvatar(StreamDetails streamDetails) {
        return new AvatarUploadParams(false, streamDetails != null, streamDetails);
    }

    private AvatarUploadParams(boolean z, boolean z2, StreamDetails streamDetails) {
        this.keepTheSame = z;
        this.hasAvatar = z2;
        this.stream = streamDetails;
    }
}
