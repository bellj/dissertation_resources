package org.whispersystems.signalservice.api.messages.multidevice;

import androidx.recyclerview.widget.RecyclerView;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* loaded from: classes5.dex */
public class ChunkedOutputStream {
    protected final OutputStream out;

    public ChunkedOutputStream(OutputStream outputStream) {
        this.out = outputStream;
    }

    public void writeVarint32(int i) throws IOException {
        while ((i & -128) != 0) {
            this.out.write((i & 127) | 128);
            i >>>= 7;
        }
        this.out.write(i);
    }

    public void writeStream(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                this.out.write(bArr, 0, read);
            } else {
                inputStream.close();
                return;
            }
        }
    }
}
