package org.whispersystems.signalservice.api.push.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes5.dex */
public class NonNormalizedPhoneNumberException extends NonSuccessfulResponseCodeException {
    private final String normalizedNumber;
    private final String originalNumber;

    public static NonNormalizedPhoneNumberException forResponse(String str) throws MalformedResponseException {
        JsonResponse jsonResponse = (JsonResponse) JsonUtil.fromJsonResponse(str, JsonResponse.class);
        return new NonNormalizedPhoneNumberException(jsonResponse.originalNumber, jsonResponse.normalizedNumber);
    }

    public NonNormalizedPhoneNumberException(String str, String str2) {
        super(400);
        this.originalNumber = str;
        this.normalizedNumber = str2;
    }

    public String getOriginalNumber() {
        return this.originalNumber;
    }

    public String getNormalizedNumber() {
        return this.normalizedNumber;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes.dex */
    public static class JsonResponse {
        @JsonProperty
        private String normalizedNumber;
        @JsonProperty
        private String originalNumber;

        private JsonResponse() {
        }
    }
}
