package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class NetworkFailureException extends Exception {
    private final String e164number;

    public NetworkFailureException(String str, Exception exc) {
        super(exc);
        this.e164number = str;
    }

    public String getE164number() {
        return this.e164number;
    }
}
