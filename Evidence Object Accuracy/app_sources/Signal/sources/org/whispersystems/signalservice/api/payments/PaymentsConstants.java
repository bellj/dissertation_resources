package org.whispersystems.signalservice.api.payments;

/* loaded from: classes5.dex */
public final class PaymentsConstants {
    public static final int MNEMONIC_LENGTH = Math.round(24.0f);
    public static final int PAYMENTS_ENTROPY_LENGTH;
    public static final int SHORT_FRACTION_LENGTH;

    private PaymentsConstants() {
    }
}
