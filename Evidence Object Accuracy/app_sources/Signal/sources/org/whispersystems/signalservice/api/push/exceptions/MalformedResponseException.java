package org.whispersystems.signalservice.api.push.exceptions;

import java.io.IOException;

/* loaded from: classes5.dex */
public class MalformedResponseException extends IOException {
    public MalformedResponseException(String str) {
        super(str);
    }

    public MalformedResponseException(String str, IOException iOException) {
        super(str, iOException);
    }
}
