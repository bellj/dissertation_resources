package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import org.whispersystems.signalservice.api.InvalidMessageStructureException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public final class SignalServiceAttachmentRemoteId {
    private final Optional<Long> v2;
    private final Optional<String> v3;

    public SignalServiceAttachmentRemoteId(long j) {
        this.v2 = Optional.of(Long.valueOf(j));
        this.v3 = Optional.empty();
    }

    public SignalServiceAttachmentRemoteId(String str) {
        this.v2 = Optional.empty();
        this.v3 = Optional.of(str);
    }

    public Optional<Long> getV2() {
        return this.v2;
    }

    public Optional<String> getV3() {
        return this.v3;
    }

    public String toString() {
        if (this.v2.isPresent()) {
            return this.v2.get().toString();
        }
        return this.v3.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId$1 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$AttachmentPointer$AttachmentIdentifierCase;

        static {
            int[] iArr = new int[SignalServiceProtos.AttachmentPointer.AttachmentIdentifierCase.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$AttachmentPointer$AttachmentIdentifierCase = iArr;
            try {
                iArr[SignalServiceProtos.AttachmentPointer.AttachmentIdentifierCase.CDNID.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$AttachmentPointer$AttachmentIdentifierCase[SignalServiceProtos.AttachmentPointer.AttachmentIdentifierCase.CDNKEY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$AttachmentPointer$AttachmentIdentifierCase[SignalServiceProtos.AttachmentPointer.AttachmentIdentifierCase.ATTACHMENTIDENTIFIER_NOT_SET.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public static SignalServiceAttachmentRemoteId from(SignalServiceProtos.AttachmentPointer attachmentPointer) throws InvalidMessageStructureException {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$push$SignalServiceProtos$AttachmentPointer$AttachmentIdentifierCase[attachmentPointer.getAttachmentIdentifierCase().ordinal()];
        if (i == 1) {
            return new SignalServiceAttachmentRemoteId(attachmentPointer.getCdnId());
        }
        if (i == 2) {
            return new SignalServiceAttachmentRemoteId(attachmentPointer.getCdnKey());
        }
        if (i != 3) {
            return null;
        }
        throw new InvalidMessageStructureException("AttachmentPointer CDN location not set");
    }

    public static SignalServiceAttachmentRemoteId from(String str) {
        try {
            return new SignalServiceAttachmentRemoteId(Long.parseLong(str));
        } catch (NumberFormatException unused) {
            return new SignalServiceAttachmentRemoteId(str);
        }
    }
}
