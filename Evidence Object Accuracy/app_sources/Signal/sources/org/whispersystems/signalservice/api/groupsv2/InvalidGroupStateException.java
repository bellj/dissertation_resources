package org.whispersystems.signalservice.api.groupsv2;

import org.signal.libsignal.zkgroup.InvalidInputException;

/* loaded from: classes5.dex */
public final class InvalidGroupStateException extends Exception {
    public InvalidGroupStateException(InvalidInputException invalidInputException) {
        super(invalidInputException);
    }

    public InvalidGroupStateException() {
    }
}
