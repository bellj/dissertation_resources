package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class StickerPackOperationMessage {
    private final Optional<byte[]> packId;
    private final Optional<byte[]> packKey;
    private final Optional<Type> type;

    /* loaded from: classes5.dex */
    public enum Type {
        INSTALL,
        REMOVE
    }

    public StickerPackOperationMessage(byte[] bArr, byte[] bArr2, Type type) {
        this.packId = Optional.ofNullable(bArr);
        this.packKey = Optional.ofNullable(bArr2);
        this.type = Optional.ofNullable(type);
    }

    public Optional<byte[]> getPackId() {
        return this.packId;
    }

    public Optional<byte[]> getPackKey() {
        return this.packKey;
    }

    public Optional<Type> getType() {
        return this.type;
    }
}
