package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.util.ArrayList;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.internal.storage.protos.ManifestRecord;
import org.whispersystems.signalservice.internal.storage.protos.StorageItem;
import org.whispersystems.signalservice.internal.storage.protos.StorageManifest;
import org.whispersystems.signalservice.internal.storage.protos.StorageRecord;

/* loaded from: classes5.dex */
public final class SignalStorageModels {
    private static final String TAG;

    public static SignalStorageManifest remoteToLocalStorageManifest(StorageManifest storageManifest, StorageKey storageKey) throws IOException, InvalidKeyException {
        ManifestRecord parseFrom = ManifestRecord.parseFrom(SignalStorageCipher.decrypt(storageKey.deriveManifestKey(storageManifest.getVersion()), storageManifest.getValue().toByteArray()));
        ArrayList arrayList = new ArrayList(parseFrom.getIdentifiersCount());
        for (ManifestRecord.Identifier identifier : parseFrom.getIdentifiersList()) {
            arrayList.add(StorageId.forType(identifier.getRaw().toByteArray(), identifier.getTypeValue()));
        }
        return new SignalStorageManifest(parseFrom.getVersion(), arrayList);
    }

    public static SignalStorageRecord remoteToLocalStorageRecord(StorageItem storageItem, int i, StorageKey storageKey) throws IOException, InvalidKeyException {
        byte[] byteArray = storageItem.getKey().toByteArray();
        StorageRecord parseFrom = StorageRecord.parseFrom(SignalStorageCipher.decrypt(storageKey.deriveItemKey(byteArray), storageItem.getValue().toByteArray()));
        StorageId forType = StorageId.forType(byteArray, i);
        if (parseFrom.hasContact() && i == 1) {
            return SignalStorageRecord.forContact(forType, new SignalContactRecord(forType, parseFrom.getContact()));
        }
        if (parseFrom.hasGroupV1() && i == 2) {
            return SignalStorageRecord.forGroupV1(forType, new SignalGroupV1Record(forType, parseFrom.getGroupV1()));
        }
        if (parseFrom.hasGroupV2() && i == 3 && parseFrom.getGroupV2().getMasterKey().size() == 32) {
            return SignalStorageRecord.forGroupV2(forType, new SignalGroupV2Record(forType, parseFrom.getGroupV2()));
        }
        if (parseFrom.hasAccount() && i == 4) {
            return SignalStorageRecord.forAccount(forType, new SignalAccountRecord(forType, parseFrom.getAccount()));
        }
        if (parseFrom.hasStoryDistributionList() && i == 5) {
            return SignalStorageRecord.forStoryDistributionList(forType, new SignalStoryDistributionListRecord(forType, parseFrom.getStoryDistributionList()));
        }
        if (StorageId.isKnownType(i)) {
            String str = TAG;
            Log.w(str, "StorageId is of known type (" + i + "), but the data is bad! Falling back to unknown.");
        }
        return SignalStorageRecord.forUnknown(StorageId.forType(byteArray, i));
    }

    public static StorageItem localToRemoteStorageRecord(SignalStorageRecord signalStorageRecord, StorageKey storageKey) {
        StorageRecord.Builder newBuilder = StorageRecord.newBuilder();
        if (signalStorageRecord.getContact().isPresent()) {
            newBuilder.setContact(signalStorageRecord.getContact().get().toProto());
        } else if (signalStorageRecord.getGroupV1().isPresent()) {
            newBuilder.setGroupV1(signalStorageRecord.getGroupV1().get().toProto());
        } else if (signalStorageRecord.getGroupV2().isPresent()) {
            newBuilder.setGroupV2(signalStorageRecord.getGroupV2().get().toProto());
        } else if (signalStorageRecord.getAccount().isPresent()) {
            newBuilder.setAccount(signalStorageRecord.getAccount().get().toProto());
        } else if (signalStorageRecord.getStoryDistributionList().isPresent()) {
            newBuilder.setStoryDistributionList(signalStorageRecord.getStoryDistributionList().get().toProto());
        } else {
            throw new InvalidStorageWriteError();
        }
        return StorageItem.newBuilder().setKey(ByteString.copyFrom(signalStorageRecord.getId().getRaw())).setValue(ByteString.copyFrom(SignalStorageCipher.encrypt(storageKey.deriveItemKey(signalStorageRecord.getId().getRaw()), newBuilder.build().toByteArray()))).build();
    }

    /* loaded from: classes5.dex */
    public static class InvalidStorageWriteError extends Error {
        private InvalidStorageWriteError() {
        }
    }
}
