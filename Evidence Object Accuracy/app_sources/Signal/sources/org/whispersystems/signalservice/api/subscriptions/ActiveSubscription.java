package org.whispersystems.signalservice.api.subscriptions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/* loaded from: classes.dex */
public final class ActiveSubscription {
    public static final ActiveSubscription EMPTY = new ActiveSubscription(null, null);
    private final Subscription activeSubscription;
    private final ChargeFailure chargeFailure;

    /* loaded from: classes5.dex */
    public enum Status {
        TRIALING("trialing"),
        ACTIVE("active"),
        INCOMPLETE("incomplete"),
        INCOMPLETE_EXPIRED("incomplete_expired"),
        PAST_DUE("past_due"),
        CANCELED("canceled"),
        UNPAID("unpaid");
        
        private static final Set<Status> FAILURE_STATUSES = new HashSet(Arrays.asList(INCOMPLETE_EXPIRED, PAST_DUE, UNPAID));
        private final String status;

        Status(String str) {
            this.status = str;
        }

        public static Status getStatus(String str) {
            Status[] values = values();
            for (Status status : values) {
                if (Objects.equals(str, status.status)) {
                    return status;
                }
            }
            throw new IllegalArgumentException("Unknown status " + str);
        }

        static boolean isPaymentFailed(String str) {
            return FAILURE_STATUSES.contains(getStatus(str));
        }
    }

    @JsonCreator
    public ActiveSubscription(@JsonProperty("subscription") Subscription subscription, @JsonProperty("chargeFailure") ChargeFailure chargeFailure) {
        this.activeSubscription = subscription;
        this.chargeFailure = chargeFailure;
    }

    public Subscription getActiveSubscription() {
        return this.activeSubscription;
    }

    public ChargeFailure getChargeFailure() {
        return this.chargeFailure;
    }

    public boolean isActive() {
        Subscription subscription = this.activeSubscription;
        return subscription != null && subscription.isActive();
    }

    public boolean isInProgress() {
        return this.activeSubscription != null && !isActive() && !this.activeSubscription.isFailedPayment();
    }

    public boolean isFailedPayment() {
        return this.chargeFailure != null || (this.activeSubscription != null && !isActive() && this.activeSubscription.isFailedPayment());
    }

    /* loaded from: classes.dex */
    public static final class Subscription {
        private final BigDecimal amount;
        private final long billingCycleAnchor;
        private final String currency;
        private final long endOfCurrentPeriod;
        private final boolean isActive;
        private final int level;
        private final String status;
        private final boolean willCancelAtPeriodEnd;

        @JsonCreator
        public Subscription(@JsonProperty("level") int i, @JsonProperty("currency") String str, @JsonProperty("amount") BigDecimal bigDecimal, @JsonProperty("endOfCurrentPeriod") long j, @JsonProperty("active") boolean z, @JsonProperty("billingCycleAnchor") long j2, @JsonProperty("cancelAtPeriodEnd") boolean z2, @JsonProperty("status") String str2) {
            this.level = i;
            this.currency = str;
            this.amount = bigDecimal;
            this.endOfCurrentPeriod = j;
            this.isActive = z;
            this.billingCycleAnchor = j2;
            this.willCancelAtPeriodEnd = z2;
            this.status = str2;
        }

        public int getLevel() {
            return this.level;
        }

        public String getCurrency() {
            return this.currency;
        }

        public BigDecimal getAmount() {
            return this.amount;
        }

        public long getBillingCycleAnchor() {
            return this.billingCycleAnchor;
        }

        public boolean isActive() {
            return this.isActive;
        }

        public long getEndOfCurrentPeriod() {
            return this.endOfCurrentPeriod;
        }

        public boolean willCancelAtPeriodEnd() {
            return this.willCancelAtPeriodEnd;
        }

        public String getStatus() {
            return this.status;
        }

        public boolean isInProgress() {
            return !isActive() && !Status.isPaymentFailed(getStatus());
        }

        public boolean isFailedPayment() {
            return Status.isPaymentFailed(getStatus());
        }

        public boolean isCanceled() {
            return Status.getStatus(getStatus()) == Status.CANCELED;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Subscription.class != obj.getClass()) {
                return false;
            }
            Subscription subscription = (Subscription) obj;
            if (this.level == subscription.level && this.endOfCurrentPeriod == subscription.endOfCurrentPeriod && this.isActive == subscription.isActive && this.billingCycleAnchor == subscription.billingCycleAnchor && this.willCancelAtPeriodEnd == subscription.willCancelAtPeriodEnd && this.currency.equals(subscription.currency) && this.amount.equals(subscription.amount) && this.status.equals(subscription.status)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hash(Integer.valueOf(this.level), this.currency, this.amount, Long.valueOf(this.endOfCurrentPeriod), Boolean.valueOf(this.isActive), Long.valueOf(this.billingCycleAnchor), Boolean.valueOf(this.willCancelAtPeriodEnd), this.status);
        }
    }

    /* loaded from: classes.dex */
    public static final class ChargeFailure {
        private final String code;
        private final String message;
        private final String outcomeNetworkReason;
        private final String outcomeNetworkStatus;
        private final String outcomeType;

        @JsonCreator
        public ChargeFailure(@JsonProperty("code") String str, @JsonProperty("message") String str2, @JsonProperty("outcomeNetworkStatus") String str3, @JsonProperty("outcomeNetworkReason") String str4, @JsonProperty("outcomeType") String str5) {
            this.code = str;
            this.message = str2;
            this.outcomeNetworkStatus = str3;
            this.outcomeNetworkReason = str4;
            this.outcomeType = str5;
        }

        public String getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }

        public String getOutcomeNetworkStatus() {
            return this.outcomeNetworkStatus;
        }

        public String getOutcomeNetworkReason() {
            return this.outcomeNetworkReason;
        }

        public String getOutcomeType() {
            return this.outcomeType;
        }

        public String toString() {
            return "ChargeFailure{code='" + this.code + "', outcomeNetworkStatus='" + this.outcomeNetworkStatus + "', outcomeNetworkReason='" + this.outcomeNetworkReason + "', outcomeType='" + this.outcomeType + "'}";
        }
    }
}
