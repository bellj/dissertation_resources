package org.whispersystems.signalservice.api.messages.calls;

import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class HangupMessage {
    private final int deviceId;
    private final long id;
    private final boolean isLegacy;
    private final Type type;

    public HangupMessage(long j, Type type, int i, boolean z) {
        this.id = j;
        this.type = type;
        this.deviceId = i;
        this.isLegacy = z;
    }

    public long getId() {
        return this.id;
    }

    public Type getType() {
        return this.type;
    }

    public int getDeviceId() {
        return this.deviceId;
    }

    public boolean isLegacy() {
        return this.isLegacy;
    }

    /* loaded from: classes5.dex */
    public enum Type {
        NORMAL("normal", SignalServiceProtos.CallMessage.Hangup.Type.HANGUP_NORMAL),
        ACCEPTED("accepted", SignalServiceProtos.CallMessage.Hangup.Type.HANGUP_ACCEPTED),
        DECLINED("declined", SignalServiceProtos.CallMessage.Hangup.Type.HANGUP_DECLINED),
        BUSY("busy", SignalServiceProtos.CallMessage.Hangup.Type.HANGUP_BUSY),
        NEED_PERMISSION("need_permission", SignalServiceProtos.CallMessage.Hangup.Type.HANGUP_NEED_PERMISSION);
        
        private final String code;
        private final SignalServiceProtos.CallMessage.Hangup.Type protoType;

        Type(String str, SignalServiceProtos.CallMessage.Hangup.Type type) {
            this.code = str;
            this.protoType = type;
        }

        public String getCode() {
            return this.code;
        }

        public SignalServiceProtos.CallMessage.Hangup.Type getProtoType() {
            return this.protoType;
        }

        public static Type fromCode(String str) {
            Type[] values = values();
            for (Type type : values) {
                if (type.getCode().equals(str)) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Unexpected code: " + str);
        }

        public static Type fromProto(SignalServiceProtos.CallMessage.Hangup.Type type) {
            Type[] values = values();
            for (Type type2 : values) {
                if (type2.getProtoType().equals(type)) {
                    return type2;
                }
            }
            throw new IllegalArgumentException("Unexpected type: " + type.name());
        }
    }
}
