package org.whispersystems.signalservice.api.account;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public final class ChangePhoneNumberRequest {
    @JsonProperty
    private String code;
    @JsonProperty
    private String number;
    @JsonProperty("reglock")
    private String registrationLock;

    public ChangePhoneNumberRequest(String str, String str2, String str3) {
        this.number = str;
        this.code = str2;
        this.registrationLock = str3;
    }

    public String getNumber() {
        return this.number;
    }

    public String getCode() {
        return this.code;
    }

    public String getRegistrationLock() {
        return this.registrationLock;
    }
}
