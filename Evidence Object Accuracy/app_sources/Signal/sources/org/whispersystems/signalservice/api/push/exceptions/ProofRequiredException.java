package org.whispersystems.signalservice.api.push.exceptions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.internal.push.ProofRequiredResponse;

/* loaded from: classes5.dex */
public class ProofRequiredException extends NonSuccessfulResponseCodeException {
    private static final String TAG;
    private final Set<Option> options;
    private final long retryAfterSeconds;
    private final String token;

    /* loaded from: classes5.dex */
    public enum Option {
        RECAPTCHA,
        PUSH_CHALLENGE
    }

    public ProofRequiredException(ProofRequiredResponse proofRequiredResponse, long j) {
        super(428);
        this.token = proofRequiredResponse.getToken();
        this.options = parseOptions(proofRequiredResponse.getOptions());
        this.retryAfterSeconds = j;
    }

    public String getToken() {
        return this.token;
    }

    public Set<Option> getOptions() {
        return this.options;
    }

    public long getRetryAfterSeconds() {
        return this.retryAfterSeconds;
    }

    private static Set<Option> parseOptions(List<String> list) {
        HashSet hashSet = new HashSet(list.size());
        for (String str : list) {
            str.hashCode();
            if (str.equals("pushChallenge")) {
                hashSet.add(Option.PUSH_CHALLENGE);
            } else if (!str.equals("recaptcha")) {
                Log.w(TAG, "Unrecognized challenge option: " + str);
            } else {
                hashSet.add(Option.RECAPTCHA);
            }
        }
        return hashSet;
    }
}
