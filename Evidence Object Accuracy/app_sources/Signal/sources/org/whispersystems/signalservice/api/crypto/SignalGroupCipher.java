package org.whispersystems.signalservice.api.crypto;

import java.util.UUID;
import org.signal.libsignal.protocol.DuplicateMessageException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.groups.GroupCipher;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes5.dex */
public class SignalGroupCipher {
    private final GroupCipher cipher;
    private final SignalSessionLock lock;

    public SignalGroupCipher(SignalSessionLock signalSessionLock, GroupCipher groupCipher) {
        this.lock = signalSessionLock;
        this.cipher = groupCipher;
    }

    public CiphertextMessage encrypt(UUID uuid, byte[] bArr) throws NoSessionException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            CiphertextMessage encrypt = this.cipher.encrypt(uuid, bArr);
            if (acquire != null) {
                acquire.close();
            }
            return encrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public byte[] decrypt(byte[] bArr) throws LegacyMessageException, DuplicateMessageException, InvalidMessageException, NoSessionException {
        SignalSessionLock.Lock acquire = this.lock.acquire();
        try {
            byte[] decrypt = this.cipher.decrypt(bArr);
            if (acquire != null) {
                acquire.close();
            }
            return decrypt;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
