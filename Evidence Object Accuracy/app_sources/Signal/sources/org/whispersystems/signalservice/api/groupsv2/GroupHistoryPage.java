package org.whispersystems.signalservice.api.groupsv2;

import java.util.List;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;

/* loaded from: classes5.dex */
public final class GroupHistoryPage {
    private final PagingData pagingData;
    private final List<DecryptedGroupHistoryEntry> results;

    public GroupHistoryPage(List<DecryptedGroupHistoryEntry> list, PagingData pagingData) {
        this.results = list;
        this.pagingData = pagingData;
    }

    public List<DecryptedGroupHistoryEntry> getResults() {
        return this.results;
    }

    public PagingData getPagingData() {
        return this.pagingData;
    }

    /* loaded from: classes5.dex */
    public static final class PagingData {
        public static final PagingData NONE = new PagingData(false, -1);
        private final boolean hasMorePages;
        private final int nextPageRevision;

        public static PagingData fromGroup(PushServiceSocket.GroupHistory groupHistory) {
            return new PagingData(groupHistory.hasMore(), groupHistory.hasMore() ? groupHistory.getNextPageStartGroupRevision() : -1);
        }

        private PagingData(boolean z, int i) {
            this.hasMorePages = z;
            this.nextPageRevision = i;
        }

        public boolean hasMorePages() {
            return this.hasMorePages;
        }

        public int getNextPageRevision() {
            return this.nextPageRevision;
        }
    }
}
