package org.whispersystems.signalservice.api.push.exceptions;

import java.io.IOException;

/* loaded from: classes5.dex */
public class PushNetworkException extends IOException {
    public PushNetworkException(Exception exc) {
        super(exc);
    }

    public PushNetworkException(String str) {
        super(str);
    }
}
