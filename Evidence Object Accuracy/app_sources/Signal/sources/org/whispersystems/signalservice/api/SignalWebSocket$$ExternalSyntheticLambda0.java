package org.whispersystems.signalservice.api;

import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class SignalWebSocket$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ BehaviorSubject f$0;

    public /* synthetic */ SignalWebSocket$$ExternalSyntheticLambda0(BehaviorSubject behaviorSubject) {
        this.f$0 = behaviorSubject;
    }

    @Override // io.reactivex.rxjava3.functions.Consumer
    public final void accept(Object obj) {
        this.f$0.onNext((WebSocketConnectionState) obj);
    }
}
