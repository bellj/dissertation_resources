package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;
import java.util.List;
import org.signal.libsignal.protocol.IdentityKey;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.RateLimitException;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class SendMessageResult {
    private final SignalServiceAddress address;
    private final IdentityFailure identityFailure;
    private final boolean networkFailure;
    private final ProofRequiredException proofRequiredFailure;
    private final RateLimitException rateLimitFailure;
    private final Success success;
    private final boolean unregisteredFailure;

    public static SendMessageResult success(SignalServiceAddress signalServiceAddress, List<Integer> list, boolean z, boolean z2, long j, Optional<SignalServiceProtos.Content> optional) {
        return new SendMessageResult(signalServiceAddress, new Success(z, z2, j, optional, list), false, false, null, null, null);
    }

    public static SendMessageResult networkFailure(SignalServiceAddress signalServiceAddress) {
        return new SendMessageResult(signalServiceAddress, null, true, false, null, null, null);
    }

    public static SendMessageResult unregisteredFailure(SignalServiceAddress signalServiceAddress) {
        return new SendMessageResult(signalServiceAddress, null, false, true, null, null, null);
    }

    public static SendMessageResult identityFailure(SignalServiceAddress signalServiceAddress, IdentityKey identityKey) {
        return new SendMessageResult(signalServiceAddress, null, false, false, new IdentityFailure(identityKey), null, null);
    }

    public static SendMessageResult proofRequiredFailure(SignalServiceAddress signalServiceAddress, ProofRequiredException proofRequiredException) {
        return new SendMessageResult(signalServiceAddress, null, false, false, null, proofRequiredException, null);
    }

    public static SendMessageResult rateLimitFailure(SignalServiceAddress signalServiceAddress, RateLimitException rateLimitException) {
        return new SendMessageResult(signalServiceAddress, null, false, false, null, null, rateLimitException);
    }

    public SignalServiceAddress getAddress() {
        return this.address;
    }

    public Success getSuccess() {
        return this.success;
    }

    public boolean isSuccess() {
        return this.success != null;
    }

    public boolean isNetworkFailure() {
        return (!this.networkFailure && this.proofRequiredFailure == null && this.rateLimitFailure == null) ? false : true;
    }

    public boolean isUnregisteredFailure() {
        return this.unregisteredFailure;
    }

    public IdentityFailure getIdentityFailure() {
        return this.identityFailure;
    }

    public ProofRequiredException getProofRequiredFailure() {
        return this.proofRequiredFailure;
    }

    public RateLimitException getRateLimitFailure() {
        return this.rateLimitFailure;
    }

    private SendMessageResult(SignalServiceAddress signalServiceAddress, Success success, boolean z, boolean z2, IdentityFailure identityFailure, ProofRequiredException proofRequiredException, RateLimitException rateLimitException) {
        this.address = signalServiceAddress;
        this.success = success;
        this.networkFailure = z;
        this.unregisteredFailure = z2;
        this.identityFailure = identityFailure;
        this.proofRequiredFailure = proofRequiredException;
        this.rateLimitFailure = rateLimitException;
    }

    /* loaded from: classes5.dex */
    public static class Success {
        private final Optional<SignalServiceProtos.Content> content;
        private final List<Integer> devices;
        private final long duration;
        private final boolean needsSync;
        private final boolean unidentified;

        private Success(boolean z, boolean z2, long j, Optional<SignalServiceProtos.Content> optional, List<Integer> list) {
            this.unidentified = z;
            this.needsSync = z2;
            this.duration = j;
            this.content = optional;
            this.devices = list;
        }

        public boolean isUnidentified() {
            return this.unidentified;
        }

        public boolean isNeedsSync() {
            return this.needsSync;
        }

        public long getDuration() {
            return this.duration;
        }

        public Optional<SignalServiceProtos.Content> getContent() {
            return this.content;
        }

        public List<Integer> getDevices() {
            return this.devices;
        }
    }

    /* loaded from: classes5.dex */
    public static class IdentityFailure {
        private final IdentityKey identityKey;

        private IdentityFailure(IdentityKey identityKey) {
            this.identityKey = identityKey;
        }

        public IdentityKey getIdentityKey() {
            return this.identityKey;
        }
    }
}
