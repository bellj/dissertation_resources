package org.whispersystems.signalservice.api;

/* loaded from: classes5.dex */
public class ContentTooLargeException extends IllegalStateException {
    public ContentTooLargeException(long j) {
        super("Too large! Size: " + j + " bytes");
    }
}
