package org.whispersystems.signalservice.api.crypto;

import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;

/* loaded from: classes5.dex */
public class ProfileCipherOutputStream extends DigestingOutputStream {
    private final Cipher cipher;

    public static long getCiphertextLength(long j) {
        return j + 28;
    }

    public ProfileCipherOutputStream(OutputStream outputStream, ProfileKey profileKey) throws IOException {
        super(outputStream);
        Object e;
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            this.cipher = instance;
            byte[] generateNonce = generateNonce();
            instance.init(1, new SecretKeySpec(profileKey.serialize(), "AES"), new GCMParameterSpec(128, generateNonce));
            super.write(generateNonce, 0, generateNonce.length);
        } catch (InvalidAlgorithmParameterException e2) {
            e = e2;
            throw new AssertionError(e);
        } catch (InvalidKeyException e3) {
            throw new IOException(e3);
        } catch (NoSuchAlgorithmException e4) {
            e = e4;
            throw new AssertionError(e);
        } catch (NoSuchPaddingException e5) {
            e = e5;
            throw new AssertionError(e);
        }
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        super.write(this.cipher.update(bArr, i, i2));
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream
    public void write(int i) throws IOException {
        super.write(this.cipher.update(new byte[]{(byte) i}));
    }

    @Override // org.whispersystems.signalservice.api.crypto.DigestingOutputStream, java.io.FilterOutputStream, java.io.OutputStream, java.io.Flushable
    public void flush() throws IOException {
        try {
            super.write(this.cipher.doFinal());
            super.flush();
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new AssertionError(e);
        }
    }

    private byte[] generateNonce() {
        byte[] bArr = new byte[12];
        new SecureRandom().nextBytes(bArr);
        return bArr;
    }
}
