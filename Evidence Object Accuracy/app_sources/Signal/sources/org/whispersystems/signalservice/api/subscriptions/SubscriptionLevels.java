package org.whispersystems.signalservice.api.subscriptions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Map;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;

/* loaded from: classes.dex */
public final class SubscriptionLevels {
    public static final String BOOST_LEVEL;
    private final Map<String, Level> levels;

    @JsonCreator
    public SubscriptionLevels(@JsonProperty("levels") Map<String, Level> map) {
        this.levels = map;
    }

    public Map<String, Level> getLevels() {
        return this.levels;
    }

    /* loaded from: classes.dex */
    public static final class Level {
        private final SignalServiceProfile.Badge badge;
        private final Map<String, BigDecimal> currencies;
        private final String name;

        @JsonCreator
        public Level(@JsonProperty("name") String str, @JsonProperty("badge") SignalServiceProfile.Badge badge, @JsonProperty("currencies") Map<String, BigDecimal> map) {
            this.name = str;
            this.badge = badge;
            this.currencies = map;
        }

        public String getName() {
            return this.name;
        }

        public SignalServiceProfile.Badge getBadge() {
            return this.badge;
        }

        public Map<String, BigDecimal> getCurrencies() {
            return this.currencies;
        }
    }
}
