package org.whispersystems.signalservice.api.messages.calls;

/* loaded from: classes5.dex */
public class IceUpdateMessage {
    private final long id;
    private final byte[] opaque;
    private final String sdp;

    public IceUpdateMessage(long j, byte[] bArr, String str) {
        this.id = j;
        this.opaque = bArr;
        this.sdp = str;
    }

    public long getId() {
        return this.id;
    }

    public byte[] getOpaque() {
        return this.opaque;
    }

    public String getSdp() {
        return this.sdp;
    }
}
