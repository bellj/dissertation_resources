package org.whispersystems.signalservice.api.messages.multidevice;

import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;

/* loaded from: classes5.dex */
public class ContactsMessage {
    private final boolean complete;
    private final SignalServiceAttachment contacts;

    public ContactsMessage(SignalServiceAttachment signalServiceAttachment, boolean z) {
        this.contacts = signalServiceAttachment;
        this.complete = z;
    }

    public SignalServiceAttachment getContactsStream() {
        return this.contacts;
    }

    public boolean isComplete() {
        return this.complete;
    }
}
