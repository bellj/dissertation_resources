package org.whispersystems.signalservice.api.websocket;

import java.io.IOException;

/* loaded from: classes5.dex */
public final class WebSocketUnavailableException extends IOException {
    public WebSocketUnavailableException() {
        super("WebSocket not currently available.");
    }
}
