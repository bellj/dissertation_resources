package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.functions.Function;
import org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class AttachmentService$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ DefaultResponseMapper f$0;

    public /* synthetic */ AttachmentService$$ExternalSyntheticLambda0(DefaultResponseMapper defaultResponseMapper) {
        this.f$0 = defaultResponseMapper;
    }

    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return this.f$0.map((WebsocketResponse) obj);
    }
}
