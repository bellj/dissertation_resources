package org.whispersystems.signalservice.api.util;

import j$.time.temporal.ChronoField;
import java.util.concurrent.TimeUnit;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;

/* loaded from: classes5.dex */
public final class ExpiringProfileCredentialUtil {
    private ExpiringProfileCredentialUtil() {
    }

    public static boolean isValid(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        if (expiringProfileKeyCredential != null && TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) < expiringProfileKeyCredential.getExpirationTime().getLong(ChronoField.INSTANT_SECONDS)) {
            return true;
        }
        return false;
    }
}
