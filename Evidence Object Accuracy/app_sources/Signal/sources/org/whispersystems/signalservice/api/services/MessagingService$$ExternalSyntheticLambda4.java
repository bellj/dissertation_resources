package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.functions.Function;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class MessagingService$$ExternalSyntheticLambda4 implements Function {
    public final /* synthetic */ ResponseMapper f$0;

    public /* synthetic */ MessagingService$$ExternalSyntheticLambda4(ResponseMapper responseMapper) {
        this.f$0 = responseMapper;
    }

    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return this.f$0.map((WebsocketResponse) obj);
    }
}
