package org.whispersystems.signalservice.api.util;

import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;

/* loaded from: classes5.dex */
public interface CredentialsProvider {
    ACI getAci();

    int getDeviceId();

    String getE164();

    String getPassword();

    PNI getPni();
}
