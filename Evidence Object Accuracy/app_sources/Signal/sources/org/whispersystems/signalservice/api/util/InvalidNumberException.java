package org.whispersystems.signalservice.api.util;

/* loaded from: classes5.dex */
public class InvalidNumberException extends Throwable {
    public InvalidNumberException(String str) {
        super(str);
    }
}
