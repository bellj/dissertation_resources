package org.whispersystems.signalservice.api.websocket;

/* loaded from: classes5.dex */
public enum WebSocketConnectionState {
    DISCONNECTED,
    CONNECTING,
    CONNECTED,
    RECONNECTING,
    DISCONNECTING,
    AUTHENTICATION_FAILED,
    FAILED;

    public boolean isFailure() {
        return this == AUTHENTICATION_FAILED || this == FAILED;
    }
}
