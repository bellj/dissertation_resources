package org.whispersystems.signalservice.api.subscriptions;

import java.security.SecureRandom;
import java.util.Arrays;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.util.Base64UrlSafe;

/* loaded from: classes5.dex */
public final class SubscriberId {
    private static final int SIZE;
    private final byte[] bytes;

    private SubscriberId(byte[] bArr) {
        this.bytes = bArr;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public String serialize() {
        return Base64UrlSafe.encodeBytes(this.bytes);
    }

    public static SubscriberId fromBytes(byte[] bArr) {
        Preconditions.checkArgument(bArr.length == 32);
        return new SubscriberId(bArr);
    }

    public static SubscriberId generate() {
        byte[] bArr = new byte[32];
        new SecureRandom().nextBytes(bArr);
        return new SubscriberId(bArr);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SubscriberId.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.bytes, ((SubscriberId) obj).bytes);
    }

    public int hashCode() {
        return Arrays.hashCode(this.bytes);
    }
}
