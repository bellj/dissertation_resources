package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class SignalServicePreview {
    private final long date;
    private final String description;
    private final Optional<SignalServiceAttachment> image;
    private final String title;
    private final String url;

    public SignalServicePreview(String str, String str2, String str3, long j, Optional<SignalServiceAttachment> optional) {
        this.url = str;
        this.title = str2;
        this.description = str3;
        this.date = j;
        this.image = optional;
    }

    public String getUrl() {
        return this.url;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public long getDate() {
        return this.date;
    }

    public Optional<SignalServiceAttachment> getImage() {
        return this.image;
    }
}
