package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public final class RangeException extends NonSuccessfulResponseCodeException {
    public RangeException(long j) {
        super(416, "Range request out of bounds " + j);
    }
}
