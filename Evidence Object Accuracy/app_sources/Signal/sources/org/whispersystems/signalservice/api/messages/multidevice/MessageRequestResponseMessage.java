package org.whispersystems.signalservice.api.messages.multidevice;

import j$.util.Optional;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes5.dex */
public class MessageRequestResponseMessage {
    private final Optional<byte[]> groupId;
    private final Optional<ServiceId> person;
    private final Type type;

    /* loaded from: classes5.dex */
    public enum Type {
        UNKNOWN,
        ACCEPT,
        DELETE,
        BLOCK,
        BLOCK_AND_DELETE,
        UNBLOCK_AND_ACCEPT
    }

    public static MessageRequestResponseMessage forIndividual(ServiceId serviceId, Type type) {
        return new MessageRequestResponseMessage(Optional.of(serviceId), Optional.empty(), type);
    }

    public static MessageRequestResponseMessage forGroup(byte[] bArr, Type type) {
        return new MessageRequestResponseMessage(Optional.empty(), Optional.of(bArr), type);
    }

    private MessageRequestResponseMessage(Optional<ServiceId> optional, Optional<byte[]> optional2, Type type) {
        this.person = optional;
        this.groupId = optional2;
        this.type = type;
    }

    public Optional<ServiceId> getPerson() {
        return this.person;
    }

    public Optional<byte[]> getGroupId() {
        return this.groupId;
    }

    public Type getType() {
        return this.type;
    }
}
