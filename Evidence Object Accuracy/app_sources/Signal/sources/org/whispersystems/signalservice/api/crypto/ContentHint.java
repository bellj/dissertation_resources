package org.whispersystems.signalservice.api.crypto;

import j$.util.Map;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes5.dex */
public enum ContentHint {
    DEFAULT(0),
    RESENDABLE(1),
    IMPLICIT(2);
    
    private static final Map<Integer, ContentHint> TYPE_MAP = new HashMap();
    private final int type;

    static {
        TYPE_MAP = new HashMap();
        ContentHint[] values = values();
        for (ContentHint contentHint : values) {
            TYPE_MAP.put(Integer.valueOf(contentHint.getType()), contentHint);
        }
    }

    ContentHint(int i) {
        this.type = i;
    }

    public int getType() {
        return this.type;
    }

    public static ContentHint fromType(int i) {
        return (ContentHint) Map.EL.getOrDefault(TYPE_MAP, Integer.valueOf(i), DEFAULT);
    }
}
