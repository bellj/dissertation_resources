package org.whispersystems.signalservice.api.messages;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class SignalServiceTextAttachment {
    private final Optional<Integer> backgroundColor;
    private final Optional<Gradient> backgroundGradient;
    private final Optional<SignalServicePreview> preview;
    private final Optional<Style> style;
    private final Optional<String> text;
    private final Optional<Integer> textBackgroundColor;
    private final Optional<Integer> textForegroundColor;

    /* loaded from: classes5.dex */
    public enum Style {
        DEFAULT,
        REGULAR,
        BOLD,
        SERIF,
        SCRIPT,
        CONDENSED
    }

    private SignalServiceTextAttachment(Optional<String> optional, Optional<Style> optional2, Optional<Integer> optional3, Optional<Integer> optional4, Optional<SignalServicePreview> optional5, Optional<Gradient> optional6, Optional<Integer> optional7) {
        this.text = optional;
        this.style = optional2;
        this.textForegroundColor = optional3;
        this.textBackgroundColor = optional4;
        this.preview = optional5;
        this.backgroundGradient = optional6;
        this.backgroundColor = optional7;
    }

    public static SignalServiceTextAttachment forGradientBackground(Optional<String> optional, Optional<Style> optional2, Optional<Integer> optional3, Optional<Integer> optional4, Optional<SignalServicePreview> optional5, Gradient gradient) {
        return new SignalServiceTextAttachment(optional, optional2, optional3, optional4, optional5, Optional.of(gradient), Optional.empty());
    }

    public static SignalServiceTextAttachment forSolidBackground(Optional<String> optional, Optional<Style> optional2, Optional<Integer> optional3, Optional<Integer> optional4, Optional<SignalServicePreview> optional5, int i) {
        return new SignalServiceTextAttachment(optional, optional2, optional3, optional4, optional5, Optional.empty(), Optional.of(Integer.valueOf(i)));
    }

    public Optional<String> getText() {
        return this.text;
    }

    public Optional<Style> getStyle() {
        return this.style;
    }

    public Optional<Integer> getTextForegroundColor() {
        return this.textForegroundColor;
    }

    public Optional<Integer> getTextBackgroundColor() {
        return this.textBackgroundColor;
    }

    public Optional<SignalServicePreview> getPreview() {
        return this.preview;
    }

    public Optional<Gradient> getBackgroundGradient() {
        return this.backgroundGradient;
    }

    public Optional<Integer> getBackgroundColor() {
        return this.backgroundColor;
    }

    /* loaded from: classes5.dex */
    public static class Gradient {
        private final Optional<Integer> angle;
        private final Optional<Integer> endColor;
        private final Optional<Integer> startColor;

        public Gradient(Optional<Integer> optional, Optional<Integer> optional2, Optional<Integer> optional3) {
            this.startColor = optional;
            this.endColor = optional2;
            this.angle = optional3;
        }

        public Optional<Integer> getStartColor() {
            return this.startColor;
        }

        public Optional<Integer> getEndColor() {
            return this.endColor;
        }

        public Optional<Integer> getAngle() {
            return this.angle;
        }
    }
}
