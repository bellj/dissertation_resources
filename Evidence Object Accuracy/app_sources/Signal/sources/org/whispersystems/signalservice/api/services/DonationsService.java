package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialRequest;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialResponse;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Operations;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.services.DonationsService;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionClientSecret;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.internal.EmptyResponse;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;

/* loaded from: classes5.dex */
public class DonationsService {
    private static final String TAG;
    private final PushServiceSocket pushServiceSocket;

    /* loaded from: classes5.dex */
    public interface Producer<T> {
        Pair<T, Integer> produce() throws IOException;
    }

    public DonationsService(SignalServiceConfiguration signalServiceConfiguration, CredentialsProvider credentialsProvider, String str, GroupsV2Operations groupsV2Operations, boolean z) {
        this(new PushServiceSocket(signalServiceConfiguration, credentialsProvider, str, groupsV2Operations.getProfileOperations(), z));
    }

    DonationsService(PushServiceSocket pushServiceSocket) {
        this.pushServiceSocket = pushServiceSocket;
    }

    public Single<ServiceResponse<EmptyResponse>> redeemReceipt(ReceiptCredentialPresentation receiptCredentialPresentation, boolean z, boolean z2) {
        return Single.fromCallable(new Callable(receiptCredentialPresentation, z, z2) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda7
            public final /* synthetic */ ReceiptCredentialPresentation f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationsService.this.lambda$redeemReceipt$0(this.f$1, this.f$2, this.f$3);
            }
        }).subscribeOn(Schedulers.io());
    }

    public /* synthetic */ ServiceResponse lambda$redeemReceipt$0(ReceiptCredentialPresentation receiptCredentialPresentation, boolean z, boolean z2) throws Exception {
        try {
            this.pushServiceSocket.redeemDonationReceipt(receiptCredentialPresentation, z, z2);
            return ServiceResponse.forResult(EmptyResponse.INSTANCE, 200, null);
        } catch (Exception e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    public /* synthetic */ Pair lambda$createDonationIntentWithAmount$1(String str, String str2, long j) throws IOException {
        return new Pair(this.pushServiceSocket.createBoostPaymentMethod(str, Long.parseLong(str2), j), 200);
    }

    public Single<ServiceResponse<SubscriptionClientSecret>> createDonationIntentWithAmount(String str, String str2, long j) {
        return createServiceResponse(new Producer(str2, str, j) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda6
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ long f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$createDonationIntentWithAmount$1(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ Pair lambda$submitBoostReceiptCredentialRequest$2(String str, ReceiptCredentialRequest receiptCredentialRequest) throws IOException {
        return new Pair(this.pushServiceSocket.submitBoostReceiptCredentials(str, receiptCredentialRequest), 200);
    }

    public Single<ServiceResponse<ReceiptCredentialResponse>> submitBoostReceiptCredentialRequest(String str, ReceiptCredentialRequest receiptCredentialRequest) {
        return createServiceResponse(new Producer(str, receiptCredentialRequest) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;
            public final /* synthetic */ ReceiptCredentialRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$submitBoostReceiptCredentialRequest$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ Pair lambda$getBoostAmounts$3() throws IOException {
        return new Pair(this.pushServiceSocket.getBoostAmounts(), 200);
    }

    public Single<ServiceResponse<Map<String, List<BigDecimal>>>> getBoostAmounts() {
        return createServiceResponse(new Producer() { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda1
            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getBoostAmounts$3();
            }
        });
    }

    public /* synthetic */ Pair lambda$getBoostBadge$4(Locale locale) throws IOException {
        return new Pair(this.pushServiceSocket.getBoostLevels(locale).getLevels().get(SubscriptionLevels.BOOST_LEVEL).getBadge(), 200);
    }

    public Single<ServiceResponse<SignalServiceProfile.Badge>> getBoostBadge(Locale locale) {
        return createServiceResponse(new Producer(locale) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda16
            public final /* synthetic */ Locale f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getBoostBadge$4(this.f$1);
            }
        });
    }

    public /* synthetic */ Pair lambda$getGiftBadge$5(Locale locale, long j) throws IOException {
        return new Pair(this.pushServiceSocket.getBoostLevels(locale).getLevels().get(String.valueOf(j)).getBadge(), 200);
    }

    public Single<ServiceResponse<SignalServiceProfile.Badge>> getGiftBadge(Locale locale, long j) {
        return createServiceResponse(new Producer(locale, j) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda3
            public final /* synthetic */ Locale f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getGiftBadge$5(this.f$1, this.f$2);
            }
        });
    }

    public Single<ServiceResponse<Map<Long, SignalServiceProfile.Badge>>> getGiftBadges(Locale locale) {
        return createServiceResponse(new Producer(locale) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda10
            public final /* synthetic */ Locale f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getGiftBadges$6(this.f$1);
            }
        });
    }

    public /* synthetic */ Pair lambda$getGiftBadges$6(Locale locale) throws IOException {
        Map<String, SubscriptionLevels.Level> levels = this.pushServiceSocket.getBoostLevels(locale).getLevels();
        TreeMap treeMap = new TreeMap();
        for (Map.Entry<String, SubscriptionLevels.Level> entry : levels.entrySet()) {
            if (!Objects.equals(entry.getKey(), SubscriptionLevels.BOOST_LEVEL)) {
                try {
                    treeMap.put(Long.valueOf(Long.parseLong(entry.getKey())), entry.getValue().getBadge());
                } catch (NumberFormatException e) {
                    String str = TAG;
                    Log.w(str, "Could not parse gift badge for level entry " + entry.getKey(), e);
                }
            }
        }
        return new Pair(treeMap, 200);
    }

    public /* synthetic */ Pair lambda$getGiftAmount$7() throws IOException {
        return new Pair(this.pushServiceSocket.getGiftAmount(), 200);
    }

    public Single<ServiceResponse<Map<String, BigDecimal>>> getGiftAmount() {
        return createServiceResponse(new Producer() { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda4
            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getGiftAmount$7();
            }
        });
    }

    public /* synthetic */ Pair lambda$getSubscriptionLevels$8(Locale locale) throws IOException {
        return new Pair(this.pushServiceSocket.getSubscriptionLevels(locale), 200);
    }

    public Single<ServiceResponse<SubscriptionLevels>> getSubscriptionLevels(Locale locale) {
        return createServiceResponse(new Producer(locale) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda15
            public final /* synthetic */ Locale f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getSubscriptionLevels$8(this.f$1);
            }
        });
    }

    public Single<ServiceResponse<EmptyResponse>> updateSubscriptionLevel(SubscriberId subscriberId, String str, String str2, String str3, Object obj) {
        return createServiceResponse(new Producer(obj, subscriberId, str, str2, str3) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda12
            public final /* synthetic */ Object f$1;
            public final /* synthetic */ SubscriberId f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String f$4;
            public final /* synthetic */ String f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$updateSubscriptionLevel$9(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    public /* synthetic */ Pair lambda$updateSubscriptionLevel$9(Object obj, SubscriberId subscriberId, String str, String str2, String str3) throws IOException {
        synchronized (obj) {
            this.pushServiceSocket.updateSubscriptionLevel(subscriberId.serialize(), str, str2, str3);
        }
        return new Pair(EmptyResponse.INSTANCE, 200);
    }

    public Single<ServiceResponse<ActiveSubscription>> getSubscription(SubscriberId subscriberId) {
        return createServiceResponse(new Producer(subscriberId) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda9
            public final /* synthetic */ SubscriberId f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$getSubscription$10(this.f$1);
            }
        });
    }

    public /* synthetic */ Pair lambda$getSubscription$10(SubscriberId subscriberId) throws IOException {
        return new Pair(this.pushServiceSocket.getSubscription(subscriberId.serialize()), 200);
    }

    public Single<ServiceResponse<EmptyResponse>> putSubscription(SubscriberId subscriberId) {
        return createServiceResponse(new Producer(subscriberId) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda0
            public final /* synthetic */ SubscriberId f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$putSubscription$11(this.f$1);
            }
        });
    }

    public /* synthetic */ Pair lambda$putSubscription$11(SubscriberId subscriberId) throws IOException {
        this.pushServiceSocket.putSubscription(subscriberId.serialize());
        return new Pair(EmptyResponse.INSTANCE, 200);
    }

    public Single<ServiceResponse<EmptyResponse>> cancelSubscription(SubscriberId subscriberId) {
        return createServiceResponse(new Producer(subscriberId) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda8
            public final /* synthetic */ SubscriberId f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$cancelSubscription$12(this.f$1);
            }
        });
    }

    public /* synthetic */ Pair lambda$cancelSubscription$12(SubscriberId subscriberId) throws IOException {
        this.pushServiceSocket.deleteSubscription(subscriberId.serialize());
        return new Pair(EmptyResponse.INSTANCE, 200);
    }

    public Single<ServiceResponse<EmptyResponse>> setDefaultPaymentMethodId(SubscriberId subscriberId, String str) {
        return createServiceResponse(new Producer(subscriberId, str) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda13
            public final /* synthetic */ SubscriberId f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$setDefaultPaymentMethodId$13(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ Pair lambda$setDefaultPaymentMethodId$13(SubscriberId subscriberId, String str) throws IOException {
        this.pushServiceSocket.setDefaultSubscriptionPaymentMethod(subscriberId.serialize(), str);
        return new Pair(EmptyResponse.INSTANCE, 200);
    }

    public Single<ServiceResponse<SubscriptionClientSecret>> createSubscriptionPaymentMethod(SubscriberId subscriberId) {
        return createServiceResponse(new Producer(subscriberId) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda14
            public final /* synthetic */ SubscriberId f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$createSubscriptionPaymentMethod$14(this.f$1);
            }
        });
    }

    public /* synthetic */ Pair lambda$createSubscriptionPaymentMethod$14(SubscriberId subscriberId) throws IOException {
        return new Pair(this.pushServiceSocket.createSubscriptionPaymentMethod(subscriberId.serialize()), 200);
    }

    public Single<ServiceResponse<ReceiptCredentialResponse>> submitReceiptCredentialRequest(SubscriberId subscriberId, ReceiptCredentialRequest receiptCredentialRequest) {
        return createServiceResponse(new Producer(subscriberId, receiptCredentialRequest) { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda5
            public final /* synthetic */ SubscriberId f$1;
            public final /* synthetic */ ReceiptCredentialRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.whispersystems.signalservice.api.services.DonationsService.Producer
            public final Pair produce() {
                return DonationsService.this.lambda$submitReceiptCredentialRequest$15(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ Pair lambda$submitReceiptCredentialRequest$15(SubscriberId subscriberId, ReceiptCredentialRequest receiptCredentialRequest) throws IOException {
        return new Pair(this.pushServiceSocket.submitReceiptCredentials(subscriberId.serialize(), receiptCredentialRequest), 200);
    }

    private <T> Single<ServiceResponse<T>> createServiceResponse(Producer<T> producer) {
        return Single.fromCallable(new Callable() { // from class: org.whispersystems.signalservice.api.services.DonationsService$$ExternalSyntheticLambda11
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationsService.lambda$createServiceResponse$16(DonationsService.Producer.this);
            }
        });
    }

    public static /* synthetic */ ServiceResponse lambda$createServiceResponse$16(Producer producer) throws Exception {
        try {
            Pair produce = producer.produce();
            return ServiceResponse.forResult(produce.first(), ((Integer) produce.second()).intValue(), null);
        } catch (NonSuccessfulResponseCodeException e) {
            Log.w(TAG, "Bad response code from server.", e);
            return ServiceResponse.forApplicationError(e, e.getCode(), e.getMessage());
        } catch (IOException e2) {
            Log.w(TAG, "An unknown error occurred.", e2);
            return ServiceResponse.forUnknownError(e2);
        }
    }
}
