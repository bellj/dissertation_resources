package org.whispersystems.signalservice.api.groupsv2;

import java.io.IOException;
import java.util.List;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.signal.storageservice.protos.groups.Group;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;

/* loaded from: classes5.dex */
public class PartialDecryptedGroup {
    private final DecryptedGroup decryptedGroup;
    private final Group group;
    private final GroupSecretParams groupSecretParams;
    private final GroupsV2Operations groupsOperations;

    public PartialDecryptedGroup(Group group, DecryptedGroup decryptedGroup, GroupsV2Operations groupsV2Operations, GroupSecretParams groupSecretParams) {
        this.group = group;
        this.decryptedGroup = decryptedGroup;
        this.groupsOperations = groupsV2Operations;
        this.groupSecretParams = groupSecretParams;
    }

    public int getRevision() {
        return this.decryptedGroup.getRevision();
    }

    public List<DecryptedMember> getMembersList() {
        return this.decryptedGroup.getMembersList();
    }

    public List<DecryptedPendingMember> getPendingMembersList() {
        return this.decryptedGroup.getPendingMembersList();
    }

    public DecryptedGroup getFullyDecryptedGroup() throws IOException {
        try {
            return this.groupsOperations.forGroup(this.groupSecretParams).decryptGroup(this.group);
        } catch (VerificationFailedException | InvalidGroupStateException e) {
            throw new IOException(e);
        }
    }
}
