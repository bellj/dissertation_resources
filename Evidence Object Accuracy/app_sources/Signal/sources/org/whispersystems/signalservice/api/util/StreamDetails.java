package org.whispersystems.signalservice.api.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import org.signal.libsignal.protocol.logging.Log;

/* loaded from: classes5.dex */
public final class StreamDetails implements Closeable {
    private static final String TAG = StreamDetails.class.getSimpleName();
    private final String contentType;
    private final long length;
    private final InputStream stream;

    public StreamDetails(InputStream inputStream, String str, long j) {
        this.stream = inputStream;
        this.contentType = str;
        this.length = j;
    }

    public InputStream getStream() {
        return this.stream;
    }

    public String getContentType() {
        return this.contentType;
    }

    public long getLength() {
        return this.length;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        try {
            this.stream.close();
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }
}
