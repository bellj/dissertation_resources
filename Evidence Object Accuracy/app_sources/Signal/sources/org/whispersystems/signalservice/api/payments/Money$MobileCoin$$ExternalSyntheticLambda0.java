package org.whispersystems.signalservice.api.payments;

import java.util.Comparator;
import org.whispersystems.signalservice.api.payments.Money;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class Money$MobileCoin$$ExternalSyntheticLambda0 implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return Money.MobileCoin.lambda$static$0((Money.MobileCoin) obj, (Money.MobileCoin) obj2);
    }
}
