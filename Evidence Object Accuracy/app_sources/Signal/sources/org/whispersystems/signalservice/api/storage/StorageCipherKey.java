package org.whispersystems.signalservice.api.storage;

/* loaded from: classes5.dex */
public interface StorageCipherKey {
    byte[] serialize();
}
