package org.whispersystems.signalservice.api.crypto;

import j$.util.Optional;
import org.signal.libsignal.metadata.certificate.SenderCertificate;
import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.signal.libsignal.protocol.message.PlaintextContent;
import org.whispersystems.signalservice.internal.push.OutgoingPushMessage;
import org.whispersystems.signalservice.internal.push.PushTransportDetails;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public interface EnvelopeContent {
    Optional<SignalServiceProtos.Content> getContent();

    OutgoingPushMessage processSealedSender(SignalSessionCipher signalSessionCipher, SignalSealedSessionCipher signalSealedSessionCipher, SignalProtocolAddress signalProtocolAddress, SenderCertificate senderCertificate) throws UntrustedIdentityException, InvalidKeyException;

    OutgoingPushMessage processUnsealedSender(SignalSessionCipher signalSessionCipher, SignalProtocolAddress signalProtocolAddress) throws UntrustedIdentityException;

    int size();

    /* renamed from: org.whispersystems.signalservice.api.crypto.EnvelopeContent$-CC */
    /* loaded from: classes5.dex */
    public final /* synthetic */ class CC {
        public static EnvelopeContent encrypted(SignalServiceProtos.Content content, ContentHint contentHint, Optional<byte[]> optional) {
            return new Encrypted(content, contentHint, optional);
        }

        public static EnvelopeContent plaintext(PlaintextContent plaintextContent, Optional<byte[]> optional) {
            return new Plaintext(plaintextContent, optional);
        }
    }

    /* loaded from: classes5.dex */
    public static class Encrypted implements EnvelopeContent {
        private final SignalServiceProtos.Content content;
        private final ContentHint contentHint;
        private final Optional<byte[]> groupId;

        public Encrypted(SignalServiceProtos.Content content, ContentHint contentHint, Optional<byte[]> optional) {
            this.content = content;
            this.contentHint = contentHint;
            this.groupId = optional;
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public OutgoingPushMessage processSealedSender(SignalSessionCipher signalSessionCipher, SignalSealedSessionCipher signalSealedSessionCipher, SignalProtocolAddress signalProtocolAddress, SenderCertificate senderCertificate) throws UntrustedIdentityException, InvalidKeyException {
            String encodeBytes = Base64.encodeBytes(signalSealedSessionCipher.encrypt(signalProtocolAddress, new UnidentifiedSenderMessageContent(signalSessionCipher.encrypt(new PushTransportDetails().getPaddedMessageBody(this.content.toByteArray())), senderCertificate, this.contentHint.getType(), this.groupId)));
            return new OutgoingPushMessage(6, signalProtocolAddress.getDeviceId(), signalSealedSessionCipher.getRemoteRegistrationId(signalProtocolAddress), encodeBytes);
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public OutgoingPushMessage processUnsealedSender(SignalSessionCipher signalSessionCipher, SignalProtocolAddress signalProtocolAddress) throws UntrustedIdentityException {
            CiphertextMessage encrypt = signalSessionCipher.encrypt(new PushTransportDetails().getPaddedMessageBody(this.content.toByteArray()));
            int remoteRegistrationId = signalSessionCipher.getRemoteRegistrationId();
            String encodeBytes = Base64.encodeBytes(encrypt.serialize());
            int type = encrypt.getType();
            int i = 3;
            if (type == 2) {
                i = 1;
            } else if (type != 3) {
                throw new AssertionError("Bad type: " + encrypt.getType());
            }
            return new OutgoingPushMessage(i, signalProtocolAddress.getDeviceId(), remoteRegistrationId, encodeBytes);
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public int size() {
            return this.content.getSerializedSize();
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public Optional<SignalServiceProtos.Content> getContent() {
            return Optional.of(this.content);
        }
    }

    /* loaded from: classes5.dex */
    public static class Plaintext implements EnvelopeContent {
        private final Optional<byte[]> groupId;
        private final PlaintextContent plaintextContent;

        public Plaintext(PlaintextContent plaintextContent, Optional<byte[]> optional) {
            this.plaintextContent = plaintextContent;
            this.groupId = optional;
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public OutgoingPushMessage processSealedSender(SignalSessionCipher signalSessionCipher, SignalSealedSessionCipher signalSealedSessionCipher, SignalProtocolAddress signalProtocolAddress, SenderCertificate senderCertificate) throws UntrustedIdentityException, InvalidKeyException {
            String encodeBytes = Base64.encodeBytes(signalSealedSessionCipher.encrypt(signalProtocolAddress, new UnidentifiedSenderMessageContent(this.plaintextContent, senderCertificate, ContentHint.IMPLICIT.getType(), this.groupId)));
            return new OutgoingPushMessage(6, signalProtocolAddress.getDeviceId(), signalSealedSessionCipher.getRemoteRegistrationId(signalProtocolAddress), encodeBytes);
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public OutgoingPushMessage processUnsealedSender(SignalSessionCipher signalSessionCipher, SignalProtocolAddress signalProtocolAddress) {
            String encodeBytes = Base64.encodeBytes(this.plaintextContent.serialize());
            return new OutgoingPushMessage(8, signalProtocolAddress.getDeviceId(), signalSessionCipher.getRemoteRegistrationId(), encodeBytes);
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public int size() {
            return this.plaintextContent.getBody().length;
        }

        @Override // org.whispersystems.signalservice.api.crypto.EnvelopeContent
        public Optional<SignalServiceProtos.Content> getContent() {
            return Optional.empty();
        }
    }
}
