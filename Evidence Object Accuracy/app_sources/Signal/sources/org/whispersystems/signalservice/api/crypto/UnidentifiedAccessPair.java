package org.whispersystems.signalservice.api.crypto;

import j$.util.Optional;

/* loaded from: classes5.dex */
public class UnidentifiedAccessPair {
    private final Optional<UnidentifiedAccess> selfUnidentifiedAccess;
    private final Optional<UnidentifiedAccess> targetUnidentifiedAccess;

    public UnidentifiedAccessPair(UnidentifiedAccess unidentifiedAccess, UnidentifiedAccess unidentifiedAccess2) {
        this.targetUnidentifiedAccess = Optional.of(unidentifiedAccess);
        this.selfUnidentifiedAccess = Optional.of(unidentifiedAccess2);
    }

    public Optional<UnidentifiedAccess> getTargetUnidentifiedAccess() {
        return this.targetUnidentifiedAccess;
    }

    public Optional<UnidentifiedAccess> getSelfUnidentifiedAccess() {
        return this.selfUnidentifiedAccess;
    }
}
