package org.whispersystems.signalservice.api.storage;

/* loaded from: classes5.dex */
public interface SignalRecord {
    SignalStorageRecord asStorageRecord();

    String describeDiff(SignalRecord signalRecord);

    StorageId getId();
}
