package org.whispersystems.signalservice.api.storage;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class SignalStorageCipher {
    private static final int IV_LENGTH;

    public static byte[] encrypt(StorageCipherKey storageCipherKey, byte[] bArr) {
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            byte[] secretBytes = Util.getSecretBytes(12);
            instance.init(1, new SecretKeySpec(storageCipherKey.serialize(), "AES"), new GCMParameterSpec(128, secretBytes));
            return Util.join(secretBytes, instance.doFinal(bArr));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] decrypt(StorageCipherKey storageCipherKey, byte[] bArr) throws org.signal.libsignal.protocol.InvalidKeyException {
        Throwable e;
        Object e2;
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            byte[][] split = Util.split(bArr, 12, bArr.length - 12);
            byte[] bArr2 = split[0];
            byte[] bArr3 = split[1];
            instance.init(2, new SecretKeySpec(storageCipherKey.serialize(), "AES"), new GCMParameterSpec(128, bArr2));
            return instance.doFinal(bArr3);
        } catch (InvalidAlgorithmParameterException e3) {
            e2 = e3;
            throw new AssertionError(e2);
        } catch (InvalidKeyException e4) {
            e = e4;
            throw new org.signal.libsignal.protocol.InvalidKeyException(e);
        } catch (NoSuchAlgorithmException e5) {
            e2 = e5;
            throw new AssertionError(e2);
        } catch (BadPaddingException e6) {
            e = e6;
            throw new org.signal.libsignal.protocol.InvalidKeyException(e);
        } catch (IllegalBlockSizeException e7) {
            e = e7;
            throw new org.signal.libsignal.protocol.InvalidKeyException(e);
        } catch (NoSuchPaddingException e8) {
            e2 = e8;
            throw new AssertionError(e2);
        }
    }
}
