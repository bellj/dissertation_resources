package org.whispersystems.signalservice.api.messages.multidevice;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.io.OutputStream;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class DeviceContactsOutputStream extends ChunkedOutputStream {
    public DeviceContactsOutputStream(OutputStream outputStream) {
        super(outputStream);
    }

    public void write(DeviceContact deviceContact) throws IOException {
        writeContactDetails(deviceContact);
        writeAvatarImage(deviceContact);
    }

    public void close() throws IOException {
        this.out.close();
    }

    private void writeAvatarImage(DeviceContact deviceContact) throws IOException {
        if (deviceContact.getAvatar().isPresent()) {
            writeStream(deviceContact.getAvatar().get().getInputStream());
        }
    }

    private void writeContactDetails(DeviceContact deviceContact) throws IOException {
        SignalServiceProtos.Verified.State state;
        SignalServiceProtos.ContactDetails.Builder newBuilder = SignalServiceProtos.ContactDetails.newBuilder();
        newBuilder.setUuid(deviceContact.getAddress().getServiceId().toString());
        if (deviceContact.getAddress().getNumber().isPresent()) {
            newBuilder.setNumber(deviceContact.getAddress().getNumber().get());
        }
        if (deviceContact.getName().isPresent()) {
            newBuilder.setName(deviceContact.getName().get());
        }
        if (deviceContact.getAvatar().isPresent()) {
            SignalServiceProtos.ContactDetails.Avatar.Builder newBuilder2 = SignalServiceProtos.ContactDetails.Avatar.newBuilder();
            newBuilder2.setContentType(deviceContact.getAvatar().get().getContentType());
            newBuilder2.setLength((int) deviceContact.getAvatar().get().getLength());
            newBuilder.setAvatar(newBuilder2);
        }
        if (deviceContact.getColor().isPresent()) {
            newBuilder.setColor(deviceContact.getColor().get());
        }
        if (deviceContact.getVerified().isPresent()) {
            int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState[deviceContact.getVerified().get().getVerified().ordinal()];
            if (i == 1) {
                state = SignalServiceProtos.Verified.State.VERIFIED;
            } else if (i != 2) {
                state = SignalServiceProtos.Verified.State.DEFAULT;
            } else {
                state = SignalServiceProtos.Verified.State.UNVERIFIED;
            }
            newBuilder.setVerified(SignalServiceProtos.Verified.newBuilder().setIdentityKey(ByteString.copyFrom(deviceContact.getVerified().get().getIdentityKey().serialize())).setDestinationUuid(deviceContact.getVerified().get().getDestination().getServiceId().toString()).setState(state).build());
        }
        if (deviceContact.getProfileKey().isPresent()) {
            newBuilder.setProfileKey(ByteString.copyFrom(deviceContact.getProfileKey().get().serialize()));
        }
        if (deviceContact.getExpirationTimer().isPresent()) {
            newBuilder.setExpireTimer(deviceContact.getExpirationTimer().get().intValue());
        }
        if (deviceContact.getInboxPosition().isPresent()) {
            newBuilder.setInboxPosition(deviceContact.getInboxPosition().get().intValue());
        }
        newBuilder.setBlocked(deviceContact.isBlocked());
        newBuilder.setArchived(deviceContact.isArchived());
        byte[] byteArray = newBuilder.build().toByteArray();
        writeVarint32(byteArray.length);
        this.out.write(byteArray);
    }

    /* renamed from: org.whispersystems.signalservice.api.messages.multidevice.DeviceContactsOutputStream$1 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState;

        static {
            int[] iArr = new int[VerifiedMessage.VerifiedState.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState = iArr;
            try {
                iArr[VerifiedMessage.VerifiedState.VERIFIED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState[VerifiedMessage.VerifiedState.UNVERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }
}
