package org.whispersystems.signalservice.api.services;

import io.reactivex.rxjava3.functions.Function;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class AttachmentService$$ExternalSyntheticLambda1 implements Function {
    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return ServiceResponse.forUnknownError((Throwable) obj);
    }
}
