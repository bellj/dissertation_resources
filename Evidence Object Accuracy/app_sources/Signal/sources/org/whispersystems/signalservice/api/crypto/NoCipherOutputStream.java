package org.whispersystems.signalservice.api.crypto;

import java.io.OutputStream;

/* loaded from: classes5.dex */
public final class NoCipherOutputStream extends DigestingOutputStream {
    public NoCipherOutputStream(OutputStream outputStream) {
        super(outputStream);
    }
}
