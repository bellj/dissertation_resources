package org.whispersystems.signalservice.api.payments;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import org.whispersystems.signalservice.api.util.Uint64RangeException;
import org.whispersystems.signalservice.api.util.Uint64Util;

/* loaded from: classes5.dex */
public abstract class Money {
    public abstract Money abs();

    public abstract Money add(Money money);

    public abstract boolean equals(Object obj);

    public abstract Currency getCurrency();

    public abstract int hashCode();

    public abstract boolean isNegative();

    public abstract boolean isPositive();

    public abstract Money negate();

    public abstract String serializeAmountString();

    public abstract Money subtract(Money money);

    public abstract String toString(Formatter formatter);

    public abstract Money toZero();

    public static MobileCoin mobileCoin(BigDecimal bigDecimal) {
        return picoMobileCoin(bigDecimal.movePointRight(12).toBigIntegerExact());
    }

    public static MobileCoin picoMobileCoin(BigInteger bigInteger) {
        if (bigInteger.signum() == 0) {
            return MobileCoin.ZERO;
        }
        return new MobileCoin(bigInteger);
    }

    public static MobileCoin picoMobileCoin(long j) {
        return picoMobileCoin(Uint64Util.uint64ToBigInteger(j));
    }

    public static Money parse(String str) throws ParseException {
        if (str != null) {
            String[] split = str.split(":");
            if (split.length != 2) {
                throw new ParseException();
            } else if (MobileCoin.CURRENCY.getCurrencyCode().equals(split[0])) {
                return picoMobileCoin(new BigInteger(split[1]));
            } else {
                throw new ParseException();
            }
        } else {
            throw new ParseException();
        }
    }

    public static Money parseOrThrow(String str) {
        try {
            return parse(str);
        } catch (ParseException e) {
            throw new AssertionError(e);
        }
    }

    public MobileCoin requireMobileCoin() {
        throw new AssertionError();
    }

    public final String serialize() {
        return getCurrency().getCurrencyCode() + ":" + serializeAmountString();
    }

    /* loaded from: classes5.dex */
    public static final class MobileCoin extends Money {
        public static final Comparator<MobileCoin> ASCENDING = new Money$MobileCoin$$ExternalSyntheticLambda0();
        public static final Currency CURRENCY = Currency.fromCodeAndPrecision("MOB", 12);
        public static final Comparator<MobileCoin> DESCENDING = new Money$MobileCoin$$ExternalSyntheticLambda1();
        public static final MobileCoin MAX_VALUE;
        private static final int PRECISION;
        public static final MobileCoin ZERO = new MobileCoin(BigInteger.ZERO);
        private final BigInteger amount;
        private final BigDecimal amountDecimal;

        @Override // org.whispersystems.signalservice.api.payments.Money
        public MobileCoin requireMobileCoin() {
            return this;
        }

        static {
            ASCENDING = new Money$MobileCoin$$ExternalSyntheticLambda0();
            DESCENDING = new Money$MobileCoin$$ExternalSyntheticLambda1();
            ZERO = new MobileCoin(BigInteger.ZERO);
            byte[] bArr = new byte[8];
            Arrays.fill(bArr, (byte) -1);
            MAX_VALUE = Money.picoMobileCoin(new BigInteger(1, bArr));
            CURRENCY = Currency.fromCodeAndPrecision("MOB", 12);
        }

        public static /* synthetic */ int lambda$static$0(MobileCoin mobileCoin, MobileCoin mobileCoin2) {
            return mobileCoin.amount.compareTo(mobileCoin2.amount);
        }

        public static /* synthetic */ int lambda$static$1(MobileCoin mobileCoin, MobileCoin mobileCoin2) {
            return mobileCoin2.amount.compareTo(mobileCoin.amount);
        }

        private MobileCoin(BigInteger bigInteger) {
            this.amount = bigInteger;
            this.amountDecimal = new BigDecimal(bigInteger).movePointLeft(12).stripTrailingZeros();
        }

        public static MobileCoin sum(Collection<MobileCoin> collection) {
            int size = collection.size();
            if (size == 0) {
                return ZERO;
            }
            if (size == 1) {
                return collection.iterator().next();
            }
            BigInteger bigInteger = ZERO.amount;
            for (MobileCoin mobileCoin : collection) {
                bigInteger = bigInteger.add(mobileCoin.amount);
            }
            return Money.picoMobileCoin(bigInteger);
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public boolean isPositive() {
            return this.amount.signum() == 1;
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public boolean isNegative() {
            return this.amount.signum() == -1;
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public MobileCoin negate() {
            return new MobileCoin(this.amount.negate());
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public MobileCoin abs() {
            return this.amount.signum() == -1 ? negate() : this;
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public Money add(Money money) {
            return new MobileCoin(this.amount.add(money.requireMobileCoin().amount));
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public Money subtract(Money money) {
            return new MobileCoin(this.amount.subtract(money.requireMobileCoin().amount));
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public Currency getCurrency() {
            return CURRENCY;
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public Money toZero() {
            return ZERO;
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public boolean equals(Object obj) {
            return (obj instanceof MobileCoin) && this.amount.equals(((MobileCoin) obj).amount);
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public int hashCode() {
            return this.amount.hashCode();
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public String serializeAmountString() {
            return toPicoMobBigInteger().toString();
        }

        public String getAmountDecimalString() {
            return this.amountDecimal.toString();
        }

        public boolean greaterThan(MobileCoin mobileCoin) {
            return this.amount.compareTo(mobileCoin.amount) > 0;
        }

        public boolean lessThan(MobileCoin mobileCoin) {
            return this.amount.compareTo(mobileCoin.amount) < 0;
        }

        @Deprecated
        public double toDouble() {
            return this.amountDecimal.doubleValue();
        }

        public BigDecimal toBigDecimal() {
            return this.amountDecimal;
        }

        public BigInteger toPicoMobBigInteger() {
            return this.amount;
        }

        public long toPicoMobUint64() throws Uint64RangeException {
            return Uint64Util.bigIntegerToUInt64(this.amount);
        }

        @Override // org.whispersystems.signalservice.api.payments.Money
        public String toString(Formatter formatter) {
            return formatter.format(this.amountDecimal);
        }
    }

    public String toString() {
        return serialize();
    }

    public final String toString(FormatterOptions formatterOptions) {
        return toString(getCurrency().getFormatter(formatterOptions));
    }

    /* loaded from: classes5.dex */
    public static final class ParseException extends Exception {
        private ParseException() {
        }
    }
}
