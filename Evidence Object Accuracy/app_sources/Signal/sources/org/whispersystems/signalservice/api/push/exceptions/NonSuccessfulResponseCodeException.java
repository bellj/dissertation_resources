package org.whispersystems.signalservice.api.push.exceptions;

import java.io.IOException;

/* loaded from: classes5.dex */
public class NonSuccessfulResponseCodeException extends IOException {
    private final int code;

    public NonSuccessfulResponseCodeException(int i) {
        super("StatusCode: " + i);
        this.code = i;
    }

    public NonSuccessfulResponseCodeException(int i, String str) {
        super("[" + i + "] " + str);
        this.code = i;
    }

    public int getCode() {
        return this.code;
    }

    public boolean is5xx() {
        int i = this.code;
        return i >= 500 && i < 600;
    }
}
