package org.whispersystems.signalservice.api.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import j$.util.Optional;
import java.util.Locale;
import java.util.regex.Pattern;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class PhoneNumberFormatter {
    private static final String COUNTRY_CODE_BR;
    private static final String COUNTRY_CODE_US;
    private static final String TAG;

    public static boolean isValidNumber(String str, String str2) {
        if (!PhoneNumberUtil.getInstance().isPossibleNumber(str, str2)) {
            Log.w(TAG, "Failed isPossibleNumber()");
            return false;
        } else if ("1".equals(str2) && !Pattern.matches("^\\+1[0-9]{10}$", str)) {
            Log.w(TAG, "Failed US number format check");
            return false;
        } else if (!COUNTRY_CODE_BR.equals(str2) || Pattern.matches("^\\+55[0-9]{2}9?[0-9]{8}$", str)) {
            return str.matches("^\\+[1-9][0-9]{6,14}$");
        } else {
            Log.w(TAG, "Failed Brazil number format check");
            return false;
        }
    }

    private static String impreciseFormatNumber(String str, String str2) throws InvalidNumberException {
        String replaceAll = str.replaceAll("[^0-9+]", "");
        if (replaceAll.charAt(0) == '+') {
            return replaceAll;
        }
        if (str2.charAt(0) == '+') {
            str2 = str2.substring(1);
        }
        if (str2.length() == replaceAll.length() || replaceAll.length() > str2.length()) {
            return "+" + replaceAll;
        }
        int length = str2.length() - replaceAll.length();
        return "+" + str2.substring(0, length) + replaceAll;
    }

    public static String formatNumberInternational(String str) {
        try {
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            return instance.format(instance.parse(str, null), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException e) {
            Log.w(TAG, e);
            return str;
        }
    }

    public static String formatNumber(String str, String str2) throws InvalidNumberException {
        if (str == null) {
            throw new InvalidNumberException("Null String passed as number.");
        } else if (!str.contains("@")) {
            String replaceAll = str.replaceAll("[^0-9+]", "");
            if (replaceAll.length() != 0) {
                try {
                    PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
                    String regionCodeForNumber = instance.getRegionCodeForNumber(instance.parse(str2, null));
                    String str3 = TAG;
                    Log.w(str3, "Got local CC: " + regionCodeForNumber);
                    return instance.format(instance.parse(replaceAll, regionCodeForNumber), PhoneNumberUtil.PhoneNumberFormat.E164);
                } catch (NumberParseException e) {
                    Log.w(TAG, e);
                    return impreciseFormatNumber(replaceAll, str2);
                }
            } else {
                throw new InvalidNumberException("No valid characters found.");
            }
        } else {
            throw new InvalidNumberException("Possible attempt to use email address.");
        }
    }

    @Deprecated
    public static String getRegionDisplayNameLegacy(String str) {
        return getRegionDisplayName(str).orElse("Unknown country");
    }

    public static Optional<String> getRegionDisplayName(String str) {
        if (str != null && !str.equals("ZZ") && !str.equals("001")) {
            String displayCountry = new Locale("", str).getDisplayCountry(Locale.getDefault());
            if (!Util.isEmpty(displayCountry)) {
                return Optional.of(displayCountry);
            }
        }
        return Optional.empty();
    }

    public static String formatE164(String str, String str2) {
        try {
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            return instance.format(instance.parse(str2, instance.getRegionCodeForCountryCode(Integer.parseInt(str))), PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (NumberParseException | NumberFormatException e) {
            Log.w(TAG, e);
            StringBuilder sb = new StringBuilder();
            sb.append("+");
            String str3 = "";
            sb.append(str.replaceAll("[^0-9]", str3).replaceAll("^0*", str3));
            if (str2 != null) {
                str3 = str2.replaceAll("[^0-9]", str3);
            }
            sb.append(str3);
            return sb.toString();
        }
    }

    public static String getInternationalFormatFromE164(String str) {
        try {
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            return instance.format(instance.parse(str, null), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException e) {
            Log.w(TAG, e);
            return str;
        }
    }
}
