package org.whispersystems.signalservice.api;

import java.util.Collection;
import java.util.Set;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.state.SenderKeyStore;
import org.whispersystems.signalservice.api.push.DistributionId;

/* loaded from: classes5.dex */
public interface SignalServiceSenderKeyStore extends SenderKeyStore {
    void clearSenderKeySharedWith(Collection<SignalProtocolAddress> collection);

    Set<SignalProtocolAddress> getSenderKeySharedWith(DistributionId distributionId);

    void markSenderKeySharedWith(DistributionId distributionId, Collection<SignalProtocolAddress> collection);
}
