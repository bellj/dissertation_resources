package org.whispersystems.signalservice.api.kbs;

/* loaded from: classes5.dex */
public final class KbsData {
    private final byte[] cipherText;
    private final byte[] kbsAccessKey;
    private final MasterKey masterKey;

    public KbsData(MasterKey masterKey, byte[] bArr, byte[] bArr2) {
        this.masterKey = masterKey;
        this.kbsAccessKey = bArr;
        this.cipherText = bArr2;
    }

    public MasterKey getMasterKey() {
        return this.masterKey;
    }

    public byte[] getKbsAccessKey() {
        return this.kbsAccessKey;
    }

    public byte[] getCipherText() {
        return this.cipherText;
    }
}
