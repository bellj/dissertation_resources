package org.whispersystems.signalservice.api.messages;

import java.util.List;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes5.dex */
public class SignalServiceStoryMessageRecipient {
    private final List<String> distributionListIds;
    private final boolean isAllowedToReply;
    private final SignalServiceAddress signalServiceAddress;

    public SignalServiceStoryMessageRecipient(SignalServiceAddress signalServiceAddress, List<String> list, boolean z) {
        this.signalServiceAddress = signalServiceAddress;
        this.distributionListIds = list;
        this.isAllowedToReply = z;
    }

    public List<String> getDistributionListIds() {
        return this.distributionListIds;
    }

    public SignalServiceAddress getSignalServiceAddress() {
        return this.signalServiceAddress;
    }

    public boolean isAllowedToReply() {
        return this.isAllowedToReply;
    }
}
