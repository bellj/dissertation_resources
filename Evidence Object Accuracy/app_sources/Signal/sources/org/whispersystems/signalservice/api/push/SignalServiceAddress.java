package org.whispersystems.signalservice.api.push;

import j$.util.Optional;
import java.util.Objects;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public class SignalServiceAddress {
    public static final int DEFAULT_DEVICE_ID;
    private final Optional<String> e164;
    private final ServiceId serviceId;

    public SignalServiceAddress(ServiceId serviceId, Optional<String> optional) {
        this.serviceId = (ServiceId) Preconditions.checkNotNull(serviceId);
        this.e164 = optional;
    }

    public SignalServiceAddress(ServiceId serviceId) {
        this.serviceId = (ServiceId) Preconditions.checkNotNull(serviceId);
        this.e164 = Optional.empty();
    }

    public SignalServiceAddress(ServiceId serviceId, String str) {
        this(serviceId, OptionalUtil.absentIfEmpty(str));
    }

    public Optional<String> getNumber() {
        return this.e164;
    }

    public ServiceId getServiceId() {
        return this.serviceId;
    }

    public boolean hasValidServiceId() {
        return !this.serviceId.isUnknown();
    }

    public String getIdentifier() {
        return this.serviceId.toString();
    }

    public boolean matches(SignalServiceAddress signalServiceAddress) {
        return this.serviceId.equals(signalServiceAddress.serviceId);
    }

    public static boolean isValidAddress(String str) {
        return isValidAddress(str, null);
    }

    public static boolean isValidAddress(String str, String str2) {
        return UuidUtil.parseOrNull(str) != null;
    }

    public static Optional<SignalServiceAddress> fromRaw(String str, String str2) {
        if (isValidAddress(str, str2)) {
            return Optional.of(new SignalServiceAddress(ServiceId.parseOrThrow(str), str2));
        }
        return Optional.empty();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SignalServiceAddress signalServiceAddress = (SignalServiceAddress) obj;
        if (!this.serviceId.equals(signalServiceAddress.serviceId) || !this.e164.equals(signalServiceAddress.e164)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.serviceId, this.e164);
    }
}
