package org.whispersystems.signalservice.api;

import com.google.protobuf.ByteString;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.function.Supplier;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidRegistrationIdException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SessionBuilder;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.GroupSessionBuilder;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.message.DecryptionErrorMessage;
import org.signal.libsignal.protocol.message.PlaintextContent;
import org.signal.libsignal.protocol.message.SenderKeyDistributionMessage;
import org.signal.libsignal.protocol.state.PreKeyBundle;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.thoughtcrime.securesms.profiles.manage.EditAboutFragment;
import org.thoughtcrime.securesms.storage.StorageSyncModels$$ExternalSyntheticLambda0;
import org.whispersystems.signalservice.api.crypto.AttachmentCipherOutputStream;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.EnvelopeContent;
import org.whispersystems.signalservice.api.crypto.SignalGroupSessionBuilder;
import org.whispersystems.signalservice.api.crypto.SignalServiceCipher;
import org.whispersystems.signalservice.api.crypto.SignalSessionBuilder;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupV2;
import org.whispersystems.signalservice.api.messages.SignalServicePreview;
import org.whispersystems.signalservice.api.messages.SignalServiceReceiptMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessageRecipient;
import org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceTypingMessage;
import org.whispersystems.signalservice.api.messages.calls.AnswerMessage;
import org.whispersystems.signalservice.api.messages.calls.CallingResponse;
import org.whispersystems.signalservice.api.messages.calls.IceUpdateMessage;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.messages.calls.OpaqueMessage;
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage;
import org.whispersystems.signalservice.api.messages.multidevice.BlockedListMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ConfigurationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.KeysMessage;
import org.whispersystems.signalservice.api.messages.multidevice.MessageRequestResponseMessage;
import org.whispersystems.signalservice.api.messages.multidevice.OutgoingPaymentMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ReadMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SentTranscriptMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.StickerPackOperationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewOnceOpenMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewedMessage;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.RateLimitException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;
import org.whispersystems.signalservice.api.services.AttachmentService;
import org.whispersystems.signalservice.api.services.MessagingService;
import org.whispersystems.signalservice.api.util.AttachmentPointerUtil;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.Uint64RangeException;
import org.whispersystems.signalservice.api.util.Uint64Util;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.crypto.PaddingInputStream;
import org.whispersystems.signalservice.internal.push.MismatchedDevices;
import org.whispersystems.signalservice.internal.push.OutgoingPushMessage;
import org.whispersystems.signalservice.internal.push.OutgoingPushMessageList;
import org.whispersystems.signalservice.internal.push.PushAttachmentData;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.SendGroupMessageResponse;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.push.StaleDevices;
import org.whispersystems.signalservice.internal.push.http.AttachmentCipherOutputStreamFactory;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.PartialSendCompleteListener;
import org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public class SignalServiceMessageSender {
    private static final int RETRY_COUNT;
    private static final String TAG;
    private final AttachmentService attachmentService;
    private final Optional<EventListener> eventListener;
    private final ExecutorService executor;
    private final SignalServiceAddress localAddress;
    private final int localDeviceId;
    private final long maxEnvelopeSize;
    private final MessagingService messagingService;
    private final SignalSessionLock sessionLock;
    private final PushServiceSocket socket;
    private final SignalServiceAccountDataStore store;

    /* loaded from: classes5.dex */
    public interface EventListener {
        void onSecurityEvent(SignalServiceAddress signalServiceAddress);
    }

    /* loaded from: classes5.dex */
    public interface IndividualSendEvents {
        public static final IndividualSendEvents EMPTY = new IndividualSendEvents() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents.1
            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents
            public void onMessageEncrypted() {
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents
            public void onMessageSent() {
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.IndividualSendEvents
            public void onSyncMessageSent() {
            }
        };

        void onMessageEncrypted();

        void onMessageSent();

        void onSyncMessageSent();
    }

    /* loaded from: classes5.dex */
    public interface LegacyGroupEvents {
        public static final LegacyGroupEvents EMPTY = new LegacyGroupEvents() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender.LegacyGroupEvents.1
            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.LegacyGroupEvents
            public void onMessageSent() {
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.LegacyGroupEvents
            public void onSyncMessageSent() {
            }
        };

        void onMessageSent();

        void onSyncMessageSent();
    }

    /* loaded from: classes5.dex */
    public interface SenderKeyGroupEvents {
        public static final SenderKeyGroupEvents EMPTY = new SenderKeyGroupEvents() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents.1
            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
            public void onMessageEncrypted() {
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
            public void onMessageSent() {
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
            public void onSenderKeyShared() {
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
            public void onSyncMessageSent() {
            }
        };

        void onMessageEncrypted();

        void onMessageSent();

        void onSenderKeyShared();

        void onSyncMessageSent();
    }

    public SignalServiceMessageSender(SignalServiceConfiguration signalServiceConfiguration, CredentialsProvider credentialsProvider, SignalServiceDataStore signalServiceDataStore, SignalSessionLock signalSessionLock, String str, SignalWebSocket signalWebSocket, Optional<EventListener> optional, ClientZkProfileOperations clientZkProfileOperations, ExecutorService executorService, long j, boolean z) {
        ExecutorService executorService2;
        this.socket = new PushServiceSocket(signalServiceConfiguration, credentialsProvider, str, clientZkProfileOperations, z);
        this.store = signalServiceDataStore.aci();
        this.sessionLock = signalSessionLock;
        this.localAddress = new SignalServiceAddress(credentialsProvider.getAci(), credentialsProvider.getE164());
        this.localDeviceId = credentialsProvider.getDeviceId();
        this.attachmentService = new AttachmentService(signalWebSocket);
        this.messagingService = new MessagingService(signalWebSocket);
        this.eventListener = optional;
        if (executorService != null) {
            executorService2 = executorService;
        } else {
            executorService2 = Executors.newSingleThreadExecutor();
        }
        this.executor = executorService2;
        this.maxEnvelopeSize = j;
    }

    public SendMessageResult sendReceipt(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccessPair> optional, SignalServiceReceiptMessage signalServiceReceiptMessage) throws IOException, UntrustedIdentityException {
        return sendMessage(signalServiceAddress, getTargetUnidentifiedAccess(optional), signalServiceReceiptMessage.getWhen(), EnvelopeContent.CC.encrypted(createReceiptContent(signalServiceReceiptMessage), ContentHint.IMPLICIT, Optional.empty()), false, null);
    }

    public void sendRetryReceipt(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccessPair> optional, Optional<byte[]> optional2, DecryptionErrorMessage decryptionErrorMessage) throws IOException, UntrustedIdentityException {
        sendMessage(signalServiceAddress, getTargetUnidentifiedAccess(optional), System.currentTimeMillis(), EnvelopeContent.CC.plaintext(new PlaintextContent(decryptionErrorMessage), optional2), false, null);
    }

    public void sendTyping(List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, SignalServiceTypingMessage signalServiceTypingMessage, CancelationSignal cancelationSignal) throws IOException {
        sendMessage(list, getTargetUnidentifiedAccess(list2), signalServiceTypingMessage.getTimestamp(), EnvelopeContent.CC.encrypted(createTypingContent(signalServiceTypingMessage), ContentHint.IMPLICIT, Optional.empty()), true, null, cancelationSignal);
    }

    public void sendGroupTyping(DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, SignalServiceTypingMessage signalServiceTypingMessage) throws IOException, UntrustedIdentityException, InvalidKeyException, NoSessionException, InvalidRegistrationIdException {
        sendGroupMessage(distributionId, list, list2, signalServiceTypingMessage.getTimestamp(), createTypingContent(signalServiceTypingMessage), ContentHint.IMPLICIT, signalServiceTypingMessage.getGroupId(), true, SenderKeyGroupEvents.EMPTY);
    }

    public List<SendMessageResult> sendStory(List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, SignalServiceStoryMessage signalServiceStoryMessage, long j, Set<SignalServiceStoryMessageRecipient> set) throws IOException, UntrustedIdentityException {
        List<SendMessageResult> sendMessage = sendMessage(list, getTargetUnidentifiedAccess(list2), j, EnvelopeContent.CC.encrypted(createStoryContent(signalServiceStoryMessage), ContentHint.RESENDABLE, Optional.empty()), false, null, null);
        sendSyncMessage(createSelfSendSyncMessageForStory(signalServiceStoryMessage, j, set), Optional.empty());
        return sendMessage;
    }

    public List<SendMessageResult> sendGroupStory(DistributionId distributionId, Optional<byte[]> optional, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, SignalServiceStoryMessage signalServiceStoryMessage, long j, Set<SignalServiceStoryMessageRecipient> set) throws IOException, UntrustedIdentityException, InvalidKeyException, NoSessionException, InvalidRegistrationIdException {
        List<SendMessageResult> sendGroupMessage = sendGroupMessage(distributionId, list, list2, j, createStoryContent(signalServiceStoryMessage), ContentHint.RESENDABLE, optional, false, SenderKeyGroupEvents.EMPTY);
        sendSyncMessage(createSelfSendSyncMessageForStory(signalServiceStoryMessage, j, set), Optional.empty());
        return sendGroupMessage;
    }

    public void sendCallMessage(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccessPair> optional, SignalServiceCallMessage signalServiceCallMessage) throws IOException, UntrustedIdentityException {
        sendMessage(signalServiceAddress, getTargetUnidentifiedAccess(optional), System.currentTimeMillis(), EnvelopeContent.CC.encrypted(createCallContent(signalServiceCallMessage), ContentHint.DEFAULT, Optional.empty()), false, null);
    }

    public List<SendMessageResult> sendCallMessage(List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, SignalServiceCallMessage signalServiceCallMessage) throws IOException {
        return sendMessage(list, getTargetUnidentifiedAccess(list2), System.currentTimeMillis(), EnvelopeContent.CC.encrypted(createCallContent(signalServiceCallMessage), ContentHint.DEFAULT, Optional.empty()), false, null, null);
    }

    public List<SendMessageResult> sendCallMessage(DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, SignalServiceCallMessage signalServiceCallMessage) throws IOException, UntrustedIdentityException, InvalidKeyException, NoSessionException, InvalidRegistrationIdException {
        return sendGroupMessage(distributionId, list, list2, signalServiceCallMessage.getTimestamp().get().longValue(), createCallContent(signalServiceCallMessage), ContentHint.IMPLICIT, signalServiceCallMessage.getGroupId(), false, SenderKeyGroupEvents.EMPTY);
    }

    public CallingResponse makeCallingRequest(long j, String str, String str2, List<Pair<String, String>> list, byte[] bArr) {
        return this.socket.makeCallingRequest(j, str, str2, list, bArr);
    }

    public SendMessageResult sendDataMessage(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccessPair> optional, ContentHint contentHint, SignalServiceDataMessage signalServiceDataMessage, IndividualSendEvents individualSendEvents) throws UntrustedIdentityException, IOException {
        String str = TAG;
        Log.d(str, "[" + signalServiceDataMessage.getTimestamp() + "] Sending a data message.");
        SignalServiceProtos.Content createMessageContent = createMessageContent(signalServiceDataMessage);
        EnvelopeContent encrypted = EnvelopeContent.CC.encrypted(createMessageContent, contentHint, signalServiceDataMessage.getGroupId());
        individualSendEvents.onMessageEncrypted();
        long timestamp = signalServiceDataMessage.getTimestamp();
        SendMessageResult sendMessage = sendMessage(signalServiceAddress, getTargetUnidentifiedAccess(optional), timestamp, encrypted, false, null);
        individualSendEvents.onMessageSent();
        if (sendMessage.getSuccess() != null && sendMessage.getSuccess().isNeedsSync()) {
            sendMessage(this.localAddress, Optional.empty(), timestamp, EnvelopeContent.CC.encrypted(createMultiDeviceSentTranscriptContent(createMessageContent, Optional.of(signalServiceAddress), timestamp, Collections.singletonList(sendMessage), false, Collections.emptySet()), ContentHint.IMPLICIT, Optional.empty()), false, null);
        }
        individualSendEvents.onSyncMessageSent();
        return sendMessage;
    }

    public SenderKeyDistributionMessage getOrCreateNewGroupSession(DistributionId distributionId) {
        return new SignalGroupSessionBuilder(this.sessionLock, new GroupSessionBuilder(this.store)).create(new SignalProtocolAddress(this.localAddress.getIdentifier(), this.localDeviceId), distributionId.asUuid());
    }

    public List<SendMessageResult> sendSenderKeyDistributionMessage(DistributionId distributionId, List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, SenderKeyDistributionMessage senderKeyDistributionMessage, Optional<byte[]> optional) throws IOException {
        EnvelopeContent encrypted = EnvelopeContent.CC.encrypted(SignalServiceProtos.Content.newBuilder().setSenderKeyDistributionMessage(ByteString.copyFrom(senderKeyDistributionMessage.serialize())).build(), ContentHint.IMPLICIT, optional);
        long currentTimeMillis = System.currentTimeMillis();
        String str = TAG;
        Log.d(str, "[" + currentTimeMillis + "] Sending SKDM to " + list.size() + " recipients for DistributionId " + distributionId);
        return sendMessage(list, getTargetUnidentifiedAccess(list2), currentTimeMillis, encrypted, false, null, null);
    }

    public void processSenderKeyDistributionMessage(SignalProtocolAddress signalProtocolAddress, SenderKeyDistributionMessage senderKeyDistributionMessage) {
        new SignalGroupSessionBuilder(this.sessionLock, new GroupSessionBuilder(this.store)).process(signalProtocolAddress, senderKeyDistributionMessage);
    }

    public SendMessageResult resendContent(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccessPair> optional, long j, SignalServiceProtos.Content content, ContentHint contentHint, Optional<byte[]> optional2) throws UntrustedIdentityException, IOException {
        return sendMessage(signalServiceAddress, optional.isPresent() ? optional.get().getTargetUnidentifiedAccess() : Optional.empty(), j, EnvelopeContent.CC.encrypted(content, contentHint, optional2), false, null);
    }

    public List<SendMessageResult> sendGroupDataMessage(DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, boolean z, ContentHint contentHint, SignalServiceDataMessage signalServiceDataMessage, SenderKeyGroupEvents senderKeyGroupEvents) throws IOException, UntrustedIdentityException, NoSessionException, InvalidKeyException, InvalidRegistrationIdException {
        String str = TAG;
        Log.d(str, "[" + signalServiceDataMessage.getTimestamp() + "] Sending a group data message to " + list.size() + " recipients using DistributionId " + distributionId);
        SignalServiceProtos.Content createMessageContent = createMessageContent(signalServiceDataMessage);
        List<SendMessageResult> sendGroupMessage = sendGroupMessage(distributionId, list, list2, signalServiceDataMessage.getTimestamp(), createMessageContent, contentHint, signalServiceDataMessage.getGroupId(), false, senderKeyGroupEvents);
        senderKeyGroupEvents.onMessageSent();
        if (this.store.isMultiDevice()) {
            sendMessage(this.localAddress, Optional.empty(), signalServiceDataMessage.getTimestamp(), EnvelopeContent.CC.encrypted(createMultiDeviceSentTranscriptContent(createMessageContent, Optional.empty(), signalServiceDataMessage.getTimestamp(), sendGroupMessage, z, Collections.emptySet()), ContentHint.IMPLICIT, Optional.empty()), false, null);
        }
        senderKeyGroupEvents.onSyncMessageSent();
        return sendGroupMessage;
    }

    public List<SendMessageResult> sendDataMessage(List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, boolean z, ContentHint contentHint, SignalServiceDataMessage signalServiceDataMessage, LegacyGroupEvents legacyGroupEvents, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException, UntrustedIdentityException {
        boolean z2;
        String str = TAG;
        Log.d(str, "[" + signalServiceDataMessage.getTimestamp() + "] Sending a data message to " + list.size() + " recipients.");
        SignalServiceProtos.Content createMessageContent = createMessageContent(signalServiceDataMessage);
        EnvelopeContent encrypted = EnvelopeContent.CC.encrypted(createMessageContent, contentHint, signalServiceDataMessage.getGroupId());
        long timestamp = signalServiceDataMessage.getTimestamp();
        List<SendMessageResult> sendMessage = sendMessage(list, getTargetUnidentifiedAccess(list2), timestamp, encrypted, false, partialSendCompleteListener, cancelationSignal);
        legacyGroupEvents.onMessageSent();
        Iterator<SendMessageResult> it = sendMessage.iterator();
        while (true) {
            if (!it.hasNext()) {
                z2 = false;
                break;
            }
            SendMessageResult next = it.next();
            if (next.getSuccess() != null && next.getSuccess().isNeedsSync()) {
                z2 = true;
                break;
            }
        }
        if (z2 || this.store.isMultiDevice()) {
            Optional<SignalServiceAddress> empty = Optional.empty();
            if (!signalServiceDataMessage.getGroupContext().isPresent() && list.size() == 1) {
                empty = Optional.of(list.get(0));
            }
            sendMessage(this.localAddress, Optional.empty(), timestamp, EnvelopeContent.CC.encrypted(createMultiDeviceSentTranscriptContent(createMessageContent, empty, timestamp, sendMessage, z, Collections.emptySet()), ContentHint.IMPLICIT, Optional.empty()), false, null);
        }
        legacyGroupEvents.onSyncMessageSent();
        return sendMessage;
    }

    public SendMessageResult sendSyncMessage(SignalServiceDataMessage signalServiceDataMessage) throws IOException, UntrustedIdentityException {
        return sendSyncMessage(createSelfSendSyncMessage(signalServiceDataMessage), Optional.empty());
    }

    public SendMessageResult sendSyncMessage(SignalServiceSyncMessage signalServiceSyncMessage, Optional<UnidentifiedAccessPair> optional) throws IOException, UntrustedIdentityException {
        SignalServiceProtos.Content content;
        long j;
        if (signalServiceSyncMessage.getContacts().isPresent()) {
            content = createMultiDeviceContactsContent(signalServiceSyncMessage.getContacts().get().getContactsStream().asStream(), signalServiceSyncMessage.getContacts().get().isComplete());
        } else if (signalServiceSyncMessage.getGroups().isPresent()) {
            content = createMultiDeviceGroupsContent(signalServiceSyncMessage.getGroups().get().asStream());
        } else if (signalServiceSyncMessage.getRead().isPresent()) {
            content = createMultiDeviceReadContent(signalServiceSyncMessage.getRead().get());
        } else if (signalServiceSyncMessage.getViewed().isPresent()) {
            content = createMultiDeviceViewedContent(signalServiceSyncMessage.getViewed().get());
        } else if (signalServiceSyncMessage.getViewOnceOpen().isPresent()) {
            content = createMultiDeviceViewOnceOpenContent(signalServiceSyncMessage.getViewOnceOpen().get());
        } else if (signalServiceSyncMessage.getBlockedList().isPresent()) {
            content = createMultiDeviceBlockedContent(signalServiceSyncMessage.getBlockedList().get());
        } else if (signalServiceSyncMessage.getConfiguration().isPresent()) {
            content = createMultiDeviceConfigurationContent(signalServiceSyncMessage.getConfiguration().get());
        } else if (signalServiceSyncMessage.getSent().isPresent()) {
            content = createMultiDeviceSentTranscriptContent(signalServiceSyncMessage.getSent().get(), optional.isPresent());
        } else if (signalServiceSyncMessage.getStickerPackOperations().isPresent()) {
            content = createMultiDeviceStickerPackOperationContent(signalServiceSyncMessage.getStickerPackOperations().get());
        } else if (signalServiceSyncMessage.getFetchType().isPresent()) {
            content = createMultiDeviceFetchTypeContent(signalServiceSyncMessage.getFetchType().get());
        } else if (signalServiceSyncMessage.getMessageRequestResponse().isPresent()) {
            content = createMultiDeviceMessageRequestResponseContent(signalServiceSyncMessage.getMessageRequestResponse().get());
        } else if (signalServiceSyncMessage.getOutgoingPaymentMessage().isPresent()) {
            content = createMultiDeviceOutgoingPaymentContent(signalServiceSyncMessage.getOutgoingPaymentMessage().get());
        } else if (signalServiceSyncMessage.getKeys().isPresent()) {
            content = createMultiDeviceSyncKeysContent(signalServiceSyncMessage.getKeys().get());
        } else if (signalServiceSyncMessage.getVerified().isPresent()) {
            return sendVerifiedSyncMessage(signalServiceSyncMessage.getVerified().get());
        } else {
            if (signalServiceSyncMessage.getRequest().isPresent()) {
                content = createRequestContent(signalServiceSyncMessage.getRequest().get().getRequest());
            } else if (signalServiceSyncMessage.getPniIdentity().isPresent()) {
                content = createPniIdentityContent(signalServiceSyncMessage.getPniIdentity().get());
            } else {
                throw new IOException("Unsupported sync message!");
            }
        }
        if (signalServiceSyncMessage.getSent().isPresent()) {
            j = signalServiceSyncMessage.getSent().get().getTimestamp();
        } else {
            j = System.currentTimeMillis();
        }
        return sendMessage(this.localAddress, Optional.empty(), j, EnvelopeContent.CC.encrypted(content, ContentHint.IMPLICIT, Optional.empty()), false, null);
    }

    public void setSoTimeoutMillis(long j) {
        this.socket.setSoTimeoutMillis(j);
    }

    public void cancelInFlightRequests() {
        this.socket.cancelInFlightRequests();
    }

    public SignalServiceAttachmentPointer uploadAttachment(SignalServiceAttachmentStream signalServiceAttachmentStream) throws IOException {
        byte[] bArr = (byte[]) signalServiceAttachmentStream.getResumableUploadSpec().map(new Function() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((ResumableUploadSpec) obj).getSecretKey();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElseGet(new Supplier() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda1
            @Override // j$.util.function.Supplier
            public final Object get() {
                return Util.getSecretBytes(64);
            }
        });
        long paddedSize = PaddingInputStream.getPaddedSize(signalServiceAttachmentStream.getLength());
        PushAttachmentData pushAttachmentData = new PushAttachmentData(signalServiceAttachmentStream.getContentType(), new PaddingInputStream(signalServiceAttachmentStream.getInputStream(), signalServiceAttachmentStream.getLength()), AttachmentCipherOutputStream.getCiphertextLength(paddedSize), new AttachmentCipherOutputStreamFactory(bArr, (byte[]) signalServiceAttachmentStream.getResumableUploadSpec().map(new Function() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((ResumableUploadSpec) obj).getIV();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElseGet(new Supplier() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda3
            @Override // j$.util.function.Supplier
            public final Object get() {
                return Util.getSecretBytes(16);
            }
        })), signalServiceAttachmentStream.getListener(), signalServiceAttachmentStream.getCancelationSignal(), signalServiceAttachmentStream.getResumableUploadSpec().orElse(null));
        if (signalServiceAttachmentStream.getResumableUploadSpec().isPresent()) {
            return uploadAttachmentV3(signalServiceAttachmentStream, bArr, pushAttachmentData);
        }
        return uploadAttachmentV2(signalServiceAttachmentStream, bArr, pushAttachmentData);
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer uploadAttachmentV2(org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream r22, byte[] r23, org.whispersystems.signalservice.internal.push.PushAttachmentData r24) throws org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException, org.whispersystems.signalservice.api.push.exceptions.PushNetworkException, org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException {
        /*
            r21 = this;
            r1 = r21
            java.lang.String r0 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.String r2 = "Using pipe to retrieve attachment upload attributes..."
            org.signal.libsignal.protocol.logging.Log.d(r0, r2)
            org.whispersystems.signalservice.api.services.AttachmentService$AttachmentAttributesResponseProcessor r0 = new org.whispersystems.signalservice.api.services.AttachmentService$AttachmentAttributesResponseProcessor     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            org.whispersystems.signalservice.api.services.AttachmentService r2 = r1.attachmentService     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            io.reactivex.rxjava3.core.Single r2 = r2.getAttachmentV2UploadAttributes()     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            java.lang.Object r2 = r2.blockingGet()     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            org.whispersystems.signalservice.internal.ServiceResponse r2 = (org.whispersystems.signalservice.internal.ServiceResponse) r2     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            r0.<init>(r2)     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            java.lang.Object r0 = r0.getResultOrThrow()     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            org.whispersystems.signalservice.internal.push.AttachmentV2UploadAttributes r0 = (org.whispersystems.signalservice.internal.push.AttachmentV2UploadAttributes) r0     // Catch: WebSocketUnavailableException -> 0x0029, IOException -> 0x0021
            goto L_0x0058
        L_0x0021:
            java.lang.String r0 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.String r2 = "Failed to retrieve attachment upload attributes using pipe. Falling back..."
            org.signal.libsignal.protocol.logging.Log.w(r0, r2)
            goto L_0x0057
        L_0x0029:
            r0 = move-exception
            java.lang.String r2 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "[uploadAttachmentV2] Pipe unavailable, falling back... ("
            r3.append(r4)
            java.lang.Class<org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException> r4 = org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException.class
            java.lang.String r4 = r4.getSimpleName()
            r3.append(r4)
            java.lang.String r4 = ": "
            r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = ")"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            org.signal.libsignal.protocol.logging.Log.w(r2, r0)
        L_0x0057:
            r0 = 0
        L_0x0058:
            if (r0 != 0) goto L_0x0067
            java.lang.String r0 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.String r2 = "Not using pipe to retrieve attachment upload attributes..."
            org.signal.libsignal.protocol.logging.Log.d(r0, r2)
            org.whispersystems.signalservice.internal.push.PushServiceSocket r0 = r1.socket
            org.whispersystems.signalservice.internal.push.AttachmentV2UploadAttributes r0 = r0.getAttachmentV2UploadAttributes()
        L_0x0067:
            org.whispersystems.signalservice.internal.push.PushServiceSocket r2 = r1.socket
            r3 = r24
            org.signal.libsignal.protocol.util.Pair r0 = r2.uploadAttachment(r3, r0)
            org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer r20 = new org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer
            r2 = r20
            r3 = 0
            org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId r5 = new org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId
            r4 = r5
            java.lang.Object r6 = r0.first()
            java.lang.Long r6 = (java.lang.Long) r6
            long r6 = r6.longValue()
            r5.<init>(r6)
            java.lang.String r5 = r22.getContentType()
            long r6 = r22.getLength()
            int r6 = org.whispersystems.signalservice.internal.util.Util.toIntExact(r6)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            j$.util.Optional r7 = j$.util.Optional.of(r6)
            j$.util.Optional r8 = r22.getPreview()
            int r9 = r22.getWidth()
            int r10 = r22.getHeight()
            java.lang.Object r0 = r0.second()
            byte[] r0 = (byte[]) r0
            j$.util.Optional r11 = j$.util.Optional.of(r0)
            j$.util.Optional r12 = r22.getFileName()
            boolean r13 = r22.getVoiceNote()
            boolean r14 = r22.isBorderless()
            boolean r15 = r22.isGif()
            j$.util.Optional r16 = r22.getCaption()
            j$.util.Optional r17 = r22.getBlurHash()
            long r18 = r22.getUploadTimestamp()
            r6 = r23
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
            return r20
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.SignalServiceMessageSender.uploadAttachmentV2(org.whispersystems.signalservice.api.messages.SignalServiceAttachmentStream, byte[], org.whispersystems.signalservice.internal.push.PushAttachmentData):org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec getResumableUploadSpec() throws java.io.IOException {
        /*
            r10 = this;
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.String r2 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.String r3 = "Using pipe to retrieve attachment upload attributes..."
            org.signal.libsignal.protocol.logging.Log.d(r2, r3)
            org.whispersystems.signalservice.api.services.AttachmentService$AttachmentAttributesResponseProcessor r2 = new org.whispersystems.signalservice.api.services.AttachmentService$AttachmentAttributesResponseProcessor     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            org.whispersystems.signalservice.api.services.AttachmentService r3 = r10.attachmentService     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            io.reactivex.rxjava3.core.Single r3 = r3.getAttachmentV3UploadAttributes()     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            java.lang.Object r3 = r3.blockingGet()     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            org.whispersystems.signalservice.internal.ServiceResponse r3 = (org.whispersystems.signalservice.internal.ServiceResponse) r3     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            r2.<init>(r3)     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            java.lang.Object r2 = r2.getResultOrThrow()     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            org.whispersystems.signalservice.internal.push.AttachmentV3UploadAttributes r2 = (org.whispersystems.signalservice.internal.push.AttachmentV3UploadAttributes) r2     // Catch: WebSocketUnavailableException -> 0x002b, IOException -> 0x0023
            goto L_0x005a
        L_0x0023:
            java.lang.String r2 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.String r3 = "Failed to retrieve attachment upload attributes using pipe. Falling back..."
            org.signal.libsignal.protocol.logging.Log.w(r2, r3)
            goto L_0x0059
        L_0x002b:
            r2 = move-exception
            java.lang.String r3 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "[getResumableUploadSpec] Pipe unavailable, falling back... ("
            r4.append(r5)
            java.lang.Class<org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException> r5 = org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException.class
            java.lang.String r5 = r5.getSimpleName()
            r4.append(r5)
            java.lang.String r5 = ": "
            r4.append(r5)
            java.lang.String r2 = r2.getMessage()
            r4.append(r2)
            java.lang.String r2 = ")"
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            org.signal.libsignal.protocol.logging.Log.w(r3, r2)
        L_0x0059:
            r2 = 0
        L_0x005a:
            long r3 = java.lang.System.currentTimeMillis()
            long r3 = r3 - r0
            if (r2 != 0) goto L_0x006e
            java.lang.String r2 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.String r5 = "Not using pipe to retrieve attachment upload attributes..."
            org.signal.libsignal.protocol.logging.Log.d(r2, r5)
            org.whispersystems.signalservice.internal.push.PushServiceSocket r2 = r10.socket
            org.whispersystems.signalservice.internal.push.AttachmentV3UploadAttributes r2 = r2.getAttachmentV3UploadAttributes()
        L_0x006e:
            long r5 = java.lang.System.currentTimeMillis()
            long r5 = r5 - r0
            org.whispersystems.signalservice.internal.push.PushServiceSocket r7 = r10.socket
            org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec r2 = r7.getResumableUploadSpec(r2)
            long r7 = java.lang.System.currentTimeMillis()
            long r7 = r7 - r0
            java.lang.String r0 = org.whispersystems.signalservice.api.SignalServiceMessageSender.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r9 = "[getResumableUploadSpec] webSocket: "
            r1.append(r9)
            r1.append(r3)
            java.lang.String r3 = " rest: "
            r1.append(r3)
            r1.append(r5)
            java.lang.String r3 = " end: "
            r1.append(r3)
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            org.signal.libsignal.protocol.logging.Log.d(r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.SignalServiceMessageSender.getResumableUploadSpec():org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec");
    }

    private SignalServiceAttachmentPointer uploadAttachmentV3(SignalServiceAttachmentStream signalServiceAttachmentStream, byte[] bArr, PushAttachmentData pushAttachmentData) throws IOException {
        return new SignalServiceAttachmentPointer(pushAttachmentData.getResumableUploadSpec().getCdnNumber().intValue(), new SignalServiceAttachmentRemoteId(pushAttachmentData.getResumableUploadSpec().getCdnKey()), signalServiceAttachmentStream.getContentType(), bArr, Optional.of(Integer.valueOf(Util.toIntExact(signalServiceAttachmentStream.getLength()))), signalServiceAttachmentStream.getPreview(), signalServiceAttachmentStream.getWidth(), signalServiceAttachmentStream.getHeight(), Optional.of(this.socket.uploadAttachment(pushAttachmentData)), signalServiceAttachmentStream.getFileName(), signalServiceAttachmentStream.getVoiceNote(), signalServiceAttachmentStream.isBorderless(), signalServiceAttachmentStream.isGif(), signalServiceAttachmentStream.getCaption(), signalServiceAttachmentStream.getBlurHash(), signalServiceAttachmentStream.getUploadTimestamp());
    }

    private SendMessageResult sendVerifiedSyncMessage(VerifiedMessage verifiedMessage) throws IOException, UntrustedIdentityException {
        SignalServiceProtos.NullMessage build = SignalServiceProtos.NullMessage.newBuilder().setPadding(ByteString.copyFrom(SignalServiceProtos.DataMessage.newBuilder().setBody(Base64.encodeBytes(Util.getRandomLengthBytes(EditAboutFragment.ABOUT_MAX_GLYPHS))).build().toByteArray())).build();
        ContentHint contentHint = ContentHint.IMPLICIT;
        SendMessageResult sendMessage = sendMessage(verifiedMessage.getDestination(), Optional.empty(), verifiedMessage.getTimestamp(), EnvelopeContent.CC.encrypted(SignalServiceProtos.Content.newBuilder().setNullMessage(build).build(), contentHint, Optional.empty()), false, null);
        if (sendMessage.getSuccess().isNeedsSync()) {
            sendMessage(this.localAddress, Optional.empty(), verifiedMessage.getTimestamp(), EnvelopeContent.CC.encrypted(createMultiDeviceVerifiedContent(verifiedMessage, build.toByteArray()), contentHint, Optional.empty()), false, null);
        }
        return sendMessage;
    }

    public SendMessageResult sendNullMessage(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccessPair> optional) throws UntrustedIdentityException, IOException {
        byte[] byteArray = SignalServiceProtos.DataMessage.newBuilder().setBody(Base64.encodeBytes(Util.getRandomLengthBytes(EditAboutFragment.ABOUT_MAX_GLYPHS))).build().toByteArray();
        return sendMessage(signalServiceAddress, getTargetUnidentifiedAccess(optional), System.currentTimeMillis(), EnvelopeContent.CC.encrypted(SignalServiceProtos.Content.newBuilder().setNullMessage(SignalServiceProtos.NullMessage.newBuilder().setPadding(ByteString.copyFrom(byteArray)).build()).build(), ContentHint.IMPLICIT, Optional.empty()), false, null);
    }

    private SignalServiceProtos.Content createTypingContent(SignalServiceTypingMessage signalServiceTypingMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.TypingMessage.Builder newBuilder2 = SignalServiceProtos.TypingMessage.newBuilder();
        newBuilder2.setTimestamp(signalServiceTypingMessage.getTimestamp());
        if (signalServiceTypingMessage.isTypingStarted()) {
            newBuilder2.setAction(SignalServiceProtos.TypingMessage.Action.STARTED);
        } else if (signalServiceTypingMessage.isTypingStopped()) {
            newBuilder2.setAction(SignalServiceProtos.TypingMessage.Action.STOPPED);
        } else {
            throw new IllegalArgumentException("Unknown typing indicator");
        }
        if (signalServiceTypingMessage.getGroupId().isPresent()) {
            newBuilder2.setGroupId(ByteString.copyFrom(signalServiceTypingMessage.getGroupId().get()));
        }
        return newBuilder.setTypingMessage(newBuilder2).build();
    }

    private SignalServiceProtos.Content createStoryContent(SignalServiceStoryMessage signalServiceStoryMessage) throws IOException {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.StoryMessage.Builder newBuilder2 = SignalServiceProtos.StoryMessage.newBuilder();
        if (signalServiceStoryMessage.getProfileKey().isPresent()) {
            newBuilder2.setProfileKey(ByteString.copyFrom(signalServiceStoryMessage.getProfileKey().get()));
        }
        if (signalServiceStoryMessage.getGroupContext().isPresent()) {
            newBuilder2.setGroup(createGroupContent(signalServiceStoryMessage.getGroupContext().get()));
        }
        if (signalServiceStoryMessage.getFileAttachment().isPresent()) {
            if (signalServiceStoryMessage.getFileAttachment().get().isStream()) {
                newBuilder2.setFileAttachment(createAttachmentPointer(signalServiceStoryMessage.getFileAttachment().get().asStream()));
            } else {
                newBuilder2.setFileAttachment(createAttachmentPointer(signalServiceStoryMessage.getFileAttachment().get().asPointer()));
            }
        }
        if (signalServiceStoryMessage.getTextAttachment().isPresent()) {
            newBuilder2.setTextAttachment(createTextAttachment(signalServiceStoryMessage.getTextAttachment().get()));
        }
        newBuilder2.setAllowsReplies(signalServiceStoryMessage.getAllowsReplies().orElse(Boolean.TRUE).booleanValue());
        return newBuilder.setStoryMessage(newBuilder2).build();
    }

    private SignalServiceProtos.Content createReceiptContent(SignalServiceReceiptMessage signalServiceReceiptMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.ReceiptMessage.Builder newBuilder2 = SignalServiceProtos.ReceiptMessage.newBuilder();
        for (Long l : signalServiceReceiptMessage.getTimestamps()) {
            newBuilder2.addTimestamp(l.longValue());
        }
        if (signalServiceReceiptMessage.isDeliveryReceipt()) {
            newBuilder2.setType(SignalServiceProtos.ReceiptMessage.Type.DELIVERY);
        } else if (signalServiceReceiptMessage.isReadReceipt()) {
            newBuilder2.setType(SignalServiceProtos.ReceiptMessage.Type.READ);
        } else if (signalServiceReceiptMessage.isViewedReceipt()) {
            newBuilder2.setType(SignalServiceProtos.ReceiptMessage.Type.VIEWED);
        }
        return newBuilder.setReceiptMessage(newBuilder2).build();
    }

    private SignalServiceProtos.Content createMessageContent(SentTranscriptMessage sentTranscriptMessage) throws IOException {
        if (sentTranscriptMessage.getStoryMessage().isPresent()) {
            return createStoryContent(sentTranscriptMessage.getStoryMessage().get());
        }
        if (sentTranscriptMessage.getDataMessage().isPresent()) {
            return createMessageContent(sentTranscriptMessage.getDataMessage().get());
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.whispersystems.signalservice.internal.push.SignalServiceProtos.Content createMessageContent(org.whispersystems.signalservice.api.messages.SignalServiceDataMessage r10) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1266
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.SignalServiceMessageSender.createMessageContent(org.whispersystems.signalservice.api.messages.SignalServiceDataMessage):org.whispersystems.signalservice.internal.push.SignalServiceProtos$Content");
    }

    private SignalServiceProtos.Preview createPreview(SignalServicePreview signalServicePreview) throws IOException {
        SignalServiceProtos.Preview.Builder url = SignalServiceProtos.Preview.newBuilder().setTitle(signalServicePreview.getTitle()).setDescription(signalServicePreview.getDescription()).setDate(signalServicePreview.getDate()).setUrl(signalServicePreview.getUrl());
        if (signalServicePreview.getImage().isPresent()) {
            if (signalServicePreview.getImage().get().isStream()) {
                url.setImage(createAttachmentPointer(signalServicePreview.getImage().get().asStream()));
            } else {
                url.setImage(createAttachmentPointer(signalServicePreview.getImage().get().asPointer()));
            }
        }
        return url.build();
    }

    private SignalServiceProtos.Content createCallContent(SignalServiceCallMessage signalServiceCallMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.CallMessage.Builder newBuilder2 = SignalServiceProtos.CallMessage.newBuilder();
        if (signalServiceCallMessage.getOfferMessage().isPresent()) {
            OfferMessage offerMessage = signalServiceCallMessage.getOfferMessage().get();
            SignalServiceProtos.CallMessage.Offer.Builder type = SignalServiceProtos.CallMessage.Offer.newBuilder().setId(offerMessage.getId()).setType(offerMessage.getType().getProtoType());
            if (offerMessage.getOpaque() != null) {
                type.setOpaque(ByteString.copyFrom(offerMessage.getOpaque()));
            }
            if (offerMessage.getSdp() != null) {
                type.setSdp(offerMessage.getSdp());
            }
            newBuilder2.setOffer(type);
        } else if (signalServiceCallMessage.getAnswerMessage().isPresent()) {
            AnswerMessage answerMessage = signalServiceCallMessage.getAnswerMessage().get();
            SignalServiceProtos.CallMessage.Answer.Builder id = SignalServiceProtos.CallMessage.Answer.newBuilder().setId(answerMessage.getId());
            if (answerMessage.getOpaque() != null) {
                id.setOpaque(ByteString.copyFrom(answerMessage.getOpaque()));
            }
            if (answerMessage.getSdp() != null) {
                id.setSdp(answerMessage.getSdp());
            }
            newBuilder2.setAnswer(id);
        } else if (signalServiceCallMessage.getIceUpdateMessages().isPresent()) {
            for (IceUpdateMessage iceUpdateMessage : signalServiceCallMessage.getIceUpdateMessages().get()) {
                SignalServiceProtos.CallMessage.IceUpdate.Builder line = SignalServiceProtos.CallMessage.IceUpdate.newBuilder().setId(iceUpdateMessage.getId()).setMid("audio").setLine(0);
                if (iceUpdateMessage.getOpaque() != null) {
                    line.setOpaque(ByteString.copyFrom(iceUpdateMessage.getOpaque()));
                }
                if (iceUpdateMessage.getSdp() != null) {
                    line.setSdp(iceUpdateMessage.getSdp());
                }
                newBuilder2.addIceUpdate(line);
            }
        } else if (signalServiceCallMessage.getHangupMessage().isPresent()) {
            SignalServiceProtos.CallMessage.Hangup.Type protoType = signalServiceCallMessage.getHangupMessage().get().getType().getProtoType();
            SignalServiceProtos.CallMessage.Hangup.Builder id2 = SignalServiceProtos.CallMessage.Hangup.newBuilder().setType(protoType).setId(signalServiceCallMessage.getHangupMessage().get().getId());
            if (protoType != SignalServiceProtos.CallMessage.Hangup.Type.HANGUP_NORMAL) {
                id2.setDeviceId(signalServiceCallMessage.getHangupMessage().get().getDeviceId());
            }
            if (signalServiceCallMessage.getHangupMessage().get().isLegacy()) {
                newBuilder2.setLegacyHangup(id2);
            } else {
                newBuilder2.setHangup(id2);
            }
        } else if (signalServiceCallMessage.getBusyMessage().isPresent()) {
            newBuilder2.setBusy(SignalServiceProtos.CallMessage.Busy.newBuilder().setId(signalServiceCallMessage.getBusyMessage().get().getId()));
        } else if (signalServiceCallMessage.getOpaqueMessage().isPresent()) {
            OpaqueMessage opaqueMessage = signalServiceCallMessage.getOpaqueMessage().get();
            ByteString copyFrom = ByteString.copyFrom(opaqueMessage.getOpaque());
            newBuilder2.setOpaque(SignalServiceProtos.CallMessage.Opaque.newBuilder().setData(copyFrom).setUrgency(opaqueMessage.getUrgency().toProto()));
        }
        newBuilder2.setMultiRing(signalServiceCallMessage.isMultiRing());
        if (signalServiceCallMessage.getDestinationDeviceId().isPresent()) {
            newBuilder2.setDestinationDeviceId(signalServiceCallMessage.getDestinationDeviceId().get().intValue());
        }
        newBuilder.setCallMessage(newBuilder2);
        return newBuilder.build();
    }

    private SignalServiceProtos.Content createMultiDeviceContactsContent(SignalServiceAttachmentStream signalServiceAttachmentStream, boolean z) throws IOException {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        createSyncMessageBuilder.setContacts(SignalServiceProtos.SyncMessage.Contacts.newBuilder().setBlob(createAttachmentPointer(signalServiceAttachmentStream)).setComplete(z));
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceGroupsContent(SignalServiceAttachmentStream signalServiceAttachmentStream) throws IOException {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        createSyncMessageBuilder.setGroups(SignalServiceProtos.SyncMessage.Groups.newBuilder().setBlob(createAttachmentPointer(signalServiceAttachmentStream)));
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceSentTranscriptContent(SentTranscriptMessage sentTranscriptMessage, boolean z) throws IOException {
        SignalServiceAddress signalServiceAddress = sentTranscriptMessage.getDestination().get();
        SignalServiceProtos.Content createMessageContent = createMessageContent(sentTranscriptMessage);
        return createMultiDeviceSentTranscriptContent(createMessageContent, Optional.of(signalServiceAddress), sentTranscriptMessage.getTimestamp(), Collections.singletonList(SendMessageResult.success(signalServiceAddress, Collections.emptyList(), z, true, -1, Optional.ofNullable(createMessageContent))), sentTranscriptMessage.isRecipientUpdate(), sentTranscriptMessage.getStoryMessageRecipients());
    }

    private SignalServiceProtos.Content createMultiDeviceSentTranscriptContent(SignalServiceProtos.Content content, Optional<SignalServiceAddress> optional, long j, List<SendMessageResult> list, boolean z, Set<SignalServiceStoryMessageRecipient> set) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.Sent.Builder newBuilder2 = SignalServiceProtos.SyncMessage.Sent.newBuilder();
        SignalServiceProtos.StoryMessage storyMessage = null;
        SignalServiceProtos.DataMessage dataMessage = (content == null || !content.hasDataMessage()) ? null : content.getDataMessage();
        if (content != null && content.hasStoryMessage()) {
            storyMessage = content.getStoryMessage();
        }
        newBuilder2.setTimestamp(j);
        for (SendMessageResult sendMessageResult : list) {
            if (sendMessageResult.getSuccess() != null) {
                newBuilder2.addUnidentifiedStatus(SignalServiceProtos.SyncMessage.Sent.UnidentifiedDeliveryStatus.newBuilder().setDestinationUuid(sendMessageResult.getAddress().getServiceId().toString()).setUnidentified(sendMessageResult.getSuccess().isUnidentified()).build());
            }
        }
        if (optional.isPresent()) {
            newBuilder2.setDestinationUuid(optional.get().getServiceId().toString());
        }
        if (dataMessage != null) {
            newBuilder2.setMessage(dataMessage);
            if (dataMessage.getExpireTimer() > 0) {
                newBuilder2.setExpirationStartTimestamp(System.currentTimeMillis());
            }
            if (dataMessage.getIsViewOnce()) {
                newBuilder2.setMessage(dataMessage.toBuilder().clearAttachments().build());
            }
        }
        if (storyMessage != null) {
            newBuilder2.setStoryMessage(storyMessage);
        }
        newBuilder2.addAllStoryMessageRecipients((Iterable) Collection$EL.stream(set).map(new Function() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SignalServiceMessageSender.this.createStoryMessageRecipient((SignalServiceStoryMessageRecipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet()));
        newBuilder2.setIsRecipientUpdate(z);
        return newBuilder.setSyncMessage(createSyncMessageBuilder.setSent(newBuilder2)).build();
    }

    public SignalServiceProtos.SyncMessage.Sent.StoryMessageRecipient createStoryMessageRecipient(SignalServiceStoryMessageRecipient signalServiceStoryMessageRecipient) {
        return SignalServiceProtos.SyncMessage.Sent.StoryMessageRecipient.newBuilder().addAllDistributionListIds(signalServiceStoryMessageRecipient.getDistributionListIds()).setDestinationUuid(signalServiceStoryMessageRecipient.getSignalServiceAddress().getIdentifier()).setIsAllowedToReply(signalServiceStoryMessageRecipient.isAllowedToReply()).build();
    }

    private SignalServiceProtos.Content createMultiDeviceReadContent(List<ReadMessage> list) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        for (ReadMessage readMessage : list) {
            createSyncMessageBuilder.addRead(SignalServiceProtos.SyncMessage.Read.newBuilder().setTimestamp(readMessage.getTimestamp()).setSenderUuid(readMessage.getSender().toString()));
        }
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceViewedContent(List<ViewedMessage> list) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        for (ViewedMessage viewedMessage : list) {
            createSyncMessageBuilder.addViewed(SignalServiceProtos.SyncMessage.Viewed.newBuilder().setTimestamp(viewedMessage.getTimestamp()).setSenderUuid(viewedMessage.getSender().toString()));
        }
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceViewOnceOpenContent(ViewOnceOpenMessage viewOnceOpenMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        createSyncMessageBuilder.setViewOnceOpen(SignalServiceProtos.SyncMessage.ViewOnceOpen.newBuilder().setTimestamp(viewOnceOpenMessage.getTimestamp()).setSenderUuid(viewOnceOpenMessage.getSender().toString()));
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceBlockedContent(BlockedListMessage blockedListMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.Blocked.Builder newBuilder2 = SignalServiceProtos.SyncMessage.Blocked.newBuilder();
        for (SignalServiceAddress signalServiceAddress : blockedListMessage.getAddresses()) {
            newBuilder2.addUuids(signalServiceAddress.getServiceId().toString());
            if (signalServiceAddress.getNumber().isPresent()) {
                newBuilder2.addNumbers(signalServiceAddress.getNumber().get());
            }
        }
        for (byte[] bArr : blockedListMessage.getGroupIds()) {
            newBuilder2.addGroupIds(ByteString.copyFrom(bArr));
        }
        return newBuilder.setSyncMessage(createSyncMessageBuilder.setBlocked(newBuilder2)).build();
    }

    private SignalServiceProtos.Content createMultiDeviceConfigurationContent(ConfigurationMessage configurationMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.Configuration.Builder newBuilder2 = SignalServiceProtos.SyncMessage.Configuration.newBuilder();
        if (configurationMessage.getReadReceipts().isPresent()) {
            newBuilder2.setReadReceipts(configurationMessage.getReadReceipts().get().booleanValue());
        }
        if (configurationMessage.getUnidentifiedDeliveryIndicators().isPresent()) {
            newBuilder2.setUnidentifiedDeliveryIndicators(configurationMessage.getUnidentifiedDeliveryIndicators().get().booleanValue());
        }
        if (configurationMessage.getTypingIndicators().isPresent()) {
            newBuilder2.setTypingIndicators(configurationMessage.getTypingIndicators().get().booleanValue());
        }
        if (configurationMessage.getLinkPreviews().isPresent()) {
            newBuilder2.setLinkPreviews(configurationMessage.getLinkPreviews().get().booleanValue());
        }
        newBuilder2.setProvisioningVersion(1);
        return newBuilder.setSyncMessage(createSyncMessageBuilder.setConfiguration(newBuilder2)).build();
    }

    private SignalServiceProtos.Content createMultiDeviceStickerPackOperationContent(List<StickerPackOperationMessage> list) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        for (StickerPackOperationMessage stickerPackOperationMessage : list) {
            SignalServiceProtos.SyncMessage.StickerPackOperation.Builder newBuilder2 = SignalServiceProtos.SyncMessage.StickerPackOperation.newBuilder();
            if (stickerPackOperationMessage.getPackId().isPresent()) {
                newBuilder2.setPackId(ByteString.copyFrom(stickerPackOperationMessage.getPackId().get()));
            }
            if (stickerPackOperationMessage.getPackKey().isPresent()) {
                newBuilder2.setPackKey(ByteString.copyFrom(stickerPackOperationMessage.getPackKey().get()));
            }
            if (stickerPackOperationMessage.getType().isPresent()) {
                int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type[stickerPackOperationMessage.getType().get().ordinal()];
                if (i == 1) {
                    newBuilder2.setType(SignalServiceProtos.SyncMessage.StickerPackOperation.Type.INSTALL);
                } else if (i == 2) {
                    newBuilder2.setType(SignalServiceProtos.SyncMessage.StickerPackOperation.Type.REMOVE);
                }
            }
            createSyncMessageBuilder.addStickerPackOperation(newBuilder2);
        }
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceFetchTypeContent(SignalServiceSyncMessage.FetchType fetchType) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.FetchLatest.Builder newBuilder2 = SignalServiceProtos.SyncMessage.FetchLatest.newBuilder();
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType[fetchType.ordinal()];
        if (i == 1) {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.FetchLatest.Type.LOCAL_PROFILE);
        } else if (i == 2) {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.FetchLatest.Type.STORAGE_MANIFEST);
        } else if (i != 3) {
            Log.w(TAG, "Unknown fetch type!");
        } else {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.FetchLatest.Type.SUBSCRIPTION_STATUS);
        }
        return newBuilder.setSyncMessage(createSyncMessageBuilder.setFetchLatest(newBuilder2)).build();
    }

    private SignalServiceProtos.Content createMultiDeviceMessageRequestResponseContent(MessageRequestResponseMessage messageRequestResponseMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.MessageRequestResponse.Builder newBuilder2 = SignalServiceProtos.SyncMessage.MessageRequestResponse.newBuilder();
        if (messageRequestResponseMessage.getGroupId().isPresent()) {
            newBuilder2.setGroupId(ByteString.copyFrom(messageRequestResponseMessage.getGroupId().get()));
        }
        if (messageRequestResponseMessage.getPerson().isPresent()) {
            newBuilder2.setThreadUuid(messageRequestResponseMessage.getPerson().get().toString());
        }
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[messageRequestResponseMessage.getType().ordinal()];
        if (i == 1) {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.ACCEPT);
        } else if (i == 2) {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.DELETE);
        } else if (i == 3) {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.BLOCK);
        } else if (i != 4) {
            Log.w(TAG, "Unknown type!");
            newBuilder2.setType(SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.UNKNOWN);
        } else {
            newBuilder2.setType(SignalServiceProtos.SyncMessage.MessageRequestResponse.Type.BLOCK_AND_DELETE);
        }
        createSyncMessageBuilder.setMessageRequestResponse(newBuilder2);
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createMultiDeviceOutgoingPaymentContent(OutgoingPaymentMessage outgoingPaymentMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.OutgoingPayment.Builder newBuilder2 = SignalServiceProtos.SyncMessage.OutgoingPayment.newBuilder();
        if (outgoingPaymentMessage.getRecipient().isPresent()) {
            newBuilder2.setRecipientUuid(outgoingPaymentMessage.getRecipient().get().toString());
        }
        if (outgoingPaymentMessage.getNote().isPresent()) {
            newBuilder2.setNote(outgoingPaymentMessage.getNote().get());
        }
        try {
            SignalServiceProtos.SyncMessage.OutgoingPayment.MobileCoin.Builder newBuilder3 = SignalServiceProtos.SyncMessage.OutgoingPayment.MobileCoin.newBuilder();
            if (outgoingPaymentMessage.getAddress().isPresent()) {
                newBuilder3.setRecipientAddress(ByteString.copyFrom(outgoingPaymentMessage.getAddress().get()));
            }
            newBuilder3.setAmountPicoMob(Uint64Util.bigIntegerToUInt64(outgoingPaymentMessage.getAmount().toPicoMobBigInteger())).setFeePicoMob(Uint64Util.bigIntegerToUInt64(outgoingPaymentMessage.getFee().toPicoMobBigInteger())).setReceipt(outgoingPaymentMessage.getReceipt()).setLedgerBlockTimestamp(outgoingPaymentMessage.getBlockTimestamp()).setLedgerBlockIndex(outgoingPaymentMessage.getBlockIndex()).addAllOutputPublicKeys(outgoingPaymentMessage.getPublicKeys()).addAllSpentKeyImages(outgoingPaymentMessage.getKeyImages());
            newBuilder2.setMobileCoin(newBuilder3);
            createSyncMessageBuilder.setOutgoingPayment(newBuilder2);
            return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
        } catch (Uint64RangeException e) {
            throw new AssertionError(e);
        }
    }

    private SignalServiceProtos.Content createMultiDeviceSyncKeysContent(KeysMessage keysMessage) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.SyncMessage.Keys.Builder newBuilder2 = SignalServiceProtos.SyncMessage.Keys.newBuilder();
        if (keysMessage.getStorageService().isPresent()) {
            newBuilder2.setStorageService(ByteString.copyFrom(keysMessage.getStorageService().get().serialize()));
        } else {
            Log.w(TAG, "Invalid keys message!");
        }
        return newBuilder.setSyncMessage(createSyncMessageBuilder.setKeys(newBuilder2)).build();
    }

    private SignalServiceProtos.Content createMultiDeviceVerifiedContent(VerifiedMessage verifiedMessage, byte[] bArr) {
        SignalServiceProtos.Content.Builder newBuilder = SignalServiceProtos.Content.newBuilder();
        SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder = createSyncMessageBuilder();
        SignalServiceProtos.Verified.Builder newBuilder2 = SignalServiceProtos.Verified.newBuilder();
        newBuilder2.setNullMessage(ByteString.copyFrom(bArr));
        newBuilder2.setIdentityKey(ByteString.copyFrom(verifiedMessage.getIdentityKey().serialize()));
        newBuilder2.setDestinationUuid(verifiedMessage.getDestination().getServiceId().toString());
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState[verifiedMessage.getVerified().ordinal()];
        if (i == 1) {
            newBuilder2.setState(SignalServiceProtos.Verified.State.DEFAULT);
        } else if (i == 2) {
            newBuilder2.setState(SignalServiceProtos.Verified.State.VERIFIED);
        } else if (i == 3) {
            newBuilder2.setState(SignalServiceProtos.Verified.State.UNVERIFIED);
        } else {
            throw new AssertionError("Unknown: " + verifiedMessage.getVerified());
        }
        createSyncMessageBuilder.setVerified(newBuilder2);
        return newBuilder.setSyncMessage(createSyncMessageBuilder).build();
    }

    private SignalServiceProtos.Content createRequestContent(SignalServiceProtos.SyncMessage.Request request) throws IOException {
        if (this.localDeviceId != 1) {
            return SignalServiceProtos.Content.newBuilder().setSyncMessage(SignalServiceProtos.SyncMessage.newBuilder().setRequest(request)).build();
        }
        throw new IOException("Sync requests should only be sent from a linked device");
    }

    private SignalServiceProtos.Content createPniIdentityContent(SignalServiceProtos.SyncMessage.PniIdentity pniIdentity) {
        return SignalServiceProtos.Content.newBuilder().setSyncMessage(SignalServiceProtos.SyncMessage.newBuilder().setPniIdentity(pniIdentity)).build();
    }

    private SignalServiceProtos.SyncMessage.Builder createSyncMessageBuilder() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomLengthBytes = Util.getRandomLengthBytes(512);
        secureRandom.nextBytes(randomLengthBytes);
        SignalServiceProtos.SyncMessage.Builder newBuilder = SignalServiceProtos.SyncMessage.newBuilder();
        newBuilder.setPadding(ByteString.copyFrom(randomLengthBytes));
        return newBuilder;
    }

    private SignalServiceProtos.GroupContext createGroupContent(SignalServiceGroup signalServiceGroup) throws IOException {
        SignalServiceProtos.GroupContext.Builder newBuilder = SignalServiceProtos.GroupContext.newBuilder();
        newBuilder.setId(ByteString.copyFrom(signalServiceGroup.getGroupId()));
        if (signalServiceGroup.getType() != SignalServiceGroup.Type.DELIVER) {
            if (signalServiceGroup.getType() == SignalServiceGroup.Type.UPDATE) {
                newBuilder.setType(SignalServiceProtos.GroupContext.Type.UPDATE);
            } else if (signalServiceGroup.getType() == SignalServiceGroup.Type.QUIT) {
                newBuilder.setType(SignalServiceProtos.GroupContext.Type.QUIT);
            } else if (signalServiceGroup.getType() == SignalServiceGroup.Type.REQUEST_INFO) {
                newBuilder.setType(SignalServiceProtos.GroupContext.Type.REQUEST_INFO);
            } else {
                throw new AssertionError("Unknown type: " + signalServiceGroup.getType());
            }
            if (signalServiceGroup.getName().isPresent()) {
                newBuilder.setName(signalServiceGroup.getName().get());
            }
            if (signalServiceGroup.getMembers().isPresent()) {
                for (SignalServiceAddress signalServiceAddress : signalServiceGroup.getMembers().get()) {
                    if (signalServiceAddress.getNumber().isPresent()) {
                        newBuilder.addMembersE164(signalServiceAddress.getNumber().get());
                        SignalServiceProtos.GroupContext.Member.Builder newBuilder2 = SignalServiceProtos.GroupContext.Member.newBuilder();
                        newBuilder2.setE164(signalServiceAddress.getNumber().get());
                        newBuilder.addMembers(newBuilder2.build());
                    }
                }
            }
            if (signalServiceGroup.getAvatar().isPresent()) {
                if (signalServiceGroup.getAvatar().get().isStream()) {
                    newBuilder.setAvatar(createAttachmentPointer(signalServiceGroup.getAvatar().get().asStream()));
                } else {
                    newBuilder.setAvatar(createAttachmentPointer(signalServiceGroup.getAvatar().get().asPointer()));
                }
            }
        } else {
            newBuilder.setType(SignalServiceProtos.GroupContext.Type.DELIVER);
        }
        return newBuilder.build();
    }

    private static SignalServiceProtos.GroupContextV2 createGroupContent(SignalServiceGroupV2 signalServiceGroupV2) {
        SignalServiceProtos.GroupContextV2.Builder revision = SignalServiceProtos.GroupContextV2.newBuilder().setMasterKey(ByteString.copyFrom(signalServiceGroupV2.getMasterKey().serialize())).setRevision(signalServiceGroupV2.getRevision());
        byte[] signedGroupChange = signalServiceGroupV2.getSignedGroupChange();
        if (signedGroupChange != null && signedGroupChange.length <= 2048) {
            revision.setGroupChange(ByteString.copyFrom(signedGroupChange));
        }
        return revision.build();
    }

    private List<SignalServiceProtos.DataMessage.Contact> createSharedContactContent(List<SharedContact> list) throws IOException {
        SignalServiceProtos.AttachmentPointer attachmentPointer;
        LinkedList linkedList = new LinkedList();
        for (SharedContact sharedContact : list) {
            SignalServiceProtos.DataMessage.Contact.Name.Builder newBuilder = SignalServiceProtos.DataMessage.Contact.Name.newBuilder();
            if (sharedContact.getName().getFamily().isPresent()) {
                newBuilder.setFamilyName(sharedContact.getName().getFamily().get());
            }
            if (sharedContact.getName().getGiven().isPresent()) {
                newBuilder.setGivenName(sharedContact.getName().getGiven().get());
            }
            if (sharedContact.getName().getMiddle().isPresent()) {
                newBuilder.setMiddleName(sharedContact.getName().getMiddle().get());
            }
            if (sharedContact.getName().getPrefix().isPresent()) {
                newBuilder.setPrefix(sharedContact.getName().getPrefix().get());
            }
            if (sharedContact.getName().getSuffix().isPresent()) {
                newBuilder.setSuffix(sharedContact.getName().getSuffix().get());
            }
            if (sharedContact.getName().getDisplay().isPresent()) {
                newBuilder.setDisplayName(sharedContact.getName().getDisplay().get());
            }
            SignalServiceProtos.DataMessage.Contact.Builder name = SignalServiceProtos.DataMessage.Contact.newBuilder().setName(newBuilder);
            if (sharedContact.getAddress().isPresent()) {
                for (SharedContact.PostalAddress postalAddress : sharedContact.getAddress().get()) {
                    SignalServiceProtos.DataMessage.Contact.PostalAddress.Builder newBuilder2 = SignalServiceProtos.DataMessage.Contact.PostalAddress.newBuilder();
                    int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type[postalAddress.getType().ordinal()];
                    if (i == 1) {
                        newBuilder2.setType(SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.HOME);
                    } else if (i == 2) {
                        newBuilder2.setType(SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.WORK);
                    } else if (i == 3) {
                        newBuilder2.setType(SignalServiceProtos.DataMessage.Contact.PostalAddress.Type.CUSTOM);
                    } else {
                        throw new AssertionError("Unknown type: " + postalAddress.getType());
                    }
                    if (postalAddress.getCity().isPresent()) {
                        newBuilder2.setCity(postalAddress.getCity().get());
                    }
                    if (postalAddress.getCountry().isPresent()) {
                        newBuilder2.setCountry(postalAddress.getCountry().get());
                    }
                    if (postalAddress.getLabel().isPresent()) {
                        newBuilder2.setLabel(postalAddress.getLabel().get());
                    }
                    if (postalAddress.getNeighborhood().isPresent()) {
                        newBuilder2.setNeighborhood(postalAddress.getNeighborhood().get());
                    }
                    if (postalAddress.getPobox().isPresent()) {
                        newBuilder2.setPobox(postalAddress.getPobox().get());
                    }
                    if (postalAddress.getPostcode().isPresent()) {
                        newBuilder2.setPostcode(postalAddress.getPostcode().get());
                    }
                    if (postalAddress.getRegion().isPresent()) {
                        newBuilder2.setRegion(postalAddress.getRegion().get());
                    }
                    if (postalAddress.getStreet().isPresent()) {
                        newBuilder2.setStreet(postalAddress.getStreet().get());
                    }
                    name.addAddress(newBuilder2);
                }
            }
            if (sharedContact.getEmail().isPresent()) {
                for (SharedContact.Email email : sharedContact.getEmail().get()) {
                    SignalServiceProtos.DataMessage.Contact.Email.Builder value = SignalServiceProtos.DataMessage.Contact.Email.newBuilder().setValue(email.getValue());
                    int i2 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[email.getType().ordinal()];
                    if (i2 == 1) {
                        value.setType(SignalServiceProtos.DataMessage.Contact.Email.Type.HOME);
                    } else if (i2 == 2) {
                        value.setType(SignalServiceProtos.DataMessage.Contact.Email.Type.WORK);
                    } else if (i2 == 3) {
                        value.setType(SignalServiceProtos.DataMessage.Contact.Email.Type.MOBILE);
                    } else if (i2 == 4) {
                        value.setType(SignalServiceProtos.DataMessage.Contact.Email.Type.CUSTOM);
                    } else {
                        throw new AssertionError("Unknown type: " + email.getType());
                    }
                    if (email.getLabel().isPresent()) {
                        value.setLabel(email.getLabel().get());
                    }
                    name.addEmail(value);
                }
            }
            if (sharedContact.getPhone().isPresent()) {
                for (SharedContact.Phone phone : sharedContact.getPhone().get()) {
                    SignalServiceProtos.DataMessage.Contact.Phone.Builder value2 = SignalServiceProtos.DataMessage.Contact.Phone.newBuilder().setValue(phone.getValue());
                    int i3 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[phone.getType().ordinal()];
                    if (i3 == 1) {
                        value2.setType(SignalServiceProtos.DataMessage.Contact.Phone.Type.HOME);
                    } else if (i3 == 2) {
                        value2.setType(SignalServiceProtos.DataMessage.Contact.Phone.Type.WORK);
                    } else if (i3 == 3) {
                        value2.setType(SignalServiceProtos.DataMessage.Contact.Phone.Type.MOBILE);
                    } else if (i3 == 4) {
                        value2.setType(SignalServiceProtos.DataMessage.Contact.Phone.Type.CUSTOM);
                    } else {
                        throw new AssertionError("Unknown type: " + phone.getType());
                    }
                    if (phone.getLabel().isPresent()) {
                        value2.setLabel(phone.getLabel().get());
                    }
                    name.addNumber(value2);
                }
            }
            if (sharedContact.getAvatar().isPresent()) {
                if (sharedContact.getAvatar().get().getAttachment().isStream()) {
                    attachmentPointer = createAttachmentPointer(sharedContact.getAvatar().get().getAttachment().asStream());
                } else {
                    attachmentPointer = createAttachmentPointer(sharedContact.getAvatar().get().getAttachment().asPointer());
                }
                name.setAvatar(SignalServiceProtos.DataMessage.Contact.Avatar.newBuilder().setAvatar(attachmentPointer).setIsProfile(sharedContact.getAvatar().get().isProfile()));
            }
            if (sharedContact.getOrganization().isPresent()) {
                name.setOrganization(sharedContact.getOrganization().get());
            }
            linkedList.add(name.build());
        }
        return linkedList;
    }

    private SignalServiceSyncMessage createSelfSendSyncMessageForStory(SignalServiceStoryMessage signalServiceStoryMessage, long j, Set<SignalServiceStoryMessageRecipient> set) {
        return SignalServiceSyncMessage.forSentTranscript(new SentTranscriptMessage(Optional.of(this.localAddress), j, Optional.empty(), 0, Collections.singletonMap(this.localAddress.getServiceId(), Boolean.FALSE), false, Optional.of(signalServiceStoryMessage), set));
    }

    private SignalServiceSyncMessage createSelfSendSyncMessage(SignalServiceDataMessage signalServiceDataMessage) {
        return SignalServiceSyncMessage.forSentTranscript(new SentTranscriptMessage(Optional.of(this.localAddress), signalServiceDataMessage.getTimestamp(), Optional.of(signalServiceDataMessage), (long) signalServiceDataMessage.getExpiresInSeconds(), Collections.singletonMap(this.localAddress.getServiceId(), Boolean.FALSE), false, Optional.empty(), Collections.emptySet()));
    }

    private List<SendMessageResult> sendMessage(List<SignalServiceAddress> list, List<Optional<UnidentifiedAccess>> list2, long j, EnvelopeContent envelopeContent, boolean z, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException {
        SignalServiceMessageSender signalServiceMessageSender = this;
        Log.d(TAG, "[" + j + "] Sending to " + list.size() + " recipients.");
        signalServiceMessageSender.enforceMaxContentSize(envelopeContent);
        long currentTimeMillis = System.currentTimeMillis();
        LinkedList<Future> linkedList = new LinkedList();
        Iterator<Optional<UnidentifiedAccess>> it = list2.iterator();
        for (SignalServiceAddress signalServiceAddress : list) {
            linkedList.add(signalServiceMessageSender.executor.submit(new Callable(signalServiceAddress, it.next(), j, envelopeContent, z, cancelationSignal, partialSendCompleteListener) { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda5
                public final /* synthetic */ SignalServiceAddress f$1;
                public final /* synthetic */ Optional f$2;
                public final /* synthetic */ long f$3;
                public final /* synthetic */ EnvelopeContent f$4;
                public final /* synthetic */ boolean f$5;
                public final /* synthetic */ CancelationSignal f$6;
                public final /* synthetic */ PartialSendCompleteListener f$7;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r6;
                    this.f$5 = r7;
                    this.f$6 = r8;
                    this.f$7 = r9;
                }

                @Override // java.util.concurrent.Callable
                public final Object call() {
                    return SignalServiceMessageSender.this.lambda$sendMessage$2(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
                }
            }));
            signalServiceMessageSender = this;
            linkedList = linkedList;
            currentTimeMillis = currentTimeMillis;
        }
        ArrayList<SendMessageResult> arrayList = new ArrayList(linkedList.size());
        Iterator<SignalServiceAddress> it2 = list.iterator();
        for (Future future : linkedList) {
            SignalServiceAddress next = it2.next();
            try {
                arrayList.add((SendMessageResult) future.get());
            } catch (InterruptedException e) {
                throw new IOException(e);
            } catch (ExecutionException e2) {
                if (e2.getCause() instanceof UntrustedIdentityException) {
                    Log.w(TAG, e2);
                    arrayList.add(SendMessageResult.identityFailure(next, ((UntrustedIdentityException) e2.getCause()).getIdentityKey()));
                } else if (e2.getCause() instanceof UnregisteredUserException) {
                    Log.w(TAG, "[" + j + "] Found unregistered user.");
                    arrayList.add(SendMessageResult.unregisteredFailure(next));
                } else if (e2.getCause() instanceof PushNetworkException) {
                    Log.w(TAG, e2);
                    arrayList.add(SendMessageResult.networkFailure(next));
                } else if (e2.getCause() instanceof ServerRejectedException) {
                    Log.w(TAG, e2);
                    throw ((ServerRejectedException) e2.getCause());
                } else if (e2.getCause() instanceof ProofRequiredException) {
                    Log.w(TAG, e2);
                    arrayList.add(SendMessageResult.proofRequiredFailure(next, (ProofRequiredException) e2.getCause()));
                } else if (e2.getCause() instanceof RateLimitException) {
                    Log.w(TAG, e2);
                    arrayList.add(SendMessageResult.rateLimitFailure(next, (RateLimitException) e2.getCause()));
                } else {
                    throw new IOException(e2);
                }
            }
        }
        double d = 0.0d;
        double d2 = 0.0d;
        for (SendMessageResult sendMessageResult : arrayList) {
            if (!(sendMessageResult.getSuccess() == null || sendMessageResult.getSuccess().getDuration() == -1)) {
                d2 += 1.0d;
            }
        }
        if (d2 > 0.0d) {
            for (SendMessageResult sendMessageResult2 : arrayList) {
                if (!(sendMessageResult2.getSuccess() == null || sendMessageResult2.getSuccess().getDuration() == -1)) {
                    double duration = (double) sendMessageResult2.getSuccess().getDuration();
                    Double.isNaN(duration);
                    d += duration / d2;
                }
            }
        }
        Log.d(TAG, "[" + j + "] Completed send to " + list.size() + " recipients in " + (System.currentTimeMillis() - currentTimeMillis) + " ms, with an average time of " + Math.round(d) + " ms per send.");
        return arrayList;
    }

    public /* synthetic */ SendMessageResult lambda$sendMessage$2(SignalServiceAddress signalServiceAddress, Optional optional, long j, EnvelopeContent envelopeContent, boolean z, CancelationSignal cancelationSignal, PartialSendCompleteListener partialSendCompleteListener) throws Exception {
        SendMessageResult sendMessage = sendMessage(signalServiceAddress, optional, j, envelopeContent, z, cancelationSignal);
        if (partialSendCompleteListener != null) {
            partialSendCompleteListener.onPartialSendComplete(sendMessage);
        }
        return sendMessage;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:145:0x02ee */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x02b0 A[Catch: InvalidKeyException -> 0x030e, AuthorizationFailedException -> 0x02f6, MismatchedDevicesException -> 0x02f0, StaleDevicesException -> 0x02ee, TryCatch #46 {InvalidKeyException -> 0x030e, blocks: (B:12:0x003b, B:25:0x00a1, B:36:0x00f6, B:73:0x01cc, B:75:0x01d2, B:77:0x01d8, B:92:0x0219, B:94:0x0228, B:114:0x0250, B:118:0x0271, B:128:0x02ab, B:131:0x02b0, B:134:0x02b7, B:135:0x02bc, B:136:0x02bd, B:138:0x02d1, B:143:0x02dd), top: B:182:0x003b }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x02d1 A[Catch: InvalidKeyException -> 0x030e, AuthorizationFailedException -> 0x02f6, MismatchedDevicesException -> 0x02f0, StaleDevicesException -> 0x02ee, TryCatch #46 {InvalidKeyException -> 0x030e, blocks: (B:12:0x003b, B:25:0x00a1, B:36:0x00f6, B:73:0x01cc, B:75:0x01d2, B:77:0x01d8, B:92:0x0219, B:94:0x0228, B:114:0x0250, B:118:0x0271, B:128:0x02ab, B:131:0x02b0, B:134:0x02b7, B:135:0x02bc, B:136:0x02bd, B:138:0x02d1, B:143:0x02dd), top: B:182:0x003b }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0386  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x0100 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x0392 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01cb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.whispersystems.signalservice.api.messages.SendMessageResult sendMessage(org.whispersystems.signalservice.api.push.SignalServiceAddress r23, j$.util.Optional<org.whispersystems.signalservice.api.crypto.UnidentifiedAccess> r24, long r25, org.whispersystems.signalservice.api.crypto.EnvelopeContent r27, boolean r28, org.whispersystems.signalservice.internal.push.http.CancelationSignal r29) throws org.whispersystems.signalservice.api.crypto.UntrustedIdentityException, java.io.IOException {
        /*
        // Method dump skipped, instructions count: 954
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.SignalServiceMessageSender.sendMessage(org.whispersystems.signalservice.api.push.SignalServiceAddress, j$.util.Optional, long, org.whispersystems.signalservice.api.crypto.EnvelopeContent, boolean, org.whispersystems.signalservice.internal.push.http.CancelationSignal):org.whispersystems.signalservice.api.messages.SendMessageResult");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:37:0x02c5 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:92:0x0287 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:74:0x0382 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:79:0x03d3 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:108:0x0426 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:36:0x02c3 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r0v55. Raw type applied. Possible types: T */
    /* JADX WARN: Type inference failed for: r24v0, types: [org.whispersystems.signalservice.api.SignalServiceMessageSender] */
    /* JADX WARN: Type inference failed for: r6v1, types: [org.whispersystems.signalservice.api.crypto.SignalServiceCipher] */
    /* JADX WARN: Type inference failed for: r10v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r11v2, types: [org.signal.libsignal.metadata.certificate.SenderCertificate] */
    /* JADX WARN: Type inference failed for: r12v2, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r13v0, types: [org.whispersystems.signalservice.api.crypto.ContentHint] */
    /* JADX WARN: Type inference failed for: r8v3, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r13v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r12v3, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r11v3 */
    /* JADX WARN: Type inference failed for: r10v3 */
    /* JADX WARN: Type inference failed for: r8v4 */
    /* JADX WARN: Type inference failed for: r1v17, types: [java.lang.StringBuilder] */
    /* JADX WARN: Type inference failed for: r13v2, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r12v5, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r11v5 */
    /* JADX WARN: Type inference failed for: r10v5 */
    /* JADX WARN: Type inference failed for: r8v6 */
    /* JADX WARN: Type inference failed for: r2v13, types: [java.lang.StringBuilder] */
    /* JADX WARN: Type inference failed for: r13v3, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r12v6, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r11v6 */
    /* JADX WARN: Type inference failed for: r10v6 */
    /* JADX WARN: Type inference failed for: r8v7 */
    /* JADX WARN: Type inference failed for: r2v16, types: [java.lang.StringBuilder] */
    /* JADX WARN: Type inference failed for: r13v4 */
    /* JADX WARN: Type inference failed for: r12v7 */
    /* JADX WARN: Type inference failed for: r11v7 */
    /* JADX WARN: Type inference failed for: r10v7 */
    /* JADX WARN: Type inference failed for: r8v8 */
    /* JADX WARN: Type inference failed for: r13v5 */
    /* JADX WARN: Type inference failed for: r12v8 */
    /* JADX WARN: Type inference failed for: r11v8 */
    /* JADX WARN: Type inference failed for: r10v8 */
    /* JADX WARN: Type inference failed for: r8v9 */
    /* JADX WARN: Type inference failed for: r13v6, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r12v9, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r11v9, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r10v9 */
    /* JADX WARN: Type inference failed for: r8v10 */
    /* JADX WARN: Type inference failed for: r8v11 */
    /* JADX WARN: Type inference failed for: r2v19, types: [java.lang.StringBuilder] */
    /* JADX WARN: Type inference failed for: r13v8 */
    /* JADX WARN: Type inference failed for: r12v11 */
    /* JADX WARN: Type inference failed for: r11v11 */
    /* JADX WARN: Type inference failed for: r10v11, types: [org.whispersystems.signalservice.internal.push.SignalServiceProtos$Content] */
    /* JADX WARN: Type inference failed for: r1v30, types: [org.whispersystems.signalservice.internal.push.PushServiceSocket] */
    /* JADX WARN: Type inference failed for: r13v9, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r12v12, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r11v12, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r10v12 */
    /* JADX WARN: Type inference failed for: r2v21, types: [java.lang.StringBuilder] */
    /* JADX WARN: Type inference failed for: r10v14 */
    /* JADX WARN: Type inference failed for: r11v14 */
    /* JADX WARN: Type inference failed for: r12v14 */
    /* JADX WARN: Type inference failed for: r13v11 */
    /* JADX WARN: Type inference failed for: r2v22, types: [org.whispersystems.signalservice.api.services.MessagingService] */
    /* JADX WARN: Type inference failed for: r10v16, types: [org.whispersystems.signalservice.internal.push.SignalServiceProtos$Content] */
    /* JADX WARN: Type inference failed for: r11v16 */
    /* JADX WARN: Type inference failed for: r12v16 */
    /* JADX WARN: Type inference failed for: r13v13 */
    /* JADX WARN: Type inference failed for: r10v17 */
    /* JADX WARN: Type inference failed for: r10v18 */
    /* JADX WARN: Type inference failed for: r11v19 */
    /* JADX WARN: Type inference failed for: r11v20 */
    /* JADX WARN: Type inference failed for: r12v17 */
    /* JADX WARN: Type inference failed for: r12v18 */
    /* JADX WARN: Type inference failed for: r13v14 */
    /* JADX WARN: Type inference failed for: r13v15 */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<org.whispersystems.signalservice.api.messages.SendMessageResult> sendGroupMessage(org.whispersystems.signalservice.api.push.DistributionId r25, java.util.List<org.whispersystems.signalservice.api.push.SignalServiceAddress> r26, java.util.List<org.whispersystems.signalservice.api.crypto.UnidentifiedAccess> r27, long r28, org.whispersystems.signalservice.internal.push.SignalServiceProtos.Content r30, org.whispersystems.signalservice.api.crypto.ContentHint r31, j$.util.Optional<byte[]> r32, boolean r33, org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents r34) throws java.io.IOException, org.whispersystems.signalservice.api.crypto.UntrustedIdentityException, org.signal.libsignal.protocol.NoSessionException, org.signal.libsignal.protocol.InvalidKeyException, org.signal.libsignal.protocol.InvalidRegistrationIdException {
        /*
        // Method dump skipped, instructions count: 1142
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.api.SignalServiceMessageSender.sendGroupMessage(org.whispersystems.signalservice.api.push.DistributionId, java.util.List, java.util.List, long, org.whispersystems.signalservice.internal.push.SignalServiceProtos$Content, org.whispersystems.signalservice.api.crypto.ContentHint, j$.util.Optional, boolean, org.whispersystems.signalservice.api.SignalServiceMessageSender$SenderKeyGroupEvents):java.util.List");
    }

    public static /* synthetic */ boolean lambda$sendGroupMessage$3(Set set, SignalProtocolAddress signalProtocolAddress) {
        return !set.contains(signalProtocolAddress);
    }

    public static /* synthetic */ ServiceId lambda$sendGroupMessage$4(SignalProtocolAddress signalProtocolAddress) {
        return ServiceId.parseOrThrow(signalProtocolAddress.getName());
    }

    public static /* synthetic */ Optional lambda$sendGroupMessage$5(Map map, SignalServiceAddress signalServiceAddress) {
        UnidentifiedAccess unidentifiedAccess = (UnidentifiedAccess) map.get(signalServiceAddress.getServiceId());
        return Optional.of(new UnidentifiedAccessPair(unidentifiedAccess, unidentifiedAccess));
    }

    public static /* synthetic */ String lambda$sendGroupMessage$6(SignalServiceAddress signalServiceAddress) {
        return signalServiceAddress.getServiceId().toString();
    }

    public static /* synthetic */ boolean lambda$sendGroupMessage$7(Set set, SignalProtocolAddress signalProtocolAddress) {
        return set.contains(signalProtocolAddress.getName());
    }

    public static /* synthetic */ boolean lambda$sendGroupMessage$8(SendMessageResult sendMessageResult) {
        return !sendMessageResult.isSuccess();
    }

    public static /* synthetic */ ServiceId lambda$sendGroupMessage$9(SendMessageResult sendMessageResult) {
        return sendMessageResult.getAddress().getServiceId();
    }

    public static /* synthetic */ boolean lambda$sendGroupMessage$10(Set set, SignalServiceAddress signalServiceAddress) {
        return !set.contains(signalServiceAddress.getServiceId());
    }

    private GroupTargetInfo buildGroupTargetInfo(List<SignalServiceAddress> list) {
        Set<SignalProtocolAddress> allAddressesWithActiveSessions = this.store.getAllAddressesWithActiveSessions((List) Collection$EL.stream(list).map(new SignalServiceMessageSender$$ExternalSyntheticLambda6()).collect(Collectors.toList()));
        HashMap hashMap = new HashMap();
        allAddressesWithActiveSessions.addAll((Collection) Collection$EL.stream(list).map(new Function() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SignalServiceMessageSender.lambda$buildGroupTargetInfo$11((SignalServiceAddress) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()));
        for (SignalProtocolAddress signalProtocolAddress : allAddressesWithActiveSessions) {
            List linkedList = hashMap.containsKey(signalProtocolAddress.getName()) ? (List) hashMap.get(signalProtocolAddress.getName()) : new LinkedList();
            linkedList.add(Integer.valueOf(signalProtocolAddress.getDeviceId()));
            hashMap.put(signalProtocolAddress.getName(), linkedList);
        }
        HashMap hashMap2 = new HashMap();
        for (SignalServiceAddress signalServiceAddress : list) {
            if (hashMap.containsKey(signalServiceAddress.getIdentifier())) {
                hashMap2.put(signalServiceAddress, (List) hashMap.get(signalServiceAddress.getIdentifier()));
            }
        }
        return new GroupTargetInfo(new ArrayList(allAddressesWithActiveSessions), hashMap2, null);
    }

    public static /* synthetic */ SignalProtocolAddress lambda$buildGroupTargetInfo$11(SignalServiceAddress signalServiceAddress) {
        return new SignalProtocolAddress(signalServiceAddress.getIdentifier(), 1);
    }

    /* loaded from: classes5.dex */
    public static final class GroupTargetInfo {
        private final List<SignalProtocolAddress> destinations;
        private final Map<SignalServiceAddress, List<Integer>> devices;

        /* synthetic */ GroupTargetInfo(List list, Map map, AnonymousClass1 r3) {
            this(list, map);
        }

        private GroupTargetInfo(List<SignalProtocolAddress> list, Map<SignalServiceAddress, List<Integer>> map) {
            this.destinations = list;
            this.devices = map;
        }
    }

    private List<SendMessageResult> transformGroupResponseToMessageResults(Map<SignalServiceAddress, List<Integer>> map, SendGroupMessageResponse sendGroupMessageResponse, SignalServiceProtos.Content content) {
        Set<ServiceId> unsentTargets = sendGroupMessageResponse.getUnsentTargets();
        List list = (List) Collection$EL.stream(unsentTargets).map(new StorageSyncModels$$ExternalSyntheticLambda0()).map(new Function() { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda8
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SendMessageResult.unregisteredFailure((SignalServiceAddress) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        List list2 = (List) Collection$EL.stream(map.keySet()).filter(new Predicate(unsentTargets) { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda9
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return SignalServiceMessageSender.lambda$transformGroupResponseToMessageResults$12(this.f$0, (SignalServiceAddress) obj);
            }
        }).map(new Function(map, content) { // from class: org.whispersystems.signalservice.api.SignalServiceMessageSender$$ExternalSyntheticLambda10
            public final /* synthetic */ Map f$1;
            public final /* synthetic */ SignalServiceProtos.Content f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SignalServiceMessageSender.this.lambda$transformGroupResponseToMessageResults$13(this.f$1, this.f$2, (SignalServiceAddress) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        ArrayList arrayList = new ArrayList(list2.size() + list.size());
        arrayList.addAll(list2);
        arrayList.addAll(list);
        return arrayList;
    }

    public static /* synthetic */ boolean lambda$transformGroupResponseToMessageResults$12(Set set, SignalServiceAddress signalServiceAddress) {
        return !set.contains(signalServiceAddress.getServiceId());
    }

    public /* synthetic */ SendMessageResult lambda$transformGroupResponseToMessageResults$13(Map map, SignalServiceProtos.Content content, SignalServiceAddress signalServiceAddress) {
        return SendMessageResult.success(signalServiceAddress, (List) map.get(signalServiceAddress), true, this.store.isMultiDevice(), -1, Optional.of(content));
    }

    private List<SignalServiceProtos.AttachmentPointer> createAttachmentPointers(Optional<List<SignalServiceAttachment>> optional) throws IOException {
        LinkedList linkedList = new LinkedList();
        if (optional.isPresent() && !optional.get().isEmpty()) {
            for (SignalServiceAttachment signalServiceAttachment : optional.get()) {
                if (signalServiceAttachment.isStream()) {
                    Log.i(TAG, "Found attachment, creating pointer...");
                    linkedList.add(createAttachmentPointer(signalServiceAttachment.asStream()));
                } else if (signalServiceAttachment.isPointer()) {
                    Log.i(TAG, "Including existing attachment pointer...");
                    linkedList.add(createAttachmentPointer(signalServiceAttachment.asPointer()));
                }
            }
        }
        return linkedList;
    }

    private SignalServiceProtos.AttachmentPointer createAttachmentPointer(SignalServiceAttachmentPointer signalServiceAttachmentPointer) {
        return AttachmentPointerUtil.createAttachmentPointer(signalServiceAttachmentPointer);
    }

    private SignalServiceProtos.AttachmentPointer createAttachmentPointer(SignalServiceAttachmentStream signalServiceAttachmentStream) throws IOException {
        return createAttachmentPointer(uploadAttachment(signalServiceAttachmentStream));
    }

    private SignalServiceProtos.TextAttachment createTextAttachment(SignalServiceTextAttachment signalServiceTextAttachment) throws IOException {
        SignalServiceProtos.TextAttachment.Builder newBuilder = SignalServiceProtos.TextAttachment.newBuilder();
        if (signalServiceTextAttachment.getStyle().isPresent()) {
            switch (AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[signalServiceTextAttachment.getStyle().get().ordinal()]) {
                case 1:
                    newBuilder.setTextStyle(SignalServiceProtos.TextAttachment.Style.DEFAULT);
                    break;
                case 2:
                    newBuilder.setTextStyle(SignalServiceProtos.TextAttachment.Style.REGULAR);
                    break;
                case 3:
                    newBuilder.setTextStyle(SignalServiceProtos.TextAttachment.Style.BOLD);
                    break;
                case 4:
                    newBuilder.setTextStyle(SignalServiceProtos.TextAttachment.Style.SERIF);
                    break;
                case 5:
                    newBuilder.setTextStyle(SignalServiceProtos.TextAttachment.Style.SCRIPT);
                    break;
                case 6:
                    newBuilder.setTextStyle(SignalServiceProtos.TextAttachment.Style.CONDENSED);
                    break;
                default:
                    throw new AssertionError("Unknown type: " + signalServiceTextAttachment.getStyle().get());
            }
        }
        SignalServiceProtos.TextAttachment.Gradient.Builder newBuilder2 = SignalServiceProtos.TextAttachment.Gradient.newBuilder();
        if (signalServiceTextAttachment.getBackgroundGradient().isPresent()) {
            SignalServiceTextAttachment.Gradient gradient = signalServiceTextAttachment.getBackgroundGradient().get();
            if (gradient.getStartColor().isPresent()) {
                newBuilder2.setStartColor(gradient.getStartColor().get().intValue());
            }
            if (gradient.getEndColor().isPresent()) {
                newBuilder2.setEndColor(gradient.getEndColor().get().intValue());
            }
            if (gradient.getAngle().isPresent()) {
                newBuilder2.setAngle(gradient.getAngle().get().intValue());
            }
            newBuilder.setGradient(newBuilder2.build());
        }
        if (signalServiceTextAttachment.getText().isPresent()) {
            newBuilder.setText(signalServiceTextAttachment.getText().get());
        }
        if (signalServiceTextAttachment.getTextForegroundColor().isPresent()) {
            newBuilder.setTextForegroundColor(signalServiceTextAttachment.getTextForegroundColor().get().intValue());
        }
        if (signalServiceTextAttachment.getTextBackgroundColor().isPresent()) {
            newBuilder.setTextBackgroundColor(signalServiceTextAttachment.getTextBackgroundColor().get().intValue());
        }
        if (signalServiceTextAttachment.getPreview().isPresent()) {
            newBuilder.setPreview(createPreview(signalServiceTextAttachment.getPreview().get()));
        }
        if (signalServiceTextAttachment.getBackgroundColor().isPresent()) {
            newBuilder.setColor(signalServiceTextAttachment.getBackgroundColor().get().intValue());
        }
        return newBuilder.build();
    }

    /* renamed from: org.whispersystems.signalservice.api.SignalServiceMessageSender$1 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type;

        static {
            int[] iArr = new int[SignalServiceTextAttachment.Style.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style = iArr;
            try {
                iArr[SignalServiceTextAttachment.Style.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.REGULAR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.BOLD.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.SERIF.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.SCRIPT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.CONDENSED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[SharedContact.Phone.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type = iArr2;
            try {
                iArr2[SharedContact.Phone.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[SharedContact.Phone.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[SharedContact.Phone.Type.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[SharedContact.Phone.Type.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr3 = new int[SharedContact.Email.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type = iArr3;
            try {
                iArr3[SharedContact.Email.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[SharedContact.Email.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[SharedContact.Email.Type.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[SharedContact.Email.Type.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused14) {
            }
            int[] iArr4 = new int[SharedContact.PostalAddress.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type = iArr4;
            try {
                iArr4[SharedContact.PostalAddress.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type[SharedContact.PostalAddress.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type[SharedContact.PostalAddress.Type.CUSTOM.ordinal()] = 3;
            } catch (NoSuchFieldError unused17) {
            }
            int[] iArr5 = new int[VerifiedMessage.VerifiedState.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState = iArr5;
            try {
                iArr5[VerifiedMessage.VerifiedState.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState[VerifiedMessage.VerifiedState.VERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$VerifiedMessage$VerifiedState[VerifiedMessage.VerifiedState.UNVERIFIED.ordinal()] = 3;
            } catch (NoSuchFieldError unused20) {
            }
            int[] iArr6 = new int[MessageRequestResponseMessage.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type = iArr6;
            try {
                iArr6[MessageRequestResponseMessage.Type.ACCEPT.ordinal()] = 1;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[MessageRequestResponseMessage.Type.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[MessageRequestResponseMessage.Type.BLOCK.ordinal()] = 3;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[MessageRequestResponseMessage.Type.BLOCK_AND_DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused24) {
            }
            int[] iArr7 = new int[SignalServiceSyncMessage.FetchType.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType = iArr7;
            try {
                iArr7[SignalServiceSyncMessage.FetchType.LOCAL_PROFILE.ordinal()] = 1;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType[SignalServiceSyncMessage.FetchType.STORAGE_MANIFEST.ordinal()] = 2;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType[SignalServiceSyncMessage.FetchType.SUBSCRIPTION_STATUS.ordinal()] = 3;
            } catch (NoSuchFieldError unused27) {
            }
            int[] iArr8 = new int[StickerPackOperationMessage.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type = iArr8;
            try {
                iArr8[StickerPackOperationMessage.Type.INSTALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused28) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type[StickerPackOperationMessage.Type.REMOVE.ordinal()] = 2;
            } catch (NoSuchFieldError unused29) {
            }
        }
    }

    private OutgoingPushMessageList getEncryptedMessages(PushServiceSocket pushServiceSocket, SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccess> optional, long j, EnvelopeContent envelopeContent, boolean z) throws IOException, InvalidKeyException, UntrustedIdentityException {
        LinkedList linkedList = new LinkedList();
        List<Integer> subDeviceSessions = this.store.getSubDeviceSessions(signalServiceAddress.getIdentifier());
        ArrayList<Integer> arrayList = new ArrayList(subDeviceSessions.size() + 1);
        arrayList.add(1);
        arrayList.addAll(subDeviceSessions);
        if (signalServiceAddress.matches(this.localAddress)) {
            arrayList.remove(Integer.valueOf(this.localDeviceId));
        }
        for (Integer num : arrayList) {
            int intValue = num.intValue();
            if (intValue == 1 || this.store.containsSession(new SignalProtocolAddress(signalServiceAddress.getIdentifier(), intValue))) {
                linkedList.add(getEncryptedMessage(pushServiceSocket, signalServiceAddress, optional, intValue, envelopeContent));
            }
        }
        return new OutgoingPushMessageList(signalServiceAddress.getIdentifier(), j, linkedList, z);
    }

    private OutgoingPushMessage getEncryptedMessage(PushServiceSocket pushServiceSocket, SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccess> optional, int i, EnvelopeContent envelopeContent) throws IOException, InvalidKeyException, UntrustedIdentityException {
        SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(signalServiceAddress.getIdentifier(), i);
        SignalServiceCipher signalServiceCipher = new SignalServiceCipher(this.localAddress, this.localDeviceId, this.store, this.sessionLock, null);
        if (!this.store.containsSession(signalProtocolAddress)) {
            try {
                for (PreKeyBundle preKeyBundle : pushServiceSocket.getPreKeys(signalServiceAddress, optional, i)) {
                    try {
                        new SignalSessionBuilder(this.sessionLock, new SessionBuilder(this.store, new SignalProtocolAddress(signalServiceAddress.getIdentifier(), preKeyBundle.getDeviceId()))).process(preKeyBundle);
                    } catch (org.signal.libsignal.protocol.UntrustedIdentityException unused) {
                        throw new UntrustedIdentityException("Untrusted identity key!", signalServiceAddress.getIdentifier(), preKeyBundle.getIdentityKey());
                    }
                }
                if (this.eventListener.isPresent()) {
                    this.eventListener.get().onSecurityEvent(signalServiceAddress);
                }
            } catch (InvalidKeyException e) {
                throw new IOException(e);
            }
        }
        try {
            return signalServiceCipher.encrypt(signalProtocolAddress, optional, envelopeContent);
        } catch (org.signal.libsignal.protocol.UntrustedIdentityException e2) {
            throw new UntrustedIdentityException("Untrusted on send", signalServiceAddress.getIdentifier(), e2.getUntrustedIdentity());
        }
    }

    private void handleMismatchedDevices(PushServiceSocket pushServiceSocket, SignalServiceAddress signalServiceAddress, MismatchedDevices mismatchedDevices) throws IOException, UntrustedIdentityException {
        try {
            String str = TAG;
            Log.w(str, "[handleMismatchedDevices] Address: " + signalServiceAddress.getIdentifier() + ", ExtraDevices: " + mismatchedDevices.getExtraDevices() + ", MissingDevices: " + mismatchedDevices.getMissingDevices());
            archiveSessions(signalServiceAddress, mismatchedDevices.getExtraDevices());
            for (Integer num : mismatchedDevices.getMissingDevices()) {
                int intValue = num.intValue();
                PreKeyBundle preKey = pushServiceSocket.getPreKey(signalServiceAddress, intValue);
                try {
                    new SignalSessionBuilder(this.sessionLock, new SessionBuilder(this.store, new SignalProtocolAddress(signalServiceAddress.getIdentifier(), intValue))).process(preKey);
                } catch (org.signal.libsignal.protocol.UntrustedIdentityException unused) {
                    throw new UntrustedIdentityException("Untrusted identity key!", signalServiceAddress.getIdentifier(), preKey.getIdentityKey());
                }
            }
        } catch (InvalidKeyException e) {
            throw new IOException(e);
        }
    }

    private void handleStaleDevices(SignalServiceAddress signalServiceAddress, StaleDevices staleDevices) {
        String str = TAG;
        Log.w(str, "[handleStaleDevices] Address: " + signalServiceAddress.getIdentifier() + ", StaleDevices: " + staleDevices.getStaleDevices());
        archiveSessions(signalServiceAddress, staleDevices.getStaleDevices());
    }

    private void archiveSessions(SignalServiceAddress signalServiceAddress, List<Integer> list) {
        for (SignalProtocolAddress signalProtocolAddress : convertToProtocolAddresses(signalServiceAddress, list)) {
            this.store.archiveSession(signalProtocolAddress);
        }
    }

    private List<SignalProtocolAddress> convertToProtocolAddresses(SignalServiceAddress signalServiceAddress, List<Integer> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (Integer num : list) {
            int intValue = num.intValue();
            arrayList.add(new SignalProtocolAddress(signalServiceAddress.getServiceId().toString(), intValue));
            if (signalServiceAddress.getNumber().isPresent()) {
                arrayList.add(new SignalProtocolAddress(signalServiceAddress.getNumber().get(), intValue));
            }
        }
        return arrayList;
    }

    private Optional<UnidentifiedAccess> getTargetUnidentifiedAccess(Optional<UnidentifiedAccessPair> optional) {
        if (optional.isPresent()) {
            return optional.get().getTargetUnidentifiedAccess();
        }
        return Optional.empty();
    }

    private List<Optional<UnidentifiedAccess>> getTargetUnidentifiedAccess(List<Optional<UnidentifiedAccessPair>> list) {
        LinkedList linkedList = new LinkedList();
        for (Optional<UnidentifiedAccessPair> optional : list) {
            if (optional.isPresent()) {
                linkedList.add(optional.get().getTargetUnidentifiedAccess());
            } else {
                linkedList.add(Optional.empty());
            }
        }
        return linkedList;
    }

    private EnvelopeContent enforceMaxContentSize(EnvelopeContent envelopeContent) {
        int size = envelopeContent.size();
        long j = this.maxEnvelopeSize;
        if (j > 0) {
            long j2 = (long) size;
            if (j2 > j) {
                throw new ContentTooLargeException(j2);
            }
        }
        return envelopeContent;
    }

    private SignalServiceProtos.Content enforceMaxContentSize(SignalServiceProtos.Content content) {
        int length = content.toByteArray().length;
        long j = this.maxEnvelopeSize;
        if (j > 0) {
            long j2 = (long) length;
            if (j2 > j) {
                throw new ContentTooLargeException(j2);
            }
        }
        return content;
    }
}
