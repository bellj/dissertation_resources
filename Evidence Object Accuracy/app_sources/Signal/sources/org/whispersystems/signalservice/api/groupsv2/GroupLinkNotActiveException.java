package org.whispersystems.signalservice.api.groupsv2;

import j$.util.Optional;

/* loaded from: classes5.dex */
public final class GroupLinkNotActiveException extends Exception {
    private final Reason reason;

    /* loaded from: classes5.dex */
    public enum Reason {
        UNKNOWN,
        BANNED
    }

    public GroupLinkNotActiveException(Throwable th, Optional<String> optional) {
        super(th);
        if (!optional.isPresent() || !optional.get().equalsIgnoreCase("banned")) {
            this.reason = Reason.UNKNOWN;
        } else {
            this.reason = Reason.BANNED;
        }
    }

    public Reason getReason() {
        return this.reason;
    }
}
