package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.api.util.ProtoUtil;
import org.whispersystems.signalservice.internal.storage.protos.ContactRecord;

/* loaded from: classes5.dex */
public final class SignalContactRecord implements SignalRecord {
    private static final String TAG;
    private final SignalServiceAddress address;
    private final Optional<String> familyName;
    private final Optional<String> givenName;
    private final boolean hasUnknownFields;
    private final StorageId id;
    private final Optional<byte[]> identityKey;
    private final Optional<byte[]> profileKey;
    private final ContactRecord proto;
    private final Optional<String> username;

    public SignalContactRecord(StorageId storageId, ContactRecord contactRecord) {
        this.id = storageId;
        this.proto = contactRecord;
        this.hasUnknownFields = ProtoUtil.hasUnknownFields(contactRecord);
        this.address = new SignalServiceAddress(ServiceId.parseOrUnknown(contactRecord.getServiceUuid()), contactRecord.getServiceE164());
        this.givenName = OptionalUtil.absentIfEmpty(contactRecord.getGivenName());
        this.familyName = OptionalUtil.absentIfEmpty(contactRecord.getFamilyName());
        this.profileKey = OptionalUtil.absentIfEmpty(contactRecord.getProfileKey());
        this.username = OptionalUtil.absentIfEmpty(contactRecord.getUsername());
        this.identityKey = OptionalUtil.absentIfEmpty(contactRecord.getIdentityKey());
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public StorageId getId() {
        return this.id;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public SignalStorageRecord asStorageRecord() {
        return SignalStorageRecord.forContact(this);
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public String describeDiff(SignalRecord signalRecord) {
        if (signalRecord instanceof SignalContactRecord) {
            SignalContactRecord signalContactRecord = (SignalContactRecord) signalRecord;
            LinkedList linkedList = new LinkedList();
            if (!Arrays.equals(this.id.getRaw(), signalContactRecord.id.getRaw())) {
                linkedList.add("ID");
            }
            if (!Objects.equals(getAddress().getNumber(), signalContactRecord.getAddress().getNumber())) {
                linkedList.add("E164");
            }
            if (!Objects.equals(getAddress().getServiceId(), signalContactRecord.getAddress().getServiceId())) {
                linkedList.add("UUID");
            }
            if (!Objects.equals(this.givenName, signalContactRecord.givenName)) {
                linkedList.add("GivenName");
            }
            if (!Objects.equals(this.familyName, signalContactRecord.familyName)) {
                linkedList.add("FamilyName");
            }
            if (!OptionalUtil.byteArrayEquals(this.profileKey, signalContactRecord.profileKey)) {
                linkedList.add("ProfileKey");
            }
            if (!Objects.equals(this.username, signalContactRecord.username)) {
                linkedList.add("Username");
            }
            if (!OptionalUtil.byteArrayEquals(this.identityKey, signalContactRecord.identityKey)) {
                linkedList.add("IdentityKey");
            }
            if (!Objects.equals(getIdentityState(), signalContactRecord.getIdentityState())) {
                linkedList.add("IdentityState");
            }
            if (!Objects.equals(Boolean.valueOf(isBlocked()), Boolean.valueOf(signalContactRecord.isBlocked()))) {
                linkedList.add("Blocked");
            }
            if (!Objects.equals(Boolean.valueOf(isProfileSharingEnabled()), Boolean.valueOf(signalContactRecord.isProfileSharingEnabled()))) {
                linkedList.add("ProfileSharing");
            }
            if (!Objects.equals(Boolean.valueOf(isArchived()), Boolean.valueOf(signalContactRecord.isArchived()))) {
                linkedList.add("Archived");
            }
            if (!Objects.equals(Boolean.valueOf(isForcedUnread()), Boolean.valueOf(signalContactRecord.isForcedUnread()))) {
                linkedList.add("ForcedUnread");
            }
            if (!Objects.equals(Long.valueOf(getMuteUntil()), Long.valueOf(signalContactRecord.getMuteUntil()))) {
                linkedList.add("MuteUntil");
            }
            if (shouldHideStory() != signalContactRecord.shouldHideStory()) {
                linkedList.add("HideStory");
            }
            if (!Objects.equals(Boolean.valueOf(hasUnknownFields()), Boolean.valueOf(signalContactRecord.hasUnknownFields()))) {
                linkedList.add("UnknownFields");
            }
            return linkedList.toString();
        }
        return "Different class. " + SignalContactRecord.class.getSimpleName() + " | " + signalRecord.getClass().getSimpleName();
    }

    public boolean hasUnknownFields() {
        return this.hasUnknownFields;
    }

    public byte[] serializeUnknownFields() {
        if (this.hasUnknownFields) {
            return this.proto.toByteArray();
        }
        return null;
    }

    public SignalServiceAddress getAddress() {
        return this.address;
    }

    public Optional<String> getGivenName() {
        return this.givenName;
    }

    public Optional<String> getFamilyName() {
        return this.familyName;
    }

    public Optional<byte[]> getProfileKey() {
        return this.profileKey;
    }

    public Optional<String> getUsername() {
        return this.username;
    }

    public Optional<byte[]> getIdentityKey() {
        return this.identityKey;
    }

    public ContactRecord.IdentityState getIdentityState() {
        return this.proto.getIdentityState();
    }

    public boolean isBlocked() {
        return this.proto.getBlocked();
    }

    public boolean isProfileSharingEnabled() {
        return this.proto.getWhitelisted();
    }

    public boolean isArchived() {
        return this.proto.getArchived();
    }

    public boolean isForcedUnread() {
        return this.proto.getMarkedUnread();
    }

    public long getMuteUntil() {
        return this.proto.getMutedUntilTimestamp();
    }

    public boolean shouldHideStory() {
        return this.proto.getHideStory();
    }

    public ContactRecord toProto() {
        return this.proto;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SignalContactRecord.class != obj.getClass()) {
            return false;
        }
        SignalContactRecord signalContactRecord = (SignalContactRecord) obj;
        if (!this.id.equals(signalContactRecord.id) || !this.proto.equals(signalContactRecord.proto)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.proto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        private final ContactRecord.Builder builder;
        private final StorageId id;

        public Builder(byte[] bArr, SignalServiceAddress signalServiceAddress, byte[] bArr2) {
            this.id = StorageId.forContact(bArr);
            if (bArr2 != null) {
                this.builder = parseUnknowns(bArr2);
            } else {
                this.builder = ContactRecord.newBuilder();
            }
            this.builder.setServiceUuid(signalServiceAddress.getServiceId().toString());
            this.builder.setServiceE164(signalServiceAddress.getNumber().orElse(""));
        }

        public Builder setGivenName(String str) {
            ContactRecord.Builder builder = this.builder;
            if (str == null) {
                str = "";
            }
            builder.setGivenName(str);
            return this;
        }

        public Builder setFamilyName(String str) {
            ContactRecord.Builder builder = this.builder;
            if (str == null) {
                str = "";
            }
            builder.setFamilyName(str);
            return this;
        }

        public Builder setProfileKey(byte[] bArr) {
            this.builder.setProfileKey(bArr == null ? ByteString.EMPTY : ByteString.copyFrom(bArr));
            return this;
        }

        public Builder setUsername(String str) {
            ContactRecord.Builder builder = this.builder;
            if (str == null) {
                str = "";
            }
            builder.setUsername(str);
            return this;
        }

        public Builder setIdentityKey(byte[] bArr) {
            this.builder.setIdentityKey(bArr == null ? ByteString.EMPTY : ByteString.copyFrom(bArr));
            return this;
        }

        public Builder setIdentityState(ContactRecord.IdentityState identityState) {
            ContactRecord.Builder builder = this.builder;
            if (identityState == null) {
                identityState = ContactRecord.IdentityState.DEFAULT;
            }
            builder.setIdentityState(identityState);
            return this;
        }

        public Builder setBlocked(boolean z) {
            this.builder.setBlocked(z);
            return this;
        }

        public Builder setProfileSharingEnabled(boolean z) {
            this.builder.setWhitelisted(z);
            return this;
        }

        public Builder setArchived(boolean z) {
            this.builder.setArchived(z);
            return this;
        }

        public Builder setForcedUnread(boolean z) {
            this.builder.setMarkedUnread(z);
            return this;
        }

        public Builder setMuteUntil(long j) {
            this.builder.setMutedUntilTimestamp(j);
            return this;
        }

        public Builder setHideStory(boolean z) {
            this.builder.setHideStory(z);
            return this;
        }

        private static ContactRecord.Builder parseUnknowns(byte[] bArr) {
            try {
                return ContactRecord.parseFrom(bArr).toBuilder();
            } catch (InvalidProtocolBufferException e) {
                Log.w(SignalContactRecord.TAG, "Failed to combine unknown fields!", e);
                return ContactRecord.newBuilder();
            }
        }

        public SignalContactRecord build() {
            return new SignalContactRecord(this.id, this.builder.build());
        }
    }
}
