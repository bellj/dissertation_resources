package org.whispersystems.signalservice.api;

import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes5.dex */
public interface SignalServiceDataStore {
    SignalServiceAccountDataStore aci();

    SignalServiceAccountDataStore get(ServiceId serviceId);

    boolean isMultiDevice();

    SignalServiceAccountDataStore pni();
}
