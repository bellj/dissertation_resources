package org.whispersystems.signalservice.api.push.exceptions;

/* loaded from: classes5.dex */
public class UsernameTakenException extends NonSuccessfulResponseCodeException {
    public UsernameTakenException() {
        super(409);
    }
}
