package org.whispersystems.signalservice.api.crypto;

import androidx.recyclerview.widget.RecyclerView;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.protocol.InvalidMacException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.signal.libsignal.protocol.kdf.HKDFv3;
import org.whispersystems.signalservice.internal.util.ContentLengthInputStream;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class AttachmentCipherInputStream extends FilterInputStream {
    private static final int BLOCK_SIZE;
    private static final int CIPHER_KEY_SIZE;
    private static final int MAC_KEY_SIZE;
    private Cipher cipher;
    private boolean done;
    private byte[] overflowBuffer;
    private long totalDataSize;
    private long totalRead;

    @Override // java.io.FilterInputStream, java.io.InputStream
    public boolean markSupported() {
        return false;
    }

    public static InputStream createForAttachment(File file, long j, byte[] bArr, byte[] bArr2) throws InvalidMessageException, IOException {
        Object e;
        try {
            byte[][] split = Util.split(bArr, 32, 32);
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(split[1], "HmacSHA256"));
            if (file.length() <= ((long) (instance.getMacLength() + 16))) {
                throw new InvalidMessageException("Message shorter than crypto overhead!");
            } else if (bArr2 != null) {
                FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    verifyMac(fileInputStream, file.length(), instance, bArr2);
                    fileInputStream.close();
                    AttachmentCipherInputStream attachmentCipherInputStream = new AttachmentCipherInputStream(new FileInputStream(file), split[0], (file.length() - 16) - ((long) instance.getMacLength()));
                    return j != 0 ? new ContentLengthInputStream(attachmentCipherInputStream, j) : attachmentCipherInputStream;
                } catch (Throwable th) {
                    try {
                        fileInputStream.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            } else {
                throw new InvalidMacException("Missing digest!");
            }
        } catch (InvalidKeyException e2) {
            e = e2;
            throw new AssertionError(e);
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            throw new AssertionError(e);
        } catch (InvalidMacException e4) {
            throw new InvalidMessageException(e4);
        }
    }

    public static InputStream createForStickerData(byte[] bArr, byte[] bArr2) throws InvalidMessageException, IOException {
        Object e;
        try {
            new HKDFv3();
            byte[][] split = Util.split(HKDF.deriveSecrets(bArr2, "Sticker Pack".getBytes(), 64), 32, 32);
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(split[1], "HmacSHA256"));
            if (bArr.length > instance.getMacLength() + 16) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
                try {
                    verifyMac(byteArrayInputStream, (long) bArr.length, instance, null);
                    byteArrayInputStream.close();
                    return new AttachmentCipherInputStream(new ByteArrayInputStream(bArr), split[0], (long) ((bArr.length - 16) - instance.getMacLength()));
                } catch (Throwable th) {
                    try {
                        byteArrayInputStream.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            } else {
                throw new InvalidMessageException("Message shorter than crypto overhead!");
            }
        } catch (InvalidKeyException e2) {
            e = e2;
            throw new AssertionError(e);
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            throw new AssertionError(e);
        } catch (InvalidMacException e4) {
            throw new InvalidMessageException(e4);
        }
    }

    private AttachmentCipherInputStream(InputStream inputStream, byte[] bArr, long j) throws IOException {
        super(inputStream);
        try {
            byte[] bArr2 = new byte[16];
            readFully(bArr2);
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.cipher = instance;
            instance.init(2, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
            this.done = false;
            this.totalRead = 0;
            this.totalDataSize = j;
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        int read;
        byte[] bArr = new byte[1];
        do {
            read = read(bArr);
        } while (read == 0);
        if (read == -1) {
            return -1;
        }
        return bArr[0] & 255;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.totalRead != this.totalDataSize) {
            return readIncremental(bArr, i, i2);
        }
        if (!this.done) {
            return readFinal(bArr, i, i2);
        }
        return -1;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) throws IOException {
        long j2 = 0;
        while (j2 < j) {
            j2 += (long) read(new byte[Math.min((int) RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, (int) (j - j2))]);
        }
        return j2;
    }

    private int readFinal(byte[] bArr, int i, int i2) throws IOException {
        try {
            byte[] bArr2 = new byte[bArr.length];
            int min = Math.min(i2, this.cipher.doFinal(bArr2, 0));
            System.arraycopy(bArr2, 0, bArr, i, min);
            this.done = true;
            return min;
        } catch (BadPaddingException | IllegalBlockSizeException | ShortBufferException e) {
            throw new IOException(e);
        }
    }

    private int readIncremental(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        byte[] bArr2 = this.overflowBuffer;
        if (bArr2 == null) {
            i3 = i;
            i4 = 0;
        } else if (bArr2.length > i2) {
            System.arraycopy(bArr2, 0, bArr, i, i2);
            byte[] bArr3 = this.overflowBuffer;
            this.overflowBuffer = Arrays.copyOfRange(bArr3, i2, bArr3.length);
            return i2;
        } else if (bArr2.length == i2) {
            System.arraycopy(bArr2, 0, bArr, i, i2);
            this.overflowBuffer = null;
            return i2;
        } else {
            System.arraycopy(bArr2, 0, bArr, i, bArr2.length);
            i4 = this.overflowBuffer.length + 0;
            i2 -= i4;
            this.overflowBuffer = null;
            i3 = i + i4;
        }
        long j = this.totalRead;
        long j2 = this.totalDataSize;
        if (((long) i2) + j > j2) {
            i2 = (int) (j2 - j);
        }
        byte[] bArr4 = new byte[i2];
        int read = super.read(bArr4, 0, i2 <= this.cipher.getBlockSize() ? i2 : i2 - this.cipher.getBlockSize());
        this.totalRead += (long) read;
        try {
            int outputSize = this.cipher.getOutputSize(read);
            if (outputSize <= i2) {
                return i4 + this.cipher.update(bArr4, 0, read, bArr, i3);
            }
            byte[] bArr5 = new byte[outputSize];
            int update = this.cipher.update(bArr4, 0, read, bArr5, 0);
            if (update <= i2) {
                System.arraycopy(bArr5, 0, bArr, i3, update);
                return i4 + update;
            }
            System.arraycopy(bArr5, 0, bArr, i3, i2);
            this.overflowBuffer = Arrays.copyOfRange(bArr5, i2, update);
            return i4 + i2;
        } catch (ShortBufferException e) {
            throw new AssertionError(e);
        }
    }

    private static void verifyMac(InputStream inputStream, long j, Mac mac, byte[] bArr) throws InvalidMacException {
        Throwable e;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA256");
            int intExact = Util.toIntExact(j) - mac.getMacLength();
            byte[] bArr2 = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (intExact > 0) {
                int read = inputStream.read(bArr2, 0, Math.min((int) RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, intExact));
                mac.update(bArr2, 0, read);
                instance.update(bArr2, 0, read);
                intExact -= read;
            }
            byte[] doFinal = mac.doFinal();
            byte[] bArr3 = new byte[mac.getMacLength()];
            Util.readFully(inputStream, bArr3);
            if (MessageDigest.isEqual(doFinal, bArr3)) {
                byte[] digest = instance.digest(bArr3);
                if (bArr != null && !MessageDigest.isEqual(digest, bArr)) {
                    throw new InvalidMacException("Digest doesn't match!");
                }
                return;
            }
            throw new InvalidMacException("MAC doesn't match!");
        } catch (IOException e2) {
            e = e2;
            throw new InvalidMacException(e);
        } catch (ArithmeticException e3) {
            e = e3;
            throw new InvalidMacException(e);
        } catch (NoSuchAlgorithmException e4) {
            throw new AssertionError(e4);
        }
    }

    private void readFully(byte[] bArr) throws IOException {
        int i = 0;
        do {
            i += super.read(bArr, i, bArr.length - i);
        } while (i < bArr.length);
    }
}
