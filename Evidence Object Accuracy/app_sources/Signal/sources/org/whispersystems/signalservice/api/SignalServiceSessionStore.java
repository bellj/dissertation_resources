package org.whispersystems.signalservice.api;

import java.util.List;
import java.util.Set;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.SessionStore;

/* loaded from: classes5.dex */
public interface SignalServiceSessionStore extends SessionStore {
    void archiveSession(SignalProtocolAddress signalProtocolAddress);

    Set<SignalProtocolAddress> getAllAddressesWithActiveSessions(List<String> list);
}
