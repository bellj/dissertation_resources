package org.whispersystems.signalservice.api.messages.calls;

/* loaded from: classes5.dex */
public abstract class CallingResponse {
    private final long requestId;

    CallingResponse(long j) {
        this.requestId = j;
    }

    public long getRequestId() {
        return this.requestId;
    }

    /* loaded from: classes5.dex */
    public static class Success extends CallingResponse {
        private final byte[] responseBody;
        private final int responseStatus;

        public Success(long j, int i, byte[] bArr) {
            super(j);
            this.responseStatus = i;
            this.responseBody = bArr;
        }

        public int getResponseStatus() {
            return this.responseStatus;
        }

        public byte[] getResponseBody() {
            return this.responseBody;
        }
    }

    /* loaded from: classes5.dex */
    public static class Error extends CallingResponse {
        private final Throwable throwable;

        public Error(long j, Throwable th) {
            super(j);
            this.throwable = th;
        }

        public Throwable getThrowable() {
            return this.throwable;
        }
    }
}
