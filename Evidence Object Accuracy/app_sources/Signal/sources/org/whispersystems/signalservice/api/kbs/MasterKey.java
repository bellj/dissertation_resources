package org.whispersystems.signalservice.api.kbs;

import java.security.SecureRandom;
import java.util.Arrays;
import org.whispersystems.signalservice.api.crypto.CryptoUtil;
import org.whispersystems.signalservice.api.storage.StorageKey;
import org.whispersystems.signalservice.internal.util.Hex;
import org.whispersystems.util.StringUtil;

/* loaded from: classes5.dex */
public final class MasterKey {
    private static final int LENGTH;
    private final byte[] masterKey;

    public MasterKey(byte[] bArr) {
        if (bArr.length == 32) {
            this.masterKey = bArr;
            return;
        }
        throw new AssertionError();
    }

    public static MasterKey createNew(SecureRandom secureRandom) {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        return new MasterKey(bArr);
    }

    public String deriveRegistrationLock() {
        return Hex.toStringCondensed(derive("Registration Lock"));
    }

    public StorageKey deriveStorageServiceKey() {
        return new StorageKey(derive("Storage Service Encryption"));
    }

    private byte[] derive(String str) {
        return CryptoUtil.hmacSha256(this.masterKey, StringUtil.utf8(str));
    }

    public byte[] serialize() {
        return (byte[]) this.masterKey.clone();
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != MasterKey.class) {
            return false;
        }
        return Arrays.equals(((MasterKey) obj).masterKey, this.masterKey);
    }

    public int hashCode() {
        return Arrays.hashCode(this.masterKey);
    }
}
