package org.whispersystems.signalservice.api.messages.multidevice;

import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes5.dex */
public class RequestMessage {
    private final SignalServiceProtos.SyncMessage.Request request;

    public static RequestMessage forType(SignalServiceProtos.SyncMessage.Request.Type type) {
        return new RequestMessage(SignalServiceProtos.SyncMessage.Request.newBuilder().setType(type).build());
    }

    public RequestMessage(SignalServiceProtos.SyncMessage.Request request) {
        this.request = request;
    }

    public SignalServiceProtos.SyncMessage.Request getRequest() {
        return this.request;
    }

    public boolean isContactsRequest() {
        return this.request.getType() == SignalServiceProtos.SyncMessage.Request.Type.CONTACTS;
    }

    public boolean isGroupsRequest() {
        return this.request.getType() == SignalServiceProtos.SyncMessage.Request.Type.GROUPS;
    }

    public boolean isBlockedListRequest() {
        return this.request.getType() == SignalServiceProtos.SyncMessage.Request.Type.BLOCKED;
    }

    public boolean isConfigurationRequest() {
        return this.request.getType() == SignalServiceProtos.SyncMessage.Request.Type.CONFIGURATION;
    }

    public boolean isKeysRequest() {
        return this.request.getType() == SignalServiceProtos.SyncMessage.Request.Type.KEYS;
    }

    public boolean isPniIdentityRequest() {
        return this.request.getType() == SignalServiceProtos.SyncMessage.Request.Type.PNI_IDENTITY;
    }
}
