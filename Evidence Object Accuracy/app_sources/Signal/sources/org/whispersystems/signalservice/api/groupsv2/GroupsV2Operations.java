package org.whispersystems.signalservice.api.groupsv2;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.Comparator;
import j$.util.Optional;
import j$.util.stream.Collectors;
import j$.util.stream.Stream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.NotarySignature;
import org.signal.libsignal.zkgroup.ServerPublicParams;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.auth.ClientZkAuthOperations;
import org.signal.libsignal.zkgroup.groups.ClientZkGroupCipher;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.signal.libsignal.zkgroup.groups.ProfileKeyCiphertext;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyCredentialPresentation;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.BannedMember;
import org.signal.storageservice.protos.groups.Group;
import org.signal.storageservice.protos.groups.GroupAttributeBlob;
import org.signal.storageservice.protos.groups.GroupChange;
import org.signal.storageservice.protos.groups.GroupJoinInfo;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.PendingMember;
import org.signal.storageservice.protos.groups.RequestingMember;
import org.signal.storageservice.protos.groups.local.DecryptedApproveMember;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.signal.storageservice.protos.groups.local.DecryptedString;
import org.signal.storageservice.protos.groups.local.DecryptedTimer;
import org.signal.storageservice.protos.groups.local.EnabledState;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes5.dex */
public final class GroupsV2Operations {
    public static final int HIGHEST_KNOWN_EPOCH;
    private static final String TAG;
    public static final UUID UNKNOWN_UUID = UuidUtil.UNKNOWN_UUID;
    private final ClientZkAuthOperations clientZkAuthOperations;
    private final ClientZkProfileOperations clientZkProfileOperations;
    private final int maxGroupSize;
    private final SecureRandom random = new SecureRandom();
    private final ServerPublicParams serverPublicParams;

    public GroupsV2Operations(ClientZkOperations clientZkOperations, int i) {
        this.serverPublicParams = clientZkOperations.getServerPublicParams();
        this.clientZkProfileOperations = clientZkOperations.getProfileOperations();
        this.clientZkAuthOperations = clientZkOperations.getAuthOperations();
        this.maxGroupSize = i;
    }

    public NewGroup createNewGroup(GroupSecretParams groupSecretParams, String str, Optional<byte[]> optional, GroupCandidate groupCandidate, Set<GroupCandidate> set, Member.Role role, int i) {
        if (!set.contains(groupCandidate)) {
            GroupOperations forGroup = forGroup(groupSecretParams);
            Group.Builder disappearingMessagesTimer = Group.newBuilder().setRevision(0).setPublicKey(ByteString.copyFrom(groupSecretParams.getPublicParams().serialize())).setTitle(forGroup.encryptTitle(str)).setDisappearingMessagesTimer(forGroup.encryptTimer(i));
            AccessControl.Builder newBuilder = AccessControl.newBuilder();
            AccessControl.AccessRequired accessRequired = AccessControl.AccessRequired.MEMBER;
            Group.Builder accessControl = disappearingMessagesTimer.setAccessControl(newBuilder.setAttributes(accessRequired).setMembers(accessRequired));
            accessControl.addMembers(forGroup.member(groupCandidate.requireExpiringProfileKeyCredential(), Member.Role.ADMINISTRATOR));
            for (GroupCandidate groupCandidate2 : set) {
                ExpiringProfileKeyCredential orElse = groupCandidate2.getExpiringProfileKeyCredential().orElse(null);
                if (orElse != null) {
                    accessControl.addMembers(forGroup.member(orElse, role));
                } else {
                    accessControl.addPendingMembers(forGroup.invitee(groupCandidate2.getUuid(), role));
                }
            }
            return new NewGroup(groupSecretParams, accessControl.build(), optional);
        }
        throw new IllegalArgumentException("Members must not contain self");
    }

    public GroupOperations forGroup(GroupSecretParams groupSecretParams) {
        return new GroupOperations(groupSecretParams);
    }

    public ClientZkProfileOperations getProfileOperations() {
        return this.clientZkProfileOperations;
    }

    public ClientZkAuthOperations getAuthOperations() {
        return this.clientZkAuthOperations;
    }

    /* loaded from: classes5.dex */
    public final class GroupOperations {
        private final ClientZkGroupCipher clientZkGroupCipher;
        private final GroupSecretParams groupSecretParams;

        private GroupOperations(GroupSecretParams groupSecretParams) {
            GroupsV2Operations.this = r1;
            this.groupSecretParams = groupSecretParams;
            this.clientZkGroupCipher = new ClientZkGroupCipher(groupSecretParams);
        }

        public GroupChange.Actions.Builder createModifyGroupTitle(String str) {
            return GroupChange.Actions.newBuilder().setModifyTitle(GroupChange.Actions.ModifyTitleAction.newBuilder().setTitle(encryptTitle(str)));
        }

        public GroupChange.Actions.ModifyDescriptionAction.Builder createModifyGroupDescriptionAction(String str) {
            return GroupChange.Actions.ModifyDescriptionAction.newBuilder().setDescription(encryptDescription(str));
        }

        public GroupChange.Actions.Builder createModifyGroupDescription(String str) {
            return GroupChange.Actions.newBuilder().setModifyDescription(createModifyGroupDescriptionAction(str));
        }

        public GroupChange.Actions.Builder createModifyGroupMembershipChange(Set<GroupCandidate> set, Set<UUID> set2, UUID uuid) {
            GroupChange.Actions.Builder builder;
            GroupOperations forGroup = GroupsV2Operations.this.forGroup(this.groupSecretParams);
            Stream map = Collection$EL.stream(set).map(new GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda2());
            Objects.requireNonNull(set2);
            Set<UUID> set3 = (Set) map.filter(new GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda3(set2)).collect(Collectors.toSet());
            if (set3.isEmpty()) {
                builder = GroupChange.Actions.newBuilder();
            } else {
                builder = createUnbanUuidsChange(set3);
            }
            for (GroupCandidate groupCandidate : set) {
                Member.Role role = Member.Role.DEFAULT;
                ExpiringProfileKeyCredential orElse = groupCandidate.getExpiringProfileKeyCredential().orElse(null);
                if (orElse != null) {
                    builder.addAddMembers(GroupChange.Actions.AddMemberAction.newBuilder().setAdded(forGroup.member(orElse, role)));
                } else {
                    builder.addAddPendingMembers(GroupChange.Actions.AddPendingMemberAction.newBuilder().setAdded(forGroup.invitee(groupCandidate.getUuid(), role).setAddedByUserId(encryptUuid(uuid))));
                }
            }
            return builder;
        }

        public GroupChange.Actions.Builder createGroupJoinRequest(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
            GroupOperations forGroup = GroupsV2Operations.this.forGroup(this.groupSecretParams);
            GroupChange.Actions.Builder newBuilder = GroupChange.Actions.newBuilder();
            newBuilder.addAddRequestingMembers(GroupChange.Actions.AddRequestingMemberAction.newBuilder().setAdded(forGroup.requestingMember(expiringProfileKeyCredential)));
            return newBuilder;
        }

        public GroupChange.Actions.Builder createGroupJoinDirect(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
            GroupOperations forGroup = GroupsV2Operations.this.forGroup(this.groupSecretParams);
            GroupChange.Actions.Builder newBuilder = GroupChange.Actions.newBuilder();
            newBuilder.addAddMembers(GroupChange.Actions.AddMemberAction.newBuilder().setAdded(forGroup.member(expiringProfileKeyCredential, Member.Role.DEFAULT)));
            return newBuilder;
        }

        public GroupChange.Actions.Builder createRefuseGroupJoinRequest(Set<UUID> set, boolean z, List<DecryptedBannedMember> list) {
            GroupChange.Actions.Builder builder;
            if (z) {
                builder = createBanUuidsChange(set, false, list);
            } else {
                builder = GroupChange.Actions.newBuilder();
            }
            for (UUID uuid : set) {
                builder.addDeleteRequestingMembers(GroupChange.Actions.DeleteRequestingMemberAction.newBuilder().setDeletedUserId(encryptUuid(uuid)));
            }
            return builder;
        }

        public GroupChange.Actions.Builder createApproveGroupJoinRequest(Set<UUID> set) {
            GroupChange.Actions.Builder newBuilder = GroupChange.Actions.newBuilder();
            for (UUID uuid : set) {
                newBuilder.addPromoteRequestingMembers(GroupChange.Actions.PromoteRequestingMemberAction.newBuilder().setRole(Member.Role.DEFAULT).setUserId(encryptUuid(uuid)));
            }
            return newBuilder;
        }

        public GroupChange.Actions.Builder createRemoveMembersChange(Set<UUID> set, boolean z, List<DecryptedBannedMember> list) {
            GroupChange.Actions.Builder builder;
            if (z) {
                builder = createBanUuidsChange(set, false, list);
            } else {
                builder = GroupChange.Actions.newBuilder();
            }
            for (UUID uuid : set) {
                builder.addDeleteMembers(GroupChange.Actions.DeleteMemberAction.newBuilder().setDeletedUserId(encryptUuid(uuid)));
            }
            return builder;
        }

        public GroupChange.Actions.Builder createLeaveAndPromoteMembersToAdmin(UUID uuid, List<UUID> list) {
            GroupChange.Actions.Builder createRemoveMembersChange = createRemoveMembersChange(Collections.singleton(uuid), false, Collections.emptyList());
            for (UUID uuid2 : list) {
                createRemoveMembersChange.addModifyMemberRoles(GroupChange.Actions.ModifyMemberRoleAction.newBuilder().setUserId(encryptUuid(uuid2)).setRole(Member.Role.ADMINISTRATOR));
            }
            return createRemoveMembersChange;
        }

        public GroupChange.Actions.Builder createModifyGroupTimerChange(int i) {
            return GroupChange.Actions.newBuilder().setModifyDisappearingMessagesTimer(GroupChange.Actions.ModifyDisappearingMessagesTimerAction.newBuilder().setTimer(encryptTimer(i)));
        }

        public GroupChange.Actions.Builder createUpdateProfileKeyCredentialChange(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
            return GroupChange.Actions.newBuilder().addModifyMemberProfileKeys(GroupChange.Actions.ModifyMemberProfileKeyAction.newBuilder().setPresentation(ByteString.copyFrom(GroupsV2Operations.this.clientZkProfileOperations.createProfileKeyCredentialPresentation(GroupsV2Operations.this.random, this.groupSecretParams, expiringProfileKeyCredential).serialize())));
        }

        public GroupChange.Actions.Builder createAcceptInviteChange(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
            return GroupChange.Actions.newBuilder().addPromotePendingMembers(GroupChange.Actions.PromotePendingMemberAction.newBuilder().setPresentation(ByteString.copyFrom(GroupsV2Operations.this.clientZkProfileOperations.createProfileKeyCredentialPresentation(GroupsV2Operations.this.random, this.groupSecretParams, expiringProfileKeyCredential).serialize())));
        }

        public GroupChange.Actions.Builder createAcceptPniInviteChange(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
            return GroupChange.Actions.newBuilder().addPromotePendingPniAciMembers(GroupChange.Actions.PromotePendingPniAciMemberProfileKeyAction.newBuilder().setPresentation(ByteString.copyFrom(GroupsV2Operations.this.clientZkProfileOperations.createProfileKeyCredentialPresentation(GroupsV2Operations.this.random, this.groupSecretParams, expiringProfileKeyCredential).serialize())));
        }

        public GroupChange.Actions.Builder createRemoveInvitationChange(Set<UuidCiphertext> set) {
            GroupChange.Actions.Builder newBuilder = GroupChange.Actions.newBuilder();
            for (UuidCiphertext uuidCiphertext : set) {
                newBuilder.addDeletePendingMembers(GroupChange.Actions.DeletePendingMemberAction.newBuilder().setDeletedUserId(ByteString.copyFrom(uuidCiphertext.serialize())));
            }
            return newBuilder;
        }

        public GroupChange.Actions.Builder createModifyGroupLinkPasswordChange(byte[] bArr) {
            return GroupChange.Actions.newBuilder().setModifyInviteLinkPassword(GroupChange.Actions.ModifyInviteLinkPasswordAction.newBuilder().setInviteLinkPassword(ByteString.copyFrom(bArr)));
        }

        public GroupChange.Actions.Builder createModifyGroupLinkPasswordAndRightsChange(byte[] bArr, AccessControl.AccessRequired accessRequired) {
            return createModifyGroupLinkPasswordChange(bArr).setModifyAddFromInviteLinkAccess(GroupChange.Actions.ModifyAddFromInviteLinkAccessControlAction.newBuilder().setAddFromInviteLinkAccess(accessRequired));
        }

        public GroupChange.Actions.Builder createChangeJoinByLinkRights(AccessControl.AccessRequired accessRequired) {
            return GroupChange.Actions.newBuilder().setModifyAddFromInviteLinkAccess(GroupChange.Actions.ModifyAddFromInviteLinkAccessControlAction.newBuilder().setAddFromInviteLinkAccess(accessRequired));
        }

        public GroupChange.Actions.Builder createChangeMembershipRights(AccessControl.AccessRequired accessRequired) {
            return GroupChange.Actions.newBuilder().setModifyMemberAccess(GroupChange.Actions.ModifyMembersAccessControlAction.newBuilder().setMembersAccess(accessRequired));
        }

        public GroupChange.Actions.Builder createChangeAttributesRights(AccessControl.AccessRequired accessRequired) {
            return GroupChange.Actions.newBuilder().setModifyAttributesAccess(GroupChange.Actions.ModifyAttributesAccessControlAction.newBuilder().setAttributesAccess(accessRequired));
        }

        public GroupChange.Actions.Builder createAnnouncementGroupChange(boolean z) {
            return GroupChange.Actions.newBuilder().setModifyAnnouncementsOnly(GroupChange.Actions.ModifyAnnouncementsOnlyAction.newBuilder().setAnnouncementsOnly(z));
        }

        public GroupChange.Actions.Builder createBanUuidsChange(Set<UUID> set, boolean z, List<DecryptedBannedMember> list) {
            GroupChange.Actions.Builder builder;
            if (z) {
                builder = createRefuseGroupJoinRequest(set, false, Collections.emptyList());
            } else {
                builder = GroupChange.Actions.newBuilder();
            }
            int size = (list.size() + set.size()) - GroupsV2Operations.this.maxGroupSize;
            if (size > 0) {
                for (ByteString byteString : (List) Collection$EL.stream(list).sorted(Comparator.CC.comparingLong(new GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda0())).limit((long) size).map(new GroupsV2Operations$GroupOperations$$ExternalSyntheticLambda1()).collect(Collectors.toList())) {
                    builder.addDeleteBannedMembers(GroupChange.Actions.DeleteBannedMemberAction.newBuilder().setDeletedUserId(encryptUuid(UuidUtil.fromByteString(byteString))));
                }
            }
            for (UUID uuid : set) {
                builder.addAddBannedMembers(GroupChange.Actions.AddBannedMemberAction.newBuilder().setAdded(BannedMember.newBuilder().setUserId(encryptUuid(uuid)).build()));
            }
            return builder;
        }

        public GroupChange.Actions.Builder createUnbanUuidsChange(Set<UUID> set) {
            GroupChange.Actions.Builder newBuilder = GroupChange.Actions.newBuilder();
            for (UUID uuid : set) {
                newBuilder.addDeleteBannedMembers(GroupChange.Actions.DeleteBannedMemberAction.newBuilder().setDeletedUserId(encryptUuid(uuid)).build());
            }
            return newBuilder;
        }

        public GroupChange.Actions.Builder replaceAddMembers(GroupChange.Actions.Builder builder, List<GroupCandidate> list) throws InvalidInputException {
            if (builder.getAddMembersCount() == list.size()) {
                for (int i = 0; i < builder.getAddMembersCount(); i++) {
                    GroupChange.Actions.AddMemberAction addMembers = builder.getAddMembers(i);
                    ExpiringProfileKeyCredential orElse = list.get(i).getExpiringProfileKeyCredential().orElse(null);
                    if (orElse != null) {
                        builder.setAddMembers(i, GroupChange.Actions.AddMemberAction.newBuilder().setAdded(member(orElse, addMembers.getAdded().getRole())));
                    } else {
                        throw new InvalidInputException("Replacement candidate missing credential");
                    }
                }
                return builder;
            }
            throw new InvalidInputException("Replacement candidates not same size as original add");
        }

        public Member.Builder member(ExpiringProfileKeyCredential expiringProfileKeyCredential, Member.Role role) {
            return Member.newBuilder().setRole(role).setPresentation(ByteString.copyFrom(GroupsV2Operations.this.clientZkProfileOperations.createProfileKeyCredentialPresentation(new SecureRandom(), this.groupSecretParams, expiringProfileKeyCredential).serialize()));
        }

        private RequestingMember.Builder requestingMember(ExpiringProfileKeyCredential expiringProfileKeyCredential) {
            return RequestingMember.newBuilder().setPresentation(ByteString.copyFrom(GroupsV2Operations.this.clientZkProfileOperations.createProfileKeyCredentialPresentation(new SecureRandom(), this.groupSecretParams, expiringProfileKeyCredential).serialize()));
        }

        public PendingMember.Builder invitee(UUID uuid, Member.Role role) {
            UuidCiphertext encryptUuid = this.clientZkGroupCipher.encryptUuid(uuid);
            return PendingMember.newBuilder().setMember(Member.newBuilder().setRole(role).setUserId(ByteString.copyFrom(encryptUuid.serialize())).build());
        }

        public PartialDecryptedGroup partialDecryptGroup(Group group) throws VerificationFailedException, InvalidGroupStateException {
            List<Member> membersList = group.getMembersList();
            List<PendingMember> pendingMembersList = group.getPendingMembersList();
            ArrayList arrayList = new ArrayList(membersList.size());
            ArrayList arrayList2 = new ArrayList(pendingMembersList.size());
            for (Member member : membersList) {
                arrayList.add(DecryptedMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuid(member.getUserId()))).setJoinedAtRevision(member.getJoinedAtRevision()).build());
            }
            for (PendingMember pendingMember : pendingMembersList) {
                arrayList2.add(DecryptedPendingMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuidOrUnknown(pendingMember.getMember().getUserId()))).build());
            }
            return new PartialDecryptedGroup(group, DecryptedGroup.newBuilder().setRevision(group.getRevision()).addAllMembers(arrayList).addAllPendingMembers(arrayList2).build(), GroupsV2Operations.this, this.groupSecretParams);
        }

        public DecryptedGroup decryptGroup(Group group) throws VerificationFailedException, InvalidGroupStateException {
            List<Member> membersList = group.getMembersList();
            List<PendingMember> pendingMembersList = group.getPendingMembersList();
            List<RequestingMember> requestingMembersList = group.getRequestingMembersList();
            ArrayList arrayList = new ArrayList(membersList.size());
            ArrayList arrayList2 = new ArrayList(pendingMembersList.size());
            ArrayList arrayList3 = new ArrayList(requestingMembersList.size());
            ArrayList arrayList4 = new ArrayList(group.getBannedMembersCount());
            for (Member member : membersList) {
                try {
                    arrayList.add(decryptMember(member).build());
                } catch (InvalidInputException e) {
                    throw new InvalidGroupStateException(e);
                }
            }
            for (PendingMember pendingMember : pendingMembersList) {
                arrayList2.add(decryptMember(pendingMember));
            }
            for (RequestingMember requestingMember : requestingMembersList) {
                arrayList3.add(decryptRequestingMember(requestingMember));
            }
            for (BannedMember bannedMember : group.getBannedMembersList()) {
                arrayList4.add(DecryptedBannedMember.newBuilder().setUuid(decryptUuidToByteString(bannedMember.getUserId())).setTimestamp(bannedMember.getTimestamp()).build());
            }
            return DecryptedGroup.newBuilder().setTitle(decryptTitle(group.getTitle())).setDescription(decryptDescription(group.getDescription())).setIsAnnouncementGroup(group.getAnnouncementsOnly() ? EnabledState.ENABLED : EnabledState.DISABLED).setAvatar(group.getAvatar()).setAccessControl(group.getAccessControl()).setRevision(group.getRevision()).addAllMembers(arrayList).addAllPendingMembers(arrayList2).addAllRequestingMembers(arrayList3).setDisappearingMessagesTimer(DecryptedTimer.newBuilder().setDuration(decryptDisappearingMessagesTimer(group.getDisappearingMessagesTimer()))).setInviteLinkPassword(group.getInviteLinkPassword()).addAllBannedMembers(arrayList4).build();
        }

        public Optional<DecryptedGroupChange> decryptChange(GroupChange groupChange, boolean z) throws InvalidProtocolBufferException, VerificationFailedException, InvalidGroupStateException {
            if (groupChange.getChangeEpoch() > 4) {
                Log.w(GroupsV2Operations.TAG, String.format(Locale.US, "Ignoring change from Epoch %d. Highest known Epoch is %d", Integer.valueOf(groupChange.getChangeEpoch()), 4));
                return Optional.empty();
            }
            return Optional.of(decryptChange(z ? getVerifiedActions(groupChange) : getActions(groupChange)));
        }

        public DecryptedGroupChange decryptChange(GroupChange.Actions actions) throws VerificationFailedException, InvalidGroupStateException {
            return decryptChange(actions, (UUID) null);
        }

        public DecryptedGroupChange decryptChange(GroupChange.Actions actions, UUID uuid) throws VerificationFailedException, InvalidGroupStateException {
            UUID uuid2;
            ProfileKey profileKey;
            UUID uuid3;
            ProfileKey profileKey2;
            DecryptedGroupChange.Builder newBuilder = DecryptedGroupChange.newBuilder();
            if (uuid != null) {
                newBuilder.setEditor(UuidUtil.toByteString(uuid));
            } else {
                newBuilder.setEditor(decryptUuidToByteString(actions.getSourceUuid()));
            }
            newBuilder.setRevision(actions.getRevision());
            for (GroupChange.Actions.AddMemberAction addMemberAction : actions.getAddMembersList()) {
                try {
                    newBuilder.addNewMembers(decryptMember(addMemberAction.getAdded()).setJoinedAtRevision(actions.getRevision()));
                } catch (InvalidInputException e) {
                    throw new InvalidGroupStateException(e);
                }
            }
            for (GroupChange.Actions.DeleteMemberAction deleteMemberAction : actions.getDeleteMembersList()) {
                newBuilder.addDeleteMembers(decryptUuidToByteString(deleteMemberAction.getDeletedUserId()));
            }
            for (GroupChange.Actions.ModifyMemberRoleAction modifyMemberRoleAction : actions.getModifyMemberRolesList()) {
                newBuilder.addModifyMemberRoles(DecryptedModifyMemberRole.newBuilder().setRole(modifyMemberRoleAction.getRole()).setUuid(decryptUuidToByteString(modifyMemberRoleAction.getUserId())));
            }
            for (GroupChange.Actions.ModifyMemberProfileKeyAction modifyMemberProfileKeyAction : actions.getModifyMemberProfileKeysList()) {
                try {
                    if (!modifyMemberProfileKeyAction.getUserId().isEmpty() && !modifyMemberProfileKeyAction.getProfileKey().isEmpty()) {
                        uuid3 = decryptUuid(modifyMemberProfileKeyAction.getUserId());
                        profileKey2 = decryptProfileKey(modifyMemberProfileKeyAction.getProfileKey(), uuid3);
                        newBuilder.addModifiedProfileKeys(DecryptedMember.newBuilder().setRole(Member.Role.UNKNOWN).setJoinedAtRevision(-1).setUuid(UuidUtil.toByteString(uuid3)).setProfileKey(ByteString.copyFrom(profileKey2.serialize())));
                    }
                    ProfileKeyCredentialPresentation profileKeyCredentialPresentation = new ProfileKeyCredentialPresentation(modifyMemberProfileKeyAction.getPresentation().toByteArray());
                    UUID decryptUuid = decryptUuid(ByteString.copyFrom(profileKeyCredentialPresentation.getUuidCiphertext().serialize()));
                    profileKey2 = decryptProfileKey(ByteString.copyFrom(profileKeyCredentialPresentation.getProfileKeyCiphertext().serialize()), decryptUuid);
                    uuid3 = decryptUuid;
                    newBuilder.addModifiedProfileKeys(DecryptedMember.newBuilder().setRole(Member.Role.UNKNOWN).setJoinedAtRevision(-1).setUuid(UuidUtil.toByteString(uuid3)).setProfileKey(ByteString.copyFrom(profileKey2.serialize())));
                } catch (InvalidInputException e2) {
                    throw new InvalidGroupStateException(e2);
                }
            }
            for (GroupChange.Actions.AddPendingMemberAction addPendingMemberAction : actions.getAddPendingMembersList()) {
                PendingMember added = addPendingMemberAction.getAdded();
                Member member = added.getMember();
                ByteString userId = member.getUserId();
                newBuilder.addNewPendingMembers(DecryptedPendingMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuidOrUnknown(userId))).setUuidCipherText(userId).setRole(member.getRole()).setAddedByUuid(decryptUuidToByteString(added.getAddedByUserId())).setTimestamp(added.getTimestamp()));
            }
            for (GroupChange.Actions.DeletePendingMemberAction deletePendingMemberAction : actions.getDeletePendingMembersList()) {
                ByteString deletedUserId = deletePendingMemberAction.getDeletedUserId();
                newBuilder.addDeletePendingMembers(DecryptedPendingMemberRemoval.newBuilder().setUuid(UuidUtil.toByteString(decryptUuidOrUnknown(deletedUserId))).setUuidCipherText(deletedUserId));
            }
            for (GroupChange.Actions.PromotePendingMemberAction promotePendingMemberAction : actions.getPromotePendingMembersList()) {
                try {
                    if (!promotePendingMemberAction.getUserId().isEmpty() && !promotePendingMemberAction.getProfileKey().isEmpty()) {
                        uuid2 = decryptUuid(promotePendingMemberAction.getUserId());
                        profileKey = decryptProfileKey(promotePendingMemberAction.getProfileKey(), uuid2);
                        newBuilder.addPromotePendingMembers(DecryptedMember.newBuilder().setJoinedAtRevision(-1).setRole(Member.Role.DEFAULT).setUuid(UuidUtil.toByteString(uuid2)).setProfileKey(ByteString.copyFrom(profileKey.serialize())));
                    }
                    ProfileKeyCredentialPresentation profileKeyCredentialPresentation2 = new ProfileKeyCredentialPresentation(promotePendingMemberAction.getPresentation().toByteArray());
                    UUID decryptUuid2 = decryptUuid(ByteString.copyFrom(profileKeyCredentialPresentation2.getUuidCiphertext().serialize()));
                    profileKey = decryptProfileKey(ByteString.copyFrom(profileKeyCredentialPresentation2.getProfileKeyCiphertext().serialize()), decryptUuid2);
                    uuid2 = decryptUuid2;
                    newBuilder.addPromotePendingMembers(DecryptedMember.newBuilder().setJoinedAtRevision(-1).setRole(Member.Role.DEFAULT).setUuid(UuidUtil.toByteString(uuid2)).setProfileKey(ByteString.copyFrom(profileKey.serialize())));
                } catch (InvalidInputException e3) {
                    throw new InvalidGroupStateException(e3);
                }
            }
            if (actions.hasModifyTitle()) {
                newBuilder.setNewTitle(DecryptedString.newBuilder().setValue(decryptTitle(actions.getModifyTitle().getTitle())));
            }
            if (actions.hasModifyAvatar()) {
                newBuilder.setNewAvatar(DecryptedString.newBuilder().setValue(actions.getModifyAvatar().getAvatar()));
            }
            if (actions.hasModifyDisappearingMessagesTimer()) {
                newBuilder.setNewTimer(DecryptedTimer.newBuilder().setDuration(decryptDisappearingMessagesTimer(actions.getModifyDisappearingMessagesTimer().getTimer())));
            }
            if (actions.hasModifyAttributesAccess()) {
                newBuilder.setNewAttributeAccess(actions.getModifyAttributesAccess().getAttributesAccess());
            }
            if (actions.hasModifyMemberAccess()) {
                newBuilder.setNewMemberAccess(actions.getModifyMemberAccess().getMembersAccess());
            }
            if (actions.hasModifyAddFromInviteLinkAccess()) {
                newBuilder.setNewInviteLinkAccess(actions.getModifyAddFromInviteLinkAccess().getAddFromInviteLinkAccess());
            }
            for (GroupChange.Actions.AddRequestingMemberAction addRequestingMemberAction : actions.getAddRequestingMembersList()) {
                newBuilder.addNewRequestingMembers(decryptRequestingMember(addRequestingMemberAction.getAdded()));
            }
            for (GroupChange.Actions.DeleteRequestingMemberAction deleteRequestingMemberAction : actions.getDeleteRequestingMembersList()) {
                newBuilder.addDeleteRequestingMembers(decryptUuidToByteString(deleteRequestingMemberAction.getDeletedUserId()));
            }
            for (GroupChange.Actions.PromoteRequestingMemberAction promoteRequestingMemberAction : actions.getPromoteRequestingMembersList()) {
                newBuilder.addPromoteRequestingMembers(DecryptedApproveMember.newBuilder().setRole(promoteRequestingMemberAction.getRole()).setUuid(decryptUuidToByteString(promoteRequestingMemberAction.getUserId())));
            }
            if (actions.hasModifyInviteLinkPassword()) {
                newBuilder.setNewInviteLinkPassword(actions.getModifyInviteLinkPassword().getInviteLinkPassword());
            }
            if (actions.hasModifyDescription()) {
                newBuilder.setNewDescription(DecryptedString.newBuilder().setValue(decryptDescription(actions.getModifyDescription().getDescription())));
            }
            if (actions.hasModifyAnnouncementsOnly()) {
                newBuilder.setNewIsAnnouncementGroup(actions.getModifyAnnouncementsOnly().getAnnouncementsOnly() ? EnabledState.ENABLED : EnabledState.DISABLED);
            }
            for (GroupChange.Actions.AddBannedMemberAction addBannedMemberAction : actions.getAddBannedMembersList()) {
                newBuilder.addNewBannedMembers(DecryptedBannedMember.newBuilder().setUuid(decryptUuidToByteString(addBannedMemberAction.getAdded().getUserId())).setTimestamp(addBannedMemberAction.getAdded().getTimestamp()).build());
            }
            for (GroupChange.Actions.DeleteBannedMemberAction deleteBannedMemberAction : actions.getDeleteBannedMembersList()) {
                newBuilder.addDeleteBannedMembers(DecryptedBannedMember.newBuilder().setUuid(decryptUuidToByteString(deleteBannedMemberAction.getDeletedUserId())).build());
            }
            for (GroupChange.Actions.PromotePendingPniAciMemberProfileKeyAction promotePendingPniAciMemberProfileKeyAction : actions.getPromotePendingPniAciMembersList()) {
                UUID decryptUuid3 = decryptUuid(promotePendingPniAciMemberProfileKeyAction.getUserId());
                newBuilder.setEditor(UuidUtil.toByteString(decryptUuid3)).addPromotePendingPniAciMembers(DecryptedMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuid3)).setRole(Member.Role.DEFAULT).setProfileKey(ByteString.copyFrom(decryptProfileKey(promotePendingPniAciMemberProfileKeyAction.getProfileKey(), decryptUuid3).serialize())).setJoinedAtRevision(actions.getRevision()).setPni(UuidUtil.toByteString(decryptUuid(promotePendingPniAciMemberProfileKeyAction.getPni()))));
            }
            return newBuilder.build();
        }

        public DecryptedGroupJoinInfo decryptGroupJoinInfo(GroupJoinInfo groupJoinInfo) {
            return DecryptedGroupJoinInfo.newBuilder().setTitle(decryptTitle(groupJoinInfo.getTitle())).setAvatar(groupJoinInfo.getAvatar()).setMemberCount(groupJoinInfo.getMemberCount()).setAddFromInviteLink(groupJoinInfo.getAddFromInviteLink()).setRevision(groupJoinInfo.getRevision()).setPendingAdminApproval(groupJoinInfo.getPendingAdminApproval()).setDescription(decryptDescription(groupJoinInfo.getDescription())).build();
        }

        private DecryptedMember.Builder decryptMember(Member member) throws InvalidGroupStateException, VerificationFailedException, InvalidInputException {
            if (member.getPresentation().isEmpty()) {
                UUID decryptUuid = decryptUuid(member.getUserId());
                return DecryptedMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuid)).setJoinedAtRevision(member.getJoinedAtRevision()).setProfileKey(decryptProfileKeyToByteString(member.getProfileKey(), decryptUuid)).setRole(member.getRole());
            }
            ProfileKeyCredentialPresentation profileKeyCredentialPresentation = new ProfileKeyCredentialPresentation(member.getPresentation().toByteArray());
            UUID decryptUuid2 = this.clientZkGroupCipher.decryptUuid(profileKeyCredentialPresentation.getUuidCiphertext());
            return DecryptedMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuid2)).setJoinedAtRevision(member.getJoinedAtRevision()).setProfileKey(ByteString.copyFrom(this.clientZkGroupCipher.decryptProfileKey(profileKeyCredentialPresentation.getProfileKeyCiphertext(), decryptUuid2).serialize())).setRole(member.getRole());
        }

        private DecryptedPendingMember decryptMember(PendingMember pendingMember) throws InvalidGroupStateException, VerificationFailedException {
            Member.Role role;
            ByteString userId = pendingMember.getMember().getUserId();
            UUID decryptUuidOrUnknown = decryptUuidOrUnknown(userId);
            UUID decryptUuid = decryptUuid(pendingMember.getAddedByUserId());
            Member.Role role2 = pendingMember.getMember().getRole();
            if (!(role2 == Member.Role.ADMINISTRATOR || role2 == (role = Member.Role.DEFAULT))) {
                role2 = role;
            }
            return DecryptedPendingMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuidOrUnknown)).setUuidCipherText(userId).setAddedByUuid(UuidUtil.toByteString(decryptUuid)).setRole(role2).setTimestamp(pendingMember.getTimestamp()).build();
        }

        private DecryptedRequestingMember decryptRequestingMember(RequestingMember requestingMember) throws InvalidGroupStateException, VerificationFailedException {
            if (requestingMember.getPresentation().isEmpty()) {
                UUID decryptUuid = decryptUuid(requestingMember.getUserId());
                return DecryptedRequestingMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuid)).setProfileKey(decryptProfileKeyToByteString(requestingMember.getProfileKey(), decryptUuid)).setTimestamp(requestingMember.getTimestamp()).build();
            }
            try {
                ProfileKeyCredentialPresentation profileKeyCredentialPresentation = new ProfileKeyCredentialPresentation(requestingMember.getPresentation().toByteArray());
                UUID decryptUuid2 = this.clientZkGroupCipher.decryptUuid(profileKeyCredentialPresentation.getUuidCiphertext());
                return DecryptedRequestingMember.newBuilder().setUuid(UuidUtil.toByteString(decryptUuid2)).setProfileKey(ByteString.copyFrom(this.clientZkGroupCipher.decryptProfileKey(profileKeyCredentialPresentation.getProfileKeyCiphertext(), decryptUuid2).serialize())).build();
            } catch (InvalidInputException e) {
                throw new InvalidGroupStateException(e);
            }
        }

        private ProfileKey decryptProfileKey(ByteString byteString, UUID uuid) throws VerificationFailedException, InvalidGroupStateException {
            try {
                return this.clientZkGroupCipher.decryptProfileKey(new ProfileKeyCiphertext(byteString.toByteArray()), uuid);
            } catch (InvalidInputException e) {
                throw new InvalidGroupStateException(e);
            }
        }

        private ByteString decryptProfileKeyToByteString(ByteString byteString, UUID uuid) throws VerificationFailedException, InvalidGroupStateException {
            return ByteString.copyFrom(decryptProfileKey(byteString, uuid).serialize());
        }

        private ByteString decryptUuidToByteString(ByteString byteString) throws InvalidGroupStateException, VerificationFailedException {
            return UuidUtil.toByteString(decryptUuid(byteString));
        }

        public ByteString encryptUuid(UUID uuid) {
            return ByteString.copyFrom(this.clientZkGroupCipher.encryptUuid(uuid).serialize());
        }

        private UUID decryptUuid(ByteString byteString) throws InvalidGroupStateException, VerificationFailedException {
            try {
                return this.clientZkGroupCipher.decryptUuid(new UuidCiphertext(byteString.toByteArray()));
            } catch (InvalidInputException e) {
                throw new InvalidGroupStateException(e);
            }
        }

        private UUID decryptUuidOrUnknown(ByteString byteString) {
            try {
                return this.clientZkGroupCipher.decryptUuid(new UuidCiphertext(byteString.toByteArray()));
            } catch (InvalidInputException | VerificationFailedException unused) {
                return GroupsV2Operations.UNKNOWN_UUID;
            }
        }

        ByteString encryptTitle(String str) {
            try {
                return ByteString.copyFrom(this.clientZkGroupCipher.encryptBlob(GroupAttributeBlob.newBuilder().setTitle(str).build().toByteArray()));
            } catch (VerificationFailedException e) {
                throw new AssertionError(e);
            }
        }

        private String decryptTitle(ByteString byteString) {
            return decryptBlob(byteString).getTitle().trim();
        }

        ByteString encryptDescription(String str) {
            try {
                return ByteString.copyFrom(this.clientZkGroupCipher.encryptBlob(GroupAttributeBlob.newBuilder().setDescription(str).build().toByteArray()));
            } catch (VerificationFailedException e) {
                throw new AssertionError(e);
            }
        }

        private String decryptDescription(ByteString byteString) {
            return decryptBlob(byteString).getDescription().trim();
        }

        private int decryptDisappearingMessagesTimer(ByteString byteString) {
            return decryptBlob(byteString).getDisappearingMessagesDuration();
        }

        public byte[] decryptAvatar(byte[] bArr) {
            return decryptBlob(bArr).getAvatar().toByteArray();
        }

        private GroupAttributeBlob decryptBlob(ByteString byteString) {
            return decryptBlob(byteString.toByteArray());
        }

        private GroupAttributeBlob decryptBlob(byte[] bArr) {
            if (bArr == null || bArr.length == 0) {
                return GroupAttributeBlob.getDefaultInstance();
            }
            if (bArr.length < 29) {
                Log.w(GroupsV2Operations.TAG, "Bad encrypted blob length");
                return GroupAttributeBlob.getDefaultInstance();
            }
            try {
                return GroupAttributeBlob.parseFrom(this.clientZkGroupCipher.decryptBlob(bArr));
            } catch (InvalidProtocolBufferException | VerificationFailedException unused) {
                Log.w(GroupsV2Operations.TAG, "Bad encrypted blob");
                return GroupAttributeBlob.getDefaultInstance();
            }
        }

        ByteString encryptTimer(int i) {
            try {
                return ByteString.copyFrom(this.clientZkGroupCipher.encryptBlob(GroupAttributeBlob.newBuilder().setDisappearingMessagesDuration(i).build().toByteArray()));
            } catch (VerificationFailedException e) {
                throw new AssertionError(e);
            }
        }

        private GroupChange.Actions getVerifiedActions(GroupChange groupChange) throws VerificationFailedException, InvalidProtocolBufferException {
            byte[] byteArray = groupChange.getActions().toByteArray();
            try {
                GroupsV2Operations.this.serverPublicParams.verifySignature(byteArray, new NotarySignature(groupChange.getServerSignature().toByteArray()));
                return GroupChange.Actions.parseFrom(byteArray);
            } catch (InvalidInputException unused) {
                throw new VerificationFailedException();
            }
        }

        private GroupChange.Actions getActions(GroupChange groupChange) throws InvalidProtocolBufferException {
            return GroupChange.Actions.parseFrom(groupChange.getActions());
        }

        public GroupChange.Actions.Builder createChangeMemberRole(UUID uuid, Member.Role role) {
            return GroupChange.Actions.newBuilder().addModifyMemberRoles(GroupChange.Actions.ModifyMemberRoleAction.newBuilder().setUserId(encryptUuid(uuid)).setRole(role));
        }

        public List<ServiceId> decryptAddMembers(List<GroupChange.Actions.AddMemberAction> list) throws InvalidInputException, VerificationFailedException {
            ArrayList arrayList = new ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(ServiceId.from(this.clientZkGroupCipher.decryptUuid(new ProfileKeyCredentialPresentation(list.get(i).getAdded().getPresentation().toByteArray()).getUuidCiphertext())));
            }
            return arrayList;
        }
    }

    /* loaded from: classes5.dex */
    public static class NewGroup {
        private final Optional<byte[]> avatar;
        private final GroupSecretParams groupSecretParams;
        private final Group newGroupMessage;

        private NewGroup(GroupSecretParams groupSecretParams, Group group, Optional<byte[]> optional) {
            this.groupSecretParams = groupSecretParams;
            this.newGroupMessage = group;
            this.avatar = optional;
        }

        public GroupSecretParams getGroupSecretParams() {
            return this.groupSecretParams;
        }

        public Group getNewGroupMessage() {
            return this.newGroupMessage;
        }

        public Optional<byte[]> getAvatar() {
            return this.avatar;
        }
    }
}
