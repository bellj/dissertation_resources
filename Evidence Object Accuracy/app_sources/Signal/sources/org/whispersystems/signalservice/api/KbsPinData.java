package org.whispersystems.signalservice.api;

import org.whispersystems.signalservice.api.kbs.MasterKey;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes5.dex */
public final class KbsPinData {
    private final MasterKey masterKey;
    private final TokenResponse tokenResponse;

    public KbsPinData(MasterKey masterKey, TokenResponse tokenResponse) {
        this.masterKey = masterKey;
        this.tokenResponse = tokenResponse;
    }

    public MasterKey getMasterKey() {
        return this.masterKey;
    }

    public TokenResponse getTokenResponse() {
        return this.tokenResponse;
    }

    public int getRemainingTries() {
        return this.tokenResponse.getTries();
    }
}
