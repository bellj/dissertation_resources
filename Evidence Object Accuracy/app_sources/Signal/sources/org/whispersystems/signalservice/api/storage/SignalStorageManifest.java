package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.whispersystems.signalservice.internal.storage.protos.ManifestRecord;
import org.whispersystems.signalservice.internal.storage.protos.StorageManifest;

/* loaded from: classes5.dex */
public class SignalStorageManifest {
    public static final SignalStorageManifest EMPTY = new SignalStorageManifest(0, Collections.emptyList());
    private final List<StorageId> storageIds;
    private final Map<Integer, List<StorageId>> storageIdsByType = new HashMap();
    private final long version;

    public SignalStorageManifest(long j, List<StorageId> list) {
        this.version = j;
        this.storageIds = list;
        for (StorageId storageId : list) {
            List<StorageId> list2 = this.storageIdsByType.get(Integer.valueOf(storageId.getType()));
            if (list2 == null) {
                list2 = new ArrayList<>();
            }
            list2.add(storageId);
            this.storageIdsByType.put(Integer.valueOf(storageId.getType()), list2);
        }
    }

    public static SignalStorageManifest deserialize(byte[] bArr) {
        try {
            StorageManifest parseFrom = StorageManifest.parseFrom(bArr);
            ManifestRecord parseFrom2 = ManifestRecord.parseFrom(parseFrom.getValue());
            ArrayList arrayList = new ArrayList(parseFrom2.getIdentifiersCount());
            for (ManifestRecord.Identifier identifier : parseFrom2.getIdentifiersList()) {
                arrayList.add(StorageId.forType(identifier.getRaw().toByteArray(), identifier.getTypeValue()));
            }
            return new SignalStorageManifest(parseFrom.getVersion(), arrayList);
        } catch (InvalidProtocolBufferException e) {
            throw new AssertionError(e);
        }
    }

    public long getVersion() {
        return this.version;
    }

    public List<StorageId> getStorageIds() {
        return this.storageIds;
    }

    public Optional<StorageId> getAccountStorageId() {
        List<StorageId> list = this.storageIdsByType.get(4);
        if (list == null || list.size() <= 0) {
            return Optional.empty();
        }
        return Optional.of(list.get(0));
    }

    public byte[] serialize() {
        ArrayList arrayList = new ArrayList(this.storageIds.size());
        for (StorageId storageId : this.storageIds) {
            arrayList.add(ManifestRecord.Identifier.newBuilder().setTypeValue(storageId.getType()).setRaw(ByteString.copyFrom(storageId.getRaw())).build());
        }
        return StorageManifest.newBuilder().setVersion(this.version).setValue(ManifestRecord.newBuilder().addAllIdentifiers(arrayList).build().toByteString()).build().toByteArray();
    }
}
