package org.whispersystems.signalservice.api.util;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import org.whispersystems.signalservice.api.InvalidMessageStructureException;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.util.FlagUtil;

/* loaded from: classes5.dex */
public final class AttachmentPointerUtil {
    public static SignalServiceAttachmentPointer createSignalAttachmentPointer(byte[] bArr) throws InvalidMessageStructureException, InvalidProtocolBufferException {
        return createSignalAttachmentPointer(SignalServiceProtos.AttachmentPointer.parseFrom(bArr));
    }

    public static SignalServiceAttachmentPointer createSignalAttachmentPointer(SignalServiceProtos.AttachmentPointer attachmentPointer) throws InvalidMessageStructureException {
        return new SignalServiceAttachmentPointer(attachmentPointer.getCdnNumber(), SignalServiceAttachmentRemoteId.from(attachmentPointer), attachmentPointer.getContentType(), attachmentPointer.getKey().toByteArray(), attachmentPointer.hasSize() ? Optional.of(Integer.valueOf(attachmentPointer.getSize())) : Optional.empty(), attachmentPointer.hasThumbnail() ? Optional.of(attachmentPointer.getThumbnail().toByteArray()) : Optional.empty(), attachmentPointer.getWidth(), attachmentPointer.getHeight(), attachmentPointer.hasDigest() ? Optional.of(attachmentPointer.getDigest().toByteArray()) : Optional.empty(), attachmentPointer.hasFileName() ? Optional.of(attachmentPointer.getFileName()) : Optional.empty(), (attachmentPointer.getFlags() & FlagUtil.toBinaryFlag(1)) != 0, (attachmentPointer.getFlags() & FlagUtil.toBinaryFlag(2)) != 0, (attachmentPointer.getFlags() & FlagUtil.toBinaryFlag(4)) != 0, attachmentPointer.hasCaption() ? Optional.of(attachmentPointer.getCaption()) : Optional.empty(), attachmentPointer.hasBlurHash() ? Optional.of(attachmentPointer.getBlurHash()) : Optional.empty(), attachmentPointer.hasUploadTimestamp() ? attachmentPointer.getUploadTimestamp() : 0);
    }

    public static SignalServiceProtos.AttachmentPointer createAttachmentPointer(SignalServiceAttachmentPointer signalServiceAttachmentPointer) {
        SignalServiceProtos.AttachmentPointer.Builder uploadTimestamp = SignalServiceProtos.AttachmentPointer.newBuilder().setCdnNumber(signalServiceAttachmentPointer.getCdnNumber()).setContentType(signalServiceAttachmentPointer.getContentType()).setKey(ByteString.copyFrom(signalServiceAttachmentPointer.getKey())).setDigest(ByteString.copyFrom(signalServiceAttachmentPointer.getDigest().get())).setSize(signalServiceAttachmentPointer.getSize().get().intValue()).setUploadTimestamp(signalServiceAttachmentPointer.getUploadTimestamp());
        if (signalServiceAttachmentPointer.getRemoteId().getV2().isPresent()) {
            uploadTimestamp.setCdnId(signalServiceAttachmentPointer.getRemoteId().getV2().get().longValue());
        }
        if (signalServiceAttachmentPointer.getRemoteId().getV3().isPresent()) {
            uploadTimestamp.setCdnKey(signalServiceAttachmentPointer.getRemoteId().getV3().get());
        }
        if (signalServiceAttachmentPointer.getFileName().isPresent()) {
            uploadTimestamp.setFileName(signalServiceAttachmentPointer.getFileName().get());
        }
        if (signalServiceAttachmentPointer.getPreview().isPresent()) {
            uploadTimestamp.setThumbnail(ByteString.copyFrom(signalServiceAttachmentPointer.getPreview().get()));
        }
        if (signalServiceAttachmentPointer.getWidth() > 0) {
            uploadTimestamp.setWidth(signalServiceAttachmentPointer.getWidth());
        }
        if (signalServiceAttachmentPointer.getHeight() > 0) {
            uploadTimestamp.setHeight(signalServiceAttachmentPointer.getHeight());
        }
        int i = 0;
        if (signalServiceAttachmentPointer.getVoiceNote()) {
            i = 0 | FlagUtil.toBinaryFlag(1);
        }
        if (signalServiceAttachmentPointer.isBorderless()) {
            i |= FlagUtil.toBinaryFlag(2);
        }
        if (signalServiceAttachmentPointer.isGif()) {
            i |= FlagUtil.toBinaryFlag(4);
        }
        uploadTimestamp.setFlags(i);
        if (signalServiceAttachmentPointer.getCaption().isPresent()) {
            uploadTimestamp.setCaption(signalServiceAttachmentPointer.getCaption().get());
        }
        if (signalServiceAttachmentPointer.getBlurHash().isPresent()) {
            uploadTimestamp.setBlurHash(signalServiceAttachmentPointer.getBlurHash().get());
        }
        return uploadTimestamp.build();
    }
}
