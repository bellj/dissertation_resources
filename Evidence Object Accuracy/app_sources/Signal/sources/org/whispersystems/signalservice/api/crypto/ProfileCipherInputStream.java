package org.whispersystems.signalservice.api.crypto;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.signal.libsignal.crypto.Aes256GcmDecryption;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class ProfileCipherInputStream extends FilterInputStream {
    private Aes256GcmDecryption aes;
    private byte[] buffer = new byte[16];
    private byte[] swapBuffer = new byte[16];

    public ProfileCipherInputStream(InputStream inputStream, ProfileKey profileKey) throws IOException {
        super(inputStream);
        try {
            byte[] bArr = new byte[12];
            Util.readFully(inputStream, bArr);
            Util.readFully(inputStream, this.buffer);
            this.aes = new Aes256GcmDecryption(profileKey.serialize(), bArr, new byte[0]);
        } catch (InvalidKeyException e) {
            throw new IOException(e);
        }
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() {
        throw new AssertionError("Not supported!");
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (this.aes == null) {
            return -1;
        }
        int read = ((FilterInputStream) this).in.read(bArr, i, i2);
        if (read == -1) {
            Aes256GcmDecryption aes256GcmDecryption = this.aes;
            this.aes = null;
            if (aes256GcmDecryption.verifyTag(this.buffer)) {
                return -1;
            }
            throw new IOException("authentication of decrypted data failed");
        }
        if (read < 16) {
            int i3 = 16 - read;
            System.arraycopy(this.buffer, read, this.swapBuffer, 0, i3);
            System.arraycopy(bArr, i, this.swapBuffer, i3, read);
            System.arraycopy(this.buffer, 0, bArr, i, read);
        } else if (read == 16) {
            System.arraycopy(bArr, i, this.swapBuffer, 0, read);
            System.arraycopy(this.buffer, 0, bArr, i, read);
        } else {
            System.arraycopy(bArr, (i + read) - 16, this.swapBuffer, 0, 16);
            System.arraycopy(bArr, i, bArr, i + 16, read - 16);
            System.arraycopy(this.buffer, 0, bArr, i, 16);
        }
        byte[] bArr2 = this.buffer;
        this.buffer = this.swapBuffer;
        this.swapBuffer = bArr2;
        this.aes.decrypt(bArr, i, read);
        return read;
    }
}
