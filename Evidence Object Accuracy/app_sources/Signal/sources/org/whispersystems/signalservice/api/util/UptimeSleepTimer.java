package org.whispersystems.signalservice.api.util;

/* loaded from: classes5.dex */
public class UptimeSleepTimer implements SleepTimer {
    @Override // org.whispersystems.signalservice.api.util.SleepTimer
    public void sleep(long j) throws InterruptedException {
        Thread.sleep(j);
    }
}
