package org.whispersystems.signalservice.api.storage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.util.ProtoUtil;
import org.whispersystems.signalservice.internal.storage.protos.GroupV1Record;

/* loaded from: classes5.dex */
public final class SignalGroupV1Record implements SignalRecord {
    private static final String TAG;
    private final byte[] groupId;
    private final boolean hasUnknownFields;
    private final StorageId id;
    private final GroupV1Record proto;

    public SignalGroupV1Record(StorageId storageId, GroupV1Record groupV1Record) {
        this.id = storageId;
        this.proto = groupV1Record;
        this.groupId = groupV1Record.getId().toByteArray();
        this.hasUnknownFields = ProtoUtil.hasUnknownFields(groupV1Record);
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public StorageId getId() {
        return this.id;
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public SignalStorageRecord asStorageRecord() {
        return SignalStorageRecord.forGroupV1(this);
    }

    @Override // org.whispersystems.signalservice.api.storage.SignalRecord
    public String describeDiff(SignalRecord signalRecord) {
        if (signalRecord instanceof SignalGroupV1Record) {
            SignalGroupV1Record signalGroupV1Record = (SignalGroupV1Record) signalRecord;
            LinkedList linkedList = new LinkedList();
            if (!Arrays.equals(this.id.getRaw(), signalGroupV1Record.id.getRaw())) {
                linkedList.add("ID");
            }
            if (!Arrays.equals(this.groupId, signalGroupV1Record.groupId)) {
                linkedList.add("MasterKey");
            }
            if (!Objects.equals(Boolean.valueOf(isBlocked()), Boolean.valueOf(signalGroupV1Record.isBlocked()))) {
                linkedList.add("Blocked");
            }
            if (!Objects.equals(Boolean.valueOf(isProfileSharingEnabled()), Boolean.valueOf(signalGroupV1Record.isProfileSharingEnabled()))) {
                linkedList.add("ProfileSharing");
            }
            if (!Objects.equals(Boolean.valueOf(isArchived()), Boolean.valueOf(signalGroupV1Record.isArchived()))) {
                linkedList.add("Archived");
            }
            if (!Objects.equals(Boolean.valueOf(isForcedUnread()), Boolean.valueOf(signalGroupV1Record.isForcedUnread()))) {
                linkedList.add("ForcedUnread");
            }
            if (!Objects.equals(Long.valueOf(getMuteUntil()), Long.valueOf(signalGroupV1Record.getMuteUntil()))) {
                linkedList.add("MuteUntil");
            }
            if (!Objects.equals(Boolean.valueOf(hasUnknownFields()), Boolean.valueOf(signalGroupV1Record.hasUnknownFields()))) {
                linkedList.add("UnknownFields");
            }
            return linkedList.toString();
        }
        return "Different class. " + SignalGroupV1Record.class.getSimpleName() + " | " + signalRecord.getClass().getSimpleName();
    }

    public boolean hasUnknownFields() {
        return this.hasUnknownFields;
    }

    public byte[] serializeUnknownFields() {
        if (this.hasUnknownFields) {
            return this.proto.toByteArray();
        }
        return null;
    }

    public byte[] getGroupId() {
        return this.groupId;
    }

    public boolean isBlocked() {
        return this.proto.getBlocked();
    }

    public boolean isProfileSharingEnabled() {
        return this.proto.getWhitelisted();
    }

    public boolean isArchived() {
        return this.proto.getArchived();
    }

    public boolean isForcedUnread() {
        return this.proto.getMarkedUnread();
    }

    public long getMuteUntil() {
        return this.proto.getMutedUntilTimestamp();
    }

    public GroupV1Record toProto() {
        return this.proto;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SignalGroupV1Record.class != obj.getClass()) {
            return false;
        }
        SignalGroupV1Record signalGroupV1Record = (SignalGroupV1Record) obj;
        if (!this.id.equals(signalGroupV1Record.id) || !this.proto.equals(signalGroupV1Record.proto)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.id, this.proto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        private final GroupV1Record.Builder builder;
        private final StorageId id;

        public Builder(byte[] bArr, byte[] bArr2, byte[] bArr3) {
            this.id = StorageId.forGroupV1(bArr);
            if (bArr3 != null) {
                this.builder = parseUnknowns(bArr3);
            } else {
                this.builder = GroupV1Record.newBuilder();
            }
            this.builder.setId(ByteString.copyFrom(bArr2));
        }

        public Builder setBlocked(boolean z) {
            this.builder.setBlocked(z);
            return this;
        }

        public Builder setProfileSharingEnabled(boolean z) {
            this.builder.setWhitelisted(z);
            return this;
        }

        public Builder setArchived(boolean z) {
            this.builder.setArchived(z);
            return this;
        }

        public Builder setForcedUnread(boolean z) {
            this.builder.setMarkedUnread(z);
            return this;
        }

        public Builder setMuteUntil(long j) {
            this.builder.setMutedUntilTimestamp(j);
            return this;
        }

        private static GroupV1Record.Builder parseUnknowns(byte[] bArr) {
            try {
                return GroupV1Record.parseFrom(bArr).toBuilder();
            } catch (InvalidProtocolBufferException e) {
                Log.w(SignalGroupV1Record.TAG, "Failed to combine unknown fields!", e);
                return GroupV1Record.newBuilder();
            }
        }

        public SignalGroupV1Record build() {
            return new SignalGroupV1Record(this.id, this.builder.build());
        }
    }
}
