package org.whispersystems.signalservice.api.push.exceptions;

import java.util.LinkedList;
import java.util.List;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;

/* loaded from: classes5.dex */
public class EncapsulatedExceptions extends Throwable {
    private final List<NetworkFailureException> networkExceptions;
    private final List<UnregisteredUserException> unregisteredUserExceptions;
    private final List<UntrustedIdentityException> untrustedIdentityExceptions;

    public EncapsulatedExceptions(List<UntrustedIdentityException> list, List<UnregisteredUserException> list2, List<NetworkFailureException> list3) {
        this.untrustedIdentityExceptions = list;
        this.unregisteredUserExceptions = list2;
        this.networkExceptions = list3;
    }

    public EncapsulatedExceptions(UntrustedIdentityException untrustedIdentityException) {
        LinkedList linkedList = new LinkedList();
        this.untrustedIdentityExceptions = linkedList;
        this.unregisteredUserExceptions = new LinkedList();
        this.networkExceptions = new LinkedList();
        linkedList.add(untrustedIdentityException);
    }

    public List<UntrustedIdentityException> getUntrustedIdentityExceptions() {
        return this.untrustedIdentityExceptions;
    }

    public List<UnregisteredUserException> getUnregisteredUserExceptions() {
        return this.unregisteredUserExceptions;
    }

    public List<NetworkFailureException> getNetworkExceptions() {
        return this.networkExceptions;
    }
}
