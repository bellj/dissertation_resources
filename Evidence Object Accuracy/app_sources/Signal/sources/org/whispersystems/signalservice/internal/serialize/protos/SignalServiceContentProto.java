package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.serialize.protos.AddressProto;
import org.whispersystems.signalservice.internal.serialize.protos.MetadataProto;

/* loaded from: classes5.dex */
public final class SignalServiceContentProto extends GeneratedMessageLite<SignalServiceContentProto, Builder> implements SignalServiceContentProtoOrBuilder {
    public static final int CONTENT_FIELD_NUMBER;
    private static final SignalServiceContentProto DEFAULT_INSTANCE;
    public static final int LEGACYDATAMESSAGE_FIELD_NUMBER;
    public static final int LOCALADDRESS_FIELD_NUMBER;
    public static final int METADATA_FIELD_NUMBER;
    private static volatile Parser<SignalServiceContentProto> PARSER;
    private int bitField0_;
    private int dataCase_ = 0;
    private Object data_;
    private AddressProto localAddress_;
    private MetadataProto metadata_;

    private SignalServiceContentProto() {
    }

    /* loaded from: classes5.dex */
    public enum DataCase {
        LEGACYDATAMESSAGE(3),
        CONTENT(4),
        DATA_NOT_SET(0);
        
        private final int value;

        DataCase(int i) {
            this.value = i;
        }

        @Deprecated
        public static DataCase valueOf(int i) {
            return forNumber(i);
        }

        public static DataCase forNumber(int i) {
            if (i == 0) {
                return DATA_NOT_SET;
            }
            if (i == 3) {
                return LEGACYDATAMESSAGE;
            }
            if (i != 4) {
                return null;
            }
            return CONTENT;
        }

        public int getNumber() {
            return this.value;
        }
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public DataCase getDataCase() {
        return DataCase.forNumber(this.dataCase_);
    }

    public void clearData() {
        this.dataCase_ = 0;
        this.data_ = null;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public boolean hasLocalAddress() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public AddressProto getLocalAddress() {
        AddressProto addressProto = this.localAddress_;
        return addressProto == null ? AddressProto.getDefaultInstance() : addressProto;
    }

    public void setLocalAddress(AddressProto addressProto) {
        addressProto.getClass();
        this.localAddress_ = addressProto;
        this.bitField0_ |= 1;
    }

    public void mergeLocalAddress(AddressProto addressProto) {
        addressProto.getClass();
        AddressProto addressProto2 = this.localAddress_;
        if (addressProto2 == null || addressProto2 == AddressProto.getDefaultInstance()) {
            this.localAddress_ = addressProto;
        } else {
            this.localAddress_ = AddressProto.newBuilder(this.localAddress_).mergeFrom((AddressProto.Builder) addressProto).buildPartial();
        }
        this.bitField0_ |= 1;
    }

    public void clearLocalAddress() {
        this.localAddress_ = null;
        this.bitField0_ &= -2;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public boolean hasMetadata() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public MetadataProto getMetadata() {
        MetadataProto metadataProto = this.metadata_;
        return metadataProto == null ? MetadataProto.getDefaultInstance() : metadataProto;
    }

    public void setMetadata(MetadataProto metadataProto) {
        metadataProto.getClass();
        this.metadata_ = metadataProto;
        this.bitField0_ |= 2;
    }

    public void mergeMetadata(MetadataProto metadataProto) {
        metadataProto.getClass();
        MetadataProto metadataProto2 = this.metadata_;
        if (metadataProto2 == null || metadataProto2 == MetadataProto.getDefaultInstance()) {
            this.metadata_ = metadataProto;
        } else {
            this.metadata_ = MetadataProto.newBuilder(this.metadata_).mergeFrom((MetadataProto.Builder) metadataProto).buildPartial();
        }
        this.bitField0_ |= 2;
    }

    public void clearMetadata() {
        this.metadata_ = null;
        this.bitField0_ &= -3;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public boolean hasLegacyDataMessage() {
        return this.dataCase_ == 3;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public SignalServiceProtos.DataMessage getLegacyDataMessage() {
        if (this.dataCase_ == 3) {
            return (SignalServiceProtos.DataMessage) this.data_;
        }
        return SignalServiceProtos.DataMessage.getDefaultInstance();
    }

    public void setLegacyDataMessage(SignalServiceProtos.DataMessage dataMessage) {
        dataMessage.getClass();
        this.data_ = dataMessage;
        this.dataCase_ = 3;
    }

    public void mergeLegacyDataMessage(SignalServiceProtos.DataMessage dataMessage) {
        dataMessage.getClass();
        if (this.dataCase_ != 3 || this.data_ == SignalServiceProtos.DataMessage.getDefaultInstance()) {
            this.data_ = dataMessage;
        } else {
            this.data_ = SignalServiceProtos.DataMessage.newBuilder((SignalServiceProtos.DataMessage) this.data_).mergeFrom((SignalServiceProtos.DataMessage.Builder) dataMessage).buildPartial();
        }
        this.dataCase_ = 3;
    }

    public void clearLegacyDataMessage() {
        if (this.dataCase_ == 3) {
            this.dataCase_ = 0;
            this.data_ = null;
        }
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public boolean hasContent() {
        return this.dataCase_ == 4;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
    public SignalServiceProtos.Content getContent() {
        if (this.dataCase_ == 4) {
            return (SignalServiceProtos.Content) this.data_;
        }
        return SignalServiceProtos.Content.getDefaultInstance();
    }

    public void setContent(SignalServiceProtos.Content content) {
        content.getClass();
        this.data_ = content;
        this.dataCase_ = 4;
    }

    public void mergeContent(SignalServiceProtos.Content content) {
        content.getClass();
        if (this.dataCase_ != 4 || this.data_ == SignalServiceProtos.Content.getDefaultInstance()) {
            this.data_ = content;
        } else {
            this.data_ = SignalServiceProtos.Content.newBuilder((SignalServiceProtos.Content) this.data_).mergeFrom((SignalServiceProtos.Content.Builder) content).buildPartial();
        }
        this.dataCase_ = 4;
    }

    public void clearContent() {
        if (this.dataCase_ == 4) {
            this.dataCase_ = 0;
            this.data_ = null;
        }
    }

    public static SignalServiceContentProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static SignalServiceContentProto parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static SignalServiceContentProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static SignalServiceContentProto parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static SignalServiceContentProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static SignalServiceContentProto parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static SignalServiceContentProto parseFrom(InputStream inputStream) throws IOException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static SignalServiceContentProto parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static SignalServiceContentProto parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static SignalServiceContentProto parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static SignalServiceContentProto parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static SignalServiceContentProto parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalServiceContentProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(SignalServiceContentProto signalServiceContentProto) {
        return DEFAULT_INSTANCE.createBuilder(signalServiceContentProto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<SignalServiceContentProto, Builder> implements SignalServiceContentProtoOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(SignalServiceContentProto.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public DataCase getDataCase() {
            return ((SignalServiceContentProto) this.instance).getDataCase();
        }

        public Builder clearData() {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).clearData();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public boolean hasLocalAddress() {
            return ((SignalServiceContentProto) this.instance).hasLocalAddress();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public AddressProto getLocalAddress() {
            return ((SignalServiceContentProto) this.instance).getLocalAddress();
        }

        public Builder setLocalAddress(AddressProto addressProto) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setLocalAddress(addressProto);
            return this;
        }

        public Builder setLocalAddress(AddressProto.Builder builder) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setLocalAddress(builder.build());
            return this;
        }

        public Builder mergeLocalAddress(AddressProto addressProto) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).mergeLocalAddress(addressProto);
            return this;
        }

        public Builder clearLocalAddress() {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).clearLocalAddress();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public boolean hasMetadata() {
            return ((SignalServiceContentProto) this.instance).hasMetadata();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public MetadataProto getMetadata() {
            return ((SignalServiceContentProto) this.instance).getMetadata();
        }

        public Builder setMetadata(MetadataProto metadataProto) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setMetadata(metadataProto);
            return this;
        }

        public Builder setMetadata(MetadataProto.Builder builder) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setMetadata(builder.build());
            return this;
        }

        public Builder mergeMetadata(MetadataProto metadataProto) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).mergeMetadata(metadataProto);
            return this;
        }

        public Builder clearMetadata() {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).clearMetadata();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public boolean hasLegacyDataMessage() {
            return ((SignalServiceContentProto) this.instance).hasLegacyDataMessage();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public SignalServiceProtos.DataMessage getLegacyDataMessage() {
            return ((SignalServiceContentProto) this.instance).getLegacyDataMessage();
        }

        public Builder setLegacyDataMessage(SignalServiceProtos.DataMessage dataMessage) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setLegacyDataMessage(dataMessage);
            return this;
        }

        public Builder setLegacyDataMessage(SignalServiceProtos.DataMessage.Builder builder) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setLegacyDataMessage(builder.build());
            return this;
        }

        public Builder mergeLegacyDataMessage(SignalServiceProtos.DataMessage dataMessage) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).mergeLegacyDataMessage(dataMessage);
            return this;
        }

        public Builder clearLegacyDataMessage() {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).clearLegacyDataMessage();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public boolean hasContent() {
            return ((SignalServiceContentProto) this.instance).hasContent();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProtoOrBuilder
        public SignalServiceProtos.Content getContent() {
            return ((SignalServiceContentProto) this.instance).getContent();
        }

        public Builder setContent(SignalServiceProtos.Content content) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setContent(content);
            return this;
        }

        public Builder setContent(SignalServiceProtos.Content.Builder builder) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).setContent(builder.build());
            return this;
        }

        public Builder mergeContent(SignalServiceProtos.Content content) {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).mergeContent(content);
            return this;
        }

        public Builder clearContent() {
            copyOnWrite();
            ((SignalServiceContentProto) this.instance).clearContent();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProto$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new SignalServiceContentProto();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0004\u0001\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ြ\u0000\u0004ြ\u0000", new Object[]{"data_", "dataCase_", "bitField0_", "localAddress_", "metadata_", SignalServiceProtos.DataMessage.class, SignalServiceProtos.Content.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<SignalServiceContentProto> parser = PARSER;
                if (parser == null) {
                    synchronized (SignalServiceContentProto.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        SignalServiceContentProto signalServiceContentProto = new SignalServiceContentProto();
        DEFAULT_INSTANCE = signalServiceContentProto;
        GeneratedMessageLite.registerDefaultInstance(SignalServiceContentProto.class, signalServiceContentProto);
    }

    public static SignalServiceContentProto getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<SignalServiceContentProto> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
