package org.whispersystems.signalservice.internal.websocket;

import j$.util.function.Function;
import java.util.Objects;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.signalservice.internal.websocket.DefaultErrorMapper;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;

/* loaded from: classes5.dex */
public class DefaultResponseMapper<Response> implements ResponseMapper<Response> {
    private final Class<Response> clazz;
    private final CustomResponseMapper<Response> customResponseMapper;
    private final ErrorMapper errorMapper;

    /* loaded from: classes5.dex */
    public interface CustomResponseMapper<T> {
        ServiceResponse<T> map(int i, String str, Function<String, String> function, boolean z) throws MalformedResponseException;
    }

    @Override // org.whispersystems.signalservice.internal.websocket.ResponseMapper
    public /* synthetic */ ServiceResponse map(WebsocketResponse websocketResponse) {
        return ResponseMapper.CC.$default$map(this, websocketResponse);
    }

    public static <T> DefaultResponseMapper<T> getDefault(Class<T> cls) {
        return new DefaultResponseMapper<>(cls);
    }

    public static <T> Builder<T> extend(Class<T> cls) {
        return new Builder<>(cls);
    }

    private DefaultResponseMapper(Class<Response> cls) {
        this(cls, null, DefaultErrorMapper.getDefault());
    }

    private DefaultResponseMapper(Class<Response> cls, CustomResponseMapper<Response> customResponseMapper, ErrorMapper errorMapper) {
        this.clazz = cls;
        this.customResponseMapper = customResponseMapper;
        this.errorMapper = errorMapper;
    }

    @Override // org.whispersystems.signalservice.internal.websocket.ResponseMapper
    public ServiceResponse<Response> map(int i, String str, Function<String, String> function, boolean z) {
        Throwable e;
        try {
            e = this.errorMapper.parseError(i, str, function);
        } catch (MalformedResponseException e2) {
            e = e2;
        }
        if (e == null) {
            try {
                CustomResponseMapper<Response> customResponseMapper = this.customResponseMapper;
                if (customResponseMapper == null) {
                    return ServiceResponse.forResult(JsonUtil.fromJsonResponse(str, this.clazz), i, str);
                }
                ServiceResponse<Response> map = customResponseMapper.map(i, str, function, z);
                Objects.requireNonNull(map);
                return map;
            } catch (MalformedResponseException e3) {
                e = e3;
            }
        }
        return ServiceResponse.forApplicationError(e, i, str);
    }

    /* loaded from: classes5.dex */
    public static class Builder<Value> {
        private final Class<Value> clazz;
        private CustomResponseMapper<Value> customResponseMapper;
        private DefaultErrorMapper.Builder errorMapperBuilder = DefaultErrorMapper.extend();

        public Builder(Class<Value> cls) {
            this.clazz = cls;
        }

        public Builder<Value> withResponseMapper(CustomResponseMapper<Value> customResponseMapper) {
            this.customResponseMapper = customResponseMapper;
            return this;
        }

        public Builder<Value> withCustomError(int i, ErrorMapper errorMapper) {
            this.errorMapperBuilder.withCustom(i, errorMapper);
            return this;
        }

        public ResponseMapper<Value> build() {
            return new DefaultResponseMapper(this.clazz, this.customResponseMapper, this.errorMapperBuilder.build());
        }
    }
}
