package org.whispersystems.signalservice.internal.contacts.crypto;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class SignatureBodyEntity {
    @JsonProperty
    private String[] advisoryIDs;
    @JsonProperty
    private String advisoryURL;
    @JsonProperty
    private byte[] isvEnclaveQuoteBody;
    @JsonProperty
    private String isvEnclaveQuoteStatus;
    @JsonProperty
    private String timestamp;
    @JsonProperty
    private Long version;

    public byte[] getIsvEnclaveQuoteBody() {
        return this.isvEnclaveQuoteBody;
    }

    public String getIsvEnclaveQuoteStatus() {
        return this.isvEnclaveQuoteStatus;
    }

    public String getAdvisoryUrl() {
        return this.advisoryURL;
    }

    public String[] getAdvisoryIds() {
        return this.advisoryIDs;
    }

    public Long getVersion() {
        return this.version;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
