package org.whispersystems.signalservice.internal.websocket;

import com.google.protobuf.InvalidProtocolBufferException;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.SingleSubject;
import j$.util.Optional;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.ConnectionSpec;
import okhttp3.Dns;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.TrustStore;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.Tls12SocketFactory;
import org.whispersystems.signalservice.api.util.TlsProxySocketFactory;
import org.whispersystems.signalservice.api.websocket.HealthMonitor;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;
import org.whispersystems.signalservice.internal.configuration.SignalProxy;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.util.BlacklistingTrustManager;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.signalservice.internal.websocket.WebSocketProtos;

/* loaded from: classes5.dex */
public class WebSocketConnection extends WebSocketListener {
    public static final int KEEPALIVE_TIMEOUT_SECONDS;
    private static final String TAG;
    private WebSocket client;
    private final Optional<CredentialsProvider> credentialsProvider;
    private final Optional<Dns> dns;
    private final HealthMonitor healthMonitor;
    private final LinkedList<WebSocketProtos.WebSocketRequestMessage> incomingRequests;
    private final List<Interceptor> interceptors;
    private final Set<Long> keepAlives;
    private final String name;
    private final Map<Long, OutgoingRequest> outgoingRequests;
    private final String signalAgent;
    private final Optional<SignalProxy> signalProxy;
    private final TrustStore trustStore;
    private final BehaviorSubject<WebSocketConnectionState> webSocketState;
    private final String wsUri;

    public WebSocketConnection(String str, SignalServiceConfiguration signalServiceConfiguration, Optional<CredentialsProvider> optional, String str2, HealthMonitor healthMonitor) {
        this(str, signalServiceConfiguration, optional, str2, healthMonitor, "");
    }

    public WebSocketConnection(String str, SignalServiceConfiguration signalServiceConfiguration, Optional<CredentialsProvider> optional, String str2, HealthMonitor healthMonitor, String str3) {
        this.incomingRequests = new LinkedList<>();
        this.outgoingRequests = new HashMap();
        this.keepAlives = new HashSet();
        this.name = "[" + str + ":" + System.identityHashCode(this) + "]";
        this.trustStore = signalServiceConfiguration.getSignalServiceUrls()[0].getTrustStore();
        this.credentialsProvider = optional;
        this.signalAgent = str2;
        this.interceptors = signalServiceConfiguration.getNetworkInterceptors();
        this.dns = signalServiceConfiguration.getDns();
        this.signalProxy = signalServiceConfiguration.getSignalProxy();
        this.healthMonitor = healthMonitor;
        this.webSocketState = BehaviorSubject.createDefault(WebSocketConnectionState.DISCONNECTED);
        String replace = signalServiceConfiguration.getSignalServiceUrls()[0].getUrl().replace("https://", "wss://").replace("http://", "ws://");
        if (optional.isPresent()) {
            this.wsUri = replace + "/v1/websocket/" + str3 + "?login=%s&password=%s";
            return;
        }
        this.wsUri = replace + "/v1/websocket/" + str3;
    }

    public String getName() {
        return this.name;
    }

    public synchronized Observable<WebSocketConnectionState> connect() {
        String str;
        log("connect()");
        if (this.client == null) {
            if (this.credentialsProvider.isPresent()) {
                ACI aci = this.credentialsProvider.get().getAci();
                Objects.requireNonNull(aci);
                String serviceId = aci.toString();
                if (this.credentialsProvider.get().getDeviceId() != 1) {
                    serviceId = serviceId + "." + this.credentialsProvider.get().getDeviceId();
                }
                str = String.format(this.wsUri, serviceId, this.credentialsProvider.get().getPassword());
            } else {
                str = this.wsUri;
            }
            Pair<SSLSocketFactory, X509TrustManager> createTlsSocketFactory = createTlsSocketFactory(this.trustStore);
            OkHttpClient.Builder connectionSpecs = new OkHttpClient.Builder().sslSocketFactory(new Tls12SocketFactory(createTlsSocketFactory.first()), createTlsSocketFactory.second()).connectionSpecs(Util.immutableList(ConnectionSpec.RESTRICTED_TLS));
            TimeUnit timeUnit = TimeUnit.SECONDS;
            OkHttpClient.Builder connectTimeout = connectionSpecs.readTimeout(65, timeUnit).dns(this.dns.orElse(Dns.SYSTEM)).connectTimeout(65, timeUnit);
            for (Interceptor interceptor : this.interceptors) {
                connectTimeout.addInterceptor(interceptor);
            }
            if (this.signalProxy.isPresent()) {
                connectTimeout.socketFactory(new TlsProxySocketFactory(this.signalProxy.get().getHost(), this.signalProxy.get().getPort(), this.dns));
            }
            OkHttpClient build = connectTimeout.build();
            Request.Builder url = new Request.Builder().url(str);
            String str2 = this.signalAgent;
            if (str2 != null) {
                url.addHeader("X-Signal-Agent", str2);
            }
            this.webSocketState.onNext(WebSocketConnectionState.CONNECTING);
            this.client = build.newWebSocket(url.build(), this);
        }
        return this.webSocketState;
    }

    public synchronized boolean isDead() {
        return this.client == null;
    }

    public synchronized void disconnect() {
        log("disconnect()");
        WebSocket webSocket = this.client;
        if (webSocket != null) {
            webSocket.close(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, "OK");
            this.client = null;
            this.webSocketState.onNext(WebSocketConnectionState.DISCONNECTING);
        }
        notifyAll();
    }

    public synchronized WebSocketProtos.WebSocketRequestMessage readRequest(long j) throws TimeoutException, IOException {
        if (this.client != null) {
            long currentTimeMillis = System.currentTimeMillis();
            while (this.client != null && this.incomingRequests.isEmpty() && elapsedTime(currentTimeMillis) < j) {
                Util.wait(this, Math.max(1L, j - elapsedTime(currentTimeMillis)));
            }
            if (this.incomingRequests.isEmpty() && this.client == null) {
                throw new IOException("Connection closed!");
            }
            if (!this.incomingRequests.isEmpty()) {
            } else {
                throw new TimeoutException("Timeout exceeded");
            }
        } else {
            throw new IOException("Connection closed!");
        }
        return this.incomingRequests.removeFirst();
    }

    public synchronized Single<WebsocketResponse> sendRequest(WebSocketProtos.WebSocketRequestMessage webSocketRequestMessage) throws IOException {
        SingleSubject create;
        if (this.client != null) {
            create = SingleSubject.create();
            this.outgoingRequests.put(Long.valueOf(webSocketRequestMessage.getId()), new OutgoingRequest(create));
            if (this.client.send(ByteString.of(WebSocketProtos.WebSocketMessage.newBuilder().setType(WebSocketProtos.WebSocketMessage.Type.REQUEST).setRequest(webSocketRequestMessage).build().toByteArray()))) {
            } else {
                throw new IOException("Write failed!");
            }
        } else {
            throw new IOException("No connection!");
        }
        return create.subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).timeout(10, TimeUnit.SECONDS, Schedulers.io());
    }

    public synchronized void sendResponse(WebSocketProtos.WebSocketResponseMessage webSocketResponseMessage) throws IOException {
        if (this.client == null) {
            throw new IOException("Connection closed!");
        } else if (!this.client.send(ByteString.of(WebSocketProtos.WebSocketMessage.newBuilder().setType(WebSocketProtos.WebSocketMessage.Type.RESPONSE).setResponse(webSocketResponseMessage).build().toByteArray()))) {
            throw new IOException("Write failed!");
        }
    }

    public synchronized void sendKeepAlive() throws IOException {
        if (this.client != null) {
            log("Sending keep alive...");
            long currentTimeMillis = System.currentTimeMillis();
            byte[] byteArray = WebSocketProtos.WebSocketMessage.newBuilder().setType(WebSocketProtos.WebSocketMessage.Type.REQUEST).setRequest(WebSocketProtos.WebSocketRequestMessage.newBuilder().setId(currentTimeMillis).setPath("/v1/keepalive").setVerb("GET").build()).build().toByteArray();
            this.keepAlives.add(Long.valueOf(currentTimeMillis));
            if (!this.client.send(ByteString.of(byteArray))) {
                throw new IOException("Write failed!");
            }
        }
    }

    @Override // okhttp3.WebSocketListener
    public synchronized void onOpen(WebSocket webSocket, Response response) {
        if (this.client != null) {
            log("onOpen() connected");
            this.webSocketState.onNext(WebSocketConnectionState.CONNECTED);
        }
    }

    @Override // okhttp3.WebSocketListener
    public synchronized void onMessage(WebSocket webSocket, ByteString byteString) {
        try {
            WebSocketProtos.WebSocketMessage parseFrom = WebSocketProtos.WebSocketMessage.parseFrom(byteString.toByteArray());
            boolean z = true;
            if (parseFrom.getType().getNumber() == 1) {
                this.incomingRequests.add(parseFrom.getRequest());
            } else if (parseFrom.getType().getNumber() == 2) {
                OutgoingRequest remove = this.outgoingRequests.remove(Long.valueOf(parseFrom.getResponse().getId()));
                if (remove != null) {
                    int status = parseFrom.getResponse().getStatus();
                    String str = new String(parseFrom.getResponse().getBody().toByteArray());
                    List<String> headersList = parseFrom.getResponse().getHeadersList();
                    if (this.credentialsProvider.isPresent()) {
                        z = false;
                    }
                    remove.onSuccess(new WebsocketResponse(status, str, headersList, z));
                    if (parseFrom.getResponse().getStatus() >= 400) {
                        this.healthMonitor.onMessageError(parseFrom.getResponse().getStatus(), this.credentialsProvider.isPresent());
                    }
                } else if (this.keepAlives.remove(Long.valueOf(parseFrom.getResponse().getId()))) {
                    this.healthMonitor.onKeepAliveResponse(parseFrom.getResponse().getId(), this.credentialsProvider.isPresent());
                }
            }
            notifyAll();
        } catch (InvalidProtocolBufferException e) {
            warn(e);
        }
    }

    @Override // okhttp3.WebSocketListener
    public synchronized void onClosed(WebSocket webSocket, int i, String str) {
        log("onClose()");
        this.webSocketState.onNext(WebSocketConnectionState.DISCONNECTED);
        cleanupAfterShutdown();
        notifyAll();
    }

    @Override // okhttp3.WebSocketListener
    public synchronized void onFailure(WebSocket webSocket, Throwable th, Response response) {
        warn("onFailure()", th);
        if (response == null || !(response.code() == 401 || response.code() == 403)) {
            this.webSocketState.onNext(WebSocketConnectionState.FAILED);
        } else {
            this.webSocketState.onNext(WebSocketConnectionState.AUTHENTICATION_FAILED);
        }
        cleanupAfterShutdown();
        notifyAll();
    }

    private void cleanupAfterShutdown() {
        Iterator<Map.Entry<Long, OutgoingRequest>> it = this.outgoingRequests.entrySet().iterator();
        while (it.hasNext()) {
            it.next().getValue().onError(new IOException("Closed unexpectedly"));
            it.remove();
        }
        if (this.client != null) {
            log("Client not null when closed");
            this.client.close(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, "OK");
            this.client = null;
        }
    }

    @Override // okhttp3.WebSocketListener
    public void onMessage(WebSocket webSocket, String str) {
        Log.d(TAG, "onMessage(text)");
    }

    @Override // okhttp3.WebSocketListener
    public synchronized void onClosing(WebSocket webSocket, int i, String str) {
        log("onClosing()");
        this.webSocketState.onNext(WebSocketConnectionState.DISCONNECTING);
        webSocket.close(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, "OK");
    }

    private long elapsedTime(long j) {
        return System.currentTimeMillis() - j;
    }

    private Pair<SSLSocketFactory, X509TrustManager> createTlsSocketFactory(TrustStore trustStore) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            TrustManager[] createFor = BlacklistingTrustManager.createFor(trustStore);
            instance.init(null, createFor, null);
            return new Pair<>(instance.getSocketFactory(), (X509TrustManager) createFor[0]);
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    private void log(String str) {
        String str2 = TAG;
        Log.i(str2, this.name + " " + str);
    }

    private void warn(String str) {
        String str2 = TAG;
        Log.w(str2, this.name + " " + str);
    }

    private void warn(Throwable th) {
        Log.w(TAG, this.name, th);
    }

    private void warn(String str, Throwable th) {
        String str2 = TAG;
        Log.w(str2, this.name + " " + str, th);
    }

    /* loaded from: classes5.dex */
    public static class OutgoingRequest {
        private final SingleSubject<WebsocketResponse> responseSingle;

        private OutgoingRequest(SingleSubject<WebsocketResponse> singleSubject) {
            this.responseSingle = singleSubject;
        }

        public void onSuccess(WebsocketResponse websocketResponse) {
            this.responseSingle.onSuccess(websocketResponse);
        }

        public void onError(Throwable th) {
            this.responseSingle.onError(th);
        }
    }
}
