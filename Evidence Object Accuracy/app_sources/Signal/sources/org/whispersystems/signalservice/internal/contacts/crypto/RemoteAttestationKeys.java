package org.whispersystems.signalservice.internal.contacts.crypto;

import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.signal.libsignal.protocol.kdf.HKDFv3;
import org.signal.libsignal.protocol.util.ByteUtil;

/* loaded from: classes5.dex */
public class RemoteAttestationKeys {
    private final byte[] clientKey;
    private final byte[] serverKey;

    public RemoteAttestationKeys(ECKeyPair eCKeyPair, byte[] bArr, byte[] bArr2) throws InvalidKeyException {
        byte[] bArr3 = new byte[32];
        this.clientKey = bArr3;
        byte[] bArr4 = new byte[32];
        this.serverKey = bArr4;
        byte[] combine = ByteUtil.combine(Curve.calculateAgreement(ECPublicKey.fromPublicKeyBytes(bArr), eCKeyPair.getPrivateKey()), Curve.calculateAgreement(ECPublicKey.fromPublicKeyBytes(bArr2), eCKeyPair.getPrivateKey()));
        byte[] combine2 = ByteUtil.combine(eCKeyPair.getPublicKey().getPublicKeyBytes(), bArr, bArr2);
        new HKDFv3();
        byte[] deriveSecrets = HKDF.deriveSecrets(combine, combine2, new byte[0], bArr3.length + bArr4.length);
        System.arraycopy(deriveSecrets, 0, bArr3, 0, bArr3.length);
        System.arraycopy(deriveSecrets, bArr3.length, bArr4, 0, bArr4.length);
    }

    public byte[] getClientKey() {
        return this.clientKey;
    }

    public byte[] getServerKey() {
        return this.serverKey;
    }
}
