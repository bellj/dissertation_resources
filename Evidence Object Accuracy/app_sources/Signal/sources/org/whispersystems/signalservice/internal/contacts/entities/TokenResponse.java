package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class TokenResponse {
    @JsonProperty
    private byte[] backupId;
    @JsonProperty
    private byte[] token;
    @JsonProperty
    private int tries;

    @JsonCreator
    public TokenResponse() {
    }

    public TokenResponse(byte[] bArr, byte[] bArr2, int i) {
        this.backupId = bArr;
        this.token = bArr2;
        this.tries = i;
    }

    public byte[] getBackupId() {
        return this.backupId;
    }

    public byte[] getToken() {
        return this.token;
    }

    public int getTries() {
        return this.tries;
    }
}
