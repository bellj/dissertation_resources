package org.whispersystems.signalservice.internal.websocket;

import j$.util.function.Function;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;

/* loaded from: classes5.dex */
public interface ErrorMapper {
    Throwable parseError(int i) throws MalformedResponseException;

    Throwable parseError(int i, String str, Function<String, String> function) throws MalformedResponseException;

    /* renamed from: org.whispersystems.signalservice.internal.websocket.ErrorMapper$-CC */
    /* loaded from: classes5.dex */
    public final /* synthetic */ class CC {
        public static /* synthetic */ String lambda$parseError$0(String str) {
            return "";
        }

        public static Throwable $default$parseError(ErrorMapper errorMapper, int i) throws MalformedResponseException {
            return errorMapper.parseError(i, "", new ErrorMapper$$ExternalSyntheticLambda0());
        }
    }
}
