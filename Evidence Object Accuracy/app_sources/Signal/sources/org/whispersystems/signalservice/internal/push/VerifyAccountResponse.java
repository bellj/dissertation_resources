package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class VerifyAccountResponse {
    @JsonProperty
    private String pni;
    @JsonProperty
    private boolean storageCapable;
    @JsonProperty
    private String uuid;

    @JsonCreator
    public VerifyAccountResponse() {
    }

    public VerifyAccountResponse(String str, String str2, boolean z) {
        this.uuid = str;
        this.pni = str2;
        this.storageCapable = z;
    }

    public String getUuid() {
        return this.uuid;
    }

    public boolean isStorageCapable() {
        return this.storageCapable;
    }

    public String getPni() {
        return this.pni;
    }
}
