package org.whispersystems.signalservice.internal.util;

import java.io.IOException;

/* loaded from: classes5.dex */
public class Hex {
    private static final int ASCII_TEXT_START;
    static final String EOL = System.getProperty("line.separator");
    private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final int HEX_DIGITS_START;

    public static String toString(byte[] bArr) {
        return toString(bArr, 0, bArr.length);
    }

    public static String toString(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i3 = 0; i3 < i2; i3++) {
            appendHexChar(stringBuffer, bArr[i + i3]);
            stringBuffer.append(' ');
        }
        return stringBuffer.toString();
    }

    public static String toStringCondensed(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            appendHexChar(stringBuffer, b);
        }
        return stringBuffer.toString();
    }

    public static byte[] fromStringCondensed(String str) throws IOException {
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        if ((length & 1) == 0) {
            byte[] bArr = new byte[length >> 1];
            int i = 0;
            int i2 = 0;
            while (i < length) {
                int i3 = i + 1;
                i = i3 + 1;
                bArr[i2] = (byte) (((Character.digit(charArray[i], 16) << 4) | Character.digit(charArray[i3], 16)) & 255);
                i2++;
            }
            return bArr;
        }
        throw new IOException("Odd number of characters.");
    }

    public static String dump(byte[] bArr) {
        return dump(bArr, 0, bArr.length);
    }

    public static String dump(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        int i3 = ((i2 - 1) / 16) + 1;
        for (int i4 = 0; i4 < i3; i4++) {
            int i5 = i4 * 16;
            appendDumpLine(stringBuffer, i4, bArr, i5 + i, Math.min(16, i2 - i5));
            stringBuffer.append(EOL);
        }
        return stringBuffer.toString();
    }

    private static void appendDumpLine(StringBuffer stringBuffer, int i, byte[] bArr, int i2, int i3) {
        char[] cArr = HEX_DIGITS;
        stringBuffer.append(cArr[(i >> 28) & 15]);
        stringBuffer.append(cArr[(i >> 24) & 15]);
        stringBuffer.append(cArr[(i >> 20) & 15]);
        stringBuffer.append(cArr[(i >> 16) & 15]);
        stringBuffer.append(cArr[(i >> 12) & 15]);
        stringBuffer.append(cArr[(i >> 8) & 15]);
        stringBuffer.append(cArr[(i >> 4) & 15]);
        stringBuffer.append(cArr[i & 15]);
        stringBuffer.append(": ");
        int i4 = 0;
        for (int i5 = 0; i5 < 16; i5++) {
            int i6 = i5 + i2;
            if (i5 < i3) {
                appendHexChar(stringBuffer, bArr[i6]);
            } else {
                stringBuffer.append("  ");
            }
            if (i5 % 2 == 1) {
                stringBuffer.append(' ');
            }
        }
        while (i4 < 16 && i4 < i3) {
            byte b = bArr[i4 + i2];
            if (b < 32 || b > 126) {
                stringBuffer.append('.');
            } else {
                stringBuffer.append((char) b);
            }
            i4++;
        }
    }

    private static void appendHexChar(StringBuffer stringBuffer, int i) {
        char[] cArr = HEX_DIGITS;
        stringBuffer.append(cArr[(i >> 4) & 15]);
        stringBuffer.append(cArr[i & 15]);
    }
}
