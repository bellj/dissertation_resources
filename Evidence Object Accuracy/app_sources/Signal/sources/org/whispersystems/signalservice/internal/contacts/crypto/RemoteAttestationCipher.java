package org.whispersystems.signalservice.internal.contacts.crypto;

import j$.time.Instant;
import j$.time.LocalDateTime;
import j$.time.Period;
import j$.time.ZoneId;
import j$.time.ZonedDateTime;
import j$.time.format.DateTimeFormatter;
import java.io.IOException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.SignatureException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.internal.contacts.entities.RemoteAttestationResponse;
import org.whispersystems.signalservice.internal.util.Hex;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes5.dex */
public final class RemoteAttestationCipher {
    private static final Set<Long> SIGNATURE_BODY_VERSIONS = new HashSet<Long>() { // from class: org.whispersystems.signalservice.internal.contacts.crypto.RemoteAttestationCipher.1
        {
            add(3L);
            add(4L);
        }
    };

    private RemoteAttestationCipher() {
    }

    public static byte[] getRequestId(RemoteAttestationKeys remoteAttestationKeys, RemoteAttestationResponse remoteAttestationResponse) throws InvalidCiphertextException {
        return AESCipher.decrypt(remoteAttestationKeys.getServerKey(), remoteAttestationResponse.getIv(), remoteAttestationResponse.getCiphertext(), remoteAttestationResponse.getTag());
    }

    public static void verifyServerQuote(Quote quote, byte[] bArr, String str) throws UnauthenticatedQuoteException {
        try {
            int length = bArr.length;
            byte[] bArr2 = new byte[length];
            System.arraycopy(quote.getReportData(), 0, bArr2, 0, length);
            if (!MessageDigest.isEqual(bArr2, bArr)) {
                throw new UnauthenticatedQuoteException("Response quote has unauthenticated report data!");
            } else if (!MessageDigest.isEqual(Hex.fromStringCondensed(str), quote.getMrenclave())) {
                throw new UnauthenticatedQuoteException("The response quote has the wrong mrenclave value in it: " + Hex.toStringCondensed(quote.getMrenclave()));
            } else if (quote.isDebugQuote()) {
                throw new UnauthenticatedQuoteException("Received quote for debuggable enclave");
            }
        } catch (IOException e) {
            throw new UnauthenticatedQuoteException(e);
        }
    }

    public static void verifyIasSignature(KeyStore keyStore, String str, String str2, String str3, Quote quote) throws SignatureException {
        if (str == null || str.isEmpty()) {
            throw new SignatureException("No certificates.");
        }
        try {
            new SigningCertificate(str, keyStore).verifySignature(str2, str3);
            SignatureBodyEntity signatureBodyEntity = (SignatureBodyEntity) JsonUtil.fromJson(str2, SignatureBodyEntity.class);
            if (!SIGNATURE_BODY_VERSIONS.contains(signatureBodyEntity.getVersion())) {
                throw new SignatureException("Unexpected signed quote version " + signatureBodyEntity.getVersion());
            } else if (!MessageDigest.isEqual(ByteUtil.trim(signatureBodyEntity.getIsvEnclaveQuoteBody(), 432), ByteUtil.trim(quote.getQuoteBytes(), 432))) {
                throw new SignatureException("Signed quote is not the same as RA quote: " + Hex.toStringCondensed(signatureBodyEntity.getIsvEnclaveQuoteBody()) + " vs " + Hex.toStringCondensed(quote.getQuoteBytes()));
            } else if (!hasValidStatus(signatureBodyEntity)) {
                throw new SignatureException("Quote status is: " + signatureBodyEntity.getIsvEnclaveQuoteStatus() + " and advisories are: " + Arrays.toString(signatureBodyEntity.getAdvisoryIds()));
            } else if (Instant.from(ZonedDateTime.of(LocalDateTime.from(DateTimeFormatter.ofPattern("yyy-MM-dd'T'HH:mm:ss.SSSSSS").parse(signatureBodyEntity.getTimestamp())), ZoneId.of("UTC"))).plus(Period.ofDays(1)).isBefore(Instant.now())) {
                throw new SignatureException("Signature is expired");
            }
        } catch (IOException | CertPathValidatorException | CertificateException e) {
            throw new SignatureException(e);
        }
    }

    private static boolean hasValidStatus(SignatureBodyEntity signatureBodyEntity) {
        if ("OK".equals(signatureBodyEntity.getIsvEnclaveQuoteStatus())) {
            return true;
        }
        if (!"SW_HARDENING_NEEDED".equals(signatureBodyEntity.getIsvEnclaveQuoteStatus()) || signatureBodyEntity.getAdvisoryIds().length != 1 || !"INTEL-SA-00334".equals(signatureBodyEntity.getAdvisoryIds()[0])) {
            return false;
        }
        return true;
    }
}
