package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.ConnectionPool;
import okhttp3.ConnectionSpec;
import okhttp3.Credentials;
import okhttp3.Dns;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.state.PreKeyBundle;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyCredentialRequestContext;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyCredentialResponse;
import org.signal.libsignal.zkgroup.profiles.ProfileKeyVersion;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialRequest;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialResponse;
import org.signal.storageservice.protos.groups.AvatarUploadAttributes;
import org.signal.storageservice.protos.groups.Group;
import org.signal.storageservice.protos.groups.GroupChange;
import org.signal.storageservice.protos.groups.GroupChanges;
import org.signal.storageservice.protos.groups.GroupExternalCredential;
import org.signal.storageservice.protos.groups.GroupJoinInfo;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.api.account.AccountAttributes;
import org.whispersystems.signalservice.api.account.ChangePhoneNumberRequest;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.groupsv2.CredentialResponse;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2AuthorizationString;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId;
import org.whispersystems.signalservice.api.messages.calls.CallingResponse;
import org.whispersystems.signalservice.api.messages.calls.TurnServerInfo;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceInfo;
import org.whispersystems.signalservice.api.messages.multidevice.VerifyDeviceResponse;
import org.whispersystems.signalservice.api.payments.CurrencyConversions;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfileWrite;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.ServiceIdType;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.SignedPreKeyEntity;
import org.whispersystems.signalservice.api.push.exceptions.AuthorizationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.CaptchaRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.ConflictException;
import org.whispersystems.signalservice.api.push.exceptions.ContactManifestMismatchException;
import org.whispersystems.signalservice.api.push.exceptions.DeprecatedVersionException;
import org.whispersystems.signalservice.api.push.exceptions.ExpectationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.ImpossiblePhoneNumberException;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.api.push.exceptions.MissingConfigurationException;
import org.whispersystems.signalservice.api.push.exceptions.NoContentException;
import org.whispersystems.signalservice.api.push.exceptions.NonNormalizedPhoneNumberException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResumableUploadResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.RateLimitException;
import org.whispersystems.signalservice.api.push.exceptions.RemoteAttestationResponseExpiredException;
import org.whispersystems.signalservice.api.push.exceptions.ResumeLocationInvalidException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;
import org.whispersystems.signalservice.api.push.exceptions.UsernameMalformedException;
import org.whispersystems.signalservice.api.push.exceptions.UsernameTakenException;
import org.whispersystems.signalservice.api.services.AttachmentService$$ExternalSyntheticLambda1;
import org.whispersystems.signalservice.api.storage.StorageAuthResponse;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionClientSecret;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.Tls12SocketFactory;
import org.whispersystems.signalservice.api.util.TlsProxySocketFactory;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.configuration.SignalCdnUrl;
import org.whispersystems.signalservice.internal.configuration.SignalProxy;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.configuration.SignalUrl;
import org.whispersystems.signalservice.internal.contacts.entities.DiscoveryRequest;
import org.whispersystems.signalservice.internal.contacts.entities.DiscoveryResponse;
import org.whispersystems.signalservice.internal.contacts.entities.KeyBackupRequest;
import org.whispersystems.signalservice.internal.contacts.entities.KeyBackupResponse;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.exceptions.ForbiddenException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupExistsException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupMismatchedDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupNotFoundException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupPatchNotAcceptedException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupStaleDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.InvalidUnidentifiedAccessHeaderException;
import org.whispersystems.signalservice.internal.push.exceptions.MismatchedDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.NotInGroupException;
import org.whispersystems.signalservice.internal.push.exceptions.PaymentsRegionException;
import org.whispersystems.signalservice.internal.push.exceptions.StaleDevicesException;
import org.whispersystems.signalservice.internal.push.http.AcceptLanguagesUtil;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.DigestingRequestBody;
import org.whispersystems.signalservice.internal.push.http.NoCipherOutputStreamFactory;
import org.whispersystems.signalservice.internal.push.http.OutputStreamFactory;
import org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec;
import org.whispersystems.signalservice.internal.storage.protos.ReadOperation;
import org.whispersystems.signalservice.internal.storage.protos.StorageItems;
import org.whispersystems.signalservice.internal.storage.protos.StorageManifest;
import org.whispersystems.signalservice.internal.storage.protos.WriteOperation;
import org.whispersystems.signalservice.internal.util.BlacklistingTrustManager;
import org.whispersystems.signalservice.internal.util.Hex;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers;
import org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture;
import org.whispersystems.signalservice.internal.util.concurrent.SettableFuture;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;
import org.whispersystems.util.Base64;
import org.whispersystems.util.Base64UrlSafe;

/* loaded from: classes5.dex */
public class PushServiceSocket {
    private static final String ATTACHMENT_ID_DOWNLOAD_PATH;
    private static final String ATTACHMENT_KEY_DOWNLOAD_PATH;
    private static final String ATTACHMENT_UPLOAD_PATH;
    private static final String ATTACHMENT_V2_PATH;
    private static final String ATTACHMENT_V3_PATH;
    private static final String AVATAR_UPLOAD_PATH;
    private static final String BOOST_AMOUNTS;
    private static final String BOOST_BADGES;
    private static final String BOOST_RECEIPT_CREDENTIALS;
    private static final long CDN2_RESUMABLE_LINK_LIFETIME_MILLIS = TimeUnit.DAYS.toMillis(7);
    private static final String CDSI_AUTH;
    private static final String CHANGE_NUMBER_PATH;
    private static final String CREATE_ACCOUNT_SMS_PATH;
    private static final String CREATE_ACCOUNT_VOICE_PATH;
    private static final String CREATE_BOOST_PAYMENT_INTENT;
    private static final String CREATE_SUBSCRIPTION_PAYMENT_METHOD;
    private static final String DEFAULT_SUBSCRIPTION_PAYMENT_METHOD;
    private static final String DELETE_ACCOUNT_PATH;
    private static final String DELETE_USERNAME_PATH;
    private static final String DEVICE_PATH;
    private static final String DIRECTORY_AUTH_PATH;
    private static final String DONATION_REDEEM_RECEIPT;
    private static final String GIFT_AMOUNT;
    private static final String GROUPSV2_AVATAR_REQUEST;
    private static final String GROUPSV2_CREDENTIAL;
    private static final String GROUPSV2_GROUP;
    private static final String GROUPSV2_GROUP_CHANGES;
    private static final String GROUPSV2_GROUP_JOIN;
    private static final String GROUPSV2_GROUP_PASSWORD;
    private static final String GROUPSV2_TOKEN;
    private static final ResponseCodeHandler GROUPS_V2_GET_CURRENT_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda6
        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public final void handle(int i, ResponseBody responseBody) {
            PushServiceSocket.lambda$static$8(i, responseBody);
        }

        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
            PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
        }
    };
    private static final ResponseCodeHandler GROUPS_V2_GET_JOIN_INFO_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket.4
        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public void handle(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException {
            if (i == 403) {
                throw new ForbiddenException();
            }
        }

        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public void handle(int i, ResponseBody responseBody, Function<String, String> function) throws NonSuccessfulResponseCodeException {
            if (i == 403) {
                throw new ForbiddenException(Optional.ofNullable(function.apply("X-Signal-Forbidden-Reason")));
            }
        }
    };
    private static final ResponseCodeHandler GROUPS_V2_GET_LOGS_HANDLER;
    private static final ResponseCodeHandler GROUPS_V2_PATCH_RESPONSE_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda7
        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public final void handle(int i, ResponseBody responseBody) {
            PushServiceSocket.lambda$static$9(i, responseBody);
        }

        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
            PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
        }
    };
    private static final ResponseCodeHandler GROUPS_V2_PUT_RESPONSE_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda5
        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public final void handle(int i, ResponseBody responseBody) {
            PushServiceSocket.lambda$static$7(i, responseBody);
        }

        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
            PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
        }
    };
    private static final String GROUP_MESSAGE_PATH;
    private static final String IDENTIFIER_REGISTERED_PATH;
    private static final String KBS_AUTH_PATH;
    private static final int MAX_FOLLOW_UPS;
    private static final String MESSAGE_PATH;
    private static final ResponseCodeHandler NO_HANDLER;
    private static final Map<String, String> NO_HEADERS = Collections.emptyMap();
    private static final String PAYMENTS_AUTH_PATH;
    private static final String PAYMENTS_CONVERSIONS;
    private static final String PIN_PATH;
    private static final String PREKEY_DEVICE_PATH;
    private static final String PREKEY_METADATA_PATH;
    private static final String PREKEY_PATH;
    private static final String PROFILE_BATCH_CHECK_PATH;
    private static final String PROFILE_PATH;
    private static final String PROFILE_USERNAME_PATH;
    private static final String PROVISIONING_CODE_PATH;
    private static final String PROVISIONING_MESSAGE_PATH;
    private static final String REGISTER_GCM_PATH;
    private static final String REGISTRATION_LOCK_PATH;
    private static final String REPORT_SPAM;
    private static final String REQUEST_PUSH_CHALLENGE;
    private static final String REQUEST_RATE_LIMIT_PUSH_CHALLENGE;
    private static final String SENDER_ACK_MESSAGE_PATH;
    private static final String SENDER_CERTIFICATE_NO_E164_PATH;
    private static final String SENDER_CERTIFICATE_PATH;
    private static final String SERVER_DELIVERED_TIMESTAMP_HEADER;
    private static final String SET_ACCOUNT_ATTRIBUTES;
    private static final String SET_USERNAME_PATH;
    private static final String SIGNED_PREKEY_PATH;
    private static final String STICKER_MANIFEST_PATH;
    private static final String STICKER_PATH;
    private static final String SUBMIT_RATE_LIMIT_CHALLENGE;
    private static final String SUBSCRIPTION;
    private static final String SUBSCRIPTION_LEVELS;
    private static final String SUBSCRIPTION_RECEIPT_CREDENTIALS;
    private static final String TAG;
    private static final String TURN_SERVER_INFO;
    private static final String UPDATE_SUBSCRIPTION_LEVEL;
    private static final String UUID_ACK_MESSAGE_PATH;
    private static final String VERIFY_ACCOUNT_CODE_PATH;
    private static final String WHO_AM_I;
    private final boolean automaticNetworkRetry;
    private final Map<Integer, ConnectionHolder[]> cdnClientsMap;
    private final ClientZkProfileOperations clientZkProfileOperations;
    private final Set<Call> connections = new HashSet();
    private final ConnectionHolder[] contactDiscoveryClients;
    private final CredentialsProvider credentialsProvider;
    private final ConnectionHolder[] keyBackupServiceClients;
    private final SecureRandom random;
    private final ServiceConnectionHolder[] serviceClients;
    private final String signalAgent;
    private long soTimeoutMillis = TimeUnit.SECONDS.toMillis(30);
    private final ConnectionHolder[] storageClients;

    /* loaded from: classes5.dex */
    public enum ClientSet {
        ContactDiscovery,
        KeyBackup
    }

    /* loaded from: classes.dex */
    public static class RegistrationLockFailure {
        @JsonProperty
        public AuthCredentials backupCredentials;
        @JsonProperty
        public int length;
        @JsonProperty
        public long timeRemaining;
    }

    static {
        NO_HEADERS = Collections.emptyMap();
        EmptyResponseCodeHandler emptyResponseCodeHandler = new EmptyResponseCodeHandler();
        NO_HANDLER = emptyResponseCodeHandler;
        CDN2_RESUMABLE_LINK_LIFETIME_MILLIS = TimeUnit.DAYS.toMillis(7);
        GROUPS_V2_PUT_RESPONSE_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda5
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PushServiceSocket.lambda$static$7(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        };
        GROUPS_V2_GET_LOGS_HANDLER = emptyResponseCodeHandler;
        GROUPS_V2_GET_CURRENT_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda6
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PushServiceSocket.lambda$static$8(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        };
        GROUPS_V2_PATCH_RESPONSE_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda7
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PushServiceSocket.lambda$static$9(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        };
        GROUPS_V2_GET_JOIN_INFO_HANDLER = new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket.4
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public void handle(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException {
                if (i == 403) {
                    throw new ForbiddenException();
                }
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public void handle(int i, ResponseBody responseBody, Function<String, String> function) throws NonSuccessfulResponseCodeException {
                if (i == 403) {
                    throw new ForbiddenException(Optional.ofNullable(function.apply("X-Signal-Forbidden-Reason")));
                }
            }
        };
    }

    public PushServiceSocket(SignalServiceConfiguration signalServiceConfiguration, CredentialsProvider credentialsProvider, String str, ClientZkProfileOperations clientZkProfileOperations, boolean z) {
        this.credentialsProvider = credentialsProvider;
        this.signalAgent = str;
        this.automaticNetworkRetry = z;
        this.serviceClients = createServiceConnectionHolders(signalServiceConfiguration.getSignalServiceUrls(), signalServiceConfiguration.getNetworkInterceptors(), signalServiceConfiguration.getDns(), signalServiceConfiguration.getSignalProxy());
        this.cdnClientsMap = createCdnClientsMap(signalServiceConfiguration.getSignalCdnUrlMap(), signalServiceConfiguration.getNetworkInterceptors(), signalServiceConfiguration.getDns(), signalServiceConfiguration.getSignalProxy());
        this.contactDiscoveryClients = createConnectionHolders(signalServiceConfiguration.getSignalContactDiscoveryUrls(), signalServiceConfiguration.getNetworkInterceptors(), signalServiceConfiguration.getDns(), signalServiceConfiguration.getSignalProxy());
        this.keyBackupServiceClients = createConnectionHolders(signalServiceConfiguration.getSignalKeyBackupServiceUrls(), signalServiceConfiguration.getNetworkInterceptors(), signalServiceConfiguration.getDns(), signalServiceConfiguration.getSignalProxy());
        this.storageClients = createConnectionHolders(signalServiceConfiguration.getSignalStorageUrls(), signalServiceConfiguration.getNetworkInterceptors(), signalServiceConfiguration.getDns(), signalServiceConfiguration.getSignalProxy());
        this.random = new SecureRandom();
        this.clientZkProfileOperations = clientZkProfileOperations;
    }

    public void requestSmsVerificationCode(boolean z, Optional<String> optional, Optional<String> optional2) throws IOException {
        Object[] objArr = new Object[2];
        objArr[0] = this.credentialsProvider.getE164();
        objArr[1] = z ? "android-2021-03" : "android";
        String format = String.format(CREATE_ACCOUNT_SMS_PATH, objArr);
        if (optional.isPresent()) {
            format = format + "&captcha=" + optional.get();
        } else if (optional2.isPresent()) {
            format = format + "&challenge=" + optional2.get();
        }
        makeServiceRequest(format, "GET", (String) null, NO_HEADERS, new VerificationCodeResponseHandler());
    }

    public void requestVoiceVerificationCode(Locale locale, Optional<String> optional, Optional<String> optional2) throws IOException {
        Map<String, String> map;
        if (locale != null) {
            map = Collections.singletonMap("Accept-Language", locale.getLanguage() + "-" + locale.getCountry());
        } else {
            map = NO_HEADERS;
        }
        String format = String.format(CREATE_ACCOUNT_VOICE_PATH, this.credentialsProvider.getE164());
        if (optional.isPresent()) {
            format = format + "?captcha=" + optional.get();
        } else if (optional2.isPresent()) {
            format = format + "?challenge=" + optional2.get();
        }
        makeServiceRequest(format, "GET", (String) null, map, new VerificationCodeResponseHandler());
    }

    public WhoAmIResponse getWhoAmI() throws IOException {
        return (WhoAmIResponse) JsonUtil.fromJson(makeServiceRequest(WHO_AM_I, "GET", null), WhoAmIResponse.class);
    }

    public boolean isIdentifierRegistered(ServiceId serviceId) throws IOException {
        try {
            makeServiceRequestWithoutAuthentication(String.format(IDENTIFIER_REGISTERED_PATH, serviceId.toString()), "HEAD", null);
            return true;
        } catch (NotFoundException unused) {
            return false;
        }
    }

    public CdsiAuthResponse getCdsiAuth() throws IOException {
        return (CdsiAuthResponse) JsonUtil.fromJsonResponse(makeServiceRequest(CDSI_AUTH, "GET", null), CdsiAuthResponse.class);
    }

    public VerifyAccountResponse verifyAccountCode(String str, String str2, int i, boolean z, String str3, String str4, byte[] bArr, boolean z2, AccountAttributes.Capabilities capabilities, boolean z3) throws IOException {
        return (VerifyAccountResponse) JsonUtil.fromJson(makeServiceRequest(String.format(VERIFY_ACCOUNT_CODE_PATH, str), "PUT", JsonUtil.toJson(new AccountAttributes(str2, i, z, str3, str4, bArr, z2, capabilities, z3, null))), VerifyAccountResponse.class);
    }

    public VerifyAccountResponse changeNumber(String str, String str2, String str3) throws IOException {
        return (VerifyAccountResponse) JsonUtil.fromJson(makeServiceRequest(CHANGE_NUMBER_PATH, "PUT", JsonUtil.toJson(new ChangePhoneNumberRequest(str2, str, str3))), VerifyAccountResponse.class);
    }

    public void setAccountAttributes(String str, int i, boolean z, String str2, String str3, byte[] bArr, boolean z2, AccountAttributes.Capabilities capabilities, boolean z3, byte[] bArr2) throws IOException {
        String str4;
        if (str3 == null || str2 == null) {
            if (bArr2 == null) {
                str4 = null;
            } else {
                str4 = Base64.encodeBytes(bArr2);
            }
            makeServiceRequest(SET_ACCOUNT_ATTRIBUTES, "PUT", JsonUtil.toJson(new AccountAttributes(str, i, z, str2, str3, bArr, z2, capabilities, z3, str4)));
            return;
        }
        throw new AssertionError("Pin should be null if registrationLock is set.");
    }

    public String getNewDeviceVerificationCode() throws IOException {
        return ((DeviceCode) JsonUtil.fromJson(makeServiceRequest(PROVISIONING_CODE_PATH, "GET", null), DeviceCode.class)).getVerificationCode();
    }

    public VerifyDeviceResponse verifySecondaryDevice(String str, AccountAttributes accountAttributes) throws IOException {
        return (VerifyDeviceResponse) JsonUtil.fromJson(makeServiceRequest(String.format(DEVICE_PATH, str), "PUT", JsonUtil.toJson(accountAttributes)), VerifyDeviceResponse.class);
    }

    public List<DeviceInfo> getDevices() throws IOException {
        return ((DeviceInfoList) JsonUtil.fromJson(makeServiceRequest(String.format(DEVICE_PATH, ""), "GET", null), DeviceInfoList.class)).getDevices();
    }

    public void removeDevice(long j) throws IOException {
        makeServiceRequest(String.format(DEVICE_PATH, String.valueOf(j)), "DELETE", null);
    }

    public void sendProvisioningMessage(String str, byte[] bArr) throws IOException {
        makeServiceRequest(String.format(PROVISIONING_MESSAGE_PATH, str), "PUT", JsonUtil.toJson(new ProvisioningMessage(Base64.encodeBytes(bArr))));
    }

    public void registerGcmId(String str) throws IOException {
        makeServiceRequest(REGISTER_GCM_PATH, "PUT", JsonUtil.toJson(new GcmRegistrationId(str, true)));
    }

    public void unregisterGcmId() throws IOException {
        makeServiceRequest(REGISTER_GCM_PATH, "DELETE", null);
    }

    public void requestPushChallenge(String str, String str2) throws IOException {
        makeServiceRequest(String.format(Locale.US, REQUEST_PUSH_CHALLENGE, str, str2), "GET", null);
    }

    public void removeRegistrationLockV1() throws IOException {
        makeServiceRequest(PIN_PATH, "DELETE", null);
    }

    public void setRegistrationLockV2(String str) throws IOException {
        makeServiceRequest(REGISTRATION_LOCK_PATH, "PUT", JsonUtil.toJson(new RegistrationLockV2(str)));
    }

    public void disableRegistrationLockV2() throws IOException {
        makeServiceRequest(REGISTRATION_LOCK_PATH, "DELETE", null);
    }

    public byte[] getSenderCertificate() throws IOException {
        return ((SenderCertificate) JsonUtil.fromJson(makeServiceRequest(SENDER_CERTIFICATE_PATH, "GET", null), SenderCertificate.class)).getCertificate();
    }

    public byte[] getUuidOnlySenderCertificate() throws IOException {
        return ((SenderCertificate) JsonUtil.fromJson(makeServiceRequest(SENDER_CERTIFICATE_NO_E164_PATH, "GET", null), SenderCertificate.class)).getCertificate();
    }

    public SendGroupMessageResponse sendGroupMessage(byte[] bArr, byte[] bArr2, long j, boolean z) throws IOException {
        ServiceConnectionHolder serviceConnectionHolder = (ServiceConnectionHolder) getRandom(this.serviceClients, this.random);
        String format = String.format(Locale.US, GROUP_MESSAGE_PATH, Long.valueOf(j), Boolean.valueOf(z));
        Request.Builder builder = new Request.Builder();
        builder.url(String.format("%s%s", serviceConnectionHolder.getUrl(), format));
        builder.put(RequestBody.create(MediaType.get("application/vnd.signal-messenger.mrm"), bArr));
        builder.addHeader("Unidentified-Access-Key", Base64.encodeBytes(bArr2));
        String str = this.signalAgent;
        if (str != null) {
            builder.addHeader("X-Signal-Agent", str);
        }
        if (serviceConnectionHolder.getHostHeader().isPresent()) {
            builder.addHeader("Host", serviceConnectionHolder.getHostHeader().get());
        }
        Call newCall = serviceConnectionHolder.getUnidentifiedClient().newCall(builder.build());
        synchronized (this.connections) {
            try {
                this.connections.add(newCall);
                try {
                } catch (Throwable th) {
                    synchronized (this.connections) {
                        try {
                            this.connections.remove(newCall);
                            throw th;
                        } catch (Throwable th2) {
                            throw th2;
                        }
                    }
                }
            } catch (Throwable th3) {
                throw th3;
            }
        }
        try {
            Response execute = newCall.execute();
            synchronized (this.connections) {
                try {
                    this.connections.remove(newCall);
                } catch (Throwable th4) {
                    throw th4;
                }
            }
            int code = execute.code();
            if (code == 200) {
                return (SendGroupMessageResponse) readBodyJson(execute.body(), SendGroupMessageResponse.class);
            }
            if (code == 401) {
                throw new InvalidUnidentifiedAccessHeaderException();
            } else if (code == 404) {
                throw new NotFoundException("At least one unregistered user in message send.");
            } else if (code == 508) {
                throw new ServerRejectedException();
            } else if (code == 409) {
                throw new GroupMismatchedDevicesException((GroupMismatchedDevices[]) readBodyJson(execute.body(), GroupMismatchedDevices[].class));
            } else if (code != 410) {
                throw new NonSuccessfulResponseCodeException(execute.code());
            } else {
                throw new GroupStaleDevicesException((GroupStaleDevices[]) readBodyJson(execute.body(), GroupStaleDevices[].class));
            }
        } catch (IOException e) {
            throw new PushNetworkException(e);
        }
    }

    public SendMessageResponse sendMessage(OutgoingPushMessageList outgoingPushMessageList, Optional<UnidentifiedAccess> optional) throws IOException {
        try {
            SendMessageResponse sendMessageResponse = (SendMessageResponse) JsonUtil.fromJson(makeServiceRequest(String.format(MESSAGE_PATH, outgoingPushMessageList.getDestination()), "PUT", JsonUtil.toJson(outgoingPushMessageList), NO_HEADERS, optional), SendMessageResponse.class);
            sendMessageResponse.setSentUnidentfied(optional.isPresent());
            return sendMessageResponse;
        } catch (NotFoundException e) {
            throw new UnregisteredUserException(outgoingPushMessageList.getDestination(), e);
        }
    }

    public SignalServiceMessagesResult getMessages() throws IOException {
        Response makeServiceRequest = makeServiceRequest(String.format(MESSAGE_PATH, ""), "GET", (RequestBody) null, NO_HEADERS, NO_HANDLER, Optional.empty());
        try {
            validateServiceResponse(makeServiceRequest);
            List<SignalServiceEnvelopeEntity> messages = ((SignalServiceEnvelopeEntityList) readBodyJson(makeServiceRequest.body(), SignalServiceEnvelopeEntityList.class)).getMessages();
            long j = 0;
            try {
                String header = makeServiceRequest.header(SERVER_DELIVERED_TIMESTAMP_HEADER);
                if (header == null) {
                    header = "0";
                }
                j = Long.parseLong(header);
            } catch (NumberFormatException e) {
                Log.w(TAG, e);
            }
            SignalServiceMessagesResult signalServiceMessagesResult = new SignalServiceMessagesResult(messages, j);
            makeServiceRequest.close();
            return signalServiceMessagesResult;
        } catch (Throwable th) {
            if (makeServiceRequest != null) {
                try {
                    makeServiceRequest.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public void acknowledgeMessage(String str, long j) throws IOException {
        makeServiceRequest(String.format(Locale.US, SENDER_ACK_MESSAGE_PATH, str, Long.valueOf(j)), "DELETE", null);
    }

    public void acknowledgeMessage(String str) throws IOException {
        makeServiceRequest(String.format(UUID_ACK_MESSAGE_PATH, str), "DELETE", null);
    }

    public void registerPreKeys(ServiceIdType serviceIdType, IdentityKey identityKey, SignedPreKeyRecord signedPreKeyRecord, List<PreKeyRecord> list) throws IOException {
        LinkedList linkedList = new LinkedList();
        try {
            for (PreKeyRecord preKeyRecord : list) {
                linkedList.add(new PreKeyEntity(preKeyRecord.getId(), preKeyRecord.getKeyPair().getPublicKey()));
            }
            makeServiceRequest(String.format(Locale.US, PREKEY_PATH, "", serviceIdType.queryParam()), "PUT", JsonUtil.toJson(new PreKeyState(linkedList, new SignedPreKeyEntity(signedPreKeyRecord.getId(), signedPreKeyRecord.getKeyPair().getPublicKey(), signedPreKeyRecord.getSignature()), identityKey)));
        } catch (InvalidKeyException e) {
            throw new AssertionError("unexpected invalid key", e);
        }
    }

    public int getAvailablePreKeys(ServiceIdType serviceIdType) throws IOException {
        return ((PreKeyStatus) JsonUtil.fromJson(makeServiceRequest(String.format(PREKEY_METADATA_PATH, serviceIdType.queryParam()), "GET", null), PreKeyStatus.class)).getCount();
    }

    public List<PreKeyBundle> getPreKeys(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccess> optional, int i) throws IOException {
        byte[] bArr;
        ECPublicKey eCPublicKey;
        int i2;
        ECPublicKey eCPublicKey2;
        int i3;
        try {
            String valueOf = String.valueOf(i);
            if (valueOf.equals(SubscriptionLevels.BOOST_LEVEL)) {
                valueOf = "*";
            }
            String format = String.format(PREKEY_DEVICE_PATH, signalServiceAddress.getIdentifier(), valueOf);
            Log.d(TAG, "Fetching prekeys for " + signalServiceAddress.getIdentifier() + "." + valueOf + ", i.e. GET " + format);
            PreKeyResponse preKeyResponse = (PreKeyResponse) JsonUtil.fromJson(makeServiceRequest(format, "GET", (String) null, NO_HEADERS, optional), PreKeyResponse.class);
            LinkedList linkedList = new LinkedList();
            for (PreKeyResponseItem preKeyResponseItem : preKeyResponse.getDevices()) {
                if (preKeyResponseItem.getSignedPreKey() != null) {
                    ECPublicKey publicKey = preKeyResponseItem.getSignedPreKey().getPublicKey();
                    int keyId = preKeyResponseItem.getSignedPreKey().getKeyId();
                    bArr = preKeyResponseItem.getSignedPreKey().getSignature();
                    eCPublicKey = publicKey;
                    i2 = keyId;
                } else {
                    eCPublicKey = null;
                    bArr = null;
                    i2 = -1;
                }
                if (preKeyResponseItem.getPreKey() != null) {
                    int keyId2 = preKeyResponseItem.getPreKey().getKeyId();
                    eCPublicKey2 = preKeyResponseItem.getPreKey().getPublicKey();
                    i3 = keyId2;
                } else {
                    eCPublicKey2 = null;
                    i3 = -1;
                }
                linkedList.add(new PreKeyBundle(preKeyResponseItem.getRegistrationId(), preKeyResponseItem.getDeviceId(), i3, eCPublicKey2, i2, eCPublicKey, bArr, preKeyResponse.getIdentityKey()));
            }
            return linkedList;
        } catch (NotFoundException e) {
            throw new UnregisteredUserException(signalServiceAddress.getIdentifier(), e);
        }
    }

    public PreKeyBundle getPreKey(SignalServiceAddress signalServiceAddress, int i) throws IOException {
        ECPublicKey eCPublicKey;
        int i2;
        byte[] bArr;
        ECPublicKey eCPublicKey2;
        int i3;
        try {
            PreKeyResponse preKeyResponse = (PreKeyResponse) JsonUtil.fromJson(makeServiceRequest(String.format(PREKEY_DEVICE_PATH, signalServiceAddress.getIdentifier(), String.valueOf(i)), "GET", null), PreKeyResponse.class);
            if (preKeyResponse.getDevices() == null || preKeyResponse.getDevices().size() < 1) {
                throw new IOException("Empty prekey list");
            }
            PreKeyResponseItem preKeyResponseItem = preKeyResponse.getDevices().get(0);
            if (preKeyResponseItem.getPreKey() != null) {
                int keyId = preKeyResponseItem.getPreKey().getKeyId();
                eCPublicKey = preKeyResponseItem.getPreKey().getPublicKey();
                i2 = keyId;
            } else {
                eCPublicKey = null;
                i2 = -1;
            }
            if (preKeyResponseItem.getSignedPreKey() != null) {
                int keyId2 = preKeyResponseItem.getSignedPreKey().getKeyId();
                ECPublicKey publicKey = preKeyResponseItem.getSignedPreKey().getPublicKey();
                bArr = preKeyResponseItem.getSignedPreKey().getSignature();
                eCPublicKey2 = publicKey;
                i3 = keyId2;
            } else {
                eCPublicKey2 = null;
                bArr = null;
                i3 = -1;
            }
            return new PreKeyBundle(preKeyResponseItem.getRegistrationId(), preKeyResponseItem.getDeviceId(), i2, eCPublicKey, i3, eCPublicKey2, bArr, preKeyResponse.getIdentityKey());
        } catch (NotFoundException e) {
            throw new UnregisteredUserException(signalServiceAddress.getIdentifier(), e);
        }
    }

    public SignedPreKeyEntity getCurrentSignedPreKey(ServiceIdType serviceIdType) throws IOException {
        try {
            return (SignedPreKeyEntity) JsonUtil.fromJson(makeServiceRequest(String.format(SIGNED_PREKEY_PATH, serviceIdType.queryParam()), "GET", null), SignedPreKeyEntity.class);
        } catch (NotFoundException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public void setCurrentSignedPreKey(ServiceIdType serviceIdType, SignedPreKeyRecord signedPreKeyRecord) throws IOException {
        makeServiceRequest(String.format(SIGNED_PREKEY_PATH, serviceIdType.queryParam()), "PUT", JsonUtil.toJson(new SignedPreKeyEntity(signedPreKeyRecord.getId(), signedPreKeyRecord.getKeyPair().getPublicKey(), signedPreKeyRecord.getSignature())));
    }

    public void retrieveAttachment(int i, SignalServiceAttachmentRemoteId signalServiceAttachmentRemoteId, File file, long j, SignalServiceAttachment.ProgressListener progressListener) throws IOException, MissingConfigurationException {
        String str;
        if (signalServiceAttachmentRemoteId.getV2().isPresent()) {
            str = String.format(Locale.US, ATTACHMENT_ID_DOWNLOAD_PATH, signalServiceAttachmentRemoteId.getV2().get());
        } else {
            str = String.format(Locale.US, ATTACHMENT_KEY_DOWNLOAD_PATH, signalServiceAttachmentRemoteId.getV3().get());
        }
        downloadFromCdn(file, i, str, j, progressListener);
    }

    public byte[] retrieveSticker(byte[] bArr, int i) throws NonSuccessfulResponseCodeException, PushNetworkException {
        String stringCondensed = Hex.toStringCondensed(bArr);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            downloadFromCdn(byteArrayOutputStream, 0, 0, String.format(Locale.US, STICKER_PATH, stringCondensed, Integer.valueOf(i)), 1048576, null);
            return byteArrayOutputStream.toByteArray();
        } catch (MissingConfigurationException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] retrieveStickerManifest(byte[] bArr) throws NonSuccessfulResponseCodeException, PushNetworkException {
        String stringCondensed = Hex.toStringCondensed(bArr);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            downloadFromCdn(byteArrayOutputStream, 0, 0, String.format(STICKER_MANIFEST_PATH, stringCondensed), 1048576, null);
            return byteArrayOutputStream.toByteArray();
        } catch (MissingConfigurationException e) {
            throw new AssertionError(e);
        }
    }

    public ListenableFuture<SignalServiceProfile> retrieveProfile(SignalServiceAddress signalServiceAddress, Optional<UnidentifiedAccess> optional, Locale locale) {
        return FutureTransformers.map(submitServiceRequest(String.format(PROFILE_PATH, signalServiceAddress.getIdentifier()), "GET", null, AcceptLanguagesUtil.getHeadersWithAcceptLanguage(locale), optional), new FutureTransformers.Transformer() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda4
            @Override // org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers.Transformer
            public final Object transform(Object obj) {
                return PushServiceSocket.lambda$retrieveProfile$0((String) obj);
            }
        });
    }

    public static /* synthetic */ SignalServiceProfile lambda$retrieveProfile$0(String str) throws Exception {
        try {
            return (SignalServiceProfile) JsonUtil.fromJson(str, SignalServiceProfile.class);
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public SignalServiceProfile retrieveProfileByUsername(String str, Optional<UnidentifiedAccess> optional, Locale locale) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        try {
            return (SignalServiceProfile) JsonUtil.fromJson(makeServiceRequest(String.format(PROFILE_USERNAME_PATH, str), "GET", (String) null, AcceptLanguagesUtil.getHeadersWithAcceptLanguage(locale), optional), SignalServiceProfile.class);
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public ListenableFuture<ProfileAndCredential> retrieveVersionedProfileAndCredential(UUID uuid, ProfileKey profileKey, Optional<UnidentifiedAccess> optional, Locale locale) {
        ProfileKeyVersion profileKeyVersion = profileKey.getProfileKeyVersion(uuid);
        ProfileKeyCredentialRequestContext createProfileKeyCredentialRequestContext = this.clientZkProfileOperations.createProfileKeyCredentialRequestContext(this.random, uuid, profileKey);
        return FutureTransformers.map(submitServiceRequest(String.format(PROFILE_PATH, String.format("%s/%s/%s?credentialType=expiringProfileKey", uuid, profileKeyVersion.serialize(), Hex.toStringCondensed(createProfileKeyCredentialRequestContext.getRequest().serialize()))), "GET", null, AcceptLanguagesUtil.getHeadersWithAcceptLanguage(locale), optional), new FutureTransformers.Transformer(createProfileKeyCredentialRequestContext) { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda2
            public final /* synthetic */ ProfileKeyCredentialRequestContext f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers.Transformer
            public final Object transform(Object obj) {
                return PushServiceSocket.this.lambda$retrieveVersionedProfileAndCredential$1(this.f$1, (String) obj);
            }
        });
    }

    /* renamed from: formatProfileAndCredentialBody */
    public ProfileAndCredential lambda$retrieveVersionedProfileAndCredential$1(ProfileKeyCredentialRequestContext profileKeyCredentialRequestContext, String str) throws MalformedResponseException {
        try {
            SignalServiceProfile signalServiceProfile = (SignalServiceProfile) JsonUtil.fromJson(str, SignalServiceProfile.class);
            try {
                return new ProfileAndCredential(signalServiceProfile, SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL, Optional.ofNullable(signalServiceProfile.getExpiringProfileKeyCredentialResponse() != null ? this.clientZkProfileOperations.receiveExpiringProfileKeyCredential(profileKeyCredentialRequestContext, signalServiceProfile.getExpiringProfileKeyCredentialResponse()) : null));
            } catch (VerificationFailedException e) {
                Log.w(TAG, "Failed to verify credential.", e);
                return new ProfileAndCredential(signalServiceProfile, SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL, Optional.empty());
            }
        } catch (IOException e2) {
            Log.w(TAG, e2);
            throw new MalformedResponseException("Unable to parse entity", e2);
        }
    }

    public ListenableFuture<SignalServiceProfile> retrieveVersionedProfile(UUID uuid, ProfileKey profileKey, Optional<UnidentifiedAccess> optional, Locale locale) {
        return FutureTransformers.map(submitServiceRequest(String.format(PROFILE_PATH, String.format("%s/%s", uuid, profileKey.getProfileKeyVersion(uuid).serialize())), "GET", null, AcceptLanguagesUtil.getHeadersWithAcceptLanguage(locale), optional), new FutureTransformers.Transformer() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda12
            @Override // org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers.Transformer
            public final Object transform(Object obj) {
                return PushServiceSocket.lambda$retrieveVersionedProfile$2((String) obj);
            }
        });
    }

    public static /* synthetic */ SignalServiceProfile lambda$retrieveVersionedProfile$2(String str) throws Exception {
        try {
            return (SignalServiceProfile) JsonUtil.fromJson(str, SignalServiceProfile.class);
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public void retrieveProfileAvatar(String str, File file, long j) throws IOException {
        try {
            downloadFromCdn(file, 0, str, j, null);
        } catch (MissingConfigurationException e) {
            throw new AssertionError(e);
        }
    }

    public Optional<String> writeProfile(SignalServiceProfileWrite signalServiceProfileWrite, ProfileAvatarData profileAvatarData) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        String makeServiceRequest = makeServiceRequest(String.format(PROFILE_PATH, ""), "PUT", JsonUtil.toJson(signalServiceProfileWrite), NO_HEADERS, new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda10
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PaymentsRegionException.responseCodeHandler(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        }, Optional.empty());
        if (!signalServiceProfileWrite.hasAvatar() || profileAvatarData == null) {
            return Optional.empty();
        }
        try {
            ProfileAvatarUploadAttributes profileAvatarUploadAttributes = (ProfileAvatarUploadAttributes) JsonUtil.fromJson(makeServiceRequest, ProfileAvatarUploadAttributes.class);
            uploadToCdn0("", profileAvatarUploadAttributes.getAcl(), profileAvatarUploadAttributes.getKey(), profileAvatarUploadAttributes.getPolicy(), profileAvatarUploadAttributes.getAlgorithm(), profileAvatarUploadAttributes.getCredential(), profileAvatarUploadAttributes.getDate(), profileAvatarUploadAttributes.getSignature(), profileAvatarData.getData(), profileAvatarData.getContentType(), profileAvatarData.getDataLength(), profileAvatarData.getOutputStreamFactory(), null, null);
            return Optional.of(profileAvatarUploadAttributes.getKey());
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public Single<ServiceResponse<IdentityCheckResponse>> performIdentityCheck(IdentityCheckRequest identityCheckRequest, Optional<UnidentifiedAccess> optional, ResponseMapper<IdentityCheckResponse> responseMapper) {
        return Single.fromCallable(new Callable(identityCheckRequest, optional, responseMapper) { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda11
            public final /* synthetic */ IdentityCheckRequest f$1;
            public final /* synthetic */ Optional f$2;
            public final /* synthetic */ ResponseMapper f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return PushServiceSocket.this.lambda$performIdentityCheck$3(this.f$1, this.f$2, this.f$3);
            }
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).onErrorReturn(new AttachmentService$$ExternalSyntheticLambda1());
    }

    public /* synthetic */ ServiceResponse lambda$performIdentityCheck$3(IdentityCheckRequest identityCheckRequest, Optional optional, ResponseMapper responseMapper) throws Exception {
        Response serviceConnection = getServiceConnection(PROFILE_BATCH_CHECK_PATH, "POST", jsonRequestBody(JsonUtil.toJson(identityCheckRequest)), Collections.emptyMap(), optional, false);
        return responseMapper.map(serviceConnection.code(), serviceConnection.body() != null ? serviceConnection.body().string() : "", new PushServiceSocket$$ExternalSyntheticLambda3(serviceConnection), optional.isPresent());
    }

    public void setUsername(String str) throws IOException {
        makeServiceRequest(String.format(SET_USERNAME_PATH, str), "PUT", "", NO_HEADERS, new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda8
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PushServiceSocket.lambda$setUsername$4(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        }, Optional.empty());
    }

    public static /* synthetic */ void lambda$setUsername$4(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
        if (i == 400) {
            throw new UsernameMalformedException();
        } else if (i == 409) {
            throw new UsernameTakenException();
        }
    }

    public void deleteUsername() throws IOException {
        makeServiceRequest(DELETE_USERNAME_PATH, "DELETE", null);
    }

    public void deleteAccount() throws IOException {
        makeServiceRequest(DELETE_ACCOUNT_PATH, "DELETE", null);
    }

    public void requestRateLimitPushChallenge() throws IOException {
        makeServiceRequest(REQUEST_RATE_LIMIT_PUSH_CHALLENGE, "POST", "");
    }

    public void submitRateLimitPushChallenge(String str) throws IOException {
        makeServiceRequest(SUBMIT_RATE_LIMIT_CHALLENGE, "PUT", JsonUtil.toJson(new SubmitPushChallengePayload(str)));
    }

    public void submitRateLimitRecaptchaChallenge(String str, String str2) throws IOException {
        makeServiceRequest(SUBMIT_RATE_LIMIT_CHALLENGE, "PUT", JsonUtil.toJson(new SubmitRecaptchaChallengePayload(str, str2)));
    }

    public void redeemDonationReceipt(ReceiptCredentialPresentation receiptCredentialPresentation, boolean z, boolean z2) throws IOException {
        makeServiceRequest(DONATION_REDEEM_RECEIPT, "POST", JsonUtil.toJson(new RedeemReceiptRequest(Base64.encodeBytes(receiptCredentialPresentation.serialize()), z, z2)));
    }

    public SubscriptionClientSecret createBoostPaymentMethod(String str, long j, long j2) throws IOException {
        return (SubscriptionClientSecret) JsonUtil.fromJsonResponse(makeServiceRequestWithoutAuthentication(CREATE_BOOST_PAYMENT_INTENT, "POST", JsonUtil.toJson(new DonationIntentPayload(j, str, j2))), SubscriptionClientSecret.class);
    }

    public Map<String, List<BigDecimal>> getBoostAmounts() throws IOException {
        return (Map) JsonUtil.fromJsonResponse(makeServiceRequestWithoutAuthentication(BOOST_AMOUNTS, "GET", null), new TypeReference<HashMap<String, List<BigDecimal>>>() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket.1
        });
    }

    public Map<String, BigDecimal> getGiftAmount() throws IOException {
        return (Map) JsonUtil.fromJsonResponse(makeServiceRequestWithoutAuthentication(GIFT_AMOUNT, "GET", null), new TypeReference<HashMap<String, BigDecimal>>() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket.2
        });
    }

    public SubscriptionLevels getBoostLevels(Locale locale) throws IOException {
        return (SubscriptionLevels) JsonUtil.fromJsonResponse(makeServiceRequestWithoutAuthentication(BOOST_BADGES, "GET", (String) null, AcceptLanguagesUtil.getHeadersWithAcceptLanguage(locale)), SubscriptionLevels.class);
    }

    public ReceiptCredentialResponse submitBoostReceiptCredentials(String str, ReceiptCredentialRequest receiptCredentialRequest) throws IOException {
        ReceiptCredentialResponseJson receiptCredentialResponseJson = (ReceiptCredentialResponseJson) JsonUtil.fromJson(makeServiceRequestWithoutAuthentication(BOOST_RECEIPT_CREDENTIALS, "POST", JsonUtil.toJson(new BoostReceiptCredentialRequestJson(str, receiptCredentialRequest)), new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda9
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PushServiceSocket.lambda$submitBoostReceiptCredentials$5(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        }), ReceiptCredentialResponseJson.class);
        if (receiptCredentialResponseJson.getReceiptCredentialResponse() != null) {
            return receiptCredentialResponseJson.getReceiptCredentialResponse();
        }
        throw new MalformedResponseException("Unable to parse response");
    }

    public static /* synthetic */ void lambda$submitBoostReceiptCredentials$5(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
        if (i == 204) {
            throw new NonSuccessfulResponseCodeException(204);
        }
    }

    public SubscriptionLevels getSubscriptionLevels(Locale locale) throws IOException {
        return (SubscriptionLevels) JsonUtil.fromJsonResponse(makeServiceRequestWithoutAuthentication(SUBSCRIPTION_LEVELS, "GET", (String) null, AcceptLanguagesUtil.getHeadersWithAcceptLanguage(locale)), SubscriptionLevels.class);
    }

    public void updateSubscriptionLevel(String str, String str2, String str3, String str4) throws IOException {
        makeServiceRequestWithoutAuthentication(String.format(UPDATE_SUBSCRIPTION_LEVEL, str, str2, str3, str4), "PUT", "");
    }

    public ActiveSubscription getSubscription(String str) throws IOException {
        return (ActiveSubscription) JsonUtil.fromJson(makeServiceRequestWithoutAuthentication(String.format(SUBSCRIPTION, str), "GET", null), ActiveSubscription.class);
    }

    public void putSubscription(String str) throws IOException {
        makeServiceRequestWithoutAuthentication(String.format(SUBSCRIPTION, str), "PUT", "");
    }

    public void deleteSubscription(String str) throws IOException {
        makeServiceRequestWithoutAuthentication(String.format(SUBSCRIPTION, str), "DELETE", null);
    }

    public SubscriptionClientSecret createSubscriptionPaymentMethod(String str) throws IOException {
        return (SubscriptionClientSecret) JsonUtil.fromJson(makeServiceRequestWithoutAuthentication(String.format(CREATE_SUBSCRIPTION_PAYMENT_METHOD, str), "POST", ""), SubscriptionClientSecret.class);
    }

    public void setDefaultSubscriptionPaymentMethod(String str, String str2) throws IOException {
        makeServiceRequestWithoutAuthentication(String.format(DEFAULT_SUBSCRIPTION_PAYMENT_METHOD, str, str2), "POST", "");
    }

    public ReceiptCredentialResponse submitReceiptCredentials(String str, ReceiptCredentialRequest receiptCredentialRequest) throws IOException {
        ReceiptCredentialResponseJson receiptCredentialResponseJson = (ReceiptCredentialResponseJson) JsonUtil.fromJson(makeServiceRequestWithoutAuthentication(String.format(SUBSCRIPTION_RECEIPT_CREDENTIALS, str), "POST", JsonUtil.toJson(new ReceiptCredentialRequestJson(receiptCredentialRequest)), new ResponseCodeHandler() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda0
            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public final void handle(int i, ResponseBody responseBody) {
                PushServiceSocket.lambda$submitReceiptCredentials$6(i, responseBody);
            }

            @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
            public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
                PushServiceSocket.ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
            }
        }), ReceiptCredentialResponseJson.class);
        if (receiptCredentialResponseJson.getReceiptCredentialResponse() != null) {
            return receiptCredentialResponseJson.getReceiptCredentialResponse();
        }
        throw new MalformedResponseException("Unable to parse response");
    }

    public static /* synthetic */ void lambda$submitReceiptCredentials$6(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
        if (i == 204) {
            throw new NonSuccessfulResponseCodeException(204);
        }
    }

    private AuthCredentials getAuthCredentials(String str) throws IOException {
        return (AuthCredentials) JsonUtil.fromJson(makeServiceRequest(str, "GET", null, NO_HEADERS), AuthCredentials.class);
    }

    private String getCredentials(String str) throws IOException {
        return getAuthCredentials(str).asBasic();
    }

    public String getContactDiscoveryAuthorization() throws IOException {
        return getCredentials(DIRECTORY_AUTH_PATH);
    }

    public String getKeyBackupServiceAuthorization() throws IOException {
        return getCredentials(KBS_AUTH_PATH);
    }

    public AuthCredentials getPaymentsAuthorization() throws IOException {
        return getAuthCredentials(PAYMENTS_AUTH_PATH);
    }

    public TokenResponse getKeyBackupServiceToken(String str, String str2) throws IOException {
        ClientSet clientSet = ClientSet.KeyBackup;
        ResponseBody body = makeRequest(clientSet, str, (List<String>) null, "/v1/token/" + str2, "GET", (String) null).body();
        if (body != null) {
            return (TokenResponse) JsonUtil.fromJson(body.string(), TokenResponse.class);
        }
        throw new MalformedResponseException("Empty response!");
    }

    public DiscoveryResponse getContactDiscoveryRegisteredUsers(String str, DiscoveryRequest discoveryRequest, List<String> list, String str2) throws IOException {
        ClientSet clientSet = ClientSet.ContactDiscovery;
        ResponseBody body = makeRequest(clientSet, str, list, "/v1/discovery/" + str2, "PUT", JsonUtil.toJson(discoveryRequest)).body();
        if (body != null) {
            return (DiscoveryResponse) JsonUtil.fromJson(body.string(), DiscoveryResponse.class);
        }
        throw new MalformedResponseException("Empty response!");
    }

    public KeyBackupResponse putKbsData(String str, KeyBackupRequest keyBackupRequest, List<String> list, String str2) throws IOException {
        ClientSet clientSet = ClientSet.KeyBackup;
        ResponseBody body = makeRequest(clientSet, str, list, "/v1/backup/" + str2, "PUT", JsonUtil.toJson(keyBackupRequest)).body();
        if (body != null) {
            return (KeyBackupResponse) JsonUtil.fromJson(body.string(), KeyBackupResponse.class);
        }
        throw new MalformedResponseException("Empty response!");
    }

    public TurnServerInfo getTurnServerInfo() throws IOException {
        return (TurnServerInfo) JsonUtil.fromJson(makeServiceRequest(TURN_SERVER_INFO, "GET", null), TurnServerInfo.class);
    }

    public String getStorageAuth() throws IOException {
        StorageAuthResponse storageAuthResponse = (StorageAuthResponse) JsonUtil.fromJson(makeServiceRequest("/v1/storage/auth", "GET", null), StorageAuthResponse.class);
        return Credentials.basic(storageAuthResponse.getUsername(), storageAuthResponse.getPassword());
    }

    public StorageManifest getStorageManifest(String str) throws IOException {
        ResponseBody makeStorageRequest = makeStorageRequest(str, "/v1/storage/manifest", "GET", null);
        if (makeStorageRequest != null) {
            return StorageManifest.parseFrom(readBodyBytes(makeStorageRequest));
        }
        throw new IOException("Missing body!");
    }

    public StorageManifest getStorageManifestIfDifferentVersion(String str, long j) throws IOException {
        ResponseBody makeStorageRequest = makeStorageRequest(str, "/v1/storage/manifest/version/" + j, "GET", null);
        if (makeStorageRequest != null) {
            return StorageManifest.parseFrom(readBodyBytes(makeStorageRequest));
        }
        throw new IOException("Missing body!");
    }

    public StorageItems readStorageItems(String str, ReadOperation readOperation) throws IOException {
        ResponseBody makeStorageRequest = makeStorageRequest(str, "/v1/storage/read", "PUT", protobufRequestBody(readOperation));
        if (makeStorageRequest != null) {
            return StorageItems.parseFrom(readBodyBytes(makeStorageRequest));
        }
        throw new IOException("Missing body!");
    }

    public Optional<StorageManifest> writeStorageContacts(String str, WriteOperation writeOperation) throws IOException {
        try {
            makeAndCloseStorageRequest(str, "/v1/storage", "PUT", protobufRequestBody(writeOperation));
            return Optional.empty();
        } catch (ContactManifestMismatchException e) {
            return Optional.of(StorageManifest.parseFrom(e.getResponseBody()));
        }
    }

    public void pingStorageService() throws IOException {
        makeAndCloseStorageRequest(null, "/ping", "GET", null);
    }

    public RemoteConfigResponse getRemoteConfig() throws IOException {
        return (RemoteConfigResponse) JsonUtil.fromJson(makeServiceRequest("/v1/config", "GET", null), RemoteConfigResponse.class);
    }

    public void setSoTimeoutMillis(long j) {
        this.soTimeoutMillis = j;
    }

    public void cancelInFlightRequests() {
        synchronized (this.connections) {
            String str = TAG;
            Log.w(str, "Canceling: " + this.connections.size());
            for (Call call : this.connections) {
                String str2 = TAG;
                Log.w(str2, "Canceling: " + call);
                call.cancel();
            }
        }
    }

    public AttachmentV2UploadAttributes getAttachmentV2UploadAttributes() throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        try {
            return (AttachmentV2UploadAttributes) JsonUtil.fromJson(makeServiceRequest(ATTACHMENT_V2_PATH, "GET", null), AttachmentV2UploadAttributes.class);
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public AttachmentV3UploadAttributes getAttachmentV3UploadAttributes() throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        try {
            return (AttachmentV3UploadAttributes) JsonUtil.fromJson(makeServiceRequest(ATTACHMENT_V3_PATH, "GET", null), AttachmentV3UploadAttributes.class);
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public byte[] uploadGroupV2Avatar(byte[] bArr, AvatarUploadAttributes avatarUploadAttributes) throws IOException {
        return uploadToCdn0("", avatarUploadAttributes.getAcl(), avatarUploadAttributes.getKey(), avatarUploadAttributes.getPolicy(), avatarUploadAttributes.getAlgorithm(), avatarUploadAttributes.getCredential(), avatarUploadAttributes.getDate(), avatarUploadAttributes.getSignature(), new ByteArrayInputStream(bArr), MediaUtil.OCTET, (long) bArr.length, new NoCipherOutputStreamFactory(), null, null);
    }

    public Pair<Long, byte[]> uploadAttachment(PushAttachmentData pushAttachmentData, AttachmentV2UploadAttributes attachmentV2UploadAttributes) throws PushNetworkException, NonSuccessfulResponseCodeException {
        long parseLong = Long.parseLong(attachmentV2UploadAttributes.getAttachmentId());
        return new Pair<>(Long.valueOf(parseLong), uploadToCdn0(ATTACHMENT_UPLOAD_PATH, attachmentV2UploadAttributes.getAcl(), attachmentV2UploadAttributes.getKey(), attachmentV2UploadAttributes.getPolicy(), attachmentV2UploadAttributes.getAlgorithm(), attachmentV2UploadAttributes.getCredential(), attachmentV2UploadAttributes.getDate(), attachmentV2UploadAttributes.getSignature(), pushAttachmentData.getData(), MediaUtil.OCTET, pushAttachmentData.getDataSize(), pushAttachmentData.getOutputStreamFactory(), pushAttachmentData.getListener(), pushAttachmentData.getCancelationSignal()));
    }

    public ResumableUploadSpec getResumableUploadSpec(AttachmentV3UploadAttributes attachmentV3UploadAttributes) throws IOException {
        return new ResumableUploadSpec(Util.getSecretBytes(64), Util.getSecretBytes(16), attachmentV3UploadAttributes.getKey(), attachmentV3UploadAttributes.getCdn(), getResumableUploadUrl(attachmentV3UploadAttributes.getSignedUploadLocation(), attachmentV3UploadAttributes.getHeaders()), System.currentTimeMillis() + CDN2_RESUMABLE_LINK_LIFETIME_MILLIS);
    }

    public byte[] uploadAttachment(PushAttachmentData pushAttachmentData) throws IOException {
        if (pushAttachmentData.getResumableUploadSpec() != null && pushAttachmentData.getResumableUploadSpec().getExpirationTimestamp().longValue() >= System.currentTimeMillis()) {
            return uploadToCdn2(pushAttachmentData.getResumableUploadSpec().getResumeLocation(), pushAttachmentData.getData(), MediaUtil.OCTET, pushAttachmentData.getDataSize(), pushAttachmentData.getOutputStreamFactory(), pushAttachmentData.getListener(), pushAttachmentData.getCancelationSignal());
        }
        throw new ResumeLocationInvalidException();
    }

    private void downloadFromCdn(File file, int i, String str, long j, SignalServiceAttachment.ProgressListener progressListener) throws IOException, MissingConfigurationException {
        FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        try {
            downloadFromCdn(fileOutputStream, file.length(), i, str, j, progressListener);
            fileOutputStream.close();
        } catch (Throwable th) {
            try {
                fileOutputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:85:0x00bb */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v7, types: [java.util.Set<okhttp3.Call>] */
    /* JADX WARN: Type inference failed for: r5v8, types: [okhttp3.ResponseBody] */
    /* JADX WARN: Type inference failed for: r5v9 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void downloadFromCdn(java.io.OutputStream r15, long r16, int r18, java.lang.String r19, long r20, org.whispersystems.signalservice.api.messages.SignalServiceAttachment.ProgressListener r22) throws org.whispersystems.signalservice.api.push.exceptions.PushNetworkException, org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException, org.whispersystems.signalservice.api.push.exceptions.MissingConfigurationException {
        /*
        // Method dump skipped, instructions count: 419
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.internal.push.PushServiceSocket.downloadFromCdn(java.io.OutputStream, long, int, java.lang.String, long, org.whispersystems.signalservice.api.messages.SignalServiceAttachment$ProgressListener):void");
    }

    private byte[] uploadToCdn0(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, InputStream inputStream, String str9, long j, OutputStreamFactory outputStreamFactory, SignalServiceAttachment.ProgressListener progressListener, CancelationSignal cancelationSignal) throws PushNetworkException, NonSuccessfulResponseCodeException {
        ConnectionHolder random = getRandom(this.cdnClientsMap.get(0), this.random);
        OkHttpClient.Builder newBuilder = random.getClient().newBuilder();
        long j2 = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = newBuilder.connectTimeout(j2, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).build();
        DigestingRequestBody digestingRequestBody = new DigestingRequestBody(inputStream, outputStreamFactory, str9, j, progressListener, cancelationSignal, 0);
        MultipartBody build2 = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("acl", str2).addFormDataPart("key", str3).addFormDataPart("policy", str4).addFormDataPart("Content-Type", str9).addFormDataPart("x-amz-algorithm", str5).addFormDataPart("x-amz-credential", str6).addFormDataPart("x-amz-date", str7).addFormDataPart("x-amz-signature", str8).addFormDataPart("file", "file", digestingRequestBody).build();
        Request.Builder builder = new Request.Builder();
        Request.Builder post = builder.url(random.getUrl() + "/" + str).post(build2);
        if (random.getHostHeader().isPresent()) {
            post.addHeader("Host", random.getHostHeader().get());
        }
        Call newCall = build.newCall(post.build());
        synchronized (this.connections) {
            try {
                this.connections.add(newCall);
            } catch (Throwable th) {
                throw th;
            }
        }
        try {
            try {
                Response execute = newCall.execute();
                if (execute.isSuccessful()) {
                    byte[] transmittedDigest = digestingRequestBody.getTransmittedDigest();
                    synchronized (this.connections) {
                        try {
                            this.connections.remove(newCall);
                        } catch (Throwable th2) {
                            throw th2;
                        }
                    }
                    return transmittedDigest;
                }
                int code = execute.code();
                throw new NonSuccessfulResponseCodeException(code, "Response: " + execute);
            } catch (IOException e) {
                throw new PushNetworkException(e);
            }
        } catch (Throwable th3) {
            synchronized (this.connections) {
                try {
                    this.connections.remove(newCall);
                    throw th3;
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        }
    }

    private String getResumableUploadUrl(String str, Map<String, String> map) throws IOException {
        ConnectionHolder random = getRandom(this.cdnClientsMap.get(2), this.random);
        OkHttpClient.Builder newBuilder = random.getClient().newBuilder();
        long j = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = newBuilder.connectTimeout(j, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).eventListener(new LoggingOkhttpEventListener()).build();
        Request.Builder post = new Request.Builder().url(buildConfiguredUrl(random, str)).post(RequestBody.create((MediaType) null, ""));
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (!entry.getKey().equalsIgnoreCase("host")) {
                post.header(entry.getKey(), entry.getValue());
            }
        }
        if (random.getHostHeader().isPresent()) {
            post.header("host", random.getHostHeader().get());
        }
        post.addHeader("Content-Length", "0");
        post.addHeader("Content-Type", MediaUtil.OCTET);
        Call newCall = build.newCall(post.build());
        synchronized (this.connections) {
            try {
                this.connections.add(newCall);
                try {
                } catch (Throwable th) {
                    synchronized (this.connections) {
                        try {
                            this.connections.remove(newCall);
                            throw th;
                        } catch (Throwable th2) {
                            throw th2;
                        }
                    }
                }
            } catch (Throwable th3) {
                throw th3;
            }
        }
        try {
            Response execute = newCall.execute();
            if (execute.isSuccessful()) {
                String header = execute.header(DraftDatabase.Draft.LOCATION);
                synchronized (this.connections) {
                    try {
                        this.connections.remove(newCall);
                    } catch (Throwable th4) {
                        throw th4;
                    }
                }
                return header;
            }
            int code = execute.code();
            throw new NonSuccessfulResponseCodeException(code, "Response: " + execute);
        } catch (IOException e) {
            throw new PushNetworkException(e);
        }
    }

    private byte[] uploadToCdn2(String str, InputStream inputStream, String str2, long j, OutputStreamFactory outputStreamFactory, SignalServiceAttachment.ProgressListener progressListener, CancelationSignal cancelationSignal) throws IOException {
        ConnectionHolder random = getRandom(this.cdnClientsMap.get(2), this.random);
        OkHttpClient.Builder newBuilder = random.getClient().newBuilder();
        long j2 = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = newBuilder.connectTimeout(j2, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).build();
        ResumeInfo resumeInfo = getResumeInfo(str, j);
        DigestingRequestBody digestingRequestBody = new DigestingRequestBody(inputStream, outputStreamFactory, str2, j, progressListener, cancelationSignal, resumeInfo.contentStart);
        if (resumeInfo.contentStart == j) {
            Log.w(TAG, "Resume start point == content length");
            NowhereBufferedSink nowhereBufferedSink = new NowhereBufferedSink();
            try {
                digestingRequestBody.writeTo(nowhereBufferedSink);
                nowhereBufferedSink.close();
                return digestingRequestBody.getTransmittedDigest();
            } catch (Throwable th) {
                try {
                    nowhereBufferedSink.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        } else {
            Request.Builder addHeader = new Request.Builder().url(buildConfiguredUrl(random, str)).put(digestingRequestBody).addHeader("Content-Range", resumeInfo.contentRange);
            if (random.getHostHeader().isPresent()) {
                addHeader.header("host", random.getHostHeader().get());
            }
            Call newCall = build.newCall(addHeader.build());
            synchronized (this.connections) {
                try {
                    this.connections.add(newCall);
                    try {
                    } catch (Throwable th3) {
                        synchronized (this.connections) {
                            try {
                                this.connections.remove(newCall);
                                throw th3;
                            } catch (Throwable th4) {
                                throw th4;
                            }
                        }
                    }
                } catch (Throwable th5) {
                    throw th5;
                }
            }
            try {
                Response execute = newCall.execute();
                try {
                    if (execute.isSuccessful()) {
                        byte[] transmittedDigest = digestingRequestBody.getTransmittedDigest();
                        execute.close();
                        synchronized (this.connections) {
                            try {
                                this.connections.remove(newCall);
                            } catch (Throwable th6) {
                                throw th6;
                            }
                        }
                        return transmittedDigest;
                    }
                    int code = execute.code();
                    throw new NonSuccessfulResponseCodeException(code, "Response: " + execute);
                } catch (Throwable th7) {
                    if (execute != null) {
                        try {
                            execute.close();
                        } catch (Throwable th8) {
                            th7.addSuppressed(th8);
                        }
                    }
                    throw th7;
                }
            } catch (IOException e) {
                throw new PushNetworkException(e);
            }
        }
    }

    private ResumeInfo getResumeInfo(String str, long j) throws IOException {
        long j2;
        String str2;
        long j3;
        ConnectionHolder random = getRandom(this.cdnClientsMap.get(2), this.random);
        OkHttpClient.Builder newBuilder = random.getClient().newBuilder();
        long j4 = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = newBuilder.connectTimeout(j4, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).build();
        Request.Builder put = new Request.Builder().url(buildConfiguredUrl(random, str)).put(RequestBody.create((MediaType) null, ""));
        Locale locale = Locale.US;
        Request.Builder addHeader = put.addHeader("Content-Range", String.format(locale, "bytes */%d", Long.valueOf(j)));
        if (random.getHostHeader().isPresent()) {
            addHeader.header("host", random.getHostHeader().get());
        }
        Call newCall = build.newCall(addHeader.build());
        synchronized (this.connections) {
            try {
                this.connections.add(newCall);
            } catch (Throwable th) {
                throw th;
            }
        }
        try {
            try {
                Response execute = newCall.execute();
                if (execute.isSuccessful()) {
                    str2 = null;
                    j2 = j;
                } else if (execute.code() == 308) {
                    String header = execute.header("Range");
                    if (header == null) {
                        j3 = 0;
                    } else {
                        j3 = Long.parseLong(header.split("-")[1]) + 1;
                    }
                    str2 = String.format(locale, "bytes %d-%d/%d", Long.valueOf(j3), Long.valueOf(j - 1), Long.valueOf(j));
                    j2 = j3;
                } else if (execute.code() == 404) {
                    throw new ResumeLocationInvalidException();
                } else {
                    throw new NonSuccessfulResumableUploadResponseCodeException(execute.code(), "Response: " + execute);
                }
                synchronized (this.connections) {
                    try {
                        this.connections.remove(newCall);
                    } catch (Throwable th2) {
                        throw th2;
                    }
                }
                return new ResumeInfo(str2, j2);
            } catch (IOException e) {
                throw new PushNetworkException(e);
            }
        } catch (Throwable th3) {
            synchronized (this.connections) {
                try {
                    this.connections.remove(newCall);
                    throw th3;
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        }
    }

    private static HttpUrl buildConfiguredUrl(ConnectionHolder connectionHolder, String str) throws IOException {
        HttpUrl httpUrl = HttpUrl.get(connectionHolder.url);
        try {
            HttpUrl httpUrl2 = HttpUrl.get(str);
            return new HttpUrl.Builder().scheme(httpUrl.scheme()).host(httpUrl.host()).port(httpUrl.port()).encodedPath(httpUrl.encodedPath()).addEncodedPathSegments(httpUrl2.encodedPath().substring(1)).encodedQuery(httpUrl2.encodedQuery()).encodedFragment(httpUrl2.encodedFragment()).build();
        } catch (IllegalArgumentException e) {
            throw new IOException("Malformed URL!", e);
        }
    }

    private String makeServiceRequestWithoutAuthentication(String str, String str2, String str3) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequestWithoutAuthentication(str, str2, str3, NO_HANDLER);
    }

    private String makeServiceRequestWithoutAuthentication(String str, String str2, String str3, ResponseCodeHandler responseCodeHandler) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequestWithoutAuthentication(str, str2, str3, NO_HEADERS, responseCodeHandler);
    }

    private String makeServiceRequestWithoutAuthentication(String str, String str2, String str3, Map<String, String> map) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequestWithoutAuthentication(str, str2, str3, map, NO_HANDLER);
    }

    private String makeServiceRequestWithoutAuthentication(String str, String str2, String str3, Map<String, String> map, ResponseCodeHandler responseCodeHandler) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        try {
            return makeServiceRequest(str, str2, jsonRequestBody(str3), map, responseCodeHandler, Optional.empty(), true).body().string();
        } catch (IOException e) {
            throw new PushNetworkException(e);
        }
    }

    private String makeServiceRequest(String str, String str2, String str3) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequest(str, str2, str3, NO_HEADERS, NO_HANDLER, Optional.empty());
    }

    private String makeServiceRequest(String str, String str2, String str3, Map<String, String> map) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequest(str, str2, str3, map, NO_HANDLER, Optional.empty());
    }

    private String makeServiceRequest(String str, String str2, String str3, Map<String, String> map, ResponseCodeHandler responseCodeHandler) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequest(str, str2, str3, map, responseCodeHandler, Optional.empty());
    }

    private String makeServiceRequest(String str, String str2, String str3, Map<String, String> map, Optional<UnidentifiedAccess> optional) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequest(str, str2, str3, map, NO_HANDLER, optional);
    }

    private String makeServiceRequest(String str, String str2, String str3, Map<String, String> map, ResponseCodeHandler responseCodeHandler, Optional<UnidentifiedAccess> optional) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        try {
            return makeServiceBodyRequest(str, str2, jsonRequestBody(str3), map, responseCodeHandler, optional).string();
        } catch (IOException e) {
            throw new PushNetworkException(e);
        }
    }

    private static RequestBody jsonRequestBody(String str) {
        if (str != null) {
            return RequestBody.create(MediaType.parse("application/json"), str);
        }
        return null;
    }

    private static RequestBody protobufRequestBody(MessageLite messageLite) {
        if (messageLite != null) {
            return RequestBody.create(MediaType.parse("application/x-protobuf"), messageLite.toByteArray());
        }
        return null;
    }

    private ListenableFuture<String> submitServiceRequest(String str, String str2, String str3, Map<String, String> map, Optional<UnidentifiedAccess> optional) {
        Call newCall = buildOkHttpClient(optional.isPresent()).newCall(buildServiceRequest(str, str2, jsonRequestBody(str3), map, optional, false));
        synchronized (this.connections) {
            this.connections.add(newCall);
        }
        final SettableFuture settableFuture = new SettableFuture();
        newCall.enqueue(new Callback() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket.3
            @Override // okhttp3.Callback
            public void onResponse(Call call, Response response) {
                try {
                    ResponseBody body = response.body();
                    PushServiceSocket.this.validateServiceResponse(response);
                    settableFuture.set(PushServiceSocket.readBodyString(body));
                    if (body != null) {
                        body.close();
                    }
                } catch (IOException e) {
                    settableFuture.setException(e);
                }
            }

            @Override // okhttp3.Callback
            public void onFailure(Call call, IOException iOException) {
                settableFuture.setException(iOException);
            }
        });
        return settableFuture;
    }

    private ResponseBody makeServiceBodyRequest(String str, String str2, RequestBody requestBody, Map<String, String> map, ResponseCodeHandler responseCodeHandler, Optional<UnidentifiedAccess> optional) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequest(str, str2, requestBody, map, responseCodeHandler, optional).body();
    }

    private Response makeServiceRequest(String str, String str2, RequestBody requestBody, Map<String, String> map, ResponseCodeHandler responseCodeHandler, Optional<UnidentifiedAccess> optional) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        return makeServiceRequest(str, str2, requestBody, map, responseCodeHandler, optional, false);
    }

    private Response makeServiceRequest(String str, String str2, RequestBody requestBody, Map<String, String> map, ResponseCodeHandler responseCodeHandler, Optional<UnidentifiedAccess> optional, boolean z) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        Response serviceConnection = getServiceConnection(str, str2, requestBody, map, optional, z);
        ResponseBody body = serviceConnection.body();
        try {
            responseCodeHandler.handle(serviceConnection.code(), body);
            return validateServiceResponse(serviceConnection);
        } catch (MalformedResponseException | NonSuccessfulResponseCodeException | PushNetworkException e) {
            if (body != null) {
                body.close();
            }
            throw e;
        }
    }

    public Response validateServiceResponse(Response response) throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        int code = response.code();
        String message = response.message();
        if (code != 401) {
            if (code != 413) {
                if (code == 417) {
                    throw new ExpectationFailedException();
                } else if (code == 423) {
                    RegistrationLockFailure registrationLockFailure = (RegistrationLockFailure) readResponseJson(response, RegistrationLockFailure.class);
                    AuthCredentials authCredentials = registrationLockFailure.backupCredentials;
                    throw new LockedException(registrationLockFailure.length, registrationLockFailure.timeRemaining, authCredentials != null ? authCredentials.asBasic() : null);
                } else if (code == 499) {
                    throw new DeprecatedVersionException();
                } else if (code == 508) {
                    throw new ServerRejectedException();
                } else if (code != 403) {
                    if (code == 404) {
                        throw new NotFoundException("Not found");
                    } else if (code == 428) {
                        throw new ProofRequiredException((ProofRequiredResponse) readResponseJson(response, ProofRequiredResponse.class), (long) Util.parseInt(response.header("Retry-After"), -1));
                    } else if (code != 429) {
                        switch (code) {
                            case 409:
                                throw new MismatchedDevicesException((MismatchedDevices) readResponseJson(response, MismatchedDevices.class));
                            case 410:
                                throw new StaleDevicesException((StaleDevices) readResponseJson(response, StaleDevices.class));
                            case 411:
                                throw new DeviceLimitExceededException((DeviceLimit) readResponseJson(response, DeviceLimit.class));
                            default:
                                if (code == 200 || code == 202 || code == 204) {
                                    return response;
                                }
                                throw new NonSuccessfulResponseCodeException(code, "Bad response: " + code + " " + message);
                        }
                    }
                }
            }
            long parseLong = Util.parseLong(response.header("Retry-After"), -1);
            Optional of = parseLong != -1 ? Optional.of(Long.valueOf(TimeUnit.SECONDS.toMillis(parseLong))) : Optional.empty();
            throw new RateLimitException(code, "Rate limit exceeded: " + code, of);
        }
        throw new AuthorizationFailedException(code, "Authorization failed!");
    }

    private Response getServiceConnection(String str, String str2, RequestBody requestBody, Map<String, String> map, Optional<UnidentifiedAccess> optional, boolean z) throws PushNetworkException {
        try {
            Call newCall = buildOkHttpClient(optional.isPresent()).newCall(buildServiceRequest(str, str2, requestBody, map, optional, z));
            synchronized (this.connections) {
                this.connections.add(newCall);
            }
            Response execute = newCall.execute();
            synchronized (this.connections) {
                this.connections.remove(newCall);
            }
            return execute;
        } catch (IOException e) {
            throw new PushNetworkException(e);
        }
    }

    private OkHttpClient buildOkHttpClient(boolean z) {
        ServiceConnectionHolder serviceConnectionHolder = (ServiceConnectionHolder) getRandom(this.serviceClients, this.random);
        OkHttpClient.Builder newBuilder = (z ? serviceConnectionHolder.getUnidentifiedClient() : serviceConnectionHolder.getClient()).newBuilder();
        long j = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        return newBuilder.connectTimeout(j, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).retryOnConnectionFailure(this.automaticNetworkRetry).build();
    }

    private Request buildServiceRequest(String str, String str2, RequestBody requestBody, Map<String, String> map, Optional<UnidentifiedAccess> optional, boolean z) {
        ServiceConnectionHolder serviceConnectionHolder = (ServiceConnectionHolder) getRandom(this.serviceClients, this.random);
        Request.Builder builder = new Request.Builder();
        builder.url(String.format("%s%s", serviceConnectionHolder.getUrl(), str));
        builder.method(str2, requestBody);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            builder.addHeader(entry.getKey(), entry.getValue());
        }
        if (!map.containsKey("Authorization") && !z) {
            if (optional.isPresent()) {
                builder.addHeader("Unidentified-Access-Key", Base64.encodeBytes(optional.get().getUnidentifiedAccessKey()));
            } else if (this.credentialsProvider.getPassword() != null) {
                builder.addHeader("Authorization", getAuthorizationHeader(this.credentialsProvider));
            }
        }
        String str3 = this.signalAgent;
        if (str3 != null) {
            builder.addHeader("X-Signal-Agent", str3);
        }
        if (serviceConnectionHolder.getHostHeader().isPresent()) {
            builder.addHeader("Host", serviceConnectionHolder.getHostHeader().get());
        }
        return builder.build();
    }

    /* renamed from: org.whispersystems.signalservice.internal.push.PushServiceSocket$5 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$push$PushServiceSocket$ClientSet;

        static {
            int[] iArr = new int[ClientSet.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$push$PushServiceSocket$ClientSet = iArr;
            try {
                iArr[ClientSet.ContactDiscovery.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$push$PushServiceSocket$ClientSet[ClientSet.KeyBackup.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    private ConnectionHolder[] clientsFor(ClientSet clientSet) {
        int i = AnonymousClass5.$SwitchMap$org$whispersystems$signalservice$internal$push$PushServiceSocket$ClientSet[clientSet.ordinal()];
        if (i == 1) {
            return this.contactDiscoveryClients;
        }
        if (i == 2) {
            return this.keyBackupServiceClients;
        }
        throw new AssertionError("Unknown attestation purpose");
    }

    public Response makeRequest(ClientSet clientSet, String str, List<String> list, String str2, String str3, String str4) throws PushNetworkException, NonSuccessfulResponseCodeException {
        return makeRequest(getRandom(clientsFor(clientSet), this.random), str, list, str2, str3, str4);
    }

    private Response makeRequest(ConnectionHolder connectionHolder, String str, List<String> list, String str2, String str3, String str4) throws PushNetworkException, NonSuccessfulResponseCodeException {
        OkHttpClient.Builder newBuilder = connectionHolder.getClient().newBuilder();
        long j = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = newBuilder.connectTimeout(j, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).build();
        Request.Builder builder = new Request.Builder();
        Request.Builder url = builder.url(connectionHolder.getUrl() + str2);
        if (str4 != null) {
            url.method(str3, RequestBody.create(MediaType.parse("application/json"), str4));
        } else {
            url.method(str3, null);
        }
        if (connectionHolder.getHostHeader().isPresent()) {
            url.addHeader("Host", connectionHolder.getHostHeader().get());
        }
        if (str != null) {
            url.addHeader("Authorization", str);
        }
        if (list != null && !list.isEmpty()) {
            url.addHeader("Cookie", Util.join(list, "; "));
        }
        Call newCall = build.newCall(url.build());
        synchronized (this.connections) {
            try {
                this.connections.add(newCall);
                try {
                } catch (Throwable th) {
                    synchronized (this.connections) {
                        try {
                            this.connections.remove(newCall);
                            throw th;
                        } catch (Throwable th2) {
                            throw th2;
                        }
                    }
                }
            } catch (Throwable th3) {
                throw th3;
            }
        }
        try {
            Response execute = newCall.execute();
            if (execute.isSuccessful()) {
                synchronized (this.connections) {
                    try {
                        this.connections.remove(newCall);
                    } catch (Throwable th4) {
                        throw th4;
                    }
                }
                return execute;
            }
            synchronized (this.connections) {
                try {
                    this.connections.remove(newCall);
                } catch (Throwable th5) {
                    throw th5;
                }
            }
            int code = execute.code();
            if (code == 401 || code == 403) {
                throw new AuthorizationFailedException(execute.code(), "Authorization failed!");
            } else if (code == 409) {
                throw new RemoteAttestationResponseExpiredException("Remote attestation response expired");
            } else if (code != 429) {
                int code2 = execute.code();
                throw new NonSuccessfulResponseCodeException(code2, "Response: " + execute);
            } else {
                int code3 = execute.code();
                throw new RateLimitException(code3, "Rate limit exceeded: " + execute.code());
            }
        } catch (IOException e) {
            throw new PushNetworkException(e);
        }
    }

    private void makeAndCloseStorageRequest(String str, String str2, String str3, RequestBody requestBody) throws PushNetworkException, NonSuccessfulResponseCodeException {
        makeAndCloseStorageRequest(str, str2, str3, requestBody, NO_HANDLER);
    }

    private void makeAndCloseStorageRequest(String str, String str2, String str3, RequestBody requestBody, ResponseCodeHandler responseCodeHandler) throws PushNetworkException, NonSuccessfulResponseCodeException {
        ResponseBody makeStorageRequest = makeStorageRequest(str, str2, str3, requestBody, responseCodeHandler);
        if (makeStorageRequest != null) {
            makeStorageRequest.close();
        }
    }

    private ResponseBody makeStorageRequest(String str, String str2, String str3, RequestBody requestBody) throws PushNetworkException, NonSuccessfulResponseCodeException {
        return makeStorageRequest(str, str2, str3, requestBody, NO_HANDLER);
    }

    private ResponseBody makeStorageRequest(String str, String str2, String str3, RequestBody requestBody, ResponseCodeHandler responseCodeHandler) throws PushNetworkException, NonSuccessfulResponseCodeException {
        return makeStorageRequestResponse(str, str2, str3, requestBody, responseCodeHandler).body();
    }

    private Response makeStorageRequestResponse(String str, String str2, String str3, RequestBody requestBody, ResponseCodeHandler responseCodeHandler) throws PushNetworkException, NonSuccessfulResponseCodeException {
        ConnectionHolder random = getRandom(this.storageClients, this.random);
        OkHttpClient.Builder newBuilder = random.getClient().newBuilder();
        long j = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = newBuilder.connectTimeout(j, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).build();
        Request.Builder builder = new Request.Builder();
        Request.Builder url = builder.url(random.getUrl() + str2);
        url.method(str3, requestBody);
        if (random.getHostHeader().isPresent()) {
            url.addHeader("Host", random.getHostHeader().get());
        }
        if (str != null) {
            url.addHeader("Authorization", str);
        }
        Call newCall = build.newCall(url.build());
        synchronized (this.connections) {
            try {
                this.connections.add(newCall);
                try {
                } catch (Throwable th) {
                    synchronized (this.connections) {
                        try {
                            this.connections.remove(newCall);
                            throw th;
                        } catch (Throwable th2) {
                            throw th2;
                        }
                    }
                }
            } catch (Throwable th3) {
                throw th3;
            }
        }
        try {
            Response execute = newCall.execute();
            if (execute.isSuccessful()) {
                if (execute.code() != 204) {
                    synchronized (this.connections) {
                        try {
                            this.connections.remove(newCall);
                        } catch (Throwable th4) {
                            throw th4;
                        }
                    }
                    return execute;
                }
            }
            synchronized (this.connections) {
                try {
                    this.connections.remove(newCall);
                } catch (Throwable th5) {
                    throw th5;
                }
            }
            ResponseBody body = execute.body();
            try {
                responseCodeHandler.handle(execute.code(), body, new PushServiceSocket$$ExternalSyntheticLambda3(execute));
                int code = execute.code();
                if (code != 204) {
                    if (code != 401) {
                        if (code != 409) {
                            if (code == 429) {
                                int code2 = execute.code();
                                throw new RateLimitException(code2, "Rate limit exceeded: " + execute.code());
                            } else if (code == 499) {
                                throw new DeprecatedVersionException();
                            } else if (code != 403) {
                                if (code != 404) {
                                    int code3 = execute.code();
                                    throw new NonSuccessfulResponseCodeException(code3, "Response: " + execute);
                                }
                                throw new NotFoundException("Not found");
                            }
                        } else if (body != null) {
                            throw new ContactManifestMismatchException(readBodyBytes(body));
                        } else {
                            throw new ConflictException();
                        }
                    }
                    throw new AuthorizationFailedException(execute.code(), "Authorization failed!");
                }
                throw new NoContentException("No content!");
            } catch (NonSuccessfulResponseCodeException | PushNetworkException e) {
                if (body != null) {
                    body.close();
                }
                throw e;
            }
        } catch (IOException e2) {
            throw new PushNetworkException(e2);
        }
    }

    public CallingResponse makeCallingRequest(long j, String str, String str2, List<Pair<String, String>> list, byte[] bArr) {
        OkHttpClient.Builder followRedirects = getRandom(this.serviceClients, this.random).getClient().newBuilder().followRedirects(false);
        long j2 = this.soTimeoutMillis;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        OkHttpClient build = followRedirects.connectTimeout(j2, timeUnit).readTimeout(this.soTimeoutMillis, timeUnit).build();
        Request.Builder method = new Request.Builder().url(str).method(str2, bArr != null ? RequestBody.create((MediaType) null, bArr) : null);
        if (list != null) {
            for (Pair<String, String> pair : list) {
                method.addHeader(pair.first(), pair.second());
            }
        }
        Request build2 = method.build();
        for (int i = 0; i < 20; i++) {
            try {
                Response execute = build.newCall(build2).execute();
                int code = execute.code();
                if (code != 307) {
                    CallingResponse.Success success = new CallingResponse.Success(j, code, execute.body() != null ? execute.body().bytes() : new byte[0]);
                    execute.close();
                    return success;
                }
                String header = execute.header("Location");
                HttpUrl resolve = header != null ? build2.url().resolve(header) : null;
                if (resolve != null) {
                    build2 = build2.newBuilder().url(resolve).build();
                    execute.close();
                } else {
                    CallingResponse.Error error = new CallingResponse.Error(j, new IOException("Received redirect without a valid Location header"));
                    execute.close();
                    return error;
                }
            } catch (IOException e) {
                Log.w(TAG, "Exception during ringrtc http call.", e);
                return new CallingResponse.Error(j, e);
            }
        }
        Log.w(TAG, "Calling request max redirects exceeded");
        return new CallingResponse.Error(j, new IOException("Redirect limit exceeded"));
    }

    private ServiceConnectionHolder[] createServiceConnectionHolders(SignalUrl[] signalUrlArr, List<Interceptor> list, Optional<Dns> optional, Optional<SignalProxy> optional2) {
        LinkedList linkedList = new LinkedList();
        for (SignalUrl signalUrl : signalUrlArr) {
            linkedList.add(new ServiceConnectionHolder(createConnectionClient(signalUrl, list, optional, optional2), createConnectionClient(signalUrl, list, optional, optional2), signalUrl.getUrl(), signalUrl.getHostHeader()));
        }
        return (ServiceConnectionHolder[]) linkedList.toArray(new ServiceConnectionHolder[0]);
    }

    private static Map<Integer, ConnectionHolder[]> createCdnClientsMap(Map<Integer, SignalCdnUrl[]> map, List<Interceptor> list, Optional<Dns> optional, Optional<SignalProxy> optional2) {
        validateConfiguration(map);
        HashMap hashMap = new HashMap();
        for (Map.Entry<Integer, SignalCdnUrl[]> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), createConnectionHolders(entry.getValue(), list, optional, optional2));
        }
        return Collections.unmodifiableMap(hashMap);
    }

    private static void validateConfiguration(Map<Integer, SignalCdnUrl[]> map) {
        if (!map.containsKey(0) || !map.containsKey(2)) {
            throw new AssertionError("Configuration used to create PushServiceSocket must support CDN 0 and CDN 2");
        }
    }

    private static ConnectionHolder[] createConnectionHolders(SignalUrl[] signalUrlArr, List<Interceptor> list, Optional<Dns> optional, Optional<SignalProxy> optional2) {
        LinkedList linkedList = new LinkedList();
        for (SignalUrl signalUrl : signalUrlArr) {
            linkedList.add(new ConnectionHolder(createConnectionClient(signalUrl, list, optional, optional2), signalUrl.getUrl(), signalUrl.getHostHeader()));
        }
        return (ConnectionHolder[]) linkedList.toArray(new ConnectionHolder[0]);
    }

    private static OkHttpClient createConnectionClient(SignalUrl signalUrl, List<Interceptor> list, Optional<Dns> optional, Optional<SignalProxy> optional2) {
        try {
            TrustManager[] createFor = BlacklistingTrustManager.createFor(signalUrl.getTrustStore());
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, createFor, null);
            OkHttpClient.Builder sslSocketFactory = new OkHttpClient.Builder().sslSocketFactory(new Tls12SocketFactory(instance.getSocketFactory()), (X509TrustManager) createFor[0]);
            Optional<List<ConnectionSpec>> connectionSpecs = signalUrl.getConnectionSpecs();
            ConnectionSpec connectionSpec = ConnectionSpec.RESTRICTED_TLS;
            OkHttpClient.Builder dns = sslSocketFactory.connectionSpecs(connectionSpecs.orElse(Util.immutableList(connectionSpec))).dns(optional.orElse(Dns.SYSTEM));
            if (optional2.isPresent()) {
                dns.socketFactory(new TlsProxySocketFactory(optional2.get().getHost(), optional2.get().getPort(), optional));
            }
            dns.sslSocketFactory(new Tls12SocketFactory(instance.getSocketFactory()), (X509TrustManager) createFor[0]).connectionSpecs(signalUrl.getConnectionSpecs().orElse(Util.immutableList(connectionSpec))).build();
            dns.connectionPool(new ConnectionPool(5, 45, TimeUnit.SECONDS));
            for (Interceptor interceptor : list) {
                dns.addInterceptor(interceptor);
            }
            return dns.build();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    private String getAuthorizationHeader(CredentialsProvider credentialsProvider) {
        try {
            String serviceId = credentialsProvider.getAci() != null ? credentialsProvider.getAci().toString() : credentialsProvider.getE164();
            if (credentialsProvider.getDeviceId() != 1) {
                serviceId = serviceId + "." + credentialsProvider.getDeviceId();
            }
            return "Basic " + Base64.encodeBytes((serviceId + ":" + credentialsProvider.getPassword()).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    private ConnectionHolder getRandom(ConnectionHolder[] connectionHolderArr, SecureRandom secureRandom) {
        return connectionHolderArr[secureRandom.nextInt(connectionHolderArr.length)];
    }

    public ProfileKeyCredential parseResponse(UUID uuid, ProfileKey profileKey, ProfileKeyCredentialResponse profileKeyCredentialResponse) throws VerificationFailedException {
        return this.clientZkProfileOperations.receiveProfileKeyCredential(this.clientZkProfileOperations.createProfileKeyCredentialRequestContext(this.random, uuid, profileKey), profileKeyCredentialResponse);
    }

    private static byte[] readBodyBytes(ResponseBody responseBody) throws PushNetworkException {
        if (responseBody != null) {
            try {
                return responseBody.bytes();
            } catch (IOException e) {
                throw new PushNetworkException(e);
            }
        } else {
            throw new PushNetworkException("No body!");
        }
    }

    public static String readBodyString(ResponseBody responseBody) throws PushNetworkException {
        if (responseBody != null) {
            try {
                return responseBody.string();
            } catch (IOException e) {
                throw new PushNetworkException(e);
            }
        } else {
            throw new PushNetworkException("No body!");
        }
    }

    private static <T> T readBodyJson(ResponseBody responseBody, Class<T> cls) throws PushNetworkException, MalformedResponseException {
        try {
            return (T) JsonUtil.fromJson(readBodyString(responseBody), cls);
        } catch (JsonProcessingException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        } catch (IOException e2) {
            throw new PushNetworkException(e2);
        }
    }

    private static <T> T readResponseJson(Response response, Class<T> cls) throws PushNetworkException, MalformedResponseException {
        return (T) readBodyJson(response.body(), cls);
    }

    /* loaded from: classes.dex */
    public static class GcmRegistrationId {
        @JsonProperty
        private String gcmRegistrationId;
        @JsonProperty
        private boolean webSocketChannel;

        public GcmRegistrationId() {
        }

        public GcmRegistrationId(String str, boolean z) {
            this.gcmRegistrationId = str;
            this.webSocketChannel = z;
        }
    }

    /* loaded from: classes.dex */
    private static class RegistrationLock {
        @JsonProperty
        private String pin;

        public RegistrationLock() {
        }

        public RegistrationLock(String str) {
            this.pin = str;
        }
    }

    /* loaded from: classes.dex */
    public static class RegistrationLockV2 {
        @JsonProperty
        private String registrationLock;

        public RegistrationLockV2() {
        }

        public RegistrationLockV2(String str) {
            this.registrationLock = str;
        }
    }

    /* loaded from: classes5.dex */
    public static class ConnectionHolder {
        private final OkHttpClient client;
        private final Optional<String> hostHeader;
        private final String url;

        private ConnectionHolder(OkHttpClient okHttpClient, String str, Optional<String> optional) {
            this.client = okHttpClient;
            this.url = str;
            this.hostHeader = optional;
        }

        OkHttpClient getClient() {
            return this.client;
        }

        public String getUrl() {
            return this.url;
        }

        Optional<String> getHostHeader() {
            return this.hostHeader;
        }
    }

    /* loaded from: classes5.dex */
    public static class ServiceConnectionHolder extends ConnectionHolder {
        private final OkHttpClient unidentifiedClient;

        private ServiceConnectionHolder(OkHttpClient okHttpClient, OkHttpClient okHttpClient2, String str, Optional<String> optional) {
            super(okHttpClient, str, optional);
            this.unidentifiedClient = okHttpClient2;
        }

        OkHttpClient getUnidentifiedClient() {
            return this.unidentifiedClient;
        }
    }

    /* loaded from: classes5.dex */
    public interface ResponseCodeHandler {
        void handle(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException;

        void handle(int i, ResponseBody responseBody, Function<String, String> function) throws NonSuccessfulResponseCodeException, PushNetworkException;

        /* renamed from: org.whispersystems.signalservice.internal.push.PushServiceSocket$ResponseCodeHandler$-CC */
        /* loaded from: classes5.dex */
        public final /* synthetic */ class CC {
            public static void $default$handle(ResponseCodeHandler responseCodeHandler, int i, ResponseBody responseBody, Function function) throws NonSuccessfulResponseCodeException, PushNetworkException {
                responseCodeHandler.handle(i, responseBody);
            }
        }
    }

    /* loaded from: classes5.dex */
    private static class EmptyResponseCodeHandler implements ResponseCodeHandler {
        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public void handle(int i, ResponseBody responseBody) {
        }

        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
            ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
        }

        private EmptyResponseCodeHandler() {
        }
    }

    public CredentialResponse retrieveGroupsV2Credentials(long j) throws IOException {
        return (CredentialResponse) JsonUtil.fromJson(makeServiceRequest(String.format(Locale.US, GROUPSV2_CREDENTIAL, Long.valueOf(j), Long.valueOf(TimeUnit.DAYS.toSeconds(7) + j)), "GET", (String) null, NO_HEADERS, Optional.empty()), CredentialResponse.class);
    }

    public static /* synthetic */ void lambda$static$7(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
        if (i == 409) {
            throw new GroupExistsException();
        }
    }

    public static /* synthetic */ void lambda$static$8(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
        if (i == 403) {
            throw new NotInGroupException();
        } else if (i == 404) {
            throw new GroupNotFoundException();
        }
    }

    public static /* synthetic */ void lambda$static$9(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
        if (i == 400) {
            throw new GroupPatchNotAcceptedException();
        }
    }

    public void putNewGroupsV2Group(Group group, GroupsV2AuthorizationString groupsV2AuthorizationString) throws NonSuccessfulResponseCodeException, PushNetworkException {
        makeAndCloseStorageRequest(groupsV2AuthorizationString.toString(), GROUPSV2_GROUP, "PUT", protobufRequestBody(group), GROUPS_V2_PUT_RESPONSE_HANDLER);
    }

    public Group getGroupsV2Group(GroupsV2AuthorizationString groupsV2AuthorizationString) throws NonSuccessfulResponseCodeException, PushNetworkException, InvalidProtocolBufferException {
        return Group.parseFrom(readBodyBytes(makeStorageRequest(groupsV2AuthorizationString.toString(), GROUPSV2_GROUP, "GET", null, GROUPS_V2_GET_CURRENT_HANDLER)));
    }

    public AvatarUploadAttributes getGroupsV2AvatarUploadForm(String str) throws NonSuccessfulResponseCodeException, PushNetworkException, InvalidProtocolBufferException {
        return AvatarUploadAttributes.parseFrom(readBodyBytes(makeStorageRequest(str, GROUPSV2_AVATAR_REQUEST, "GET", null, NO_HANDLER)));
    }

    public GroupChange patchGroupsV2Group(GroupChange.Actions actions, String str, Optional<byte[]> optional) throws NonSuccessfulResponseCodeException, PushNetworkException, InvalidProtocolBufferException {
        return GroupChange.parseFrom(readBodyBytes(makeStorageRequest(str, optional.isPresent() ? String.format(GROUPSV2_GROUP_PASSWORD, Base64UrlSafe.encodeBytesWithoutPadding(optional.get())) : GROUPSV2_GROUP, "PATCH", protobufRequestBody(actions), GROUPS_V2_PATCH_RESPONSE_HANDLER)));
    }

    public GroupHistory getGroupsV2GroupHistory(int i, GroupsV2AuthorizationString groupsV2AuthorizationString, int i2, boolean z) throws IOException {
        Response makeStorageRequestResponse = makeStorageRequestResponse(groupsV2AuthorizationString.toString(), String.format(Locale.US, GROUPSV2_GROUP_CHANGES, Integer.valueOf(i), Integer.valueOf(i2), Boolean.valueOf(z)), "GET", null, GROUPS_V2_GET_LOGS_HANDLER);
        if (makeStorageRequestResponse.body() != null) {
            try {
                InputStream byteStream = makeStorageRequestResponse.body().byteStream();
                GroupChanges parseFrom = GroupChanges.parseFrom(byteStream);
                if (byteStream != null) {
                    byteStream.close();
                }
                if (makeStorageRequestResponse.code() != 206) {
                    return new GroupHistory(parseFrom, Optional.empty());
                }
                String header = makeStorageRequestResponse.header("Content-Range");
                Optional<ContentRange> parse = ContentRange.parse(header);
                if (parse.isPresent()) {
                    String str = TAG;
                    Log.i(str, "Additional logs for group: " + header);
                    return new GroupHistory(parseFrom, parse);
                }
                String str2 = TAG;
                Log.w(str2, "Unable to parse Content-Range header: " + header);
                throw new MalformedResponseException("Unable to parse content range header on 206");
            } catch (IOException e) {
                throw new PushNetworkException(e);
            }
        } else {
            throw new PushNetworkException("No body!");
        }
    }

    public GroupJoinInfo getGroupJoinInfo(Optional<byte[]> optional, GroupsV2AuthorizationString groupsV2AuthorizationString) throws NonSuccessfulResponseCodeException, PushNetworkException, InvalidProtocolBufferException {
        return GroupJoinInfo.parseFrom(readBodyBytes(makeStorageRequest(groupsV2AuthorizationString.toString(), String.format(GROUPSV2_GROUP_JOIN, (String) optional.map(new Function() { // from class: org.whispersystems.signalservice.internal.push.PushServiceSocket$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Base64UrlSafe.encodeBytesWithoutPadding((byte[]) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse("")), "GET", null, GROUPS_V2_GET_JOIN_INFO_HANDLER)));
    }

    public GroupExternalCredential getGroupExternalCredential(GroupsV2AuthorizationString groupsV2AuthorizationString) throws NonSuccessfulResponseCodeException, PushNetworkException, InvalidProtocolBufferException {
        return GroupExternalCredential.parseFrom(readBodyBytes(makeStorageRequest(groupsV2AuthorizationString.toString(), GROUPSV2_TOKEN, "GET", null, NO_HANDLER)));
    }

    public CurrencyConversions getCurrencyConversions() throws NonSuccessfulResponseCodeException, PushNetworkException, MalformedResponseException {
        try {
            return (CurrencyConversions) JsonUtil.fromJson(makeServiceRequest(PAYMENTS_CONVERSIONS, "GET", null), CurrencyConversions.class);
        } catch (IOException e) {
            Log.w(TAG, e);
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public void reportSpam(ServiceId serviceId, String str) throws NonSuccessfulResponseCodeException, MalformedResponseException, PushNetworkException {
        makeServiceRequest(String.format(REPORT_SPAM, serviceId.toString(), str), "POST", "");
    }

    /* loaded from: classes5.dex */
    public static class VerificationCodeResponseHandler implements ResponseCodeHandler {
        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public /* synthetic */ void handle(int i, ResponseBody responseBody, Function function) {
            ResponseCodeHandler.CC.$default$handle(this, i, responseBody, function);
        }

        private VerificationCodeResponseHandler() {
        }

        @Override // org.whispersystems.signalservice.internal.push.PushServiceSocket.ResponseCodeHandler
        public void handle(int i, ResponseBody responseBody) throws NonSuccessfulResponseCodeException, PushNetworkException {
            String string;
            if (i == 400) {
                if (responseBody != null) {
                    try {
                        string = responseBody.string();
                    } catch (IOException e) {
                        throw new PushNetworkException(e);
                    }
                } else {
                    string = "";
                }
                if (string.isEmpty()) {
                    throw new ImpossiblePhoneNumberException();
                }
                try {
                    throw NonNormalizedPhoneNumberException.forResponse(string);
                } catch (MalformedResponseException unused) {
                    Log.w(PushServiceSocket.TAG, "Unable to parse 400 response! Assuming a generic 400.");
                    throw new ImpossiblePhoneNumberException();
                }
            } else if (i == 402) {
                throw new CaptchaRequiredException();
            }
        }
    }

    /* loaded from: classes5.dex */
    public static final class GroupHistory {
        private final Optional<ContentRange> contentRange;
        private final GroupChanges groupChanges;

        public GroupHistory(GroupChanges groupChanges, Optional<ContentRange> optional) {
            this.groupChanges = groupChanges;
            this.contentRange = optional;
        }

        public GroupChanges getGroupChanges() {
            return this.groupChanges;
        }

        public boolean hasMore() {
            return this.contentRange.isPresent();
        }

        public int getNextPageStartGroupRevision() {
            return this.contentRange.get().getRangeEnd() + 1;
        }
    }

    /* loaded from: classes5.dex */
    public final class ResumeInfo {
        private final String contentRange;
        private final long contentStart;

        private ResumeInfo(String str, long j) {
            PushServiceSocket.this = r1;
            this.contentRange = str;
            this.contentStart = j;
        }
    }
}
