package org.whispersystems.signalservice.internal.push;

import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes5.dex */
public final class LockedException extends NonSuccessfulResponseCodeException {
    private final String basicStorageCredentials;
    private final int length;
    private final long timeRemaining;

    public LockedException(int i, long j, String str) {
        super(423);
        this.length = i;
        this.timeRemaining = j;
        this.basicStorageCredentials = str;
    }

    public int getLength() {
        return this.length;
    }

    public long getTimeRemaining() {
        return this.timeRemaining;
    }

    public String getBasicStorageCredentials() {
        return this.basicStorageCredentials;
    }
}
