package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class BackupResponse extends GeneratedMessageLite<BackupResponse, Builder> implements BackupResponseOrBuilder {
    private static final BackupResponse DEFAULT_INSTANCE;
    private static volatile Parser<BackupResponse> PARSER;
    public static final int STATUS_FIELD_NUMBER;
    public static final int TOKEN_FIELD_NUMBER;
    private int bitField0_;
    private int status_ = 1;
    private ByteString token_ = ByteString.EMPTY;

    private BackupResponse() {
    }

    /* loaded from: classes5.dex */
    public enum Status implements Internal.EnumLite {
        OK(1),
        ALREADY_EXISTS(2),
        NOT_YET_VALID(3);
        
        public static final int ALREADY_EXISTS_VALUE;
        public static final int NOT_YET_VALID_VALUE;
        public static final int OK_VALUE;
        private static final Internal.EnumLiteMap<Status> internalValueMap = new Internal.EnumLiteMap<Status>() { // from class: org.whispersystems.signalservice.internal.keybackup.protos.BackupResponse.Status.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public Status findValueByNumber(int i) {
                return Status.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            return this.value;
        }

        @Deprecated
        public static Status valueOf(int i) {
            return forNumber(i);
        }

        public static Status forNumber(int i) {
            if (i == 1) {
                return OK;
            }
            if (i == 2) {
                return ALREADY_EXISTS;
            }
            if (i != 3) {
                return null;
            }
            return NOT_YET_VALID;
        }

        public static Internal.EnumLiteMap<Status> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return StatusVerifier.INSTANCE;
        }

        /* loaded from: classes5.dex */
        public static final class StatusVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new StatusVerifier();

            private StatusVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return Status.forNumber(i) != null;
            }
        }

        Status(int i) {
            this.value = i;
        }
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
    public boolean hasStatus() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
    public Status getStatus() {
        Status forNumber = Status.forNumber(this.status_);
        return forNumber == null ? Status.OK : forNumber;
    }

    public void setStatus(Status status) {
        this.status_ = status.getNumber();
        this.bitField0_ |= 1;
    }

    public void clearStatus() {
        this.bitField0_ &= -2;
        this.status_ = 1;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
    public boolean hasToken() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
    public ByteString getToken() {
        return this.token_;
    }

    public void setToken(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 2;
        this.token_ = byteString;
    }

    public void clearToken() {
        this.bitField0_ &= -3;
        this.token_ = getDefaultInstance().getToken();
    }

    public static BackupResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static BackupResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static BackupResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static BackupResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static BackupResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static BackupResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static BackupResponse parseFrom(InputStream inputStream) throws IOException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BackupResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BackupResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (BackupResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BackupResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BackupResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BackupResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static BackupResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BackupResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(BackupResponse backupResponse) {
        return DEFAULT_INSTANCE.createBuilder(backupResponse);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<BackupResponse, Builder> implements BackupResponseOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(BackupResponse.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
        public boolean hasStatus() {
            return ((BackupResponse) this.instance).hasStatus();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
        public Status getStatus() {
            return ((BackupResponse) this.instance).getStatus();
        }

        public Builder setStatus(Status status) {
            copyOnWrite();
            ((BackupResponse) this.instance).setStatus(status);
            return this;
        }

        public Builder clearStatus() {
            copyOnWrite();
            ((BackupResponse) this.instance).clearStatus();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
        public boolean hasToken() {
            return ((BackupResponse) this.instance).hasToken();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupResponseOrBuilder
        public ByteString getToken() {
            return ((BackupResponse) this.instance).getToken();
        }

        public Builder setToken(ByteString byteString) {
            copyOnWrite();
            ((BackupResponse) this.instance).setToken(byteString);
            return this;
        }

        public Builder clearToken() {
            copyOnWrite();
            ((BackupResponse) this.instance).clearToken();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.BackupResponse$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new BackupResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဌ\u0000\u0002ည\u0001", new Object[]{"bitField0_", "status_", Status.internalGetVerifier(), "token_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<BackupResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (BackupResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        BackupResponse backupResponse = new BackupResponse();
        DEFAULT_INSTANCE = backupResponse;
        GeneratedMessageLite.registerDefaultInstance(BackupResponse.class, backupResponse);
    }

    public static BackupResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<BackupResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
