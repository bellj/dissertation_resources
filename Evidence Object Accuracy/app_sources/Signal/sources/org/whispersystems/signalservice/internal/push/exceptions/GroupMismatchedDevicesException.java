package org.whispersystems.signalservice.internal.push.exceptions;

import java.util.Arrays;
import java.util.List;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.push.GroupMismatchedDevices;

/* loaded from: classes5.dex */
public class GroupMismatchedDevicesException extends NonSuccessfulResponseCodeException {
    private final List<GroupMismatchedDevices> mismatchedDevices;

    public GroupMismatchedDevicesException(GroupMismatchedDevices[] groupMismatchedDevicesArr) {
        super(409);
        this.mismatchedDevices = Arrays.asList(groupMismatchedDevicesArr);
    }

    public List<GroupMismatchedDevices> getMismatchedDevices() {
        return this.mismatchedDevices;
    }
}
