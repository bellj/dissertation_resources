package org.whispersystems.signalservice.internal.websocket;

import j$.util.function.Function;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* loaded from: classes5.dex */
public interface ResponseMapper<T> {
    ServiceResponse<T> map(int i, String str, Function<String, String> function, boolean z);

    ServiceResponse<T> map(WebsocketResponse websocketResponse);

    /* renamed from: org.whispersystems.signalservice.internal.websocket.ResponseMapper$-CC */
    /* loaded from: classes5.dex */
    public final /* synthetic */ class CC<T> {
        public static ServiceResponse $default$map(ResponseMapper responseMapper, WebsocketResponse websocketResponse) {
            return responseMapper.map(websocketResponse.getStatus(), websocketResponse.getBody(), new DefaultErrorMapper$$ExternalSyntheticLambda0(websocketResponse), websocketResponse.isUnidentified());
        }
    }
}
