package org.whispersystems.signalservice.internal.push;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.Locale;
import okhttp3.Call;
import okhttp3.Connection;
import okhttp3.EventListener;
import okhttp3.Handshake;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import org.signal.libsignal.protocol.logging.Log;

/* loaded from: classes5.dex */
public final class LoggingOkhttpEventListener extends EventListener {
    private static final String TAG;
    private long callStartNanos;
    private StringBuilder logMessage;

    private void printEvent(String str) {
        long nanoTime = System.nanoTime();
        if (str.equals("callStart")) {
            this.callStartNanos = nanoTime;
            this.logMessage = new StringBuilder();
        }
        long j = nanoTime - this.callStartNanos;
        StringBuilder sb = this.logMessage;
        Locale locale = Locale.US;
        double d = (double) j;
        Double.isNaN(d);
        sb.append(String.format(locale, "[%.3f %s] ", Double.valueOf(d / 1.0E9d), str));
        if (str.equals("callEnd") || str.equals("callFailed")) {
            Log.d(TAG, this.logMessage.toString());
        }
    }

    @Override // okhttp3.EventListener
    public void callStart(Call call) {
        printEvent("callStart");
    }

    @Override // okhttp3.EventListener
    public void dnsStart(Call call, String str) {
        printEvent("dnsStart");
    }

    @Override // okhttp3.EventListener
    public void dnsEnd(Call call, String str, List<InetAddress> list) {
        printEvent("dnsEnd");
    }

    @Override // okhttp3.EventListener
    public void connectStart(Call call, InetSocketAddress inetSocketAddress, Proxy proxy) {
        printEvent("connectStart");
    }

    @Override // okhttp3.EventListener
    public void secureConnectStart(Call call) {
        printEvent("secureConnectStart");
    }

    @Override // okhttp3.EventListener
    public void secureConnectEnd(Call call, Handshake handshake) {
        printEvent("secureConnectEnd");
    }

    @Override // okhttp3.EventListener
    public void connectEnd(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol) {
        printEvent("connectEnd");
    }

    @Override // okhttp3.EventListener
    public void connectFailed(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol, IOException iOException) {
        printEvent("connectFailed");
    }

    @Override // okhttp3.EventListener
    public void connectionAcquired(Call call, Connection connection) {
        printEvent("connectionAcquired");
    }

    @Override // okhttp3.EventListener
    public void connectionReleased(Call call, Connection connection) {
        printEvent("connectionReleased");
    }

    @Override // okhttp3.EventListener
    public void requestHeadersStart(Call call) {
        printEvent("requestHeadersStart");
    }

    @Override // okhttp3.EventListener
    public void requestHeadersEnd(Call call, Request request) {
        printEvent("requestHeadersEnd");
    }

    @Override // okhttp3.EventListener
    public void requestBodyStart(Call call) {
        printEvent("requestBodyStart");
    }

    @Override // okhttp3.EventListener
    public void requestBodyEnd(Call call, long j) {
        printEvent("requestBodyEnd");
    }

    @Override // okhttp3.EventListener
    public void responseHeadersStart(Call call) {
        printEvent("responseHeadersStart");
    }

    @Override // okhttp3.EventListener
    public void responseHeadersEnd(Call call, Response response) {
        printEvent("responseHeadersEnd");
    }

    @Override // okhttp3.EventListener
    public void responseBodyStart(Call call) {
        printEvent("responseBodyStart");
    }

    @Override // okhttp3.EventListener
    public void responseBodyEnd(Call call, long j) {
        printEvent("responseBodyEnd");
    }

    @Override // okhttp3.EventListener
    public void callEnd(Call call) {
        printEvent("callEnd");
    }

    @Override // okhttp3.EventListener
    public void callFailed(Call call, IOException iOException) {
        printEvent("callFailed");
    }
}
