package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public class SenderCertificate {
    @JsonProperty
    @JsonDeserialize(using = ByteArrayDesieralizer.class)
    @JsonSerialize(using = ByteArraySerializer.class)
    private byte[] certificate;

    public byte[] getCertificate() {
        return this.certificate;
    }

    /* loaded from: classes5.dex */
    public static class ByteArraySerializer extends JsonSerializer<byte[]> {
        public void serialize(byte[] bArr, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Base64.encodeBytes(bArr));
        }
    }

    /* loaded from: classes5.dex */
    public static class ByteArrayDesieralizer extends JsonDeserializer<byte[]> {
        public byte[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return Base64.decode(jsonParser.getValueAsString());
        }
    }
}
