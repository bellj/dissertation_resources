package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.List;
import org.signal.libsignal.protocol.IdentityKey;
import org.whispersystems.signalservice.api.push.SignedPreKeyEntity;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes.dex */
public class PreKeyState {
    @JsonProperty
    @JsonDeserialize(using = JsonUtil.IdentityKeyDeserializer.class)
    @JsonSerialize(using = JsonUtil.IdentityKeySerializer.class)
    private IdentityKey identityKey;
    @JsonProperty
    private List<PreKeyEntity> preKeys;
    @JsonProperty
    private SignedPreKeyEntity signedPreKey;

    public PreKeyState(List<PreKeyEntity> list, SignedPreKeyEntity signedPreKeyEntity, IdentityKey identityKey) {
        this.preKeys = list;
        this.signedPreKey = signedPreKeyEntity;
        this.identityKey = identityKey;
    }
}
