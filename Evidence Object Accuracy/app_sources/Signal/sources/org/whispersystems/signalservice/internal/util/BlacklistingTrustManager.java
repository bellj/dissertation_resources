package org.whispersystems.signalservice.internal.util;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.signal.libsignal.protocol.util.Pair;
import org.whispersystems.signalservice.api.push.TrustStore;

/* loaded from: classes5.dex */
public class BlacklistingTrustManager implements X509TrustManager {
    private static final List<Pair<String, BigInteger>> BLACKLIST = new LinkedList<Pair<String, BigInteger>>() { // from class: org.whispersystems.signalservice.internal.util.BlacklistingTrustManager.1
        {
            add(new Pair("Open Whisper Systems", new BigInteger("4098")));
        }
    };
    private final X509TrustManager trustManager;

    public static TrustManager[] createFor(TrustManager[] trustManagerArr) {
        for (TrustManager trustManager : trustManagerArr) {
            if (trustManager instanceof X509TrustManager) {
                return new BlacklistingTrustManager[]{new BlacklistingTrustManager((X509TrustManager) trustManager)};
            }
        }
        throw new AssertionError("No X509 Trust Managers!");
    }

    public static TrustManager[] createFor(TrustStore trustStore) {
        try {
            InputStream keyStoreInputStream = trustStore.getKeyStoreInputStream();
            KeyStore instance = KeyStore.getInstance("BKS");
            instance.load(keyStoreInputStream, trustStore.getKeyStorePassword().toCharArray());
            TrustManagerFactory instance2 = TrustManagerFactory.getInstance("X509");
            instance2.init(instance);
            return createFor(instance2.getTrustManagers());
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new AssertionError(e);
        }
    }

    public BlacklistingTrustManager(X509TrustManager x509TrustManager) {
        this.trustManager = x509TrustManager;
    }

    @Override // javax.net.ssl.X509TrustManager
    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        this.trustManager.checkClientTrusted(x509CertificateArr, str);
    }

    @Override // javax.net.ssl.X509TrustManager
    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        this.trustManager.checkServerTrusted(x509CertificateArr, str);
        for (X509Certificate x509Certificate : x509CertificateArr) {
            for (Pair<String, BigInteger> pair : BLACKLIST) {
                if (x509Certificate.getIssuerDN().getName().equals(pair.first()) && x509Certificate.getSerialNumber().equals(pair.second())) {
                    throw new CertificateException("Blacklisted Serial: " + x509Certificate.getSerialNumber());
                }
            }
        }
    }

    @Override // javax.net.ssl.X509TrustManager
    public X509Certificate[] getAcceptedIssuers() {
        return this.trustManager.getAcceptedIssuers();
    }
}
