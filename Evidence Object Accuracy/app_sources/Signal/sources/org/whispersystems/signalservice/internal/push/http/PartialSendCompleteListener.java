package org.whispersystems.signalservice.internal.push.http;

import org.whispersystems.signalservice.api.messages.SendMessageResult;

/* loaded from: classes5.dex */
public interface PartialSendCompleteListener {
    void onPartialSendComplete(SendMessageResult sendMessageResult);
}
