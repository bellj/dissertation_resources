package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class SendMessageResponse {
    @JsonProperty
    private boolean needsSync;
    private boolean sentUnidentfied;

    public SendMessageResponse() {
    }

    public SendMessageResponse(boolean z, boolean z2) {
        this.needsSync = z;
        this.sentUnidentfied = z2;
    }

    public boolean getNeedsSync() {
        return this.needsSync;
    }

    public boolean sentUnidentified() {
        return this.sentUnidentfied;
    }

    public void setSentUnidentfied(boolean z) {
        this.sentUnidentfied = z;
    }
}
