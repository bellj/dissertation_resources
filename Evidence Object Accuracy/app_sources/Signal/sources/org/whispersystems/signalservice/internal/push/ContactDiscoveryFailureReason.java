package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class ContactDiscoveryFailureReason {
    @JsonProperty
    private final String reason;

    public ContactDiscoveryFailureReason(String str) {
        this.reason = str == null ? "" : str;
    }
}
