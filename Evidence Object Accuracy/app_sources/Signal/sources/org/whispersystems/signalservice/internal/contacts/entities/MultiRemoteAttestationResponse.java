package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/* loaded from: classes.dex */
public class MultiRemoteAttestationResponse {
    @JsonProperty
    private Map<String, RemoteAttestationResponse> attestations;

    public Map<String, RemoteAttestationResponse> getAttestations() {
        return this.attestations;
    }
}
