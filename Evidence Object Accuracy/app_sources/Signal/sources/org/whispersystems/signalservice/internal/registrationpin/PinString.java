package org.whispersystems.signalservice.internal.registrationpin;

/* loaded from: classes5.dex */
public final class PinString {
    PinString() {
    }

    public static boolean allNumeric(CharSequence charSequence) {
        for (int i = 0; i < charSequence.length(); i++) {
            if (!Character.isDigit(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String toArabic(CharSequence charSequence) {
        int length = charSequence.length();
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = (char) (Character.digit(charSequence.charAt(i), 10) + 48);
        }
        return new String(cArr);
    }
}
