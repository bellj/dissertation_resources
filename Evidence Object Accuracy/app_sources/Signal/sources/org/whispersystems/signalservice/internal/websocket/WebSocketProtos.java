package org.whispersystems.signalservice.internal.websocket;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes5.dex */
public final class WebSocketProtos {

    /* loaded from: classes5.dex */
    public interface WebSocketMessageOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        WebSocketRequestMessage getRequest();

        WebSocketResponseMessage getResponse();

        WebSocketMessage.Type getType();

        boolean hasRequest();

        boolean hasResponse();

        boolean hasType();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes5.dex */
    public interface WebSocketRequestMessageOrBuilder extends MessageLiteOrBuilder {
        ByteString getBody();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getHeaders(int i);

        ByteString getHeadersBytes(int i);

        int getHeadersCount();

        List<String> getHeadersList();

        long getId();

        String getPath();

        ByteString getPathBytes();

        String getVerb();

        ByteString getVerbBytes();

        boolean hasBody();

        boolean hasId();

        boolean hasPath();

        boolean hasVerb();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes5.dex */
    public interface WebSocketResponseMessageOrBuilder extends MessageLiteOrBuilder {
        ByteString getBody();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getHeaders(int i);

        ByteString getHeadersBytes(int i);

        int getHeadersCount();

        List<String> getHeadersList();

        long getId();

        String getMessage();

        ByteString getMessageBytes();

        int getStatus();

        boolean hasBody();

        boolean hasId();

        boolean hasMessage();

        boolean hasStatus();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }

    private WebSocketProtos() {
    }

    /* loaded from: classes5.dex */
    public static final class WebSocketRequestMessage extends GeneratedMessageLite<WebSocketRequestMessage, Builder> implements WebSocketRequestMessageOrBuilder {
        public static final int BODY_FIELD_NUMBER;
        private static final WebSocketRequestMessage DEFAULT_INSTANCE;
        public static final int HEADERS_FIELD_NUMBER;
        public static final int ID_FIELD_NUMBER;
        private static volatile Parser<WebSocketRequestMessage> PARSER;
        public static final int PATH_FIELD_NUMBER;
        public static final int VERB_FIELD_NUMBER;
        private int bitField0_;
        private ByteString body_ = ByteString.EMPTY;
        private Internal.ProtobufList<String> headers_ = GeneratedMessageLite.emptyProtobufList();
        private long id_;
        private String path_ = "";
        private String verb_ = "";

        private WebSocketRequestMessage() {
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public boolean hasVerb() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public String getVerb() {
            return this.verb_;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public ByteString getVerbBytes() {
            return ByteString.copyFromUtf8(this.verb_);
        }

        public void setVerb(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.verb_ = str;
        }

        public void clearVerb() {
            this.bitField0_ &= -2;
            this.verb_ = getDefaultInstance().getVerb();
        }

        public void setVerbBytes(ByteString byteString) {
            this.verb_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public boolean hasPath() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public String getPath() {
            return this.path_;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public ByteString getPathBytes() {
            return ByteString.copyFromUtf8(this.path_);
        }

        public void setPath(String str) {
            str.getClass();
            this.bitField0_ |= 2;
            this.path_ = str;
        }

        public void clearPath() {
            this.bitField0_ &= -3;
            this.path_ = getDefaultInstance().getPath();
        }

        public void setPathBytes(ByteString byteString) {
            this.path_ = byteString.toStringUtf8();
            this.bitField0_ |= 2;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public boolean hasBody() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public ByteString getBody() {
            return this.body_;
        }

        public void setBody(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 4;
            this.body_ = byteString;
        }

        public void clearBody() {
            this.bitField0_ &= -5;
            this.body_ = getDefaultInstance().getBody();
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public List<String> getHeadersList() {
            return this.headers_;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public int getHeadersCount() {
            return this.headers_.size();
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public String getHeaders(int i) {
            return this.headers_.get(i);
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public ByteString getHeadersBytes(int i) {
            return ByteString.copyFromUtf8(this.headers_.get(i));
        }

        private void ensureHeadersIsMutable() {
            Internal.ProtobufList<String> protobufList = this.headers_;
            if (!protobufList.isModifiable()) {
                this.headers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setHeaders(int i, String str) {
            str.getClass();
            ensureHeadersIsMutable();
            this.headers_.set(i, str);
        }

        public void addHeaders(String str) {
            str.getClass();
            ensureHeadersIsMutable();
            this.headers_.add(str);
        }

        public void addAllHeaders(Iterable<String> iterable) {
            ensureHeadersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.headers_);
        }

        public void clearHeaders() {
            this.headers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void addHeadersBytes(ByteString byteString) {
            ensureHeadersIsMutable();
            this.headers_.add(byteString.toStringUtf8());
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public boolean hasId() {
            return (this.bitField0_ & 8) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
        public long getId() {
            return this.id_;
        }

        public void setId(long j) {
            this.bitField0_ |= 8;
            this.id_ = j;
        }

        public void clearId() {
            this.bitField0_ &= -9;
            this.id_ = 0;
        }

        public static WebSocketRequestMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static WebSocketRequestMessage parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static WebSocketRequestMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static WebSocketRequestMessage parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static WebSocketRequestMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static WebSocketRequestMessage parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static WebSocketRequestMessage parseFrom(InputStream inputStream) throws IOException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static WebSocketRequestMessage parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static WebSocketRequestMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static WebSocketRequestMessage parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static WebSocketRequestMessage parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static WebSocketRequestMessage parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketRequestMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(WebSocketRequestMessage webSocketRequestMessage) {
            return DEFAULT_INSTANCE.createBuilder(webSocketRequestMessage);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<WebSocketRequestMessage, Builder> implements WebSocketRequestMessageOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(WebSocketRequestMessage.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public boolean hasVerb() {
                return ((WebSocketRequestMessage) this.instance).hasVerb();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public String getVerb() {
                return ((WebSocketRequestMessage) this.instance).getVerb();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public ByteString getVerbBytes() {
                return ((WebSocketRequestMessage) this.instance).getVerbBytes();
            }

            public Builder setVerb(String str) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setVerb(str);
                return this;
            }

            public Builder clearVerb() {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).clearVerb();
                return this;
            }

            public Builder setVerbBytes(ByteString byteString) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setVerbBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public boolean hasPath() {
                return ((WebSocketRequestMessage) this.instance).hasPath();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public String getPath() {
                return ((WebSocketRequestMessage) this.instance).getPath();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public ByteString getPathBytes() {
                return ((WebSocketRequestMessage) this.instance).getPathBytes();
            }

            public Builder setPath(String str) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setPath(str);
                return this;
            }

            public Builder clearPath() {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).clearPath();
                return this;
            }

            public Builder setPathBytes(ByteString byteString) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setPathBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public boolean hasBody() {
                return ((WebSocketRequestMessage) this.instance).hasBody();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public ByteString getBody() {
                return ((WebSocketRequestMessage) this.instance).getBody();
            }

            public Builder setBody(ByteString byteString) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setBody(byteString);
                return this;
            }

            public Builder clearBody() {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).clearBody();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public List<String> getHeadersList() {
                return Collections.unmodifiableList(((WebSocketRequestMessage) this.instance).getHeadersList());
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public int getHeadersCount() {
                return ((WebSocketRequestMessage) this.instance).getHeadersCount();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public String getHeaders(int i) {
                return ((WebSocketRequestMessage) this.instance).getHeaders(i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public ByteString getHeadersBytes(int i) {
                return ((WebSocketRequestMessage) this.instance).getHeadersBytes(i);
            }

            public Builder setHeaders(int i, String str) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setHeaders(i, str);
                return this;
            }

            public Builder addHeaders(String str) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).addHeaders(str);
                return this;
            }

            public Builder addAllHeaders(Iterable<String> iterable) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).addAllHeaders(iterable);
                return this;
            }

            public Builder clearHeaders() {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).clearHeaders();
                return this;
            }

            public Builder addHeadersBytes(ByteString byteString) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).addHeadersBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public boolean hasId() {
                return ((WebSocketRequestMessage) this.instance).hasId();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketRequestMessageOrBuilder
            public long getId() {
                return ((WebSocketRequestMessage) this.instance).getId();
            }

            public Builder setId(long j) {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).setId(j);
                return this;
            }

            public Builder clearId() {
                copyOnWrite();
                ((WebSocketRequestMessage) this.instance).clearId();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new WebSocketRequestMessage();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ည\u0002\u0004ဃ\u0003\u0005\u001a", new Object[]{"bitField0_", "verb_", "path_", "body_", "id_", "headers_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<WebSocketRequestMessage> parser = PARSER;
                    if (parser == null) {
                        synchronized (WebSocketRequestMessage.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            WebSocketRequestMessage webSocketRequestMessage = new WebSocketRequestMessage();
            DEFAULT_INSTANCE = webSocketRequestMessage;
            GeneratedMessageLite.registerDefaultInstance(WebSocketRequestMessage.class, webSocketRequestMessage);
        }

        public static WebSocketRequestMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<WebSocketRequestMessage> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.websocket.WebSocketProtos$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes5.dex */
    public static final class WebSocketResponseMessage extends GeneratedMessageLite<WebSocketResponseMessage, Builder> implements WebSocketResponseMessageOrBuilder {
        public static final int BODY_FIELD_NUMBER;
        private static final WebSocketResponseMessage DEFAULT_INSTANCE;
        public static final int HEADERS_FIELD_NUMBER;
        public static final int ID_FIELD_NUMBER;
        public static final int MESSAGE_FIELD_NUMBER;
        private static volatile Parser<WebSocketResponseMessage> PARSER;
        public static final int STATUS_FIELD_NUMBER;
        private int bitField0_;
        private ByteString body_ = ByteString.EMPTY;
        private Internal.ProtobufList<String> headers_ = GeneratedMessageLite.emptyProtobufList();
        private long id_;
        private String message_ = "";
        private int status_;

        private WebSocketResponseMessage() {
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public boolean hasId() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public long getId() {
            return this.id_;
        }

        public void setId(long j) {
            this.bitField0_ |= 1;
            this.id_ = j;
        }

        public void clearId() {
            this.bitField0_ &= -2;
            this.id_ = 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public boolean hasStatus() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public int getStatus() {
            return this.status_;
        }

        public void setStatus(int i) {
            this.bitField0_ |= 2;
            this.status_ = i;
        }

        public void clearStatus() {
            this.bitField0_ &= -3;
            this.status_ = 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public boolean hasMessage() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public String getMessage() {
            return this.message_;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public ByteString getMessageBytes() {
            return ByteString.copyFromUtf8(this.message_);
        }

        public void setMessage(String str) {
            str.getClass();
            this.bitField0_ |= 4;
            this.message_ = str;
        }

        public void clearMessage() {
            this.bitField0_ &= -5;
            this.message_ = getDefaultInstance().getMessage();
        }

        public void setMessageBytes(ByteString byteString) {
            this.message_ = byteString.toStringUtf8();
            this.bitField0_ |= 4;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public List<String> getHeadersList() {
            return this.headers_;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public int getHeadersCount() {
            return this.headers_.size();
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public String getHeaders(int i) {
            return this.headers_.get(i);
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public ByteString getHeadersBytes(int i) {
            return ByteString.copyFromUtf8(this.headers_.get(i));
        }

        private void ensureHeadersIsMutable() {
            Internal.ProtobufList<String> protobufList = this.headers_;
            if (!protobufList.isModifiable()) {
                this.headers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setHeaders(int i, String str) {
            str.getClass();
            ensureHeadersIsMutable();
            this.headers_.set(i, str);
        }

        public void addHeaders(String str) {
            str.getClass();
            ensureHeadersIsMutable();
            this.headers_.add(str);
        }

        public void addAllHeaders(Iterable<String> iterable) {
            ensureHeadersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.headers_);
        }

        public void clearHeaders() {
            this.headers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void addHeadersBytes(ByteString byteString) {
            ensureHeadersIsMutable();
            this.headers_.add(byteString.toStringUtf8());
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public boolean hasBody() {
            return (this.bitField0_ & 8) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
        public ByteString getBody() {
            return this.body_;
        }

        public void setBody(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 8;
            this.body_ = byteString;
        }

        public void clearBody() {
            this.bitField0_ &= -9;
            this.body_ = getDefaultInstance().getBody();
        }

        public static WebSocketResponseMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static WebSocketResponseMessage parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static WebSocketResponseMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static WebSocketResponseMessage parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static WebSocketResponseMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static WebSocketResponseMessage parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static WebSocketResponseMessage parseFrom(InputStream inputStream) throws IOException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static WebSocketResponseMessage parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static WebSocketResponseMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static WebSocketResponseMessage parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static WebSocketResponseMessage parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static WebSocketResponseMessage parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketResponseMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(WebSocketResponseMessage webSocketResponseMessage) {
            return DEFAULT_INSTANCE.createBuilder(webSocketResponseMessage);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<WebSocketResponseMessage, Builder> implements WebSocketResponseMessageOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(WebSocketResponseMessage.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public boolean hasId() {
                return ((WebSocketResponseMessage) this.instance).hasId();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public long getId() {
                return ((WebSocketResponseMessage) this.instance).getId();
            }

            public Builder setId(long j) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).setId(j);
                return this;
            }

            public Builder clearId() {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).clearId();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public boolean hasStatus() {
                return ((WebSocketResponseMessage) this.instance).hasStatus();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public int getStatus() {
                return ((WebSocketResponseMessage) this.instance).getStatus();
            }

            public Builder setStatus(int i) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).setStatus(i);
                return this;
            }

            public Builder clearStatus() {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).clearStatus();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public boolean hasMessage() {
                return ((WebSocketResponseMessage) this.instance).hasMessage();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public String getMessage() {
                return ((WebSocketResponseMessage) this.instance).getMessage();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public ByteString getMessageBytes() {
                return ((WebSocketResponseMessage) this.instance).getMessageBytes();
            }

            public Builder setMessage(String str) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).setMessage(str);
                return this;
            }

            public Builder clearMessage() {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).clearMessage();
                return this;
            }

            public Builder setMessageBytes(ByteString byteString) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).setMessageBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public List<String> getHeadersList() {
                return Collections.unmodifiableList(((WebSocketResponseMessage) this.instance).getHeadersList());
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public int getHeadersCount() {
                return ((WebSocketResponseMessage) this.instance).getHeadersCount();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public String getHeaders(int i) {
                return ((WebSocketResponseMessage) this.instance).getHeaders(i);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public ByteString getHeadersBytes(int i) {
                return ((WebSocketResponseMessage) this.instance).getHeadersBytes(i);
            }

            public Builder setHeaders(int i, String str) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).setHeaders(i, str);
                return this;
            }

            public Builder addHeaders(String str) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).addHeaders(str);
                return this;
            }

            public Builder addAllHeaders(Iterable<String> iterable) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).addAllHeaders(iterable);
                return this;
            }

            public Builder clearHeaders() {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).clearHeaders();
                return this;
            }

            public Builder addHeadersBytes(ByteString byteString) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).addHeadersBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public boolean hasBody() {
                return ((WebSocketResponseMessage) this.instance).hasBody();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketResponseMessageOrBuilder
            public ByteString getBody() {
                return ((WebSocketResponseMessage) this.instance).getBody();
            }

            public Builder setBody(ByteString byteString) {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).setBody(byteString);
                return this;
            }

            public Builder clearBody() {
                copyOnWrite();
                ((WebSocketResponseMessage) this.instance).clearBody();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new WebSocketResponseMessage();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001ဃ\u0000\u0002ဋ\u0001\u0003ဈ\u0002\u0004ည\u0003\u0005\u001a", new Object[]{"bitField0_", "id_", "status_", "message_", "body_", "headers_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<WebSocketResponseMessage> parser = PARSER;
                    if (parser == null) {
                        synchronized (WebSocketResponseMessage.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            WebSocketResponseMessage webSocketResponseMessage = new WebSocketResponseMessage();
            DEFAULT_INSTANCE = webSocketResponseMessage;
            GeneratedMessageLite.registerDefaultInstance(WebSocketResponseMessage.class, webSocketResponseMessage);
        }

        public static WebSocketResponseMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<WebSocketResponseMessage> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes5.dex */
    public static final class WebSocketMessage extends GeneratedMessageLite<WebSocketMessage, Builder> implements WebSocketMessageOrBuilder {
        private static final WebSocketMessage DEFAULT_INSTANCE;
        private static volatile Parser<WebSocketMessage> PARSER;
        public static final int REQUEST_FIELD_NUMBER;
        public static final int RESPONSE_FIELD_NUMBER;
        public static final int TYPE_FIELD_NUMBER;
        private int bitField0_;
        private WebSocketRequestMessage request_;
        private WebSocketResponseMessage response_;
        private int type_;

        private WebSocketMessage() {
        }

        /* loaded from: classes5.dex */
        public enum Type implements Internal.EnumLite {
            UNKNOWN(0),
            REQUEST(1),
            RESPONSE(2);
            
            public static final int REQUEST_VALUE;
            public static final int RESPONSE_VALUE;
            public static final int UNKNOWN_VALUE;
            private static final Internal.EnumLiteMap<Type> internalValueMap = new Internal.EnumLiteMap<Type>() { // from class: org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessage.Type.1
                @Override // com.google.protobuf.Internal.EnumLiteMap
                public Type findValueByNumber(int i) {
                    return Type.forNumber(i);
                }
            };
            private final int value;

            @Override // com.google.protobuf.Internal.EnumLite
            public final int getNumber() {
                return this.value;
            }

            @Deprecated
            public static Type valueOf(int i) {
                return forNumber(i);
            }

            public static Type forNumber(int i) {
                if (i == 0) {
                    return UNKNOWN;
                }
                if (i == 1) {
                    return REQUEST;
                }
                if (i != 2) {
                    return null;
                }
                return RESPONSE;
            }

            public static Internal.EnumLiteMap<Type> internalGetValueMap() {
                return internalValueMap;
            }

            public static Internal.EnumVerifier internalGetVerifier() {
                return TypeVerifier.INSTANCE;
            }

            /* loaded from: classes5.dex */
            public static final class TypeVerifier implements Internal.EnumVerifier {
                static final Internal.EnumVerifier INSTANCE = new TypeVerifier();

                private TypeVerifier() {
                }

                @Override // com.google.protobuf.Internal.EnumVerifier
                public boolean isInRange(int i) {
                    return Type.forNumber(i) != null;
                }
            }

            Type(int i) {
                this.value = i;
            }
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
        public boolean hasType() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
        public Type getType() {
            Type forNumber = Type.forNumber(this.type_);
            return forNumber == null ? Type.UNKNOWN : forNumber;
        }

        public void setType(Type type) {
            this.type_ = type.getNumber();
            this.bitField0_ |= 1;
        }

        public void clearType() {
            this.bitField0_ &= -2;
            this.type_ = 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
        public boolean hasRequest() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
        public WebSocketRequestMessage getRequest() {
            WebSocketRequestMessage webSocketRequestMessage = this.request_;
            return webSocketRequestMessage == null ? WebSocketRequestMessage.getDefaultInstance() : webSocketRequestMessage;
        }

        public void setRequest(WebSocketRequestMessage webSocketRequestMessage) {
            webSocketRequestMessage.getClass();
            this.request_ = webSocketRequestMessage;
            this.bitField0_ |= 2;
        }

        public void mergeRequest(WebSocketRequestMessage webSocketRequestMessage) {
            webSocketRequestMessage.getClass();
            WebSocketRequestMessage webSocketRequestMessage2 = this.request_;
            if (webSocketRequestMessage2 == null || webSocketRequestMessage2 == WebSocketRequestMessage.getDefaultInstance()) {
                this.request_ = webSocketRequestMessage;
            } else {
                this.request_ = WebSocketRequestMessage.newBuilder(this.request_).mergeFrom((WebSocketRequestMessage.Builder) webSocketRequestMessage).buildPartial();
            }
            this.bitField0_ |= 2;
        }

        public void clearRequest() {
            this.request_ = null;
            this.bitField0_ &= -3;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
        public boolean hasResponse() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
        public WebSocketResponseMessage getResponse() {
            WebSocketResponseMessage webSocketResponseMessage = this.response_;
            return webSocketResponseMessage == null ? WebSocketResponseMessage.getDefaultInstance() : webSocketResponseMessage;
        }

        public void setResponse(WebSocketResponseMessage webSocketResponseMessage) {
            webSocketResponseMessage.getClass();
            this.response_ = webSocketResponseMessage;
            this.bitField0_ |= 4;
        }

        public void mergeResponse(WebSocketResponseMessage webSocketResponseMessage) {
            webSocketResponseMessage.getClass();
            WebSocketResponseMessage webSocketResponseMessage2 = this.response_;
            if (webSocketResponseMessage2 == null || webSocketResponseMessage2 == WebSocketResponseMessage.getDefaultInstance()) {
                this.response_ = webSocketResponseMessage;
            } else {
                this.response_ = WebSocketResponseMessage.newBuilder(this.response_).mergeFrom((WebSocketResponseMessage.Builder) webSocketResponseMessage).buildPartial();
            }
            this.bitField0_ |= 4;
        }

        public void clearResponse() {
            this.response_ = null;
            this.bitField0_ &= -5;
        }

        public static WebSocketMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static WebSocketMessage parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static WebSocketMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static WebSocketMessage parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static WebSocketMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static WebSocketMessage parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static WebSocketMessage parseFrom(InputStream inputStream) throws IOException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static WebSocketMessage parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static WebSocketMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (WebSocketMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static WebSocketMessage parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static WebSocketMessage parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static WebSocketMessage parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (WebSocketMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(WebSocketMessage webSocketMessage) {
            return DEFAULT_INSTANCE.createBuilder(webSocketMessage);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<WebSocketMessage, Builder> implements WebSocketMessageOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(WebSocketMessage.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
            public boolean hasType() {
                return ((WebSocketMessage) this.instance).hasType();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
            public Type getType() {
                return ((WebSocketMessage) this.instance).getType();
            }

            public Builder setType(Type type) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).setType(type);
                return this;
            }

            public Builder clearType() {
                copyOnWrite();
                ((WebSocketMessage) this.instance).clearType();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
            public boolean hasRequest() {
                return ((WebSocketMessage) this.instance).hasRequest();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
            public WebSocketRequestMessage getRequest() {
                return ((WebSocketMessage) this.instance).getRequest();
            }

            public Builder setRequest(WebSocketRequestMessage webSocketRequestMessage) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).setRequest(webSocketRequestMessage);
                return this;
            }

            public Builder setRequest(WebSocketRequestMessage.Builder builder) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).setRequest(builder.build());
                return this;
            }

            public Builder mergeRequest(WebSocketRequestMessage webSocketRequestMessage) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).mergeRequest(webSocketRequestMessage);
                return this;
            }

            public Builder clearRequest() {
                copyOnWrite();
                ((WebSocketMessage) this.instance).clearRequest();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
            public boolean hasResponse() {
                return ((WebSocketMessage) this.instance).hasResponse();
            }

            @Override // org.whispersystems.signalservice.internal.websocket.WebSocketProtos.WebSocketMessageOrBuilder
            public WebSocketResponseMessage getResponse() {
                return ((WebSocketMessage) this.instance).getResponse();
            }

            public Builder setResponse(WebSocketResponseMessage webSocketResponseMessage) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).setResponse(webSocketResponseMessage);
                return this;
            }

            public Builder setResponse(WebSocketResponseMessage.Builder builder) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).setResponse(builder.build());
                return this;
            }

            public Builder mergeResponse(WebSocketResponseMessage webSocketResponseMessage) {
                copyOnWrite();
                ((WebSocketMessage) this.instance).mergeResponse(webSocketResponseMessage);
                return this;
            }

            public Builder clearResponse() {
                copyOnWrite();
                ((WebSocketMessage) this.instance).clearResponse();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new WebSocketMessage();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"bitField0_", "type_", Type.internalGetVerifier(), "request_", "response_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<WebSocketMessage> parser = PARSER;
                    if (parser == null) {
                        synchronized (WebSocketMessage.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            WebSocketMessage webSocketMessage = new WebSocketMessage();
            DEFAULT_INSTANCE = webSocketMessage;
            GeneratedMessageLite.registerDefaultInstance(WebSocketMessage.class, webSocketMessage);
        }

        public static WebSocketMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<WebSocketMessage> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }
}
