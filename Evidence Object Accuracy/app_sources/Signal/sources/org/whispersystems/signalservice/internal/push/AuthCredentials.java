package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import okhttp3.Credentials;

/* loaded from: classes.dex */
public class AuthCredentials {
    @JsonProperty
    private String password;
    @JsonProperty
    private String username;

    public static AuthCredentials create(String str, String str2) {
        AuthCredentials authCredentials = new AuthCredentials();
        authCredentials.username = str;
        authCredentials.password = str2;
        return authCredentials;
    }

    public String asBasic() {
        return Credentials.basic(this.username, this.password);
    }

    public String username() {
        return this.username;
    }

    public String password() {
        return this.password;
    }
}
