package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class GroupV2Record extends GeneratedMessageLite<GroupV2Record, Builder> implements GroupV2RecordOrBuilder {
    public static final int ARCHIVED_FIELD_NUMBER;
    public static final int BLOCKED_FIELD_NUMBER;
    private static final GroupV2Record DEFAULT_INSTANCE;
    public static final int DONTNOTIFYFORMENTIONSIFMUTED_FIELD_NUMBER;
    public static final int HIDESTORY_FIELD_NUMBER;
    public static final int MARKEDUNREAD_FIELD_NUMBER;
    public static final int MASTERKEY_FIELD_NUMBER;
    public static final int MUTEDUNTILTIMESTAMP_FIELD_NUMBER;
    private static volatile Parser<GroupV2Record> PARSER;
    public static final int WHITELISTED_FIELD_NUMBER;
    private boolean archived_;
    private boolean blocked_;
    private boolean dontNotifyForMentionsIfMuted_;
    private boolean hideStory_;
    private boolean markedUnread_;
    private ByteString masterKey_ = ByteString.EMPTY;
    private long mutedUntilTimestamp_;
    private boolean whitelisted_;

    private GroupV2Record() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public ByteString getMasterKey() {
        return this.masterKey_;
    }

    public void setMasterKey(ByteString byteString) {
        byteString.getClass();
        this.masterKey_ = byteString;
    }

    public void clearMasterKey() {
        this.masterKey_ = getDefaultInstance().getMasterKey();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public boolean getBlocked() {
        return this.blocked_;
    }

    public void setBlocked(boolean z) {
        this.blocked_ = z;
    }

    public void clearBlocked() {
        this.blocked_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public boolean getWhitelisted() {
        return this.whitelisted_;
    }

    public void setWhitelisted(boolean z) {
        this.whitelisted_ = z;
    }

    public void clearWhitelisted() {
        this.whitelisted_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public boolean getArchived() {
        return this.archived_;
    }

    public void setArchived(boolean z) {
        this.archived_ = z;
    }

    public void clearArchived() {
        this.archived_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public boolean getMarkedUnread() {
        return this.markedUnread_;
    }

    public void setMarkedUnread(boolean z) {
        this.markedUnread_ = z;
    }

    public void clearMarkedUnread() {
        this.markedUnread_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public long getMutedUntilTimestamp() {
        return this.mutedUntilTimestamp_;
    }

    public void setMutedUntilTimestamp(long j) {
        this.mutedUntilTimestamp_ = j;
    }

    public void clearMutedUntilTimestamp() {
        this.mutedUntilTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public boolean getDontNotifyForMentionsIfMuted() {
        return this.dontNotifyForMentionsIfMuted_;
    }

    public void setDontNotifyForMentionsIfMuted(boolean z) {
        this.dontNotifyForMentionsIfMuted_ = z;
    }

    public void clearDontNotifyForMentionsIfMuted() {
        this.dontNotifyForMentionsIfMuted_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
    public boolean getHideStory() {
        return this.hideStory_;
    }

    public void setHideStory(boolean z) {
        this.hideStory_ = z;
    }

    public void clearHideStory() {
        this.hideStory_ = false;
    }

    public static GroupV2Record parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GroupV2Record parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GroupV2Record parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GroupV2Record parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GroupV2Record parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GroupV2Record parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GroupV2Record parseFrom(InputStream inputStream) throws IOException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupV2Record parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupV2Record parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GroupV2Record) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupV2Record parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupV2Record) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupV2Record parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GroupV2Record parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupV2Record) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GroupV2Record groupV2Record) {
        return DEFAULT_INSTANCE.createBuilder(groupV2Record);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GroupV2Record, Builder> implements GroupV2RecordOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GroupV2Record.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public ByteString getMasterKey() {
            return ((GroupV2Record) this.instance).getMasterKey();
        }

        public Builder setMasterKey(ByteString byteString) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setMasterKey(byteString);
            return this;
        }

        public Builder clearMasterKey() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearMasterKey();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public boolean getBlocked() {
            return ((GroupV2Record) this.instance).getBlocked();
        }

        public Builder setBlocked(boolean z) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setBlocked(z);
            return this;
        }

        public Builder clearBlocked() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearBlocked();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public boolean getWhitelisted() {
            return ((GroupV2Record) this.instance).getWhitelisted();
        }

        public Builder setWhitelisted(boolean z) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setWhitelisted(z);
            return this;
        }

        public Builder clearWhitelisted() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearWhitelisted();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public boolean getArchived() {
            return ((GroupV2Record) this.instance).getArchived();
        }

        public Builder setArchived(boolean z) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setArchived(z);
            return this;
        }

        public Builder clearArchived() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearArchived();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public boolean getMarkedUnread() {
            return ((GroupV2Record) this.instance).getMarkedUnread();
        }

        public Builder setMarkedUnread(boolean z) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setMarkedUnread(z);
            return this;
        }

        public Builder clearMarkedUnread() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearMarkedUnread();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public long getMutedUntilTimestamp() {
            return ((GroupV2Record) this.instance).getMutedUntilTimestamp();
        }

        public Builder setMutedUntilTimestamp(long j) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setMutedUntilTimestamp(j);
            return this;
        }

        public Builder clearMutedUntilTimestamp() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearMutedUntilTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public boolean getDontNotifyForMentionsIfMuted() {
            return ((GroupV2Record) this.instance).getDontNotifyForMentionsIfMuted();
        }

        public Builder setDontNotifyForMentionsIfMuted(boolean z) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setDontNotifyForMentionsIfMuted(z);
            return this;
        }

        public Builder clearDontNotifyForMentionsIfMuted() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearDontNotifyForMentionsIfMuted();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.GroupV2RecordOrBuilder
        public boolean getHideStory() {
            return ((GroupV2Record) this.instance).getHideStory();
        }

        public Builder setHideStory(boolean z) {
            copyOnWrite();
            ((GroupV2Record) this.instance).setHideStory(z);
            return this;
        }

        public Builder clearHideStory() {
            copyOnWrite();
            ((GroupV2Record) this.instance).clearHideStory();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.GroupV2Record$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GroupV2Record();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\b\u0000\u0000\u0001\b\b\u0000\u0000\u0000\u0001\n\u0002\u0007\u0003\u0007\u0004\u0007\u0005\u0007\u0006\u0003\u0007\u0007\b\u0007", new Object[]{"masterKey_", "blocked_", "whitelisted_", "archived_", "markedUnread_", "mutedUntilTimestamp_", "dontNotifyForMentionsIfMuted_", "hideStory_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GroupV2Record> parser = PARSER;
                if (parser == null) {
                    synchronized (GroupV2Record.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GroupV2Record groupV2Record = new GroupV2Record();
        DEFAULT_INSTANCE = groupV2Record;
        GeneratedMessageLite.registerDefaultInstance(GroupV2Record.class, groupV2Record);
    }

    public static GroupV2Record getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GroupV2Record> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
