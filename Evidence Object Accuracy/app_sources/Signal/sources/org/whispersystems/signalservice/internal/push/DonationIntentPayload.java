package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class DonationIntentPayload {
    @JsonProperty
    private long amount;
    @JsonProperty
    private String currency;
    @JsonProperty
    private long level;

    public DonationIntentPayload(long j, String str, long j2) {
        this.amount = j;
        this.currency = str;
        this.level = j2;
    }
}
