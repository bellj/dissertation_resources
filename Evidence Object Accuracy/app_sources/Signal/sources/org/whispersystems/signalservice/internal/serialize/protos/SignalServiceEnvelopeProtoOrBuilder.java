package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface SignalServiceEnvelopeProtoOrBuilder extends MessageLiteOrBuilder {
    ByteString getContent();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    String getDestinationUuid();

    ByteString getDestinationUuidBytes();

    int getDeviceId();

    ByteString getLegacyMessage();

    long getServerDeliveredTimestamp();

    String getServerGuid();

    ByteString getServerGuidBytes();

    long getServerReceivedTimestamp();

    String getSourceE164();

    ByteString getSourceE164Bytes();

    String getSourceUuid();

    ByteString getSourceUuidBytes();

    long getTimestamp();

    int getType();

    boolean hasContent();

    boolean hasDestinationUuid();

    boolean hasDeviceId();

    boolean hasLegacyMessage();

    boolean hasServerDeliveredTimestamp();

    boolean hasServerGuid();

    boolean hasServerReceivedTimestamp();

    boolean hasSourceE164();

    boolean hasSourceUuid();

    boolean hasTimestamp();

    boolean hasType();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
