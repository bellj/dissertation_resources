package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class RedeemReceiptRequest {
    private final boolean primary;
    private final String receiptCredentialPresentation;
    private final boolean visible;

    @JsonCreator
    public RedeemReceiptRequest(@JsonProperty("receiptCredentialPresentation") String str, @JsonProperty("visible") boolean z, @JsonProperty("primary") boolean z2) {
        this.receiptCredentialPresentation = str;
        this.visible = z;
        this.primary = z2;
    }

    public String getReceiptCredentialPresentation() {
        return this.receiptCredentialPresentation;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public boolean isPrimary() {
        return this.primary;
    }
}
