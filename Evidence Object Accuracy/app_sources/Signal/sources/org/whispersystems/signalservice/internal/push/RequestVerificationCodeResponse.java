package org.whispersystems.signalservice.internal.push;

import j$.util.Optional;

/* loaded from: classes5.dex */
public final class RequestVerificationCodeResponse {
    private final Optional<String> fcmToken;

    public RequestVerificationCodeResponse(Optional<String> optional) {
        this.fcmToken = optional;
    }

    public Optional<String> getFcmToken() {
        return this.fcmToken;
    }
}
