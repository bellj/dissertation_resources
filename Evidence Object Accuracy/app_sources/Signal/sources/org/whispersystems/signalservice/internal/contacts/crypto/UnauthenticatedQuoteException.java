package org.whispersystems.signalservice.internal.contacts.crypto;

/* loaded from: classes5.dex */
public class UnauthenticatedQuoteException extends Exception {
    public UnauthenticatedQuoteException(String str) {
        super(str);
    }

    public UnauthenticatedQuoteException(Exception exc) {
        super(exc);
    }
}
