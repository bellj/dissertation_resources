package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class RestoreResponse extends GeneratedMessageLite<RestoreResponse, Builder> implements RestoreResponseOrBuilder {
    public static final int DATA_FIELD_NUMBER;
    private static final RestoreResponse DEFAULT_INSTANCE;
    private static volatile Parser<RestoreResponse> PARSER;
    public static final int STATUS_FIELD_NUMBER;
    public static final int TOKEN_FIELD_NUMBER;
    public static final int TRIES_FIELD_NUMBER;
    private int bitField0_;
    private ByteString data_;
    private int status_ = 1;
    private ByteString token_;
    private int tries_;

    private RestoreResponse() {
        ByteString byteString = ByteString.EMPTY;
        this.token_ = byteString;
        this.data_ = byteString;
    }

    /* loaded from: classes5.dex */
    public enum Status implements Internal.EnumLite {
        OK(1),
        TOKEN_MISMATCH(2),
        NOT_YET_VALID(3),
        MISSING(4),
        PIN_MISMATCH(5);
        
        public static final int MISSING_VALUE;
        public static final int NOT_YET_VALID_VALUE;
        public static final int OK_VALUE;
        public static final int PIN_MISMATCH_VALUE;
        public static final int TOKEN_MISMATCH_VALUE;
        private static final Internal.EnumLiteMap<Status> internalValueMap = new Internal.EnumLiteMap<Status>() { // from class: org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponse.Status.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public Status findValueByNumber(int i) {
                return Status.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            return this.value;
        }

        @Deprecated
        public static Status valueOf(int i) {
            return forNumber(i);
        }

        public static Status forNumber(int i) {
            if (i == 1) {
                return OK;
            }
            if (i == 2) {
                return TOKEN_MISMATCH;
            }
            if (i == 3) {
                return NOT_YET_VALID;
            }
            if (i == 4) {
                return MISSING;
            }
            if (i != 5) {
                return null;
            }
            return PIN_MISMATCH;
        }

        public static Internal.EnumLiteMap<Status> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return StatusVerifier.INSTANCE;
        }

        /* loaded from: classes5.dex */
        public static final class StatusVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new StatusVerifier();

            private StatusVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return Status.forNumber(i) != null;
            }
        }

        Status(int i) {
            this.value = i;
        }
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public boolean hasStatus() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public Status getStatus() {
        Status forNumber = Status.forNumber(this.status_);
        return forNumber == null ? Status.OK : forNumber;
    }

    public void setStatus(Status status) {
        this.status_ = status.getNumber();
        this.bitField0_ |= 1;
    }

    public void clearStatus() {
        this.bitField0_ &= -2;
        this.status_ = 1;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public boolean hasToken() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public ByteString getToken() {
        return this.token_;
    }

    public void setToken(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 2;
        this.token_ = byteString;
    }

    public void clearToken() {
        this.bitField0_ &= -3;
        this.token_ = getDefaultInstance().getToken();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public boolean hasData() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public ByteString getData() {
        return this.data_;
    }

    public void setData(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 4;
        this.data_ = byteString;
    }

    public void clearData() {
        this.bitField0_ &= -5;
        this.data_ = getDefaultInstance().getData();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public boolean hasTries() {
        return (this.bitField0_ & 8) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
    public int getTries() {
        return this.tries_;
    }

    public void setTries(int i) {
        this.bitField0_ |= 8;
        this.tries_ = i;
    }

    public void clearTries() {
        this.bitField0_ &= -9;
        this.tries_ = 0;
    }

    public static RestoreResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static RestoreResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static RestoreResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static RestoreResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static RestoreResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static RestoreResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static RestoreResponse parseFrom(InputStream inputStream) throws IOException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RestoreResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RestoreResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (RestoreResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RestoreResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RestoreResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RestoreResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static RestoreResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RestoreResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(RestoreResponse restoreResponse) {
        return DEFAULT_INSTANCE.createBuilder(restoreResponse);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<RestoreResponse, Builder> implements RestoreResponseOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(RestoreResponse.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public boolean hasStatus() {
            return ((RestoreResponse) this.instance).hasStatus();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public Status getStatus() {
            return ((RestoreResponse) this.instance).getStatus();
        }

        public Builder setStatus(Status status) {
            copyOnWrite();
            ((RestoreResponse) this.instance).setStatus(status);
            return this;
        }

        public Builder clearStatus() {
            copyOnWrite();
            ((RestoreResponse) this.instance).clearStatus();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public boolean hasToken() {
            return ((RestoreResponse) this.instance).hasToken();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public ByteString getToken() {
            return ((RestoreResponse) this.instance).getToken();
        }

        public Builder setToken(ByteString byteString) {
            copyOnWrite();
            ((RestoreResponse) this.instance).setToken(byteString);
            return this;
        }

        public Builder clearToken() {
            copyOnWrite();
            ((RestoreResponse) this.instance).clearToken();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public boolean hasData() {
            return ((RestoreResponse) this.instance).hasData();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public ByteString getData() {
            return ((RestoreResponse) this.instance).getData();
        }

        public Builder setData(ByteString byteString) {
            copyOnWrite();
            ((RestoreResponse) this.instance).setData(byteString);
            return this;
        }

        public Builder clearData() {
            copyOnWrite();
            ((RestoreResponse) this.instance).clearData();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public boolean hasTries() {
            return ((RestoreResponse) this.instance).hasTries();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponseOrBuilder
        public int getTries() {
            return ((RestoreResponse) this.instance).getTries();
        }

        public Builder setTries(int i) {
            copyOnWrite();
            ((RestoreResponse) this.instance).setTries(i);
            return this;
        }

        public Builder clearTries() {
            copyOnWrite();
            ((RestoreResponse) this.instance).clearTries();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponse$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new RestoreResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဌ\u0000\u0002ည\u0001\u0003ည\u0002\u0004ဋ\u0003", new Object[]{"bitField0_", "status_", Status.internalGetVerifier(), "token_", "data_", "tries_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<RestoreResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (RestoreResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        RestoreResponse restoreResponse = new RestoreResponse();
        DEFAULT_INSTANCE = restoreResponse;
        GeneratedMessageLite.registerDefaultInstance(RestoreResponse.class, restoreResponse);
    }

    public static RestoreResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<RestoreResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
