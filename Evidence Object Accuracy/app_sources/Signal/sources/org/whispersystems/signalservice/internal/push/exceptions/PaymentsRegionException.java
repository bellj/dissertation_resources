package org.whispersystems.signalservice.internal.push.exceptions;

import okhttp3.ResponseBody;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes5.dex */
public final class PaymentsRegionException extends NonSuccessfulResponseCodeException {
    public PaymentsRegionException(int i) {
        super(i);
    }

    public static void responseCodeHandler(int i, ResponseBody responseBody) throws PaymentsRegionException {
        if (i == 403) {
            throw new PaymentsRegionException(i);
        }
    }
}
