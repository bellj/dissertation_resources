package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/* loaded from: classes.dex */
public class DiscoveryRequest {
    @JsonProperty
    private int addressCount;
    @JsonProperty
    private byte[] commitment;
    @JsonProperty
    private byte[] data;
    @JsonProperty
    private Map<String, QueryEnvelope> envelopes;
    @JsonProperty
    private byte[] iv;
    @JsonProperty
    private byte[] mac;

    public DiscoveryRequest() {
    }

    public DiscoveryRequest(int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, Map<String, QueryEnvelope> map) {
        this.addressCount = i;
        this.commitment = bArr;
        this.iv = bArr2;
        this.data = bArr3;
        this.mac = bArr4;
        this.envelopes = map;
    }

    public byte[] getCommitment() {
        return this.commitment;
    }

    public byte[] getIv() {
        return this.iv;
    }

    public byte[] getData() {
        return this.data;
    }

    public byte[] getMac() {
        return this.mac;
    }

    public int getAddressCount() {
        return this.addressCount;
    }

    public String toString() {
        return "{ addressCount: " + this.addressCount + ", envelopes: " + this.envelopes.size() + " }";
    }
}
