package org.whispersystems.signalservice.internal.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.util.UUID;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public class JsonUtil {
    private static final String TAG;
    private static final ObjectMapper objectMapper;

    static {
        ObjectMapper objectMapper2 = new ObjectMapper();
        objectMapper = objectMapper2;
        objectMapper2.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static String toJson(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            Log.w(TAG, e);
            return "";
        }
    }

    public static ByteString toJsonByteString(Object obj) {
        return ByteString.copyFrom(toJson(obj).getBytes());
    }

    public static <T> T fromJson(String str, Class<T> cls) throws IOException {
        return (T) objectMapper.readValue(str, cls);
    }

    public static <T> T fromJson(String str, TypeReference<T> typeReference) throws IOException {
        return (T) objectMapper.readValue(str, typeReference);
    }

    public static <T> T fromJsonResponse(String str, TypeReference<T> typeReference) throws MalformedResponseException {
        try {
            return (T) fromJson(str, typeReference);
        } catch (IOException e) {
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    public static <T> T fromJsonResponse(String str, Class<T> cls) throws MalformedResponseException {
        try {
            return (T) fromJson(str, cls);
        } catch (IOException e) {
            throw new MalformedResponseException("Unable to parse entity", e);
        }
    }

    /* loaded from: classes5.dex */
    public static class IdentityKeySerializer extends JsonSerializer<IdentityKey> {
        public void serialize(IdentityKey identityKey, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Base64.encodeBytesWithoutPadding(identityKey.serialize()));
        }
    }

    /* loaded from: classes5.dex */
    public static class IdentityKeyDeserializer extends JsonDeserializer<IdentityKey> {
        public IdentityKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            try {
                return new IdentityKey(Base64.decodeWithoutPadding(jsonParser.getValueAsString()), 0);
            } catch (InvalidKeyException e) {
                throw new IOException(e);
            }
        }
    }

    /* loaded from: classes5.dex */
    public static class UuidSerializer extends JsonSerializer<UUID> {
        public void serialize(UUID uuid, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(uuid.toString());
        }
    }

    /* loaded from: classes5.dex */
    public static class UuidDeserializer extends JsonDeserializer<UUID> {
        public UUID deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return UuidUtil.parseOrNull(jsonParser.getValueAsString());
        }
    }

    /* loaded from: classes5.dex */
    public static class AciSerializer extends JsonSerializer<ACI> {
        public void serialize(ACI aci, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(aci.toString());
        }
    }

    /* loaded from: classes5.dex */
    public static class AciDeserializer extends JsonDeserializer<ACI> {
        public ACI deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return ACI.parseOrNull(jsonParser.getValueAsString());
        }
    }

    /* loaded from: classes5.dex */
    public static class ServiceIdSerializer extends JsonSerializer<ServiceId> {
        public void serialize(ServiceId serviceId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(serviceId.toString());
        }
    }

    /* loaded from: classes5.dex */
    public static class ServiceIdDeserializer extends JsonDeserializer<ServiceId> {
        public ServiceId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return ServiceId.parseOrNull(jsonParser.getValueAsString());
        }
    }
}
