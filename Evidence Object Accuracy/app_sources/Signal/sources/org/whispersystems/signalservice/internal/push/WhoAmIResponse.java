package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class WhoAmIResponse {
    @JsonProperty
    private String number;
    @JsonProperty
    private String pni;
    @JsonProperty
    private String uuid;

    public String getAci() {
        return this.uuid;
    }

    public String getPni() {
        return this.pni;
    }

    public String getNumber() {
        return this.number;
    }
}
