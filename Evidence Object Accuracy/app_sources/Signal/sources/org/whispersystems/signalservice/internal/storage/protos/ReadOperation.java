package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes5.dex */
public final class ReadOperation extends GeneratedMessageLite<ReadOperation, Builder> implements ReadOperationOrBuilder {
    private static final ReadOperation DEFAULT_INSTANCE;
    private static volatile Parser<ReadOperation> PARSER;
    public static final int READKEY_FIELD_NUMBER;
    private Internal.ProtobufList<ByteString> readKey_ = GeneratedMessageLite.emptyProtobufList();

    private ReadOperation() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ReadOperationOrBuilder
    public List<ByteString> getReadKeyList() {
        return this.readKey_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ReadOperationOrBuilder
    public int getReadKeyCount() {
        return this.readKey_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ReadOperationOrBuilder
    public ByteString getReadKey(int i) {
        return this.readKey_.get(i);
    }

    private void ensureReadKeyIsMutable() {
        Internal.ProtobufList<ByteString> protobufList = this.readKey_;
        if (!protobufList.isModifiable()) {
            this.readKey_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setReadKey(int i, ByteString byteString) {
        byteString.getClass();
        ensureReadKeyIsMutable();
        this.readKey_.set(i, byteString);
    }

    public void addReadKey(ByteString byteString) {
        byteString.getClass();
        ensureReadKeyIsMutable();
        this.readKey_.add(byteString);
    }

    public void addAllReadKey(Iterable<? extends ByteString> iterable) {
        ensureReadKeyIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.readKey_);
    }

    public void clearReadKey() {
        this.readKey_ = GeneratedMessageLite.emptyProtobufList();
    }

    public static ReadOperation parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ReadOperation parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ReadOperation parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ReadOperation parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ReadOperation parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ReadOperation parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ReadOperation parseFrom(InputStream inputStream) throws IOException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReadOperation parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReadOperation parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ReadOperation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReadOperation parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReadOperation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReadOperation parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ReadOperation parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReadOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ReadOperation readOperation) {
        return DEFAULT_INSTANCE.createBuilder(readOperation);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ReadOperation, Builder> implements ReadOperationOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ReadOperation.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ReadOperationOrBuilder
        public List<ByteString> getReadKeyList() {
            return Collections.unmodifiableList(((ReadOperation) this.instance).getReadKeyList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ReadOperationOrBuilder
        public int getReadKeyCount() {
            return ((ReadOperation) this.instance).getReadKeyCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ReadOperationOrBuilder
        public ByteString getReadKey(int i) {
            return ((ReadOperation) this.instance).getReadKey(i);
        }

        public Builder setReadKey(int i, ByteString byteString) {
            copyOnWrite();
            ((ReadOperation) this.instance).setReadKey(i, byteString);
            return this;
        }

        public Builder addReadKey(ByteString byteString) {
            copyOnWrite();
            ((ReadOperation) this.instance).addReadKey(byteString);
            return this;
        }

        public Builder addAllReadKey(Iterable<? extends ByteString> iterable) {
            copyOnWrite();
            ((ReadOperation) this.instance).addAllReadKey(iterable);
            return this;
        }

        public Builder clearReadKey() {
            copyOnWrite();
            ((ReadOperation) this.instance).clearReadKey();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.ReadOperation$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ReadOperation();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001c", new Object[]{"readKey_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ReadOperation> parser = PARSER;
                if (parser == null) {
                    synchronized (ReadOperation.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ReadOperation readOperation = new ReadOperation();
        DEFAULT_INSTANCE = readOperation;
        GeneratedMessageLite.registerDefaultInstance(ReadOperation.class, readOperation);
    }

    public static ReadOperation getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ReadOperation> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
