package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface GroupV1RecordOrBuilder extends MessageLiteOrBuilder {
    boolean getArchived();

    boolean getBlocked();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ByteString getId();

    boolean getMarkedUnread();

    long getMutedUntilTimestamp();

    boolean getWhitelisted();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
