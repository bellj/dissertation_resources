package org.whispersystems.signalservice.internal.contacts.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public final class AESCipher {
    private static final int TAG_LENGTH_BITS;
    private static final int TAG_LENGTH_BYTES;

    AESCipher() {
    }

    public static byte[] decrypt(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) throws InvalidCiphertextException {
        Object e;
        Exception e2;
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(2, new SecretKeySpec(bArr, "AES"), new GCMParameterSpec(128, bArr2));
            return instance.doFinal(ByteUtil.combine(bArr3, bArr4));
        } catch (InvalidAlgorithmParameterException e3) {
            e = e3;
            throw new AssertionError(e);
        } catch (InvalidKeyException e4) {
            e2 = e4;
            throw new InvalidCiphertextException(e2);
        } catch (NoSuchAlgorithmException e5) {
            e = e5;
            throw new AssertionError(e);
        } catch (BadPaddingException e6) {
            e2 = e6;
            throw new InvalidCiphertextException(e2);
        } catch (IllegalBlockSizeException e7) {
            e = e7;
            throw new AssertionError(e);
        } catch (NoSuchPaddingException e8) {
            e = e8;
            throw new AssertionError(e);
        }
    }

    public static AESEncryptedResult encrypt(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        try {
            byte[] secretBytes = Util.getSecretBytes(12);
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(1, new SecretKeySpec(bArr, "AES"), new GCMParameterSpec(128, secretBytes));
            if (bArr2 != null) {
                instance.updateAAD(bArr2);
            }
            byte[] doFinal = instance.doFinal(bArr3);
            byte[][] split = ByteUtil.split(doFinal, doFinal.length - 16, 16);
            return new AESEncryptedResult(secretBytes, split[0], split[1], bArr2);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    /* loaded from: classes5.dex */
    public static class AESEncryptedResult {
        final byte[] aad;
        final byte[] data;
        final byte[] iv;
        final byte[] mac;

        private AESEncryptedResult(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
            this.iv = bArr;
            this.data = bArr2;
            this.mac = bArr3;
            this.aad = bArr4;
        }
    }
}
