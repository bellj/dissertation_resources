package org.whispersystems.signalservice.internal.push;

import java.io.IOException;
import java.security.KeyStore;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.internal.contacts.crypto.Quote;
import org.whispersystems.signalservice.internal.contacts.crypto.RemoteAttestation;
import org.whispersystems.signalservice.internal.contacts.crypto.RemoteAttestationCipher;
import org.whispersystems.signalservice.internal.contacts.crypto.RemoteAttestationKeys;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedQuoteException;
import org.whispersystems.signalservice.internal.contacts.entities.MultiRemoteAttestationResponse;
import org.whispersystems.signalservice.internal.contacts.entities.RemoteAttestationRequest;
import org.whispersystems.signalservice.internal.contacts.entities.RemoteAttestationResponse;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes5.dex */
public final class RemoteAttestationUtil {
    private RemoteAttestationUtil() {
    }

    public static RemoteAttestation getAndVerifyRemoteAttestation(PushServiceSocket pushServiceSocket, PushServiceSocket.ClientSet clientSet, KeyStore keyStore, String str, String str2, String str3) throws IOException, Quote.InvalidQuoteFormatException, InvalidCiphertextException, UnauthenticatedQuoteException, SignatureException, InvalidKeyException {
        ECKeyPair buildKeyPair = buildKeyPair();
        ResponsePair makeAttestationRequest = makeAttestationRequest(pushServiceSocket, clientSet, str3, str, buildKeyPair);
        return validateAndBuildRemoteAttestation((RemoteAttestationResponse) JsonUtil.fromJson(makeAttestationRequest.body, RemoteAttestationResponse.class), makeAttestationRequest.cookies, keyStore, buildKeyPair, str2);
    }

    public static Map<String, RemoteAttestation> getAndVerifyMultiRemoteAttestation(PushServiceSocket pushServiceSocket, PushServiceSocket.ClientSet clientSet, KeyStore keyStore, String str, String str2, String str3) throws IOException, Quote.InvalidQuoteFormatException, InvalidCiphertextException, UnauthenticatedQuoteException, SignatureException, InvalidKeyException {
        ECKeyPair buildKeyPair = buildKeyPair();
        ResponsePair makeAttestationRequest = makeAttestationRequest(pushServiceSocket, clientSet, str3, str, buildKeyPair);
        MultiRemoteAttestationResponse multiRemoteAttestationResponse = (MultiRemoteAttestationResponse) JsonUtil.fromJson(makeAttestationRequest.body, MultiRemoteAttestationResponse.class);
        HashMap hashMap = new HashMap();
        if (multiRemoteAttestationResponse.getAttestations().isEmpty() || multiRemoteAttestationResponse.getAttestations().size() > 3) {
            throw new MalformedResponseException("Incorrect number of attestations: " + multiRemoteAttestationResponse.getAttestations().size());
        }
        for (Map.Entry<String, RemoteAttestationResponse> entry : multiRemoteAttestationResponse.getAttestations().entrySet()) {
            hashMap.put(entry.getKey(), validateAndBuildRemoteAttestation(entry.getValue(), makeAttestationRequest.cookies, keyStore, buildKeyPair, str2));
        }
        return hashMap;
    }

    private static ECKeyPair buildKeyPair() {
        return Curve.generateKeyPair();
    }

    private static ResponsePair makeAttestationRequest(PushServiceSocket pushServiceSocket, PushServiceSocket.ClientSet clientSet, String str, String str2, ECKeyPair eCKeyPair) throws IOException {
        RemoteAttestationRequest remoteAttestationRequest = new RemoteAttestationRequest(eCKeyPair.getPublicKey().getPublicKeyBytes());
        LinkedList linkedList = new LinkedList();
        Response makeRequest = pushServiceSocket.makeRequest(clientSet, str, linkedList, "/v1/attestation/" + str2, "PUT", JsonUtil.toJson(remoteAttestationRequest));
        ResponseBody body = makeRequest.body();
        if (body != null) {
            return new ResponsePair(body.string(), parseCookies(makeRequest));
        }
        throw new MalformedResponseException("Empty response!");
    }

    private static List<String> parseCookies(Response response) {
        List<String> headers = response.headers("Set-Cookie");
        LinkedList linkedList = new LinkedList();
        for (String str : headers) {
            linkedList.add(str.split(";")[0]);
        }
        return linkedList;
    }

    private static RemoteAttestation validateAndBuildRemoteAttestation(RemoteAttestationResponse remoteAttestationResponse, List<String> list, KeyStore keyStore, ECKeyPair eCKeyPair, String str) throws Quote.InvalidQuoteFormatException, InvalidCiphertextException, UnauthenticatedQuoteException, SignatureException, InvalidKeyException {
        RemoteAttestationKeys remoteAttestationKeys = new RemoteAttestationKeys(eCKeyPair, remoteAttestationResponse.getServerEphemeralPublic(), remoteAttestationResponse.getServerStaticPublic());
        Quote quote = new Quote(remoteAttestationResponse.getQuote());
        byte[] requestId = RemoteAttestationCipher.getRequestId(remoteAttestationKeys, remoteAttestationResponse);
        RemoteAttestationCipher.verifyServerQuote(quote, remoteAttestationResponse.getServerStaticPublic(), str);
        RemoteAttestationCipher.verifyIasSignature(keyStore, remoteAttestationResponse.getCertificates(), remoteAttestationResponse.getSignatureBody(), remoteAttestationResponse.getSignature(), quote);
        return new RemoteAttestation(requestId, remoteAttestationKeys, list);
    }

    /* loaded from: classes5.dex */
    public static class ResponsePair {
        final String body;
        final List<String> cookies;

        private ResponsePair(String str, List<String> list) {
            this.body = str;
            this.cookies = list;
        }
    }
}
