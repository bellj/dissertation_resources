package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public class ProofRequiredResponse {
    @JsonProperty
    public List<String> options;
    @JsonProperty
    public String token;

    public String getToken() {
        return this.token;
    }

    public List<String> getOptions() {
        return this.options;
    }
}
