package org.whispersystems.signalservice.internal.push;

/* loaded from: classes5.dex */
public class PushTransportDetails {
    private static final String TAG;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
        r2 = new byte[r0];
        java.lang.System.arraycopy(r5, 0, r2, 0, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getStrippedPaddingMessageBody(byte[] r5) {
        /*
            r4 = this;
            int r0 = r5.length
            int r0 = r0 + -1
        L_0x0003:
            r1 = 0
            if (r0 < 0) goto L_0x001a
            byte r2 = r5[r0]
            r3 = -128(0xffffffffffffff80, float:NaN)
            if (r2 != r3) goto L_0x000d
            goto L_0x001b
        L_0x000d:
            if (r2 == 0) goto L_0x0017
            java.lang.String r0 = org.whispersystems.signalservice.internal.push.PushTransportDetails.TAG
            java.lang.String r1 = "Padding byte is malformed, returning unstripped padding."
            org.signal.libsignal.protocol.logging.Log.w(r0, r1)
            return r5
        L_0x0017:
            int r0 = r0 + -1
            goto L_0x0003
        L_0x001a:
            r0 = 0
        L_0x001b:
            byte[] r2 = new byte[r0]
            java.lang.System.arraycopy(r5, r1, r2, r1, r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.signalservice.internal.push.PushTransportDetails.getStrippedPaddingMessageBody(byte[]):byte[]");
    }

    public byte[] getPaddedMessageBody(byte[] bArr) {
        byte[] bArr2 = new byte[getPaddedMessageLength(bArr.length + 1) - 1];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        bArr2[bArr.length] = Byte.MIN_VALUE;
        return bArr2;
    }

    private int getPaddedMessageLength(int i) {
        int i2 = i + 1;
        int i3 = i2 / 160;
        if (i2 % 160 != 0) {
            i3++;
        }
        return i3 * 160;
    }
}
