package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class CdsiAuthResponse {
    @JsonProperty
    private String password;
    @JsonProperty
    private String username;

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }
}
