package org.whispersystems.signalservice.internal.serialize;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import j$.util.function.Function;
import org.whispersystems.signalservice.api.messages.SignalServiceMetadata;
import org.whispersystems.signalservice.internal.serialize.protos.MetadataProto;

/* loaded from: classes5.dex */
public final class SignalServiceMetadataProtobufSerializer {
    private SignalServiceMetadataProtobufSerializer() {
    }

    public static MetadataProto toProtobuf(SignalServiceMetadata signalServiceMetadata) {
        MetadataProto.Builder destinationUuid = MetadataProto.newBuilder().setAddress(SignalServiceAddressProtobufSerializer.toProtobuf(signalServiceMetadata.getSender())).setSenderDevice(signalServiceMetadata.getSenderDevice()).setNeedsReceipt(signalServiceMetadata.isNeedsReceipt()).setTimestamp(signalServiceMetadata.getTimestamp()).setServerReceivedTimestamp(signalServiceMetadata.getServerReceivedTimestamp()).setServerDeliveredTimestamp(signalServiceMetadata.getServerDeliveredTimestamp()).setServerGuid(signalServiceMetadata.getServerGuid()).setDestinationUuid(signalServiceMetadata.getDestinationUuid());
        if (signalServiceMetadata.getGroupId().isPresent()) {
            destinationUuid.setGroupId(ByteString.copyFrom(signalServiceMetadata.getGroupId().get()));
        }
        return destinationUuid.build();
    }

    public static SignalServiceMetadata fromProtobuf(MetadataProto metadataProto) {
        return new SignalServiceMetadata(SignalServiceAddressProtobufSerializer.fromProtobuf(metadataProto.getAddress()), metadataProto.getSenderDevice(), metadataProto.getTimestamp(), metadataProto.getServerReceivedTimestamp(), metadataProto.getServerDeliveredTimestamp(), metadataProto.getNeedsReceipt(), metadataProto.getServerGuid(), Optional.ofNullable(metadataProto.getGroupId()).map(new Function() { // from class: org.whispersystems.signalservice.internal.serialize.SignalServiceMetadataProtobufSerializer$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((ByteString) obj).toByteArray();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }), metadataProto.getDestinationUuid());
    }
}
