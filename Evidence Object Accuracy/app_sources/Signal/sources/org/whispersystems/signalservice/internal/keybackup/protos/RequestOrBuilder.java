package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface RequestOrBuilder extends MessageLiteOrBuilder {
    BackupRequest getBackup();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    DeleteRequest getDelete();

    RestoreRequest getRestore();

    boolean hasBackup();

    boolean hasDelete();

    boolean hasRestore();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
