package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.List;
import org.signal.libsignal.protocol.IdentityKey;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes.dex */
public class IdentityCheckResponse {
    @JsonProperty("elements")
    private List<AciIdentityPair> aciKeyPairs;

    public List<AciIdentityPair> getAciKeyPairs() {
        return this.aciKeyPairs;
    }

    /* loaded from: classes.dex */
    public static final class AciIdentityPair {
        @JsonProperty
        @JsonDeserialize(using = JsonUtil.ServiceIdDeserializer.class)
        private ServiceId aci;
        @JsonProperty
        @JsonDeserialize(using = JsonUtil.IdentityKeyDeserializer.class)
        private IdentityKey identityKey;

        public ServiceId getAci() {
            return this.aci;
        }

        public IdentityKey getIdentityKey() {
            return this.identityKey;
        }
    }
}
