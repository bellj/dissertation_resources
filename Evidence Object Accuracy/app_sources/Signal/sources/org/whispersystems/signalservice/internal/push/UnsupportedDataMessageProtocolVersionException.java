package org.whispersystems.signalservice.internal.push;

import j$.util.Optional;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;

/* loaded from: classes5.dex */
public final class UnsupportedDataMessageProtocolVersionException extends UnsupportedDataMessageException {
    private final int requiredVersion;

    public UnsupportedDataMessageProtocolVersionException(int i, int i2, String str, int i3, Optional<SignalServiceGroupContext> optional) {
        super("Required version: " + i2 + ", Our version: " + i, str, i3, optional);
        this.requiredVersion = i2;
    }

    public int getRequiredVersion() {
        return this.requiredVersion;
    }
}
