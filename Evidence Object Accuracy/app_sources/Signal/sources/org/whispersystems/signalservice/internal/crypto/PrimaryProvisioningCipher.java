package org.whispersystems.signalservice.internal.crypto;

import com.google.protobuf.ByteString;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.whispersystems.signalservice.internal.push.ProvisioningProtos;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class PrimaryProvisioningCipher {
    public static final String PROVISIONING_MESSAGE;
    private final ECPublicKey theirPublicKey;

    public PrimaryProvisioningCipher(ECPublicKey eCPublicKey) {
        this.theirPublicKey = eCPublicKey;
    }

    public byte[] encrypt(ProvisioningProtos.ProvisionMessage provisionMessage) throws InvalidKeyException {
        ECKeyPair generateKeyPair = Curve.generateKeyPair();
        byte[][] split = Util.split(HKDF.deriveSecrets(Curve.calculateAgreement(this.theirPublicKey, generateKeyPair.getPrivateKey()), PROVISIONING_MESSAGE.getBytes(), 64), 32, 32);
        byte[] bArr = {1};
        byte[] ciphertext = getCiphertext(split[0], provisionMessage.toByteArray());
        return ProvisioningProtos.ProvisionEnvelope.newBuilder().setPublicKey(ByteString.copyFrom(generateKeyPair.getPublicKey().serialize())).setBody(ByteString.copyFrom(Util.join(bArr, ciphertext, getMac(split[1], Util.join(bArr, ciphertext))))).build().toByteArray();
    }

    private byte[] getCiphertext(byte[] bArr, byte[] bArr2) {
        try {
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, new SecretKeySpec(bArr, "AES"));
            return Util.join(instance.getIV(), instance.doFinal(bArr2));
        } catch (java.security.InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    private byte[] getMac(byte[] bArr, byte[] bArr2) {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
            return instance.doFinal(bArr2);
        } catch (java.security.InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
