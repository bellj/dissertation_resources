package org.whispersystems.signalservice.internal.util.concurrent;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture;

/* loaded from: classes5.dex */
public class SettableFuture<T> implements ListenableFuture<T> {
    private boolean canceled;
    private boolean completed;
    private volatile Throwable exception;
    private final List<ListenableFuture.Listener<T>> listeners = new LinkedList();
    private volatile T result;

    @Override // java.util.concurrent.Future
    public synchronized boolean cancel(boolean z) {
        if (this.completed || this.canceled) {
            return false;
        }
        this.canceled = true;
        return true;
    }

    @Override // java.util.concurrent.Future
    public synchronized boolean isCancelled() {
        return this.canceled;
    }

    @Override // java.util.concurrent.Future
    public synchronized boolean isDone() {
        return this.completed;
    }

    public boolean set(T t) {
        synchronized (this) {
            if (!this.completed && !this.canceled) {
                this.result = t;
                this.completed = true;
                notifyAll();
                notifyAllListeners();
                return true;
            }
            return false;
        }
    }

    public boolean setException(Throwable th) {
        synchronized (this) {
            if (!this.completed && !this.canceled) {
                this.exception = th;
                this.completed = true;
                notifyAll();
                notifyAllListeners();
                return true;
            }
            return false;
        }
    }

    @Override // java.util.concurrent.Future
    public synchronized T get() throws InterruptedException, ExecutionException {
        while (!this.completed) {
            wait();
        }
        if (this.exception == null) {
        } else {
            throw new ExecutionException(this.exception);
        }
        return this.result;
    }

    @Override // java.util.concurrent.Future
    public synchronized T get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        long currentTimeMillis = System.currentTimeMillis();
        while (!this.completed && System.currentTimeMillis() - currentTimeMillis < timeUnit.toMillis(j)) {
            wait(timeUnit.toMillis(j));
        }
        if (this.completed) {
        } else {
            throw new TimeoutException();
        }
        return get();
    }

    @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture
    public void addListener(ListenableFuture.Listener<T> listener) {
        synchronized (this) {
            this.listeners.add(listener);
            if (this.completed) {
                notifyListener(listener);
            }
        }
    }

    private void notifyAllListeners() {
        LinkedList<ListenableFuture.Listener<T>> linkedList;
        synchronized (this) {
            linkedList = new LinkedList(this.listeners);
        }
        for (ListenableFuture.Listener<T> listener : linkedList) {
            notifyListener(listener);
        }
    }

    private void notifyListener(ListenableFuture.Listener<T> listener) {
        if (this.exception != null) {
            listener.onFailure(new ExecutionException(this.exception));
        } else {
            listener.onSuccess(this.result);
        }
    }
}
