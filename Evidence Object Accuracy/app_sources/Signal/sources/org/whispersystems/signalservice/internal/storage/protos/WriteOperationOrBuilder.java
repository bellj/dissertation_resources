package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;

/* loaded from: classes5.dex */
public interface WriteOperationOrBuilder extends MessageLiteOrBuilder {
    boolean getClearAll();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ByteString getDeleteKey(int i);

    int getDeleteKeyCount();

    List<ByteString> getDeleteKeyList();

    StorageItem getInsertItem(int i);

    int getInsertItemCount();

    List<StorageItem> getInsertItemList();

    StorageManifest getManifest();

    boolean hasManifest();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
