package org.whispersystems.signalservice.internal.push;

import androidx.recyclerview.widget.RecyclerView;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class ProvisioningProtos {

    /* loaded from: classes5.dex */
    public interface ProvisionEnvelopeOrBuilder extends MessageLiteOrBuilder {
        ByteString getBody();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getPublicKey();

        boolean hasBody();

        boolean hasPublicKey();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes5.dex */
    public interface ProvisionMessageOrBuilder extends MessageLiteOrBuilder {
        String getAci();

        ByteString getAciBytes();

        ByteString getAciIdentityKeyPrivate();

        ByteString getAciIdentityKeyPublic();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getNumber();

        ByteString getNumberBytes();

        String getPni();

        ByteString getPniBytes();

        ByteString getPniIdentityKeyPrivate();

        ByteString getPniIdentityKeyPublic();

        ByteString getProfileKey();

        String getProvisioningCode();

        ByteString getProvisioningCodeBytes();

        int getProvisioningVersion();

        boolean getReadReceipts();

        String getUserAgent();

        ByteString getUserAgentBytes();

        boolean hasAci();

        boolean hasAciIdentityKeyPrivate();

        boolean hasAciIdentityKeyPublic();

        boolean hasNumber();

        boolean hasPni();

        boolean hasPniIdentityKeyPrivate();

        boolean hasPniIdentityKeyPublic();

        boolean hasProfileKey();

        boolean hasProvisioningCode();

        boolean hasProvisioningVersion();

        boolean hasReadReceipts();

        boolean hasUserAgent();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes5.dex */
    public interface ProvisioningUuidOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getUuid();

        ByteString getUuidBytes();

        boolean hasUuid();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }

    private ProvisioningProtos() {
    }

    /* loaded from: classes5.dex */
    public enum ProvisioningVersion implements Internal.EnumLite {
        INITIAL(0),
        TABLET_SUPPORT(1);
        
        public static final ProvisioningVersion CURRENT = TABLET_SUPPORT;
        public static final int CURRENT_VALUE;
        public static final int INITIAL_VALUE;
        public static final int TABLET_SUPPORT_VALUE;
        private static final Internal.EnumLiteMap<ProvisioningVersion> internalValueMap = new Internal.EnumLiteMap<ProvisioningVersion>() { // from class: org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningVersion.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public ProvisioningVersion findValueByNumber(int i) {
                return ProvisioningVersion.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            return this.value;
        }

        @Deprecated
        public static ProvisioningVersion valueOf(int i) {
            return forNumber(i);
        }

        public static ProvisioningVersion forNumber(int i) {
            if (i == 0) {
                return INITIAL;
            }
            if (i != 1) {
                return null;
            }
            return TABLET_SUPPORT;
        }

        public static Internal.EnumLiteMap<ProvisioningVersion> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return ProvisioningVersionVerifier.INSTANCE;
        }

        /* loaded from: classes5.dex */
        private static final class ProvisioningVersionVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new ProvisioningVersionVerifier();

            private ProvisioningVersionVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return ProvisioningVersion.forNumber(i) != null;
            }
        }

        ProvisioningVersion(int i) {
            this.value = i;
        }
    }

    /* loaded from: classes5.dex */
    public static final class ProvisioningUuid extends GeneratedMessageLite<ProvisioningUuid, Builder> implements ProvisioningUuidOrBuilder {
        private static final ProvisioningUuid DEFAULT_INSTANCE;
        private static volatile Parser<ProvisioningUuid> PARSER;
        public static final int UUID_FIELD_NUMBER;
        private int bitField0_;
        private String uuid_ = "";

        private ProvisioningUuid() {
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningUuidOrBuilder
        public boolean hasUuid() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningUuidOrBuilder
        public String getUuid() {
            return this.uuid_;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningUuidOrBuilder
        public ByteString getUuidBytes() {
            return ByteString.copyFromUtf8(this.uuid_);
        }

        public void setUuid(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.uuid_ = str;
        }

        public void clearUuid() {
            this.bitField0_ &= -2;
            this.uuid_ = getDefaultInstance().getUuid();
        }

        public void setUuidBytes(ByteString byteString) {
            this.uuid_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        public static ProvisioningUuid parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static ProvisioningUuid parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static ProvisioningUuid parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static ProvisioningUuid parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static ProvisioningUuid parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static ProvisioningUuid parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static ProvisioningUuid parseFrom(InputStream inputStream) throws IOException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static ProvisioningUuid parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static ProvisioningUuid parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ProvisioningUuid) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static ProvisioningUuid parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisioningUuid) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static ProvisioningUuid parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static ProvisioningUuid parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisioningUuid) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(ProvisioningUuid provisioningUuid) {
            return DEFAULT_INSTANCE.createBuilder(provisioningUuid);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<ProvisioningUuid, Builder> implements ProvisioningUuidOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(ProvisioningUuid.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningUuidOrBuilder
            public boolean hasUuid() {
                return ((ProvisioningUuid) this.instance).hasUuid();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningUuidOrBuilder
            public String getUuid() {
                return ((ProvisioningUuid) this.instance).getUuid();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisioningUuidOrBuilder
            public ByteString getUuidBytes() {
                return ((ProvisioningUuid) this.instance).getUuidBytes();
            }

            public Builder setUuid(String str) {
                copyOnWrite();
                ((ProvisioningUuid) this.instance).setUuid(str);
                return this;
            }

            public Builder clearUuid() {
                copyOnWrite();
                ((ProvisioningUuid) this.instance).clearUuid();
                return this;
            }

            public Builder setUuidBytes(ByteString byteString) {
                copyOnWrite();
                ((ProvisioningUuid) this.instance).setUuidBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new ProvisioningUuid();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဈ\u0000", new Object[]{"bitField0_", "uuid_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<ProvisioningUuid> parser = PARSER;
                    if (parser == null) {
                        synchronized (ProvisioningUuid.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            ProvisioningUuid provisioningUuid = new ProvisioningUuid();
            DEFAULT_INSTANCE = provisioningUuid;
            GeneratedMessageLite.registerDefaultInstance(ProvisioningUuid.class, provisioningUuid);
        }

        public static ProvisioningUuid getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<ProvisioningUuid> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.push.ProvisioningProtos$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes5.dex */
    public static final class ProvisionEnvelope extends GeneratedMessageLite<ProvisionEnvelope, Builder> implements ProvisionEnvelopeOrBuilder {
        public static final int BODY_FIELD_NUMBER;
        private static final ProvisionEnvelope DEFAULT_INSTANCE;
        private static volatile Parser<ProvisionEnvelope> PARSER;
        public static final int PUBLICKEY_FIELD_NUMBER;
        private int bitField0_;
        private ByteString body_;
        private ByteString publicKey_;

        private ProvisionEnvelope() {
            ByteString byteString = ByteString.EMPTY;
            this.publicKey_ = byteString;
            this.body_ = byteString;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
        public boolean hasPublicKey() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
        public ByteString getPublicKey() {
            return this.publicKey_;
        }

        public void setPublicKey(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 1;
            this.publicKey_ = byteString;
        }

        public void clearPublicKey() {
            this.bitField0_ &= -2;
            this.publicKey_ = getDefaultInstance().getPublicKey();
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
        public boolean hasBody() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
        public ByteString getBody() {
            return this.body_;
        }

        public void setBody(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 2;
            this.body_ = byteString;
        }

        public void clearBody() {
            this.bitField0_ &= -3;
            this.body_ = getDefaultInstance().getBody();
        }

        public static ProvisionEnvelope parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static ProvisionEnvelope parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static ProvisionEnvelope parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static ProvisionEnvelope parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static ProvisionEnvelope parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static ProvisionEnvelope parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static ProvisionEnvelope parseFrom(InputStream inputStream) throws IOException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static ProvisionEnvelope parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static ProvisionEnvelope parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static ProvisionEnvelope parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static ProvisionEnvelope parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static ProvisionEnvelope parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisionEnvelope) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(ProvisionEnvelope provisionEnvelope) {
            return DEFAULT_INSTANCE.createBuilder(provisionEnvelope);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<ProvisionEnvelope, Builder> implements ProvisionEnvelopeOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(ProvisionEnvelope.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
            public boolean hasPublicKey() {
                return ((ProvisionEnvelope) this.instance).hasPublicKey();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
            public ByteString getPublicKey() {
                return ((ProvisionEnvelope) this.instance).getPublicKey();
            }

            public Builder setPublicKey(ByteString byteString) {
                copyOnWrite();
                ((ProvisionEnvelope) this.instance).setPublicKey(byteString);
                return this;
            }

            public Builder clearPublicKey() {
                copyOnWrite();
                ((ProvisionEnvelope) this.instance).clearPublicKey();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
            public boolean hasBody() {
                return ((ProvisionEnvelope) this.instance).hasBody();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionEnvelopeOrBuilder
            public ByteString getBody() {
                return ((ProvisionEnvelope) this.instance).getBody();
            }

            public Builder setBody(ByteString byteString) {
                copyOnWrite();
                ((ProvisionEnvelope) this.instance).setBody(byteString);
                return this;
            }

            public Builder clearBody() {
                copyOnWrite();
                ((ProvisionEnvelope) this.instance).clearBody();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new ProvisionEnvelope();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001", new Object[]{"bitField0_", "publicKey_", "body_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<ProvisionEnvelope> parser = PARSER;
                    if (parser == null) {
                        synchronized (ProvisionEnvelope.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            ProvisionEnvelope provisionEnvelope = new ProvisionEnvelope();
            DEFAULT_INSTANCE = provisionEnvelope;
            GeneratedMessageLite.registerDefaultInstance(ProvisionEnvelope.class, provisionEnvelope);
        }

        public static ProvisionEnvelope getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<ProvisionEnvelope> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes5.dex */
    public static final class ProvisionMessage extends GeneratedMessageLite<ProvisionMessage, Builder> implements ProvisionMessageOrBuilder {
        public static final int ACIIDENTITYKEYPRIVATE_FIELD_NUMBER;
        public static final int ACIIDENTITYKEYPUBLIC_FIELD_NUMBER;
        public static final int ACI_FIELD_NUMBER;
        private static final ProvisionMessage DEFAULT_INSTANCE;
        public static final int NUMBER_FIELD_NUMBER;
        private static volatile Parser<ProvisionMessage> PARSER;
        public static final int PNIIDENTITYKEYPRIVATE_FIELD_NUMBER;
        public static final int PNIIDENTITYKEYPUBLIC_FIELD_NUMBER;
        public static final int PNI_FIELD_NUMBER;
        public static final int PROFILEKEY_FIELD_NUMBER;
        public static final int PROVISIONINGCODE_FIELD_NUMBER;
        public static final int PROVISIONINGVERSION_FIELD_NUMBER;
        public static final int READRECEIPTS_FIELD_NUMBER;
        public static final int USERAGENT_FIELD_NUMBER;
        private ByteString aciIdentityKeyPrivate_;
        private ByteString aciIdentityKeyPublic_;
        private String aci_ = "";
        private int bitField0_;
        private String number_ = "";
        private ByteString pniIdentityKeyPrivate_;
        private ByteString pniIdentityKeyPublic_;
        private String pni_ = "";
        private ByteString profileKey_;
        private String provisioningCode_ = "";
        private int provisioningVersion_;
        private boolean readReceipts_;
        private String userAgent_ = "";

        private ProvisionMessage() {
            ByteString byteString = ByteString.EMPTY;
            this.aciIdentityKeyPublic_ = byteString;
            this.aciIdentityKeyPrivate_ = byteString;
            this.pniIdentityKeyPublic_ = byteString;
            this.pniIdentityKeyPrivate_ = byteString;
            this.profileKey_ = byteString;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasAciIdentityKeyPublic() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getAciIdentityKeyPublic() {
            return this.aciIdentityKeyPublic_;
        }

        public void setAciIdentityKeyPublic(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 1;
            this.aciIdentityKeyPublic_ = byteString;
        }

        public void clearAciIdentityKeyPublic() {
            this.bitField0_ &= -2;
            this.aciIdentityKeyPublic_ = getDefaultInstance().getAciIdentityKeyPublic();
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasAciIdentityKeyPrivate() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getAciIdentityKeyPrivate() {
            return this.aciIdentityKeyPrivate_;
        }

        public void setAciIdentityKeyPrivate(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 2;
            this.aciIdentityKeyPrivate_ = byteString;
        }

        public void clearAciIdentityKeyPrivate() {
            this.bitField0_ &= -3;
            this.aciIdentityKeyPrivate_ = getDefaultInstance().getAciIdentityKeyPrivate();
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasPniIdentityKeyPublic() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getPniIdentityKeyPublic() {
            return this.pniIdentityKeyPublic_;
        }

        public void setPniIdentityKeyPublic(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 4;
            this.pniIdentityKeyPublic_ = byteString;
        }

        public void clearPniIdentityKeyPublic() {
            this.bitField0_ &= -5;
            this.pniIdentityKeyPublic_ = getDefaultInstance().getPniIdentityKeyPublic();
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasPniIdentityKeyPrivate() {
            return (this.bitField0_ & 8) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getPniIdentityKeyPrivate() {
            return this.pniIdentityKeyPrivate_;
        }

        public void setPniIdentityKeyPrivate(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 8;
            this.pniIdentityKeyPrivate_ = byteString;
        }

        public void clearPniIdentityKeyPrivate() {
            this.bitField0_ &= -9;
            this.pniIdentityKeyPrivate_ = getDefaultInstance().getPniIdentityKeyPrivate();
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasAci() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public String getAci() {
            return this.aci_;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getAciBytes() {
            return ByteString.copyFromUtf8(this.aci_);
        }

        public void setAci(String str) {
            str.getClass();
            this.bitField0_ |= 16;
            this.aci_ = str;
        }

        public void clearAci() {
            this.bitField0_ &= -17;
            this.aci_ = getDefaultInstance().getAci();
        }

        public void setAciBytes(ByteString byteString) {
            this.aci_ = byteString.toStringUtf8();
            this.bitField0_ |= 16;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasPni() {
            return (this.bitField0_ & 32) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public String getPni() {
            return this.pni_;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getPniBytes() {
            return ByteString.copyFromUtf8(this.pni_);
        }

        public void setPni(String str) {
            str.getClass();
            this.bitField0_ |= 32;
            this.pni_ = str;
        }

        public void clearPni() {
            this.bitField0_ &= -33;
            this.pni_ = getDefaultInstance().getPni();
        }

        public void setPniBytes(ByteString byteString) {
            this.pni_ = byteString.toStringUtf8();
            this.bitField0_ |= 32;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasNumber() {
            return (this.bitField0_ & 64) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public String getNumber() {
            return this.number_;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getNumberBytes() {
            return ByteString.copyFromUtf8(this.number_);
        }

        public void setNumber(String str) {
            str.getClass();
            this.bitField0_ |= 64;
            this.number_ = str;
        }

        public void clearNumber() {
            this.bitField0_ &= -65;
            this.number_ = getDefaultInstance().getNumber();
        }

        public void setNumberBytes(ByteString byteString) {
            this.number_ = byteString.toStringUtf8();
            this.bitField0_ |= 64;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasProvisioningCode() {
            return (this.bitField0_ & 128) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public String getProvisioningCode() {
            return this.provisioningCode_;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getProvisioningCodeBytes() {
            return ByteString.copyFromUtf8(this.provisioningCode_);
        }

        public void setProvisioningCode(String str) {
            str.getClass();
            this.bitField0_ |= 128;
            this.provisioningCode_ = str;
        }

        public void clearProvisioningCode() {
            this.bitField0_ &= -129;
            this.provisioningCode_ = getDefaultInstance().getProvisioningCode();
        }

        public void setProvisioningCodeBytes(ByteString byteString) {
            this.provisioningCode_ = byteString.toStringUtf8();
            this.bitField0_ |= 128;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasUserAgent() {
            return (this.bitField0_ & 256) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public String getUserAgent() {
            return this.userAgent_;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getUserAgentBytes() {
            return ByteString.copyFromUtf8(this.userAgent_);
        }

        public void setUserAgent(String str) {
            str.getClass();
            this.bitField0_ |= 256;
            this.userAgent_ = str;
        }

        public void clearUserAgent() {
            this.bitField0_ &= -257;
            this.userAgent_ = getDefaultInstance().getUserAgent();
        }

        public void setUserAgentBytes(ByteString byteString) {
            this.userAgent_ = byteString.toStringUtf8();
            this.bitField0_ |= 256;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasProfileKey() {
            return (this.bitField0_ & 512) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public ByteString getProfileKey() {
            return this.profileKey_;
        }

        public void setProfileKey(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 512;
            this.profileKey_ = byteString;
        }

        public void clearProfileKey() {
            this.bitField0_ &= -513;
            this.profileKey_ = getDefaultInstance().getProfileKey();
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasReadReceipts() {
            return (this.bitField0_ & 1024) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean getReadReceipts() {
            return this.readReceipts_;
        }

        public void setReadReceipts(boolean z) {
            this.bitField0_ |= 1024;
            this.readReceipts_ = z;
        }

        public void clearReadReceipts() {
            this.bitField0_ &= -1025;
            this.readReceipts_ = false;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public boolean hasProvisioningVersion() {
            return (this.bitField0_ & RecyclerView.ItemAnimator.FLAG_MOVED) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
        public int getProvisioningVersion() {
            return this.provisioningVersion_;
        }

        public void setProvisioningVersion(int i) {
            this.bitField0_ |= RecyclerView.ItemAnimator.FLAG_MOVED;
            this.provisioningVersion_ = i;
        }

        public void clearProvisioningVersion() {
            this.bitField0_ &= -2049;
            this.provisioningVersion_ = 0;
        }

        public static ProvisionMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static ProvisionMessage parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static ProvisionMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static ProvisionMessage parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static ProvisionMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static ProvisionMessage parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static ProvisionMessage parseFrom(InputStream inputStream) throws IOException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static ProvisionMessage parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static ProvisionMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (ProvisionMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static ProvisionMessage parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisionMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static ProvisionMessage parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static ProvisionMessage parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (ProvisionMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(ProvisionMessage provisionMessage) {
            return DEFAULT_INSTANCE.createBuilder(provisionMessage);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<ProvisionMessage, Builder> implements ProvisionMessageOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(ProvisionMessage.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasAciIdentityKeyPublic() {
                return ((ProvisionMessage) this.instance).hasAciIdentityKeyPublic();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getAciIdentityKeyPublic() {
                return ((ProvisionMessage) this.instance).getAciIdentityKeyPublic();
            }

            public Builder setAciIdentityKeyPublic(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setAciIdentityKeyPublic(byteString);
                return this;
            }

            public Builder clearAciIdentityKeyPublic() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearAciIdentityKeyPublic();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasAciIdentityKeyPrivate() {
                return ((ProvisionMessage) this.instance).hasAciIdentityKeyPrivate();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getAciIdentityKeyPrivate() {
                return ((ProvisionMessage) this.instance).getAciIdentityKeyPrivate();
            }

            public Builder setAciIdentityKeyPrivate(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setAciIdentityKeyPrivate(byteString);
                return this;
            }

            public Builder clearAciIdentityKeyPrivate() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearAciIdentityKeyPrivate();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasPniIdentityKeyPublic() {
                return ((ProvisionMessage) this.instance).hasPniIdentityKeyPublic();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getPniIdentityKeyPublic() {
                return ((ProvisionMessage) this.instance).getPniIdentityKeyPublic();
            }

            public Builder setPniIdentityKeyPublic(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setPniIdentityKeyPublic(byteString);
                return this;
            }

            public Builder clearPniIdentityKeyPublic() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearPniIdentityKeyPublic();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasPniIdentityKeyPrivate() {
                return ((ProvisionMessage) this.instance).hasPniIdentityKeyPrivate();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getPniIdentityKeyPrivate() {
                return ((ProvisionMessage) this.instance).getPniIdentityKeyPrivate();
            }

            public Builder setPniIdentityKeyPrivate(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setPniIdentityKeyPrivate(byteString);
                return this;
            }

            public Builder clearPniIdentityKeyPrivate() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearPniIdentityKeyPrivate();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasAci() {
                return ((ProvisionMessage) this.instance).hasAci();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public String getAci() {
                return ((ProvisionMessage) this.instance).getAci();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getAciBytes() {
                return ((ProvisionMessage) this.instance).getAciBytes();
            }

            public Builder setAci(String str) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setAci(str);
                return this;
            }

            public Builder clearAci() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearAci();
                return this;
            }

            public Builder setAciBytes(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setAciBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasPni() {
                return ((ProvisionMessage) this.instance).hasPni();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public String getPni() {
                return ((ProvisionMessage) this.instance).getPni();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getPniBytes() {
                return ((ProvisionMessage) this.instance).getPniBytes();
            }

            public Builder setPni(String str) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setPni(str);
                return this;
            }

            public Builder clearPni() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearPni();
                return this;
            }

            public Builder setPniBytes(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setPniBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasNumber() {
                return ((ProvisionMessage) this.instance).hasNumber();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public String getNumber() {
                return ((ProvisionMessage) this.instance).getNumber();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getNumberBytes() {
                return ((ProvisionMessage) this.instance).getNumberBytes();
            }

            public Builder setNumber(String str) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setNumber(str);
                return this;
            }

            public Builder clearNumber() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearNumber();
                return this;
            }

            public Builder setNumberBytes(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setNumberBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasProvisioningCode() {
                return ((ProvisionMessage) this.instance).hasProvisioningCode();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public String getProvisioningCode() {
                return ((ProvisionMessage) this.instance).getProvisioningCode();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getProvisioningCodeBytes() {
                return ((ProvisionMessage) this.instance).getProvisioningCodeBytes();
            }

            public Builder setProvisioningCode(String str) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setProvisioningCode(str);
                return this;
            }

            public Builder clearProvisioningCode() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearProvisioningCode();
                return this;
            }

            public Builder setProvisioningCodeBytes(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setProvisioningCodeBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasUserAgent() {
                return ((ProvisionMessage) this.instance).hasUserAgent();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public String getUserAgent() {
                return ((ProvisionMessage) this.instance).getUserAgent();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getUserAgentBytes() {
                return ((ProvisionMessage) this.instance).getUserAgentBytes();
            }

            public Builder setUserAgent(String str) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setUserAgent(str);
                return this;
            }

            public Builder clearUserAgent() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearUserAgent();
                return this;
            }

            public Builder setUserAgentBytes(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setUserAgentBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasProfileKey() {
                return ((ProvisionMessage) this.instance).hasProfileKey();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public ByteString getProfileKey() {
                return ((ProvisionMessage) this.instance).getProfileKey();
            }

            public Builder setProfileKey(ByteString byteString) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setProfileKey(byteString);
                return this;
            }

            public Builder clearProfileKey() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearProfileKey();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasReadReceipts() {
                return ((ProvisionMessage) this.instance).hasReadReceipts();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean getReadReceipts() {
                return ((ProvisionMessage) this.instance).getReadReceipts();
            }

            public Builder setReadReceipts(boolean z) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setReadReceipts(z);
                return this;
            }

            public Builder clearReadReceipts() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearReadReceipts();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public boolean hasProvisioningVersion() {
                return ((ProvisionMessage) this.instance).hasProvisioningVersion();
            }

            @Override // org.whispersystems.signalservice.internal.push.ProvisioningProtos.ProvisionMessageOrBuilder
            public int getProvisioningVersion() {
                return ((ProvisionMessage) this.instance).getProvisioningVersion();
            }

            public Builder setProvisioningVersion(int i) {
                copyOnWrite();
                ((ProvisionMessage) this.instance).setProvisioningVersion(i);
                return this;
            }

            public Builder clearProvisioningVersion() {
                copyOnWrite();
                ((ProvisionMessage) this.instance).clearProvisioningVersion();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new ProvisionMessage();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\f\u0000\u0001\u0001\f\f\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001\u0003ဈ\u0006\u0004ဈ\u0007\u0005ဈ\b\u0006ည\t\u0007ဇ\n\bဈ\u0004\tဋ\u000b\nဈ\u0005\u000bည\u0002\fည\u0003", new Object[]{"bitField0_", "aciIdentityKeyPublic_", "aciIdentityKeyPrivate_", "number_", "provisioningCode_", "userAgent_", "profileKey_", "readReceipts_", "aci_", "provisioningVersion_", "pni_", "pniIdentityKeyPublic_", "pniIdentityKeyPrivate_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<ProvisionMessage> parser = PARSER;
                    if (parser == null) {
                        synchronized (ProvisionMessage.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            ProvisionMessage provisionMessage = new ProvisionMessage();
            DEFAULT_INSTANCE = provisionMessage;
            GeneratedMessageLite.registerDefaultInstance(ProvisionMessage.class, provisionMessage);
        }

        public static ProvisionMessage getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<ProvisionMessage> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }
}
