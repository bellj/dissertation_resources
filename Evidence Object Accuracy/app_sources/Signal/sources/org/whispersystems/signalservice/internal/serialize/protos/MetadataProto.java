package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.whispersystems.signalservice.internal.serialize.protos.AddressProto;

/* loaded from: classes5.dex */
public final class MetadataProto extends GeneratedMessageLite<MetadataProto, Builder> implements MetadataProtoOrBuilder {
    public static final int ADDRESS_FIELD_NUMBER;
    private static final MetadataProto DEFAULT_INSTANCE;
    public static final int DESTINATIONUUID_FIELD_NUMBER;
    public static final int GROUPID_FIELD_NUMBER;
    public static final int NEEDSRECEIPT_FIELD_NUMBER;
    private static volatile Parser<MetadataProto> PARSER;
    public static final int SENDERDEVICE_FIELD_NUMBER;
    public static final int SERVERDELIVEREDTIMESTAMP_FIELD_NUMBER;
    public static final int SERVERGUID_FIELD_NUMBER;
    public static final int SERVERRECEIVEDTIMESTAMP_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    private AddressProto address_;
    private int bitField0_;
    private String destinationUuid_ = "";
    private ByteString groupId_ = ByteString.EMPTY;
    private boolean needsReceipt_;
    private int senderDevice_;
    private long serverDeliveredTimestamp_;
    private String serverGuid_ = "";
    private long serverReceivedTimestamp_;
    private long timestamp_;

    private MetadataProto() {
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasAddress() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public AddressProto getAddress() {
        AddressProto addressProto = this.address_;
        return addressProto == null ? AddressProto.getDefaultInstance() : addressProto;
    }

    public void setAddress(AddressProto addressProto) {
        addressProto.getClass();
        this.address_ = addressProto;
        this.bitField0_ |= 1;
    }

    public void mergeAddress(AddressProto addressProto) {
        addressProto.getClass();
        AddressProto addressProto2 = this.address_;
        if (addressProto2 == null || addressProto2 == AddressProto.getDefaultInstance()) {
            this.address_ = addressProto;
        } else {
            this.address_ = AddressProto.newBuilder(this.address_).mergeFrom((AddressProto.Builder) addressProto).buildPartial();
        }
        this.bitField0_ |= 1;
    }

    public void clearAddress() {
        this.address_ = null;
        this.bitField0_ &= -2;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasSenderDevice() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public int getSenderDevice() {
        return this.senderDevice_;
    }

    public void setSenderDevice(int i) {
        this.bitField0_ |= 2;
        this.senderDevice_ = i;
    }

    public void clearSenderDevice() {
        this.bitField0_ &= -3;
        this.senderDevice_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasTimestamp() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.bitField0_ |= 4;
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.bitField0_ &= -5;
        this.timestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasServerReceivedTimestamp() {
        return (this.bitField0_ & 8) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public long getServerReceivedTimestamp() {
        return this.serverReceivedTimestamp_;
    }

    public void setServerReceivedTimestamp(long j) {
        this.bitField0_ |= 8;
        this.serverReceivedTimestamp_ = j;
    }

    public void clearServerReceivedTimestamp() {
        this.bitField0_ &= -9;
        this.serverReceivedTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasServerDeliveredTimestamp() {
        return (this.bitField0_ & 16) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public long getServerDeliveredTimestamp() {
        return this.serverDeliveredTimestamp_;
    }

    public void setServerDeliveredTimestamp(long j) {
        this.bitField0_ |= 16;
        this.serverDeliveredTimestamp_ = j;
    }

    public void clearServerDeliveredTimestamp() {
        this.bitField0_ &= -17;
        this.serverDeliveredTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasNeedsReceipt() {
        return (this.bitField0_ & 32) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean getNeedsReceipt() {
        return this.needsReceipt_;
    }

    public void setNeedsReceipt(boolean z) {
        this.bitField0_ |= 32;
        this.needsReceipt_ = z;
    }

    public void clearNeedsReceipt() {
        this.bitField0_ &= -33;
        this.needsReceipt_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasServerGuid() {
        return (this.bitField0_ & 64) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public String getServerGuid() {
        return this.serverGuid_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public ByteString getServerGuidBytes() {
        return ByteString.copyFromUtf8(this.serverGuid_);
    }

    public void setServerGuid(String str) {
        str.getClass();
        this.bitField0_ |= 64;
        this.serverGuid_ = str;
    }

    public void clearServerGuid() {
        this.bitField0_ &= -65;
        this.serverGuid_ = getDefaultInstance().getServerGuid();
    }

    public void setServerGuidBytes(ByteString byteString) {
        this.serverGuid_ = byteString.toStringUtf8();
        this.bitField0_ |= 64;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasGroupId() {
        return (this.bitField0_ & 128) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public ByteString getGroupId() {
        return this.groupId_;
    }

    public void setGroupId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 128;
        this.groupId_ = byteString;
    }

    public void clearGroupId() {
        this.bitField0_ &= -129;
        this.groupId_ = getDefaultInstance().getGroupId();
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public boolean hasDestinationUuid() {
        return (this.bitField0_ & 256) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public String getDestinationUuid() {
        return this.destinationUuid_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
    public ByteString getDestinationUuidBytes() {
        return ByteString.copyFromUtf8(this.destinationUuid_);
    }

    public void setDestinationUuid(String str) {
        str.getClass();
        this.bitField0_ |= 256;
        this.destinationUuid_ = str;
    }

    public void clearDestinationUuid() {
        this.bitField0_ &= -257;
        this.destinationUuid_ = getDefaultInstance().getDestinationUuid();
    }

    public void setDestinationUuidBytes(ByteString byteString) {
        this.destinationUuid_ = byteString.toStringUtf8();
        this.bitField0_ |= 256;
    }

    public static MetadataProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MetadataProto parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MetadataProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MetadataProto parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MetadataProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MetadataProto parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MetadataProto parseFrom(InputStream inputStream) throws IOException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MetadataProto parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MetadataProto parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MetadataProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MetadataProto parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MetadataProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MetadataProto parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MetadataProto parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MetadataProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MetadataProto metadataProto) {
        return DEFAULT_INSTANCE.createBuilder(metadataProto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MetadataProto, Builder> implements MetadataProtoOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(MetadataProto.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasAddress() {
            return ((MetadataProto) this.instance).hasAddress();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public AddressProto getAddress() {
            return ((MetadataProto) this.instance).getAddress();
        }

        public Builder setAddress(AddressProto addressProto) {
            copyOnWrite();
            ((MetadataProto) this.instance).setAddress(addressProto);
            return this;
        }

        public Builder setAddress(AddressProto.Builder builder) {
            copyOnWrite();
            ((MetadataProto) this.instance).setAddress(builder.build());
            return this;
        }

        public Builder mergeAddress(AddressProto addressProto) {
            copyOnWrite();
            ((MetadataProto) this.instance).mergeAddress(addressProto);
            return this;
        }

        public Builder clearAddress() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearAddress();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasSenderDevice() {
            return ((MetadataProto) this.instance).hasSenderDevice();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public int getSenderDevice() {
            return ((MetadataProto) this.instance).getSenderDevice();
        }

        public Builder setSenderDevice(int i) {
            copyOnWrite();
            ((MetadataProto) this.instance).setSenderDevice(i);
            return this;
        }

        public Builder clearSenderDevice() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearSenderDevice();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasTimestamp() {
            return ((MetadataProto) this.instance).hasTimestamp();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public long getTimestamp() {
            return ((MetadataProto) this.instance).getTimestamp();
        }

        public Builder setTimestamp(long j) {
            copyOnWrite();
            ((MetadataProto) this.instance).setTimestamp(j);
            return this;
        }

        public Builder clearTimestamp() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasServerReceivedTimestamp() {
            return ((MetadataProto) this.instance).hasServerReceivedTimestamp();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public long getServerReceivedTimestamp() {
            return ((MetadataProto) this.instance).getServerReceivedTimestamp();
        }

        public Builder setServerReceivedTimestamp(long j) {
            copyOnWrite();
            ((MetadataProto) this.instance).setServerReceivedTimestamp(j);
            return this;
        }

        public Builder clearServerReceivedTimestamp() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearServerReceivedTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasServerDeliveredTimestamp() {
            return ((MetadataProto) this.instance).hasServerDeliveredTimestamp();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public long getServerDeliveredTimestamp() {
            return ((MetadataProto) this.instance).getServerDeliveredTimestamp();
        }

        public Builder setServerDeliveredTimestamp(long j) {
            copyOnWrite();
            ((MetadataProto) this.instance).setServerDeliveredTimestamp(j);
            return this;
        }

        public Builder clearServerDeliveredTimestamp() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearServerDeliveredTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasNeedsReceipt() {
            return ((MetadataProto) this.instance).hasNeedsReceipt();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean getNeedsReceipt() {
            return ((MetadataProto) this.instance).getNeedsReceipt();
        }

        public Builder setNeedsReceipt(boolean z) {
            copyOnWrite();
            ((MetadataProto) this.instance).setNeedsReceipt(z);
            return this;
        }

        public Builder clearNeedsReceipt() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearNeedsReceipt();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasServerGuid() {
            return ((MetadataProto) this.instance).hasServerGuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public String getServerGuid() {
            return ((MetadataProto) this.instance).getServerGuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public ByteString getServerGuidBytes() {
            return ((MetadataProto) this.instance).getServerGuidBytes();
        }

        public Builder setServerGuid(String str) {
            copyOnWrite();
            ((MetadataProto) this.instance).setServerGuid(str);
            return this;
        }

        public Builder clearServerGuid() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearServerGuid();
            return this;
        }

        public Builder setServerGuidBytes(ByteString byteString) {
            copyOnWrite();
            ((MetadataProto) this.instance).setServerGuidBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasGroupId() {
            return ((MetadataProto) this.instance).hasGroupId();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public ByteString getGroupId() {
            return ((MetadataProto) this.instance).getGroupId();
        }

        public Builder setGroupId(ByteString byteString) {
            copyOnWrite();
            ((MetadataProto) this.instance).setGroupId(byteString);
            return this;
        }

        public Builder clearGroupId() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearGroupId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public boolean hasDestinationUuid() {
            return ((MetadataProto) this.instance).hasDestinationUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public String getDestinationUuid() {
            return ((MetadataProto) this.instance).getDestinationUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.MetadataProtoOrBuilder
        public ByteString getDestinationUuidBytes() {
            return ((MetadataProto) this.instance).getDestinationUuidBytes();
        }

        public Builder setDestinationUuid(String str) {
            copyOnWrite();
            ((MetadataProto) this.instance).setDestinationUuid(str);
            return this;
        }

        public Builder clearDestinationUuid() {
            copyOnWrite();
            ((MetadataProto) this.instance).clearDestinationUuid();
            return this;
        }

        public Builder setDestinationUuidBytes(ByteString byteString) {
            copyOnWrite();
            ((MetadataProto) this.instance).setDestinationUuidBytes(byteString);
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.serialize.protos.MetadataProto$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MetadataProto();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0000\u0000\u0001ဉ\u0000\u0002င\u0001\u0003ဂ\u0002\u0004ဇ\u0005\u0005ဂ\u0003\u0006ဂ\u0004\u0007ဈ\u0006\bည\u0007\tဈ\b", new Object[]{"bitField0_", "address_", "senderDevice_", "timestamp_", "needsReceipt_", "serverReceivedTimestamp_", "serverDeliveredTimestamp_", "serverGuid_", "groupId_", "destinationUuid_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MetadataProto> parser = PARSER;
                if (parser == null) {
                    synchronized (MetadataProto.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MetadataProto metadataProto = new MetadataProto();
        DEFAULT_INSTANCE = metadataProto;
        GeneratedMessageLite.registerDefaultInstance(MetadataProto.class, metadataProto);
    }

    public static MetadataProto getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MetadataProto> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
