package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialResponse;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public class ReceiptCredentialResponseJson {
    private final ReceiptCredentialResponse receiptCredentialResponse;

    ReceiptCredentialResponseJson(@JsonProperty("receiptCredentialResponse") String str) {
        ReceiptCredentialResponse receiptCredentialResponse;
        try {
            receiptCredentialResponse = new ReceiptCredentialResponse(Base64.decode(str));
        } catch (IOException | InvalidInputException unused) {
            receiptCredentialResponse = null;
        }
        this.receiptCredentialResponse = receiptCredentialResponse;
    }

    public ReceiptCredentialResponse getReceiptCredentialResponse() {
        return this.receiptCredentialResponse;
    }
}
