package org.whispersystems.signalservice.internal.push.http;

import java.io.IOException;
import java.io.OutputStream;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.whispersystems.signalservice.api.crypto.DigestingOutputStream;
import org.whispersystems.signalservice.api.crypto.ProfileCipherOutputStream;

/* loaded from: classes5.dex */
public class ProfileCipherOutputStreamFactory implements OutputStreamFactory {
    private final ProfileKey key;

    public ProfileCipherOutputStreamFactory(ProfileKey profileKey) {
        this.key = profileKey;
    }

    @Override // org.whispersystems.signalservice.internal.push.http.OutputStreamFactory
    public DigestingOutputStream createFor(OutputStream outputStream) throws IOException {
        return new ProfileCipherOutputStream(outputStream, this.key);
    }
}
