package org.whispersystems.signalservice.internal.contacts.crypto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.whispersystems.signalservice.api.crypto.CryptoUtil;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.internal.contacts.crypto.AESCipher;
import org.whispersystems.signalservice.internal.contacts.entities.DiscoveryRequest;
import org.whispersystems.signalservice.internal.contacts.entities.DiscoveryResponse;
import org.whispersystems.signalservice.internal.contacts.entities.QueryEnvelope;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public final class ContactDiscoveryCipher {
    private ContactDiscoveryCipher() {
    }

    public static DiscoveryRequest createDiscoveryRequest(List<String> list, Map<String, RemoteAttestation> map) {
        byte[] secretBytes = Util.getSecretBytes(32);
        byte[] buildQueryData = buildQueryData(list);
        AESCipher.AESEncryptedResult encrypt = AESCipher.encrypt(secretBytes, null, buildQueryData);
        byte[] sha256 = CryptoUtil.sha256(buildQueryData);
        HashMap hashMap = new HashMap(map.size());
        for (Map.Entry<String, RemoteAttestation> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), buildQueryEnvelope(entry.getValue().getRequestId(), entry.getValue().getKeys().getClientKey(), secretBytes));
        }
        return new DiscoveryRequest(list.size(), sha256, encrypt.iv, encrypt.data, encrypt.mac, hashMap);
    }

    public static byte[] getDiscoveryResponseData(DiscoveryResponse discoveryResponse, Collection<RemoteAttestation> collection) throws InvalidCiphertextException, IOException {
        for (RemoteAttestation remoteAttestation : collection) {
            if (Arrays.equals(discoveryResponse.getRequestId(), remoteAttestation.getRequestId())) {
                return AESCipher.decrypt(remoteAttestation.getKeys().getServerKey(), discoveryResponse.getIv(), discoveryResponse.getData(), discoveryResponse.getMac());
            }
        }
        throw new NoMatchingRequestIdException();
    }

    private static byte[] buildQueryData(List<String> list) {
        try {
            byte[] secretBytes = Util.getSecretBytes(32);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(secretBytes);
            for (String str : list) {
                byteArrayOutputStream.write(ByteUtil.longToByteArray(Long.parseLong(str)));
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private static QueryEnvelope buildQueryEnvelope(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        AESCipher.AESEncryptedResult encrypt = AESCipher.encrypt(bArr2, bArr, bArr3);
        return new QueryEnvelope(bArr, encrypt.iv, encrypt.data, encrypt.mac);
    }

    /* loaded from: classes5.dex */
    public static class NoMatchingRequestIdException extends IOException {
        NoMatchingRequestIdException() {
        }
    }
}
