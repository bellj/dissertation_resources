package org.whispersystems.signalservice.internal.push.exceptions;

import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.push.StaleDevices;

/* loaded from: classes5.dex */
public class StaleDevicesException extends NonSuccessfulResponseCodeException {
    private final StaleDevices staleDevices;

    public StaleDevicesException(StaleDevices staleDevices) {
        super(410);
        this.staleDevices = staleDevices;
    }

    public StaleDevices getStaleDevices() {
        return this.staleDevices;
    }
}
