package org.whispersystems.signalservice.internal.push;

import java.util.List;

/* loaded from: classes5.dex */
public class ContactTokenList {
    private List<String> contacts;

    public ContactTokenList(List<String> list) {
        this.contacts = list;
    }

    public ContactTokenList() {
    }

    public List<String> getContacts() {
        return this.contacts;
    }
}
