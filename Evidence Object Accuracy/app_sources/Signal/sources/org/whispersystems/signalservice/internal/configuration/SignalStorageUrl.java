package org.whispersystems.signalservice.internal.configuration;

import okhttp3.ConnectionSpec;
import org.whispersystems.signalservice.api.push.TrustStore;

/* loaded from: classes5.dex */
public class SignalStorageUrl extends SignalUrl {
    public SignalStorageUrl(String str, TrustStore trustStore) {
        super(str, trustStore);
    }

    public SignalStorageUrl(String str, String str2, TrustStore trustStore, ConnectionSpec connectionSpec) {
        super(str, str2, trustStore, connectionSpec);
    }
}
