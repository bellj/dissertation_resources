package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.List;

/* loaded from: classes.dex */
public class OutgoingPushMessageList {
    @JsonProperty
    private String destination;
    @JsonProperty
    private List<OutgoingPushMessage> messages;
    @JsonProperty
    private boolean online;
    @JsonProperty
    private long timestamp;

    public OutgoingPushMessageList(String str, long j, List<OutgoingPushMessage> list, boolean z) {
        this.timestamp = j;
        this.destination = str;
        this.messages = list;
        this.online = z;
    }

    public String getDestination() {
        return this.destination;
    }

    public List<OutgoingPushMessage> getMessages() {
        return this.messages;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public boolean isOnline() {
        return this.online;
    }

    @JsonIgnore
    public List<Integer> getDevices() {
        return (List) Collection$EL.stream(this.messages).map(new Function() { // from class: org.whispersystems.signalservice.internal.push.OutgoingPushMessageList$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Integer.valueOf(((OutgoingPushMessage) obj).getDestinationDeviceId());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }
}
