package org.whispersystems.signalservice.internal.serialize;

import j$.util.Optional;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.serialize.protos.AddressProto;

/* loaded from: classes5.dex */
public final class SignalServiceAddressProtobufSerializer {
    private SignalServiceAddressProtobufSerializer() {
    }

    public static AddressProto toProtobuf(SignalServiceAddress signalServiceAddress) {
        AddressProto.Builder newBuilder = AddressProto.newBuilder();
        newBuilder.setUuid(signalServiceAddress.getServiceId().toByteString());
        if (signalServiceAddress.getNumber().isPresent()) {
            newBuilder.setE164(signalServiceAddress.getNumber().get());
        }
        return newBuilder.build();
    }

    public static SignalServiceAddress fromProtobuf(AddressProto addressProto) {
        return new SignalServiceAddress(ServiceId.parseOrThrow(addressProto.getUuid().toByteArray()), addressProto.hasE164() ? Optional.of(addressProto.getE164()) : Optional.empty());
    }
}
