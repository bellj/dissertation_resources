package org.whispersystems.signalservice.internal.contacts.crypto;

/* loaded from: classes5.dex */
public class UnauthenticatedResponseException extends Exception {
    public UnauthenticatedResponseException(Exception exc) {
        super(exc);
    }

    public UnauthenticatedResponseException(String str) {
        super(str);
    }
}
