package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class PreKeyStatus {
    @JsonProperty
    private int count;

    public int getCount() {
        return this.count;
    }
}
