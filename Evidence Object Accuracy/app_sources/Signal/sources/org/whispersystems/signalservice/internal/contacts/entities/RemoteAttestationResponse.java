package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class RemoteAttestationResponse {
    @JsonProperty
    private String certificates;
    @JsonProperty
    private byte[] ciphertext;
    @JsonProperty
    private byte[] iv;
    @JsonProperty
    private byte[] quote;
    @JsonProperty
    private byte[] serverEphemeralPublic;
    @JsonProperty
    private byte[] serverStaticPublic;
    @JsonProperty
    private String signature;
    @JsonProperty
    private String signatureBody;
    @JsonProperty
    private byte[] tag;

    public RemoteAttestationResponse(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5, byte[] bArr6, String str, String str2, String str3) {
        this.serverEphemeralPublic = bArr;
        this.serverStaticPublic = bArr2;
        this.iv = bArr3;
        this.ciphertext = bArr4;
        this.tag = bArr5;
        this.quote = bArr6;
        this.signature = str;
        this.certificates = str2;
        this.signatureBody = str3;
    }

    public RemoteAttestationResponse() {
    }

    public byte[] getServerEphemeralPublic() {
        return this.serverEphemeralPublic;
    }

    public byte[] getServerStaticPublic() {
        return this.serverStaticPublic;
    }

    public byte[] getQuote() {
        return this.quote;
    }

    public byte[] getIv() {
        return this.iv;
    }

    public byte[] getCiphertext() {
        return this.ciphertext;
    }

    public byte[] getTag() {
        return this.tag;
    }

    public String getSignature() {
        return this.signature;
    }

    public String getCertificates() {
        return this.certificates;
    }

    public String getSignatureBody() {
        return this.signatureBody;
    }
}
