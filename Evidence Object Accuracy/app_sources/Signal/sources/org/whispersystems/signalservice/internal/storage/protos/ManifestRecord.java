package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes5.dex */
public final class ManifestRecord extends GeneratedMessageLite<ManifestRecord, Builder> implements ManifestRecordOrBuilder {
    private static final ManifestRecord DEFAULT_INSTANCE;
    public static final int IDENTIFIERS_FIELD_NUMBER;
    private static volatile Parser<ManifestRecord> PARSER;
    public static final int VERSION_FIELD_NUMBER;
    private Internal.ProtobufList<Identifier> identifiers_ = GeneratedMessageLite.emptyProtobufList();
    private long version_;

    /* loaded from: classes5.dex */
    public interface IdentifierOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getRaw();

        Identifier.Type getType();

        int getTypeValue();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private ManifestRecord() {
    }

    /* loaded from: classes5.dex */
    public static final class Identifier extends GeneratedMessageLite<Identifier, Builder> implements IdentifierOrBuilder {
        private static final Identifier DEFAULT_INSTANCE;
        private static volatile Parser<Identifier> PARSER;
        public static final int RAW_FIELD_NUMBER;
        public static final int TYPE_FIELD_NUMBER;
        private ByteString raw_ = ByteString.EMPTY;
        private int type_;

        private Identifier() {
        }

        /* loaded from: classes5.dex */
        public enum Type implements Internal.EnumLite {
            UNKNOWN(0),
            CONTACT(1),
            GROUPV1(2),
            GROUPV2(3),
            ACCOUNT(4),
            STORY_DISTRIBUTION_LIST(5),
            UNRECOGNIZED(-1);
            
            public static final int ACCOUNT_VALUE;
            public static final int CONTACT_VALUE;
            public static final int GROUPV1_VALUE;
            public static final int GROUPV2_VALUE;
            public static final int STORY_DISTRIBUTION_LIST_VALUE;
            public static final int UNKNOWN_VALUE;
            private static final Internal.EnumLiteMap<Type> internalValueMap = new Internal.EnumLiteMap<Type>() { // from class: org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.Identifier.Type.1
                @Override // com.google.protobuf.Internal.EnumLiteMap
                public Type findValueByNumber(int i) {
                    return Type.forNumber(i);
                }
            };
            private final int value;

            @Override // com.google.protobuf.Internal.EnumLite
            public final int getNumber() {
                if (this != UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }

            @Deprecated
            public static Type valueOf(int i) {
                return forNumber(i);
            }

            public static Type forNumber(int i) {
                if (i == 0) {
                    return UNKNOWN;
                }
                if (i == 1) {
                    return CONTACT;
                }
                if (i == 2) {
                    return GROUPV1;
                }
                if (i == 3) {
                    return GROUPV2;
                }
                if (i == 4) {
                    return ACCOUNT;
                }
                if (i != 5) {
                    return null;
                }
                return STORY_DISTRIBUTION_LIST;
            }

            public static Internal.EnumLiteMap<Type> internalGetValueMap() {
                return internalValueMap;
            }

            public static Internal.EnumVerifier internalGetVerifier() {
                return TypeVerifier.INSTANCE;
            }

            /* loaded from: classes5.dex */
            private static final class TypeVerifier implements Internal.EnumVerifier {
                static final Internal.EnumVerifier INSTANCE = new TypeVerifier();

                private TypeVerifier() {
                }

                @Override // com.google.protobuf.Internal.EnumVerifier
                public boolean isInRange(int i) {
                    return Type.forNumber(i) != null;
                }
            }

            Type(int i) {
                this.value = i;
            }
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.IdentifierOrBuilder
        public ByteString getRaw() {
            return this.raw_;
        }

        public void setRaw(ByteString byteString) {
            byteString.getClass();
            this.raw_ = byteString;
        }

        public void clearRaw() {
            this.raw_ = getDefaultInstance().getRaw();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.IdentifierOrBuilder
        public int getTypeValue() {
            return this.type_;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.IdentifierOrBuilder
        public Type getType() {
            Type forNumber = Type.forNumber(this.type_);
            return forNumber == null ? Type.UNRECOGNIZED : forNumber;
        }

        public void setTypeValue(int i) {
            this.type_ = i;
        }

        public void setType(Type type) {
            this.type_ = type.getNumber();
        }

        public void clearType() {
            this.type_ = 0;
        }

        public static Identifier parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Identifier parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Identifier parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Identifier parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Identifier parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Identifier parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Identifier parseFrom(InputStream inputStream) throws IOException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Identifier parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Identifier parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Identifier) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Identifier parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Identifier) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Identifier parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Identifier parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Identifier) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Identifier identifier) {
            return DEFAULT_INSTANCE.createBuilder(identifier);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Identifier, Builder> implements IdentifierOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Identifier.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.IdentifierOrBuilder
            public ByteString getRaw() {
                return ((Identifier) this.instance).getRaw();
            }

            public Builder setRaw(ByteString byteString) {
                copyOnWrite();
                ((Identifier) this.instance).setRaw(byteString);
                return this;
            }

            public Builder clearRaw() {
                copyOnWrite();
                ((Identifier) this.instance).clearRaw();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.IdentifierOrBuilder
            public int getTypeValue() {
                return ((Identifier) this.instance).getTypeValue();
            }

            public Builder setTypeValue(int i) {
                copyOnWrite();
                ((Identifier) this.instance).setTypeValue(i);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecord.IdentifierOrBuilder
            public Type getType() {
                return ((Identifier) this.instance).getType();
            }

            public Builder setType(Type type) {
                copyOnWrite();
                ((Identifier) this.instance).setType(type);
                return this;
            }

            public Builder clearType() {
                copyOnWrite();
                ((Identifier) this.instance).clearType();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Identifier();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\f", new Object[]{"raw_", "type_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Identifier> parser = PARSER;
                    if (parser == null) {
                        synchronized (Identifier.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Identifier identifier = new Identifier();
            DEFAULT_INSTANCE = identifier;
            GeneratedMessageLite.registerDefaultInstance(Identifier.class, identifier);
        }

        public static Identifier getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Identifier> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.ManifestRecord$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
    public long getVersion() {
        return this.version_;
    }

    public void setVersion(long j) {
        this.version_ = j;
    }

    public void clearVersion() {
        this.version_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
    public List<Identifier> getIdentifiersList() {
        return this.identifiers_;
    }

    public List<? extends IdentifierOrBuilder> getIdentifiersOrBuilderList() {
        return this.identifiers_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
    public int getIdentifiersCount() {
        return this.identifiers_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
    public Identifier getIdentifiers(int i) {
        return this.identifiers_.get(i);
    }

    public IdentifierOrBuilder getIdentifiersOrBuilder(int i) {
        return this.identifiers_.get(i);
    }

    private void ensureIdentifiersIsMutable() {
        Internal.ProtobufList<Identifier> protobufList = this.identifiers_;
        if (!protobufList.isModifiable()) {
            this.identifiers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setIdentifiers(int i, Identifier identifier) {
        identifier.getClass();
        ensureIdentifiersIsMutable();
        this.identifiers_.set(i, identifier);
    }

    public void addIdentifiers(Identifier identifier) {
        identifier.getClass();
        ensureIdentifiersIsMutable();
        this.identifiers_.add(identifier);
    }

    public void addIdentifiers(int i, Identifier identifier) {
        identifier.getClass();
        ensureIdentifiersIsMutable();
        this.identifiers_.add(i, identifier);
    }

    public void addAllIdentifiers(Iterable<? extends Identifier> iterable) {
        ensureIdentifiersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.identifiers_);
    }

    public void clearIdentifiers() {
        this.identifiers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeIdentifiers(int i) {
        ensureIdentifiersIsMutable();
        this.identifiers_.remove(i);
    }

    public static ManifestRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ManifestRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ManifestRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ManifestRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ManifestRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ManifestRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ManifestRecord parseFrom(InputStream inputStream) throws IOException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ManifestRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ManifestRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ManifestRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ManifestRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ManifestRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ManifestRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ManifestRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ManifestRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ManifestRecord manifestRecord) {
        return DEFAULT_INSTANCE.createBuilder(manifestRecord);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ManifestRecord, Builder> implements ManifestRecordOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ManifestRecord.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
        public long getVersion() {
            return ((ManifestRecord) this.instance).getVersion();
        }

        public Builder setVersion(long j) {
            copyOnWrite();
            ((ManifestRecord) this.instance).setVersion(j);
            return this;
        }

        public Builder clearVersion() {
            copyOnWrite();
            ((ManifestRecord) this.instance).clearVersion();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
        public List<Identifier> getIdentifiersList() {
            return Collections.unmodifiableList(((ManifestRecord) this.instance).getIdentifiersList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
        public int getIdentifiersCount() {
            return ((ManifestRecord) this.instance).getIdentifiersCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ManifestRecordOrBuilder
        public Identifier getIdentifiers(int i) {
            return ((ManifestRecord) this.instance).getIdentifiers(i);
        }

        public Builder setIdentifiers(int i, Identifier identifier) {
            copyOnWrite();
            ((ManifestRecord) this.instance).setIdentifiers(i, identifier);
            return this;
        }

        public Builder setIdentifiers(int i, Identifier.Builder builder) {
            copyOnWrite();
            ((ManifestRecord) this.instance).setIdentifiers(i, builder.build());
            return this;
        }

        public Builder addIdentifiers(Identifier identifier) {
            copyOnWrite();
            ((ManifestRecord) this.instance).addIdentifiers(identifier);
            return this;
        }

        public Builder addIdentifiers(int i, Identifier identifier) {
            copyOnWrite();
            ((ManifestRecord) this.instance).addIdentifiers(i, identifier);
            return this;
        }

        public Builder addIdentifiers(Identifier.Builder builder) {
            copyOnWrite();
            ((ManifestRecord) this.instance).addIdentifiers(builder.build());
            return this;
        }

        public Builder addIdentifiers(int i, Identifier.Builder builder) {
            copyOnWrite();
            ((ManifestRecord) this.instance).addIdentifiers(i, builder.build());
            return this;
        }

        public Builder addAllIdentifiers(Iterable<? extends Identifier> iterable) {
            copyOnWrite();
            ((ManifestRecord) this.instance).addAllIdentifiers(iterable);
            return this;
        }

        public Builder clearIdentifiers() {
            copyOnWrite();
            ((ManifestRecord) this.instance).clearIdentifiers();
            return this;
        }

        public Builder removeIdentifiers(int i) {
            copyOnWrite();
            ((ManifestRecord) this.instance).removeIdentifiers(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ManifestRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u0003\u0002\u001b", new Object[]{"version_", "identifiers_", Identifier.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ManifestRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (ManifestRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ManifestRecord manifestRecord = new ManifestRecord();
        DEFAULT_INSTANCE = manifestRecord;
        GeneratedMessageLite.registerDefaultInstance(ManifestRecord.class, manifestRecord);
    }

    public static ManifestRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ManifestRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
