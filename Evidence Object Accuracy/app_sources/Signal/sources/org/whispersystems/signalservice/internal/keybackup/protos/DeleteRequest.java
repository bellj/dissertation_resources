package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class DeleteRequest extends GeneratedMessageLite<DeleteRequest, Builder> implements DeleteRequestOrBuilder {
    public static final int BACKUP_ID_FIELD_NUMBER;
    private static final DeleteRequest DEFAULT_INSTANCE;
    private static volatile Parser<DeleteRequest> PARSER;
    public static final int SERVICE_ID_FIELD_NUMBER;
    private ByteString backupId_;
    private int bitField0_;
    private ByteString serviceId_;

    private DeleteRequest() {
        ByteString byteString = ByteString.EMPTY;
        this.serviceId_ = byteString;
        this.backupId_ = byteString;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
    public boolean hasServiceId() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
    public ByteString getServiceId() {
        return this.serviceId_;
    }

    public void setServiceId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 1;
        this.serviceId_ = byteString;
    }

    public void clearServiceId() {
        this.bitField0_ &= -2;
        this.serviceId_ = getDefaultInstance().getServiceId();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
    public boolean hasBackupId() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
    public ByteString getBackupId() {
        return this.backupId_;
    }

    public void setBackupId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 2;
        this.backupId_ = byteString;
    }

    public void clearBackupId() {
        this.bitField0_ &= -3;
        this.backupId_ = getDefaultInstance().getBackupId();
    }

    public static DeleteRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DeleteRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DeleteRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DeleteRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DeleteRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DeleteRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DeleteRequest parseFrom(InputStream inputStream) throws IOException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DeleteRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DeleteRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DeleteRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DeleteRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DeleteRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DeleteRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DeleteRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DeleteRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DeleteRequest deleteRequest) {
        return DEFAULT_INSTANCE.createBuilder(deleteRequest);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DeleteRequest, Builder> implements DeleteRequestOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DeleteRequest.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
        public boolean hasServiceId() {
            return ((DeleteRequest) this.instance).hasServiceId();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
        public ByteString getServiceId() {
            return ((DeleteRequest) this.instance).getServiceId();
        }

        public Builder setServiceId(ByteString byteString) {
            copyOnWrite();
            ((DeleteRequest) this.instance).setServiceId(byteString);
            return this;
        }

        public Builder clearServiceId() {
            copyOnWrite();
            ((DeleteRequest) this.instance).clearServiceId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
        public boolean hasBackupId() {
            return ((DeleteRequest) this.instance).hasBackupId();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequestOrBuilder
        public ByteString getBackupId() {
            return ((DeleteRequest) this.instance).getBackupId();
        }

        public Builder setBackupId(ByteString byteString) {
            copyOnWrite();
            ((DeleteRequest) this.instance).setBackupId(byteString);
            return this;
        }

        public Builder clearBackupId() {
            copyOnWrite();
            ((DeleteRequest) this.instance).clearBackupId();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequest$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DeleteRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001", new Object[]{"bitField0_", "serviceId_", "backupId_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DeleteRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (DeleteRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DeleteRequest deleteRequest = new DeleteRequest();
        DEFAULT_INSTANCE = deleteRequest;
        GeneratedMessageLite.registerDefaultInstance(DeleteRequest.class, deleteRequest);
    }

    public static DeleteRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DeleteRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
