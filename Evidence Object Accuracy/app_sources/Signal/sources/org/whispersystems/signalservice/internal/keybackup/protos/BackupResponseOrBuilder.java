package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.whispersystems.signalservice.internal.keybackup.protos.BackupResponse;

/* loaded from: classes5.dex */
public interface BackupResponseOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    BackupResponse.Status getStatus();

    ByteString getToken();

    boolean hasStatus();

    boolean hasToken();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
