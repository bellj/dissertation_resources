package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface MetadataProtoOrBuilder extends MessageLiteOrBuilder {
    AddressProto getAddress();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    String getDestinationUuid();

    ByteString getDestinationUuidBytes();

    ByteString getGroupId();

    boolean getNeedsReceipt();

    int getSenderDevice();

    long getServerDeliveredTimestamp();

    String getServerGuid();

    ByteString getServerGuidBytes();

    long getServerReceivedTimestamp();

    long getTimestamp();

    boolean hasAddress();

    boolean hasDestinationUuid();

    boolean hasGroupId();

    boolean hasNeedsReceipt();

    boolean hasSenderDevice();

    boolean hasServerDeliveredTimestamp();

    boolean hasServerGuid();

    boolean hasServerReceivedTimestamp();

    boolean hasTimestamp();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
