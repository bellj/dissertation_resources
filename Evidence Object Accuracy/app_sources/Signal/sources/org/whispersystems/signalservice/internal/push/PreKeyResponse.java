package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.List;
import org.signal.libsignal.protocol.IdentityKey;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes.dex */
public class PreKeyResponse {
    @JsonProperty
    private List<PreKeyResponseItem> devices;
    @JsonProperty
    @JsonDeserialize(using = JsonUtil.IdentityKeyDeserializer.class)
    @JsonSerialize(using = JsonUtil.IdentityKeySerializer.class)
    private IdentityKey identityKey;

    public IdentityKey getIdentityKey() {
        return this.identityKey;
    }

    public List<PreKeyResponseItem> getDevices() {
        return this.devices;
    }
}
