package org.whispersystems.signalservice.internal.contacts.crypto;

import com.squareup.okhttp.internal.http.StatusLine;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.thoughtcrime.securesms.R;

/* loaded from: classes5.dex */
public class Quote {
    private static final long SGX_FLAGS_DEBUG;
    private static final long SGX_FLAGS_EINITTOKEN_KEY;
    private static final long SGX_FLAGS_INITTED;
    private static final long SGX_FLAGS_MODE64BIT;
    private static final long SGX_FLAGS_PROVISION_KEY;
    private static final long SGX_FLAGS_RESERVED;
    private static final long SGX_XFRM_AVX;
    private static final long SGX_XFRM_LEGACY;
    private static final long SGX_XFRM_RESERVED;
    private final byte[] basename;
    private final byte[] cpuSvn;
    private final long flags;
    private final long gid;
    private final boolean isSigLinkable;
    private final int isvProdId;
    private final int isvSvn;
    private final byte[] mrenclave;
    private final byte[] mrsigner;
    private final int pceSvn;
    private final int qeSvn;
    private final byte[] quoteBytes;
    private final byte[] reportData;
    private final byte[] signature;
    private final int version;
    private final long xfrm;

    public Quote(byte[] bArr) throws InvalidQuoteFormatException {
        byte[] bArr2 = new byte[32];
        this.basename = bArr2;
        byte[] bArr3 = new byte[16];
        this.cpuSvn = bArr3;
        byte[] bArr4 = new byte[32];
        this.mrenclave = bArr4;
        byte[] bArr5 = new byte[32];
        this.mrsigner = bArr5;
        byte[] bArr6 = new byte[64];
        this.reportData = bArr6;
        this.quoteBytes = bArr;
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        wrap.order(ByteOrder.LITTLE_ENDIAN);
        int i = wrap.getShort(0) & 65535;
        this.version = i;
        if (i < 1 || i > 2) {
            throw new InvalidQuoteFormatException("unknown_quote_version " + i);
        }
        int i2 = wrap.getShort(2) & 65535;
        if ((i2 & -2) == 0) {
            this.isSigLinkable = i2 == 1;
            this.gid = (long) (wrap.getInt(4) & -1);
            this.qeSvn = wrap.getShort(8) & 65535;
            if (i > 1) {
                this.pceSvn = wrap.getShort(10) & 65535;
            } else {
                readZero(wrap, 10, 2);
                this.pceSvn = 0;
            }
            readZero(wrap, 12, 4);
            read(wrap, 16, bArr2);
            read(wrap, 48, bArr3);
            readZero(wrap, 64, 4);
            readZero(wrap, 68, 28);
            long j = wrap.getLong(96);
            this.flags = j;
            if ((SGX_FLAGS_RESERVED & j) != 0 || (1 & j) == 0 || (4 & j) == 0) {
                throw new InvalidQuoteFormatException("bad_quote_flags " + j);
            }
            long j2 = wrap.getLong(104);
            this.xfrm = j2;
            if ((SGX_XFRM_RESERVED & j2) == 0) {
                read(wrap, R.styleable.AppCompatTheme_tooltipForegroundColor, bArr4);
                readZero(wrap, 144, 32);
                read(wrap, 176, bArr5);
                readZero(wrap, 208, 96);
                this.isvProdId = wrap.getShort(304) & 65535;
                this.isvSvn = wrap.getShort(306) & 65535;
                readZero(wrap, StatusLine.HTTP_PERM_REDIRECT, 60);
                read(wrap, 368, bArr6);
                int i3 = wrap.getInt(432) & -1;
                if (i3 == bArr.length - 436) {
                    byte[] bArr7 = new byte[i3];
                    this.signature = bArr7;
                    read(wrap, 436, bArr7);
                    return;
                }
                throw new InvalidQuoteFormatException("bad_quote_sig_len " + i3);
            }
            throw new InvalidQuoteFormatException("bad_quote_xfrm " + j2);
        }
        throw new InvalidQuoteFormatException("unknown_quote_sign_type " + i2);
    }

    public byte[] getReportData() {
        return this.reportData;
    }

    private void read(ByteBuffer byteBuffer, int i, byte[] bArr) {
        byteBuffer.position(i);
        byteBuffer.get(bArr);
    }

    private void readZero(ByteBuffer byteBuffer, int i, int i2) {
        byte[] bArr = new byte[i2];
        read(byteBuffer, i, bArr);
        for (int i3 = 0; i3 < i2; i3++) {
            if (bArr[i3] != 0) {
                throw new IllegalArgumentException("quote_reserved_mismatch " + i);
            }
        }
    }

    public byte[] getQuoteBytes() {
        return this.quoteBytes;
    }

    public byte[] getMrenclave() {
        return this.mrenclave;
    }

    public boolean isDebugQuote() {
        return (this.flags & SGX_FLAGS_DEBUG) != 0;
    }

    /* loaded from: classes5.dex */
    public static class InvalidQuoteFormatException extends Exception {
        public InvalidQuoteFormatException(String str) {
            super(str);
        }
    }
}
