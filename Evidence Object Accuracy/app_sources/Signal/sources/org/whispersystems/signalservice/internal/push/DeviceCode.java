package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class DeviceCode {
    @JsonProperty
    private String verificationCode;

    public String getVerificationCode() {
        return this.verificationCode;
    }
}
