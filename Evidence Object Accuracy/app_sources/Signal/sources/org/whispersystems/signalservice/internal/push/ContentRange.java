package org.whispersystems.signalservice.internal.push;

import j$.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* loaded from: classes5.dex */
public final class ContentRange {
    private static final Pattern PATTERN = Pattern.compile("versions (\\d+)-(\\d+)\\/(\\d+)");
    private final int rangeEnd;
    private final int rangeStart;
    private final int totalSize;

    public static Optional<ContentRange> parse(String str) {
        if (str != null) {
            Matcher matcher = PATTERN.matcher(str);
            if (matcher.matches()) {
                return Optional.of(new ContentRange(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3))));
            }
        }
        return Optional.empty();
    }

    private ContentRange(int i, int i2, int i3) {
        this.rangeStart = i;
        this.rangeEnd = i2;
        this.totalSize = i3;
    }

    public int getRangeStart() {
        return this.rangeStart;
    }

    public int getRangeEnd() {
        return this.rangeEnd;
    }

    public int getTotalSize() {
        return this.totalSize;
    }
}
