package org.whispersystems.signalservice.internal.push;

import j$.util.function.Function;
import okhttp3.Response;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class PushServiceSocket$$ExternalSyntheticLambda3 implements Function {
    public final /* synthetic */ Response f$0;

    public /* synthetic */ PushServiceSocket$$ExternalSyntheticLambda3(Response response) {
        this.f$0 = response;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return this.f$0.header((String) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
