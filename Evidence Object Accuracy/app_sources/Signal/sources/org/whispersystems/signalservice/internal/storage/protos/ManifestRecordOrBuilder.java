package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.whispersystems.signalservice.internal.storage.protos.ManifestRecord;

/* loaded from: classes5.dex */
public interface ManifestRecordOrBuilder extends MessageLiteOrBuilder {
    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ManifestRecord.Identifier getIdentifiers(int i);

    int getIdentifiersCount();

    List<ManifestRecord.Identifier> getIdentifiersList();

    long getVersion();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
