package org.whispersystems.signalservice.internal.push;

import java.util.List;

/* loaded from: classes5.dex */
public final class SignalServiceMessagesResult {
    private final List<SignalServiceEnvelopeEntity> envelopes;
    private final long serverDeliveredTimestamp;

    public SignalServiceMessagesResult(List<SignalServiceEnvelopeEntity> list, long j) {
        this.envelopes = list;
        this.serverDeliveredTimestamp = j;
    }

    public List<SignalServiceEnvelopeEntity> getEnvelopes() {
        return this.envelopes;
    }

    public long getServerDeliveredTimestamp() {
        return this.serverDeliveredTimestamp;
    }
}
