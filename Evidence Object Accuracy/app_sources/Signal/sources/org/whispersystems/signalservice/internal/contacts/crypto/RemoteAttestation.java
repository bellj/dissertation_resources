package org.whispersystems.signalservice.internal.contacts.crypto;

import java.util.List;

/* loaded from: classes5.dex */
public class RemoteAttestation {
    private final List<String> cookies;
    private final RemoteAttestationKeys keys;
    private final byte[] requestId;

    public RemoteAttestation(byte[] bArr, RemoteAttestationKeys remoteAttestationKeys, List<String> list) {
        this.requestId = bArr;
        this.keys = remoteAttestationKeys;
        this.cookies = list;
    }

    public byte[] getRequestId() {
        return this.requestId;
    }

    public RemoteAttestationKeys getKeys() {
        return this.keys;
    }

    public List<String> getCookies() {
        return this.cookies;
    }
}
