package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class GroupMismatchedDevices {
    @JsonProperty
    private MismatchedDevices devices;
    @JsonProperty
    private String uuid;

    public String getUuid() {
        return this.uuid;
    }

    public MismatchedDevices getDevices() {
        return this.devices;
    }
}
