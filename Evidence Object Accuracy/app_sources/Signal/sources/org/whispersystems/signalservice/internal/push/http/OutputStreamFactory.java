package org.whispersystems.signalservice.internal.push.http;

import java.io.IOException;
import java.io.OutputStream;
import org.whispersystems.signalservice.api.crypto.DigestingOutputStream;

/* loaded from: classes5.dex */
public interface OutputStreamFactory {
    DigestingOutputStream createFor(OutputStream outputStream) throws IOException;
}
