package org.whispersystems.signalservice.internal.sticker;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes5.dex */
public final class StickerProtos {

    /* loaded from: classes5.dex */
    public interface PackOrBuilder extends MessageLiteOrBuilder {
        String getAuthor();

        ByteString getAuthorBytes();

        Pack.Sticker getCover();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        Pack.Sticker getStickers(int i);

        int getStickersCount();

        List<Pack.Sticker> getStickersList();

        String getTitle();

        ByteString getTitleBytes();

        boolean hasAuthor();

        boolean hasCover();

        boolean hasTitle();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }

    private StickerProtos() {
    }

    /* loaded from: classes5.dex */
    public static final class Pack extends GeneratedMessageLite<Pack, Builder> implements PackOrBuilder {
        public static final int AUTHOR_FIELD_NUMBER;
        public static final int COVER_FIELD_NUMBER;
        private static final Pack DEFAULT_INSTANCE;
        private static volatile Parser<Pack> PARSER;
        public static final int STICKERS_FIELD_NUMBER;
        public static final int TITLE_FIELD_NUMBER;
        private String author_ = "";
        private int bitField0_;
        private Sticker cover_;
        private Internal.ProtobufList<Sticker> stickers_ = GeneratedMessageLite.emptyProtobufList();
        private String title_ = "";

        /* loaded from: classes5.dex */
        public interface StickerOrBuilder extends MessageLiteOrBuilder {
            String getContentType();

            ByteString getContentTypeBytes();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ MessageLite getDefaultInstanceForType();

            String getEmoji();

            ByteString getEmojiBytes();

            int getId();

            boolean hasContentType();

            boolean hasEmoji();

            boolean hasId();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ boolean isInitialized();
        }

        private Pack() {
        }

        /* loaded from: classes5.dex */
        public static final class Sticker extends GeneratedMessageLite<Sticker, Builder> implements StickerOrBuilder {
            public static final int CONTENTTYPE_FIELD_NUMBER;
            private static final Sticker DEFAULT_INSTANCE;
            public static final int EMOJI_FIELD_NUMBER;
            public static final int ID_FIELD_NUMBER;
            private static volatile Parser<Sticker> PARSER;
            private int bitField0_;
            private String contentType_ = "";
            private String emoji_ = "";
            private int id_;

            private Sticker() {
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public boolean hasId() {
                return (this.bitField0_ & 1) != 0;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public int getId() {
                return this.id_;
            }

            public void setId(int i) {
                this.bitField0_ |= 1;
                this.id_ = i;
            }

            public void clearId() {
                this.bitField0_ &= -2;
                this.id_ = 0;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public boolean hasEmoji() {
                return (this.bitField0_ & 2) != 0;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public String getEmoji() {
                return this.emoji_;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public ByteString getEmojiBytes() {
                return ByteString.copyFromUtf8(this.emoji_);
            }

            public void setEmoji(String str) {
                str.getClass();
                this.bitField0_ |= 2;
                this.emoji_ = str;
            }

            public void clearEmoji() {
                this.bitField0_ &= -3;
                this.emoji_ = getDefaultInstance().getEmoji();
            }

            public void setEmojiBytes(ByteString byteString) {
                this.emoji_ = byteString.toStringUtf8();
                this.bitField0_ |= 2;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public boolean hasContentType() {
                return (this.bitField0_ & 4) != 0;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public String getContentType() {
                return this.contentType_;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
            public ByteString getContentTypeBytes() {
                return ByteString.copyFromUtf8(this.contentType_);
            }

            public void setContentType(String str) {
                str.getClass();
                this.bitField0_ |= 4;
                this.contentType_ = str;
            }

            public void clearContentType() {
                this.bitField0_ &= -5;
                this.contentType_ = getDefaultInstance().getContentType();
            }

            public void setContentTypeBytes(ByteString byteString) {
                this.contentType_ = byteString.toStringUtf8();
                this.bitField0_ |= 4;
            }

            public static Sticker parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static Sticker parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static Sticker parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static Sticker parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static Sticker parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static Sticker parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static Sticker parseFrom(InputStream inputStream) throws IOException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static Sticker parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static Sticker parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Sticker) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static Sticker parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Sticker) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static Sticker parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static Sticker parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(Sticker sticker) {
                return DEFAULT_INSTANCE.createBuilder(sticker);
            }

            /* loaded from: classes5.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<Sticker, Builder> implements StickerOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(Sticker.DEFAULT_INSTANCE);
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public boolean hasId() {
                    return ((Sticker) this.instance).hasId();
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public int getId() {
                    return ((Sticker) this.instance).getId();
                }

                public Builder setId(int i) {
                    copyOnWrite();
                    ((Sticker) this.instance).setId(i);
                    return this;
                }

                public Builder clearId() {
                    copyOnWrite();
                    ((Sticker) this.instance).clearId();
                    return this;
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public boolean hasEmoji() {
                    return ((Sticker) this.instance).hasEmoji();
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public String getEmoji() {
                    return ((Sticker) this.instance).getEmoji();
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public ByteString getEmojiBytes() {
                    return ((Sticker) this.instance).getEmojiBytes();
                }

                public Builder setEmoji(String str) {
                    copyOnWrite();
                    ((Sticker) this.instance).setEmoji(str);
                    return this;
                }

                public Builder clearEmoji() {
                    copyOnWrite();
                    ((Sticker) this.instance).clearEmoji();
                    return this;
                }

                public Builder setEmojiBytes(ByteString byteString) {
                    copyOnWrite();
                    ((Sticker) this.instance).setEmojiBytes(byteString);
                    return this;
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public boolean hasContentType() {
                    return ((Sticker) this.instance).hasContentType();
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public String getContentType() {
                    return ((Sticker) this.instance).getContentType();
                }

                @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.Pack.StickerOrBuilder
                public ByteString getContentTypeBytes() {
                    return ((Sticker) this.instance).getContentTypeBytes();
                }

                public Builder setContentType(String str) {
                    copyOnWrite();
                    ((Sticker) this.instance).setContentType(str);
                    return this;
                }

                public Builder clearContentType() {
                    copyOnWrite();
                    ((Sticker) this.instance).clearContentType();
                    return this;
                }

                public Builder setContentTypeBytes(ByteString byteString) {
                    copyOnWrite();
                    ((Sticker) this.instance).setContentTypeBytes(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new Sticker();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဋ\u0000\u0002ဈ\u0001\u0003ဈ\u0002", new Object[]{"bitField0_", "id_", "emoji_", "contentType_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<Sticker> parser = PARSER;
                        if (parser == null) {
                            synchronized (Sticker.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                Sticker sticker = new Sticker();
                DEFAULT_INSTANCE = sticker;
                GeneratedMessageLite.registerDefaultInstance(Sticker.class, sticker);
            }

            public static Sticker getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<Sticker> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public boolean hasTitle() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public String getTitle() {
            return this.title_;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public ByteString getTitleBytes() {
            return ByteString.copyFromUtf8(this.title_);
        }

        public void setTitle(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.title_ = str;
        }

        public void clearTitle() {
            this.bitField0_ &= -2;
            this.title_ = getDefaultInstance().getTitle();
        }

        public void setTitleBytes(ByteString byteString) {
            this.title_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public boolean hasAuthor() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public String getAuthor() {
            return this.author_;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public ByteString getAuthorBytes() {
            return ByteString.copyFromUtf8(this.author_);
        }

        public void setAuthor(String str) {
            str.getClass();
            this.bitField0_ |= 2;
            this.author_ = str;
        }

        public void clearAuthor() {
            this.bitField0_ &= -3;
            this.author_ = getDefaultInstance().getAuthor();
        }

        public void setAuthorBytes(ByteString byteString) {
            this.author_ = byteString.toStringUtf8();
            this.bitField0_ |= 2;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public boolean hasCover() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public Sticker getCover() {
            Sticker sticker = this.cover_;
            return sticker == null ? Sticker.getDefaultInstance() : sticker;
        }

        public void setCover(Sticker sticker) {
            sticker.getClass();
            this.cover_ = sticker;
            this.bitField0_ |= 4;
        }

        public void mergeCover(Sticker sticker) {
            sticker.getClass();
            Sticker sticker2 = this.cover_;
            if (sticker2 == null || sticker2 == Sticker.getDefaultInstance()) {
                this.cover_ = sticker;
            } else {
                this.cover_ = Sticker.newBuilder(this.cover_).mergeFrom((Sticker.Builder) sticker).buildPartial();
            }
            this.bitField0_ |= 4;
        }

        public void clearCover() {
            this.cover_ = null;
            this.bitField0_ &= -5;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public List<Sticker> getStickersList() {
            return this.stickers_;
        }

        public List<? extends StickerOrBuilder> getStickersOrBuilderList() {
            return this.stickers_;
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public int getStickersCount() {
            return this.stickers_.size();
        }

        @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
        public Sticker getStickers(int i) {
            return this.stickers_.get(i);
        }

        public StickerOrBuilder getStickersOrBuilder(int i) {
            return this.stickers_.get(i);
        }

        private void ensureStickersIsMutable() {
            Internal.ProtobufList<Sticker> protobufList = this.stickers_;
            if (!protobufList.isModifiable()) {
                this.stickers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setStickers(int i, Sticker sticker) {
            sticker.getClass();
            ensureStickersIsMutable();
            this.stickers_.set(i, sticker);
        }

        public void addStickers(Sticker sticker) {
            sticker.getClass();
            ensureStickersIsMutable();
            this.stickers_.add(sticker);
        }

        public void addStickers(int i, Sticker sticker) {
            sticker.getClass();
            ensureStickersIsMutable();
            this.stickers_.add(i, sticker);
        }

        public void addAllStickers(Iterable<? extends Sticker> iterable) {
            ensureStickersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.stickers_);
        }

        public void clearStickers() {
            this.stickers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeStickers(int i) {
            ensureStickersIsMutable();
            this.stickers_.remove(i);
        }

        public static Pack parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Pack parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Pack parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Pack parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Pack parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Pack parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Pack parseFrom(InputStream inputStream) throws IOException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Pack parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Pack parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Pack) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Pack parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Pack) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Pack parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Pack parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Pack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Pack pack) {
            return DEFAULT_INSTANCE.createBuilder(pack);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Pack, Builder> implements PackOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Pack.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public boolean hasTitle() {
                return ((Pack) this.instance).hasTitle();
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public String getTitle() {
                return ((Pack) this.instance).getTitle();
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public ByteString getTitleBytes() {
                return ((Pack) this.instance).getTitleBytes();
            }

            public Builder setTitle(String str) {
                copyOnWrite();
                ((Pack) this.instance).setTitle(str);
                return this;
            }

            public Builder clearTitle() {
                copyOnWrite();
                ((Pack) this.instance).clearTitle();
                return this;
            }

            public Builder setTitleBytes(ByteString byteString) {
                copyOnWrite();
                ((Pack) this.instance).setTitleBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public boolean hasAuthor() {
                return ((Pack) this.instance).hasAuthor();
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public String getAuthor() {
                return ((Pack) this.instance).getAuthor();
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public ByteString getAuthorBytes() {
                return ((Pack) this.instance).getAuthorBytes();
            }

            public Builder setAuthor(String str) {
                copyOnWrite();
                ((Pack) this.instance).setAuthor(str);
                return this;
            }

            public Builder clearAuthor() {
                copyOnWrite();
                ((Pack) this.instance).clearAuthor();
                return this;
            }

            public Builder setAuthorBytes(ByteString byteString) {
                copyOnWrite();
                ((Pack) this.instance).setAuthorBytes(byteString);
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public boolean hasCover() {
                return ((Pack) this.instance).hasCover();
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public Sticker getCover() {
                return ((Pack) this.instance).getCover();
            }

            public Builder setCover(Sticker sticker) {
                copyOnWrite();
                ((Pack) this.instance).setCover(sticker);
                return this;
            }

            public Builder setCover(Sticker.Builder builder) {
                copyOnWrite();
                ((Pack) this.instance).setCover(builder.build());
                return this;
            }

            public Builder mergeCover(Sticker sticker) {
                copyOnWrite();
                ((Pack) this.instance).mergeCover(sticker);
                return this;
            }

            public Builder clearCover() {
                copyOnWrite();
                ((Pack) this.instance).clearCover();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public List<Sticker> getStickersList() {
                return Collections.unmodifiableList(((Pack) this.instance).getStickersList());
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public int getStickersCount() {
                return ((Pack) this.instance).getStickersCount();
            }

            @Override // org.whispersystems.signalservice.internal.sticker.StickerProtos.PackOrBuilder
            public Sticker getStickers(int i) {
                return ((Pack) this.instance).getStickers(i);
            }

            public Builder setStickers(int i, Sticker sticker) {
                copyOnWrite();
                ((Pack) this.instance).setStickers(i, sticker);
                return this;
            }

            public Builder setStickers(int i, Sticker.Builder builder) {
                copyOnWrite();
                ((Pack) this.instance).setStickers(i, builder.build());
                return this;
            }

            public Builder addStickers(Sticker sticker) {
                copyOnWrite();
                ((Pack) this.instance).addStickers(sticker);
                return this;
            }

            public Builder addStickers(int i, Sticker sticker) {
                copyOnWrite();
                ((Pack) this.instance).addStickers(i, sticker);
                return this;
            }

            public Builder addStickers(Sticker.Builder builder) {
                copyOnWrite();
                ((Pack) this.instance).addStickers(builder.build());
                return this;
            }

            public Builder addStickers(int i, Sticker.Builder builder) {
                copyOnWrite();
                ((Pack) this.instance).addStickers(i, builder.build());
                return this;
            }

            public Builder addAllStickers(Iterable<? extends Sticker> iterable) {
                copyOnWrite();
                ((Pack) this.instance).addAllStickers(iterable);
                return this;
            }

            public Builder clearStickers() {
                copyOnWrite();
                ((Pack) this.instance).clearStickers();
                return this;
            }

            public Builder removeStickers(int i) {
                copyOnWrite();
                ((Pack) this.instance).removeStickers(i);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Pack();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဉ\u0002\u0004\u001b", new Object[]{"bitField0_", "title_", "author_", "cover_", "stickers_", Sticker.class});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Pack> parser = PARSER;
                    if (parser == null) {
                        synchronized (Pack.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Pack pack = new Pack();
            DEFAULT_INSTANCE = pack;
            GeneratedMessageLite.registerDefaultInstance(Pack.class, pack);
        }

        public static Pack getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Pack> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.sticker.StickerProtos$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }
}
