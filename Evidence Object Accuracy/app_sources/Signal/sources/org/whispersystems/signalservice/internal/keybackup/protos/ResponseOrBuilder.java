package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface ResponseOrBuilder extends MessageLiteOrBuilder {
    BackupResponse getBackup();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    DeleteResponse getDelete();

    RestoreResponse getRestore();

    boolean hasBackup();

    boolean hasDelete();

    boolean hasRestore();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
