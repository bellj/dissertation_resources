package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.whispersystems.signalservice.api.push.ContactTokenDetails;

/* loaded from: classes.dex */
public class ContactTokenDetailsList {
    @JsonProperty
    private List<ContactTokenDetails> contacts;

    public List<ContactTokenDetails> getContacts() {
        return this.contacts;
    }
}
