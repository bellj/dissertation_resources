package org.whispersystems.signalservice.internal.configuration;

import j$.util.Optional;
import java.util.List;
import java.util.Map;
import okhttp3.Dns;
import okhttp3.Interceptor;

/* loaded from: classes5.dex */
public final class SignalServiceConfiguration {
    private final Optional<Dns> dns;
    private final List<Interceptor> networkInterceptors;
    private final Optional<SignalProxy> proxy;
    private final Map<Integer, SignalCdnUrl[]> signalCdnUrlMap;
    private final SignalCdsiUrl[] signalCdsiUrls;
    private final SignalContactDiscoveryUrl[] signalContactDiscoveryUrls;
    private final SignalKeyBackupServiceUrl[] signalKeyBackupServiceUrls;
    private final SignalServiceUrl[] signalServiceUrls;
    private final SignalStorageUrl[] signalStorageUrls;
    private final byte[] zkGroupServerPublicParams;

    public SignalServiceConfiguration(SignalServiceUrl[] signalServiceUrlArr, Map<Integer, SignalCdnUrl[]> map, SignalContactDiscoveryUrl[] signalContactDiscoveryUrlArr, SignalKeyBackupServiceUrl[] signalKeyBackupServiceUrlArr, SignalStorageUrl[] signalStorageUrlArr, SignalCdsiUrl[] signalCdsiUrlArr, List<Interceptor> list, Optional<Dns> optional, Optional<SignalProxy> optional2, byte[] bArr) {
        this.signalServiceUrls = signalServiceUrlArr;
        this.signalCdnUrlMap = map;
        this.signalContactDiscoveryUrls = signalContactDiscoveryUrlArr;
        this.signalCdsiUrls = signalCdsiUrlArr;
        this.signalKeyBackupServiceUrls = signalKeyBackupServiceUrlArr;
        this.signalStorageUrls = signalStorageUrlArr;
        this.networkInterceptors = list;
        this.dns = optional;
        this.proxy = optional2;
        this.zkGroupServerPublicParams = bArr;
    }

    public SignalServiceUrl[] getSignalServiceUrls() {
        return this.signalServiceUrls;
    }

    public Map<Integer, SignalCdnUrl[]> getSignalCdnUrlMap() {
        return this.signalCdnUrlMap;
    }

    public SignalContactDiscoveryUrl[] getSignalContactDiscoveryUrls() {
        return this.signalContactDiscoveryUrls;
    }

    public SignalCdsiUrl[] getSignalCdsiUrls() {
        return this.signalCdsiUrls;
    }

    public SignalKeyBackupServiceUrl[] getSignalKeyBackupServiceUrls() {
        return this.signalKeyBackupServiceUrls;
    }

    public SignalStorageUrl[] getSignalStorageUrls() {
        return this.signalStorageUrls;
    }

    public List<Interceptor> getNetworkInterceptors() {
        return this.networkInterceptors;
    }

    public Optional<Dns> getDns() {
        return this.dns;
    }

    public byte[] getZkGroupServerPublicParams() {
        return this.zkGroupServerPublicParams;
    }

    public Optional<SignalProxy> getSignalProxy() {
        return this.proxy;
    }
}
