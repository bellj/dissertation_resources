package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface RestoreRequestOrBuilder extends MessageLiteOrBuilder {
    ByteString getBackupId();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ByteString getPin();

    ByteString getServiceId();

    ByteString getToken();

    long getValidFrom();

    boolean hasBackupId();

    boolean hasPin();

    boolean hasServiceId();

    boolean hasToken();

    boolean hasValidFrom();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
