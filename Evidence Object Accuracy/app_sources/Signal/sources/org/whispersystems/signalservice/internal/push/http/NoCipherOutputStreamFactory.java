package org.whispersystems.signalservice.internal.push.http;

import java.io.OutputStream;
import org.whispersystems.signalservice.api.crypto.DigestingOutputStream;
import org.whispersystems.signalservice.api.crypto.NoCipherOutputStream;

/* loaded from: classes5.dex */
public final class NoCipherOutputStreamFactory implements OutputStreamFactory {
    @Override // org.whispersystems.signalservice.internal.push.http.OutputStreamFactory
    public DigestingOutputStream createFor(OutputStream outputStream) {
        return new NoCipherOutputStream(outputStream);
    }
}
