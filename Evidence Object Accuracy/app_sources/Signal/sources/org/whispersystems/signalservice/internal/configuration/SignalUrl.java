package org.whispersystems.signalservice.internal.configuration;

import j$.util.Optional;
import java.util.Collections;
import java.util.List;
import okhttp3.ConnectionSpec;
import org.whispersystems.signalservice.api.push.TrustStore;

/* loaded from: classes5.dex */
public class SignalUrl {
    private final Optional<ConnectionSpec> connectionSpec;
    private final Optional<String> hostHeader;
    private TrustStore trustStore;
    private final String url;

    public SignalUrl(String str, TrustStore trustStore) {
        this(str, null, trustStore, null);
    }

    public SignalUrl(String str, String str2, TrustStore trustStore, ConnectionSpec connectionSpec) {
        this.url = str;
        this.hostHeader = Optional.ofNullable(str2);
        this.trustStore = trustStore;
        this.connectionSpec = Optional.ofNullable(connectionSpec);
    }

    public Optional<String> getHostHeader() {
        return this.hostHeader;
    }

    public String getUrl() {
        return this.url;
    }

    public TrustStore getTrustStore() {
        return this.trustStore;
    }

    public Optional<List<ConnectionSpec>> getConnectionSpecs() {
        return this.connectionSpec.isPresent() ? Optional.of(Collections.singletonList(this.connectionSpec.get())) : Optional.empty();
    }
}
