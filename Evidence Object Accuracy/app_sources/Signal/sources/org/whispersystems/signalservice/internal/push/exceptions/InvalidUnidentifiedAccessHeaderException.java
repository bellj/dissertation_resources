package org.whispersystems.signalservice.internal.push.exceptions;

import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes5.dex */
public class InvalidUnidentifiedAccessHeaderException extends NonSuccessfulResponseCodeException {
    public InvalidUnidentifiedAccessHeaderException() {
        super(401);
    }
}
