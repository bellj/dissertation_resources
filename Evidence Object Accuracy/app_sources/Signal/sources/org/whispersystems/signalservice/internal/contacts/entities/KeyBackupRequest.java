package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whispersystems.signalservice.internal.util.Hex;

/* loaded from: classes.dex */
public class KeyBackupRequest {
    @JsonProperty
    private byte[] data;
    @JsonProperty
    private byte[] iv;
    @JsonProperty
    private byte[] mac;
    @JsonProperty
    private byte[] requestId;
    @JsonProperty
    private String type;

    public KeyBackupRequest() {
    }

    public KeyBackupRequest(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, String str) {
        this.requestId = bArr;
        this.iv = bArr2;
        this.data = bArr3;
        this.mac = bArr4;
        this.type = str;
    }

    public byte[] getRequestId() {
        return this.requestId;
    }

    public byte[] getIv() {
        return this.iv;
    }

    public byte[] getData() {
        return this.data;
    }

    public byte[] getMac() {
        return this.mac;
    }

    public String getType() {
        return this.type;
    }

    public String toString() {
        return "{ type:" + this.type + ", requestId: " + Hex.toString(this.requestId) + ", iv: " + Hex.toString(this.iv) + ", data: " + Hex.toString(this.data) + ", mac: " + Hex.toString(this.mac) + "}";
    }
}
