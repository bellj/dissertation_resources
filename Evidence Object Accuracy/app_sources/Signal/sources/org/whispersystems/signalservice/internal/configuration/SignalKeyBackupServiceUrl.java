package org.whispersystems.signalservice.internal.configuration;

import okhttp3.ConnectionSpec;
import org.whispersystems.signalservice.api.push.TrustStore;

/* loaded from: classes5.dex */
public class SignalKeyBackupServiceUrl extends SignalUrl {
    public SignalKeyBackupServiceUrl(String str, TrustStore trustStore) {
        super(str, trustStore);
    }

    public SignalKeyBackupServiceUrl(String str, String str2, TrustStore trustStore, ConnectionSpec connectionSpec) {
        super(str, str2, trustStore, connectionSpec);
    }
}
