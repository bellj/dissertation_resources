package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.whispersystems.signalservice.internal.storage.protos.StorageRecord;

/* loaded from: classes5.dex */
public interface StorageRecordOrBuilder extends MessageLiteOrBuilder {
    AccountRecord getAccount();

    ContactRecord getContact();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    GroupV1Record getGroupV1();

    GroupV2Record getGroupV2();

    StorageRecord.RecordCase getRecordCase();

    StoryDistributionListRecord getStoryDistributionList();

    boolean hasAccount();

    boolean hasContact();

    boolean hasGroupV1();

    boolean hasGroupV2();

    boolean hasStoryDistributionList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
