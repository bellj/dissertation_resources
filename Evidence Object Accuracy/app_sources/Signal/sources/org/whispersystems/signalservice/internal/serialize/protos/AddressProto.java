package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class AddressProto extends GeneratedMessageLite<AddressProto, Builder> implements AddressProtoOrBuilder {
    private static final AddressProto DEFAULT_INSTANCE;
    public static final int E164_FIELD_NUMBER;
    private static volatile Parser<AddressProto> PARSER;
    public static final int UUID_FIELD_NUMBER;
    private int bitField0_;
    private String e164_ = "";
    private ByteString uuid_ = ByteString.EMPTY;

    private AddressProto() {
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
    public boolean hasUuid() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
    public ByteString getUuid() {
        return this.uuid_;
    }

    public void setUuid(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 1;
        this.uuid_ = byteString;
    }

    public void clearUuid() {
        this.bitField0_ &= -2;
        this.uuid_ = getDefaultInstance().getUuid();
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
    public boolean hasE164() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
    public String getE164() {
        return this.e164_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
    public ByteString getE164Bytes() {
        return ByteString.copyFromUtf8(this.e164_);
    }

    public void setE164(String str) {
        str.getClass();
        this.bitField0_ |= 2;
        this.e164_ = str;
    }

    public void clearE164() {
        this.bitField0_ &= -3;
        this.e164_ = getDefaultInstance().getE164();
    }

    public void setE164Bytes(ByteString byteString) {
        this.e164_ = byteString.toStringUtf8();
        this.bitField0_ |= 2;
    }

    public static AddressProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static AddressProto parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static AddressProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static AddressProto parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static AddressProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static AddressProto parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static AddressProto parseFrom(InputStream inputStream) throws IOException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AddressProto parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AddressProto parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (AddressProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AddressProto parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AddressProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AddressProto parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static AddressProto parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AddressProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(AddressProto addressProto) {
        return DEFAULT_INSTANCE.createBuilder(addressProto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<AddressProto, Builder> implements AddressProtoOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(AddressProto.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
        public boolean hasUuid() {
            return ((AddressProto) this.instance).hasUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
        public ByteString getUuid() {
            return ((AddressProto) this.instance).getUuid();
        }

        public Builder setUuid(ByteString byteString) {
            copyOnWrite();
            ((AddressProto) this.instance).setUuid(byteString);
            return this;
        }

        public Builder clearUuid() {
            copyOnWrite();
            ((AddressProto) this.instance).clearUuid();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
        public boolean hasE164() {
            return ((AddressProto) this.instance).hasE164();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
        public String getE164() {
            return ((AddressProto) this.instance).getE164();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.AddressProtoOrBuilder
        public ByteString getE164Bytes() {
            return ((AddressProto) this.instance).getE164Bytes();
        }

        public Builder setE164(String str) {
            copyOnWrite();
            ((AddressProto) this.instance).setE164(str);
            return this;
        }

        public Builder clearE164() {
            copyOnWrite();
            ((AddressProto) this.instance).clearE164();
            return this;
        }

        public Builder setE164Bytes(ByteString byteString) {
            copyOnWrite();
            ((AddressProto) this.instance).setE164Bytes(byteString);
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.serialize.protos.AddressProto$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new AddressProto();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ည\u0000\u0002ဈ\u0001", new Object[]{"bitField0_", "uuid_", "e164_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<AddressProto> parser = PARSER;
                if (parser == null) {
                    synchronized (AddressProto.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        AddressProto addressProto = new AddressProto();
        DEFAULT_INSTANCE = addressProto;
        GeneratedMessageLite.registerDefaultInstance(AddressProto.class, addressProto);
    }

    public static AddressProto getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<AddressProto> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
