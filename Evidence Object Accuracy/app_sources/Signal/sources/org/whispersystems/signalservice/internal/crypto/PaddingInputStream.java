package org.whispersystems.signalservice.internal.crypto;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class PaddingInputStream extends FilterInputStream {
    private long paddingRemaining;

    public PaddingInputStream(InputStream inputStream, long j) {
        super(inputStream);
        this.paddingRemaining = getPaddedSize(j) - j;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        int read = super.read();
        if (read != -1) {
            return read;
        }
        long j = this.paddingRemaining;
        if (j <= 0) {
            return -1;
        }
        this.paddingRemaining = j - 1;
        return 0;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = super.read(bArr, i, i2);
        if (read != -1) {
            return read;
        }
        long j = this.paddingRemaining;
        if (j <= 0) {
            return -1;
        }
        int min = Math.min(i2, Util.toIntExact(j));
        this.paddingRemaining -= (long) min;
        return min;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() throws IOException {
        return super.available() + Util.toIntExact(this.paddingRemaining);
    }

    public static long getPaddedSize(long j) {
        return (long) ((int) Math.max(541.0d, Math.floor(Math.pow(1.05d, Math.ceil(Math.log((double) j) / Math.log(1.05d))))));
    }
}
