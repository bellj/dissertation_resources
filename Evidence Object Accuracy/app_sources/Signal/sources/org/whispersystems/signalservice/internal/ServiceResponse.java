package org.whispersystems.signalservice.internal;

import io.reactivex.rxjava3.core.Single;
import j$.util.Optional;
import java.util.concurrent.ExecutionException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.util.Preconditions;
import org.whispersystems.signalservice.internal.websocket.WebsocketResponse;

/* loaded from: classes5.dex */
public final class ServiceResponse<Result> {
    private final Optional<Throwable> applicationError;
    private final Optional<String> body;
    private final Optional<Throwable> executionError;
    private final Optional<Result> result;
    private final int status;

    private ServiceResponse(Result result, WebsocketResponse websocketResponse) {
        this(websocketResponse.getStatus(), websocketResponse.getBody(), result, null, null);
    }

    private ServiceResponse(Throwable th, WebsocketResponse websocketResponse) {
        this(websocketResponse.getStatus(), websocketResponse.getBody(), null, th, null);
    }

    public ServiceResponse(int i, String str, Result result, Throwable th, Throwable th2) {
        boolean z = false;
        if (result != null) {
            if (th == null && th2 == null) {
                z = true;
            }
            Preconditions.checkArgument(z);
        } else {
            Preconditions.checkArgument((th == null && th2 == null) ? z : true);
        }
        this.status = i;
        this.body = Optional.ofNullable(str);
        this.result = Optional.ofNullable(result);
        this.applicationError = Optional.ofNullable(th);
        this.executionError = Optional.ofNullable(th2);
    }

    public int getStatus() {
        return this.status;
    }

    public Optional<String> getBody() {
        return this.body;
    }

    public Optional<Result> getResult() {
        return this.result;
    }

    public Optional<Throwable> getApplicationError() {
        return this.applicationError;
    }

    public Optional<Throwable> getExecutionError() {
        return this.executionError;
    }

    public Single<Result> flattenResult() {
        if (this.result.isPresent()) {
            return Single.just(this.result.get());
        }
        if (this.applicationError.isPresent()) {
            return Single.error(this.applicationError.get());
        }
        if (this.executionError.isPresent()) {
            return Single.error(this.executionError.get());
        }
        return Single.error(new AssertionError("Should never get here."));
    }

    public static <T> ServiceResponse<T> forResult(T t, WebsocketResponse websocketResponse) {
        return new ServiceResponse<>(t, websocketResponse);
    }

    public static <T> ServiceResponse<T> forResult(T t, int i, String str) {
        return new ServiceResponse<>(i, str, t, null, null);
    }

    public static <T> ServiceResponse<T> forApplicationError(Throwable th, WebsocketResponse websocketResponse) {
        return new ServiceResponse<>(th, websocketResponse);
    }

    public static <T> ServiceResponse<T> forApplicationError(Throwable th, int i, String str) {
        return new ServiceResponse<>(i, str, null, th, null);
    }

    public static <T> ServiceResponse<T> forExecutionError(Throwable th) {
        return new ServiceResponse<>(0, null, null, null, th);
    }

    public static <T> ServiceResponse<T> forUnknownError(Throwable th) {
        if (th instanceof ExecutionException) {
            return forUnknownError(th.getCause());
        }
        if (th instanceof NonSuccessfulResponseCodeException) {
            return forApplicationError(th, ((NonSuccessfulResponseCodeException) th).getCode(), null);
        }
        if (!(th instanceof PushNetworkException) || th.getCause() == null) {
            return forExecutionError(th);
        }
        return forUnknownError(th.getCause());
    }

    public static <T, I> ServiceResponse<T> coerceError(ServiceResponse<I> serviceResponse) {
        if (((ServiceResponse) serviceResponse).applicationError.isPresent()) {
            return forApplicationError(((ServiceResponse) serviceResponse).applicationError.get(), ((ServiceResponse) serviceResponse).status, ((ServiceResponse) serviceResponse).body.orElse(null));
        }
        return forExecutionError(((ServiceResponse) serviceResponse).executionError.orElse(null));
    }
}
