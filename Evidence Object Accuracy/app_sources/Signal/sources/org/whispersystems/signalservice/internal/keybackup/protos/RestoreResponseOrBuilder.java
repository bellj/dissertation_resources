package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponse;

/* loaded from: classes5.dex */
public interface RestoreResponseOrBuilder extends MessageLiteOrBuilder {
    ByteString getData();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    RestoreResponse.Status getStatus();

    ByteString getToken();

    int getTries();

    boolean hasData();

    boolean hasStatus();

    boolean hasToken();

    boolean hasTries();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
