package org.whispersystems.signalservice.internal.push;

import java.util.List;

/* loaded from: classes5.dex */
public class SignalServiceEnvelopeEntityList {
    private List<SignalServiceEnvelopeEntity> messages;

    public List<SignalServiceEnvelopeEntity> getMessages() {
        return this.messages;
    }
}
