package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class ProvisioningMessage {
    @JsonProperty
    private String body;

    public ProvisioningMessage(String str) {
        this.body = str;
    }
}
