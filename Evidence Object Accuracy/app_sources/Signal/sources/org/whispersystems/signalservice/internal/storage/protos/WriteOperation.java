package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.whispersystems.signalservice.internal.storage.protos.StorageItem;
import org.whispersystems.signalservice.internal.storage.protos.StorageManifest;

/* loaded from: classes5.dex */
public final class WriteOperation extends GeneratedMessageLite<WriteOperation, Builder> implements WriteOperationOrBuilder {
    public static final int CLEARALL_FIELD_NUMBER;
    private static final WriteOperation DEFAULT_INSTANCE;
    public static final int DELETEKEY_FIELD_NUMBER;
    public static final int INSERTITEM_FIELD_NUMBER;
    public static final int MANIFEST_FIELD_NUMBER;
    private static volatile Parser<WriteOperation> PARSER;
    private boolean clearAll_;
    private Internal.ProtobufList<ByteString> deleteKey_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<StorageItem> insertItem_ = GeneratedMessageLite.emptyProtobufList();
    private StorageManifest manifest_;

    private WriteOperation() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public boolean hasManifest() {
        return this.manifest_ != null;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public StorageManifest getManifest() {
        StorageManifest storageManifest = this.manifest_;
        return storageManifest == null ? StorageManifest.getDefaultInstance() : storageManifest;
    }

    public void setManifest(StorageManifest storageManifest) {
        storageManifest.getClass();
        this.manifest_ = storageManifest;
    }

    public void mergeManifest(StorageManifest storageManifest) {
        storageManifest.getClass();
        StorageManifest storageManifest2 = this.manifest_;
        if (storageManifest2 == null || storageManifest2 == StorageManifest.getDefaultInstance()) {
            this.manifest_ = storageManifest;
        } else {
            this.manifest_ = StorageManifest.newBuilder(this.manifest_).mergeFrom((StorageManifest.Builder) storageManifest).buildPartial();
        }
    }

    public void clearManifest() {
        this.manifest_ = null;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public List<StorageItem> getInsertItemList() {
        return this.insertItem_;
    }

    public List<? extends StorageItemOrBuilder> getInsertItemOrBuilderList() {
        return this.insertItem_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public int getInsertItemCount() {
        return this.insertItem_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public StorageItem getInsertItem(int i) {
        return this.insertItem_.get(i);
    }

    public StorageItemOrBuilder getInsertItemOrBuilder(int i) {
        return this.insertItem_.get(i);
    }

    private void ensureInsertItemIsMutable() {
        Internal.ProtobufList<StorageItem> protobufList = this.insertItem_;
        if (!protobufList.isModifiable()) {
            this.insertItem_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setInsertItem(int i, StorageItem storageItem) {
        storageItem.getClass();
        ensureInsertItemIsMutable();
        this.insertItem_.set(i, storageItem);
    }

    public void addInsertItem(StorageItem storageItem) {
        storageItem.getClass();
        ensureInsertItemIsMutable();
        this.insertItem_.add(storageItem);
    }

    public void addInsertItem(int i, StorageItem storageItem) {
        storageItem.getClass();
        ensureInsertItemIsMutable();
        this.insertItem_.add(i, storageItem);
    }

    public void addAllInsertItem(Iterable<? extends StorageItem> iterable) {
        ensureInsertItemIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.insertItem_);
    }

    public void clearInsertItem() {
        this.insertItem_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeInsertItem(int i) {
        ensureInsertItemIsMutable();
        this.insertItem_.remove(i);
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public List<ByteString> getDeleteKeyList() {
        return this.deleteKey_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public int getDeleteKeyCount() {
        return this.deleteKey_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public ByteString getDeleteKey(int i) {
        return this.deleteKey_.get(i);
    }

    private void ensureDeleteKeyIsMutable() {
        Internal.ProtobufList<ByteString> protobufList = this.deleteKey_;
        if (!protobufList.isModifiable()) {
            this.deleteKey_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setDeleteKey(int i, ByteString byteString) {
        byteString.getClass();
        ensureDeleteKeyIsMutable();
        this.deleteKey_.set(i, byteString);
    }

    public void addDeleteKey(ByteString byteString) {
        byteString.getClass();
        ensureDeleteKeyIsMutable();
        this.deleteKey_.add(byteString);
    }

    public void addAllDeleteKey(Iterable<? extends ByteString> iterable) {
        ensureDeleteKeyIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteKey_);
    }

    public void clearDeleteKey() {
        this.deleteKey_ = GeneratedMessageLite.emptyProtobufList();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
    public boolean getClearAll() {
        return this.clearAll_;
    }

    public void setClearAll(boolean z) {
        this.clearAll_ = z;
    }

    public void clearClearAll() {
        this.clearAll_ = false;
    }

    public static WriteOperation parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static WriteOperation parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static WriteOperation parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static WriteOperation parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static WriteOperation parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static WriteOperation parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static WriteOperation parseFrom(InputStream inputStream) throws IOException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static WriteOperation parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static WriteOperation parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (WriteOperation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static WriteOperation parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (WriteOperation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static WriteOperation parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static WriteOperation parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (WriteOperation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(WriteOperation writeOperation) {
        return DEFAULT_INSTANCE.createBuilder(writeOperation);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<WriteOperation, Builder> implements WriteOperationOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(WriteOperation.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public boolean hasManifest() {
            return ((WriteOperation) this.instance).hasManifest();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public StorageManifest getManifest() {
            return ((WriteOperation) this.instance).getManifest();
        }

        public Builder setManifest(StorageManifest storageManifest) {
            copyOnWrite();
            ((WriteOperation) this.instance).setManifest(storageManifest);
            return this;
        }

        public Builder setManifest(StorageManifest.Builder builder) {
            copyOnWrite();
            ((WriteOperation) this.instance).setManifest(builder.build());
            return this;
        }

        public Builder mergeManifest(StorageManifest storageManifest) {
            copyOnWrite();
            ((WriteOperation) this.instance).mergeManifest(storageManifest);
            return this;
        }

        public Builder clearManifest() {
            copyOnWrite();
            ((WriteOperation) this.instance).clearManifest();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public List<StorageItem> getInsertItemList() {
            return Collections.unmodifiableList(((WriteOperation) this.instance).getInsertItemList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public int getInsertItemCount() {
            return ((WriteOperation) this.instance).getInsertItemCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public StorageItem getInsertItem(int i) {
            return ((WriteOperation) this.instance).getInsertItem(i);
        }

        public Builder setInsertItem(int i, StorageItem storageItem) {
            copyOnWrite();
            ((WriteOperation) this.instance).setInsertItem(i, storageItem);
            return this;
        }

        public Builder setInsertItem(int i, StorageItem.Builder builder) {
            copyOnWrite();
            ((WriteOperation) this.instance).setInsertItem(i, builder.build());
            return this;
        }

        public Builder addInsertItem(StorageItem storageItem) {
            copyOnWrite();
            ((WriteOperation) this.instance).addInsertItem(storageItem);
            return this;
        }

        public Builder addInsertItem(int i, StorageItem storageItem) {
            copyOnWrite();
            ((WriteOperation) this.instance).addInsertItem(i, storageItem);
            return this;
        }

        public Builder addInsertItem(StorageItem.Builder builder) {
            copyOnWrite();
            ((WriteOperation) this.instance).addInsertItem(builder.build());
            return this;
        }

        public Builder addInsertItem(int i, StorageItem.Builder builder) {
            copyOnWrite();
            ((WriteOperation) this.instance).addInsertItem(i, builder.build());
            return this;
        }

        public Builder addAllInsertItem(Iterable<? extends StorageItem> iterable) {
            copyOnWrite();
            ((WriteOperation) this.instance).addAllInsertItem(iterable);
            return this;
        }

        public Builder clearInsertItem() {
            copyOnWrite();
            ((WriteOperation) this.instance).clearInsertItem();
            return this;
        }

        public Builder removeInsertItem(int i) {
            copyOnWrite();
            ((WriteOperation) this.instance).removeInsertItem(i);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public List<ByteString> getDeleteKeyList() {
            return Collections.unmodifiableList(((WriteOperation) this.instance).getDeleteKeyList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public int getDeleteKeyCount() {
            return ((WriteOperation) this.instance).getDeleteKeyCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public ByteString getDeleteKey(int i) {
            return ((WriteOperation) this.instance).getDeleteKey(i);
        }

        public Builder setDeleteKey(int i, ByteString byteString) {
            copyOnWrite();
            ((WriteOperation) this.instance).setDeleteKey(i, byteString);
            return this;
        }

        public Builder addDeleteKey(ByteString byteString) {
            copyOnWrite();
            ((WriteOperation) this.instance).addDeleteKey(byteString);
            return this;
        }

        public Builder addAllDeleteKey(Iterable<? extends ByteString> iterable) {
            copyOnWrite();
            ((WriteOperation) this.instance).addAllDeleteKey(iterable);
            return this;
        }

        public Builder clearDeleteKey() {
            copyOnWrite();
            ((WriteOperation) this.instance).clearDeleteKey();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.WriteOperationOrBuilder
        public boolean getClearAll() {
            return ((WriteOperation) this.instance).getClearAll();
        }

        public Builder setClearAll(boolean z) {
            copyOnWrite();
            ((WriteOperation) this.instance).setClearAll(z);
            return this;
        }

        public Builder clearClearAll() {
            copyOnWrite();
            ((WriteOperation) this.instance).clearClearAll();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.WriteOperation$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new WriteOperation();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0002\u0000\u0001\t\u0002\u001b\u0003\u001c\u0004\u0007", new Object[]{"manifest_", "insertItem_", StorageItem.class, "deleteKey_", "clearAll_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<WriteOperation> parser = PARSER;
                if (parser == null) {
                    synchronized (WriteOperation.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        WriteOperation writeOperation = new WriteOperation();
        DEFAULT_INSTANCE = writeOperation;
        GeneratedMessageLite.registerDefaultInstance(WriteOperation.class, writeOperation);
    }

    public static WriteOperation getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<WriteOperation> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
