package org.whispersystems.signalservice.internal.push;

import j$.util.Optional;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;

/* loaded from: classes5.dex */
public abstract class UnsupportedDataMessageException extends Exception {
    private final Optional<SignalServiceGroupContext> group;
    private final String sender;
    private final int senderDevice;

    public UnsupportedDataMessageException(String str, String str2, int i, Optional<SignalServiceGroupContext> optional) {
        super(str);
        this.sender = str2;
        this.senderDevice = i;
        this.group = optional;
    }

    public String getSender() {
        return this.sender;
    }

    public int getSenderDevice() {
        return this.senderDevice;
    }

    public Optional<SignalServiceGroupContext> getGroup() {
        return this.group;
    }
}
