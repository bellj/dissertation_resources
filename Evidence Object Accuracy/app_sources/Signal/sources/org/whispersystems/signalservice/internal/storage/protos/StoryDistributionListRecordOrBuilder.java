package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;

/* loaded from: classes5.dex */
public interface StoryDistributionListRecordOrBuilder extends MessageLiteOrBuilder {
    boolean getAllowsReplies();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    long getDeletedAtTimestamp();

    ByteString getIdentifier();

    boolean getIsBlockList();

    String getName();

    ByteString getNameBytes();

    String getRecipientUuids(int i);

    ByteString getRecipientUuidsBytes(int i);

    int getRecipientUuidsCount();

    List<String> getRecipientUuidsList();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
