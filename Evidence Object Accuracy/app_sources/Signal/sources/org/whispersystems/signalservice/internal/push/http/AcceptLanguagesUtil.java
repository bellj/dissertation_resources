package org.whispersystems.signalservice.internal.push.http;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes5.dex */
public class AcceptLanguagesUtil {
    public static Map<String, String> getHeadersWithAcceptLanguage(Locale locale) {
        HashMap hashMap = new HashMap();
        hashMap.put("Accept-Language", formatLanguages(locale.getLanguage(), locale.getCountry(), Locale.US.getLanguage()));
        return hashMap;
    }

    public static String getAcceptLanguageHeader(Locale locale) {
        return "Accept-Language:" + formatLanguages(locale.getLanguage(), locale.getCountry(), Locale.US.getLanguage());
    }

    private static String formatLanguages(String str, String str2, String str3) {
        if (Objects.equals(str, str3)) {
            return str + ";q=1";
        } else if (Util.isEmpty(str2)) {
            return str + ";q=1," + str3 + ";q=0.5";
        } else {
            return str + "-" + str2 + ";q=1," + str + ";q=0.75," + str3 + ";q=0.5";
        }
    }
}
