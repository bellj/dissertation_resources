package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public class PreKeyEntity {
    @JsonProperty
    private int keyId;
    @JsonProperty
    @JsonDeserialize(using = ECPublicKeyDeserializer.class)
    @JsonSerialize(using = ECPublicKeySerializer.class)
    private ECPublicKey publicKey;

    public PreKeyEntity() {
    }

    public PreKeyEntity(int i, ECPublicKey eCPublicKey) {
        this.keyId = i;
        this.publicKey = eCPublicKey;
    }

    public int getKeyId() {
        return this.keyId;
    }

    public ECPublicKey getPublicKey() {
        return this.publicKey;
    }

    /* loaded from: classes5.dex */
    private static class ECPublicKeySerializer extends JsonSerializer<ECPublicKey> {
        private ECPublicKeySerializer() {
        }

        public void serialize(ECPublicKey eCPublicKey, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Base64.encodeBytesWithoutPadding(eCPublicKey.serialize()));
        }
    }

    /* loaded from: classes5.dex */
    private static class ECPublicKeyDeserializer extends JsonDeserializer<ECPublicKey> {
        private ECPublicKeyDeserializer() {
        }

        public ECPublicKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            try {
                return Curve.decodePoint(Base64.decodeWithoutPadding(jsonParser.getValueAsString()), 0);
            } catch (InvalidKeyException e) {
                throw new IOException(e);
            }
        }
    }
}
