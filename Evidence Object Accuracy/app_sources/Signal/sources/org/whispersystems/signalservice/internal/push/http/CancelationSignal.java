package org.whispersystems.signalservice.internal.push.http;

/* loaded from: classes5.dex */
public interface CancelationSignal {
    boolean isCanceled();
}
