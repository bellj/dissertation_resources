package org.whispersystems.signalservice.internal.util.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture;

/* loaded from: classes5.dex */
public final class CascadingFuture<T> implements ListenableFuture<T> {
    private static final String TAG = CascadingFuture.class.getSimpleName();
    private SettableFuture<T> result;

    /* loaded from: classes5.dex */
    public interface ExceptionChecker {
        boolean shouldContinue(Exception exc);
    }

    public CascadingFuture(List<Callable<ListenableFuture<T>>> list, ExceptionChecker exceptionChecker) {
        if (!list.isEmpty()) {
            this.result = new SettableFuture<>();
            doNext(new ArrayList(list), exceptionChecker);
            return;
        }
        throw new IllegalArgumentException("Must have at least one callable!");
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        return this.result.cancel(z);
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        return this.result.isCancelled();
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return this.result.isDone();
    }

    @Override // java.util.concurrent.Future
    public T get() throws ExecutionException, InterruptedException {
        return this.result.get();
    }

    @Override // java.util.concurrent.Future
    public T get(long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        return this.result.get(j, timeUnit);
    }

    @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture
    public void addListener(ListenableFuture.Listener<T> listener) {
        this.result.addListener(listener);
    }

    public void doNext(final List<Callable<ListenableFuture<T>>> list, final ExceptionChecker exceptionChecker) {
        try {
            list.remove(0).call().addListener(new ListenableFuture.Listener<T>() { // from class: org.whispersystems.signalservice.internal.util.concurrent.CascadingFuture.1
                @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture.Listener
                public void onSuccess(T t) {
                    CascadingFuture.this.result.set(t);
                }

                @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture.Listener
                public void onFailure(ExecutionException executionException) {
                    if (list.isEmpty() || !exceptionChecker.shouldContinue(executionException)) {
                        Log.w(CascadingFuture.TAG, executionException);
                        CascadingFuture.this.result.setException(executionException.getCause());
                    } else if (!CascadingFuture.this.result.isCancelled()) {
                        CascadingFuture.this.doNext(list, exceptionChecker);
                    }
                }
            });
        } catch (Exception e) {
            if (list.isEmpty() || !exceptionChecker.shouldContinue(e)) {
                this.result.setException(e.getCause());
            } else if (!this.result.isCancelled()) {
                Log.w(TAG, e);
                doNext(list, exceptionChecker);
            }
        }
    }
}
