package org.whispersystems.signalservice.internal.push;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import okio.Buffer;
import okio.BufferedSink;
import okio.ByteString;
import okio.Source;
import okio.Timeout;

/* loaded from: classes5.dex */
public class NowhereBufferedSink implements BufferedSink {
    @Override // okio.BufferedSink
    public Buffer buffer() {
        return null;
    }

    @Override // okio.Sink, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
    }

    @Override // okio.BufferedSink
    public BufferedSink emit() throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink emitCompleteSegments() throws IOException {
        return this;
    }

    @Override // okio.BufferedSink, okio.Sink, java.io.Flushable
    public void flush() throws IOException {
    }

    @Override // okio.BufferedSink
    public Buffer getBuffer() {
        return null;
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return false;
    }

    @Override // okio.Sink
    public Timeout timeout() {
        return null;
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) throws IOException {
        return 0;
    }

    @Override // okio.BufferedSink
    public BufferedSink write(ByteString byteString) throws IOException {
        return this;
    }

    public BufferedSink write(Source source, long j) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink write(byte[] bArr) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink write(byte[] bArr, int i, int i2) throws IOException {
        return this;
    }

    @Override // okio.Sink
    public void write(Buffer buffer, long j) throws IOException {
    }

    @Override // okio.BufferedSink
    public long writeAll(Source source) throws IOException {
        return 0;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeByte(int i) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeDecimalLong(long j) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeHexadecimalUnsignedLong(long j) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeInt(int i) throws IOException {
        return this;
    }

    public BufferedSink writeIntLe(int i) throws IOException {
        return this;
    }

    public BufferedSink writeLong(long j) throws IOException {
        return this;
    }

    public BufferedSink writeLongLe(long j) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeShort(int i) throws IOException {
        return this;
    }

    public BufferedSink writeShortLe(int i) throws IOException {
        return this;
    }

    public BufferedSink writeString(String str, int i, int i2, Charset charset) throws IOException {
        return this;
    }

    public BufferedSink writeString(String str, Charset charset) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeUtf8(String str) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public BufferedSink writeUtf8(String str, int i, int i2) throws IOException {
        return this;
    }

    public BufferedSink writeUtf8CodePoint(int i) throws IOException {
        return this;
    }

    @Override // okio.BufferedSink
    public OutputStream outputStream() {
        return new OutputStream() { // from class: org.whispersystems.signalservice.internal.push.NowhereBufferedSink.1
            @Override // java.io.OutputStream
            public void write(int i) throws IOException {
            }

            @Override // java.io.OutputStream
            public void write(byte[] bArr) throws IOException {
            }

            @Override // java.io.OutputStream
            public void write(byte[] bArr, int i, int i2) throws IOException {
            }
        };
    }
}
