package org.whispersystems.signalservice.internal.push;

import java.io.InputStream;
import org.whispersystems.signalservice.internal.push.http.OutputStreamFactory;

/* loaded from: classes5.dex */
public class ProfileAvatarData {
    private final String contentType;
    private final InputStream data;
    private final long dataLength;
    private final OutputStreamFactory outputStreamFactory;

    public ProfileAvatarData(InputStream inputStream, long j, String str, OutputStreamFactory outputStreamFactory) {
        this.data = inputStream;
        this.dataLength = j;
        this.contentType = str;
        this.outputStreamFactory = outputStreamFactory;
    }

    public InputStream getData() {
        return this.data;
    }

    public long getDataLength() {
        return this.dataLength;
    }

    public OutputStreamFactory getOutputStreamFactory() {
        return this.outputStreamFactory;
    }

    public String getContentType() {
        return this.contentType;
    }
}
