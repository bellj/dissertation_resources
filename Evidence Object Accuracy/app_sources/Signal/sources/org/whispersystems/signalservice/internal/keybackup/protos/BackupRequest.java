package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class BackupRequest extends GeneratedMessageLite<BackupRequest, Builder> implements BackupRequestOrBuilder {
    public static final int BACKUP_ID_FIELD_NUMBER;
    public static final int DATA_FIELD_NUMBER;
    private static final BackupRequest DEFAULT_INSTANCE;
    private static volatile Parser<BackupRequest> PARSER;
    public static final int PIN_FIELD_NUMBER;
    public static final int SERVICE_ID_FIELD_NUMBER;
    public static final int TOKEN_FIELD_NUMBER;
    public static final int TRIES_FIELD_NUMBER;
    public static final int VALID_FROM_FIELD_NUMBER;
    private ByteString backupId_;
    private int bitField0_;
    private ByteString data_;
    private ByteString pin_;
    private ByteString serviceId_;
    private ByteString token_;
    private int tries_;
    private long validFrom_;

    private BackupRequest() {
        ByteString byteString = ByteString.EMPTY;
        this.serviceId_ = byteString;
        this.backupId_ = byteString;
        this.token_ = byteString;
        this.data_ = byteString;
        this.pin_ = byteString;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasServiceId() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public ByteString getServiceId() {
        return this.serviceId_;
    }

    public void setServiceId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 1;
        this.serviceId_ = byteString;
    }

    public void clearServiceId() {
        this.bitField0_ &= -2;
        this.serviceId_ = getDefaultInstance().getServiceId();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasBackupId() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public ByteString getBackupId() {
        return this.backupId_;
    }

    public void setBackupId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 2;
        this.backupId_ = byteString;
    }

    public void clearBackupId() {
        this.bitField0_ &= -3;
        this.backupId_ = getDefaultInstance().getBackupId();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasToken() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public ByteString getToken() {
        return this.token_;
    }

    public void setToken(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 4;
        this.token_ = byteString;
    }

    public void clearToken() {
        this.bitField0_ &= -5;
        this.token_ = getDefaultInstance().getToken();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasValidFrom() {
        return (this.bitField0_ & 8) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public long getValidFrom() {
        return this.validFrom_;
    }

    public void setValidFrom(long j) {
        this.bitField0_ |= 8;
        this.validFrom_ = j;
    }

    public void clearValidFrom() {
        this.bitField0_ &= -9;
        this.validFrom_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasData() {
        return (this.bitField0_ & 16) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public ByteString getData() {
        return this.data_;
    }

    public void setData(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 16;
        this.data_ = byteString;
    }

    public void clearData() {
        this.bitField0_ &= -17;
        this.data_ = getDefaultInstance().getData();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasPin() {
        return (this.bitField0_ & 32) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public ByteString getPin() {
        return this.pin_;
    }

    public void setPin(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 32;
        this.pin_ = byteString;
    }

    public void clearPin() {
        this.bitField0_ &= -33;
        this.pin_ = getDefaultInstance().getPin();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public boolean hasTries() {
        return (this.bitField0_ & 64) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
    public int getTries() {
        return this.tries_;
    }

    public void setTries(int i) {
        this.bitField0_ |= 64;
        this.tries_ = i;
    }

    public void clearTries() {
        this.bitField0_ &= -65;
        this.tries_ = 0;
    }

    public static BackupRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static BackupRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static BackupRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static BackupRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static BackupRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static BackupRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static BackupRequest parseFrom(InputStream inputStream) throws IOException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BackupRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BackupRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (BackupRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static BackupRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BackupRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static BackupRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static BackupRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (BackupRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(BackupRequest backupRequest) {
        return DEFAULT_INSTANCE.createBuilder(backupRequest);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<BackupRequest, Builder> implements BackupRequestOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(BackupRequest.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasServiceId() {
            return ((BackupRequest) this.instance).hasServiceId();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public ByteString getServiceId() {
            return ((BackupRequest) this.instance).getServiceId();
        }

        public Builder setServiceId(ByteString byteString) {
            copyOnWrite();
            ((BackupRequest) this.instance).setServiceId(byteString);
            return this;
        }

        public Builder clearServiceId() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearServiceId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasBackupId() {
            return ((BackupRequest) this.instance).hasBackupId();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public ByteString getBackupId() {
            return ((BackupRequest) this.instance).getBackupId();
        }

        public Builder setBackupId(ByteString byteString) {
            copyOnWrite();
            ((BackupRequest) this.instance).setBackupId(byteString);
            return this;
        }

        public Builder clearBackupId() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearBackupId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasToken() {
            return ((BackupRequest) this.instance).hasToken();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public ByteString getToken() {
            return ((BackupRequest) this.instance).getToken();
        }

        public Builder setToken(ByteString byteString) {
            copyOnWrite();
            ((BackupRequest) this.instance).setToken(byteString);
            return this;
        }

        public Builder clearToken() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearToken();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasValidFrom() {
            return ((BackupRequest) this.instance).hasValidFrom();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public long getValidFrom() {
            return ((BackupRequest) this.instance).getValidFrom();
        }

        public Builder setValidFrom(long j) {
            copyOnWrite();
            ((BackupRequest) this.instance).setValidFrom(j);
            return this;
        }

        public Builder clearValidFrom() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearValidFrom();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasData() {
            return ((BackupRequest) this.instance).hasData();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public ByteString getData() {
            return ((BackupRequest) this.instance).getData();
        }

        public Builder setData(ByteString byteString) {
            copyOnWrite();
            ((BackupRequest) this.instance).setData(byteString);
            return this;
        }

        public Builder clearData() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearData();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasPin() {
            return ((BackupRequest) this.instance).hasPin();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public ByteString getPin() {
            return ((BackupRequest) this.instance).getPin();
        }

        public Builder setPin(ByteString byteString) {
            copyOnWrite();
            ((BackupRequest) this.instance).setPin(byteString);
            return this;
        }

        public Builder clearPin() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearPin();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public boolean hasTries() {
            return ((BackupRequest) this.instance).hasTries();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.BackupRequestOrBuilder
        public int getTries() {
            return ((BackupRequest) this.instance).getTries();
        }

        public Builder setTries(int i) {
            copyOnWrite();
            ((BackupRequest) this.instance).setTries(i);
            return this;
        }

        public Builder clearTries() {
            copyOnWrite();
            ((BackupRequest) this.instance).clearTries();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.BackupRequest$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new BackupRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001\u0003ည\u0002\u0004ဃ\u0003\u0005ည\u0004\u0006ည\u0005\u0007ဋ\u0006", new Object[]{"bitField0_", "serviceId_", "backupId_", "token_", "validFrom_", "data_", "pin_", "tries_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<BackupRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (BackupRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        BackupRequest backupRequest = new BackupRequest();
        DEFAULT_INSTANCE = backupRequest;
        GeneratedMessageLite.registerDefaultInstance(BackupRequest.class, backupRequest);
    }

    public static BackupRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<BackupRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
