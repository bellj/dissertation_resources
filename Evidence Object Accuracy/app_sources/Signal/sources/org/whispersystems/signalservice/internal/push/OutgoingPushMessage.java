package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class OutgoingPushMessage {
    @JsonProperty
    private String content;
    @JsonProperty
    private int destinationDeviceId;
    @JsonProperty
    private int destinationRegistrationId;
    @JsonProperty
    private int type;

    public OutgoingPushMessage(int i, int i2, int i3, String str) {
        this.type = i;
        this.destinationDeviceId = i2;
        this.destinationRegistrationId = i3;
        this.content = str;
    }

    public int getDestinationDeviceId() {
        return this.destinationDeviceId;
    }
}
