package org.whispersystems.signalservice.internal.util.concurrent;

/* loaded from: classes5.dex */
public final class FutureTransformers {

    /* loaded from: classes5.dex */
    public interface Transformer<Input, Output> {
        Output transform(Input input) throws Exception;
    }

    public static <Input, Output> ListenableFuture<Output> map(ListenableFuture<Input> listenableFuture, Transformer<Input, Output> transformer) {
        return new FutureMapTransformer(listenableFuture, transformer);
    }
}
