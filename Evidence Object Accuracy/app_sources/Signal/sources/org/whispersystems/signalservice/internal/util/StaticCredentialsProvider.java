package org.whispersystems.signalservice.internal.util;

import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.util.CredentialsProvider;

/* loaded from: classes5.dex */
public class StaticCredentialsProvider implements CredentialsProvider {
    private final ACI aci;
    private final int deviceId;
    private final String e164;
    private final String password;
    private final PNI pni;

    public StaticCredentialsProvider(ACI aci, PNI pni, String str, int i, String str2) {
        this.aci = aci;
        this.pni = pni;
        this.e164 = str;
        this.deviceId = i;
        this.password = str2;
    }

    @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
    public ACI getAci() {
        return this.aci;
    }

    @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
    public PNI getPni() {
        return this.pni;
    }

    @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
    public String getE164() {
        return this.e164;
    }

    @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
    public int getDeviceId() {
        return this.deviceId;
    }

    @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
    public String getPassword() {
        return this.password;
    }
}
