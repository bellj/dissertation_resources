package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.whispersystems.signalservice.internal.storage.protos.AccountRecord;
import org.whispersystems.signalservice.internal.storage.protos.ContactRecord;
import org.whispersystems.signalservice.internal.storage.protos.GroupV1Record;
import org.whispersystems.signalservice.internal.storage.protos.GroupV2Record;
import org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecord;

/* loaded from: classes5.dex */
public final class StorageRecord extends GeneratedMessageLite<StorageRecord, Builder> implements StorageRecordOrBuilder {
    public static final int ACCOUNT_FIELD_NUMBER;
    public static final int CONTACT_FIELD_NUMBER;
    private static final StorageRecord DEFAULT_INSTANCE;
    public static final int GROUPV1_FIELD_NUMBER;
    public static final int GROUPV2_FIELD_NUMBER;
    private static volatile Parser<StorageRecord> PARSER;
    public static final int STORYDISTRIBUTIONLIST_FIELD_NUMBER;
    private int recordCase_ = 0;
    private Object record_;

    private StorageRecord() {
    }

    /* loaded from: classes5.dex */
    public enum RecordCase {
        CONTACT(1),
        GROUPV1(2),
        GROUPV2(3),
        ACCOUNT(4),
        STORYDISTRIBUTIONLIST(5),
        RECORD_NOT_SET(0);
        
        private final int value;

        RecordCase(int i) {
            this.value = i;
        }

        @Deprecated
        public static RecordCase valueOf(int i) {
            return forNumber(i);
        }

        public static RecordCase forNumber(int i) {
            if (i == 0) {
                return RECORD_NOT_SET;
            }
            if (i == 1) {
                return CONTACT;
            }
            if (i == 2) {
                return GROUPV1;
            }
            if (i == 3) {
                return GROUPV2;
            }
            if (i == 4) {
                return ACCOUNT;
            }
            if (i != 5) {
                return null;
            }
            return STORYDISTRIBUTIONLIST;
        }

        public int getNumber() {
            return this.value;
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public RecordCase getRecordCase() {
        return RecordCase.forNumber(this.recordCase_);
    }

    public void clearRecord() {
        this.recordCase_ = 0;
        this.record_ = null;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public boolean hasContact() {
        return this.recordCase_ == 1;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public ContactRecord getContact() {
        if (this.recordCase_ == 1) {
            return (ContactRecord) this.record_;
        }
        return ContactRecord.getDefaultInstance();
    }

    public void setContact(ContactRecord contactRecord) {
        contactRecord.getClass();
        this.record_ = contactRecord;
        this.recordCase_ = 1;
    }

    public void mergeContact(ContactRecord contactRecord) {
        contactRecord.getClass();
        if (this.recordCase_ != 1 || this.record_ == ContactRecord.getDefaultInstance()) {
            this.record_ = contactRecord;
        } else {
            this.record_ = ContactRecord.newBuilder((ContactRecord) this.record_).mergeFrom((ContactRecord.Builder) contactRecord).buildPartial();
        }
        this.recordCase_ = 1;
    }

    public void clearContact() {
        if (this.recordCase_ == 1) {
            this.recordCase_ = 0;
            this.record_ = null;
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public boolean hasGroupV1() {
        return this.recordCase_ == 2;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public GroupV1Record getGroupV1() {
        if (this.recordCase_ == 2) {
            return (GroupV1Record) this.record_;
        }
        return GroupV1Record.getDefaultInstance();
    }

    public void setGroupV1(GroupV1Record groupV1Record) {
        groupV1Record.getClass();
        this.record_ = groupV1Record;
        this.recordCase_ = 2;
    }

    public void mergeGroupV1(GroupV1Record groupV1Record) {
        groupV1Record.getClass();
        if (this.recordCase_ != 2 || this.record_ == GroupV1Record.getDefaultInstance()) {
            this.record_ = groupV1Record;
        } else {
            this.record_ = GroupV1Record.newBuilder((GroupV1Record) this.record_).mergeFrom((GroupV1Record.Builder) groupV1Record).buildPartial();
        }
        this.recordCase_ = 2;
    }

    public void clearGroupV1() {
        if (this.recordCase_ == 2) {
            this.recordCase_ = 0;
            this.record_ = null;
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public boolean hasGroupV2() {
        return this.recordCase_ == 3;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public GroupV2Record getGroupV2() {
        if (this.recordCase_ == 3) {
            return (GroupV2Record) this.record_;
        }
        return GroupV2Record.getDefaultInstance();
    }

    public void setGroupV2(GroupV2Record groupV2Record) {
        groupV2Record.getClass();
        this.record_ = groupV2Record;
        this.recordCase_ = 3;
    }

    public void mergeGroupV2(GroupV2Record groupV2Record) {
        groupV2Record.getClass();
        if (this.recordCase_ != 3 || this.record_ == GroupV2Record.getDefaultInstance()) {
            this.record_ = groupV2Record;
        } else {
            this.record_ = GroupV2Record.newBuilder((GroupV2Record) this.record_).mergeFrom((GroupV2Record.Builder) groupV2Record).buildPartial();
        }
        this.recordCase_ = 3;
    }

    public void clearGroupV2() {
        if (this.recordCase_ == 3) {
            this.recordCase_ = 0;
            this.record_ = null;
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public boolean hasAccount() {
        return this.recordCase_ == 4;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public AccountRecord getAccount() {
        if (this.recordCase_ == 4) {
            return (AccountRecord) this.record_;
        }
        return AccountRecord.getDefaultInstance();
    }

    public void setAccount(AccountRecord accountRecord) {
        accountRecord.getClass();
        this.record_ = accountRecord;
        this.recordCase_ = 4;
    }

    public void mergeAccount(AccountRecord accountRecord) {
        accountRecord.getClass();
        if (this.recordCase_ != 4 || this.record_ == AccountRecord.getDefaultInstance()) {
            this.record_ = accountRecord;
        } else {
            this.record_ = AccountRecord.newBuilder((AccountRecord) this.record_).mergeFrom((AccountRecord.Builder) accountRecord).buildPartial();
        }
        this.recordCase_ = 4;
    }

    public void clearAccount() {
        if (this.recordCase_ == 4) {
            this.recordCase_ = 0;
            this.record_ = null;
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public boolean hasStoryDistributionList() {
        return this.recordCase_ == 5;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
    public StoryDistributionListRecord getStoryDistributionList() {
        if (this.recordCase_ == 5) {
            return (StoryDistributionListRecord) this.record_;
        }
        return StoryDistributionListRecord.getDefaultInstance();
    }

    public void setStoryDistributionList(StoryDistributionListRecord storyDistributionListRecord) {
        storyDistributionListRecord.getClass();
        this.record_ = storyDistributionListRecord;
        this.recordCase_ = 5;
    }

    public void mergeStoryDistributionList(StoryDistributionListRecord storyDistributionListRecord) {
        storyDistributionListRecord.getClass();
        if (this.recordCase_ != 5 || this.record_ == StoryDistributionListRecord.getDefaultInstance()) {
            this.record_ = storyDistributionListRecord;
        } else {
            this.record_ = StoryDistributionListRecord.newBuilder((StoryDistributionListRecord) this.record_).mergeFrom((StoryDistributionListRecord.Builder) storyDistributionListRecord).buildPartial();
        }
        this.recordCase_ = 5;
    }

    public void clearStoryDistributionList() {
        if (this.recordCase_ == 5) {
            this.recordCase_ = 0;
            this.record_ = null;
        }
    }

    public static StorageRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static StorageRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static StorageRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static StorageRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static StorageRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static StorageRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static StorageRecord parseFrom(InputStream inputStream) throws IOException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (StorageRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static StorageRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(StorageRecord storageRecord) {
        return DEFAULT_INSTANCE.createBuilder(storageRecord);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<StorageRecord, Builder> implements StorageRecordOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(StorageRecord.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public RecordCase getRecordCase() {
            return ((StorageRecord) this.instance).getRecordCase();
        }

        public Builder clearRecord() {
            copyOnWrite();
            ((StorageRecord) this.instance).clearRecord();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public boolean hasContact() {
            return ((StorageRecord) this.instance).hasContact();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public ContactRecord getContact() {
            return ((StorageRecord) this.instance).getContact();
        }

        public Builder setContact(ContactRecord contactRecord) {
            copyOnWrite();
            ((StorageRecord) this.instance).setContact(contactRecord);
            return this;
        }

        public Builder setContact(ContactRecord.Builder builder) {
            copyOnWrite();
            ((StorageRecord) this.instance).setContact(builder.build());
            return this;
        }

        public Builder mergeContact(ContactRecord contactRecord) {
            copyOnWrite();
            ((StorageRecord) this.instance).mergeContact(contactRecord);
            return this;
        }

        public Builder clearContact() {
            copyOnWrite();
            ((StorageRecord) this.instance).clearContact();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public boolean hasGroupV1() {
            return ((StorageRecord) this.instance).hasGroupV1();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public GroupV1Record getGroupV1() {
            return ((StorageRecord) this.instance).getGroupV1();
        }

        public Builder setGroupV1(GroupV1Record groupV1Record) {
            copyOnWrite();
            ((StorageRecord) this.instance).setGroupV1(groupV1Record);
            return this;
        }

        public Builder setGroupV1(GroupV1Record.Builder builder) {
            copyOnWrite();
            ((StorageRecord) this.instance).setGroupV1(builder.build());
            return this;
        }

        public Builder mergeGroupV1(GroupV1Record groupV1Record) {
            copyOnWrite();
            ((StorageRecord) this.instance).mergeGroupV1(groupV1Record);
            return this;
        }

        public Builder clearGroupV1() {
            copyOnWrite();
            ((StorageRecord) this.instance).clearGroupV1();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public boolean hasGroupV2() {
            return ((StorageRecord) this.instance).hasGroupV2();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public GroupV2Record getGroupV2() {
            return ((StorageRecord) this.instance).getGroupV2();
        }

        public Builder setGroupV2(GroupV2Record groupV2Record) {
            copyOnWrite();
            ((StorageRecord) this.instance).setGroupV2(groupV2Record);
            return this;
        }

        public Builder setGroupV2(GroupV2Record.Builder builder) {
            copyOnWrite();
            ((StorageRecord) this.instance).setGroupV2(builder.build());
            return this;
        }

        public Builder mergeGroupV2(GroupV2Record groupV2Record) {
            copyOnWrite();
            ((StorageRecord) this.instance).mergeGroupV2(groupV2Record);
            return this;
        }

        public Builder clearGroupV2() {
            copyOnWrite();
            ((StorageRecord) this.instance).clearGroupV2();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public boolean hasAccount() {
            return ((StorageRecord) this.instance).hasAccount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public AccountRecord getAccount() {
            return ((StorageRecord) this.instance).getAccount();
        }

        public Builder setAccount(AccountRecord accountRecord) {
            copyOnWrite();
            ((StorageRecord) this.instance).setAccount(accountRecord);
            return this;
        }

        public Builder setAccount(AccountRecord.Builder builder) {
            copyOnWrite();
            ((StorageRecord) this.instance).setAccount(builder.build());
            return this;
        }

        public Builder mergeAccount(AccountRecord accountRecord) {
            copyOnWrite();
            ((StorageRecord) this.instance).mergeAccount(accountRecord);
            return this;
        }

        public Builder clearAccount() {
            copyOnWrite();
            ((StorageRecord) this.instance).clearAccount();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public boolean hasStoryDistributionList() {
            return ((StorageRecord) this.instance).hasStoryDistributionList();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageRecordOrBuilder
        public StoryDistributionListRecord getStoryDistributionList() {
            return ((StorageRecord) this.instance).getStoryDistributionList();
        }

        public Builder setStoryDistributionList(StoryDistributionListRecord storyDistributionListRecord) {
            copyOnWrite();
            ((StorageRecord) this.instance).setStoryDistributionList(storyDistributionListRecord);
            return this;
        }

        public Builder setStoryDistributionList(StoryDistributionListRecord.Builder builder) {
            copyOnWrite();
            ((StorageRecord) this.instance).setStoryDistributionList(builder.build());
            return this;
        }

        public Builder mergeStoryDistributionList(StoryDistributionListRecord storyDistributionListRecord) {
            copyOnWrite();
            ((StorageRecord) this.instance).mergeStoryDistributionList(storyDistributionListRecord);
            return this;
        }

        public Builder clearStoryDistributionList() {
            copyOnWrite();
            ((StorageRecord) this.instance).clearStoryDistributionList();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.StorageRecord$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new StorageRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0001\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000\u0003<\u0000\u0004<\u0000\u0005<\u0000", new Object[]{"record_", "recordCase_", ContactRecord.class, GroupV1Record.class, GroupV2Record.class, AccountRecord.class, StoryDistributionListRecord.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<StorageRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (StorageRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        StorageRecord storageRecord = new StorageRecord();
        DEFAULT_INSTANCE = storageRecord;
        GeneratedMessageLite.registerDefaultInstance(StorageRecord.class, storageRecord);
    }

    public static StorageRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<StorageRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
