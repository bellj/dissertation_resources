package org.whispersystems.signalservice.internal.websocket;

import j$.util.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class DefaultErrorMapper$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ WebsocketResponse f$0;

    public /* synthetic */ DefaultErrorMapper$$ExternalSyntheticLambda0(WebsocketResponse websocketResponse) {
        this.f$0 = websocketResponse;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return this.f$0.getHeader((String) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
