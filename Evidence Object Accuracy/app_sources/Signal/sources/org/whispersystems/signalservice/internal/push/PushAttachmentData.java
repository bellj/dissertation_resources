package org.whispersystems.signalservice.internal.push;

import java.io.InputStream;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.OutputStreamFactory;
import org.whispersystems.signalservice.internal.push.http.ResumableUploadSpec;

/* loaded from: classes5.dex */
public class PushAttachmentData {
    private final CancelationSignal cancelationSignal;
    private final String contentType;
    private final InputStream data;
    private final long dataSize;
    private final SignalServiceAttachment.ProgressListener listener;
    private final OutputStreamFactory outputStreamFactory;
    private final ResumableUploadSpec resumableUploadSpec;

    public PushAttachmentData(String str, InputStream inputStream, long j, OutputStreamFactory outputStreamFactory, SignalServiceAttachment.ProgressListener progressListener, CancelationSignal cancelationSignal, ResumableUploadSpec resumableUploadSpec) {
        this.contentType = str;
        this.data = inputStream;
        this.dataSize = j;
        this.outputStreamFactory = outputStreamFactory;
        this.resumableUploadSpec = resumableUploadSpec;
        this.listener = progressListener;
        this.cancelationSignal = cancelationSignal;
    }

    public String getContentType() {
        return this.contentType;
    }

    public InputStream getData() {
        return this.data;
    }

    public long getDataSize() {
        return this.dataSize;
    }

    public OutputStreamFactory getOutputStreamFactory() {
        return this.outputStreamFactory;
    }

    public SignalServiceAttachment.ProgressListener getListener() {
        return this.listener;
    }

    public CancelationSignal getCancelationSignal() {
        return this.cancelationSignal;
    }

    public ResumableUploadSpec getResumableUploadSpec() {
        return this.resumableUploadSpec;
    }
}
