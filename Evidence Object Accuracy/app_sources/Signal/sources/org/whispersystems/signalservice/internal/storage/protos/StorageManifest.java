package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class StorageManifest extends GeneratedMessageLite<StorageManifest, Builder> implements StorageManifestOrBuilder {
    private static final StorageManifest DEFAULT_INSTANCE;
    private static volatile Parser<StorageManifest> PARSER;
    public static final int VALUE_FIELD_NUMBER;
    public static final int VERSION_FIELD_NUMBER;
    private ByteString value_ = ByteString.EMPTY;
    private long version_;

    private StorageManifest() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageManifestOrBuilder
    public long getVersion() {
        return this.version_;
    }

    public void setVersion(long j) {
        this.version_ = j;
    }

    public void clearVersion() {
        this.version_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageManifestOrBuilder
    public ByteString getValue() {
        return this.value_;
    }

    public void setValue(ByteString byteString) {
        byteString.getClass();
        this.value_ = byteString;
    }

    public void clearValue() {
        this.value_ = getDefaultInstance().getValue();
    }

    public static StorageManifest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static StorageManifest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static StorageManifest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static StorageManifest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static StorageManifest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static StorageManifest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static StorageManifest parseFrom(InputStream inputStream) throws IOException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageManifest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageManifest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (StorageManifest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageManifest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageManifest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageManifest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static StorageManifest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageManifest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(StorageManifest storageManifest) {
        return DEFAULT_INSTANCE.createBuilder(storageManifest);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<StorageManifest, Builder> implements StorageManifestOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(StorageManifest.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageManifestOrBuilder
        public long getVersion() {
            return ((StorageManifest) this.instance).getVersion();
        }

        public Builder setVersion(long j) {
            copyOnWrite();
            ((StorageManifest) this.instance).setVersion(j);
            return this;
        }

        public Builder clearVersion() {
            copyOnWrite();
            ((StorageManifest) this.instance).clearVersion();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageManifestOrBuilder
        public ByteString getValue() {
            return ((StorageManifest) this.instance).getValue();
        }

        public Builder setValue(ByteString byteString) {
            copyOnWrite();
            ((StorageManifest) this.instance).setValue(byteString);
            return this;
        }

        public Builder clearValue() {
            copyOnWrite();
            ((StorageManifest) this.instance).clearValue();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.StorageManifest$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new StorageManifest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0003\u0002\n", new Object[]{"version_", "value_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<StorageManifest> parser = PARSER;
                if (parser == null) {
                    synchronized (StorageManifest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        StorageManifest storageManifest = new StorageManifest();
        DEFAULT_INSTANCE = storageManifest;
        GeneratedMessageLite.registerDefaultInstance(StorageManifest.class, storageManifest);
    }

    public static StorageManifest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<StorageManifest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
