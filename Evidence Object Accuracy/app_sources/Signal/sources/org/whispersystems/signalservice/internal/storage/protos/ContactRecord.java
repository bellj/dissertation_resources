package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class ContactRecord extends GeneratedMessageLite<ContactRecord, Builder> implements ContactRecordOrBuilder {
    public static final int ARCHIVED_FIELD_NUMBER;
    public static final int BLOCKED_FIELD_NUMBER;
    private static final ContactRecord DEFAULT_INSTANCE;
    public static final int FAMILYNAME_FIELD_NUMBER;
    public static final int GIVENNAME_FIELD_NUMBER;
    public static final int HIDESTORY_FIELD_NUMBER;
    public static final int IDENTITYKEY_FIELD_NUMBER;
    public static final int IDENTITYSTATE_FIELD_NUMBER;
    public static final int MARKEDUNREAD_FIELD_NUMBER;
    public static final int MUTEDUNTILTIMESTAMP_FIELD_NUMBER;
    private static volatile Parser<ContactRecord> PARSER;
    public static final int PROFILEKEY_FIELD_NUMBER;
    public static final int SERVICEE164_FIELD_NUMBER;
    public static final int SERVICEUUID_FIELD_NUMBER;
    public static final int USERNAME_FIELD_NUMBER;
    public static final int WHITELISTED_FIELD_NUMBER;
    private boolean archived_;
    private boolean blocked_;
    private String familyName_;
    private String givenName_;
    private boolean hideStory_;
    private ByteString identityKey_;
    private int identityState_;
    private boolean markedUnread_;
    private long mutedUntilTimestamp_;
    private ByteString profileKey_;
    private String serviceE164_ = "";
    private String serviceUuid_ = "";
    private String username_;
    private boolean whitelisted_;

    private ContactRecord() {
        ByteString byteString = ByteString.EMPTY;
        this.profileKey_ = byteString;
        this.identityKey_ = byteString;
        this.givenName_ = "";
        this.familyName_ = "";
        this.username_ = "";
    }

    /* loaded from: classes5.dex */
    public enum IdentityState implements Internal.EnumLite {
        DEFAULT(0),
        VERIFIED(1),
        UNVERIFIED(2),
        UNRECOGNIZED(-1);
        
        public static final int DEFAULT_VALUE;
        public static final int UNVERIFIED_VALUE;
        public static final int VERIFIED_VALUE;
        private static final Internal.EnumLiteMap<IdentityState> internalValueMap = new Internal.EnumLiteMap<IdentityState>() { // from class: org.whispersystems.signalservice.internal.storage.protos.ContactRecord.IdentityState.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public IdentityState findValueByNumber(int i) {
                return IdentityState.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static IdentityState valueOf(int i) {
            return forNumber(i);
        }

        public static IdentityState forNumber(int i) {
            if (i == 0) {
                return DEFAULT;
            }
            if (i == 1) {
                return VERIFIED;
            }
            if (i != 2) {
                return null;
            }
            return UNVERIFIED;
        }

        public static Internal.EnumLiteMap<IdentityState> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return IdentityStateVerifier.INSTANCE;
        }

        /* loaded from: classes5.dex */
        private static final class IdentityStateVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new IdentityStateVerifier();

            private IdentityStateVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return IdentityState.forNumber(i) != null;
            }
        }

        IdentityState(int i) {
            this.value = i;
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public String getServiceUuid() {
        return this.serviceUuid_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getServiceUuidBytes() {
        return ByteString.copyFromUtf8(this.serviceUuid_);
    }

    public void setServiceUuid(String str) {
        str.getClass();
        this.serviceUuid_ = str;
    }

    public void clearServiceUuid() {
        this.serviceUuid_ = getDefaultInstance().getServiceUuid();
    }

    public void setServiceUuidBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.serviceUuid_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public String getServiceE164() {
        return this.serviceE164_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getServiceE164Bytes() {
        return ByteString.copyFromUtf8(this.serviceE164_);
    }

    public void setServiceE164(String str) {
        str.getClass();
        this.serviceE164_ = str;
    }

    public void clearServiceE164() {
        this.serviceE164_ = getDefaultInstance().getServiceE164();
    }

    public void setServiceE164Bytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.serviceE164_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getProfileKey() {
        return this.profileKey_;
    }

    public void setProfileKey(ByteString byteString) {
        byteString.getClass();
        this.profileKey_ = byteString;
    }

    public void clearProfileKey() {
        this.profileKey_ = getDefaultInstance().getProfileKey();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getIdentityKey() {
        return this.identityKey_;
    }

    public void setIdentityKey(ByteString byteString) {
        byteString.getClass();
        this.identityKey_ = byteString;
    }

    public void clearIdentityKey() {
        this.identityKey_ = getDefaultInstance().getIdentityKey();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public int getIdentityStateValue() {
        return this.identityState_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public IdentityState getIdentityState() {
        IdentityState forNumber = IdentityState.forNumber(this.identityState_);
        return forNumber == null ? IdentityState.UNRECOGNIZED : forNumber;
    }

    public void setIdentityStateValue(int i) {
        this.identityState_ = i;
    }

    public void setIdentityState(IdentityState identityState) {
        this.identityState_ = identityState.getNumber();
    }

    public void clearIdentityState() {
        this.identityState_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public String getGivenName() {
        return this.givenName_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getGivenNameBytes() {
        return ByteString.copyFromUtf8(this.givenName_);
    }

    public void setGivenName(String str) {
        str.getClass();
        this.givenName_ = str;
    }

    public void clearGivenName() {
        this.givenName_ = getDefaultInstance().getGivenName();
    }

    public void setGivenNameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.givenName_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public String getFamilyName() {
        return this.familyName_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getFamilyNameBytes() {
        return ByteString.copyFromUtf8(this.familyName_);
    }

    public void setFamilyName(String str) {
        str.getClass();
        this.familyName_ = str;
    }

    public void clearFamilyName() {
        this.familyName_ = getDefaultInstance().getFamilyName();
    }

    public void setFamilyNameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.familyName_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public String getUsername() {
        return this.username_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public ByteString getUsernameBytes() {
        return ByteString.copyFromUtf8(this.username_);
    }

    public void setUsername(String str) {
        str.getClass();
        this.username_ = str;
    }

    public void clearUsername() {
        this.username_ = getDefaultInstance().getUsername();
    }

    public void setUsernameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.username_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public boolean getBlocked() {
        return this.blocked_;
    }

    public void setBlocked(boolean z) {
        this.blocked_ = z;
    }

    public void clearBlocked() {
        this.blocked_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public boolean getWhitelisted() {
        return this.whitelisted_;
    }

    public void setWhitelisted(boolean z) {
        this.whitelisted_ = z;
    }

    public void clearWhitelisted() {
        this.whitelisted_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public boolean getArchived() {
        return this.archived_;
    }

    public void setArchived(boolean z) {
        this.archived_ = z;
    }

    public void clearArchived() {
        this.archived_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public boolean getMarkedUnread() {
        return this.markedUnread_;
    }

    public void setMarkedUnread(boolean z) {
        this.markedUnread_ = z;
    }

    public void clearMarkedUnread() {
        this.markedUnread_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public long getMutedUntilTimestamp() {
        return this.mutedUntilTimestamp_;
    }

    public void setMutedUntilTimestamp(long j) {
        this.mutedUntilTimestamp_ = j;
    }

    public void clearMutedUntilTimestamp() {
        this.mutedUntilTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
    public boolean getHideStory() {
        return this.hideStory_;
    }

    public void setHideStory(boolean z) {
        this.hideStory_ = z;
    }

    public void clearHideStory() {
        this.hideStory_ = false;
    }

    public static ContactRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ContactRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ContactRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ContactRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ContactRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ContactRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ContactRecord parseFrom(InputStream inputStream) throws IOException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ContactRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ContactRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ContactRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ContactRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ContactRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ContactRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ContactRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ContactRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ContactRecord contactRecord) {
        return DEFAULT_INSTANCE.createBuilder(contactRecord);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ContactRecord, Builder> implements ContactRecordOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ContactRecord.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public String getServiceUuid() {
            return ((ContactRecord) this.instance).getServiceUuid();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getServiceUuidBytes() {
            return ((ContactRecord) this.instance).getServiceUuidBytes();
        }

        public Builder setServiceUuid(String str) {
            copyOnWrite();
            ((ContactRecord) this.instance).setServiceUuid(str);
            return this;
        }

        public Builder clearServiceUuid() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearServiceUuid();
            return this;
        }

        public Builder setServiceUuidBytes(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setServiceUuidBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public String getServiceE164() {
            return ((ContactRecord) this.instance).getServiceE164();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getServiceE164Bytes() {
            return ((ContactRecord) this.instance).getServiceE164Bytes();
        }

        public Builder setServiceE164(String str) {
            copyOnWrite();
            ((ContactRecord) this.instance).setServiceE164(str);
            return this;
        }

        public Builder clearServiceE164() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearServiceE164();
            return this;
        }

        public Builder setServiceE164Bytes(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setServiceE164Bytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getProfileKey() {
            return ((ContactRecord) this.instance).getProfileKey();
        }

        public Builder setProfileKey(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setProfileKey(byteString);
            return this;
        }

        public Builder clearProfileKey() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearProfileKey();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getIdentityKey() {
            return ((ContactRecord) this.instance).getIdentityKey();
        }

        public Builder setIdentityKey(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setIdentityKey(byteString);
            return this;
        }

        public Builder clearIdentityKey() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearIdentityKey();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public int getIdentityStateValue() {
            return ((ContactRecord) this.instance).getIdentityStateValue();
        }

        public Builder setIdentityStateValue(int i) {
            copyOnWrite();
            ((ContactRecord) this.instance).setIdentityStateValue(i);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public IdentityState getIdentityState() {
            return ((ContactRecord) this.instance).getIdentityState();
        }

        public Builder setIdentityState(IdentityState identityState) {
            copyOnWrite();
            ((ContactRecord) this.instance).setIdentityState(identityState);
            return this;
        }

        public Builder clearIdentityState() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearIdentityState();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public String getGivenName() {
            return ((ContactRecord) this.instance).getGivenName();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getGivenNameBytes() {
            return ((ContactRecord) this.instance).getGivenNameBytes();
        }

        public Builder setGivenName(String str) {
            copyOnWrite();
            ((ContactRecord) this.instance).setGivenName(str);
            return this;
        }

        public Builder clearGivenName() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearGivenName();
            return this;
        }

        public Builder setGivenNameBytes(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setGivenNameBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public String getFamilyName() {
            return ((ContactRecord) this.instance).getFamilyName();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getFamilyNameBytes() {
            return ((ContactRecord) this.instance).getFamilyNameBytes();
        }

        public Builder setFamilyName(String str) {
            copyOnWrite();
            ((ContactRecord) this.instance).setFamilyName(str);
            return this;
        }

        public Builder clearFamilyName() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearFamilyName();
            return this;
        }

        public Builder setFamilyNameBytes(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setFamilyNameBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public String getUsername() {
            return ((ContactRecord) this.instance).getUsername();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public ByteString getUsernameBytes() {
            return ((ContactRecord) this.instance).getUsernameBytes();
        }

        public Builder setUsername(String str) {
            copyOnWrite();
            ((ContactRecord) this.instance).setUsername(str);
            return this;
        }

        public Builder clearUsername() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearUsername();
            return this;
        }

        public Builder setUsernameBytes(ByteString byteString) {
            copyOnWrite();
            ((ContactRecord) this.instance).setUsernameBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public boolean getBlocked() {
            return ((ContactRecord) this.instance).getBlocked();
        }

        public Builder setBlocked(boolean z) {
            copyOnWrite();
            ((ContactRecord) this.instance).setBlocked(z);
            return this;
        }

        public Builder clearBlocked() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearBlocked();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public boolean getWhitelisted() {
            return ((ContactRecord) this.instance).getWhitelisted();
        }

        public Builder setWhitelisted(boolean z) {
            copyOnWrite();
            ((ContactRecord) this.instance).setWhitelisted(z);
            return this;
        }

        public Builder clearWhitelisted() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearWhitelisted();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public boolean getArchived() {
            return ((ContactRecord) this.instance).getArchived();
        }

        public Builder setArchived(boolean z) {
            copyOnWrite();
            ((ContactRecord) this.instance).setArchived(z);
            return this;
        }

        public Builder clearArchived() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearArchived();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public boolean getMarkedUnread() {
            return ((ContactRecord) this.instance).getMarkedUnread();
        }

        public Builder setMarkedUnread(boolean z) {
            copyOnWrite();
            ((ContactRecord) this.instance).setMarkedUnread(z);
            return this;
        }

        public Builder clearMarkedUnread() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearMarkedUnread();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public long getMutedUntilTimestamp() {
            return ((ContactRecord) this.instance).getMutedUntilTimestamp();
        }

        public Builder setMutedUntilTimestamp(long j) {
            copyOnWrite();
            ((ContactRecord) this.instance).setMutedUntilTimestamp(j);
            return this;
        }

        public Builder clearMutedUntilTimestamp() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearMutedUntilTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.ContactRecordOrBuilder
        public boolean getHideStory() {
            return ((ContactRecord) this.instance).getHideStory();
        }

        public Builder setHideStory(boolean z) {
            copyOnWrite();
            ((ContactRecord) this.instance).setHideStory(z);
            return this;
        }

        public Builder clearHideStory() {
            copyOnWrite();
            ((ContactRecord) this.instance).clearHideStory();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.ContactRecord$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ContactRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u000e\u0000\u0000\u0001\u000e\u000e\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ\u0003\n\u0004\n\u0005\f\u0006Ȉ\u0007Ȉ\bȈ\t\u0007\n\u0007\u000b\u0007\f\u0007\r\u0003\u000e\u0007", new Object[]{"serviceUuid_", "serviceE164_", "profileKey_", "identityKey_", "identityState_", "givenName_", "familyName_", "username_", "blocked_", "whitelisted_", "archived_", "markedUnread_", "mutedUntilTimestamp_", "hideStory_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ContactRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (ContactRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ContactRecord contactRecord = new ContactRecord();
        DEFAULT_INSTANCE = contactRecord;
        GeneratedMessageLite.registerDefaultInstance(ContactRecord.class, contactRecord);
    }

    public static ContactRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ContactRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
