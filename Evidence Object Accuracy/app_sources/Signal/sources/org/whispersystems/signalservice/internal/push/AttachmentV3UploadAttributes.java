package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/* loaded from: classes.dex */
public final class AttachmentV3UploadAttributes {
    @JsonProperty
    private int cdn;
    @JsonProperty
    private Map<String, String> headers;
    @JsonProperty
    private String key;
    @JsonProperty
    private String signedUploadLocation;

    public int getCdn() {
        return this.cdn;
    }

    public String getKey() {
        return this.key;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public String getSignedUploadLocation() {
        return this.signedUploadLocation;
    }
}
