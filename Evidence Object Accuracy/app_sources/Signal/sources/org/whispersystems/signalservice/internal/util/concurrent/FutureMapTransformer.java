package org.whispersystems.signalservice.internal.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.whispersystems.signalservice.internal.util.concurrent.FutureTransformers;
import org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture;

/* loaded from: classes5.dex */
public class FutureMapTransformer<Input, Output> implements ListenableFuture<Output> {
    private final ListenableFuture<Input> future;
    private final FutureTransformers.Transformer<Input, Output> transformer;

    public FutureMapTransformer(ListenableFuture<Input> listenableFuture, FutureTransformers.Transformer<Input, Output> transformer) {
        this.future = listenableFuture;
        this.transformer = transformer;
    }

    @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture
    public void addListener(final ListenableFuture.Listener<Output> listener) {
        this.future.addListener(new ListenableFuture.Listener<Input>() { // from class: org.whispersystems.signalservice.internal.util.concurrent.FutureMapTransformer.1
            @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture.Listener
            public void onSuccess(Input input) {
                try {
                    listener.onSuccess(FutureMapTransformer.this.transformer.transform(input));
                } catch (Exception e) {
                    listener.onFailure(new ExecutionException(e));
                }
            }

            @Override // org.whispersystems.signalservice.internal.util.concurrent.ListenableFuture.Listener
            public void onFailure(ExecutionException executionException) {
                listener.onFailure(executionException);
            }
        });
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        return this.future.cancel(z);
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        return this.future.isCancelled();
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return this.future.isDone();
    }

    @Override // java.util.concurrent.Future
    public Output get() throws InterruptedException, ExecutionException {
        try {
            return this.transformer.transform(this.future.get());
        } catch (Exception e) {
            throw new ExecutionException(e);
        }
    }

    @Override // java.util.concurrent.Future
    public Output get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        try {
            return this.transformer.transform(this.future.get(j, timeUnit));
        } catch (Exception e) {
            throw new ExecutionException(e);
        }
    }
}
