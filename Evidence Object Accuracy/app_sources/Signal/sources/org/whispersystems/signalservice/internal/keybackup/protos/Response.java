package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.whispersystems.signalservice.internal.keybackup.protos.BackupResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.DeleteResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponse;

/* loaded from: classes5.dex */
public final class Response extends GeneratedMessageLite<Response, Builder> implements ResponseOrBuilder {
    public static final int BACKUP_FIELD_NUMBER;
    private static final Response DEFAULT_INSTANCE;
    public static final int DELETE_FIELD_NUMBER;
    private static volatile Parser<Response> PARSER;
    public static final int RESTORE_FIELD_NUMBER;
    private BackupResponse backup_;
    private int bitField0_;
    private DeleteResponse delete_;
    private RestoreResponse restore_;

    private Response() {
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
    public boolean hasBackup() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
    public BackupResponse getBackup() {
        BackupResponse backupResponse = this.backup_;
        return backupResponse == null ? BackupResponse.getDefaultInstance() : backupResponse;
    }

    public void setBackup(BackupResponse backupResponse) {
        backupResponse.getClass();
        this.backup_ = backupResponse;
        this.bitField0_ |= 1;
    }

    public void mergeBackup(BackupResponse backupResponse) {
        backupResponse.getClass();
        BackupResponse backupResponse2 = this.backup_;
        if (backupResponse2 == null || backupResponse2 == BackupResponse.getDefaultInstance()) {
            this.backup_ = backupResponse;
        } else {
            this.backup_ = BackupResponse.newBuilder(this.backup_).mergeFrom((BackupResponse.Builder) backupResponse).buildPartial();
        }
        this.bitField0_ |= 1;
    }

    public void clearBackup() {
        this.backup_ = null;
        this.bitField0_ &= -2;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
    public boolean hasRestore() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
    public RestoreResponse getRestore() {
        RestoreResponse restoreResponse = this.restore_;
        return restoreResponse == null ? RestoreResponse.getDefaultInstance() : restoreResponse;
    }

    public void setRestore(RestoreResponse restoreResponse) {
        restoreResponse.getClass();
        this.restore_ = restoreResponse;
        this.bitField0_ |= 2;
    }

    public void mergeRestore(RestoreResponse restoreResponse) {
        restoreResponse.getClass();
        RestoreResponse restoreResponse2 = this.restore_;
        if (restoreResponse2 == null || restoreResponse2 == RestoreResponse.getDefaultInstance()) {
            this.restore_ = restoreResponse;
        } else {
            this.restore_ = RestoreResponse.newBuilder(this.restore_).mergeFrom((RestoreResponse.Builder) restoreResponse).buildPartial();
        }
        this.bitField0_ |= 2;
    }

    public void clearRestore() {
        this.restore_ = null;
        this.bitField0_ &= -3;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
    public boolean hasDelete() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
    public DeleteResponse getDelete() {
        DeleteResponse deleteResponse = this.delete_;
        return deleteResponse == null ? DeleteResponse.getDefaultInstance() : deleteResponse;
    }

    public void setDelete(DeleteResponse deleteResponse) {
        deleteResponse.getClass();
        this.delete_ = deleteResponse;
        this.bitField0_ |= 4;
    }

    public void mergeDelete(DeleteResponse deleteResponse) {
        deleteResponse.getClass();
        DeleteResponse deleteResponse2 = this.delete_;
        if (deleteResponse2 == null || deleteResponse2 == DeleteResponse.getDefaultInstance()) {
            this.delete_ = deleteResponse;
        } else {
            this.delete_ = DeleteResponse.newBuilder(this.delete_).mergeFrom((DeleteResponse.Builder) deleteResponse).buildPartial();
        }
        this.bitField0_ |= 4;
    }

    public void clearDelete() {
        this.delete_ = null;
        this.bitField0_ &= -5;
    }

    public static Response parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Response parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Response parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Response parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Response parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Response parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Response parseFrom(InputStream inputStream) throws IOException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Response parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Response parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Response) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Response parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Response) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Response parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Response parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Response) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Response response) {
        return DEFAULT_INSTANCE.createBuilder(response);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Response, Builder> implements ResponseOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(Response.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
        public boolean hasBackup() {
            return ((Response) this.instance).hasBackup();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
        public BackupResponse getBackup() {
            return ((Response) this.instance).getBackup();
        }

        public Builder setBackup(BackupResponse backupResponse) {
            copyOnWrite();
            ((Response) this.instance).setBackup(backupResponse);
            return this;
        }

        public Builder setBackup(BackupResponse.Builder builder) {
            copyOnWrite();
            ((Response) this.instance).setBackup(builder.build());
            return this;
        }

        public Builder mergeBackup(BackupResponse backupResponse) {
            copyOnWrite();
            ((Response) this.instance).mergeBackup(backupResponse);
            return this;
        }

        public Builder clearBackup() {
            copyOnWrite();
            ((Response) this.instance).clearBackup();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
        public boolean hasRestore() {
            return ((Response) this.instance).hasRestore();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
        public RestoreResponse getRestore() {
            return ((Response) this.instance).getRestore();
        }

        public Builder setRestore(RestoreResponse restoreResponse) {
            copyOnWrite();
            ((Response) this.instance).setRestore(restoreResponse);
            return this;
        }

        public Builder setRestore(RestoreResponse.Builder builder) {
            copyOnWrite();
            ((Response) this.instance).setRestore(builder.build());
            return this;
        }

        public Builder mergeRestore(RestoreResponse restoreResponse) {
            copyOnWrite();
            ((Response) this.instance).mergeRestore(restoreResponse);
            return this;
        }

        public Builder clearRestore() {
            copyOnWrite();
            ((Response) this.instance).clearRestore();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
        public boolean hasDelete() {
            return ((Response) this.instance).hasDelete();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.ResponseOrBuilder
        public DeleteResponse getDelete() {
            return ((Response) this.instance).getDelete();
        }

        public Builder setDelete(DeleteResponse deleteResponse) {
            copyOnWrite();
            ((Response) this.instance).setDelete(deleteResponse);
            return this;
        }

        public Builder setDelete(DeleteResponse.Builder builder) {
            copyOnWrite();
            ((Response) this.instance).setDelete(builder.build());
            return this;
        }

        public Builder mergeDelete(DeleteResponse deleteResponse) {
            copyOnWrite();
            ((Response) this.instance).mergeDelete(deleteResponse);
            return this;
        }

        public Builder clearDelete() {
            copyOnWrite();
            ((Response) this.instance).clearDelete();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.Response$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Response();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"bitField0_", "backup_", "restore_", "delete_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Response> parser = PARSER;
                if (parser == null) {
                    synchronized (Response.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Response response = new Response();
        DEFAULT_INSTANCE = response;
        GeneratedMessageLite.registerDefaultInstance(Response.class, response);
    }

    public static Response getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Response> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
