package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class GroupStaleDevices {
    @JsonProperty
    private StaleDevices devices;
    @JsonProperty
    private String uuid;

    public String getUuid() {
        return this.uuid;
    }

    public StaleDevices getDevices() {
        return this.devices;
    }
}
