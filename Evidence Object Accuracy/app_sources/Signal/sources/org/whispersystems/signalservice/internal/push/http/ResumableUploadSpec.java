package org.whispersystems.signalservice.internal.push.http;

import com.google.protobuf.ByteString;
import java.io.IOException;
import org.signal.protos.resumableuploads.ResumableUploads$ResumableUpload;
import org.whispersystems.signalservice.api.push.exceptions.ResumeLocationInvalidException;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public final class ResumableUploadSpec {
    private final String cdnKey;
    private final Integer cdnNumber;
    private final Long expirationTimestamp;
    private final byte[] iv;
    private final String resumeLocation;
    private final byte[] secretKey;

    public ResumableUploadSpec(byte[] bArr, byte[] bArr2, String str, int i, String str2, long j) {
        this.secretKey = bArr;
        this.iv = bArr2;
        this.cdnKey = str;
        this.cdnNumber = Integer.valueOf(i);
        this.resumeLocation = str2;
        this.expirationTimestamp = Long.valueOf(j);
    }

    public byte[] getSecretKey() {
        return this.secretKey;
    }

    public byte[] getIV() {
        return this.iv;
    }

    public String getCdnKey() {
        return this.cdnKey;
    }

    public Integer getCdnNumber() {
        return this.cdnNumber;
    }

    public String getResumeLocation() {
        return this.resumeLocation;
    }

    public Long getExpirationTimestamp() {
        return this.expirationTimestamp;
    }

    public String serialize() {
        return Base64.encodeBytes(ResumableUploads$ResumableUpload.newBuilder().setSecretKey(ByteString.copyFrom(getSecretKey())).setIv(ByteString.copyFrom(getIV())).setTimeout(getExpirationTimestamp().longValue()).setCdnNumber(getCdnNumber().intValue()).setCdnKey(getCdnKey()).setLocation(getResumeLocation()).setTimeout(getExpirationTimestamp().longValue()).build().toByteArray());
    }

    public static ResumableUploadSpec deserialize(String str) throws ResumeLocationInvalidException {
        if (str == null) {
            return null;
        }
        try {
            ResumableUploads$ResumableUpload parseFrom = ResumableUploads$ResumableUpload.parseFrom(ByteString.copyFrom(Base64.decode(str)));
            return new ResumableUploadSpec(parseFrom.getSecretKey().toByteArray(), parseFrom.getIv().toByteArray(), parseFrom.getCdnKey(), parseFrom.getCdnNumber(), parseFrom.getLocation(), parseFrom.getTimeout());
        } catch (IOException unused) {
            throw new ResumeLocationInvalidException();
        }
    }
}
