package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import java.util.List;
import org.whispersystems.signalservice.internal.storage.protos.AccountRecord;

/* loaded from: classes5.dex */
public interface AccountRecordOrBuilder extends MessageLiteOrBuilder {
    String getAvatarUrlPath();

    ByteString getAvatarUrlPathBytes();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    boolean getDisplayBadgesOnProfile();

    String getE164();

    ByteString getE164Bytes();

    String getFamilyName();

    ByteString getFamilyNameBytes();

    String getGivenName();

    ByteString getGivenNameBytes();

    boolean getLinkPreviews();

    boolean getNoteToSelfArchived();

    boolean getNoteToSelfMarkedUnread();

    Payments getPayments();

    AccountRecord.PhoneNumberSharingMode getPhoneNumberSharingMode();

    int getPhoneNumberSharingModeValue();

    AccountRecord.PinnedConversation getPinnedConversations(int i);

    int getPinnedConversationsCount();

    List<AccountRecord.PinnedConversation> getPinnedConversationsList();

    boolean getPreferContactAvatars();

    String getPreferredReactionEmoji(int i);

    ByteString getPreferredReactionEmojiBytes(int i);

    int getPreferredReactionEmojiCount();

    List<String> getPreferredReactionEmojiList();

    boolean getPrimarySendsSms();

    ByteString getProfileKey();

    boolean getProxiedLinkPreviews();

    boolean getReadReceipts();

    boolean getSealedSenderIndicators();

    String getSubscriberCurrencyCode();

    ByteString getSubscriberCurrencyCodeBytes();

    ByteString getSubscriberId();

    boolean getSubscriptionManuallyCancelled();

    boolean getTypingIndicators();

    int getUniversalExpireTimer();

    boolean getUnlistedPhoneNumber();

    boolean hasPayments();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
