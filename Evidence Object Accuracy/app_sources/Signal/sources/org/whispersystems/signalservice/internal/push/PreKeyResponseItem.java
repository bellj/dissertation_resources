package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whispersystems.signalservice.api.push.SignedPreKeyEntity;

/* loaded from: classes.dex */
public class PreKeyResponseItem {
    @JsonProperty
    private int deviceId;
    @JsonProperty
    private PreKeyEntity preKey;
    @JsonProperty
    private int registrationId;
    @JsonProperty
    private SignedPreKeyEntity signedPreKey;

    public int getDeviceId() {
        return this.deviceId;
    }

    public int getRegistrationId() {
        return this.registrationId;
    }

    public SignedPreKeyEntity getSignedPreKey() {
        return this.signedPreKey;
    }

    public PreKeyEntity getPreKey() {
        return this.preKey;
    }
}
