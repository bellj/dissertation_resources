package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whispersystems.signalservice.internal.util.Hex;

/* loaded from: classes.dex */
public class KeyBackupResponse {
    @JsonProperty
    private byte[] data;
    @JsonProperty
    private byte[] iv;
    @JsonProperty
    private byte[] mac;

    public KeyBackupResponse() {
    }

    public KeyBackupResponse(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        this.iv = bArr;
        this.data = bArr2;
        this.mac = bArr3;
    }

    public byte[] getIv() {
        return this.iv;
    }

    public byte[] getData() {
        return this.data;
    }

    public byte[] getMac() {
        return this.mac;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{iv: ");
        byte[] bArr = this.iv;
        String str = null;
        sb.append(bArr == null ? null : Hex.toString(bArr));
        sb.append(", data: ");
        byte[] bArr2 = this.data;
        sb.append(bArr2 == null ? null : Hex.toString(bArr2));
        sb.append(", mac: ");
        byte[] bArr3 = this.mac;
        if (bArr3 != null) {
            str = Hex.toString(bArr3);
        }
        sb.append(str);
        sb.append("}");
        return sb.toString();
    }
}
