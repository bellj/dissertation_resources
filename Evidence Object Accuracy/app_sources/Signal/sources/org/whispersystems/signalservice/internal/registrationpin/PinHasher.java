package org.whispersystems.signalservice.internal.registrationpin;

import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import org.whispersystems.signalservice.api.kbs.HashedPin;

/* loaded from: classes5.dex */
public final class PinHasher {

    /* loaded from: classes5.dex */
    public interface Argon2 {
        byte[] hash(byte[] bArr);
    }

    public static byte[] normalize(String str) {
        String trim = str.trim();
        if (PinString.allNumeric(trim)) {
            trim = PinString.toArabic(trim);
        }
        return Normalizer.normalize(trim, Normalizer.Form.NFKD).getBytes(StandardCharsets.UTF_8);
    }

    public static HashedPin hashPin(byte[] bArr, Argon2 argon2) {
        return HashedPin.fromArgon2Hash(argon2.hash(bArr));
    }
}
