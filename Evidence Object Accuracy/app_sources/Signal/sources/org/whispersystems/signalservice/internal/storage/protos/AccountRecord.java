package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.whispersystems.signalservice.internal.storage.protos.Payments;

/* loaded from: classes5.dex */
public final class AccountRecord extends GeneratedMessageLite<AccountRecord, Builder> implements AccountRecordOrBuilder {
    public static final int AVATARURLPATH_FIELD_NUMBER;
    private static final AccountRecord DEFAULT_INSTANCE;
    public static final int DISPLAYBADGESONPROFILE_FIELD_NUMBER;
    public static final int E164_FIELD_NUMBER;
    public static final int FAMILYNAME_FIELD_NUMBER;
    public static final int GIVENNAME_FIELD_NUMBER;
    public static final int LINKPREVIEWS_FIELD_NUMBER;
    public static final int NOTETOSELFARCHIVED_FIELD_NUMBER;
    public static final int NOTETOSELFMARKEDUNREAD_FIELD_NUMBER;
    private static volatile Parser<AccountRecord> PARSER;
    public static final int PAYMENTS_FIELD_NUMBER;
    public static final int PHONENUMBERSHARINGMODE_FIELD_NUMBER;
    public static final int PINNEDCONVERSATIONS_FIELD_NUMBER;
    public static final int PREFERCONTACTAVATARS_FIELD_NUMBER;
    public static final int PREFERREDREACTIONEMOJI_FIELD_NUMBER;
    public static final int PRIMARYSENDSSMS_FIELD_NUMBER;
    public static final int PROFILEKEY_FIELD_NUMBER;
    public static final int PROXIEDLINKPREVIEWS_FIELD_NUMBER;
    public static final int READRECEIPTS_FIELD_NUMBER;
    public static final int SEALEDSENDERINDICATORS_FIELD_NUMBER;
    public static final int SUBSCRIBERCURRENCYCODE_FIELD_NUMBER;
    public static final int SUBSCRIBERID_FIELD_NUMBER;
    public static final int SUBSCRIPTIONMANUALLYCANCELLED_FIELD_NUMBER;
    public static final int TYPINGINDICATORS_FIELD_NUMBER;
    public static final int UNIVERSALEXPIRETIMER_FIELD_NUMBER;
    public static final int UNLISTEDPHONENUMBER_FIELD_NUMBER;
    private String avatarUrlPath_ = "";
    private boolean displayBadgesOnProfile_;
    private String e164_ = "";
    private String familyName_ = "";
    private String givenName_ = "";
    private boolean linkPreviews_;
    private boolean noteToSelfArchived_;
    private boolean noteToSelfMarkedUnread_;
    private Payments payments_;
    private int phoneNumberSharingMode_;
    private Internal.ProtobufList<PinnedConversation> pinnedConversations_ = GeneratedMessageLite.emptyProtobufList();
    private boolean preferContactAvatars_;
    private Internal.ProtobufList<String> preferredReactionEmoji_ = GeneratedMessageLite.emptyProtobufList();
    private boolean primarySendsSms_;
    private ByteString profileKey_;
    private boolean proxiedLinkPreviews_;
    private boolean readReceipts_;
    private boolean sealedSenderIndicators_;
    private String subscriberCurrencyCode_;
    private ByteString subscriberId_;
    private boolean subscriptionManuallyCancelled_;
    private boolean typingIndicators_;
    private int universalExpireTimer_;
    private boolean unlistedPhoneNumber_;

    /* loaded from: classes5.dex */
    public interface PinnedConversationOrBuilder extends MessageLiteOrBuilder {
        PinnedConversation.Contact getContact();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getGroupMasterKey();

        PinnedConversation.IdentifierCase getIdentifierCase();

        ByteString getLegacyGroupId();

        boolean hasContact();

        boolean hasGroupMasterKey();

        boolean hasLegacyGroupId();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    private AccountRecord() {
        ByteString byteString = ByteString.EMPTY;
        this.profileKey_ = byteString;
        this.subscriberId_ = byteString;
        this.subscriberCurrencyCode_ = "";
    }

    /* loaded from: classes5.dex */
    public enum PhoneNumberSharingMode implements Internal.EnumLite {
        EVERYBODY(0),
        CONTACTS_ONLY(1),
        NOBODY(2),
        UNRECOGNIZED(-1);
        
        public static final int CONTACTS_ONLY_VALUE;
        public static final int EVERYBODY_VALUE;
        public static final int NOBODY_VALUE;
        private static final Internal.EnumLiteMap<PhoneNumberSharingMode> internalValueMap = new Internal.EnumLiteMap<PhoneNumberSharingMode>() { // from class: org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PhoneNumberSharingMode.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public PhoneNumberSharingMode findValueByNumber(int i) {
                return PhoneNumberSharingMode.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        @Deprecated
        public static PhoneNumberSharingMode valueOf(int i) {
            return forNumber(i);
        }

        public static PhoneNumberSharingMode forNumber(int i) {
            if (i == 0) {
                return EVERYBODY;
            }
            if (i == 1) {
                return CONTACTS_ONLY;
            }
            if (i != 2) {
                return null;
            }
            return NOBODY;
        }

        public static Internal.EnumLiteMap<PhoneNumberSharingMode> internalGetValueMap() {
            return internalValueMap;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return PhoneNumberSharingModeVerifier.INSTANCE;
        }

        /* loaded from: classes5.dex */
        private static final class PhoneNumberSharingModeVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new PhoneNumberSharingModeVerifier();

            private PhoneNumberSharingModeVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return PhoneNumberSharingMode.forNumber(i) != null;
            }
        }

        PhoneNumberSharingMode(int i) {
            this.value = i;
        }
    }

    /* loaded from: classes5.dex */
    public static final class PinnedConversation extends GeneratedMessageLite<PinnedConversation, Builder> implements PinnedConversationOrBuilder {
        public static final int CONTACT_FIELD_NUMBER;
        private static final PinnedConversation DEFAULT_INSTANCE;
        public static final int GROUPMASTERKEY_FIELD_NUMBER;
        public static final int LEGACYGROUPID_FIELD_NUMBER;
        private static volatile Parser<PinnedConversation> PARSER;
        private int identifierCase_ = 0;
        private Object identifier_;

        /* loaded from: classes5.dex */
        public interface ContactOrBuilder extends MessageLiteOrBuilder {
            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ MessageLite getDefaultInstanceForType();

            String getE164();

            ByteString getE164Bytes();

            String getUuid();

            ByteString getUuidBytes();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ boolean isInitialized();
        }

        private PinnedConversation() {
        }

        /* loaded from: classes5.dex */
        public static final class Contact extends GeneratedMessageLite<Contact, Builder> implements ContactOrBuilder {
            private static final Contact DEFAULT_INSTANCE;
            public static final int E164_FIELD_NUMBER;
            private static volatile Parser<Contact> PARSER;
            public static final int UUID_FIELD_NUMBER;
            private String e164_ = "";
            private String uuid_ = "";

            private Contact() {
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
            public String getUuid() {
                return this.uuid_;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
            public ByteString getUuidBytes() {
                return ByteString.copyFromUtf8(this.uuid_);
            }

            public void setUuid(String str) {
                str.getClass();
                this.uuid_ = str;
            }

            public void clearUuid() {
                this.uuid_ = getDefaultInstance().getUuid();
            }

            public void setUuidBytes(ByteString byteString) {
                AbstractMessageLite.checkByteStringIsUtf8(byteString);
                this.uuid_ = byteString.toStringUtf8();
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
            public String getE164() {
                return this.e164_;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
            public ByteString getE164Bytes() {
                return ByteString.copyFromUtf8(this.e164_);
            }

            public void setE164(String str) {
                str.getClass();
                this.e164_ = str;
            }

            public void clearE164() {
                this.e164_ = getDefaultInstance().getE164();
            }

            public void setE164Bytes(ByteString byteString) {
                AbstractMessageLite.checkByteStringIsUtf8(byteString);
                this.e164_ = byteString.toStringUtf8();
            }

            public static Contact parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static Contact parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static Contact parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static Contact parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static Contact parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static Contact parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static Contact parseFrom(InputStream inputStream) throws IOException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static Contact parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static Contact parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (Contact) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static Contact parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Contact) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static Contact parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static Contact parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (Contact) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(Contact contact) {
                return DEFAULT_INSTANCE.createBuilder(contact);
            }

            /* loaded from: classes5.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<Contact, Builder> implements ContactOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(Contact.DEFAULT_INSTANCE);
                }

                @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
                public String getUuid() {
                    return ((Contact) this.instance).getUuid();
                }

                @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
                public ByteString getUuidBytes() {
                    return ((Contact) this.instance).getUuidBytes();
                }

                public Builder setUuid(String str) {
                    copyOnWrite();
                    ((Contact) this.instance).setUuid(str);
                    return this;
                }

                public Builder clearUuid() {
                    copyOnWrite();
                    ((Contact) this.instance).clearUuid();
                    return this;
                }

                public Builder setUuidBytes(ByteString byteString) {
                    copyOnWrite();
                    ((Contact) this.instance).setUuidBytes(byteString);
                    return this;
                }

                @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
                public String getE164() {
                    return ((Contact) this.instance).getE164();
                }

                @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversation.ContactOrBuilder
                public ByteString getE164Bytes() {
                    return ((Contact) this.instance).getE164Bytes();
                }

                public Builder setE164(String str) {
                    copyOnWrite();
                    ((Contact) this.instance).setE164(str);
                    return this;
                }

                public Builder clearE164() {
                    copyOnWrite();
                    ((Contact) this.instance).clearE164();
                    return this;
                }

                public Builder setE164Bytes(ByteString byteString) {
                    copyOnWrite();
                    ((Contact) this.instance).setE164Bytes(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new Contact();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ", new Object[]{"uuid_", "e164_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<Contact> parser = PARSER;
                        if (parser == null) {
                            synchronized (Contact.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                Contact contact = new Contact();
                DEFAULT_INSTANCE = contact;
                GeneratedMessageLite.registerDefaultInstance(Contact.class, contact);
            }

            public static Contact getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<Contact> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes5.dex */
        public enum IdentifierCase {
            CONTACT(1),
            LEGACYGROUPID(3),
            GROUPMASTERKEY(4),
            IDENTIFIER_NOT_SET(0);
            
            private final int value;

            IdentifierCase(int i) {
                this.value = i;
            }

            @Deprecated
            public static IdentifierCase valueOf(int i) {
                return forNumber(i);
            }

            public static IdentifierCase forNumber(int i) {
                if (i == 0) {
                    return IDENTIFIER_NOT_SET;
                }
                if (i == 1) {
                    return CONTACT;
                }
                if (i == 3) {
                    return LEGACYGROUPID;
                }
                if (i != 4) {
                    return null;
                }
                return GROUPMASTERKEY;
            }

            public int getNumber() {
                return this.value;
            }
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public IdentifierCase getIdentifierCase() {
            return IdentifierCase.forNumber(this.identifierCase_);
        }

        public void clearIdentifier() {
            this.identifierCase_ = 0;
            this.identifier_ = null;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public boolean hasContact() {
            return this.identifierCase_ == 1;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public Contact getContact() {
            if (this.identifierCase_ == 1) {
                return (Contact) this.identifier_;
            }
            return Contact.getDefaultInstance();
        }

        public void setContact(Contact contact) {
            contact.getClass();
            this.identifier_ = contact;
            this.identifierCase_ = 1;
        }

        public void mergeContact(Contact contact) {
            contact.getClass();
            if (this.identifierCase_ != 1 || this.identifier_ == Contact.getDefaultInstance()) {
                this.identifier_ = contact;
            } else {
                this.identifier_ = Contact.newBuilder((Contact) this.identifier_).mergeFrom((Contact.Builder) contact).buildPartial();
            }
            this.identifierCase_ = 1;
        }

        public void clearContact() {
            if (this.identifierCase_ == 1) {
                this.identifierCase_ = 0;
                this.identifier_ = null;
            }
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public boolean hasLegacyGroupId() {
            return this.identifierCase_ == 3;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public ByteString getLegacyGroupId() {
            if (this.identifierCase_ == 3) {
                return (ByteString) this.identifier_;
            }
            return ByteString.EMPTY;
        }

        public void setLegacyGroupId(ByteString byteString) {
            byteString.getClass();
            this.identifierCase_ = 3;
            this.identifier_ = byteString;
        }

        public void clearLegacyGroupId() {
            if (this.identifierCase_ == 3) {
                this.identifierCase_ = 0;
                this.identifier_ = null;
            }
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public boolean hasGroupMasterKey() {
            return this.identifierCase_ == 4;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
        public ByteString getGroupMasterKey() {
            if (this.identifierCase_ == 4) {
                return (ByteString) this.identifier_;
            }
            return ByteString.EMPTY;
        }

        public void setGroupMasterKey(ByteString byteString) {
            byteString.getClass();
            this.identifierCase_ = 4;
            this.identifier_ = byteString;
        }

        public void clearGroupMasterKey() {
            if (this.identifierCase_ == 4) {
                this.identifierCase_ = 0;
                this.identifier_ = null;
            }
        }

        public static PinnedConversation parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static PinnedConversation parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static PinnedConversation parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static PinnedConversation parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static PinnedConversation parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static PinnedConversation parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static PinnedConversation parseFrom(InputStream inputStream) throws IOException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static PinnedConversation parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static PinnedConversation parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (PinnedConversation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static PinnedConversation parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (PinnedConversation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static PinnedConversation parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static PinnedConversation parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (PinnedConversation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(PinnedConversation pinnedConversation) {
            return DEFAULT_INSTANCE.createBuilder(pinnedConversation);
        }

        /* loaded from: classes5.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<PinnedConversation, Builder> implements PinnedConversationOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(PinnedConversation.DEFAULT_INSTANCE);
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public IdentifierCase getIdentifierCase() {
                return ((PinnedConversation) this.instance).getIdentifierCase();
            }

            public Builder clearIdentifier() {
                copyOnWrite();
                ((PinnedConversation) this.instance).clearIdentifier();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public boolean hasContact() {
                return ((PinnedConversation) this.instance).hasContact();
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public Contact getContact() {
                return ((PinnedConversation) this.instance).getContact();
            }

            public Builder setContact(Contact contact) {
                copyOnWrite();
                ((PinnedConversation) this.instance).setContact(contact);
                return this;
            }

            public Builder setContact(Contact.Builder builder) {
                copyOnWrite();
                ((PinnedConversation) this.instance).setContact(builder.build());
                return this;
            }

            public Builder mergeContact(Contact contact) {
                copyOnWrite();
                ((PinnedConversation) this.instance).mergeContact(contact);
                return this;
            }

            public Builder clearContact() {
                copyOnWrite();
                ((PinnedConversation) this.instance).clearContact();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public boolean hasLegacyGroupId() {
                return ((PinnedConversation) this.instance).hasLegacyGroupId();
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public ByteString getLegacyGroupId() {
                return ((PinnedConversation) this.instance).getLegacyGroupId();
            }

            public Builder setLegacyGroupId(ByteString byteString) {
                copyOnWrite();
                ((PinnedConversation) this.instance).setLegacyGroupId(byteString);
                return this;
            }

            public Builder clearLegacyGroupId() {
                copyOnWrite();
                ((PinnedConversation) this.instance).clearLegacyGroupId();
                return this;
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public boolean hasGroupMasterKey() {
                return ((PinnedConversation) this.instance).hasGroupMasterKey();
            }

            @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecord.PinnedConversationOrBuilder
            public ByteString getGroupMasterKey() {
                return ((PinnedConversation) this.instance).getGroupMasterKey();
            }

            public Builder setGroupMasterKey(ByteString byteString) {
                copyOnWrite();
                ((PinnedConversation) this.instance).setGroupMasterKey(byteString);
                return this;
            }

            public Builder clearGroupMasterKey() {
                copyOnWrite();
                ((PinnedConversation) this.instance).clearGroupMasterKey();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new PinnedConversation();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0001\u0000\u0001\u0004\u0003\u0000\u0000\u0000\u0001<\u0000\u0003=\u0000\u0004=\u0000", new Object[]{"identifier_", "identifierCase_", Contact.class});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<PinnedConversation> parser = PARSER;
                    if (parser == null) {
                        synchronized (PinnedConversation.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            PinnedConversation pinnedConversation = new PinnedConversation();
            DEFAULT_INSTANCE = pinnedConversation;
            GeneratedMessageLite.registerDefaultInstance(PinnedConversation.class, pinnedConversation);
        }

        public static PinnedConversation getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<PinnedConversation> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.AccountRecord$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getProfileKey() {
        return this.profileKey_;
    }

    public void setProfileKey(ByteString byteString) {
        byteString.getClass();
        this.profileKey_ = byteString;
    }

    public void clearProfileKey() {
        this.profileKey_ = getDefaultInstance().getProfileKey();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public String getGivenName() {
        return this.givenName_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getGivenNameBytes() {
        return ByteString.copyFromUtf8(this.givenName_);
    }

    public void setGivenName(String str) {
        str.getClass();
        this.givenName_ = str;
    }

    public void clearGivenName() {
        this.givenName_ = getDefaultInstance().getGivenName();
    }

    public void setGivenNameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.givenName_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public String getFamilyName() {
        return this.familyName_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getFamilyNameBytes() {
        return ByteString.copyFromUtf8(this.familyName_);
    }

    public void setFamilyName(String str) {
        str.getClass();
        this.familyName_ = str;
    }

    public void clearFamilyName() {
        this.familyName_ = getDefaultInstance().getFamilyName();
    }

    public void setFamilyNameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.familyName_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public String getAvatarUrlPath() {
        return this.avatarUrlPath_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getAvatarUrlPathBytes() {
        return ByteString.copyFromUtf8(this.avatarUrlPath_);
    }

    public void setAvatarUrlPath(String str) {
        str.getClass();
        this.avatarUrlPath_ = str;
    }

    public void clearAvatarUrlPath() {
        this.avatarUrlPath_ = getDefaultInstance().getAvatarUrlPath();
    }

    public void setAvatarUrlPathBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.avatarUrlPath_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getNoteToSelfArchived() {
        return this.noteToSelfArchived_;
    }

    public void setNoteToSelfArchived(boolean z) {
        this.noteToSelfArchived_ = z;
    }

    public void clearNoteToSelfArchived() {
        this.noteToSelfArchived_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getReadReceipts() {
        return this.readReceipts_;
    }

    public void setReadReceipts(boolean z) {
        this.readReceipts_ = z;
    }

    public void clearReadReceipts() {
        this.readReceipts_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getSealedSenderIndicators() {
        return this.sealedSenderIndicators_;
    }

    public void setSealedSenderIndicators(boolean z) {
        this.sealedSenderIndicators_ = z;
    }

    public void clearSealedSenderIndicators() {
        this.sealedSenderIndicators_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getTypingIndicators() {
        return this.typingIndicators_;
    }

    public void setTypingIndicators(boolean z) {
        this.typingIndicators_ = z;
    }

    public void clearTypingIndicators() {
        this.typingIndicators_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getProxiedLinkPreviews() {
        return this.proxiedLinkPreviews_;
    }

    public void setProxiedLinkPreviews(boolean z) {
        this.proxiedLinkPreviews_ = z;
    }

    public void clearProxiedLinkPreviews() {
        this.proxiedLinkPreviews_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getNoteToSelfMarkedUnread() {
        return this.noteToSelfMarkedUnread_;
    }

    public void setNoteToSelfMarkedUnread(boolean z) {
        this.noteToSelfMarkedUnread_ = z;
    }

    public void clearNoteToSelfMarkedUnread() {
        this.noteToSelfMarkedUnread_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getLinkPreviews() {
        return this.linkPreviews_;
    }

    public void setLinkPreviews(boolean z) {
        this.linkPreviews_ = z;
    }

    public void clearLinkPreviews() {
        this.linkPreviews_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public int getPhoneNumberSharingModeValue() {
        return this.phoneNumberSharingMode_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public PhoneNumberSharingMode getPhoneNumberSharingMode() {
        PhoneNumberSharingMode forNumber = PhoneNumberSharingMode.forNumber(this.phoneNumberSharingMode_);
        return forNumber == null ? PhoneNumberSharingMode.UNRECOGNIZED : forNumber;
    }

    public void setPhoneNumberSharingModeValue(int i) {
        this.phoneNumberSharingMode_ = i;
    }

    public void setPhoneNumberSharingMode(PhoneNumberSharingMode phoneNumberSharingMode) {
        this.phoneNumberSharingMode_ = phoneNumberSharingMode.getNumber();
    }

    public void clearPhoneNumberSharingMode() {
        this.phoneNumberSharingMode_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getUnlistedPhoneNumber() {
        return this.unlistedPhoneNumber_;
    }

    public void setUnlistedPhoneNumber(boolean z) {
        this.unlistedPhoneNumber_ = z;
    }

    public void clearUnlistedPhoneNumber() {
        this.unlistedPhoneNumber_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public List<PinnedConversation> getPinnedConversationsList() {
        return this.pinnedConversations_;
    }

    public List<? extends PinnedConversationOrBuilder> getPinnedConversationsOrBuilderList() {
        return this.pinnedConversations_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public int getPinnedConversationsCount() {
        return this.pinnedConversations_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public PinnedConversation getPinnedConversations(int i) {
        return this.pinnedConversations_.get(i);
    }

    public PinnedConversationOrBuilder getPinnedConversationsOrBuilder(int i) {
        return this.pinnedConversations_.get(i);
    }

    private void ensurePinnedConversationsIsMutable() {
        Internal.ProtobufList<PinnedConversation> protobufList = this.pinnedConversations_;
        if (!protobufList.isModifiable()) {
            this.pinnedConversations_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPinnedConversations(int i, PinnedConversation pinnedConversation) {
        pinnedConversation.getClass();
        ensurePinnedConversationsIsMutable();
        this.pinnedConversations_.set(i, pinnedConversation);
    }

    public void addPinnedConversations(PinnedConversation pinnedConversation) {
        pinnedConversation.getClass();
        ensurePinnedConversationsIsMutable();
        this.pinnedConversations_.add(pinnedConversation);
    }

    public void addPinnedConversations(int i, PinnedConversation pinnedConversation) {
        pinnedConversation.getClass();
        ensurePinnedConversationsIsMutable();
        this.pinnedConversations_.add(i, pinnedConversation);
    }

    public void addAllPinnedConversations(Iterable<? extends PinnedConversation> iterable) {
        ensurePinnedConversationsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.pinnedConversations_);
    }

    public void clearPinnedConversations() {
        this.pinnedConversations_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePinnedConversations(int i) {
        ensurePinnedConversationsIsMutable();
        this.pinnedConversations_.remove(i);
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getPreferContactAvatars() {
        return this.preferContactAvatars_;
    }

    public void setPreferContactAvatars(boolean z) {
        this.preferContactAvatars_ = z;
    }

    public void clearPreferContactAvatars() {
        this.preferContactAvatars_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean hasPayments() {
        return this.payments_ != null;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public Payments getPayments() {
        Payments payments = this.payments_;
        return payments == null ? Payments.getDefaultInstance() : payments;
    }

    public void setPayments(Payments payments) {
        payments.getClass();
        this.payments_ = payments;
    }

    public void mergePayments(Payments payments) {
        payments.getClass();
        Payments payments2 = this.payments_;
        if (payments2 == null || payments2 == Payments.getDefaultInstance()) {
            this.payments_ = payments;
        } else {
            this.payments_ = Payments.newBuilder(this.payments_).mergeFrom((Payments.Builder) payments).buildPartial();
        }
    }

    public void clearPayments() {
        this.payments_ = null;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public int getUniversalExpireTimer() {
        return this.universalExpireTimer_;
    }

    public void setUniversalExpireTimer(int i) {
        this.universalExpireTimer_ = i;
    }

    public void clearUniversalExpireTimer() {
        this.universalExpireTimer_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getPrimarySendsSms() {
        return this.primarySendsSms_;
    }

    public void setPrimarySendsSms(boolean z) {
        this.primarySendsSms_ = z;
    }

    public void clearPrimarySendsSms() {
        this.primarySendsSms_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public String getE164() {
        return this.e164_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getE164Bytes() {
        return ByteString.copyFromUtf8(this.e164_);
    }

    public void setE164(String str) {
        str.getClass();
        this.e164_ = str;
    }

    public void clearE164() {
        this.e164_ = getDefaultInstance().getE164();
    }

    public void setE164Bytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.e164_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public List<String> getPreferredReactionEmojiList() {
        return this.preferredReactionEmoji_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public int getPreferredReactionEmojiCount() {
        return this.preferredReactionEmoji_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public String getPreferredReactionEmoji(int i) {
        return this.preferredReactionEmoji_.get(i);
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getPreferredReactionEmojiBytes(int i) {
        return ByteString.copyFromUtf8(this.preferredReactionEmoji_.get(i));
    }

    private void ensurePreferredReactionEmojiIsMutable() {
        Internal.ProtobufList<String> protobufList = this.preferredReactionEmoji_;
        if (!protobufList.isModifiable()) {
            this.preferredReactionEmoji_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPreferredReactionEmoji(int i, String str) {
        str.getClass();
        ensurePreferredReactionEmojiIsMutable();
        this.preferredReactionEmoji_.set(i, str);
    }

    public void addPreferredReactionEmoji(String str) {
        str.getClass();
        ensurePreferredReactionEmojiIsMutable();
        this.preferredReactionEmoji_.add(str);
    }

    public void addAllPreferredReactionEmoji(Iterable<String> iterable) {
        ensurePreferredReactionEmojiIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.preferredReactionEmoji_);
    }

    public void clearPreferredReactionEmoji() {
        this.preferredReactionEmoji_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addPreferredReactionEmojiBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        ensurePreferredReactionEmojiIsMutable();
        this.preferredReactionEmoji_.add(byteString.toStringUtf8());
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getSubscriberId() {
        return this.subscriberId_;
    }

    public void setSubscriberId(ByteString byteString) {
        byteString.getClass();
        this.subscriberId_ = byteString;
    }

    public void clearSubscriberId() {
        this.subscriberId_ = getDefaultInstance().getSubscriberId();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public String getSubscriberCurrencyCode() {
        return this.subscriberCurrencyCode_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public ByteString getSubscriberCurrencyCodeBytes() {
        return ByteString.copyFromUtf8(this.subscriberCurrencyCode_);
    }

    public void setSubscriberCurrencyCode(String str) {
        str.getClass();
        this.subscriberCurrencyCode_ = str;
    }

    public void clearSubscriberCurrencyCode() {
        this.subscriberCurrencyCode_ = getDefaultInstance().getSubscriberCurrencyCode();
    }

    public void setSubscriberCurrencyCodeBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.subscriberCurrencyCode_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getDisplayBadgesOnProfile() {
        return this.displayBadgesOnProfile_;
    }

    public void setDisplayBadgesOnProfile(boolean z) {
        this.displayBadgesOnProfile_ = z;
    }

    public void clearDisplayBadgesOnProfile() {
        this.displayBadgesOnProfile_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
    public boolean getSubscriptionManuallyCancelled() {
        return this.subscriptionManuallyCancelled_;
    }

    public void setSubscriptionManuallyCancelled(boolean z) {
        this.subscriptionManuallyCancelled_ = z;
    }

    public void clearSubscriptionManuallyCancelled() {
        this.subscriptionManuallyCancelled_ = false;
    }

    public static AccountRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static AccountRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static AccountRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static AccountRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static AccountRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static AccountRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static AccountRecord parseFrom(InputStream inputStream) throws IOException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AccountRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AccountRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (AccountRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AccountRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AccountRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AccountRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static AccountRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AccountRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(AccountRecord accountRecord) {
        return DEFAULT_INSTANCE.createBuilder(accountRecord);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<AccountRecord, Builder> implements AccountRecordOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(AccountRecord.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getProfileKey() {
            return ((AccountRecord) this.instance).getProfileKey();
        }

        public Builder setProfileKey(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setProfileKey(byteString);
            return this;
        }

        public Builder clearProfileKey() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearProfileKey();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public String getGivenName() {
            return ((AccountRecord) this.instance).getGivenName();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getGivenNameBytes() {
            return ((AccountRecord) this.instance).getGivenNameBytes();
        }

        public Builder setGivenName(String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).setGivenName(str);
            return this;
        }

        public Builder clearGivenName() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearGivenName();
            return this;
        }

        public Builder setGivenNameBytes(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setGivenNameBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public String getFamilyName() {
            return ((AccountRecord) this.instance).getFamilyName();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getFamilyNameBytes() {
            return ((AccountRecord) this.instance).getFamilyNameBytes();
        }

        public Builder setFamilyName(String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).setFamilyName(str);
            return this;
        }

        public Builder clearFamilyName() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearFamilyName();
            return this;
        }

        public Builder setFamilyNameBytes(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setFamilyNameBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public String getAvatarUrlPath() {
            return ((AccountRecord) this.instance).getAvatarUrlPath();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getAvatarUrlPathBytes() {
            return ((AccountRecord) this.instance).getAvatarUrlPathBytes();
        }

        public Builder setAvatarUrlPath(String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).setAvatarUrlPath(str);
            return this;
        }

        public Builder clearAvatarUrlPath() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearAvatarUrlPath();
            return this;
        }

        public Builder setAvatarUrlPathBytes(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setAvatarUrlPathBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getNoteToSelfArchived() {
            return ((AccountRecord) this.instance).getNoteToSelfArchived();
        }

        public Builder setNoteToSelfArchived(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setNoteToSelfArchived(z);
            return this;
        }

        public Builder clearNoteToSelfArchived() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearNoteToSelfArchived();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getReadReceipts() {
            return ((AccountRecord) this.instance).getReadReceipts();
        }

        public Builder setReadReceipts(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setReadReceipts(z);
            return this;
        }

        public Builder clearReadReceipts() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearReadReceipts();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getSealedSenderIndicators() {
            return ((AccountRecord) this.instance).getSealedSenderIndicators();
        }

        public Builder setSealedSenderIndicators(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setSealedSenderIndicators(z);
            return this;
        }

        public Builder clearSealedSenderIndicators() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearSealedSenderIndicators();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getTypingIndicators() {
            return ((AccountRecord) this.instance).getTypingIndicators();
        }

        public Builder setTypingIndicators(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setTypingIndicators(z);
            return this;
        }

        public Builder clearTypingIndicators() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearTypingIndicators();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getProxiedLinkPreviews() {
            return ((AccountRecord) this.instance).getProxiedLinkPreviews();
        }

        public Builder setProxiedLinkPreviews(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setProxiedLinkPreviews(z);
            return this;
        }

        public Builder clearProxiedLinkPreviews() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearProxiedLinkPreviews();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getNoteToSelfMarkedUnread() {
            return ((AccountRecord) this.instance).getNoteToSelfMarkedUnread();
        }

        public Builder setNoteToSelfMarkedUnread(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setNoteToSelfMarkedUnread(z);
            return this;
        }

        public Builder clearNoteToSelfMarkedUnread() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearNoteToSelfMarkedUnread();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getLinkPreviews() {
            return ((AccountRecord) this.instance).getLinkPreviews();
        }

        public Builder setLinkPreviews(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setLinkPreviews(z);
            return this;
        }

        public Builder clearLinkPreviews() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearLinkPreviews();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public int getPhoneNumberSharingModeValue() {
            return ((AccountRecord) this.instance).getPhoneNumberSharingModeValue();
        }

        public Builder setPhoneNumberSharingModeValue(int i) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPhoneNumberSharingModeValue(i);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public PhoneNumberSharingMode getPhoneNumberSharingMode() {
            return ((AccountRecord) this.instance).getPhoneNumberSharingMode();
        }

        public Builder setPhoneNumberSharingMode(PhoneNumberSharingMode phoneNumberSharingMode) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPhoneNumberSharingMode(phoneNumberSharingMode);
            return this;
        }

        public Builder clearPhoneNumberSharingMode() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearPhoneNumberSharingMode();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getUnlistedPhoneNumber() {
            return ((AccountRecord) this.instance).getUnlistedPhoneNumber();
        }

        public Builder setUnlistedPhoneNumber(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setUnlistedPhoneNumber(z);
            return this;
        }

        public Builder clearUnlistedPhoneNumber() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearUnlistedPhoneNumber();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public List<PinnedConversation> getPinnedConversationsList() {
            return Collections.unmodifiableList(((AccountRecord) this.instance).getPinnedConversationsList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public int getPinnedConversationsCount() {
            return ((AccountRecord) this.instance).getPinnedConversationsCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public PinnedConversation getPinnedConversations(int i) {
            return ((AccountRecord) this.instance).getPinnedConversations(i);
        }

        public Builder setPinnedConversations(int i, PinnedConversation pinnedConversation) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPinnedConversations(i, pinnedConversation);
            return this;
        }

        public Builder setPinnedConversations(int i, PinnedConversation.Builder builder) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPinnedConversations(i, builder.build());
            return this;
        }

        public Builder addPinnedConversations(PinnedConversation pinnedConversation) {
            copyOnWrite();
            ((AccountRecord) this.instance).addPinnedConversations(pinnedConversation);
            return this;
        }

        public Builder addPinnedConversations(int i, PinnedConversation pinnedConversation) {
            copyOnWrite();
            ((AccountRecord) this.instance).addPinnedConversations(i, pinnedConversation);
            return this;
        }

        public Builder addPinnedConversations(PinnedConversation.Builder builder) {
            copyOnWrite();
            ((AccountRecord) this.instance).addPinnedConversations(builder.build());
            return this;
        }

        public Builder addPinnedConversations(int i, PinnedConversation.Builder builder) {
            copyOnWrite();
            ((AccountRecord) this.instance).addPinnedConversations(i, builder.build());
            return this;
        }

        public Builder addAllPinnedConversations(Iterable<? extends PinnedConversation> iterable) {
            copyOnWrite();
            ((AccountRecord) this.instance).addAllPinnedConversations(iterable);
            return this;
        }

        public Builder clearPinnedConversations() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearPinnedConversations();
            return this;
        }

        public Builder removePinnedConversations(int i) {
            copyOnWrite();
            ((AccountRecord) this.instance).removePinnedConversations(i);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getPreferContactAvatars() {
            return ((AccountRecord) this.instance).getPreferContactAvatars();
        }

        public Builder setPreferContactAvatars(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPreferContactAvatars(z);
            return this;
        }

        public Builder clearPreferContactAvatars() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearPreferContactAvatars();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean hasPayments() {
            return ((AccountRecord) this.instance).hasPayments();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public Payments getPayments() {
            return ((AccountRecord) this.instance).getPayments();
        }

        public Builder setPayments(Payments payments) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPayments(payments);
            return this;
        }

        public Builder setPayments(Payments.Builder builder) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPayments(builder.build());
            return this;
        }

        public Builder mergePayments(Payments payments) {
            copyOnWrite();
            ((AccountRecord) this.instance).mergePayments(payments);
            return this;
        }

        public Builder clearPayments() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearPayments();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public int getUniversalExpireTimer() {
            return ((AccountRecord) this.instance).getUniversalExpireTimer();
        }

        public Builder setUniversalExpireTimer(int i) {
            copyOnWrite();
            ((AccountRecord) this.instance).setUniversalExpireTimer(i);
            return this;
        }

        public Builder clearUniversalExpireTimer() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearUniversalExpireTimer();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getPrimarySendsSms() {
            return ((AccountRecord) this.instance).getPrimarySendsSms();
        }

        public Builder setPrimarySendsSms(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPrimarySendsSms(z);
            return this;
        }

        public Builder clearPrimarySendsSms() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearPrimarySendsSms();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public String getE164() {
            return ((AccountRecord) this.instance).getE164();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getE164Bytes() {
            return ((AccountRecord) this.instance).getE164Bytes();
        }

        public Builder setE164(String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).setE164(str);
            return this;
        }

        public Builder clearE164() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearE164();
            return this;
        }

        public Builder setE164Bytes(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setE164Bytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public List<String> getPreferredReactionEmojiList() {
            return Collections.unmodifiableList(((AccountRecord) this.instance).getPreferredReactionEmojiList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public int getPreferredReactionEmojiCount() {
            return ((AccountRecord) this.instance).getPreferredReactionEmojiCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public String getPreferredReactionEmoji(int i) {
            return ((AccountRecord) this.instance).getPreferredReactionEmoji(i);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getPreferredReactionEmojiBytes(int i) {
            return ((AccountRecord) this.instance).getPreferredReactionEmojiBytes(i);
        }

        public Builder setPreferredReactionEmoji(int i, String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).setPreferredReactionEmoji(i, str);
            return this;
        }

        public Builder addPreferredReactionEmoji(String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).addPreferredReactionEmoji(str);
            return this;
        }

        public Builder addAllPreferredReactionEmoji(Iterable<String> iterable) {
            copyOnWrite();
            ((AccountRecord) this.instance).addAllPreferredReactionEmoji(iterable);
            return this;
        }

        public Builder clearPreferredReactionEmoji() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearPreferredReactionEmoji();
            return this;
        }

        public Builder addPreferredReactionEmojiBytes(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).addPreferredReactionEmojiBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getSubscriberId() {
            return ((AccountRecord) this.instance).getSubscriberId();
        }

        public Builder setSubscriberId(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setSubscriberId(byteString);
            return this;
        }

        public Builder clearSubscriberId() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearSubscriberId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public String getSubscriberCurrencyCode() {
            return ((AccountRecord) this.instance).getSubscriberCurrencyCode();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public ByteString getSubscriberCurrencyCodeBytes() {
            return ((AccountRecord) this.instance).getSubscriberCurrencyCodeBytes();
        }

        public Builder setSubscriberCurrencyCode(String str) {
            copyOnWrite();
            ((AccountRecord) this.instance).setSubscriberCurrencyCode(str);
            return this;
        }

        public Builder clearSubscriberCurrencyCode() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearSubscriberCurrencyCode();
            return this;
        }

        public Builder setSubscriberCurrencyCodeBytes(ByteString byteString) {
            copyOnWrite();
            ((AccountRecord) this.instance).setSubscriberCurrencyCodeBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getDisplayBadgesOnProfile() {
            return ((AccountRecord) this.instance).getDisplayBadgesOnProfile();
        }

        public Builder setDisplayBadgesOnProfile(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setDisplayBadgesOnProfile(z);
            return this;
        }

        public Builder clearDisplayBadgesOnProfile() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearDisplayBadgesOnProfile();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.AccountRecordOrBuilder
        public boolean getSubscriptionManuallyCancelled() {
            return ((AccountRecord) this.instance).getSubscriptionManuallyCancelled();
        }

        public Builder setSubscriptionManuallyCancelled(boolean z) {
            copyOnWrite();
            ((AccountRecord) this.instance).setSubscriptionManuallyCancelled(z);
            return this;
        }

        public Builder clearSubscriptionManuallyCancelled() {
            copyOnWrite();
            ((AccountRecord) this.instance).clearSubscriptionManuallyCancelled();
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new AccountRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0018\u0000\u0000\u0001\u0018\u0018\u0000\u0002\u0000\u0001\n\u0002Ȉ\u0003Ȉ\u0004Ȉ\u0005\u0007\u0006\u0007\u0007\u0007\b\u0007\t\u0007\n\u0007\u000b\u0007\f\f\r\u0007\u000e\u001b\u000f\u0007\u0010\t\u0011\u000b\u0012\u0007\u0013Ȉ\u0014Ț\u0015\n\u0016Ȉ\u0017\u0007\u0018\u0007", new Object[]{"profileKey_", "givenName_", "familyName_", "avatarUrlPath_", "noteToSelfArchived_", "readReceipts_", "sealedSenderIndicators_", "typingIndicators_", "proxiedLinkPreviews_", "noteToSelfMarkedUnread_", "linkPreviews_", "phoneNumberSharingMode_", "unlistedPhoneNumber_", "pinnedConversations_", PinnedConversation.class, "preferContactAvatars_", "payments_", "universalExpireTimer_", "primarySendsSms_", "e164_", "preferredReactionEmoji_", "subscriberId_", "subscriberCurrencyCode_", "displayBadgesOnProfile_", "subscriptionManuallyCancelled_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<AccountRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (AccountRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        AccountRecord accountRecord = new AccountRecord();
        DEFAULT_INSTANCE = accountRecord;
        GeneratedMessageLite.registerDefaultInstance(AccountRecord.class, accountRecord);
    }

    public static AccountRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<AccountRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
