package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.whispersystems.signalservice.internal.keybackup.protos.BackupRequest;
import org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequest;
import org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequest;

/* loaded from: classes5.dex */
public final class Request extends GeneratedMessageLite<Request, Builder> implements RequestOrBuilder {
    public static final int BACKUP_FIELD_NUMBER;
    private static final Request DEFAULT_INSTANCE;
    public static final int DELETE_FIELD_NUMBER;
    private static volatile Parser<Request> PARSER;
    public static final int RESTORE_FIELD_NUMBER;
    private BackupRequest backup_;
    private int bitField0_;
    private DeleteRequest delete_;
    private RestoreRequest restore_;

    private Request() {
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
    public boolean hasBackup() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
    public BackupRequest getBackup() {
        BackupRequest backupRequest = this.backup_;
        return backupRequest == null ? BackupRequest.getDefaultInstance() : backupRequest;
    }

    public void setBackup(BackupRequest backupRequest) {
        backupRequest.getClass();
        this.backup_ = backupRequest;
        this.bitField0_ |= 1;
    }

    public void mergeBackup(BackupRequest backupRequest) {
        backupRequest.getClass();
        BackupRequest backupRequest2 = this.backup_;
        if (backupRequest2 == null || backupRequest2 == BackupRequest.getDefaultInstance()) {
            this.backup_ = backupRequest;
        } else {
            this.backup_ = BackupRequest.newBuilder(this.backup_).mergeFrom((BackupRequest.Builder) backupRequest).buildPartial();
        }
        this.bitField0_ |= 1;
    }

    public void clearBackup() {
        this.backup_ = null;
        this.bitField0_ &= -2;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
    public boolean hasRestore() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
    public RestoreRequest getRestore() {
        RestoreRequest restoreRequest = this.restore_;
        return restoreRequest == null ? RestoreRequest.getDefaultInstance() : restoreRequest;
    }

    public void setRestore(RestoreRequest restoreRequest) {
        restoreRequest.getClass();
        this.restore_ = restoreRequest;
        this.bitField0_ |= 2;
    }

    public void mergeRestore(RestoreRequest restoreRequest) {
        restoreRequest.getClass();
        RestoreRequest restoreRequest2 = this.restore_;
        if (restoreRequest2 == null || restoreRequest2 == RestoreRequest.getDefaultInstance()) {
            this.restore_ = restoreRequest;
        } else {
            this.restore_ = RestoreRequest.newBuilder(this.restore_).mergeFrom((RestoreRequest.Builder) restoreRequest).buildPartial();
        }
        this.bitField0_ |= 2;
    }

    public void clearRestore() {
        this.restore_ = null;
        this.bitField0_ &= -3;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
    public boolean hasDelete() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
    public DeleteRequest getDelete() {
        DeleteRequest deleteRequest = this.delete_;
        return deleteRequest == null ? DeleteRequest.getDefaultInstance() : deleteRequest;
    }

    public void setDelete(DeleteRequest deleteRequest) {
        deleteRequest.getClass();
        this.delete_ = deleteRequest;
        this.bitField0_ |= 4;
    }

    public void mergeDelete(DeleteRequest deleteRequest) {
        deleteRequest.getClass();
        DeleteRequest deleteRequest2 = this.delete_;
        if (deleteRequest2 == null || deleteRequest2 == DeleteRequest.getDefaultInstance()) {
            this.delete_ = deleteRequest;
        } else {
            this.delete_ = DeleteRequest.newBuilder(this.delete_).mergeFrom((DeleteRequest.Builder) deleteRequest).buildPartial();
        }
        this.bitField0_ |= 4;
    }

    public void clearDelete() {
        this.delete_ = null;
        this.bitField0_ &= -5;
    }

    public static Request parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Request parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Request parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Request parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Request parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Request parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Request parseFrom(InputStream inputStream) throws IOException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Request parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Request parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Request) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Request parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Request) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Request parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Request parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Request) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Request request) {
        return DEFAULT_INSTANCE.createBuilder(request);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Request, Builder> implements RequestOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(Request.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
        public boolean hasBackup() {
            return ((Request) this.instance).hasBackup();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
        public BackupRequest getBackup() {
            return ((Request) this.instance).getBackup();
        }

        public Builder setBackup(BackupRequest backupRequest) {
            copyOnWrite();
            ((Request) this.instance).setBackup(backupRequest);
            return this;
        }

        public Builder setBackup(BackupRequest.Builder builder) {
            copyOnWrite();
            ((Request) this.instance).setBackup(builder.build());
            return this;
        }

        public Builder mergeBackup(BackupRequest backupRequest) {
            copyOnWrite();
            ((Request) this.instance).mergeBackup(backupRequest);
            return this;
        }

        public Builder clearBackup() {
            copyOnWrite();
            ((Request) this.instance).clearBackup();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
        public boolean hasRestore() {
            return ((Request) this.instance).hasRestore();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
        public RestoreRequest getRestore() {
            return ((Request) this.instance).getRestore();
        }

        public Builder setRestore(RestoreRequest restoreRequest) {
            copyOnWrite();
            ((Request) this.instance).setRestore(restoreRequest);
            return this;
        }

        public Builder setRestore(RestoreRequest.Builder builder) {
            copyOnWrite();
            ((Request) this.instance).setRestore(builder.build());
            return this;
        }

        public Builder mergeRestore(RestoreRequest restoreRequest) {
            copyOnWrite();
            ((Request) this.instance).mergeRestore(restoreRequest);
            return this;
        }

        public Builder clearRestore() {
            copyOnWrite();
            ((Request) this.instance).clearRestore();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
        public boolean hasDelete() {
            return ((Request) this.instance).hasDelete();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RequestOrBuilder
        public DeleteRequest getDelete() {
            return ((Request) this.instance).getDelete();
        }

        public Builder setDelete(DeleteRequest deleteRequest) {
            copyOnWrite();
            ((Request) this.instance).setDelete(deleteRequest);
            return this;
        }

        public Builder setDelete(DeleteRequest.Builder builder) {
            copyOnWrite();
            ((Request) this.instance).setDelete(builder.build());
            return this;
        }

        public Builder mergeDelete(DeleteRequest deleteRequest) {
            copyOnWrite();
            ((Request) this.instance).mergeDelete(deleteRequest);
            return this;
        }

        public Builder clearDelete() {
            copyOnWrite();
            ((Request) this.instance).clearDelete();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.Request$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Request();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002", new Object[]{"bitField0_", "backup_", "restore_", "delete_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Request> parser = PARSER;
                if (parser == null) {
                    synchronized (Request.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Request request = new Request();
        DEFAULT_INSTANCE = request;
        GeneratedMessageLite.registerDefaultInstance(Request.class, request);
    }

    public static Request getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Request> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
