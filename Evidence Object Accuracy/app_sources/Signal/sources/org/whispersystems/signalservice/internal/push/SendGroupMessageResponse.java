package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;
import org.signal.libsignal.protocol.logging.Log;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes.dex */
public class SendGroupMessageResponse {
    private static final String TAG;
    @JsonProperty
    private String[] uuids404;

    public Set<ServiceId> getUnsentTargets() {
        HashSet hashSet = new HashSet(this.uuids404.length);
        for (String str : this.uuids404) {
            ServiceId parseOrNull = ServiceId.parseOrNull(str);
            if (parseOrNull != null) {
                hashSet.add(parseOrNull);
            } else {
                Log.w(TAG, "Failed to parse ServiceId!");
            }
        }
        return hashSet;
    }
}
