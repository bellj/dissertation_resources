package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.whispersystems.signalservice.internal.storage.protos.StorageItem;

/* loaded from: classes5.dex */
public final class StorageItems extends GeneratedMessageLite<StorageItems, Builder> implements StorageItemsOrBuilder {
    private static final StorageItems DEFAULT_INSTANCE;
    public static final int ITEMS_FIELD_NUMBER;
    private static volatile Parser<StorageItems> PARSER;
    private Internal.ProtobufList<StorageItem> items_ = GeneratedMessageLite.emptyProtobufList();

    private StorageItems() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemsOrBuilder
    public List<StorageItem> getItemsList() {
        return this.items_;
    }

    public List<? extends StorageItemOrBuilder> getItemsOrBuilderList() {
        return this.items_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemsOrBuilder
    public int getItemsCount() {
        return this.items_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemsOrBuilder
    public StorageItem getItems(int i) {
        return this.items_.get(i);
    }

    public StorageItemOrBuilder getItemsOrBuilder(int i) {
        return this.items_.get(i);
    }

    private void ensureItemsIsMutable() {
        Internal.ProtobufList<StorageItem> protobufList = this.items_;
        if (!protobufList.isModifiable()) {
            this.items_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setItems(int i, StorageItem storageItem) {
        storageItem.getClass();
        ensureItemsIsMutable();
        this.items_.set(i, storageItem);
    }

    public void addItems(StorageItem storageItem) {
        storageItem.getClass();
        ensureItemsIsMutable();
        this.items_.add(storageItem);
    }

    public void addItems(int i, StorageItem storageItem) {
        storageItem.getClass();
        ensureItemsIsMutable();
        this.items_.add(i, storageItem);
    }

    public void addAllItems(Iterable<? extends StorageItem> iterable) {
        ensureItemsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.items_);
    }

    public void clearItems() {
        this.items_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeItems(int i) {
        ensureItemsIsMutable();
        this.items_.remove(i);
    }

    public static StorageItems parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static StorageItems parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static StorageItems parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static StorageItems parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static StorageItems parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static StorageItems parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static StorageItems parseFrom(InputStream inputStream) throws IOException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageItems parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageItems parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (StorageItems) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageItems parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageItems) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageItems parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static StorageItems parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageItems) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(StorageItems storageItems) {
        return DEFAULT_INSTANCE.createBuilder(storageItems);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<StorageItems, Builder> implements StorageItemsOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(StorageItems.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemsOrBuilder
        public List<StorageItem> getItemsList() {
            return Collections.unmodifiableList(((StorageItems) this.instance).getItemsList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemsOrBuilder
        public int getItemsCount() {
            return ((StorageItems) this.instance).getItemsCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemsOrBuilder
        public StorageItem getItems(int i) {
            return ((StorageItems) this.instance).getItems(i);
        }

        public Builder setItems(int i, StorageItem storageItem) {
            copyOnWrite();
            ((StorageItems) this.instance).setItems(i, storageItem);
            return this;
        }

        public Builder setItems(int i, StorageItem.Builder builder) {
            copyOnWrite();
            ((StorageItems) this.instance).setItems(i, builder.build());
            return this;
        }

        public Builder addItems(StorageItem storageItem) {
            copyOnWrite();
            ((StorageItems) this.instance).addItems(storageItem);
            return this;
        }

        public Builder addItems(int i, StorageItem storageItem) {
            copyOnWrite();
            ((StorageItems) this.instance).addItems(i, storageItem);
            return this;
        }

        public Builder addItems(StorageItem.Builder builder) {
            copyOnWrite();
            ((StorageItems) this.instance).addItems(builder.build());
            return this;
        }

        public Builder addItems(int i, StorageItem.Builder builder) {
            copyOnWrite();
            ((StorageItems) this.instance).addItems(i, builder.build());
            return this;
        }

        public Builder addAllItems(Iterable<? extends StorageItem> iterable) {
            copyOnWrite();
            ((StorageItems) this.instance).addAllItems(iterable);
            return this;
        }

        public Builder clearItems() {
            copyOnWrite();
            ((StorageItems) this.instance).clearItems();
            return this;
        }

        public Builder removeItems(int i) {
            copyOnWrite();
            ((StorageItems) this.instance).removeItems(i);
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.StorageItems$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new StorageItems();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"items_", StorageItem.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<StorageItems> parser = PARSER;
                if (parser == null) {
                    synchronized (StorageItems.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        StorageItems storageItems = new StorageItems();
        DEFAULT_INSTANCE = storageItems;
        GeneratedMessageLite.registerDefaultInstance(StorageItems.class, storageItems);
    }

    public static StorageItems getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<StorageItems> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
