package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceInfo;

/* loaded from: classes.dex */
public class DeviceInfoList {
    @JsonProperty
    private List<DeviceInfo> devices;

    public List<DeviceInfo> getDevices() {
        return this.devices;
    }
}
