package org.whispersystems.signalservice.internal.contacts.crypto;

import java.io.ByteArrayInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import org.whispersystems.util.Base64;

/* loaded from: classes5.dex */
public class SigningCertificate {
    private final CertPath path;

    public SigningCertificate(String str, KeyStore keyStore) throws CertificateException, CertPathValidatorException {
        try {
            CertificateFactory instance = CertificateFactory.getInstance("X.509");
            LinkedList linkedList = new LinkedList(instance.generateCertificates(new ByteArrayInputStream(str.getBytes())));
            PKIXParameters pKIXParameters = new PKIXParameters(keyStore);
            CertPathValidator instance2 = CertPathValidator.getInstance("PKIX");
            if (!linkedList.isEmpty()) {
                CertPath generateCertPath = instance.generateCertPath(linkedList);
                this.path = generateCertPath;
                pKIXParameters.setRevocationEnabled(false);
                instance2.validate(generateCertPath, pKIXParameters);
                verifyDistinguishedName(generateCertPath);
                return;
            }
            throw new CertificateException("No certificates available! Badly-formatted cert chain?");
        } catch (InvalidAlgorithmParameterException | KeyStoreException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public void verifySignature(String str, String str2) throws SignatureException {
        try {
            Signature instance = Signature.getInstance("SHA256withRSA");
            instance.initVerify((Certificate) this.path.getCertificates().get(0));
            instance.update(str.getBytes());
            if (!instance.verify(Base64.decode(str2.getBytes()))) {
                throw new SignatureException("Signature verification failed.");
            }
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    private void verifyDistinguishedName(CertPath certPath) throws CertificateException {
        String name = ((X509Certificate) certPath.getCertificates().get(0)).getSubjectX500Principal().getName();
        if (!"CN=Intel SGX Attestation Report Signing,O=Intel Corporation,L=Santa Clara,ST=CA,C=US".equals(name)) {
            throw new CertificateException("Bad DN: " + name);
        }
    }
}
