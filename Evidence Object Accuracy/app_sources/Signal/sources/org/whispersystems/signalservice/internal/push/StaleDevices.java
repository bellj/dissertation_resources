package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public class StaleDevices {
    @JsonProperty
    private List<Integer> staleDevices;

    public List<Integer> getStaleDevices() {
        return this.staleDevices;
    }
}
