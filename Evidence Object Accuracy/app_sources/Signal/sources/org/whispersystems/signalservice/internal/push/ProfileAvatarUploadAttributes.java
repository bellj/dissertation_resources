package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class ProfileAvatarUploadAttributes {
    @JsonProperty
    private String acl;
    @JsonProperty
    private String algorithm;
    @JsonProperty
    private String credential;
    @JsonProperty
    private String date;
    @JsonProperty
    private String key;
    @JsonProperty
    private String policy;
    @JsonProperty
    private String signature;

    public String getKey() {
        return this.key;
    }

    public String getCredential() {
        return this.credential;
    }

    public String getAcl() {
        return this.acl;
    }

    public String getAlgorithm() {
        return this.algorithm;
    }

    public String getDate() {
        return this.date;
    }

    public String getPolicy() {
        return this.policy;
    }

    public String getSignature() {
        return this.signature;
    }
}
