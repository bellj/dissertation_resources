package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.whispersystems.signalservice.internal.storage.protos.ContactRecord;

/* loaded from: classes5.dex */
public interface ContactRecordOrBuilder extends MessageLiteOrBuilder {
    boolean getArchived();

    boolean getBlocked();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    String getFamilyName();

    ByteString getFamilyNameBytes();

    String getGivenName();

    ByteString getGivenNameBytes();

    boolean getHideStory();

    ByteString getIdentityKey();

    ContactRecord.IdentityState getIdentityState();

    int getIdentityStateValue();

    boolean getMarkedUnread();

    long getMutedUntilTimestamp();

    ByteString getProfileKey();

    String getServiceE164();

    ByteString getServiceE164Bytes();

    String getServiceUuid();

    ByteString getServiceUuidBytes();

    String getUsername();

    ByteString getUsernameBytes();

    boolean getWhitelisted();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
