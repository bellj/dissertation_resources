package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes5.dex */
public final class StoryDistributionListRecord extends GeneratedMessageLite<StoryDistributionListRecord, Builder> implements StoryDistributionListRecordOrBuilder {
    public static final int ALLOWSREPLIES_FIELD_NUMBER;
    private static final StoryDistributionListRecord DEFAULT_INSTANCE;
    public static final int DELETEDATTIMESTAMP_FIELD_NUMBER;
    public static final int IDENTIFIER_FIELD_NUMBER;
    public static final int ISBLOCKLIST_FIELD_NUMBER;
    public static final int NAME_FIELD_NUMBER;
    private static volatile Parser<StoryDistributionListRecord> PARSER;
    public static final int RECIPIENTUUIDS_FIELD_NUMBER;
    private boolean allowsReplies_;
    private long deletedAtTimestamp_;
    private ByteString identifier_ = ByteString.EMPTY;
    private boolean isBlockList_;
    private String name_ = "";
    private Internal.ProtobufList<String> recipientUuids_ = GeneratedMessageLite.emptyProtobufList();

    private StoryDistributionListRecord() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public ByteString getIdentifier() {
        return this.identifier_;
    }

    public void setIdentifier(ByteString byteString) {
        byteString.getClass();
        this.identifier_ = byteString;
    }

    public void clearIdentifier() {
        this.identifier_ = getDefaultInstance().getIdentifier();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public String getName() {
        return this.name_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public ByteString getNameBytes() {
        return ByteString.copyFromUtf8(this.name_);
    }

    public void setName(String str) {
        str.getClass();
        this.name_ = str;
    }

    public void clearName() {
        this.name_ = getDefaultInstance().getName();
    }

    public void setNameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.name_ = byteString.toStringUtf8();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public List<String> getRecipientUuidsList() {
        return this.recipientUuids_;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public int getRecipientUuidsCount() {
        return this.recipientUuids_.size();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public String getRecipientUuids(int i) {
        return this.recipientUuids_.get(i);
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public ByteString getRecipientUuidsBytes(int i) {
        return ByteString.copyFromUtf8(this.recipientUuids_.get(i));
    }

    private void ensureRecipientUuidsIsMutable() {
        Internal.ProtobufList<String> protobufList = this.recipientUuids_;
        if (!protobufList.isModifiable()) {
            this.recipientUuids_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setRecipientUuids(int i, String str) {
        str.getClass();
        ensureRecipientUuidsIsMutable();
        this.recipientUuids_.set(i, str);
    }

    public void addRecipientUuids(String str) {
        str.getClass();
        ensureRecipientUuidsIsMutable();
        this.recipientUuids_.add(str);
    }

    public void addAllRecipientUuids(Iterable<String> iterable) {
        ensureRecipientUuidsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.recipientUuids_);
    }

    public void clearRecipientUuids() {
        this.recipientUuids_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addRecipientUuidsBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        ensureRecipientUuidsIsMutable();
        this.recipientUuids_.add(byteString.toStringUtf8());
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public long getDeletedAtTimestamp() {
        return this.deletedAtTimestamp_;
    }

    public void setDeletedAtTimestamp(long j) {
        this.deletedAtTimestamp_ = j;
    }

    public void clearDeletedAtTimestamp() {
        this.deletedAtTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public boolean getAllowsReplies() {
        return this.allowsReplies_;
    }

    public void setAllowsReplies(boolean z) {
        this.allowsReplies_ = z;
    }

    public void clearAllowsReplies() {
        this.allowsReplies_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
    public boolean getIsBlockList() {
        return this.isBlockList_;
    }

    public void setIsBlockList(boolean z) {
        this.isBlockList_ = z;
    }

    public void clearIsBlockList() {
        this.isBlockList_ = false;
    }

    public static StoryDistributionListRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static StoryDistributionListRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static StoryDistributionListRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static StoryDistributionListRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static StoryDistributionListRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static StoryDistributionListRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static StoryDistributionListRecord parseFrom(InputStream inputStream) throws IOException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StoryDistributionListRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StoryDistributionListRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StoryDistributionListRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StoryDistributionListRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static StoryDistributionListRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StoryDistributionListRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(StoryDistributionListRecord storyDistributionListRecord) {
        return DEFAULT_INSTANCE.createBuilder(storyDistributionListRecord);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<StoryDistributionListRecord, Builder> implements StoryDistributionListRecordOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(StoryDistributionListRecord.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public ByteString getIdentifier() {
            return ((StoryDistributionListRecord) this.instance).getIdentifier();
        }

        public Builder setIdentifier(ByteString byteString) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setIdentifier(byteString);
            return this;
        }

        public Builder clearIdentifier() {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).clearIdentifier();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public String getName() {
            return ((StoryDistributionListRecord) this.instance).getName();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public ByteString getNameBytes() {
            return ((StoryDistributionListRecord) this.instance).getNameBytes();
        }

        public Builder setName(String str) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setName(str);
            return this;
        }

        public Builder clearName() {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).clearName();
            return this;
        }

        public Builder setNameBytes(ByteString byteString) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setNameBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public List<String> getRecipientUuidsList() {
            return Collections.unmodifiableList(((StoryDistributionListRecord) this.instance).getRecipientUuidsList());
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public int getRecipientUuidsCount() {
            return ((StoryDistributionListRecord) this.instance).getRecipientUuidsCount();
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public String getRecipientUuids(int i) {
            return ((StoryDistributionListRecord) this.instance).getRecipientUuids(i);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public ByteString getRecipientUuidsBytes(int i) {
            return ((StoryDistributionListRecord) this.instance).getRecipientUuidsBytes(i);
        }

        public Builder setRecipientUuids(int i, String str) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setRecipientUuids(i, str);
            return this;
        }

        public Builder addRecipientUuids(String str) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).addRecipientUuids(str);
            return this;
        }

        public Builder addAllRecipientUuids(Iterable<String> iterable) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).addAllRecipientUuids(iterable);
            return this;
        }

        public Builder clearRecipientUuids() {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).clearRecipientUuids();
            return this;
        }

        public Builder addRecipientUuidsBytes(ByteString byteString) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).addRecipientUuidsBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public long getDeletedAtTimestamp() {
            return ((StoryDistributionListRecord) this.instance).getDeletedAtTimestamp();
        }

        public Builder setDeletedAtTimestamp(long j) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setDeletedAtTimestamp(j);
            return this;
        }

        public Builder clearDeletedAtTimestamp() {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).clearDeletedAtTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public boolean getAllowsReplies() {
            return ((StoryDistributionListRecord) this.instance).getAllowsReplies();
        }

        public Builder setAllowsReplies(boolean z) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setAllowsReplies(z);
            return this;
        }

        public Builder clearAllowsReplies() {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).clearAllowsReplies();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecordOrBuilder
        public boolean getIsBlockList() {
            return ((StoryDistributionListRecord) this.instance).getIsBlockList();
        }

        public Builder setIsBlockList(boolean z) {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).setIsBlockList(z);
            return this;
        }

        public Builder clearIsBlockList() {
            copyOnWrite();
            ((StoryDistributionListRecord) this.instance).clearIsBlockList();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.StoryDistributionListRecord$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new StoryDistributionListRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0006\u0000\u0000\u0001\u0006\u0006\u0000\u0001\u0000\u0001\n\u0002Ȉ\u0003Ț\u0004\u0003\u0005\u0007\u0006\u0007", new Object[]{"identifier_", "name_", "recipientUuids_", "deletedAtTimestamp_", "allowsReplies_", "isBlockList_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<StoryDistributionListRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (StoryDistributionListRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        StoryDistributionListRecord storyDistributionListRecord = new StoryDistributionListRecord();
        DEFAULT_INSTANCE = storyDistributionListRecord;
        GeneratedMessageLite.registerDefaultInstance(StoryDistributionListRecord.class, storyDistributionListRecord);
    }

    public static StoryDistributionListRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<StoryDistributionListRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
