package org.whispersystems.signalservice.internal.push.exceptions;

import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.push.MismatchedDevices;

/* loaded from: classes5.dex */
public class MismatchedDevicesException extends NonSuccessfulResponseCodeException {
    private final MismatchedDevices mismatchedDevices;

    public MismatchedDevicesException(MismatchedDevices mismatchedDevices) {
        super(409);
        this.mismatchedDevices = mismatchedDevices;
    }

    public MismatchedDevices getMismatchedDevices() {
        return this.mismatchedDevices;
    }
}
