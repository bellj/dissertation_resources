package org.whispersystems.signalservice.internal.push.exceptions;

import j$.util.Optional;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes5.dex */
public final class ForbiddenException extends NonSuccessfulResponseCodeException {
    private Optional<String> reason;

    public ForbiddenException() {
        this(Optional.empty());
    }

    public ForbiddenException(Optional<String> optional) {
        super(403);
        this.reason = optional;
    }

    public Optional<String> getReason() {
        return this.reason;
    }
}
