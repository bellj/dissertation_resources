package org.whispersystems.signalservice.internal.contacts.crypto;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.concurrent.TimeUnit;
import org.whispersystems.signalservice.api.crypto.InvalidCiphertextException;
import org.whispersystems.signalservice.internal.contacts.crypto.AESCipher;
import org.whispersystems.signalservice.internal.contacts.entities.KeyBackupRequest;
import org.whispersystems.signalservice.internal.contacts.entities.KeyBackupResponse;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.BackupRequest;
import org.whispersystems.signalservice.internal.keybackup.protos.BackupResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.DeleteRequest;
import org.whispersystems.signalservice.internal.keybackup.protos.DeleteResponse;
import org.whispersystems.signalservice.internal.keybackup.protos.Request;
import org.whispersystems.signalservice.internal.keybackup.protos.Response;
import org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequest;
import org.whispersystems.signalservice.internal.keybackup.protos.RestoreResponse;

/* loaded from: classes5.dex */
public final class KeyBackupCipher {
    private static final long VALID_FROM_BUFFER_MS = TimeUnit.DAYS.toMillis(1);

    private KeyBackupCipher() {
    }

    public static KeyBackupRequest createKeyBackupRequest(byte[] bArr, byte[] bArr2, TokenResponse tokenResponse, RemoteAttestation remoteAttestation, byte[] bArr3, int i) {
        long currentTimeMillis = System.currentTimeMillis();
        return createKeyBackupRequest(Request.newBuilder().setBackup(BackupRequest.newBuilder().setServiceId(ByteString.copyFrom(bArr3)).setBackupId(ByteString.copyFrom(tokenResponse.getBackupId())).setToken(ByteString.copyFrom(tokenResponse.getToken())).setValidFrom(getValidFromSeconds(currentTimeMillis)).setData(ByteString.copyFrom(bArr2)).setPin(ByteString.copyFrom(bArr)).setTries(i).build()).build(), remoteAttestation, "backup");
    }

    public static KeyBackupRequest createKeyRestoreRequest(byte[] bArr, TokenResponse tokenResponse, RemoteAttestation remoteAttestation, byte[] bArr2) {
        long currentTimeMillis = System.currentTimeMillis();
        return createKeyBackupRequest(Request.newBuilder().setRestore(RestoreRequest.newBuilder().setServiceId(ByteString.copyFrom(bArr2)).setBackupId(ByteString.copyFrom(tokenResponse.getBackupId())).setToken(ByteString.copyFrom(tokenResponse.getToken())).setValidFrom(getValidFromSeconds(currentTimeMillis)).setPin(ByteString.copyFrom(bArr)).build()).build(), remoteAttestation, "restore");
    }

    public static KeyBackupRequest createKeyDeleteRequest(TokenResponse tokenResponse, RemoteAttestation remoteAttestation, byte[] bArr) {
        return createKeyBackupRequest(Request.newBuilder().setDelete(DeleteRequest.newBuilder().setServiceId(ByteString.copyFrom(bArr)).setBackupId(ByteString.copyFrom(tokenResponse.getBackupId())).build()).build(), remoteAttestation, "delete");
    }

    public static BackupResponse getKeyBackupResponse(KeyBackupResponse keyBackupResponse, RemoteAttestation remoteAttestation) throws InvalidCiphertextException, InvalidProtocolBufferException {
        return Response.parseFrom(decryptData(keyBackupResponse, remoteAttestation)).getBackup();
    }

    public static RestoreResponse getKeyRestoreResponse(KeyBackupResponse keyBackupResponse, RemoteAttestation remoteAttestation) throws InvalidCiphertextException, InvalidProtocolBufferException {
        return Response.parseFrom(decryptData(keyBackupResponse, remoteAttestation)).getRestore();
    }

    public static DeleteResponse getKeyDeleteResponseStatus(KeyBackupResponse keyBackupResponse, RemoteAttestation remoteAttestation) throws InvalidCiphertextException, InvalidProtocolBufferException {
        return DeleteResponse.parseFrom(decryptData(keyBackupResponse, remoteAttestation));
    }

    private static KeyBackupRequest createKeyBackupRequest(Request request, RemoteAttestation remoteAttestation, String str) {
        AESCipher.AESEncryptedResult encrypt = AESCipher.encrypt(remoteAttestation.getKeys().getClientKey(), remoteAttestation.getRequestId(), request.toByteArray());
        return new KeyBackupRequest(encrypt.aad, encrypt.iv, encrypt.data, encrypt.mac, str);
    }

    private static byte[] decryptData(KeyBackupResponse keyBackupResponse, RemoteAttestation remoteAttestation) throws InvalidCiphertextException {
        return AESCipher.decrypt(remoteAttestation.getKeys().getServerKey(), keyBackupResponse.getIv(), keyBackupResponse.getData(), keyBackupResponse.getMac());
    }

    private static long getValidFromSeconds(long j) {
        return TimeUnit.MILLISECONDS.toSeconds(j - VALID_FROM_BUFFER_MS);
    }
}
