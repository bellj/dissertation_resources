package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class DeviceLimit {
    @JsonProperty
    private int current;
    @JsonProperty
    private int max;

    public int getCurrent() {
        return this.current;
    }

    public int getMax() {
        return this.max;
    }
}
