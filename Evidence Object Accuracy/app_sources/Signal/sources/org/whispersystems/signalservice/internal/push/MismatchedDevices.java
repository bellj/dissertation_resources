package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public class MismatchedDevices {
    @JsonProperty
    private List<Integer> extraDevices;
    @JsonProperty
    private List<Integer> missingDevices;

    public List<Integer> getMissingDevices() {
        return this.missingDevices;
    }

    public List<Integer> getExtraDevices() {
        return this.extraDevices;
    }
}
