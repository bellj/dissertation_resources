package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes5.dex */
public interface BackupRequestOrBuilder extends MessageLiteOrBuilder {
    ByteString getBackupId();

    ByteString getData();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    ByteString getPin();

    ByteString getServiceId();

    ByteString getToken();

    int getTries();

    long getValidFrom();

    boolean hasBackupId();

    boolean hasData();

    boolean hasPin();

    boolean hasServiceId();

    boolean hasToken();

    boolean hasTries();

    boolean hasValidFrom();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
