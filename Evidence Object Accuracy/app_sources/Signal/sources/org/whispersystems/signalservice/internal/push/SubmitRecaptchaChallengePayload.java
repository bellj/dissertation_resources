package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class SubmitRecaptchaChallengePayload {
    @JsonProperty
    private String captcha;
    @JsonProperty
    private String token;
    @JsonProperty
    private String type;

    public SubmitRecaptchaChallengePayload() {
    }

    public SubmitRecaptchaChallengePayload(String str, String str2) {
        this.type = "recaptcha";
        this.token = str;
        this.captcha = str2;
    }
}
