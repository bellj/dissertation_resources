package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* loaded from: classes.dex */
public class DonationIntentResult {
    @JsonProperty("client_secret")
    private String clientSecret;
    @JsonProperty(ContactRepository.ID_COLUMN)
    private String id;

    public DonationIntentResult(@JsonProperty("id") String str, @JsonProperty("client_secret") String str2) {
        this.id = str;
        this.clientSecret = str2;
    }

    public String getId() {
        return this.id;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }
}
