package org.whispersystems.signalservice.internal.push.http;

import java.io.IOException;
import java.io.InputStream;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import org.whispersystems.signalservice.api.crypto.DigestingOutputStream;
import org.whispersystems.signalservice.api.crypto.SkippingOutputStream;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes5.dex */
public class DigestingRequestBody extends RequestBody {
    private final CancelationSignal cancelationSignal;
    private final long contentLength;
    private final long contentStart;
    private final String contentType;
    private byte[] digest;
    private final InputStream inputStream;
    private final OutputStreamFactory outputStreamFactory;
    private final SignalServiceAttachment.ProgressListener progressListener;

    public DigestingRequestBody(InputStream inputStream, OutputStreamFactory outputStreamFactory, String str, long j, SignalServiceAttachment.ProgressListener progressListener, CancelationSignal cancelationSignal, long j2) {
        boolean z = true;
        Preconditions.checkArgument(j >= j2);
        Preconditions.checkArgument(j2 < 0 ? false : z);
        this.inputStream = inputStream;
        this.outputStreamFactory = outputStreamFactory;
        this.contentType = str;
        this.contentLength = j;
        this.progressListener = progressListener;
        this.cancelationSignal = cancelationSignal;
        this.contentStart = j2;
    }

    @Override // okhttp3.RequestBody
    public MediaType contentType() {
        return MediaType.parse(this.contentType);
    }

    @Override // okhttp3.RequestBody
    public void writeTo(BufferedSink bufferedSink) throws IOException {
        DigestingOutputStream createFor = this.outputStreamFactory.createFor(new SkippingOutputStream(this.contentStart, bufferedSink.outputStream()));
        byte[] bArr = new byte[8192];
        long j = 0;
        while (true) {
            int read = this.inputStream.read(bArr, 0, 8192);
            if (read != -1) {
                CancelationSignal cancelationSignal = this.cancelationSignal;
                if (cancelationSignal == null || !cancelationSignal.isCanceled()) {
                    createFor.write(bArr, 0, read);
                    j += (long) read;
                    SignalServiceAttachment.ProgressListener progressListener = this.progressListener;
                    if (progressListener != null) {
                        progressListener.onAttachmentProgress(this.contentLength, j);
                    }
                } else {
                    throw new IOException("Canceled!");
                }
            } else {
                createFor.flush();
                this.digest = createFor.getTransmittedDigest();
                return;
            }
        }
    }

    @Override // okhttp3.RequestBody
    public long contentLength() {
        long j = this.contentLength;
        if (j > 0) {
            return j - this.contentStart;
        }
        return -1;
    }

    public byte[] getTransmittedDigest() {
        return this.digest;
    }
}
