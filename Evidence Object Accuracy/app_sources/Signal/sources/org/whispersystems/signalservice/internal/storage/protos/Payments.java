package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class Payments extends GeneratedMessageLite<Payments, Builder> implements PaymentsOrBuilder {
    private static final Payments DEFAULT_INSTANCE;
    public static final int ENABLED_FIELD_NUMBER;
    public static final int ENTROPY_FIELD_NUMBER;
    private static volatile Parser<Payments> PARSER;
    private boolean enabled_;
    private ByteString entropy_ = ByteString.EMPTY;

    private Payments() {
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.PaymentsOrBuilder
    public boolean getEnabled() {
        return this.enabled_;
    }

    public void setEnabled(boolean z) {
        this.enabled_ = z;
    }

    public void clearEnabled() {
        this.enabled_ = false;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.PaymentsOrBuilder
    public ByteString getEntropy() {
        return this.entropy_;
    }

    public void setEntropy(ByteString byteString) {
        byteString.getClass();
        this.entropy_ = byteString;
    }

    public void clearEntropy() {
        this.entropy_ = getDefaultInstance().getEntropy();
    }

    public static Payments parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Payments parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Payments parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Payments parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Payments parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Payments parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Payments parseFrom(InputStream inputStream) throws IOException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Payments parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Payments parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Payments) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Payments parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Payments) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Payments parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Payments parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Payments) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Payments payments) {
        return DEFAULT_INSTANCE.createBuilder(payments);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Payments, Builder> implements PaymentsOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(Payments.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.PaymentsOrBuilder
        public boolean getEnabled() {
            return ((Payments) this.instance).getEnabled();
        }

        public Builder setEnabled(boolean z) {
            copyOnWrite();
            ((Payments) this.instance).setEnabled(z);
            return this;
        }

        public Builder clearEnabled() {
            copyOnWrite();
            ((Payments) this.instance).clearEnabled();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.PaymentsOrBuilder
        public ByteString getEntropy() {
            return ((Payments) this.instance).getEntropy();
        }

        public Builder setEntropy(ByteString byteString) {
            copyOnWrite();
            ((Payments) this.instance).setEntropy(byteString);
            return this;
        }

        public Builder clearEntropy() {
            copyOnWrite();
            ((Payments) this.instance).clearEntropy();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.Payments$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Payments();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0007\u0002\n", new Object[]{"enabled_", "entropy_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Payments> parser = PARSER;
                if (parser == null) {
                    synchronized (Payments.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Payments payments = new Payments();
        DEFAULT_INSTANCE = payments;
        GeneratedMessageLite.registerDefaultInstance(Payments.class, payments);
    }

    public static Payments getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Payments> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
