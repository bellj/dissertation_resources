package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.signal.libsignal.protocol.IdentityKey;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public class IdentityCheckRequest {
    @JsonProperty("elements")
    private final List<AciFingerprintPair> aciFingerprintPairs;

    public IdentityCheckRequest(List<AciFingerprintPair> list) {
        this.aciFingerprintPairs = list;
    }

    public List<AciFingerprintPair> getAciFingerprintPairs() {
        return this.aciFingerprintPairs;
    }

    /* loaded from: classes.dex */
    public static final class AciFingerprintPair {
        @JsonProperty
        @JsonSerialize(using = JsonUtil.ServiceIdSerializer.class)
        private final ServiceId aci;
        @JsonProperty
        private final String fingerprint;

        public AciFingerprintPair(ServiceId serviceId, IdentityKey identityKey) {
            this.aci = serviceId;
            try {
                this.fingerprint = Base64.encodeBytes(MessageDigest.getInstance("SHA-256").digest(identityKey.serialize()), 0, 4);
            } catch (NoSuchAlgorithmException e) {
                throw new AssertionError(e);
            }
        }

        public ServiceId getAci() {
            return this.aci;
        }

        public String getFingerprint() {
            return this.fingerprint;
        }
    }
}
