package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class SubmitPushChallengePayload {
    @JsonProperty
    private String challenge;
    @JsonProperty
    private String type;

    public SubmitPushChallengePayload() {
    }

    public SubmitPushChallengePayload(String str) {
        this.type = "rateLimitPushChallenge";
        this.challenge = str;
    }
}
