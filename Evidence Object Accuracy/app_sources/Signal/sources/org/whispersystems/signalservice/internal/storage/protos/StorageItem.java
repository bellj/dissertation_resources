package org.whispersystems.signalservice.internal.storage.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class StorageItem extends GeneratedMessageLite<StorageItem, Builder> implements StorageItemOrBuilder {
    private static final StorageItem DEFAULT_INSTANCE;
    public static final int KEY_FIELD_NUMBER;
    private static volatile Parser<StorageItem> PARSER;
    public static final int VALUE_FIELD_NUMBER;
    private ByteString key_;
    private ByteString value_;

    private StorageItem() {
        ByteString byteString = ByteString.EMPTY;
        this.key_ = byteString;
        this.value_ = byteString;
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemOrBuilder
    public ByteString getKey() {
        return this.key_;
    }

    public void setKey(ByteString byteString) {
        byteString.getClass();
        this.key_ = byteString;
    }

    public void clearKey() {
        this.key_ = getDefaultInstance().getKey();
    }

    @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemOrBuilder
    public ByteString getValue() {
        return this.value_;
    }

    public void setValue(ByteString byteString) {
        byteString.getClass();
        this.value_ = byteString;
    }

    public void clearValue() {
        this.value_ = getDefaultInstance().getValue();
    }

    public static StorageItem parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static StorageItem parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static StorageItem parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static StorageItem parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static StorageItem parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static StorageItem parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static StorageItem parseFrom(InputStream inputStream) throws IOException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageItem parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageItem parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (StorageItem) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static StorageItem parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageItem) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static StorageItem parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static StorageItem parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (StorageItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(StorageItem storageItem) {
        return DEFAULT_INSTANCE.createBuilder(storageItem);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<StorageItem, Builder> implements StorageItemOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(StorageItem.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemOrBuilder
        public ByteString getKey() {
            return ((StorageItem) this.instance).getKey();
        }

        public Builder setKey(ByteString byteString) {
            copyOnWrite();
            ((StorageItem) this.instance).setKey(byteString);
            return this;
        }

        public Builder clearKey() {
            copyOnWrite();
            ((StorageItem) this.instance).clearKey();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.storage.protos.StorageItemOrBuilder
        public ByteString getValue() {
            return ((StorageItem) this.instance).getValue();
        }

        public Builder setValue(ByteString byteString) {
            copyOnWrite();
            ((StorageItem) this.instance).setValue(byteString);
            return this;
        }

        public Builder clearValue() {
            copyOnWrite();
            ((StorageItem) this.instance).clearValue();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.storage.protos.StorageItem$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new StorageItem();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\n", new Object[]{"key_", "value_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<StorageItem> parser = PARSER;
                if (parser == null) {
                    synchronized (StorageItem.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        StorageItem storageItem = new StorageItem();
        DEFAULT_INSTANCE = storageItem;
        GeneratedMessageLite.registerDefaultInstance(StorageItem.class, storageItem);
    }

    public static StorageItem getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<StorageItem> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
