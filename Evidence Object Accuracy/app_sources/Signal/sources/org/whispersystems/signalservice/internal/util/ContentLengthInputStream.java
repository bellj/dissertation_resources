package org.whispersystems.signalservice.internal.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes5.dex */
public class ContentLengthInputStream extends FilterInputStream {
    private long bytesRemaining;

    public ContentLengthInputStream(InputStream inputStream, long j) {
        super(inputStream);
        this.bytesRemaining = j;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        if (this.bytesRemaining == 0) {
            return -1;
        }
        int read = super.read();
        this.bytesRemaining--;
        return read;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        long j = this.bytesRemaining;
        if (j == 0) {
            return -1;
        }
        int read = super.read(bArr, i, Math.min(i2, Util.toIntExact(j)));
        this.bytesRemaining -= (long) read;
        return read;
    }
}
