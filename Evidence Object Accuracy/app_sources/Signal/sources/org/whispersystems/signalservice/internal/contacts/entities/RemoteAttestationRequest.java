package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class RemoteAttestationRequest {
    @JsonProperty
    private byte[] clientPublic;
    @JsonProperty
    private int iasVersion;

    public RemoteAttestationRequest() {
    }

    public RemoteAttestationRequest(byte[] bArr) {
        this.clientPublic = bArr;
        this.iasVersion = 4;
    }

    public byte[] getClientPublic() {
        return this.clientPublic;
    }
}
