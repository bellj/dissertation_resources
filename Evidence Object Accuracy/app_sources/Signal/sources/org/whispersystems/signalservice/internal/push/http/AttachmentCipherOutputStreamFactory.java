package org.whispersystems.signalservice.internal.push.http;

import java.io.IOException;
import java.io.OutputStream;
import org.whispersystems.signalservice.api.crypto.AttachmentCipherOutputStream;
import org.whispersystems.signalservice.api.crypto.DigestingOutputStream;

/* loaded from: classes5.dex */
public class AttachmentCipherOutputStreamFactory implements OutputStreamFactory {
    private final byte[] iv;
    private final byte[] key;

    public AttachmentCipherOutputStreamFactory(byte[] bArr, byte[] bArr2) {
        this.key = bArr;
        this.iv = bArr2;
    }

    @Override // org.whispersystems.signalservice.internal.push.http.OutputStreamFactory
    public DigestingOutputStream createFor(OutputStream outputStream) throws IOException {
        return new AttachmentCipherOutputStream(this.key, this.iv, outputStream);
    }
}
