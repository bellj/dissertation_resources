package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class SignalServiceEnvelopeProto extends GeneratedMessageLite<SignalServiceEnvelopeProto, Builder> implements SignalServiceEnvelopeProtoOrBuilder {
    public static final int CONTENT_FIELD_NUMBER;
    private static final SignalServiceEnvelopeProto DEFAULT_INSTANCE;
    public static final int DESTINATIONUUID_FIELD_NUMBER;
    public static final int DEVICEID_FIELD_NUMBER;
    public static final int LEGACYMESSAGE_FIELD_NUMBER;
    private static volatile Parser<SignalServiceEnvelopeProto> PARSER;
    public static final int SERVERDELIVEREDTIMESTAMP_FIELD_NUMBER;
    public static final int SERVERGUID_FIELD_NUMBER;
    public static final int SERVERRECEIVEDTIMESTAMP_FIELD_NUMBER;
    public static final int SOURCEE164_FIELD_NUMBER;
    public static final int SOURCEUUID_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int TYPE_FIELD_NUMBER;
    private int bitField0_;
    private ByteString content_;
    private String destinationUuid_;
    private int deviceId_;
    private ByteString legacyMessage_;
    private long serverDeliveredTimestamp_;
    private String serverGuid_;
    private long serverReceivedTimestamp_;
    private String sourceE164_ = "";
    private String sourceUuid_ = "";
    private long timestamp_;
    private int type_;

    private SignalServiceEnvelopeProto() {
        ByteString byteString = ByteString.EMPTY;
        this.legacyMessage_ = byteString;
        this.content_ = byteString;
        this.serverGuid_ = "";
        this.destinationUuid_ = "";
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasType() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public int getType() {
        return this.type_;
    }

    public void setType(int i) {
        this.bitField0_ |= 1;
        this.type_ = i;
    }

    public void clearType() {
        this.bitField0_ &= -2;
        this.type_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasSourceUuid() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public String getSourceUuid() {
        return this.sourceUuid_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public ByteString getSourceUuidBytes() {
        return ByteString.copyFromUtf8(this.sourceUuid_);
    }

    public void setSourceUuid(String str) {
        str.getClass();
        this.bitField0_ |= 2;
        this.sourceUuid_ = str;
    }

    public void clearSourceUuid() {
        this.bitField0_ &= -3;
        this.sourceUuid_ = getDefaultInstance().getSourceUuid();
    }

    public void setSourceUuidBytes(ByteString byteString) {
        this.sourceUuid_ = byteString.toStringUtf8();
        this.bitField0_ |= 2;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasSourceE164() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public String getSourceE164() {
        return this.sourceE164_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public ByteString getSourceE164Bytes() {
        return ByteString.copyFromUtf8(this.sourceE164_);
    }

    public void setSourceE164(String str) {
        str.getClass();
        this.bitField0_ |= 4;
        this.sourceE164_ = str;
    }

    public void clearSourceE164() {
        this.bitField0_ &= -5;
        this.sourceE164_ = getDefaultInstance().getSourceE164();
    }

    public void setSourceE164Bytes(ByteString byteString) {
        this.sourceE164_ = byteString.toStringUtf8();
        this.bitField0_ |= 4;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasDeviceId() {
        return (this.bitField0_ & 8) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public int getDeviceId() {
        return this.deviceId_;
    }

    public void setDeviceId(int i) {
        this.bitField0_ |= 8;
        this.deviceId_ = i;
    }

    public void clearDeviceId() {
        this.bitField0_ &= -9;
        this.deviceId_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasLegacyMessage() {
        return (this.bitField0_ & 16) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public ByteString getLegacyMessage() {
        return this.legacyMessage_;
    }

    public void setLegacyMessage(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 16;
        this.legacyMessage_ = byteString;
    }

    public void clearLegacyMessage() {
        this.bitField0_ &= -17;
        this.legacyMessage_ = getDefaultInstance().getLegacyMessage();
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasContent() {
        return (this.bitField0_ & 32) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public ByteString getContent() {
        return this.content_;
    }

    public void setContent(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 32;
        this.content_ = byteString;
    }

    public void clearContent() {
        this.bitField0_ &= -33;
        this.content_ = getDefaultInstance().getContent();
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasTimestamp() {
        return (this.bitField0_ & 64) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.bitField0_ |= 64;
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.bitField0_ &= -65;
        this.timestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasServerReceivedTimestamp() {
        return (this.bitField0_ & 128) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public long getServerReceivedTimestamp() {
        return this.serverReceivedTimestamp_;
    }

    public void setServerReceivedTimestamp(long j) {
        this.bitField0_ |= 128;
        this.serverReceivedTimestamp_ = j;
    }

    public void clearServerReceivedTimestamp() {
        this.bitField0_ &= -129;
        this.serverReceivedTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasServerDeliveredTimestamp() {
        return (this.bitField0_ & 256) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public long getServerDeliveredTimestamp() {
        return this.serverDeliveredTimestamp_;
    }

    public void setServerDeliveredTimestamp(long j) {
        this.bitField0_ |= 256;
        this.serverDeliveredTimestamp_ = j;
    }

    public void clearServerDeliveredTimestamp() {
        this.bitField0_ &= -257;
        this.serverDeliveredTimestamp_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasServerGuid() {
        return (this.bitField0_ & 512) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public String getServerGuid() {
        return this.serverGuid_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public ByteString getServerGuidBytes() {
        return ByteString.copyFromUtf8(this.serverGuid_);
    }

    public void setServerGuid(String str) {
        str.getClass();
        this.bitField0_ |= 512;
        this.serverGuid_ = str;
    }

    public void clearServerGuid() {
        this.bitField0_ &= -513;
        this.serverGuid_ = getDefaultInstance().getServerGuid();
    }

    public void setServerGuidBytes(ByteString byteString) {
        this.serverGuid_ = byteString.toStringUtf8();
        this.bitField0_ |= 512;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public boolean hasDestinationUuid() {
        return (this.bitField0_ & 1024) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public String getDestinationUuid() {
        return this.destinationUuid_;
    }

    @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
    public ByteString getDestinationUuidBytes() {
        return ByteString.copyFromUtf8(this.destinationUuid_);
    }

    public void setDestinationUuid(String str) {
        str.getClass();
        this.bitField0_ |= 1024;
        this.destinationUuid_ = str;
    }

    public void clearDestinationUuid() {
        this.bitField0_ &= -1025;
        this.destinationUuid_ = getDefaultInstance().getDestinationUuid();
    }

    public void setDestinationUuidBytes(ByteString byteString) {
        this.destinationUuid_ = byteString.toStringUtf8();
        this.bitField0_ |= 1024;
    }

    public static SignalServiceEnvelopeProto parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static SignalServiceEnvelopeProto parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static SignalServiceEnvelopeProto parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static SignalServiceEnvelopeProto parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static SignalServiceEnvelopeProto parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static SignalServiceEnvelopeProto parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static SignalServiceEnvelopeProto parseFrom(InputStream inputStream) throws IOException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static SignalServiceEnvelopeProto parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static SignalServiceEnvelopeProto parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static SignalServiceEnvelopeProto parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static SignalServiceEnvelopeProto parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static SignalServiceEnvelopeProto parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (SignalServiceEnvelopeProto) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(SignalServiceEnvelopeProto signalServiceEnvelopeProto) {
        return DEFAULT_INSTANCE.createBuilder(signalServiceEnvelopeProto);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<SignalServiceEnvelopeProto, Builder> implements SignalServiceEnvelopeProtoOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(SignalServiceEnvelopeProto.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasType() {
            return ((SignalServiceEnvelopeProto) this.instance).hasType();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public int getType() {
            return ((SignalServiceEnvelopeProto) this.instance).getType();
        }

        public Builder setType(int i) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setType(i);
            return this;
        }

        public Builder clearType() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearType();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasSourceUuid() {
            return ((SignalServiceEnvelopeProto) this.instance).hasSourceUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public String getSourceUuid() {
            return ((SignalServiceEnvelopeProto) this.instance).getSourceUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public ByteString getSourceUuidBytes() {
            return ((SignalServiceEnvelopeProto) this.instance).getSourceUuidBytes();
        }

        public Builder setSourceUuid(String str) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setSourceUuid(str);
            return this;
        }

        public Builder clearSourceUuid() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearSourceUuid();
            return this;
        }

        public Builder setSourceUuidBytes(ByteString byteString) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setSourceUuidBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasSourceE164() {
            return ((SignalServiceEnvelopeProto) this.instance).hasSourceE164();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public String getSourceE164() {
            return ((SignalServiceEnvelopeProto) this.instance).getSourceE164();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public ByteString getSourceE164Bytes() {
            return ((SignalServiceEnvelopeProto) this.instance).getSourceE164Bytes();
        }

        public Builder setSourceE164(String str) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setSourceE164(str);
            return this;
        }

        public Builder clearSourceE164() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearSourceE164();
            return this;
        }

        public Builder setSourceE164Bytes(ByteString byteString) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setSourceE164Bytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasDeviceId() {
            return ((SignalServiceEnvelopeProto) this.instance).hasDeviceId();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public int getDeviceId() {
            return ((SignalServiceEnvelopeProto) this.instance).getDeviceId();
        }

        public Builder setDeviceId(int i) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setDeviceId(i);
            return this;
        }

        public Builder clearDeviceId() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearDeviceId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasLegacyMessage() {
            return ((SignalServiceEnvelopeProto) this.instance).hasLegacyMessage();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public ByteString getLegacyMessage() {
            return ((SignalServiceEnvelopeProto) this.instance).getLegacyMessage();
        }

        public Builder setLegacyMessage(ByteString byteString) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setLegacyMessage(byteString);
            return this;
        }

        public Builder clearLegacyMessage() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearLegacyMessage();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasContent() {
            return ((SignalServiceEnvelopeProto) this.instance).hasContent();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public ByteString getContent() {
            return ((SignalServiceEnvelopeProto) this.instance).getContent();
        }

        public Builder setContent(ByteString byteString) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setContent(byteString);
            return this;
        }

        public Builder clearContent() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearContent();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasTimestamp() {
            return ((SignalServiceEnvelopeProto) this.instance).hasTimestamp();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public long getTimestamp() {
            return ((SignalServiceEnvelopeProto) this.instance).getTimestamp();
        }

        public Builder setTimestamp(long j) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setTimestamp(j);
            return this;
        }

        public Builder clearTimestamp() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasServerReceivedTimestamp() {
            return ((SignalServiceEnvelopeProto) this.instance).hasServerReceivedTimestamp();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public long getServerReceivedTimestamp() {
            return ((SignalServiceEnvelopeProto) this.instance).getServerReceivedTimestamp();
        }

        public Builder setServerReceivedTimestamp(long j) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setServerReceivedTimestamp(j);
            return this;
        }

        public Builder clearServerReceivedTimestamp() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearServerReceivedTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasServerDeliveredTimestamp() {
            return ((SignalServiceEnvelopeProto) this.instance).hasServerDeliveredTimestamp();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public long getServerDeliveredTimestamp() {
            return ((SignalServiceEnvelopeProto) this.instance).getServerDeliveredTimestamp();
        }

        public Builder setServerDeliveredTimestamp(long j) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setServerDeliveredTimestamp(j);
            return this;
        }

        public Builder clearServerDeliveredTimestamp() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearServerDeliveredTimestamp();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasServerGuid() {
            return ((SignalServiceEnvelopeProto) this.instance).hasServerGuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public String getServerGuid() {
            return ((SignalServiceEnvelopeProto) this.instance).getServerGuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public ByteString getServerGuidBytes() {
            return ((SignalServiceEnvelopeProto) this.instance).getServerGuidBytes();
        }

        public Builder setServerGuid(String str) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setServerGuid(str);
            return this;
        }

        public Builder clearServerGuid() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearServerGuid();
            return this;
        }

        public Builder setServerGuidBytes(ByteString byteString) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setServerGuidBytes(byteString);
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public boolean hasDestinationUuid() {
            return ((SignalServiceEnvelopeProto) this.instance).hasDestinationUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public String getDestinationUuid() {
            return ((SignalServiceEnvelopeProto) this.instance).getDestinationUuid();
        }

        @Override // org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProtoOrBuilder
        public ByteString getDestinationUuidBytes() {
            return ((SignalServiceEnvelopeProto) this.instance).getDestinationUuidBytes();
        }

        public Builder setDestinationUuid(String str) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setDestinationUuid(str);
            return this;
        }

        public Builder clearDestinationUuid() {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).clearDestinationUuid();
            return this;
        }

        public Builder setDestinationUuidBytes(ByteString byteString) {
            copyOnWrite();
            ((SignalServiceEnvelopeProto) this.instance).setDestinationUuidBytes(byteString);
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.serialize.protos.SignalServiceEnvelopeProto$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new SignalServiceEnvelopeProto();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u000b\u0000\u0001\u0001\u000b\u000b\u0000\u0000\u0000\u0001င\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004င\u0003\u0005ည\u0004\u0006ည\u0005\u0007ဂ\u0006\bဂ\u0007\tဂ\b\nဈ\t\u000bဈ\n", new Object[]{"bitField0_", "type_", "sourceUuid_", "sourceE164_", "deviceId_", "legacyMessage_", "content_", "timestamp_", "serverReceivedTimestamp_", "serverDeliveredTimestamp_", "serverGuid_", "destinationUuid_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<SignalServiceEnvelopeProto> parser = PARSER;
                if (parser == null) {
                    synchronized (SignalServiceEnvelopeProto.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        SignalServiceEnvelopeProto signalServiceEnvelopeProto = new SignalServiceEnvelopeProto();
        DEFAULT_INSTANCE = signalServiceEnvelopeProto;
        GeneratedMessageLite.registerDefaultInstance(SignalServiceEnvelopeProto.class, signalServiceEnvelopeProto);
    }

    public static SignalServiceEnvelopeProto getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<SignalServiceEnvelopeProto> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
