package org.whispersystems.signalservice.internal.push;

import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes5.dex */
public class DeviceLimitExceededException extends NonSuccessfulResponseCodeException {
    private final DeviceLimit deviceLimit;

    public DeviceLimitExceededException(DeviceLimit deviceLimit) {
        super(411);
        this.deviceLimit = deviceLimit;
    }

    public int getCurrent() {
        return this.deviceLimit.getCurrent();
    }

    public int getMax() {
        return this.deviceLimit.getMax();
    }
}
