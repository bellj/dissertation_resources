package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/* loaded from: classes.dex */
public class RemoteConfigResponse {
    @JsonProperty
    private List<Config> config;

    public List<Config> getConfig() {
        return this.config;
    }

    /* loaded from: classes.dex */
    public static class Config {
        @JsonProperty
        private boolean enabled;
        @JsonProperty
        private String name;
        @JsonProperty
        private String value;

        public String getName() {
            return this.name;
        }

        public boolean isEnabled() {
            return this.enabled;
        }

        public String getValue() {
            return this.value;
        }
    }
}
