package org.whispersystems.signalservice.internal.configuration;

/* loaded from: classes5.dex */
public class SignalProxy {
    private final String host;
    private final int port;

    public SignalProxy(String str, int i) {
        this.host = str;
        this.port = i;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }
}
