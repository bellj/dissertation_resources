package org.whispersystems.signalservice.internal.push.exceptions;

import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;

/* loaded from: classes5.dex */
public final class GroupPatchNotAcceptedException extends NonSuccessfulResponseCodeException {
    public GroupPatchNotAcceptedException() {
        super(400);
    }
}
