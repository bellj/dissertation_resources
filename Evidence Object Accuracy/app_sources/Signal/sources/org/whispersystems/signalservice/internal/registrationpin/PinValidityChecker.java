package org.whispersystems.signalservice.internal.registrationpin;

/* loaded from: classes5.dex */
public final class PinValidityChecker {
    public static boolean valid(String str) {
        String trim = str.trim();
        if (trim.isEmpty()) {
            return false;
        }
        if (!PinString.allNumeric(trim)) {
            return true;
        }
        String arabic = PinString.toArabic(trim);
        if (sequential(arabic) || sequential(reverse(arabic)) || allTheSame(arabic)) {
            return false;
        }
        return true;
    }

    private static String reverse(String str) {
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length / 2; i++) {
            char c = charArray[i];
            charArray[i] = charArray[(charArray.length - i) - 1];
            charArray[(charArray.length - i) - 1] = c;
        }
        return new String(charArray);
    }

    private static boolean sequential(String str) {
        int length = str.length();
        if (length == 0) {
            return false;
        }
        char charAt = str.charAt(0);
        int i = 1;
        while (i < length) {
            char charAt2 = str.charAt(i);
            if (charAt2 != charAt + 1) {
                return false;
            }
            i++;
            charAt = charAt2;
        }
        return true;
    }

    private static boolean allTheSame(String str) {
        int length = str.length();
        if (length == 0) {
            return false;
        }
        char charAt = str.charAt(0);
        for (int i = 1; i < length; i++) {
            if (str.charAt(i) != charAt) {
                return false;
            }
        }
        return true;
    }
}
