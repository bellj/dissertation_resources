package org.whispersystems.signalservice.internal.serialize.protos;

import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;
import org.whispersystems.signalservice.internal.serialize.protos.SignalServiceContentProto;

/* loaded from: classes5.dex */
public interface SignalServiceContentProtoOrBuilder extends MessageLiteOrBuilder {
    SignalServiceProtos.Content getContent();

    SignalServiceContentProto.DataCase getDataCase();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ MessageLite getDefaultInstanceForType();

    SignalServiceProtos.DataMessage getLegacyDataMessage();

    AddressProto getLocalAddress();

    MetadataProto getMetadata();

    boolean hasContent();

    boolean hasLegacyDataMessage();

    boolean hasLocalAddress();

    boolean hasMetadata();

    @Override // com.google.protobuf.MessageLiteOrBuilder
    /* synthetic */ boolean isInitialized();
}
