package org.whispersystems.signalservice.internal.websocket;

import j$.util.Optional;
import j$.util.function.Function;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.whispersystems.signalservice.api.push.exceptions.AuthorizationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.CaptchaRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.DeprecatedVersionException;
import org.whispersystems.signalservice.api.push.exceptions.ExpectationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.MalformedResponseException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;
import org.whispersystems.signalservice.api.push.exceptions.ProofRequiredException;
import org.whispersystems.signalservice.api.push.exceptions.RateLimitException;
import org.whispersystems.signalservice.api.push.exceptions.ServerRejectedException;
import org.whispersystems.signalservice.internal.push.AuthCredentials;
import org.whispersystems.signalservice.internal.push.DeviceLimit;
import org.whispersystems.signalservice.internal.push.DeviceLimitExceededException;
import org.whispersystems.signalservice.internal.push.LockedException;
import org.whispersystems.signalservice.internal.push.MismatchedDevices;
import org.whispersystems.signalservice.internal.push.ProofRequiredResponse;
import org.whispersystems.signalservice.internal.push.PushServiceSocket;
import org.whispersystems.signalservice.internal.push.StaleDevices;
import org.whispersystems.signalservice.internal.push.exceptions.MismatchedDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.StaleDevicesException;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.signalservice.internal.util.Util;
import org.whispersystems.signalservice.internal.websocket.ErrorMapper;

/* loaded from: classes5.dex */
public final class DefaultErrorMapper implements ErrorMapper {
    private static final DefaultErrorMapper INSTANCE = new DefaultErrorMapper();
    private final Map<Integer, ErrorMapper> customErrorMappers;

    @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
    public /* synthetic */ Throwable parseError(int i) {
        return ErrorMapper.CC.$default$parseError(this, i);
    }

    public static DefaultErrorMapper getDefault() {
        return INSTANCE;
    }

    public static Builder extend() {
        return new Builder();
    }

    private DefaultErrorMapper() {
        this(Collections.emptyMap());
    }

    private DefaultErrorMapper(Map<Integer, ErrorMapper> map) {
        this.customErrorMappers = map;
    }

    public Throwable parseError(WebsocketResponse websocketResponse) {
        return parseError(websocketResponse.getStatus(), websocketResponse.getBody(), new DefaultErrorMapper$$ExternalSyntheticLambda0(websocketResponse));
    }

    @Override // org.whispersystems.signalservice.internal.websocket.ErrorMapper
    public Throwable parseError(int i, String str, Function<String, String> function) {
        if (this.customErrorMappers.containsKey(Integer.valueOf(i))) {
            try {
                return this.customErrorMappers.get(Integer.valueOf(i)).parseError(i, str, function);
            } catch (MalformedResponseException e) {
                return e;
            }
        } else {
            if (i != 413) {
                if (i == 417) {
                    return new ExpectationFailedException();
                }
                String str2 = null;
                if (i == 423) {
                    try {
                        PushServiceSocket.RegistrationLockFailure registrationLockFailure = (PushServiceSocket.RegistrationLockFailure) JsonUtil.fromJsonResponse(str, PushServiceSocket.RegistrationLockFailure.class);
                        AuthCredentials authCredentials = registrationLockFailure.backupCredentials;
                        if (authCredentials != null) {
                            str2 = authCredentials.asBasic();
                        }
                        return new LockedException(registrationLockFailure.length, registrationLockFailure.timeRemaining, str2);
                    } catch (MalformedResponseException e2) {
                        return e2;
                    }
                } else if (i == 499) {
                    return new DeprecatedVersionException();
                } else {
                    if (i == 508) {
                        return new ServerRejectedException();
                    }
                    if (i == 428) {
                        try {
                            return new ProofRequiredException((ProofRequiredResponse) JsonUtil.fromJsonResponse(str, ProofRequiredResponse.class), (long) Util.parseInt(function.apply("Retry-After"), -1));
                        } catch (MalformedResponseException e3) {
                            return e3;
                        }
                    } else if (i != 429) {
                        switch (i) {
                            case 401:
                            case 403:
                                return new AuthorizationFailedException(i, "Authorization failed!");
                            case 402:
                                return new CaptchaRequiredException();
                            case 404:
                                return new NotFoundException("Not found");
                            default:
                                switch (i) {
                                    case 409:
                                        try {
                                            return new MismatchedDevicesException((MismatchedDevices) JsonUtil.fromJsonResponse(str, MismatchedDevices.class));
                                        } catch (MalformedResponseException e4) {
                                            return e4;
                                        }
                                    case 410:
                                        try {
                                            return new StaleDevicesException((StaleDevices) JsonUtil.fromJsonResponse(str, StaleDevices.class));
                                        } catch (MalformedResponseException e5) {
                                            return e5;
                                        }
                                    case 411:
                                        try {
                                            return new DeviceLimitExceededException((DeviceLimit) JsonUtil.fromJsonResponse(str, DeviceLimit.class));
                                        } catch (MalformedResponseException e6) {
                                            return e6;
                                        }
                                    default:
                                        if (i == 200 || i == 202 || i == 204) {
                                            return null;
                                        }
                                        return new NonSuccessfulResponseCodeException(i, "Bad response: " + i);
                                }
                        }
                    }
                }
            }
            long parseLong = Util.parseLong(function.apply("Retry-After"), -1);
            Optional of = parseLong != -1 ? Optional.of(Long.valueOf(TimeUnit.SECONDS.toMillis(parseLong))) : Optional.empty();
            return new RateLimitException(i, "Rate limit exceeded: " + i, of);
        }
    }

    /* loaded from: classes5.dex */
    public static class Builder {
        private final Map<Integer, ErrorMapper> customErrorMappers = new HashMap();

        public Builder withCustom(int i, ErrorMapper errorMapper) {
            this.customErrorMappers.put(Integer.valueOf(i), errorMapper);
            return this;
        }

        public ErrorMapper build() {
            return new DefaultErrorMapper(this.customErrorMappers);
        }
    }
}
