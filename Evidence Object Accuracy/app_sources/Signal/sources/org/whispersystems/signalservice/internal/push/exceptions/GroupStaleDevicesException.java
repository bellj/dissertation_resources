package org.whispersystems.signalservice.internal.push.exceptions;

import java.util.Arrays;
import java.util.List;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.push.GroupStaleDevices;

/* loaded from: classes5.dex */
public class GroupStaleDevicesException extends NonSuccessfulResponseCodeException {
    private final List<GroupStaleDevices> staleDevices;

    public GroupStaleDevicesException(GroupStaleDevices[] groupStaleDevicesArr) {
        super(410);
        this.staleDevices = Arrays.asList(groupStaleDevicesArr);
    }

    public List<GroupStaleDevices> getStaleDevices() {
        return this.staleDevices;
    }
}
