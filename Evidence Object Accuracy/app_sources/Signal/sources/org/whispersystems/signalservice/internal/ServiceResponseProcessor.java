package org.whispersystems.signalservice.internal;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes5.dex */
public abstract class ServiceResponseProcessor<T> {
    protected final ServiceResponse<T> response;

    public ServiceResponseProcessor(ServiceResponse<T> serviceResponse) {
        this.response = serviceResponse;
    }

    public ServiceResponse<T> getResponse() {
        return this.response;
    }

    public T getResult() {
        Preconditions.checkArgument(this.response.getResult().isPresent());
        return this.response.getResult().get();
    }

    public T getResultOrThrow() throws IOException {
        if (hasResult()) {
            return getResult();
        }
        Throwable error = getError();
        if (error instanceof IOException) {
            throw ((IOException) error);
        } else if (error instanceof RuntimeException) {
            throw ((RuntimeException) error);
        } else if ((error instanceof InterruptedException) || (error instanceof TimeoutException)) {
            throw new IOException(error);
        } else {
            throw new IllegalStateException("Unexpected error type for response processor", error);
        }
    }

    public boolean hasResult() {
        return this.response.getResult().isPresent();
    }

    public Throwable getError() {
        return (Throwable) OptionalUtil.or(this.response.getApplicationError(), this.response.getExecutionError()).orElse(null);
    }

    public boolean authorizationFailed() {
        return this.response.getStatus() == 401 || this.response.getStatus() == 403;
    }

    public boolean captchaRequired() {
        return this.response.getStatus() == 402;
    }

    public boolean notFound() {
        return this.response.getStatus() == 404;
    }

    protected boolean mismatchedDevices() {
        return this.response.getStatus() == 409;
    }

    protected boolean staleDevices() {
        return this.response.getStatus() == 410;
    }

    protected boolean deviceLimitedExceeded() {
        return this.response.getStatus() == 411;
    }

    public boolean rateLimit() {
        return this.response.getStatus() == 413 || this.response.getStatus() == 429;
    }

    protected boolean expectationFailed() {
        return this.response.getStatus() == 417;
    }

    public boolean registrationLock() {
        return this.response.getStatus() == 423;
    }

    protected boolean proofRequired() {
        return this.response.getStatus() == 428;
    }

    protected boolean deprecatedVersion() {
        return this.response.getStatus() == 499;
    }

    protected boolean serverRejected() {
        return this.response.getStatus() == 508;
    }

    protected boolean notSuccessful() {
        return (this.response.getStatus() == 200 || this.response.getStatus() == 202 || this.response.getStatus() == 204) ? false : true;
    }

    public boolean genericIoError() {
        Throwable error = getError();
        if (error instanceof NonSuccessfulResponseCodeException) {
            return false;
        }
        if ((error instanceof IOException) || (error instanceof TimeoutException) || (error instanceof InterruptedException)) {
            return true;
        }
        return false;
    }

    /* loaded from: classes5.dex */
    public static final class DefaultProcessor<T> extends ServiceResponseProcessor<T> {
        public DefaultProcessor(ServiceResponse<T> serviceResponse) {
            super(serviceResponse);
        }
    }
}
