package org.whispersystems.signalservice.internal.contacts.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class QueryEnvelope {
    @JsonProperty
    private byte[] data;
    @JsonProperty
    private byte[] iv;
    @JsonProperty
    private byte[] mac;
    @JsonProperty
    private byte[] requestId;

    public QueryEnvelope() {
    }

    public QueryEnvelope(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        this.requestId = bArr;
        this.iv = bArr2;
        this.data = bArr3;
        this.mac = bArr4;
    }
}
