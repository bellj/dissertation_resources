package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialRequest;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public class ReceiptCredentialRequestJson {
    @JsonProperty("receiptCredentialRequest")
    private final String receiptCredentialRequest;

    public ReceiptCredentialRequestJson(ReceiptCredentialRequest receiptCredentialRequest) {
        this.receiptCredentialRequest = Base64.encodeBytes(receiptCredentialRequest.serialize());
    }
}
