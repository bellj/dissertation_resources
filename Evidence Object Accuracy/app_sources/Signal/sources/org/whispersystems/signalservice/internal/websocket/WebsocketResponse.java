package org.whispersystems.signalservice.internal.websocket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes5.dex */
public class WebsocketResponse {
    private final String body;
    private final Map<String, String> headers;
    private final int status;
    private final boolean unidentified;

    public WebsocketResponse(int i, String str, List<String> list, boolean z) {
        this.status = i;
        this.body = str;
        this.headers = parseHeaders(list);
        this.unidentified = z;
    }

    public int getStatus() {
        return this.status;
    }

    public String getBody() {
        return this.body;
    }

    public String getHeader(String str) {
        return this.headers.get(Preconditions.checkNotNull(str.toLowerCase()));
    }

    public boolean isUnidentified() {
        return this.unidentified;
    }

    private static Map<String, String> parseHeaders(List<String> list) {
        int indexOf;
        HashMap hashMap = new HashMap(list.size());
        for (String str : list) {
            if (str != null && str.length() > 0 && (indexOf = str.indexOf(":")) > 0 && indexOf < str.length() - 1) {
                hashMap.put(str.substring(0, indexOf).trim().toLowerCase(), str.substring(indexOf + 1).trim());
            }
        }
        return hashMap;
    }
}
