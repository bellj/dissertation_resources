package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class SignalServiceEnvelopeEntity {
    @JsonProperty
    private byte[] content;
    @JsonProperty
    private String destinationUuid;
    @JsonProperty
    private String guid;
    @JsonProperty
    private byte[] message;
    @JsonProperty
    private String relay;
    @JsonProperty
    private long serverTimestamp;
    @JsonProperty
    private String source;
    @JsonProperty
    private int sourceDevice;
    @JsonProperty
    private String sourceUuid;
    @JsonProperty
    private long timestamp;
    @JsonProperty
    private int type;

    public int getType() {
        return this.type;
    }

    public String getRelay() {
        return this.relay;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getSourceE164() {
        return this.source;
    }

    public String getSourceUuid() {
        return this.sourceUuid;
    }

    public boolean hasSource() {
        return this.sourceUuid != null;
    }

    public int getSourceDevice() {
        return this.sourceDevice;
    }

    public byte[] getMessage() {
        return this.message;
    }

    public byte[] getContent() {
        return this.content;
    }

    public long getServerTimestamp() {
        return this.serverTimestamp;
    }

    public String getServerUuid() {
        return this.guid;
    }

    public String getDestinationUuid() {
        return this.destinationUuid;
    }
}
