package org.whispersystems.signalservice.internal.keybackup.protos;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class RestoreRequest extends GeneratedMessageLite<RestoreRequest, Builder> implements RestoreRequestOrBuilder {
    public static final int BACKUP_ID_FIELD_NUMBER;
    private static final RestoreRequest DEFAULT_INSTANCE;
    private static volatile Parser<RestoreRequest> PARSER;
    public static final int PIN_FIELD_NUMBER;
    public static final int SERVICE_ID_FIELD_NUMBER;
    public static final int TOKEN_FIELD_NUMBER;
    public static final int VALID_FROM_FIELD_NUMBER;
    private ByteString backupId_;
    private int bitField0_;
    private ByteString pin_;
    private ByteString serviceId_;
    private ByteString token_;
    private long validFrom_;

    private RestoreRequest() {
        ByteString byteString = ByteString.EMPTY;
        this.serviceId_ = byteString;
        this.backupId_ = byteString;
        this.token_ = byteString;
        this.pin_ = byteString;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public boolean hasServiceId() {
        return (this.bitField0_ & 1) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public ByteString getServiceId() {
        return this.serviceId_;
    }

    public void setServiceId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 1;
        this.serviceId_ = byteString;
    }

    public void clearServiceId() {
        this.bitField0_ &= -2;
        this.serviceId_ = getDefaultInstance().getServiceId();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public boolean hasBackupId() {
        return (this.bitField0_ & 2) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public ByteString getBackupId() {
        return this.backupId_;
    }

    public void setBackupId(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 2;
        this.backupId_ = byteString;
    }

    public void clearBackupId() {
        this.bitField0_ &= -3;
        this.backupId_ = getDefaultInstance().getBackupId();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public boolean hasToken() {
        return (this.bitField0_ & 4) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public ByteString getToken() {
        return this.token_;
    }

    public void setToken(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 4;
        this.token_ = byteString;
    }

    public void clearToken() {
        this.bitField0_ &= -5;
        this.token_ = getDefaultInstance().getToken();
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public boolean hasValidFrom() {
        return (this.bitField0_ & 8) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public long getValidFrom() {
        return this.validFrom_;
    }

    public void setValidFrom(long j) {
        this.bitField0_ |= 8;
        this.validFrom_ = j;
    }

    public void clearValidFrom() {
        this.bitField0_ &= -9;
        this.validFrom_ = 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public boolean hasPin() {
        return (this.bitField0_ & 16) != 0;
    }

    @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
    public ByteString getPin() {
        return this.pin_;
    }

    public void setPin(ByteString byteString) {
        byteString.getClass();
        this.bitField0_ |= 16;
        this.pin_ = byteString;
    }

    public void clearPin() {
        this.bitField0_ &= -17;
        this.pin_ = getDefaultInstance().getPin();
    }

    public static RestoreRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static RestoreRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static RestoreRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static RestoreRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static RestoreRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static RestoreRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static RestoreRequest parseFrom(InputStream inputStream) throws IOException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RestoreRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RestoreRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (RestoreRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RestoreRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RestoreRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RestoreRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static RestoreRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RestoreRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(RestoreRequest restoreRequest) {
        return DEFAULT_INSTANCE.createBuilder(restoreRequest);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<RestoreRequest, Builder> implements RestoreRequestOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(RestoreRequest.DEFAULT_INSTANCE);
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public boolean hasServiceId() {
            return ((RestoreRequest) this.instance).hasServiceId();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public ByteString getServiceId() {
            return ((RestoreRequest) this.instance).getServiceId();
        }

        public Builder setServiceId(ByteString byteString) {
            copyOnWrite();
            ((RestoreRequest) this.instance).setServiceId(byteString);
            return this;
        }

        public Builder clearServiceId() {
            copyOnWrite();
            ((RestoreRequest) this.instance).clearServiceId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public boolean hasBackupId() {
            return ((RestoreRequest) this.instance).hasBackupId();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public ByteString getBackupId() {
            return ((RestoreRequest) this.instance).getBackupId();
        }

        public Builder setBackupId(ByteString byteString) {
            copyOnWrite();
            ((RestoreRequest) this.instance).setBackupId(byteString);
            return this;
        }

        public Builder clearBackupId() {
            copyOnWrite();
            ((RestoreRequest) this.instance).clearBackupId();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public boolean hasToken() {
            return ((RestoreRequest) this.instance).hasToken();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public ByteString getToken() {
            return ((RestoreRequest) this.instance).getToken();
        }

        public Builder setToken(ByteString byteString) {
            copyOnWrite();
            ((RestoreRequest) this.instance).setToken(byteString);
            return this;
        }

        public Builder clearToken() {
            copyOnWrite();
            ((RestoreRequest) this.instance).clearToken();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public boolean hasValidFrom() {
            return ((RestoreRequest) this.instance).hasValidFrom();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public long getValidFrom() {
            return ((RestoreRequest) this.instance).getValidFrom();
        }

        public Builder setValidFrom(long j) {
            copyOnWrite();
            ((RestoreRequest) this.instance).setValidFrom(j);
            return this;
        }

        public Builder clearValidFrom() {
            copyOnWrite();
            ((RestoreRequest) this.instance).clearValidFrom();
            return this;
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public boolean hasPin() {
            return ((RestoreRequest) this.instance).hasPin();
        }

        @Override // org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequestOrBuilder
        public ByteString getPin() {
            return ((RestoreRequest) this.instance).getPin();
        }

        public Builder setPin(ByteString byteString) {
            copyOnWrite();
            ((RestoreRequest) this.instance).setPin(byteString);
            return this;
        }

        public Builder clearPin() {
            copyOnWrite();
            ((RestoreRequest) this.instance).clearPin();
            return this;
        }
    }

    /* renamed from: org.whispersystems.signalservice.internal.keybackup.protos.RestoreRequest$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new RestoreRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001\u0003ည\u0002\u0004ဃ\u0003\u0005ည\u0004", new Object[]{"bitField0_", "serviceId_", "backupId_", "token_", "validFrom_", "pin_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<RestoreRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (RestoreRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        RestoreRequest restoreRequest = new RestoreRequest();
        DEFAULT_INSTANCE = restoreRequest;
        GeneratedMessageLite.registerDefaultInstance(RestoreRequest.class, restoreRequest);
    }

    public static RestoreRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<RestoreRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
