package report;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class ReportOuterClass$ReportRequest extends GeneratedMessageLite<ReportOuterClass$ReportRequest, Builder> implements MessageLiteOrBuilder {
    private static final ReportOuterClass$ReportRequest DEFAULT_INSTANCE;
    private static volatile Parser<ReportOuterClass$ReportRequest> PARSER;

    private ReportOuterClass$ReportRequest() {
    }

    public static ReportOuterClass$ReportRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ReportOuterClass$ReportRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ReportOuterClass$ReportRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ReportOuterClass$ReportRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportRequest parseFrom(InputStream inputStream) throws IOException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReportOuterClass$ReportRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReportOuterClass$ReportRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ReportOuterClass$ReportRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$ReportRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ReportOuterClass$ReportRequest reportOuterClass$ReportRequest) {
        return DEFAULT_INSTANCE.createBuilder(reportOuterClass$ReportRequest);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ReportOuterClass$ReportRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ReportOuterClass$1 reportOuterClass$1) {
            this();
        }

        private Builder() {
            super(ReportOuterClass$ReportRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ReportOuterClass$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ReportOuterClass$ReportRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0000", null);
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ReportOuterClass$ReportRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (ReportOuterClass$ReportRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ReportOuterClass$ReportRequest reportOuterClass$ReportRequest = new ReportOuterClass$ReportRequest();
        DEFAULT_INSTANCE = reportOuterClass$ReportRequest;
        GeneratedMessageLite.registerDefaultInstance(ReportOuterClass$ReportRequest.class, reportOuterClass$ReportRequest);
    }

    public static ReportOuterClass$ReportRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ReportOuterClass$ReportRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
