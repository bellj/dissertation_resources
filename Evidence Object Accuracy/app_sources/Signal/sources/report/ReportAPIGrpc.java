package report;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes5.dex */
public final class ReportAPIGrpc {
    private static volatile MethodDescriptor<ReportOuterClass$ReportRequest, ReportOuterClass$ReportResponse> getGetReportsMethod;

    /* renamed from: report.ReportAPIGrpc$1 */
    /* loaded from: classes5.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private ReportAPIGrpc() {
    }

    public static MethodDescriptor<ReportOuterClass$ReportRequest, ReportOuterClass$ReportResponse> getGetReportsMethod() {
        MethodDescriptor<ReportOuterClass$ReportRequest, ReportOuterClass$ReportResponse> methodDescriptor = getGetReportsMethod;
        if (methodDescriptor == null) {
            synchronized (ReportAPIGrpc.class) {
                methodDescriptor = getGetReportsMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("report.ReportAPI", "GetReports")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(ReportOuterClass$ReportRequest.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(ReportOuterClass$ReportResponse.getDefaultInstance())).build();
                    getGetReportsMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static ReportAPIBlockingStub newBlockingStub(Channel channel) {
        return (ReportAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<ReportAPIBlockingStub>() { // from class: report.ReportAPIGrpc.2
            public ReportAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new ReportAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes5.dex */
    public static final class ReportAPIBlockingStub extends AbstractBlockingStub<ReportAPIBlockingStub> {
        /* synthetic */ ReportAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private ReportAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        public ReportAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new ReportAPIBlockingStub(channel, callOptions);
        }

        public ReportOuterClass$ReportResponse getReports(ReportOuterClass$ReportRequest reportOuterClass$ReportRequest) {
            return (ReportOuterClass$ReportResponse) ClientCalls.blockingUnaryCall(getChannel(), ReportAPIGrpc.getGetReportsMethod(), getCallOptions(), reportOuterClass$ReportRequest);
        }
    }
}
