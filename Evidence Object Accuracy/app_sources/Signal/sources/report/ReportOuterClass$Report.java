package report;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$VerificationReport;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class ReportOuterClass$Report extends GeneratedMessageLite<ReportOuterClass$Report, Builder> implements ReportOuterClass$ReportOrBuilder {
    private static final ReportOuterClass$Report DEFAULT_INSTANCE;
    public static final int FOG_REPORT_ID_FIELD_NUMBER;
    private static volatile Parser<ReportOuterClass$Report> PARSER;
    public static final int PUBKEY_EXPIRY_FIELD_NUMBER;
    public static final int REPORT_FIELD_NUMBER;
    private String fogReportId_ = "";
    private long pubkeyExpiry_;
    private MobileCoinAPI$VerificationReport report_;

    private ReportOuterClass$Report() {
    }

    public String getFogReportId() {
        return this.fogReportId_;
    }

    public ByteString getFogReportIdBytes() {
        return ByteString.copyFromUtf8(this.fogReportId_);
    }

    public void setFogReportId(String str) {
        str.getClass();
        this.fogReportId_ = str;
    }

    public void clearFogReportId() {
        this.fogReportId_ = getDefaultInstance().getFogReportId();
    }

    public void setFogReportIdBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportId_ = byteString.toStringUtf8();
    }

    public boolean hasReport() {
        return this.report_ != null;
    }

    public MobileCoinAPI$VerificationReport getReport() {
        MobileCoinAPI$VerificationReport mobileCoinAPI$VerificationReport = this.report_;
        return mobileCoinAPI$VerificationReport == null ? MobileCoinAPI$VerificationReport.getDefaultInstance() : mobileCoinAPI$VerificationReport;
    }

    public void setReport(MobileCoinAPI$VerificationReport mobileCoinAPI$VerificationReport) {
        mobileCoinAPI$VerificationReport.getClass();
        this.report_ = mobileCoinAPI$VerificationReport;
    }

    public void setReport(MobileCoinAPI$VerificationReport.Builder builder) {
        this.report_ = builder.build();
    }

    public void mergeReport(MobileCoinAPI$VerificationReport mobileCoinAPI$VerificationReport) {
        mobileCoinAPI$VerificationReport.getClass();
        MobileCoinAPI$VerificationReport mobileCoinAPI$VerificationReport2 = this.report_;
        if (mobileCoinAPI$VerificationReport2 == null || mobileCoinAPI$VerificationReport2 == MobileCoinAPI$VerificationReport.getDefaultInstance()) {
            this.report_ = mobileCoinAPI$VerificationReport;
        } else {
            this.report_ = MobileCoinAPI$VerificationReport.newBuilder(this.report_).mergeFrom((MobileCoinAPI$VerificationReport.Builder) mobileCoinAPI$VerificationReport).buildPartial();
        }
    }

    public void clearReport() {
        this.report_ = null;
    }

    public long getPubkeyExpiry() {
        return this.pubkeyExpiry_;
    }

    public void setPubkeyExpiry(long j) {
        this.pubkeyExpiry_ = j;
    }

    public void clearPubkeyExpiry() {
        this.pubkeyExpiry_ = 0;
    }

    public static ReportOuterClass$Report parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ReportOuterClass$Report parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ReportOuterClass$Report parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ReportOuterClass$Report parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ReportOuterClass$Report parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ReportOuterClass$Report parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ReportOuterClass$Report parseFrom(InputStream inputStream) throws IOException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReportOuterClass$Report parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReportOuterClass$Report parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReportOuterClass$Report parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReportOuterClass$Report parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ReportOuterClass$Report parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$Report) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ReportOuterClass$Report reportOuterClass$Report) {
        return DEFAULT_INSTANCE.createBuilder(reportOuterClass$Report);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ReportOuterClass$Report, Builder> implements ReportOuterClass$ReportOrBuilder {
        /* synthetic */ Builder(ReportOuterClass$1 reportOuterClass$1) {
            this();
        }

        private Builder() {
            super(ReportOuterClass$Report.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ReportOuterClass$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ReportOuterClass$Report();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001Ȉ\u0002\t\u0003\u0005", new Object[]{"fogReportId_", "report_", "pubkeyExpiry_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ReportOuterClass$Report> parser = PARSER;
                if (parser == null) {
                    synchronized (ReportOuterClass$Report.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ReportOuterClass$Report reportOuterClass$Report = new ReportOuterClass$Report();
        DEFAULT_INSTANCE = reportOuterClass$Report;
        GeneratedMessageLite.registerDefaultInstance(ReportOuterClass$Report.class, reportOuterClass$Report);
    }

    public static ReportOuterClass$Report getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ReportOuterClass$Report> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
