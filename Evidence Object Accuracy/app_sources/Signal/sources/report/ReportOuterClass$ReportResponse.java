package report;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import report.ReportOuterClass$Report;

/* loaded from: classes5.dex */
public final class ReportOuterClass$ReportResponse extends GeneratedMessageLite<ReportOuterClass$ReportResponse, Builder> implements MessageLiteOrBuilder {
    public static final int CHAIN_FIELD_NUMBER;
    private static final ReportOuterClass$ReportResponse DEFAULT_INSTANCE;
    private static volatile Parser<ReportOuterClass$ReportResponse> PARSER;
    public static final int REPORTS_FIELD_NUMBER;
    public static final int SIGNATURE_FIELD_NUMBER;
    private Internal.ProtobufList<ByteString> chain_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<ReportOuterClass$Report> reports_ = GeneratedMessageLite.emptyProtobufList();
    private ByteString signature_ = ByteString.EMPTY;

    private ReportOuterClass$ReportResponse() {
    }

    public List<ReportOuterClass$Report> getReportsList() {
        return this.reports_;
    }

    public List<? extends ReportOuterClass$ReportOrBuilder> getReportsOrBuilderList() {
        return this.reports_;
    }

    public int getReportsCount() {
        return this.reports_.size();
    }

    public ReportOuterClass$Report getReports(int i) {
        return this.reports_.get(i);
    }

    public ReportOuterClass$ReportOrBuilder getReportsOrBuilder(int i) {
        return this.reports_.get(i);
    }

    private void ensureReportsIsMutable() {
        if (!this.reports_.isModifiable()) {
            this.reports_ = GeneratedMessageLite.mutableCopy(this.reports_);
        }
    }

    public void setReports(int i, ReportOuterClass$Report reportOuterClass$Report) {
        reportOuterClass$Report.getClass();
        ensureReportsIsMutable();
        this.reports_.set(i, reportOuterClass$Report);
    }

    public void setReports(int i, ReportOuterClass$Report.Builder builder) {
        ensureReportsIsMutable();
        this.reports_.set(i, builder.build());
    }

    public void addReports(ReportOuterClass$Report reportOuterClass$Report) {
        reportOuterClass$Report.getClass();
        ensureReportsIsMutable();
        this.reports_.add(reportOuterClass$Report);
    }

    public void addReports(int i, ReportOuterClass$Report reportOuterClass$Report) {
        reportOuterClass$Report.getClass();
        ensureReportsIsMutable();
        this.reports_.add(i, reportOuterClass$Report);
    }

    public void addReports(ReportOuterClass$Report.Builder builder) {
        ensureReportsIsMutable();
        this.reports_.add(builder.build());
    }

    public void addReports(int i, ReportOuterClass$Report.Builder builder) {
        ensureReportsIsMutable();
        this.reports_.add(i, builder.build());
    }

    public void addAllReports(Iterable<? extends ReportOuterClass$Report> iterable) {
        ensureReportsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.reports_);
    }

    public void clearReports() {
        this.reports_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeReports(int i) {
        ensureReportsIsMutable();
        this.reports_.remove(i);
    }

    public List<ByteString> getChainList() {
        return this.chain_;
    }

    public int getChainCount() {
        return this.chain_.size();
    }

    public ByteString getChain(int i) {
        return this.chain_.get(i);
    }

    private void ensureChainIsMutable() {
        if (!this.chain_.isModifiable()) {
            this.chain_ = GeneratedMessageLite.mutableCopy(this.chain_);
        }
    }

    public void setChain(int i, ByteString byteString) {
        byteString.getClass();
        ensureChainIsMutable();
        this.chain_.set(i, byteString);
    }

    public void addChain(ByteString byteString) {
        byteString.getClass();
        ensureChainIsMutable();
        this.chain_.add(byteString);
    }

    public void addAllChain(Iterable<? extends ByteString> iterable) {
        ensureChainIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.chain_);
    }

    public void clearChain() {
        this.chain_ = GeneratedMessageLite.emptyProtobufList();
    }

    public ByteString getSignature() {
        return this.signature_;
    }

    public void setSignature(ByteString byteString) {
        byteString.getClass();
        this.signature_ = byteString;
    }

    public void clearSignature() {
        this.signature_ = getDefaultInstance().getSignature();
    }

    public static ReportOuterClass$ReportResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ReportOuterClass$ReportResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ReportOuterClass$ReportResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ReportOuterClass$ReportResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportResponse parseFrom(InputStream inputStream) throws IOException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReportOuterClass$ReportResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ReportOuterClass$ReportResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ReportOuterClass$ReportResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ReportOuterClass$ReportResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ReportOuterClass$ReportResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ReportOuterClass$ReportResponse reportOuterClass$ReportResponse) {
        return DEFAULT_INSTANCE.createBuilder(reportOuterClass$ReportResponse);
    }

    /* loaded from: classes5.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ReportOuterClass$ReportResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ReportOuterClass$1 reportOuterClass$1) {
            this();
        }

        private Builder() {
            super(ReportOuterClass$ReportResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ReportOuterClass$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ReportOuterClass$ReportResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0002\u0000\u0001\u001b\u0002\u001c\u0003\n", new Object[]{"reports_", ReportOuterClass$Report.class, "chain_", "signature_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ReportOuterClass$ReportResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ReportOuterClass$ReportResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ReportOuterClass$ReportResponse reportOuterClass$ReportResponse = new ReportOuterClass$ReportResponse();
        DEFAULT_INSTANCE = reportOuterClass$ReportResponse;
        GeneratedMessageLite.registerDefaultInstance(ReportOuterClass$ReportResponse.class, reportOuterClass$ReportResponse);
    }

    public static ReportOuterClass$ReportResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ReportOuterClass$ReportResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
