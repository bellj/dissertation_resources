package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteFullException extends SQLiteException {
    public SQLiteFullException() {
    }

    public SQLiteFullException(String str) {
        super(str);
    }
}
