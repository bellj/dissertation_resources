package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteReadOnlyDatabaseException extends SQLiteException {
    public SQLiteReadOnlyDatabaseException() {
    }

    public SQLiteReadOnlyDatabaseException(String str) {
        super(str);
    }
}
