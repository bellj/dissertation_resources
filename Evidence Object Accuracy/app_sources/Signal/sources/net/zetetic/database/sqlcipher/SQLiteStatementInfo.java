package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public final class SQLiteStatementInfo {
    public String[] columnNames;
    public int numParameters;
    public boolean readOnly;
}
