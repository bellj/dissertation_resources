package net.zetetic.database.sqlcipher;

import android.database.CursorWindow;
import android.os.CancellationSignal;
import android.util.Log;

/* loaded from: classes3.dex */
public final class SQLiteQuery extends SQLiteProgram {
    private static final String TAG;
    private final CancellationSignal mCancellationSignal;

    public SQLiteQuery(SQLiteDatabase sQLiteDatabase, String str, CancellationSignal cancellationSignal) {
        super(sQLiteDatabase, str, null, cancellationSignal);
        this.mCancellationSignal = cancellationSignal;
    }

    public int fillWindow(CursorWindow cursorWindow, int i, int i2, boolean z) {
        acquireReference();
        try {
            cursorWindow.acquireReference();
            try {
                try {
                    int executeForCursorWindow = getSession().executeForCursorWindow(getSql(), getBindArgs(), cursorWindow, i, i2, z, getConnectionFlags(), this.mCancellationSignal);
                    cursorWindow.releaseReference();
                    return executeForCursorWindow;
                } catch (SQLiteException e) {
                    Log.e(TAG, "exception: " + e.getMessage() + "; query: " + getSql());
                    throw e;
                }
            } catch (SQLiteDatabaseCorruptException e2) {
                onCorruption();
                throw e2;
            }
        } finally {
            releaseReference();
        }
    }

    @Override // java.lang.Object
    public String toString() {
        return "SQLiteQuery: " + getSql();
    }
}
