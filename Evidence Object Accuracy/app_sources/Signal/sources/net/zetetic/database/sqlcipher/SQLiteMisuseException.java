package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteMisuseException extends SQLiteException {
    public SQLiteMisuseException() {
    }

    public SQLiteMisuseException(String str) {
        super(str);
    }
}
