package net.zetetic.database.sqlcipher;

import android.os.StatFs;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes3.dex */
public final class SQLiteGlobal {
    private static final String TAG;
    private static int sDefaultPageSize;
    private static final Object sLock = new Object();

    public static String getDefaultJournalMode() {
        return "delete";
    }

    public static String getDefaultSyncMode() {
        return "normal";
    }

    public static int getJournalSizeLimit() {
        return 10000;
    }

    public static String getWALSyncMode() {
        return "normal";
    }

    private static native int nativeReleaseMemory();

    private SQLiteGlobal() {
    }

    public static int releaseMemory() {
        return nativeReleaseMemory();
    }

    public static int getDefaultPageSize() {
        synchronized (sLock) {
            if (sDefaultPageSize == 0) {
                sDefaultPageSize = new StatFs("/data").getBlockSize();
            }
        }
        return RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT;
    }

    public static int getWALAutoCheckpoint() {
        return Math.max(1, (int) MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
    }

    public static int getWALConnectionPoolSize() {
        return Math.max(2, 10);
    }
}
