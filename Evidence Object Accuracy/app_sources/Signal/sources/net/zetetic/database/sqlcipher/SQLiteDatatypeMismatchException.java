package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteDatatypeMismatchException extends SQLiteException {
    public SQLiteDatatypeMismatchException() {
    }

    public SQLiteDatatypeMismatchException(String str) {
        super(str);
    }
}
