package net.zetetic.database.sqlcipher;

import androidx.sqlite.db.SupportSQLiteOpenHelper;

/* loaded from: classes3.dex */
public class SupportOpenHelperFactory {
    private final SQLiteDatabaseHook hook;
    private final byte[] password;

    public SupportOpenHelperFactory(byte[] bArr) {
        this(bArr, null);
    }

    public SupportOpenHelperFactory(byte[] bArr, SQLiteDatabaseHook sQLiteDatabaseHook) {
        this.password = bArr;
        this.hook = sQLiteDatabaseHook;
    }

    public SupportSQLiteOpenHelper create(SupportSQLiteOpenHelper.Configuration configuration) {
        return new SupportHelper(configuration, this.password, this.hook);
    }
}
