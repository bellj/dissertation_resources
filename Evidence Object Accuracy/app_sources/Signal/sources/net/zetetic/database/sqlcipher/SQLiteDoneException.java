package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteDoneException extends SQLiteException {
    public SQLiteDoneException() {
    }

    public SQLiteDoneException(String str) {
        super(str);
    }
}
