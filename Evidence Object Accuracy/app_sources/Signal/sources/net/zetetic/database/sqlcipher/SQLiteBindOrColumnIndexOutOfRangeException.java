package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteBindOrColumnIndexOutOfRangeException extends SQLiteException {
    public SQLiteBindOrColumnIndexOutOfRangeException() {
    }

    public SQLiteBindOrColumnIndexOutOfRangeException(String str) {
        super(str);
    }
}
