package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteCantOpenDatabaseException extends SQLiteException {
    public SQLiteCantOpenDatabaseException() {
    }

    public SQLiteCantOpenDatabaseException(String str) {
        super(str);
    }
}
