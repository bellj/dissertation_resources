package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class DatabaseObjectNotClosedException extends RuntimeException {
    private static final String s;

    public DatabaseObjectNotClosedException() {
        super(s);
    }
}
