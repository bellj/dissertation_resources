package net.zetetic.database.sqlcipher;

import android.content.Context;
import android.util.Log;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import net.zetetic.database.DatabaseErrorHandler;
import net.zetetic.database.sqlcipher.SQLiteDatabase;

/* loaded from: classes.dex */
public abstract class SQLiteOpenHelper implements SupportSQLiteOpenHelper {
    private static final boolean DEBUG_STRICT_READONLY;
    private static final String TAG = SQLiteOpenHelper.class.getSimpleName();
    private final Context mContext;
    private SQLiteDatabase mDatabase;
    private final SQLiteDatabaseHook mDatabaseHook;
    private boolean mEnableWriteAheadLogging;
    private final DatabaseErrorHandler mErrorHandler;
    private final SQLiteDatabase.CursorFactory mFactory;
    private boolean mIsInitializing;
    private final int mMinimumSupportedVersion;
    private final String mName;
    private final int mNewVersion;
    private byte[] mPassword;

    public void onBeforeDelete(SQLiteDatabase sQLiteDatabase) {
    }

    public void onConfigure(SQLiteDatabase sQLiteDatabase) {
    }

    public abstract void onCreate(SQLiteDatabase sQLiteDatabase);

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
    }

    public abstract void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2);

    public SQLiteOpenHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        this(context, str, cursorFactory, i, null);
    }

    public SQLiteOpenHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i, DatabaseErrorHandler databaseErrorHandler) {
        this(context, str, cursorFactory, i, 0, databaseErrorHandler);
    }

    public SQLiteOpenHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i, int i2, DatabaseErrorHandler databaseErrorHandler) {
        this(context, str, new byte[0], cursorFactory, i, i2, databaseErrorHandler, (SQLiteDatabaseHook) null);
    }

    public SQLiteOpenHelper(Context context, String str, String str2, SQLiteDatabase.CursorFactory cursorFactory, int i, int i2, DatabaseErrorHandler databaseErrorHandler, SQLiteDatabaseHook sQLiteDatabaseHook) {
        this(context, str, getBytes(str2), cursorFactory, i, i2, databaseErrorHandler, sQLiteDatabaseHook);
    }

    public SQLiteOpenHelper(Context context, String str, byte[] bArr, SQLiteDatabase.CursorFactory cursorFactory, int i, int i2, DatabaseErrorHandler databaseErrorHandler, SQLiteDatabaseHook sQLiteDatabaseHook) {
        if (i >= 1) {
            this.mContext = context;
            this.mName = str;
            this.mPassword = bArr;
            this.mFactory = cursorFactory;
            this.mNewVersion = i;
            this.mErrorHandler = databaseErrorHandler;
            this.mDatabaseHook = sQLiteDatabaseHook;
            this.mMinimumSupportedVersion = Math.max(0, i2);
            return;
        }
        throw new IllegalArgumentException("Version must be >= 1, was " + i);
    }

    public String getDatabaseName() {
        return this.mName;
    }

    public void setWriteAheadLoggingEnabled(boolean z) {
        synchronized (this) {
            if (this.mEnableWriteAheadLogging != z) {
                SQLiteDatabase sQLiteDatabase = this.mDatabase;
                if (sQLiteDatabase != null && sQLiteDatabase.isOpen() && !this.mDatabase.isReadOnly()) {
                    if (z) {
                        this.mDatabase.enableWriteAheadLogging();
                    } else {
                        this.mDatabase.disableWriteAheadLogging();
                    }
                }
                this.mEnableWriteAheadLogging = z;
            }
        }
    }

    public SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase databaseLocked;
        synchronized (this) {
            databaseLocked = getDatabaseLocked(true);
        }
        return databaseLocked;
    }

    public SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase databaseLocked;
        synchronized (this) {
            databaseLocked = getDatabaseLocked(false);
        }
        return databaseLocked;
    }

    private SQLiteDatabase getDatabaseLocked(boolean z) {
        SQLiteDatabase sQLiteDatabase = this.mDatabase;
        if (sQLiteDatabase != null) {
            if (!sQLiteDatabase.isOpen()) {
                this.mDatabase = null;
            } else if (!z || !this.mDatabase.isReadOnly()) {
                return this.mDatabase;
            }
        }
        if (!this.mIsInitializing) {
            SQLiteDatabase sQLiteDatabase2 = this.mDatabase;
            try {
                this.mIsInitializing = true;
                if (sQLiteDatabase2 == null) {
                    String str = this.mName;
                    if (str == null) {
                        sQLiteDatabase2 = SQLiteDatabase.create(null);
                    } else {
                        try {
                            File databasePath = this.mContext.getDatabasePath(str);
                            String path = databasePath.getPath();
                            File file = new File(databasePath.getParent());
                            if (!file.exists()) {
                                file.mkdirs();
                            }
                            sQLiteDatabase2 = SQLiteDatabase.openDatabase(path, this.mPassword, this.mFactory, this.mEnableWriteAheadLogging ? 805306368 : SQLiteDatabase.CREATE_IF_NECESSARY, this.mErrorHandler, this.mDatabaseHook);
                        } catch (SQLiteException e) {
                            if (!z) {
                                String str2 = TAG;
                                Log.e(str2, "Couldn't open " + this.mName + " for writing (will try read-only):", e);
                                sQLiteDatabase2 = SQLiteDatabase.openDatabase(this.mContext.getDatabasePath(this.mName).getPath(), this.mPassword, this.mFactory, 1, this.mErrorHandler, this.mDatabaseHook);
                            } else {
                                throw e;
                            }
                        }
                    }
                } else if (z && sQLiteDatabase2.isReadOnly()) {
                    sQLiteDatabase2.reopenReadWrite();
                }
                onConfigure(sQLiteDatabase2);
                int version = sQLiteDatabase2.getVersion();
                if (version != this.mNewVersion) {
                    if (sQLiteDatabase2.isReadOnly()) {
                        throw new SQLiteException("Can't upgrade read-only database from version " + sQLiteDatabase2.getVersion() + " to " + this.mNewVersion + ": " + this.mName);
                    } else if (version <= 0 || version >= this.mMinimumSupportedVersion) {
                        sQLiteDatabase2.beginTransaction();
                        if (version == 0) {
                            onCreate(sQLiteDatabase2);
                        } else {
                            int i = this.mNewVersion;
                            if (version > i) {
                                onDowngrade(sQLiteDatabase2, version, i);
                            } else {
                                onUpgrade(sQLiteDatabase2, version, i);
                            }
                        }
                        sQLiteDatabase2.setVersion(this.mNewVersion);
                        sQLiteDatabase2.setTransactionSuccessful();
                        sQLiteDatabase2.endTransaction();
                    } else {
                        File file2 = new File(sQLiteDatabase2.getPath());
                        onBeforeDelete(sQLiteDatabase2);
                        sQLiteDatabase2.close();
                        if (SQLiteDatabase.deleteDatabase(file2)) {
                            this.mIsInitializing = false;
                            SQLiteDatabase databaseLocked = getDatabaseLocked(z);
                            this.mIsInitializing = false;
                            if (sQLiteDatabase2 != this.mDatabase) {
                                sQLiteDatabase2.close();
                            }
                            return databaseLocked;
                        }
                        throw new IllegalStateException("Unable to delete obsolete database " + this.mName + " with version " + version);
                    }
                }
                onOpen(sQLiteDatabase2);
                if (sQLiteDatabase2.isReadOnly()) {
                    String str3 = TAG;
                    Log.w(str3, "Opened " + this.mName + " in read-only mode");
                }
                this.mDatabase = sQLiteDatabase2;
                this.mIsInitializing = false;
                return sQLiteDatabase2;
            } catch (Throwable th) {
                this.mIsInitializing = false;
                if (!(sQLiteDatabase2 == null || sQLiteDatabase2 == this.mDatabase)) {
                    sQLiteDatabase2.close();
                }
                throw th;
            }
        } else {
            throw new IllegalStateException("getDatabase called recursively");
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        if (!this.mIsInitializing) {
            SQLiteDatabase sQLiteDatabase = this.mDatabase;
            if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                this.mDatabase.close();
                this.mDatabase = null;
            }
        } else {
            throw new IllegalStateException("Closed during initialization");
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        throw new SQLiteException("Can't downgrade database from version " + i + " to " + i2);
    }

    private static byte[] getBytes(String str) {
        if (str == null || str.length() == 0) {
            return new byte[0];
        }
        ByteBuffer encode = Charset.forName("UTF-8").encode(CharBuffer.wrap(str));
        byte[] bArr = new byte[encode.limit()];
        encode.get(bArr);
        return bArr;
    }
}
