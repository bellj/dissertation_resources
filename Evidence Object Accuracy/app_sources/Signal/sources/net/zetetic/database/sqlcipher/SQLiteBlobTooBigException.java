package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteBlobTooBigException extends SQLiteException {
    public SQLiteBlobTooBigException() {
    }

    public SQLiteBlobTooBigException(String str) {
        super(str);
    }
}
