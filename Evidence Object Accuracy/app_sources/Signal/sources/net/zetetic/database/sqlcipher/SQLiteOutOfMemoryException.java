package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteOutOfMemoryException extends SQLiteException {
    public SQLiteOutOfMemoryException() {
    }

    public SQLiteOutOfMemoryException(String str) {
        super(str);
    }
}
