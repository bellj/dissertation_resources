package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteTableLockedException extends SQLiteException {
    public SQLiteTableLockedException() {
    }

    public SQLiteTableLockedException(String str) {
        super(str);
    }
}
