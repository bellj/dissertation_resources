package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteDatabaseLockedException extends SQLiteException {
    public SQLiteDatabaseLockedException() {
    }

    public SQLiteDatabaseLockedException(String str) {
        super(str);
    }
}
