package net.zetetic.database.sqlcipher;

/* loaded from: classes3.dex */
public class SQLiteDiskIOException extends SQLiteException {
    public SQLiteDiskIOException() {
    }

    public SQLiteDiskIOException(String str) {
        super(str);
    }
}
