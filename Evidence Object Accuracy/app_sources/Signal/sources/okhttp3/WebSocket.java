package okhttp3;

import okio.ByteString;

/* loaded from: classes3.dex */
public interface WebSocket {
    boolean close(int i, String str);

    boolean send(ByteString byteString);
}
