package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/* loaded from: classes.dex */
public class LinkAddress implements Parcelable {
    public static final Parcelable.Creator<LinkAddress> CREATOR = new Parcelable.Creator<LinkAddress>() { // from class: android.net.LinkAddress.1
        @Override // android.os.Parcelable.Creator
        public LinkAddress createFromParcel(Parcel parcel) {
            InetAddress inetAddress = null;
            int i = 0;
            if (parcel.readByte() == 1) {
                try {
                    inetAddress = InetAddress.getByAddress(parcel.createByteArray());
                    i = parcel.readInt();
                } catch (UnknownHostException unused) {
                }
            }
            return new LinkAddress(inetAddress, i);
        }

        @Override // android.os.Parcelable.Creator
        public LinkAddress[] newArray(int i) {
            return new LinkAddress[i];
        }
    };
    private final InetAddress address;
    private final int prefixLength;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public LinkAddress(InetAddress inetAddress, int i) {
        if (inetAddress == null || i < 0 || (((inetAddress instanceof Inet4Address) && i > 32) || i > 128)) {
            throw new IllegalArgumentException("Bad LinkAddress haloParams " + inetAddress + i);
        }
        this.address = inetAddress;
        this.prefixLength = i;
    }

    @Override // java.lang.Object
    public String toString() {
        if (this.address == null) {
            return "";
        }
        return this.address.getHostAddress() + "/" + this.prefixLength;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof LinkAddress)) {
            return false;
        }
        LinkAddress linkAddress = (LinkAddress) obj;
        if (!this.address.equals(linkAddress.address) || this.prefixLength != linkAddress.prefixLength) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        InetAddress inetAddress = this.address;
        return (inetAddress == null ? 0 : inetAddress.hashCode()) + this.prefixLength;
    }

    public InetAddress getAddress() {
        return this.address;
    }

    public int getNetworkPrefixLength() {
        return this.prefixLength;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        if (this.address != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByteArray(this.address.getAddress());
            parcel.writeInt(this.prefixLength);
            return;
        }
        parcel.writeByte((byte) 0);
    }
}
