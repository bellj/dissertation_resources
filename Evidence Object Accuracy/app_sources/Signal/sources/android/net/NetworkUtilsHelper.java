package android.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

/* loaded from: classes.dex */
public class NetworkUtilsHelper {
    public static InetAddress getNetworkPart(InetAddress inetAddress, int i) {
        if (inetAddress != null) {
            byte[] address = inetAddress.getAddress();
            if (i < 0 || i > address.length * 8) {
                throw new RuntimeException("getNetworkPart - bad prefixLength");
            }
            int i2 = i / 8;
            byte b = (byte) (255 << (8 - (i % 8)));
            if (i2 < address.length) {
                address[i2] = (byte) (b & address[i2]);
            }
            while (true) {
                i2++;
                if (i2 < address.length) {
                    address[i2] = 0;
                } else {
                    try {
                        return InetAddress.getByAddress(address);
                    } catch (UnknownHostException e) {
                        throw new RuntimeException("getNetworkPart error - " + e.toString());
                    }
                }
            }
        } else {
            throw new RuntimeException("getNetworkPart doesn't accept null address");
        }
    }
}
