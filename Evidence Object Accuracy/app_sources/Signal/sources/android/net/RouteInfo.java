package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import com.klinker.android.logger.Log;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/* loaded from: classes.dex */
public class RouteInfo implements Parcelable {
    public static final Parcelable.Creator<RouteInfo> CREATOR = new Parcelable.Creator<RouteInfo>() { // from class: android.net.RouteInfo.1
        /* JADX WARNING: Removed duplicated region for block: B:11:0x001f  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x002c  */
        @Override // android.os.Parcelable.Creator
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.net.RouteInfo createFromParcel(android.os.Parcel r6) {
            /*
                r5 = this;
                byte r0 = r6.readByte()
                r1 = 1
                r2 = 0
                if (r0 != r1) goto L_0x0017
                byte[] r0 = r6.createByteArray()
                int r3 = r6.readInt()
                java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r0)     // Catch: UnknownHostException -> 0x0015
                goto L_0x0019
            L_0x0015:
                goto L_0x0018
            L_0x0017:
                r3 = 0
            L_0x0018:
                r0 = r2
            L_0x0019:
                byte r4 = r6.readByte()
                if (r4 != r1) goto L_0x0029
                byte[] r6 = r6.createByteArray()
                java.net.InetAddress r6 = java.net.InetAddress.getByAddress(r6)     // Catch: UnknownHostException -> 0x0028
                goto L_0x002a
            L_0x0028:
            L_0x0029:
                r6 = r2
            L_0x002a:
                if (r0 == 0) goto L_0x0031
                android.net.LinkAddress r2 = new android.net.LinkAddress
                r2.<init>(r0, r3)
            L_0x0031:
                android.net.RouteInfo r0 = new android.net.RouteInfo
                r0.<init>(r2, r6)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: android.net.RouteInfo.AnonymousClass1.createFromParcel(android.os.Parcel):android.net.RouteInfo");
        }

        @Override // android.os.Parcelable.Creator
        public RouteInfo[] newArray(int i) {
            return new RouteInfo[i];
        }
    };
    private final LinkAddress mDestination;
    private final InetAddress mGateway;
    private final boolean mIsDefault;
    private final boolean mIsHost;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public RouteInfo(LinkAddress linkAddress, InetAddress inetAddress) {
        LinkAddress linkAddress2;
        if (linkAddress == null) {
            if (inetAddress != null) {
                if (inetAddress instanceof Inet4Address) {
                    try {
                        linkAddress2 = new LinkAddress(Inet4Address.getLocalHost(), 0);
                    } catch (UnknownHostException e) {
                        Log.e("RouteInfo", "exception thrown", e);
                    }
                } else {
                    try {
                        linkAddress2 = new LinkAddress(Inet6Address.getLocalHost(), 0);
                    } catch (UnknownHostException e2) {
                        Log.e("RouteInfo", "exception thrown", e2);
                    }
                }
                linkAddress = linkAddress2;
            } else {
                throw new RuntimeException("Invalid arguments passed in.");
            }
        }
        if (inetAddress == null) {
            if (linkAddress.getAddress() instanceof Inet4Address) {
                try {
                    inetAddress = Inet4Address.getLocalHost();
                } catch (UnknownHostException e3) {
                    Log.e("RouteInfo", "exception thrown", e3);
                }
            } else {
                try {
                    inetAddress = Inet6Address.getLocalHost();
                } catch (UnknownHostException e4) {
                    Log.e("RouteInfo", "exception thrown", e4);
                }
            }
        }
        this.mDestination = new LinkAddress(NetworkUtilsHelper.getNetworkPart(linkAddress.getAddress(), linkAddress.getNetworkPrefixLength()), linkAddress.getNetworkPrefixLength());
        this.mGateway = inetAddress;
        this.mIsDefault = isDefault();
        this.mIsHost = isHost();
    }

    private boolean isHost() {
        try {
            if (!this.mGateway.equals(Inet4Address.getLocalHost())) {
                if (!this.mGateway.equals(Inet6Address.getLocalHost())) {
                    return false;
                }
            }
            return true;
        } catch (UnknownHostException unused) {
            return false;
        }
    }

    private boolean isDefault() {
        InetAddress inetAddress = this.mGateway;
        if (inetAddress == null) {
            return false;
        }
        if (inetAddress instanceof Inet4Address) {
            LinkAddress linkAddress = this.mDestination;
            if (linkAddress != null && linkAddress.getNetworkPrefixLength() != 0) {
                return false;
            }
        } else {
            LinkAddress linkAddress2 = this.mDestination;
            if (linkAddress2 != null && linkAddress2.getNetworkPrefixLength() != 0) {
                return false;
            }
        }
        return true;
    }

    public LinkAddress getDestination() {
        return this.mDestination;
    }

    public InetAddress getGateway() {
        return this.mGateway;
    }

    @Override // java.lang.Object
    public String toString() {
        LinkAddress linkAddress = this.mDestination;
        String linkAddress2 = linkAddress != null ? linkAddress.toString() : "";
        if (this.mGateway == null) {
            return linkAddress2;
        }
        return linkAddress2 + " -> " + this.mGateway.getHostAddress();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        if (this.mDestination == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeByteArray(this.mDestination.getAddress().getAddress());
            parcel.writeInt(this.mDestination.getNetworkPrefixLength());
        }
        if (this.mGateway == null) {
            parcel.writeByte((byte) 0);
            return;
        }
        parcel.writeByte((byte) 1);
        parcel.writeByteArray(this.mGateway.getAddress());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RouteInfo)) {
            return false;
        }
        RouteInfo routeInfo = (RouteInfo) obj;
        LinkAddress linkAddress = this.mDestination;
        if (linkAddress == null) {
            z = routeInfo.getDestination() == null;
        } else {
            z = linkAddress.equals(routeInfo.getDestination());
        }
        InetAddress inetAddress = this.mGateway;
        if (inetAddress == null) {
            z2 = routeInfo.getGateway() == null;
        } else {
            z2 = inetAddress.equals(routeInfo.getGateway());
        }
        if (!z || !z2 || this.mIsDefault != routeInfo.mIsDefault) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        LinkAddress linkAddress = this.mDestination;
        int i = 0;
        int hashCode = linkAddress == null ? 0 : linkAddress.hashCode();
        InetAddress inetAddress = this.mGateway;
        if (inetAddress != null) {
            i = inetAddress.hashCode();
        }
        return hashCode + i + (this.mIsDefault ? 3 : 7);
    }
}
