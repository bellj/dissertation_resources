package android.net;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes.dex */
public class ProxyProperties implements Parcelable {
    public static final Parcelable.Creator<ProxyProperties> CREATOR = new Parcelable.Creator<ProxyProperties>() { // from class: android.net.ProxyProperties.1
        @Override // android.os.Parcelable.Creator
        public ProxyProperties createFromParcel(Parcel parcel) {
            int i;
            String str;
            if (parcel.readByte() == 1) {
                String readString = parcel.readString();
                i = parcel.readInt();
                str = readString;
            } else {
                str = null;
                i = 0;
            }
            return new ProxyProperties(str, i, parcel.readString(), null);
        }

        @Override // android.os.Parcelable.Creator
        public ProxyProperties[] newArray(int i) {
            return new ProxyProperties[i];
        }
    };
    private String mExclusionList;
    private String mHost;
    private String[] mParsedExclusionList;
    private int mPort;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private ProxyProperties(String str, int i, String str2, String[] strArr) {
        this.mHost = str;
        this.mPort = i;
        this.mExclusionList = str2;
        this.mParsedExclusionList = strArr;
    }

    public String getHost() {
        return this.mHost;
    }

    public String getExclusionList() {
        return this.mExclusionList;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.mHost != null) {
            sb.append("[");
            sb.append(this.mHost);
            sb.append("] ");
            sb.append(Integer.toString(this.mPort));
            if (this.mExclusionList != null) {
                sb.append(" xl=");
                sb.append(this.mExclusionList);
            }
        } else {
            sb.append("[ProxyProperties.mHost == null]");
        }
        return sb.toString();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof ProxyProperties)) {
            return false;
        }
        ProxyProperties proxyProperties = (ProxyProperties) obj;
        String str = this.mExclusionList;
        if (str != null && !str.equals(proxyProperties.getExclusionList())) {
            return false;
        }
        if (this.mHost != null && proxyProperties.getHost() != null && !this.mHost.equals(proxyProperties.getHost())) {
            return false;
        }
        String str2 = this.mHost;
        if (str2 != null && proxyProperties.mHost == null) {
            return false;
        }
        if ((str2 != null || proxyProperties.mHost == null) && this.mPort == proxyProperties.mPort) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        String str = this.mHost;
        int i = 0;
        int hashCode = str == null ? 0 : str.hashCode();
        String str2 = this.mExclusionList;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i + this.mPort;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        if (this.mHost != null) {
            parcel.writeByte((byte) 1);
            parcel.writeString(this.mHost);
            parcel.writeInt(this.mPort);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeString(this.mExclusionList);
        parcel.writeStringArray(this.mParsedExclusionList);
    }
}
