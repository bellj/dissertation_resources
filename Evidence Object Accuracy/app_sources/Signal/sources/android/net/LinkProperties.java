package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/* loaded from: classes.dex */
public class LinkProperties implements Parcelable {
    public static final Parcelable.Creator<LinkProperties> CREATOR = new Parcelable.Creator<LinkProperties>() { // from class: android.net.LinkProperties.1
        @Override // android.os.Parcelable.Creator
        public LinkProperties createFromParcel(Parcel parcel) {
            LinkProperties linkProperties = new LinkProperties();
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    linkProperties.setInterfaceName(readString);
                } catch (Exception unused) {
                    return null;
                }
            }
            int readInt = parcel.readInt();
            for (int i = 0; i < readInt; i++) {
                linkProperties.addLinkAddress((LinkAddress) parcel.readParcelable(null));
            }
            int readInt2 = parcel.readInt();
            for (int i2 = 0; i2 < readInt2; i2++) {
                try {
                    linkProperties.addDns(InetAddress.getByAddress(parcel.createByteArray()));
                } catch (UnknownHostException unused2) {
                }
            }
            int readInt3 = parcel.readInt();
            for (int i3 = 0; i3 < readInt3; i3++) {
                linkProperties.addRoute((RouteInfo) parcel.readParcelable(null));
            }
            if (parcel.readByte() == 1) {
                linkProperties.setHttpProxy((ProxyProperties) parcel.readParcelable(null));
            }
            return linkProperties;
        }

        @Override // android.os.Parcelable.Creator
        public LinkProperties[] newArray(int i) {
            return new LinkProperties[i];
        }
    };
    private Collection<InetAddress> mDnses = new ArrayList();
    private ProxyProperties mHttpProxy;
    String mIfaceName;
    private Collection<LinkAddress> mLinkAddresses = new ArrayList();
    private Collection<RouteInfo> mRoutes = new ArrayList();

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public LinkProperties() {
        clear();
    }

    public void setInterfaceName(String str) {
        this.mIfaceName = str;
    }

    public String getInterfaceName() {
        return this.mIfaceName;
    }

    public Collection<InetAddress> getAddresses() {
        ArrayList arrayList = new ArrayList();
        for (LinkAddress linkAddress : this.mLinkAddresses) {
            arrayList.add(linkAddress.getAddress());
        }
        return Collections.unmodifiableCollection(arrayList);
    }

    public void addLinkAddress(LinkAddress linkAddress) {
        if (linkAddress != null) {
            this.mLinkAddresses.add(linkAddress);
        }
    }

    public void addDns(InetAddress inetAddress) {
        if (inetAddress != null) {
            this.mDnses.add(inetAddress);
        }
    }

    public Collection<InetAddress> getDnses() {
        return Collections.unmodifiableCollection(this.mDnses);
    }

    public void addRoute(RouteInfo routeInfo) {
        if (routeInfo != null) {
            this.mRoutes.add(routeInfo);
        }
    }

    public Collection<RouteInfo> getRoutes() {
        return Collections.unmodifiableCollection(this.mRoutes);
    }

    public void setHttpProxy(ProxyProperties proxyProperties) {
        this.mHttpProxy = proxyProperties;
    }

    public ProxyProperties getHttpProxy() {
        return this.mHttpProxy;
    }

    public void clear() {
        this.mIfaceName = null;
        this.mLinkAddresses.clear();
        this.mDnses.clear();
        this.mRoutes.clear();
        this.mHttpProxy = null;
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        String str2 = "";
        if (this.mIfaceName == null) {
            str = str2;
        } else {
            str = "InterfaceName: " + this.mIfaceName + " ";
        }
        Iterator<LinkAddress> it = this.mLinkAddresses.iterator();
        String str3 = "LinkAddresses: [";
        while (it.hasNext()) {
            str3 = str3 + it.next().toString() + ",";
        }
        String str4 = str3 + "] ";
        Iterator<InetAddress> it2 = this.mDnses.iterator();
        String str5 = "DnsAddresses: [";
        while (it2.hasNext()) {
            str5 = str5 + it2.next().getHostAddress() + ",";
        }
        String str6 = str5 + "] ";
        Iterator<RouteInfo> it3 = this.mRoutes.iterator();
        String str7 = "Routes: [";
        while (it3.hasNext()) {
            str7 = str7 + it3.next().toString() + ",";
        }
        String str8 = str7 + "] ";
        if (this.mHttpProxy != null) {
            str2 = "HttpProxy: " + this.mHttpProxy.toString() + " ";
        }
        return str + str4 + str8 + str6 + str2;
    }

    public boolean isIdenticalInterfaceName(LinkProperties linkProperties) {
        return TextUtils.equals(getInterfaceName(), linkProperties.getInterfaceName());
    }

    public boolean isIdenticalAddresses(LinkProperties linkProperties) {
        Collection<InetAddress> addresses = linkProperties.getAddresses();
        Collection<InetAddress> addresses2 = getAddresses();
        if (addresses2.size() == addresses.size()) {
            return addresses2.containsAll(addresses);
        }
        return false;
    }

    public boolean isIdenticalDnses(LinkProperties linkProperties) {
        Collection<InetAddress> dnses = linkProperties.getDnses();
        if (this.mDnses.size() == dnses.size()) {
            return this.mDnses.containsAll(dnses);
        }
        return false;
    }

    public boolean isIdenticalRoutes(LinkProperties linkProperties) {
        Collection<RouteInfo> routes = linkProperties.getRoutes();
        if (this.mRoutes.size() == routes.size()) {
            return this.mRoutes.containsAll(routes);
        }
        return false;
    }

    public boolean isIdenticalHttpProxy(LinkProperties linkProperties) {
        if (getHttpProxy() == null) {
            return linkProperties.getHttpProxy() == null;
        }
        return getHttpProxy().equals(linkProperties.getHttpProxy());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LinkProperties)) {
            return false;
        }
        LinkProperties linkProperties = (LinkProperties) obj;
        if (!isIdenticalInterfaceName(linkProperties) || !isIdenticalAddresses(linkProperties) || !isIdenticalDnses(linkProperties) || !isIdenticalRoutes(linkProperties) || !isIdenticalHttpProxy(linkProperties)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        String str = this.mIfaceName;
        int i = 0;
        if (str == null) {
            return 0;
        }
        int hashCode = str.hashCode() + (this.mLinkAddresses.size() * 31) + (this.mDnses.size() * 37) + (this.mRoutes.size() * 41);
        ProxyProperties proxyProperties = this.mHttpProxy;
        if (proxyProperties != null) {
            i = proxyProperties.hashCode();
        }
        return i + hashCode;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getInterfaceName());
        parcel.writeInt(this.mLinkAddresses.size());
        for (LinkAddress linkAddress : this.mLinkAddresses) {
            parcel.writeParcelable(linkAddress, i);
        }
        parcel.writeInt(this.mDnses.size());
        for (InetAddress inetAddress : this.mDnses) {
            parcel.writeByteArray(inetAddress.getAddress());
        }
        parcel.writeInt(this.mRoutes.size());
        for (RouteInfo routeInfo : this.mRoutes) {
            parcel.writeParcelable(routeInfo, i);
        }
        if (this.mHttpProxy != null) {
            parcel.writeByte((byte) 1);
            parcel.writeParcelable(this.mHttpProxy, i);
            return;
        }
        parcel.writeByte((byte) 0);
    }
}
