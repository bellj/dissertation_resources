package account_ingest;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Ingest$ReportMissedBlockRangeRequest extends GeneratedMessageLite<Ingest$ReportMissedBlockRangeRequest, Builder> implements MessageLiteOrBuilder {
    private static final Ingest$ReportMissedBlockRangeRequest DEFAULT_INSTANCE;
    public static final int END_INDEX_FIELD_NUMBER;
    private static volatile Parser<Ingest$ReportMissedBlockRangeRequest> PARSER;
    public static final int START_INDEX_FIELD_NUMBER;
    private long endIndex_;
    private long startIndex_;

    private Ingest$ReportMissedBlockRangeRequest() {
    }

    public long getStartIndex() {
        return this.startIndex_;
    }

    public void setStartIndex(long j) {
        this.startIndex_ = j;
    }

    public void clearStartIndex() {
        this.startIndex_ = 0;
    }

    public long getEndIndex() {
        return this.endIndex_;
    }

    public void setEndIndex(long j) {
        this.endIndex_ = j;
    }

    public void clearEndIndex() {
        this.endIndex_ = 0;
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(InputStream inputStream) throws IOException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ingest$ReportMissedBlockRangeRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$ReportMissedBlockRangeRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ingest$ReportMissedBlockRangeRequest ingest$ReportMissedBlockRangeRequest) {
        return DEFAULT_INSTANCE.createBuilder(ingest$ReportMissedBlockRangeRequest);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ingest$ReportMissedBlockRangeRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ingest$1 ingest$1) {
            this();
        }

        private Builder() {
            super(Ingest$ReportMissedBlockRangeRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ingest$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ingest$ReportMissedBlockRangeRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0003\u0002\u0003", new Object[]{"startIndex_", "endIndex_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ingest$ReportMissedBlockRangeRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Ingest$ReportMissedBlockRangeRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ingest$ReportMissedBlockRangeRequest ingest$ReportMissedBlockRangeRequest = new Ingest$ReportMissedBlockRangeRequest();
        DEFAULT_INSTANCE = ingest$ReportMissedBlockRangeRequest;
        GeneratedMessageLite.registerDefaultInstance(Ingest$ReportMissedBlockRangeRequest.class, ingest$ReportMissedBlockRangeRequest);
    }

    public static Ingest$ReportMissedBlockRangeRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ingest$ReportMissedBlockRangeRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
