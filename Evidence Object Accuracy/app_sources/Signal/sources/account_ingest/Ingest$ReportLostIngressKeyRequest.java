package account_ingest;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Ingest$ReportLostIngressKeyRequest extends GeneratedMessageLite<Ingest$ReportLostIngressKeyRequest, Builder> implements MessageLiteOrBuilder {
    private static final Ingest$ReportLostIngressKeyRequest DEFAULT_INSTANCE;
    public static final int KEY_FIELD_NUMBER;
    private static volatile Parser<Ingest$ReportLostIngressKeyRequest> PARSER;
    private MobileCoinAPI$CompressedRistretto key_;

    private Ingest$ReportLostIngressKeyRequest() {
    }

    public boolean hasKey() {
        return this.key_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.key_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.key_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.key_ = builder.build();
    }

    public void mergeKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.key_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.key_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.key_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.key_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearKey() {
        this.key_ = null;
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(InputStream inputStream) throws IOException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$ReportLostIngressKeyRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$ReportLostIngressKeyRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ingest$ReportLostIngressKeyRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$ReportLostIngressKeyRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ingest$ReportLostIngressKeyRequest ingest$ReportLostIngressKeyRequest) {
        return DEFAULT_INSTANCE.createBuilder(ingest$ReportLostIngressKeyRequest);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ingest$ReportLostIngressKeyRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ingest$1 ingest$1) {
            this();
        }

        private Builder() {
            super(Ingest$ReportLostIngressKeyRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ingest$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ingest$ReportLostIngressKeyRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"key_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ingest$ReportLostIngressKeyRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Ingest$ReportLostIngressKeyRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ingest$ReportLostIngressKeyRequest ingest$ReportLostIngressKeyRequest = new Ingest$ReportLostIngressKeyRequest();
        DEFAULT_INSTANCE = ingest$ReportLostIngressKeyRequest;
        GeneratedMessageLite.registerDefaultInstance(Ingest$ReportLostIngressKeyRequest.class, ingest$ReportLostIngressKeyRequest);
    }

    public static Ingest$ReportLostIngressKeyRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ingest$ReportLostIngressKeyRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
