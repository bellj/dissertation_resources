package account_ingest;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Ingest$SetPubkeyExpiryWindowRequest extends GeneratedMessageLite<Ingest$SetPubkeyExpiryWindowRequest, Builder> implements MessageLiteOrBuilder {
    private static final Ingest$SetPubkeyExpiryWindowRequest DEFAULT_INSTANCE;
    private static volatile Parser<Ingest$SetPubkeyExpiryWindowRequest> PARSER = null;
    public static final int PUBKEY_EXPIRY_WINDOW_FIELD_NUMBER = 1;
    private long pubkeyExpiryWindow_;

    private Ingest$SetPubkeyExpiryWindowRequest() {
    }

    public long getPubkeyExpiryWindow() {
        return this.pubkeyExpiryWindow_;
    }

    /* access modifiers changed from: private */
    public void setPubkeyExpiryWindow(long j) {
        this.pubkeyExpiryWindow_ = j;
    }

    /* access modifiers changed from: private */
    public void clearPubkeyExpiryWindow() {
        this.pubkeyExpiryWindow_ = 0;
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(InputStream inputStream) throws IOException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$SetPubkeyExpiryWindowRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ingest$SetPubkeyExpiryWindowRequest ingest$SetPubkeyExpiryWindowRequest) {
        return DEFAULT_INSTANCE.createBuilder(ingest$SetPubkeyExpiryWindowRequest);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ingest$SetPubkeyExpiryWindowRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ingest$1 ingest$1) {
            this();
        }

        private Builder() {
            super(Ingest$SetPubkeyExpiryWindowRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ingest$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ingest$SetPubkeyExpiryWindowRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u0003", new Object[]{"pubkeyExpiryWindow_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ingest$SetPubkeyExpiryWindowRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Ingest$SetPubkeyExpiryWindowRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ingest$SetPubkeyExpiryWindowRequest ingest$SetPubkeyExpiryWindowRequest = new Ingest$SetPubkeyExpiryWindowRequest();
        DEFAULT_INSTANCE = ingest$SetPubkeyExpiryWindowRequest;
        GeneratedMessageLite.registerDefaultInstance(Ingest$SetPubkeyExpiryWindowRequest.class, ingest$SetPubkeyExpiryWindowRequest);
    }

    public static Ingest$SetPubkeyExpiryWindowRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ingest$SetPubkeyExpiryWindowRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
