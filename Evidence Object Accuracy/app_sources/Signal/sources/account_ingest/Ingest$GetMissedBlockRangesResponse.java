package account_ingest;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import fog_common.FogCommon$BlockRange;
import fog_common.FogCommon$BlockRangeOrBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes.dex */
public final class Ingest$GetMissedBlockRangesResponse extends GeneratedMessageLite<Ingest$GetMissedBlockRangesResponse, Builder> implements MessageLiteOrBuilder {
    private static final Ingest$GetMissedBlockRangesResponse DEFAULT_INSTANCE;
    public static final int MISSED_BLOCK_RANGES_FIELD_NUMBER;
    private static volatile Parser<Ingest$GetMissedBlockRangesResponse> PARSER;
    private Internal.ProtobufList<FogCommon$BlockRange> missedBlockRanges_ = GeneratedMessageLite.emptyProtobufList();

    private Ingest$GetMissedBlockRangesResponse() {
    }

    public List<FogCommon$BlockRange> getMissedBlockRangesList() {
        return this.missedBlockRanges_;
    }

    public List<? extends FogCommon$BlockRangeOrBuilder> getMissedBlockRangesOrBuilderList() {
        return this.missedBlockRanges_;
    }

    public int getMissedBlockRangesCount() {
        return this.missedBlockRanges_.size();
    }

    public FogCommon$BlockRange getMissedBlockRanges(int i) {
        return this.missedBlockRanges_.get(i);
    }

    public FogCommon$BlockRangeOrBuilder getMissedBlockRangesOrBuilder(int i) {
        return this.missedBlockRanges_.get(i);
    }

    private void ensureMissedBlockRangesIsMutable() {
        if (!this.missedBlockRanges_.isModifiable()) {
            this.missedBlockRanges_ = GeneratedMessageLite.mutableCopy(this.missedBlockRanges_);
        }
    }

    public void setMissedBlockRanges(int i, FogCommon$BlockRange fogCommon$BlockRange) {
        fogCommon$BlockRange.getClass();
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.set(i, fogCommon$BlockRange);
    }

    public void setMissedBlockRanges(int i, FogCommon$BlockRange.Builder builder) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.set(i, builder.build());
    }

    public void addMissedBlockRanges(FogCommon$BlockRange fogCommon$BlockRange) {
        fogCommon$BlockRange.getClass();
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(fogCommon$BlockRange);
    }

    public void addMissedBlockRanges(int i, FogCommon$BlockRange fogCommon$BlockRange) {
        fogCommon$BlockRange.getClass();
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(i, fogCommon$BlockRange);
    }

    public void addMissedBlockRanges(FogCommon$BlockRange.Builder builder) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(builder.build());
    }

    public void addMissedBlockRanges(int i, FogCommon$BlockRange.Builder builder) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(i, builder.build());
    }

    public void addAllMissedBlockRanges(Iterable<? extends FogCommon$BlockRange> iterable) {
        ensureMissedBlockRangesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.missedBlockRanges_);
    }

    public void clearMissedBlockRanges() {
        this.missedBlockRanges_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeMissedBlockRanges(int i) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.remove(i);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(InputStream inputStream) throws IOException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$GetMissedBlockRangesResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ingest$GetMissedBlockRangesResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ingest$GetMissedBlockRangesResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ingest$GetMissedBlockRangesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ingest$GetMissedBlockRangesResponse ingest$GetMissedBlockRangesResponse) {
        return DEFAULT_INSTANCE.createBuilder(ingest$GetMissedBlockRangesResponse);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ingest$GetMissedBlockRangesResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ingest$1 ingest$1) {
            this();
        }

        private Builder() {
            super(Ingest$GetMissedBlockRangesResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ingest$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ingest$GetMissedBlockRangesResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"missedBlockRanges_", FogCommon$BlockRange.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ingest$GetMissedBlockRangesResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (Ingest$GetMissedBlockRangesResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ingest$GetMissedBlockRangesResponse ingest$GetMissedBlockRangesResponse = new Ingest$GetMissedBlockRangesResponse();
        DEFAULT_INSTANCE = ingest$GetMissedBlockRangesResponse;
        GeneratedMessageLite.registerDefaultInstance(Ingest$GetMissedBlockRangesResponse.class, ingest$GetMissedBlockRangesResponse);
    }

    public static Ingest$GetMissedBlockRangesResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ingest$GetMissedBlockRangesResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
