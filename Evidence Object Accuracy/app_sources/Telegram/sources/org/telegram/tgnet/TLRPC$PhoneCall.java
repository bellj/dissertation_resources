package org.telegram.tgnet;

import java.util.ArrayList;

/* loaded from: classes.dex */
public abstract class TLRPC$PhoneCall extends TLObject {
    public long access_hash;
    public long admin_id;
    public ArrayList<TLRPC$PhoneConnection> connections = new ArrayList<>();
    public int date;
    public int duration;
    public int flags;
    public byte[] g_a_hash;
    public byte[] g_a_or_b;
    public byte[] g_b;
    public long id;
    public long key_fingerprint;
    public boolean need_debug;
    public boolean need_rating;
    public boolean p2p_allowed;
    public long participant_id;
    public TLRPC$PhoneCallProtocol protocol;
    public TLRPC$PhoneCallDiscardReason reason;
    public int receive_date;
    public int start_date;
    public boolean video;

    public static TLRPC$PhoneCall TLdeserialize(AbstractSerializedData abstractSerializedData, int i, boolean z) {
        TLRPC$PhoneCall tLRPC$PhoneCall;
        switch (i) {
            case -1770029977:
                tLRPC$PhoneCall = new TLRPC$TL_phoneCall();
                break;
            case -987599081:
                tLRPC$PhoneCall = new TLRPC$PhoneCall() { // from class: org.telegram.tgnet.TLRPC$TL_phoneCallWaiting
                    public static int constructor = -987599081;

                    @Override // org.telegram.tgnet.TLObject
                    public void readParams(AbstractSerializedData abstractSerializedData2, boolean z2) {
                        int readInt32 = abstractSerializedData2.readInt32(z2);
                        this.flags = readInt32;
                        this.video = (readInt32 & 64) != 0;
                        this.id = abstractSerializedData2.readInt64(z2);
                        this.access_hash = abstractSerializedData2.readInt64(z2);
                        this.date = abstractSerializedData2.readInt32(z2);
                        this.admin_id = abstractSerializedData2.readInt64(z2);
                        this.participant_id = abstractSerializedData2.readInt64(z2);
                        this.protocol = TLRPC$PhoneCallProtocol.TLdeserialize(abstractSerializedData2, abstractSerializedData2.readInt32(z2), z2);
                        if ((this.flags & 1) != 0) {
                            this.receive_date = abstractSerializedData2.readInt32(z2);
                        }
                    }

                    @Override // org.telegram.tgnet.TLObject
                    public void serializeToStream(AbstractSerializedData abstractSerializedData2) {
                        abstractSerializedData2.writeInt32(constructor);
                        int i2 = this.video ? this.flags | 64 : this.flags & -65;
                        this.flags = i2;
                        abstractSerializedData2.writeInt32(i2);
                        abstractSerializedData2.writeInt64(this.id);
                        abstractSerializedData2.writeInt64(this.access_hash);
                        abstractSerializedData2.writeInt32(this.date);
                        abstractSerializedData2.writeInt64(this.admin_id);
                        abstractSerializedData2.writeInt64(this.participant_id);
                        this.protocol.serializeToStream(abstractSerializedData2);
                        if ((this.flags & 1) != 0) {
                            abstractSerializedData2.writeInt32(this.receive_date);
                        }
                    }
                };
                break;
            case 347139340:
                tLRPC$PhoneCall = new TLRPC$TL_phoneCallRequested();
                break;
            case 912311057:
                tLRPC$PhoneCall = new TLRPC$TL_phoneCallAccepted();
                break;
            case 1355435489:
                tLRPC$PhoneCall = new TLRPC$TL_phoneCallDiscarded();
                break;
            case 1399245077:
                tLRPC$PhoneCall = new TLRPC$PhoneCall() { // from class: org.telegram.tgnet.TLRPC$TL_phoneCallEmpty
                    public static int constructor = 1399245077;

                    @Override // org.telegram.tgnet.TLObject
                    public void readParams(AbstractSerializedData abstractSerializedData2, boolean z2) {
                        this.id = abstractSerializedData2.readInt64(z2);
                    }

                    @Override // org.telegram.tgnet.TLObject
                    public void serializeToStream(AbstractSerializedData abstractSerializedData2) {
                        abstractSerializedData2.writeInt32(constructor);
                        abstractSerializedData2.writeInt64(this.id);
                    }
                };
                break;
            default:
                tLRPC$PhoneCall = null;
                break;
        }
        if (tLRPC$PhoneCall != null || !z) {
            if (tLRPC$PhoneCall != null) {
                tLRPC$PhoneCall.readParams(abstractSerializedData, z);
            }
            return tLRPC$PhoneCall;
        }
        throw new RuntimeException(String.format("can't parse magic %x in PhoneCall", Integer.valueOf(i)));
    }
}
