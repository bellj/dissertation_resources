package org.telegram.tgnet;

/* loaded from: classes.dex */
public class TLRPC$TL_privacyKeyPhoneNumber extends TLRPC$PrivacyKey {
    public static int constructor = -778378131;

    @Override // org.telegram.tgnet.TLObject
    public void serializeToStream(AbstractSerializedData abstractSerializedData) {
        abstractSerializedData.writeInt32(constructor);
    }
}
