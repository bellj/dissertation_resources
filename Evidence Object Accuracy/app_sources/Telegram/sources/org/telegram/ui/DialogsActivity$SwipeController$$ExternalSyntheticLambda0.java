package org.telegram.ui;

import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class DialogsActivity$SwipeController$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ View f$0;

    public /* synthetic */ DialogsActivity$SwipeController$$ExternalSyntheticLambda0(View view) {
        this.f$0 = view;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.setBackgroundDrawable(null);
    }
}
