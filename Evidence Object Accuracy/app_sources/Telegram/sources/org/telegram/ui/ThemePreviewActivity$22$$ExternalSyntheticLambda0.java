package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.ThemePreviewActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemePreviewActivity$22$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ThemePreviewActivity.AnonymousClass22 f$0;

    public /* synthetic */ ThemePreviewActivity$22$$ExternalSyntheticLambda0(ThemePreviewActivity.AnonymousClass22 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$deleteTheme$0(dialogInterface, i);
    }
}
