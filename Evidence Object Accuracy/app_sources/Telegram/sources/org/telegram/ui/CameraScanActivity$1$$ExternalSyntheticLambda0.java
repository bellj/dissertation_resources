package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.CameraScanActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CameraScanActivity$1$$ExternalSyntheticLambda0 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ CameraScanActivity.AnonymousClass1 f$0;

    public /* synthetic */ CameraScanActivity$1$$ExternalSyntheticLambda0(CameraScanActivity.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        this.f$0.lambda$new$0(dialogInterface);
    }
}
