package org.telegram.ui;

import org.telegram.ui.WebviewActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class WebviewActivity$TelegramWebviewProxy$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ WebviewActivity.TelegramWebviewProxy f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ WebviewActivity$TelegramWebviewProxy$$ExternalSyntheticLambda0(WebviewActivity.TelegramWebviewProxy telegramWebviewProxy, String str) {
        this.f$0 = telegramWebviewProxy;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$postEvent$0(this.f$1);
    }
}
