package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChannelAdminLogActivity$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ ChannelAdminLogActivity$$ExternalSyntheticLambda5 INSTANCE = new ChannelAdminLogActivity$$ExternalSyntheticLambda5();

    private /* synthetic */ ChannelAdminLogActivity$$ExternalSyntheticLambda5() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChannelAdminLogActivity.$r8$lambda$ELcKKlDFHmfmtJvyr7USZYsM5GE(view, motionEvent);
    }
}
