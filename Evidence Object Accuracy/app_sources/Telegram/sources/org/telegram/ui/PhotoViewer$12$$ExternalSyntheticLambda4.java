package org.telegram.ui;

import android.view.View;
import org.telegram.ui.PhotoViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoViewer$12$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ boolean[] f$0;

    public /* synthetic */ PhotoViewer$12$$ExternalSyntheticLambda4(boolean[] zArr) {
        this.f$0 = zArr;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PhotoViewer.AnonymousClass12.lambda$onItemClick$5(this.f$0, view);
    }
}
