package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CalendarActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CalendarActivity$MonthView$2$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CalendarActivity.MonthView.AnonymousClass2 f$0;

    public /* synthetic */ CalendarActivity$MonthView$2$$ExternalSyntheticLambda0(CalendarActivity.MonthView.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CalendarActivity.MonthView.AnonymousClass2.m657$r8$lambda$3d1TAWw1eVjS0za95p9MjFJjqM(this.f$0, view);
    }
}
