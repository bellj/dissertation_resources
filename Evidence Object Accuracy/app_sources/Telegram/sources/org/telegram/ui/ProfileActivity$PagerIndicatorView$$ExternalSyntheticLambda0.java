package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.ProfileActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProfileActivity$PagerIndicatorView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ProfileActivity.PagerIndicatorView f$0;

    public /* synthetic */ ProfileActivity$PagerIndicatorView$$ExternalSyntheticLambda0(ProfileActivity.PagerIndicatorView pagerIndicatorView) {
        this.f$0 = pagerIndicatorView;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$new$0(valueAnimator);
    }
}
