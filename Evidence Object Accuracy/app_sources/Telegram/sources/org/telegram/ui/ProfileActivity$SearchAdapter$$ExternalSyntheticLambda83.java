package org.telegram.ui;

import org.telegram.ui.ProfileActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProfileActivity$SearchAdapter$$ExternalSyntheticLambda83 implements Runnable {
    public final /* synthetic */ ProfileActivity.SearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ ProfileActivity$SearchAdapter$$ExternalSyntheticLambda83(ProfileActivity.SearchAdapter searchAdapter, String str) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$search$87(this.f$1);
    }
}
