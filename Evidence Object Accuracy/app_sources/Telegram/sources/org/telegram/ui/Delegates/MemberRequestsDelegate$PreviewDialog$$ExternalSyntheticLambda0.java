package org.telegram.ui.Delegates;

import android.animation.ValueAnimator;
import org.telegram.ui.Delegates.MemberRequestsDelegate;

/* loaded from: classes3.dex */
public final /* synthetic */ class MemberRequestsDelegate$PreviewDialog$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ MemberRequestsDelegate.PreviewDialog f$0;
    public final /* synthetic */ float f$1;
    public final /* synthetic */ float f$2;
    public final /* synthetic */ float f$3;
    public final /* synthetic */ float f$4;
    public final /* synthetic */ int f$5;

    public /* synthetic */ MemberRequestsDelegate$PreviewDialog$$ExternalSyntheticLambda0(MemberRequestsDelegate.PreviewDialog previewDialog, float f, float f2, float f3, float f4, int i) {
        this.f$0 = previewDialog;
        this.f$1 = f;
        this.f$2 = f2;
        this.f$3 = f3;
        this.f$4 = f4;
        this.f$5 = i;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$runAnimation$4(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, valueAnimator);
    }
}
