package org.telegram.ui.Delegates;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;

/* loaded from: classes3.dex */
public final /* synthetic */ class MemberRequestsDelegate$$ExternalSyntheticLambda9 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ MemberRequestsDelegate f$0;

    public /* synthetic */ MemberRequestsDelegate$$ExternalSyntheticLambda9(MemberRequestsDelegate memberRequestsDelegate) {
        this.f$0 = memberRequestsDelegate;
    }

    @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListener
    public final void onItemClick(View view, int i) {
        this.f$0.onItemClick(view, i);
    }
}
