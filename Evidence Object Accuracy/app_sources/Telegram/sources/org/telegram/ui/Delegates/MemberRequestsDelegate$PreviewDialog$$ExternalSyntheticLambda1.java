package org.telegram.ui.Delegates;

import android.view.View;
import org.telegram.ui.Delegates.MemberRequestsDelegate;

/* loaded from: classes3.dex */
public final /* synthetic */ class MemberRequestsDelegate$PreviewDialog$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ MemberRequestsDelegate.PreviewDialog f$0;

    public /* synthetic */ MemberRequestsDelegate$PreviewDialog$$ExternalSyntheticLambda1(MemberRequestsDelegate.PreviewDialog previewDialog) {
        this.f$0 = previewDialog;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$2(view);
    }
}
