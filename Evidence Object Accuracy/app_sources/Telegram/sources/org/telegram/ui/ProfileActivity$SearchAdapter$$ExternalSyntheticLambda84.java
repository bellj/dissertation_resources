package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.ProfileActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProfileActivity$SearchAdapter$$ExternalSyntheticLambda84 implements Runnable {
    public final /* synthetic */ ProfileActivity.SearchAdapter f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ ArrayList f$2;
    public final /* synthetic */ ArrayList f$3;
    public final /* synthetic */ ArrayList f$4;

    public /* synthetic */ ProfileActivity$SearchAdapter$$ExternalSyntheticLambda84(ProfileActivity.SearchAdapter searchAdapter, String str, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
        this.f$2 = arrayList;
        this.f$3 = arrayList2;
        this.f$4 = arrayList3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$search$86(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
