package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoAlbumPickerActivity$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda4 INSTANCE = new PhotoAlbumPickerActivity$$ExternalSyntheticLambda4();

    private /* synthetic */ PhotoAlbumPickerActivity$$ExternalSyntheticLambda4() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return PhotoAlbumPickerActivity.lambda$createView$1(view, motionEvent);
    }
}
