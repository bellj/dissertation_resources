package org.telegram.ui;

import android.view.View;
import org.telegram.ui.FeaturedStickersActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class FeaturedStickersActivity$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ FeaturedStickersActivity.ListAdapter f$0;

    public /* synthetic */ FeaturedStickersActivity$ListAdapter$$ExternalSyntheticLambda0(FeaturedStickersActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        FeaturedStickersActivity.ListAdapter.$r8$lambda$kklHG2NKbpKCBfqAEpNdNtrwnDM(this.f$0, view);
    }
}
