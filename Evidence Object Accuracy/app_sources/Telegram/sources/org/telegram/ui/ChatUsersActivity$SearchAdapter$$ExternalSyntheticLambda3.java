package org.telegram.ui;

import androidx.collection.LongSparseArray;
import java.util.ArrayList;
import org.telegram.ui.ChatUsersActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatUsersActivity$SearchAdapter$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ChatUsersActivity.SearchAdapter f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ LongSparseArray f$2;
    public final /* synthetic */ ArrayList f$3;
    public final /* synthetic */ ArrayList f$4;

    public /* synthetic */ ChatUsersActivity$SearchAdapter$$ExternalSyntheticLambda3(ChatUsersActivity.SearchAdapter searchAdapter, ArrayList arrayList, LongSparseArray longSparseArray, ArrayList arrayList2, ArrayList arrayList3) {
        this.f$0 = searchAdapter;
        this.f$1 = arrayList;
        this.f$2 = longSparseArray;
        this.f$3 = arrayList2;
        this.f$4 = arrayList3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$updateSearchResults$4(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
