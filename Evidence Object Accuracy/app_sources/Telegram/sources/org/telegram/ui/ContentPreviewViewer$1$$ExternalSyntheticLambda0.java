package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.ContentPreviewViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class ContentPreviewViewer$1$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ContentPreviewViewer.AnonymousClass1 f$0;

    public /* synthetic */ ContentPreviewViewer$1$$ExternalSyntheticLambda0(ContentPreviewViewer.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$run$2(valueAnimator);
    }
}
