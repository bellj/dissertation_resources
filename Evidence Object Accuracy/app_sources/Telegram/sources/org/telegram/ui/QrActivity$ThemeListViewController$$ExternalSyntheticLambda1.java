package org.telegram.ui;

import android.view.View;
import org.telegram.ui.QrActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class QrActivity$ThemeListViewController$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ QrActivity.ThemeListViewController f$0;

    public /* synthetic */ QrActivity$ThemeListViewController$$ExternalSyntheticLambda1(QrActivity.ThemeListViewController themeListViewController) {
        this.f$0 = themeListViewController;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
