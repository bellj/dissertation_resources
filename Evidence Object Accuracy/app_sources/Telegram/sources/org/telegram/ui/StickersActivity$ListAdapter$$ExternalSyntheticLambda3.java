package org.telegram.ui;

import android.view.View;
import org.telegram.ui.StickersActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class StickersActivity$ListAdapter$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ StickersActivity.ListAdapter f$0;

    public /* synthetic */ StickersActivity$ListAdapter$$ExternalSyntheticLambda3(StickersActivity.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onBindViewHolder$1(view);
    }
}
