package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CallLogActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CallLogActivity.EmptyTextProgressView f$0;

    public /* synthetic */ CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda0(CallLogActivity.EmptyTextProgressView emptyTextProgressView) {
        this.f$0 = emptyTextProgressView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CallLogActivity.EmptyTextProgressView.$r8$lambda$FeLFIX1uUcHqaq4rJaTvfcYvliw(this.f$0, view);
    }
}
