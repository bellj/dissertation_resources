package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ArticleViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class ArticleViewer$BlockChannelCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ArticleViewer.BlockChannelCell f$0;

    public /* synthetic */ ArticleViewer$BlockChannelCell$$ExternalSyntheticLambda0(ArticleViewer.BlockChannelCell blockChannelCell) {
        this.f$0 = blockChannelCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ArticleViewer.BlockChannelCell.m648$r8$lambda$DKnhx7DKCgdblTXK2peYxVCdXc(this.f$0, view);
    }
}
