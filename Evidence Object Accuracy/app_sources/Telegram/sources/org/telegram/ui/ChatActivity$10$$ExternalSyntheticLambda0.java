package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ChatActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivity$10$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ChatActivity.AnonymousClass10 f$0;

    public /* synthetic */ ChatActivity$10$$ExternalSyntheticLambda0(ChatActivity.AnonymousClass10 r1) {
        this.f$0 = r1;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onItemClick$2(view);
    }
}
