package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChangeBioActivity$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ChangeBioActivity$$ExternalSyntheticLambda1 INSTANCE = new ChangeBioActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ ChangeBioActivity$$ExternalSyntheticLambda1() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChangeBioActivity.$r8$lambda$DxhKRheu8pWVyqnQEnlye9rLLUE(view, motionEvent);
    }
}
