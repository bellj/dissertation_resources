package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;
import org.telegram.ui.CallLogActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1 INSTANCE = new CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1();

    private /* synthetic */ CallLogActivity$EmptyTextProgressView$$ExternalSyntheticLambda1() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return CallLogActivity.EmptyTextProgressView.m664$r8$lambda$0DfzUYohm5_C8g7TQPSfQTZEqo(view, motionEvent);
    }
}
