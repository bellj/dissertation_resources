package org.telegram.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.Pair;
import android.util.Property;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import androidx.collection.LongSparseArray;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.ColorUtils;
import androidx.core.util.Consumer;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.ChatListItemAnimator;
import androidx.recyclerview.widget.GridLayoutManagerFixed;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import j$.util.Comparator$CC;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.telegram.PhoneFormat.PhoneFormat;
import org.telegram.messenger.AccountInstance;
import org.telegram.messenger.AndroidUtilities;
import org.telegram.messenger.ApplicationLoader;
import org.telegram.messenger.BuildVars;
import org.telegram.messenger.ChatObject;
import org.telegram.messenger.ChatThemeController;
import org.telegram.messenger.ContactsController;
import org.telegram.messenger.DialogObject;
import org.telegram.messenger.Emoji;
import org.telegram.messenger.EmojiData;
import org.telegram.messenger.FileLoader;
import org.telegram.messenger.FileLog;
import org.telegram.messenger.ForwardingMessagesParams;
import org.telegram.messenger.ImageLocation;
import org.telegram.messenger.ImageReceiver;
import org.telegram.messenger.LocaleController;
import org.telegram.messenger.MediaController;
import org.telegram.messenger.MediaDataController;
import org.telegram.messenger.MessageObject;
import org.telegram.messenger.MessagesController;
import org.telegram.messenger.MessagesStorage;
import org.telegram.messenger.NotificationCenter;
import org.telegram.messenger.R;
import org.telegram.messenger.SendMessagesHelper;
import org.telegram.messenger.SharedConfig;
import org.telegram.messenger.UserConfig;
import org.telegram.messenger.UserObject;
import org.telegram.messenger.Utilities;
import org.telegram.messenger.VideoEditedInfo;
import org.telegram.messenger.browser.Browser;
import org.telegram.tgnet.ConnectionsManager;
import org.telegram.tgnet.RequestDelegate;
import org.telegram.tgnet.ResultCallback;
import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC$BotInfo;
import org.telegram.tgnet.TLRPC$BotInlineResult;
import org.telegram.tgnet.TLRPC$Chat;
import org.telegram.tgnet.TLRPC$ChatFull;
import org.telegram.tgnet.TLRPC$ChatInvite;
import org.telegram.tgnet.TLRPC$ChatParticipant;
import org.telegram.tgnet.TLRPC$ChatParticipants;
import org.telegram.tgnet.TLRPC$Dialog;
import org.telegram.tgnet.TLRPC$Document;
import org.telegram.tgnet.TLRPC$EncryptedChat;
import org.telegram.tgnet.TLRPC$FileLocation;
import org.telegram.tgnet.TLRPC$InputPeer;
import org.telegram.tgnet.TLRPC$InputStickerSet;
import org.telegram.tgnet.TLRPC$KeyboardButton;
import org.telegram.tgnet.TLRPC$Message;
import org.telegram.tgnet.TLRPC$MessageAction;
import org.telegram.tgnet.TLRPC$MessageEntity;
import org.telegram.tgnet.TLRPC$MessageFwdHeader;
import org.telegram.tgnet.TLRPC$MessageMedia;
import org.telegram.tgnet.TLRPC$MessageReplies;
import org.telegram.tgnet.TLRPC$Peer;
import org.telegram.tgnet.TLRPC$PhotoSize;
import org.telegram.tgnet.TLRPC$Poll;
import org.telegram.tgnet.TLRPC$PollResults;
import org.telegram.tgnet.TLRPC$ReplyMarkup;
import org.telegram.tgnet.TLRPC$TL_attachMenuBot;
import org.telegram.tgnet.TLRPC$TL_attachMenuBotsBot;
import org.telegram.tgnet.TLRPC$TL_availableReaction;
import org.telegram.tgnet.TLRPC$TL_botCommand;
import org.telegram.tgnet.TLRPC$TL_botInlineMessageMediaAuto;
import org.telegram.tgnet.TLRPC$TL_botInlineMessageMediaInvoice;
import org.telegram.tgnet.TLRPC$TL_channelForbidden;
import org.telegram.tgnet.TLRPC$TL_channelFull;
import org.telegram.tgnet.TLRPC$TL_channels_sendAsPeers;
import org.telegram.tgnet.TLRPC$TL_channels_viewSponsoredMessage;
import org.telegram.tgnet.TLRPC$TL_chatBannedRights;
import org.telegram.tgnet.TLRPC$TL_chatFull;
import org.telegram.tgnet.TLRPC$TL_chatInviteExported;
import org.telegram.tgnet.TLRPC$TL_contacts_acceptContact;
import org.telegram.tgnet.TLRPC$TL_contacts_resolvedPeer;
import org.telegram.tgnet.TLRPC$TL_document;
import org.telegram.tgnet.TLRPC$TL_error;
import org.telegram.tgnet.TLRPC$TL_exportedMessageLink;
import org.telegram.tgnet.TLRPC$TL_game;
import org.telegram.tgnet.TLRPC$TL_groupCall;
import org.telegram.tgnet.TLRPC$TL_inlineBotSwitchPM;
import org.telegram.tgnet.TLRPC$TL_inputMediaPoll;
import org.telegram.tgnet.TLRPC$TL_inputStickerSetID;
import org.telegram.tgnet.TLRPC$TL_inputStickerSetShortName;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonBuy;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonCallback;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonGame;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonRow;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonSwitchInline;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonUrl;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonUrlAuth;
import org.telegram.tgnet.TLRPC$TL_keyboardButtonUserProfile;
import org.telegram.tgnet.TLRPC$TL_messageActionGameScore;
import org.telegram.tgnet.TLRPC$TL_messageActionPaymentSent;
import org.telegram.tgnet.TLRPC$TL_messageActionPinMessage;
import org.telegram.tgnet.TLRPC$TL_messageActionSetMessagesTTL;
import org.telegram.tgnet.TLRPC$TL_messageEmpty;
import org.telegram.tgnet.TLRPC$TL_messageMediaPhoto;
import org.telegram.tgnet.TLRPC$TL_messageMediaPoll;
import org.telegram.tgnet.TLRPC$TL_messageMediaWebPage;
import org.telegram.tgnet.TLRPC$TL_messagePeerReaction;
import org.telegram.tgnet.TLRPC$TL_messageReplyHeader;
import org.telegram.tgnet.TLRPC$TL_messages_acceptUrlAuth;
import org.telegram.tgnet.TLRPC$TL_messages_discussionMessage;
import org.telegram.tgnet.TLRPC$TL_messages_editMessage;
import org.telegram.tgnet.TLRPC$TL_messages_getAttachMenuBot;
import org.telegram.tgnet.TLRPC$TL_messages_getDiscussionMessage;
import org.telegram.tgnet.TLRPC$TL_messages_getHistory;
import org.telegram.tgnet.TLRPC$TL_messages_getMessageEditData;
import org.telegram.tgnet.TLRPC$TL_messages_getReplies;
import org.telegram.tgnet.TLRPC$TL_messages_getWebPagePreview;
import org.telegram.tgnet.TLRPC$TL_messages_rateTranscribedAudio;
import org.telegram.tgnet.TLRPC$TL_messages_requestUrlAuth;
import org.telegram.tgnet.TLRPC$TL_messages_sendScheduledMessages;
import org.telegram.tgnet.TLRPC$TL_messages_toggleBotInAttachMenu;
import org.telegram.tgnet.TLRPC$TL_payments_bankCardData;
import org.telegram.tgnet.TLRPC$TL_payments_paymentReceipt;
import org.telegram.tgnet.TLRPC$TL_poll;
import org.telegram.tgnet.TLRPC$TL_pollAnswer;
import org.telegram.tgnet.TLRPC$TL_reactionCount;
import org.telegram.tgnet.TLRPC$TL_replyKeyboardForceReply;
import org.telegram.tgnet.TLRPC$TL_updates_channelDifferenceTooLong;
import org.telegram.tgnet.TLRPC$TL_urlAuthResultAccepted;
import org.telegram.tgnet.TLRPC$TL_urlAuthResultDefault;
import org.telegram.tgnet.TLRPC$TL_urlAuthResultRequest;
import org.telegram.tgnet.TLRPC$TL_webPage;
import org.telegram.tgnet.TLRPC$TL_webPagePending;
import org.telegram.tgnet.TLRPC$Updates;
import org.telegram.tgnet.TLRPC$User;
import org.telegram.tgnet.TLRPC$UserFull;
import org.telegram.tgnet.TLRPC$WebPage;
import org.telegram.tgnet.TLRPC$messages_Messages;
import org.telegram.ui.ActionBar.ActionBar;
import org.telegram.ui.ActionBar.ActionBarLayout;
import org.telegram.ui.ActionBar.ActionBarMenuItem;
import org.telegram.ui.ActionBar.ActionBarMenuSubItem;
import org.telegram.ui.ActionBar.ActionBarPopupWindow;
import org.telegram.ui.ActionBar.AlertDialog;
import org.telegram.ui.ActionBar.BackDrawable;
import org.telegram.ui.ActionBar.BaseFragment;
import org.telegram.ui.ActionBar.BottomSheet;
import org.telegram.ui.ActionBar.EmojiThemes;
import org.telegram.ui.ActionBar.SimpleTextView;
import org.telegram.ui.ActionBar.Theme;
import org.telegram.ui.ActionBar.ThemeDescription;
import org.telegram.ui.Adapters.MessagesSearchAdapter;
import org.telegram.ui.Adapters.StickersAdapter;
import org.telegram.ui.AvatarPreviewer;
import org.telegram.ui.Cells.BotHelpCell;
import org.telegram.ui.Cells.BotSwitchCell;
import org.telegram.ui.Cells.ChatActionCell;
import org.telegram.ui.Cells.ChatLoadingCell;
import org.telegram.ui.Cells.ChatMessageCell;
import org.telegram.ui.Cells.ChatUnreadCell;
import org.telegram.ui.Cells.CheckBoxCell;
import org.telegram.ui.Cells.ContextLinkCell;
import org.telegram.ui.Cells.DialogCell;
import org.telegram.ui.Cells.MentionCell;
import org.telegram.ui.Cells.StickerCell;
import org.telegram.ui.Cells.TextSelectionHelper;
import org.telegram.ui.Components.AlertsCreator;
import org.telegram.ui.Components.AnimatedFileDrawable;
import org.telegram.ui.Components.AttachBotIntroTopView;
import org.telegram.ui.Components.BackButtonMenu;
import org.telegram.ui.Components.BackupImageView;
import org.telegram.ui.Components.BluredView;
import org.telegram.ui.Components.BlurredFrameLayout;
import org.telegram.ui.Components.BotCommandsMenuView;
import org.telegram.ui.Components.Bulletin;
import org.telegram.ui.Components.BulletinFactory;
import org.telegram.ui.Components.ChatActivityEnterTopView;
import org.telegram.ui.Components.ChatActivityEnterView;
import org.telegram.ui.Components.ChatAttachAlert;
import org.telegram.ui.Components.ChatAttachAlertDocumentLayout;
import org.telegram.ui.Components.ChatAttachAlertPhotoLayout;
import org.telegram.ui.Components.ChatAvatarContainer;
import org.telegram.ui.Components.ChatBigEmptyView;
import org.telegram.ui.Components.ChatGreetingsView;
import org.telegram.ui.Components.ChatNotificationsPopupWrapper;
import org.telegram.ui.Components.ChatThemeBottomSheet;
import org.telegram.ui.Components.ChecksHintView;
import org.telegram.ui.Components.ClippingImageView;
import org.telegram.ui.Components.CounterView;
import org.telegram.ui.Components.CrossfadeDrawable;
import org.telegram.ui.Components.CubicBezierInterpolator;
import org.telegram.ui.Components.EditTextCaption;
import org.telegram.ui.Components.EmbedBottomSheet;
import org.telegram.ui.Components.EmojiView;
import org.telegram.ui.Components.FireworksOverlay;
import org.telegram.ui.Components.ForwardingPreviewView;
import org.telegram.ui.Components.FragmentContextView;
import org.telegram.ui.Components.GigagroupConvertAlert;
import org.telegram.ui.Components.HideViewAfterAnimation;
import org.telegram.ui.Components.HintView;
import org.telegram.ui.Components.ImportingAlert;
import org.telegram.ui.Components.InstantCameraView;
import org.telegram.ui.Components.InviteMembersBottomSheet;
import org.telegram.ui.Components.JoinGroupAlert;
import org.telegram.ui.Components.LayoutHelper;
import org.telegram.ui.Components.MentionsContainerView;
import org.telegram.ui.Components.MotionBackgroundDrawable;
import org.telegram.ui.Components.NumberTextView;
import org.telegram.ui.Components.PhonebookShareAlert;
import org.telegram.ui.Components.PinnedLineView;
import org.telegram.ui.Components.PipRoundVideoView;
import org.telegram.ui.Components.PollVotesAlert;
import org.telegram.ui.Components.RLottieDrawable;
import org.telegram.ui.Components.RadialProgressView;
import org.telegram.ui.Components.ReactedUsersListView;
import org.telegram.ui.Components.ReactionTabHolderView;
import org.telegram.ui.Components.Reactions.ReactionsEffectOverlay;
import org.telegram.ui.Components.Reactions.ReactionsLayoutInBubble;
import org.telegram.ui.Components.ReactionsContainerLayout;
import org.telegram.ui.Components.RecyclerAnimationScrollHelper;
import org.telegram.ui.Components.RecyclerListView;
import org.telegram.ui.Components.ReportAlert;
import org.telegram.ui.Components.SearchCounterView;
import org.telegram.ui.Components.ShareAlert;
import org.telegram.ui.Components.SizeNotifierFrameLayout;
import org.telegram.ui.Components.TextSelectionHint;
import org.telegram.ui.Components.TextStyleSpan;
import org.telegram.ui.Components.ThemeEditorView;
import org.telegram.ui.Components.TranscribeButton;
import org.telegram.ui.Components.TranslateAlert;
import org.telegram.ui.Components.TrendingStickersAlert;
import org.telegram.ui.Components.TypefaceSpan;
import org.telegram.ui.Components.URLSpanBotCommand;
import org.telegram.ui.Components.URLSpanReplacement;
import org.telegram.ui.Components.URLSpanUserMention;
import org.telegram.ui.Components.UndoView;
import org.telegram.ui.Components.voip.VoIPHelper;
import org.telegram.ui.ContactAddActivity;
import org.telegram.ui.ContentPreviewViewer;
import org.telegram.ui.Delegates.ChatActivityMemberRequestsDelegate;
import org.telegram.ui.DialogsActivity;
import org.telegram.ui.GroupCreateActivity;
import org.telegram.ui.LocationActivity;
import org.telegram.ui.PhotoAlbumPickerActivity;
import org.telegram.ui.PhotoViewer;
import org.telegram.ui.PollCreateActivity;
import org.webrtc.MediaStreamTrack;

/* loaded from: classes3.dex */
public class ChatActivity extends BaseFragment implements NotificationCenter.NotificationCenterDelegate, DialogsActivity.DialogsActivityDelegate, LocationActivity.LocationActivityDelegate, ChatAttachAlertDocumentLayout.DocumentSelectActivityDelegate {
    private static final int[] allowedNotificationsDuringChatListAnimations = {NotificationCenter.messagesRead, NotificationCenter.threadMessagesRead, NotificationCenter.commentsRead, NotificationCenter.messagesReadEncrypted, NotificationCenter.messagesReadContent, NotificationCenter.didLoadPinnedMessages, NotificationCenter.newDraftReceived, NotificationCenter.updateMentionsCount, NotificationCenter.didUpdateConnectionState, NotificationCenter.updateInterfaces, NotificationCenter.updateDefaultSendAsPeer, NotificationCenter.closeChats, NotificationCenter.chatInfoCantLoad, NotificationCenter.userInfoDidLoad, NotificationCenter.pinnedInfoDidLoad, NotificationCenter.didSetNewWallpapper, NotificationCenter.didApplyNewTheme};
    public static Pattern privateMsgUrlPattern;
    public static Pattern publicMsgUrlPattern;
    private static boolean replacingChatActivity = false;
    public static Pattern voiceChatUrlPattern;
    private Paint actionBarBackgroundPaint = new Paint(1);
    private ArrayList<View> actionModeViews = new ArrayList<>();
    private long activityResumeTime;
    private ActionBarMenuSubItem addContactItem;
    private TextView addToContactsButton;
    private boolean addToContactsButtonArchive;
    private TextView alertNameTextView;
    private TextView alertTextView;
    private FrameLayout alertView;
    private AnimatorSet alertViewAnimator;
    private float alertViewEnterProgress;
    private boolean allowContextBotPanel;
    private boolean allowContextBotPanelSecond = true;
    public boolean allowExpandPreviewByClick;
    private boolean allowStickersPanel;
    private HashMap<MessageObject, Boolean> alreadyPlayedStickers = new HashMap<>();
    private ArrayList<ChatMessageCell> animateSendingViews = new ArrayList<>();
    boolean animateTo;
    private HashMap<TLRPC$Document, Integer> animatingDocuments = new HashMap<>();
    private ClippingImageView animatingImageView;
    public ArrayList<MessageObject> animatingMessageObjects = new ArrayList<>();
    private Paint aspectPaint;
    private Path aspectPath;
    private AspectRatioFrameLayout aspectRatioFrameLayout;
    private ActionBarMenuItem attachItem;
    private String attachMenuBotStartCommand;
    private String attachMenuBotToOpen;
    private ActionBarMenuItem audioCallIconItem;
    private ChatAvatarContainer avatarContainer;
    private ChatActivity backToPreviousFragment;
    private ChatBigEmptyView bigEmptyView;
    private BluredView blurredView;
    public int blurredViewBottomOffset;
    public int blurredViewTopOffset;
    private MessageObject botButtons;
    private PhotoViewer.PhotoViewerProvider botContextProvider = new PhotoViewer.EmptyPhotoViewerProvider() { // from class: org.telegram.ui.ChatActivity.5
        /* JADX WARNING: Removed duplicated region for block: B:24:0x008f A[LOOP:0: B:11:0x0035->B:24:0x008f, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0058 A[SYNTHETIC] */
        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public org.telegram.ui.PhotoViewer.PlaceProviderObject getPlaceForPhoto(org.telegram.messenger.MessageObject r5, org.telegram.tgnet.TLRPC$FileLocation r6, int r7, boolean r8) {
            /*
                r4 = this;
                r5 = 0
                if (r7 < 0) goto L_0x0092
                org.telegram.ui.ChatActivity r6 = org.telegram.ui.ChatActivity.this
                java.util.ArrayList r6 = org.telegram.ui.ChatActivity.access$900(r6)
                int r6 = r6.size()
                if (r7 >= r6) goto L_0x0092
                org.telegram.ui.ChatActivity r6 = org.telegram.ui.ChatActivity.this
                org.telegram.ui.Components.MentionsContainerView r6 = r6.mentionContainer
                if (r6 == 0) goto L_0x0092
                org.telegram.ui.Components.MentionsContainerView$MentionsListView r6 = r6.getListView()
                if (r6 != 0) goto L_0x001d
                goto L_0x0092
            L_0x001d:
                org.telegram.ui.ChatActivity r6 = org.telegram.ui.ChatActivity.this
                org.telegram.ui.Components.MentionsContainerView r6 = r6.mentionContainer
                org.telegram.ui.Components.MentionsContainerView$MentionsListView r6 = r6.getListView()
                int r6 = r6.getChildCount()
                org.telegram.ui.ChatActivity r8 = org.telegram.ui.ChatActivity.this
                java.util.ArrayList r8 = org.telegram.ui.ChatActivity.access$900(r8)
                java.lang.Object r7 = r8.get(r7)
                r8 = 0
                r0 = 0
            L_0x0035:
                if (r0 >= r6) goto L_0x0092
                org.telegram.ui.ChatActivity r1 = org.telegram.ui.ChatActivity.this
                org.telegram.ui.Components.MentionsContainerView r1 = r1.mentionContainer
                org.telegram.ui.Components.MentionsContainerView$MentionsListView r1 = r1.getListView()
                android.view.View r1 = r1.getChildAt(r0)
                boolean r2 = r1 instanceof org.telegram.ui.Cells.ContextLinkCell
                if (r2 == 0) goto L_0x0055
                r2 = r1
                org.telegram.ui.Cells.ContextLinkCell r2 = (org.telegram.ui.Cells.ContextLinkCell) r2
                org.telegram.tgnet.TLRPC$BotInlineResult r3 = r2.getResult()
                if (r3 != r7) goto L_0x0055
                org.telegram.messenger.ImageReceiver r2 = r2.getPhotoImage()
                goto L_0x0056
            L_0x0055:
                r2 = r5
            L_0x0056:
                if (r2 == 0) goto L_0x008f
                r5 = 2
                int[] r5 = new int[r5]
                r1.getLocationInWindow(r5)
                org.telegram.ui.PhotoViewer$PlaceProviderObject r6 = new org.telegram.ui.PhotoViewer$PlaceProviderObject
                r6.<init>()
                r7 = r5[r8]
                r6.viewX = r7
                r7 = 1
                r5 = r5[r7]
                int r7 = android.os.Build.VERSION.SDK_INT
                r0 = 21
                if (r7 < r0) goto L_0x0071
                goto L_0x0073
            L_0x0071:
                int r8 = org.telegram.messenger.AndroidUtilities.statusBarHeight
            L_0x0073:
                int r5 = r5 - r8
                r6.viewY = r5
                org.telegram.ui.ChatActivity r5 = org.telegram.ui.ChatActivity.this
                org.telegram.ui.Components.MentionsContainerView r5 = r5.mentionContainer
                org.telegram.ui.Components.MentionsContainerView$MentionsListView r5 = r5.getListView()
                r6.parentView = r5
                r6.imageReceiver = r2
                org.telegram.messenger.ImageReceiver$BitmapHolder r5 = r2.getBitmapSafe()
                r6.thumb = r5
                int[] r5 = r2.getRoundRadius()
                r6.radius = r5
                return r6
            L_0x008f:
                int r0 = r0 + 1
                goto L_0x0035
            L_0x0092:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.AnonymousClass5.getPlaceForPhoto(org.telegram.messenger.MessageObject, org.telegram.tgnet.TLRPC$FileLocation, int, boolean):org.telegram.ui.PhotoViewer$PlaceProviderObject");
        }

        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
        public void sendButtonPressed(int i, VideoEditedInfo videoEditedInfo, boolean z, int i2, boolean z2) {
            if (i >= 0 && i < ChatActivity.this.botContextResults.size()) {
                ChatActivity chatActivity = ChatActivity.this;
                chatActivity.lambda$createView$45((TLRPC$BotInlineResult) chatActivity.botContextResults.get(i), z, i2);
            }
        }
    };
    private ArrayList<Object> botContextResults;
    private LongSparseArray<TLRPC$BotInfo> botInfo = new LongSparseArray<>();
    private MessageObject botReplyButtons;
    private String botUser;
    private int botsCount;
    private BlurredFrameLayout bottomMessagesActionContainer;
    private FrameLayout bottomOverlay;
    private AnimatorSet bottomOverlayAnimation;
    private BlurredFrameLayout bottomOverlayChat;
    private UnreadCounterTextView bottomOverlayChatText;
    private ImageView bottomOverlayImage;
    private RadialProgressView bottomOverlayProgress;
    private TextView bottomOverlayText;
    private float bottomPanelTranslationY;
    private float bottomPanelTranslationYReverse;
    Bulletin.Delegate bulletinDelegate;
    private boolean[] cacheEndReached = new boolean[2];
    private int canEditMessagesCount;
    private int canForwardMessagesCount;
    private int canSaveDocumentsCount;
    private int canSaveMusicCount;
    private boolean canShowPagedownButton;
    private Runnable cancelFixedPositionRunnable;
    private int cantDeleteMessagesCount;
    private int cantForwardMessagesCount;
    private int cantSaveMessagesCount;
    private ValueAnimator changeBoundAnimator;
    private ChatActivityDelegate chatActivityDelegate;
    private ChatActivityEnterTopView chatActivityEnterTopView;
    protected ChatActivityEnterView chatActivityEnterView;
    private boolean chatActivityEnterViewAnimateBeforeSending;
    private int chatActivityEnterViewAnimateFromTop;
    private ChatActivityAdapter chatAdapter;
    private ChatAttachAlert chatAttachAlert;
    private int chatEmojiViewPadding;
    private long chatEnterTime;
    protected TLRPC$ChatFull chatInfo;
    private TLRPC$ChatInvite chatInvite;
    private Runnable chatInviteRunnable;
    private long chatInviterId;
    private GridLayoutManagerFixed chatLayoutManager;
    private long chatLeaveTime;
    private ChatListItemAnimator chatListItemAnimator;
    private RecyclerListView chatListView;
    private float chatListViewPaddingTop;
    private int chatListViewPaddingVisibleOffset;
    private ArrayList<ChatMessageCell> chatMessageCellsCache = new ArrayList<>();
    private int chatMode;
    private ChatNotificationsPopupWrapper chatNotificationsPopupWrapper;
    private RecyclerAnimationScrollHelper chatScrollHelper;
    private final ChatScrollCallback chatScrollHelperCallback = new ChatScrollCallback();
    private ChatThemeBottomSheet chatThemeBottomSheet;
    private boolean chatWasReset;
    private TextView chatWithAdminTextView;
    private Runnable checkPaddingsRunnable;
    private boolean checkTextureViewPosition;
    private ChecksHintView checksHintView;
    private ActionBarMenuSubItem clearHistoryItem;
    private boolean clearingHistory;
    private Dialog closeChatDialog;
    private ImageView closePinned;
    private ImageView closeReportSpam;
    private int commentLoadingGuid;
    private int commentLoadingMessageId;
    private int commentMessagesLoadingGuid;
    private int commentMessagesRequestId;
    private int commentRequestId;
    private int contentPaddingTop;
    private float contentPanTranslation;
    public SizeNotifierFrameLayout contentView;
    private boolean createGroupCall;
    private int createUnreadMessageAfterId;
    private boolean createUnreadMessageAfterIdLoading;
    protected TLRPC$Chat currentChat;
    protected TLRPC$EncryptedChat currentEncryptedChat;
    private boolean currentFloatingDateOnScreen;
    private boolean currentFloatingTopIsNotMessage;
    private String currentPicturePath;
    private int currentPinnedMessageId;
    private int[] currentPinnedMessageIndex = new int[1];
    protected TLRPC$User currentUser;
    private Runnable delayedReadRunnable;
    private Runnable destroyTextureViewRunnable = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda126
        @Override // java.lang.Runnable
        public final void run() {
            ChatActivity.this.lambda$new$0();
        }
    };
    int dialogFilterId;
    int dialogFolderId;
    private long dialog_id;
    private Long dialog_id_Long;
    private int distanceToPeer;
    private ChatMessageCell drawLaterRoundProgressCell;
    public float drawingChatLisViewYoffset;
    private ChatMessageCell dummyMessageCell;
    private AnimatorSet editButtonAnimation;
    private int editTextEnd;
    private ActionBarMenuItem editTextItem;
    private int editTextStart;
    private MessageObject editingMessageObject;
    private int editingMessageObjectReqId;
    private EmojiAnimationsOverlay emojiAnimationsOverlay;
    private View emojiButtonRed;
    private TextView emptyView;
    private FrameLayout emptyViewContainer;
    private boolean[] endReached = new boolean[2];
    private FireworksOverlay fireworksOverlay;
    private boolean first = true;
    private boolean firstLoading = true;
    boolean firstOpen = true;
    private boolean firstUnreadSent;
    private int first_unread_id;
    private boolean fixPaddingsInLayout;
    private int fixedKeyboardHeight = -1;
    private AnimatorSet floatingDateAnimation;
    private ChatActionCell floatingDateView;
    private float floatingDateViewOffset;
    private boolean forceHistoryEmpty;
    private int forceNextPinnedMessageId;
    private boolean forceScrollToFirst;
    private boolean forceScrollToTop;
    private TextView forwardButton;
    private AnimatorSet forwardButtonAnimation;
    private boolean[] forwardEndReached = {true, true};
    private HintView forwardHintView;
    private MessageObject forwardingMessage;
    private MessageObject.GroupedMessages forwardingMessageGroup;
    private ForwardingMessagesParams forwardingMessages;
    ForwardingPreviewView forwardingPreviewView;
    private ArrayList<CharSequence> foundUrls;
    private TLRPC$WebPage foundWebPage;
    private FragmentContextView fragmentContextView;
    private FragmentContextView fragmentLocationContextView;
    public boolean fragmentOpened;
    private AnimatorSet fragmentTransition;
    private Runnable fragmentTransitionRunnable = new Runnable() { // from class: org.telegram.ui.ChatActivity.2
        @Override // java.lang.Runnable
        public void run() {
            if (ChatActivity.this.fragmentTransition != null && !ChatActivity.this.fragmentTransition.isRunning()) {
                ChatActivity.this.fragmentTransition.start();
            }
        }
    };
    private boolean fromPullingDownTransition;
    private HintView fwdRestrictedBottomHint;
    private HintView fwdRestrictedTopHint;
    private HintView gifHintTextView;
    private boolean globalIgnoreLayout;
    private ChatGreetingsView greetingsViewContainer;
    private ChatObject.Call groupCall;
    private LongSparseArray<MessageObject.GroupedMessages> groupedMessagesMap = new LongSparseArray<>();
    private boolean hasAllMentionsLocal;
    private boolean hasBotsCommands;
    private boolean hasUnfavedSelected;
    private ActionBarMenuItem headerItem;
    private Runnable hideAlertViewRunnable;
    private int hideDateDelay = 500;
    private boolean hideForwardEndReached;
    private int highlightMessageId = ConnectionsManager.DEFAULT_DATACENTER_ID;
    private MessageObject hintMessageObject;
    private int hintMessageType;
    private boolean ignoreAttachOnPause;
    private ChatActionCell infoTopView;
    private Animator infoTopViewAnimator;
    private long inlineReturn;
    private InstantCameraView instantCameraView;
    private boolean invalidateChatListViewTopPadding;
    private boolean invalidateMessagesVisiblePart;
    private boolean isComments;
    private boolean isFullyVisible;
    private boolean isPauseOnThemePreview;
    private KeyboardHideHelper keyboardHideHelper = new KeyboardHideHelper();
    private boolean lastCallCheckFromServer;
    private int lastLoadIndex = 1;
    private int lastStableId = 10;
    private float lastTouchY;
    private int last_message_id = 0;
    private int linkSearchRequestId;
    private boolean livestream;
    private int loadedPinnedMessagesCount;
    private boolean loading;
    private boolean loadingForward;
    private boolean loadingFromOldPosition;
    private SparseArray<Boolean> loadingPinnedMessages = new SparseArray<>();
    private boolean loadingPinnedMessagesList;
    private int loadsCount;
    private boolean locationAlertShown;
    private int[] maxDate = {Integer.MIN_VALUE, Integer.MIN_VALUE};
    private int[] maxMessageId = {ConnectionsManager.DEFAULT_DATACENTER_ID, ConnectionsManager.DEFAULT_DATACENTER_ID};
    private boolean maybeStartTrackingSlidingView;
    private HintView mediaBanTooltip;
    public MentionsContainerView mentionContainer;
    private AnimatorSet mentionListAnimation;
    private FrameLayout mentiondownButton;
    private ValueAnimator mentiondownButtonAnimation;
    private SimpleTextView mentiondownButtonCounter;
    private ImageView mentiondownButtonImage;
    private float mentionsButtonEnterProgress;
    private RecyclerListView.OnItemClickListener mentionsOnItemClickListener;
    private ActionBarMenuSubItem menuDeleteItem;
    private long mergeDialogId;
    private Animator messageEditTextAnimator;
    public MessageEnterTransitionContainer messageEnterTransitionContainer;
    protected ArrayList<MessageObject> messages = new ArrayList<>();
    private HashMap<String, ArrayList<MessageObject>> messagesByDays = new HashMap<>();
    private SparseArray<MessageObject>[] messagesDict = {new SparseArray<>(), new SparseArray<>()};
    private MessagesSearchAdapter messagesSearchAdapter;
    private RecyclerListView messagesSearchListView;
    private AnimatorSet messagesSearchListViewAnimation;
    private int[] minDate = new int[2];
    private int[] minMessageId = {Integer.MIN_VALUE, Integer.MIN_VALUE};
    private ActionBarMenuSubItem muteItem;
    private View muteItemGap;
    private MessageObject needAnimateToMessage;
    private boolean needRemovePreviousSameChatActivity = true;
    private boolean needSelectFromMessageId;
    private int newMentionsCount;
    private int newUnreadMessageCount;
    private boolean nextScrollForce;
    private int nextScrollForcePinnedMessageId;
    private int nextScrollFromMessageId;
    private int nextScrollLoadIndex;
    private boolean nextScrollSelect;
    private int nextScrollToMessageId;
    private HintView noSoundHintView;
    private Runnable onChatMessagesLoaded;
    RecyclerListView.OnItemClickListenerExtended onItemClickListener = new RecyclerListView.OnItemClickListenerExtended() { // from class: org.telegram.ui.ChatActivity.8
        @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListenerExtended
        public void onItemClick(View view, int i, float f, float f2) {
            if (!((BaseFragment) ChatActivity.this).inPreviewMode) {
                ChatActivity.this.wasManualScroll = true;
                boolean z = false;
                if (view instanceof ChatActionCell) {
                    ChatActionCell chatActionCell = (ChatActionCell) view;
                    if (chatActionCell.getMessageObject().isDateObject) {
                        Bundle bundle = new Bundle();
                        int i2 = chatActionCell.getMessageObject().messageOwner.date;
                        bundle.putLong("dialog_id", ChatActivity.this.dialog_id);
                        bundle.putInt("type", 0);
                        ChatActivity.this.presentFragment(new CalendarActivity(bundle, 0, i2));
                        return;
                    }
                }
                if (((BaseFragment) ChatActivity.this).actionBar.isActionModeShowed() || ChatActivity.this.reportType >= 0) {
                    if (view instanceof ChatMessageCell) {
                        ChatMessageCell chatMessageCell = (ChatMessageCell) view;
                        if (!ChatActivity.this.textSelectionHelper.isSelected(chatMessageCell.getMessageObject())) {
                            z = !chatMessageCell.isInsideBackground(f, f2);
                        } else {
                            return;
                        }
                    }
                    ChatActivity.this.processRowSelect(view, z, f, f2);
                    return;
                }
                ChatActivity.this.createMenu(view, true, false, f, f2);
            }
        }

        @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListenerExtended
        public boolean hasDoubleTap(View view, int i) {
            TLRPC$ChatFull tLRPC$ChatFull;
            TLRPC$TL_availableReaction tLRPC$TL_availableReaction = ChatActivity.this.getMediaDataController().getReactionsMap().get(ChatActivity.this.getMediaDataController().getDoubleTapReaction());
            if (tLRPC$TL_availableReaction == null) {
                return false;
            }
            boolean z = ChatActivity.this.dialog_id >= 0;
            if (!z && (tLRPC$ChatFull = ChatActivity.this.chatInfo) != null) {
                Iterator<String> it = tLRPC$ChatFull.available_reactions.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().equals(tLRPC$TL_availableReaction.reaction)) {
                            z = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (!z || !(view instanceof ChatMessageCell)) {
                return false;
            }
            ChatMessageCell chatMessageCell = (ChatMessageCell) view;
            if (chatMessageCell.getMessageObject().isSending() || chatMessageCell.getMessageObject().isEditing() || chatMessageCell.getMessageObject().type == 16 || ((BaseFragment) ChatActivity.this).actionBar.isActionModeShowed() || ChatActivity.this.isSecretChat() || ChatActivity.this.isInScheduleMode() || chatMessageCell.getMessageObject().isSponsored()) {
                return false;
            }
            return true;
        }

        @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListenerExtended
        public void onDoubleTap(View view, int i, float f, float f2) {
            TLRPC$ChatFull tLRPC$ChatFull;
            if ((view instanceof ChatMessageCell) && ChatActivity.this.getParentActivity() != null && !ChatActivity.this.isSecretChat() && !ChatActivity.this.isInScheduleMode() && !ChatActivity.this.isInPreviewMode()) {
                ChatMessageCell chatMessageCell = (ChatMessageCell) view;
                MessageObject primaryMessageObject = chatMessageCell.getPrimaryMessageObject();
                boolean z = false;
                ReactionsEffectOverlay.removeCurrent(false);
                TLRPC$TL_availableReaction tLRPC$TL_availableReaction = ChatActivity.this.getMediaDataController().getReactionsMap().get(ChatActivity.this.getMediaDataController().getDoubleTapReaction());
                if (tLRPC$TL_availableReaction != null && !chatMessageCell.getMessageObject().isSponsored()) {
                    boolean z2 = true;
                    if (ChatActivity.this.dialog_id >= 0) {
                        z = true;
                    }
                    if (!z && (tLRPC$ChatFull = ChatActivity.this.chatInfo) != null) {
                        Iterator<String> it = tLRPC$ChatFull.available_reactions.iterator();
                        while (it.hasNext()) {
                            if (it.next().equals(tLRPC$TL_availableReaction.reaction)) {
                                break;
                            }
                        }
                    }
                    z2 = z;
                    if (z2) {
                        ChatActivity.this.selectReaction(primaryMessageObject, null, f, f2, tLRPC$TL_availableReaction, true, false);
                    }
                }
            }
        }
    };
    RecyclerListView.OnItemLongClickListenerExtended onItemLongClickListener = new RecyclerListView.OnItemLongClickListenerExtended() { // from class: org.telegram.ui.ChatActivity.6
        @Override // org.telegram.ui.Components.RecyclerListView.OnItemLongClickListenerExtended
        public /* synthetic */ void onLongClickRelease() {
            RecyclerListView.OnItemLongClickListenerExtended.CC.$default$onLongClickRelease(this);
        }

        @Override // org.telegram.ui.Components.RecyclerListView.OnItemLongClickListenerExtended
        public /* synthetic */ void onMove(float f, float f2) {
            RecyclerListView.OnItemLongClickListenerExtended.CC.$default$onMove(this, f, f2);
        }

        @Override // org.telegram.ui.Components.RecyclerListView.OnItemLongClickListenerExtended
        public boolean onItemClick(View view, int i, float f, float f2) {
            boolean z;
            boolean z2 = false;
            if (ChatActivity.this.keyboardHideHelper.disableScrolling() || ChatActivity.this.textSelectionHelper.isTryingSelect() || ChatActivity.this.textSelectionHelper.isSelectionMode() || ((BaseFragment) ChatActivity.this).inPreviewMode) {
                return false;
            }
            ChatActivity.this.wasManualScroll = true;
            if (((BaseFragment) ChatActivity.this).actionBar.isActionModeShowed() || (ChatActivity.this.reportType >= 0 && (!(view instanceof ChatActionCell) || !(((ChatActionCell) view).getMessageObject().messageOwner.action instanceof TLRPC$TL_messageActionSetMessagesTTL)))) {
                if (view instanceof ChatMessageCell) {
                    z2 = !((ChatMessageCell) view).isInsideBackground(f, f2);
                }
                ChatActivity.this.processRowSelect(view, z2, f, f2);
                z = true;
            } else {
                z = ChatActivity.this.createMenu(view, false, true, f, f2);
            }
            if (!(view instanceof ChatMessageCell)) {
                return z;
            }
            ChatActivity.this.startMultiselect(i);
            return true;
        }
    };
    public Runnable onThemeChange;
    public boolean openAnimationEnded;
    private long openAnimationStartTime;
    private boolean openImport;
    protected boolean openKeyboardOnAttachMenuClose;
    private boolean openSearchKeyboard;
    private View overlayView;
    private FrameLayout pagedownButton;
    private ValueAnimator pagedownButtonAnimation;
    private CounterView pagedownButtonCounter;
    private float pagedownButtonEnterProgress;
    private ImageView pagedownButtonImage;
    private boolean pagedownButtonShowedByScroll;
    private boolean paused = true;
    private boolean pausedOnLastMessage;
    private String pendingLinkSearchString;
    private ChatActivityMemberRequestsDelegate pendingRequestsDelegate;
    private ArrayList<MessageObject> pendingSendMessages = new ArrayList<>();
    private SparseArray<MessageObject> pendingSendMessagesDict = new SparseArray<>();
    private PhotoViewer.PhotoViewerProvider photoViewerProvider = new PhotoViewer.EmptyPhotoViewerProvider() { // from class: org.telegram.ui.ChatActivity.4
        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
        public PhotoViewer.PlaceProviderObject getPlaceForPhoto(MessageObject messageObject, TLRPC$FileLocation tLRPC$FileLocation, int i, boolean z) {
            return ChatActivity.this.getPlaceForPhoto(messageObject, tLRPC$FileLocation, z, false);
        }
    };
    private int pinBullerinTag;
    private Bulletin pinBulletin;
    private PinchToZoomHelper pinchToZoomHelper;
    private NumberTextView pinnedCounterTextView;
    private int pinnedCounterTextViewX;
    private boolean pinnedEndReached;
    private int pinnedImageCacheType;
    private TLRPC$PhotoSize pinnedImageLocation;
    private TLObject pinnedImageLocationObject;
    private int pinnedImageSize;
    private TLRPC$PhotoSize pinnedImageThumbLocation;
    private PinnedLineView pinnedLineView;
    private AnimatorSet pinnedListAnimator;
    private ImageView pinnedListButton;
    private PinnedMessageButton[] pinnedMessageButton = new PinnedMessageButton[2];
    private boolean pinnedMessageButtonShown = false;
    private float pinnedMessageEnterOffset;
    private ArrayList<Integer> pinnedMessageIds = new ArrayList<>();
    private BackupImageView[] pinnedMessageImageView = new BackupImageView[2];
    private HashMap<Integer, MessageObject> pinnedMessageObjects = new HashMap<>();
    private SimpleTextView[] pinnedMessageTextView = new SimpleTextView[2];
    private BlurredFrameLayout pinnedMessageView;
    private AnimatorSet pinnedMessageViewAnimator;
    private TrackingWidthSimpleTextView[] pinnedNameTextView = new TrackingWidthSimpleTextView[2];
    private AnimatorSet[] pinnedNextAnimation = new AnimatorSet[2];
    private RadialProgressView pinnedProgress;
    private boolean pinnedProgressIsShowing;
    private ChatMessageCell pollHintCell;
    private HintView pollHintView;
    private int pollHintX;
    private int pollHintY;
    private LongSparseArray<ArrayList<MessageObject>> polls = new LongSparseArray<>();
    private ArrayList<MessageObject> pollsToCheck = new ArrayList<>(10);
    private NotificationCenter.PostponeNotificationCallback postponeNotificationsWhileLoadingCallback = new NotificationCenter.PostponeNotificationCallback() { // from class: org.telegram.ui.ChatActivity.3
        @Override // org.telegram.messenger.NotificationCenter.PostponeNotificationCallback
        public boolean needPostpone(int i, int i2, Object[] objArr) {
            if (i == NotificationCenter.didReceiveNewMessages) {
                long longValue = ((Long) objArr[0]).longValue();
                if (ChatActivity.this.firstLoading && longValue == ChatActivity.this.dialog_id) {
                    return true;
                }
            }
            return false;
        }
    };
    private final DialogInterface.OnCancelListener postponedScrollCancelListener = new DialogInterface.OnCancelListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda19
        @Override // android.content.DialogInterface.OnCancelListener
        public final void onCancel(DialogInterface dialogInterface) {
            ChatActivity.this.lambda$new$1(dialogInterface);
        }
    };
    private boolean postponedScrollIsCanceled;
    private int postponedScrollMessageId;
    private int postponedScrollMinMessageId;
    private int postponedScrollToLastMessageQueryIndex;
    private TLRPC$Document preloadedGreetingsSticker;
    private boolean premiumInvoiceBot;
    private int prevSetUnreadCount = Integer.MIN_VALUE;
    private RadialProgressView progressBar;
    private AlertDialog progressDialog;
    private FrameLayout progressView;
    private View progressView2;
    private float pullingBottomOffset;
    private float pullingDownAnimateProgress;
    private ChatActivity pullingDownAnimateToActivity;
    private Animator pullingDownBackAnimator;
    private ChatPullingDownDrawable pullingDownDrawable;
    private float pullingDownOffset;
    private ValueAnimator reactionsMentionButtonAnimation;
    private float reactionsMentionButtonEnterProgress;
    private int reactionsMentionCount;
    private FrameLayout reactionsMentiondownButton;
    private CounterView reactionsMentiondownButtonCounter;
    private ImageView reactionsMentiondownButtonImage;
    private ArrayList<MessageObject> reactionsToCheck = new ArrayList<>(10);
    private SparseArray<MessageObject> repliesMessagesDict = new SparseArray<>();
    private TextView replyButton;
    private AnimatorSet replyButtonAnimation;
    private ImageView replyCloseImageView;
    private ImageView replyIconImageView;
    private int replyImageCacheType;
    private TLRPC$PhotoSize replyImageLocation;
    private TLObject replyImageLocationObject;
    private int replyImageSize;
    private TLRPC$PhotoSize replyImageThumbLocation;
    private BackupImageView replyImageView;
    private View replyLineView;
    private int replyMaxReadId;
    private MessageObject replyMessageHeaderObject;
    private SparseArray<ArrayList<Integer>> replyMessageOwners = new SparseArray<>();
    private SimpleTextView replyNameTextView;
    private SimpleTextView replyObjectHintTextView;
    private SimpleTextView replyObjectTextView;
    private TLRPC$Chat replyOriginalChat;
    private int replyOriginalMessageId;
    private MessageObject replyingMessageObject;
    private TextView reportSpamButton;
    private AnimatorSet reportSpamViewAnimator;
    private int reportType = -1;
    private int returnToLoadIndex;
    private int returnToMessageId;
    private AnimatorSet runningAnimation;
    private int runningAnimationIndex = -1;
    private TLRPC$TL_messages_discussionMessage savedDiscussionMessage;
    private TLRPC$messages_Messages savedHistory;
    private boolean savedNoDiscussion;
    private boolean savedNoHistory;
    private int scheduledMessagesCount = -1;
    private HintView scheduledOrNoSoundHint;
    private AnimatorSet scrimAnimatorSet;
    private Paint scrimPaint;
    private float scrimPaintAlpha = 0.0f;
    public ActionBarPopupWindow scrimPopupWindow;
    private boolean scrimPopupWindowHideDimOnDismiss = true;
    private ActionBarMenuSubItem[] scrimPopupWindowItems;
    private int scrimPopupX;
    private int scrimPopupY;
    private View scrimView;
    private float scrimViewAlpha = 1.0f;
    private ValueAnimator scrimViewAlphaAnimator;
    private String scrimViewReaction;
    private int scrollAnimationIndex;
    private boolean scrollByTouch;
    private int scrollCallbackAnimationIndex;
    private MessageObject scrollToMessage;
    private int scrollToMessagePosition = -10000;
    private int scrollToOffsetOnRecreate = 0;
    private int scrollToPositionOnRecreate = -1;
    private boolean scrollToThreadMessage;
    private boolean scrollToTopOnResume;
    private boolean scrollToTopUnReadOnResume;
    private boolean scrollToVideo;
    private boolean scrollingChatListView;
    private boolean scrollingFloatingDate;
    private HintView searchAsListHint;
    private boolean searchAsListHintShown;
    private View searchAsListTogglerView;
    private ImageView searchCalendarButton;
    private BlurredFrameLayout searchContainer;
    private SearchCounterView searchCountText;
    private ImageView searchDownButton;
    private ValueAnimator searchExpandAnimator;
    private float searchExpandProgress;
    private ActionBarMenuItem searchIconItem;
    private ActionBarMenuItem searchItem;
    private boolean searchItemVisible;
    private ImageView searchUpButton;
    private ImageView searchUserButton;
    private TLRPC$Chat searchingChatMessages;
    private boolean searchingForUser;
    private TLRPC$User searchingUserMessages;
    private SparseArray<MessageObject>[] selectedMessagesCanCopyIds = {new SparseArray<>(), new SparseArray<>()};
    private SparseArray<MessageObject>[] selectedMessagesCanStarIds = {new SparseArray<>(), new SparseArray<>()};
    private NumberTextView selectedMessagesCountTextView;
    private SparseArray<MessageObject>[] selectedMessagesIds = {new SparseArray<>(), new SparseArray<>()};
    private MessageObject selectedObject;
    private MessageObject.GroupedMessages selectedObjectGroup;
    private MessageObject selectedObjectToEditCaption;
    private TLRPC$TL_channels_sendAsPeers sendAsPeersObj;
    private boolean setPinnedTextTranslationX;
    private boolean showAudioCallAsIcon;
    private boolean showCloseChatDialogLater;
    private boolean showPinBulletin;
    private final Runnable showScheduledOrNoSoundRunnable = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda128
        @Override // java.lang.Runnable
        public final void run() {
            ChatActivity.this.lambda$new$5();
        }
    };
    private boolean showScrollToMessageError;
    private boolean showSearchAsIcon;
    private boolean showTapForForwardingOptionsHit;
    private ChatMessageCell slidingView;
    private HintView slowModeHint;
    private boolean sponsoredMessagesAdded;
    private int startFromVideoMessageId;
    private int startFromVideoTimestamp = -1;
    private int startLoadFromDate;
    private int startLoadFromMessageId;
    private int startLoadFromMessageOffset = ConnectionsManager.DEFAULT_DATACENTER_ID;
    private String startVideoEdit;
    private boolean startedTrackingSlidingView;
    private StickersAdapter stickersAdapter;
    private RecyclerListView stickersListView;
    private RecyclerListView.OnItemClickListener stickersOnItemClickListener;
    private FrameLayout stickersPanel;
    private ImageView stickersPanelArrow;
    private boolean swipeBackEnabled = true;
    private Runnable tapForForwardingOptionsHitRunnable;
    private TextSelectionHelper.ChatListTextSelectionHelper textSelectionHelper;
    private TextSelectionHint textSelectionHint;
    private boolean textSelectionHintWasShowed;
    public ThemeDelegate themeDelegate;
    private int threadMaxInboxReadId;
    private int threadMaxOutboxReadId;
    private boolean threadMessageAdded;
    private int threadMessageId;
    private MessageObject threadMessageObject;
    private ArrayList<MessageObject> threadMessageObjects;
    private boolean threadMessageVisible = true;
    private int threadUnreadMessagesCount;
    private View timeItem2;
    private HintView timerHintView;
    private boolean toPullingDownTransition;
    private BlurredFrameLayout topChatPanelView;
    private float topChatPanelViewOffset;
    private UndoView topUndoView;
    private float topViewOffset;
    private int topViewWasVisible;
    private int totalPinnedMessagesCount;
    private int transitionAnimationIndex;
    private UndoView undoView;
    private MessageObject unreadMessageObject;
    private Runnable unregisterFlagSecureNoforwards;
    private Runnable unregisterFlagSecurePasscode;
    private Runnable unselectRunnable;
    private Runnable updateDeleteItemRunnable = new Runnable() { // from class: org.telegram.ui.ChatActivity.1
        @Override // java.lang.Runnable
        public void run() {
            String str;
            if (ChatActivity.this.selectedObject != null && ChatActivity.this.menuDeleteItem != null) {
                int max = Math.max(0, ChatActivity.this.selectedObject.messageOwner.ttl_period - (ChatActivity.this.getConnectionsManager().getCurrentTime() - ChatActivity.this.selectedObject.messageOwner.date));
                if (max < 86400) {
                    str = AndroidUtilities.formatDuration(max, false);
                } else {
                    str = LocaleController.formatPluralString("Days", Math.round(((float) max) / 86400.0f), new Object[0]);
                }
                ChatActivity.this.menuDeleteItem.setSubtext(LocaleController.formatString("AutoDeleteIn", R.string.AutoDeleteIn, str));
                AndroidUtilities.runOnUIThread(ChatActivity.this.updateDeleteItemRunnable, 1000);
            }
        }
    };
    Runnable updatePinnedProgressRunnable;
    Runnable updateReactionRunnable;
    private boolean userBlocked;
    protected TLRPC$UserFull userInfo;
    private FrameLayout videoPlayerContainer;
    private TextureView videoTextureView;
    private String voiceChatHash;
    private HintView voiceHintTextView;
    private Runnable waitingForCharaterEnterRunnable;
    private ArrayList<Integer> waitingForLoad = new ArrayList<>();
    private SparseArray<MessageObject> waitingForReplies = new SparseArray<>();
    private boolean waitingForReplyMessageLoad;
    private boolean waitingForSendingMessageLoad;
    private boolean wasManualScroll;
    private boolean wasPaused;

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public interface ChatActivityDelegate {

        /* renamed from: org.telegram.ui.ChatActivity$ChatActivityDelegate$-CC  reason: invalid class name */
        /* loaded from: classes3.dex */
        public final /* synthetic */ class CC {
            public static void $default$onReport(ChatActivityDelegate chatActivityDelegate) {
            }

            public static void $default$onUnpin(ChatActivityDelegate chatActivityDelegate, boolean z, boolean z2) {
            }

            public static void $default$openReplyMessage(ChatActivityDelegate chatActivityDelegate, int i) {
            }

            public static void $default$openSearch(ChatActivityDelegate chatActivityDelegate, String str) {
            }
        }

        void onReport();

        void onUnpin(boolean z, boolean z2);

        void openReplyMessage(int i);

        void openSearch(String str);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$createView$23(View view, MotionEvent motionEvent) {
        return true;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$createView$24(View view, MotionEvent motionEvent) {
        return true;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$createView$56(View view, MotionEvent motionEvent) {
        return true;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$markSponsoredAsRead$245(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean lambda$showChatThemeBottomSheet$248(MotionEvent motionEvent) {
        return true;
    }

    @Override // org.telegram.ui.Components.ChatAttachAlertDocumentLayout.DocumentSelectActivityDelegate
    public /* synthetic */ void startMusicSelectActivity() {
        ChatAttachAlertDocumentLayout.DocumentSelectActivityDelegate.CC.$default$startMusicSelectActivity(this);
    }

    static /* synthetic */ float access$12116(ChatActivity chatActivity, float f) {
        float f2 = chatActivity.pullingDownOffset + f;
        chatActivity.pullingDownOffset = f2;
        return f2;
    }

    static /* synthetic */ int access$24010(ChatActivity chatActivity) {
        int i = chatActivity.newMentionsCount;
        chatActivity.newMentionsCount = i - 1;
        return i;
    }

    static /* synthetic */ int access$26908(ChatActivity chatActivity) {
        int i = chatActivity.scheduledMessagesCount;
        chatActivity.scheduledMessagesCount = i + 1;
        return i;
    }

    static /* synthetic */ int access$26912(ChatActivity chatActivity, int i) {
        int i2 = chatActivity.scheduledMessagesCount + i;
        chatActivity.scheduledMessagesCount = i2;
        return i2;
    }

    static /* synthetic */ int access$31804(ChatActivity chatActivity) {
        int i = chatActivity.pinBullerinTag + 1;
        chatActivity.pinBullerinTag = i;
        return i;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$new$1(DialogInterface dialogInterface) {
        this.postponedScrollIsCanceled = true;
        this.postponedScrollMessageId = 0;
        this.nextScrollToMessageId = 0;
        this.forceNextPinnedMessageId = 0;
        invalidateMessagesVisiblePart();
        showPinnedProgress(false);
    }

    public void deleteHistory(int i, int i2, boolean z) {
        this.chatAdapter.frozenMessages.clear();
        for (int i3 = 0; i3 < this.messages.size(); i3++) {
            MessageObject messageObject = this.messages.get(i3);
            int i4 = messageObject.messageOwner.date;
            if (i4 <= i || i4 >= i2) {
                this.chatAdapter.frozenMessages.add(messageObject);
            }
        }
        RecyclerListView recyclerListView = this.chatListView;
        if (recyclerListView != null) {
            recyclerListView.setEmptyView(null);
        }
        if (this.chatAdapter.frozenMessages.isEmpty()) {
            showProgressView(true);
        }
        ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
        chatActivityAdapter.isFrozen = true;
        chatActivityAdapter.notifyDataSetChanged(true);
        getUndoView().showWithAction(this.dialog_id, 81, new Runnable(i, i2, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda165
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$deleteHistory$3(this.f$1, this.f$2, this.f$3);
            }
        }, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda130
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$deleteHistory$4();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$deleteHistory$3(int i, int i2, boolean z) {
        getMessagesController().deleteMessagesRange(this.dialog_id, ChatObject.isChannel(this.currentChat) ? this.dialog_id : 0, i, i2, z, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda137
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$deleteHistory$2();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$deleteHistory$2() {
        this.chatAdapter.frozenMessages.clear();
        ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
        chatActivityAdapter.isFrozen = false;
        chatActivityAdapter.notifyDataSetChanged(true);
        showProgressView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$deleteHistory$4() {
        this.chatAdapter.frozenMessages.clear();
        ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
        chatActivityAdapter.isFrozen = false;
        chatActivityAdapter.notifyDataSetChanged(true);
        showProgressView(false);
    }

    public void showHeaderItem(boolean z) {
        if (!z) {
            ActionBarMenuItem actionBarMenuItem = this.attachItem;
            if (actionBarMenuItem != null) {
                actionBarMenuItem.setVisibility(8);
            }
            ActionBarMenuItem actionBarMenuItem2 = this.headerItem;
            if (actionBarMenuItem2 != null) {
                actionBarMenuItem2.setVisibility(8);
            }
        } else if (!this.chatActivityEnterView.hasText() || !TextUtils.isEmpty(this.chatActivityEnterView.getSlowModeTimer())) {
            ActionBarMenuItem actionBarMenuItem3 = this.attachItem;
            if (actionBarMenuItem3 != null) {
                actionBarMenuItem3.setVisibility(8);
            }
            ActionBarMenuItem actionBarMenuItem4 = this.headerItem;
            if (actionBarMenuItem4 != null) {
                actionBarMenuItem4.setVisibility(0);
            }
        } else {
            ActionBarMenuItem actionBarMenuItem5 = this.attachItem;
            if (actionBarMenuItem5 != null) {
                actionBarMenuItem5.setVisibility(0);
            }
            ActionBarMenuItem actionBarMenuItem6 = this.headerItem;
            if (actionBarMenuItem6 != null) {
                actionBarMenuItem6.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public class UnreadCounterTextView extends View {
        boolean animatedFromBottom;
        private int circleWidth;
        int counterColor;
        private String currentCounterString;
        CharSequence lastText;
        private TextPaint layoutPaint = new TextPaint(1);
        private int layoutTextWidth;
        private Paint paint = new Paint(1);
        int panelBackgroundColor;
        private RectF rect = new RectF();
        ValueAnimator replaceAnimator;
        float replaceProgress = 1.0f;
        private int rippleColor;
        Drawable selectableBackground;
        int textColor;
        private StaticLayout textLayout;
        private StaticLayout textLayoutOut;
        private TextPaint textPaint = new TextPaint(1);
        private int textWidth;

        public UnreadCounterTextView(Context context) {
            super(context);
            this.textPaint.setTextSize((float) AndroidUtilities.dp(13.0f));
            this.textPaint.setTypeface(AndroidUtilities.getTypeface(AndroidUtilities.TYPEFACE_ROBOTO_MEDIUM));
            this.layoutPaint.setTextSize((float) AndroidUtilities.dp(15.0f));
            this.layoutPaint.setTypeface(AndroidUtilities.getTypeface(AndroidUtilities.TYPEFACE_ROBOTO_MEDIUM));
        }

        public void setText(CharSequence charSequence, boolean z) {
            if (this.lastText != charSequence) {
                this.lastText = charSequence;
                this.animatedFromBottom = z;
                this.textLayoutOut = this.textLayout;
                this.layoutTextWidth = (int) Math.ceil((double) this.layoutPaint.measureText(charSequence, 0, charSequence.length()));
                this.textLayout = new StaticLayout(charSequence, this.layoutPaint, this.layoutTextWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
                setContentDescription(charSequence);
                invalidate();
                if (this.textLayoutOut != null) {
                    ValueAnimator valueAnimator = this.replaceAnimator;
                    if (valueAnimator != null) {
                        valueAnimator.cancel();
                    }
                    this.replaceProgress = 0.0f;
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                    this.replaceAnimator = ofFloat;
                    ofFloat.addUpdateListener(new ChatActivity$UnreadCounterTextView$$ExternalSyntheticLambda0(this));
                    this.replaceAnimator.setDuration(150L);
                    this.replaceAnimator.start();
                }
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$setText$0(ValueAnimator valueAnimator) {
            this.replaceProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            invalidate();
        }

        public void setText(CharSequence charSequence) {
            this.layoutTextWidth = (int) Math.ceil((double) this.layoutPaint.measureText(charSequence, 0, charSequence.length()));
            this.textLayout = new StaticLayout(charSequence, this.layoutPaint, this.layoutTextWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
            setContentDescription(charSequence);
            invalidate();
        }

        @Override // android.view.View
        protected void drawableStateChanged() {
            super.drawableStateChanged();
            Drawable drawable = this.selectableBackground;
            if (drawable != null) {
                drawable.setState(getDrawableState());
            }
        }

        @Override // android.view.View
        public boolean verifyDrawable(Drawable drawable) {
            Drawable drawable2 = this.selectableBackground;
            if (drawable2 != null) {
                return drawable2 == drawable || super.verifyDrawable(drawable);
            }
            return super.verifyDrawable(drawable);
        }

        @Override // android.view.View
        public void jumpDrawablesToCurrentState() {
            super.jumpDrawablesToCurrentState();
            Drawable drawable = this.selectableBackground;
            if (drawable != null) {
                drawable.jumpToCurrentState();
            }
        }

        @Override // android.view.View
        public boolean onTouchEvent(MotionEvent motionEvent) {
            StaticLayout staticLayout;
            int i;
            if (motionEvent.getAction() == 0 && (staticLayout = this.textLayout) != null) {
                int ceil = (int) Math.ceil((double) staticLayout.getLineWidth(0));
                if (getMeasuredWidth() == ((View) getParent()).getMeasuredWidth()) {
                    i = getMeasuredWidth() - AndroidUtilities.dp(96.0f);
                } else if (ChatActivity.this.botInfo != null) {
                    i = getMeasuredWidth();
                } else {
                    int i2 = this.circleWidth;
                    i = ceil + (i2 > 0 ? i2 + AndroidUtilities.dp(8.0f) : 0) + AndroidUtilities.dp(48.0f);
                }
                int measuredWidth = (getMeasuredWidth() - i) / 2;
                float f = ((float) i) / 2.0f;
                this.rect.set((float) measuredWidth, (((float) getMeasuredHeight()) / 2.0f) - f, (float) (measuredWidth + i), (((float) getMeasuredHeight()) / 2.0f) + f);
                if (!this.rect.contains(motionEvent.getX(), motionEvent.getY())) {
                    setPressed(false);
                    return false;
                }
            }
            return super.onTouchEvent(motionEvent);
        }

        @Override // android.view.View
        protected void onDraw(Canvas canvas) {
            StaticLayout staticLayout = this.textLayout;
            int themedColor = ChatActivity.this.getThemedColor(isEnabled() ? "chat_fieldOverlayText" : "windowBackgroundWhiteGrayText");
            if (this.textColor != themedColor) {
                TextPaint textPaint = this.layoutPaint;
                this.textColor = themedColor;
                textPaint.setColor(themedColor);
            }
            int themedColor2 = ChatActivity.this.getThemedColor("chat_messagePanelBackground");
            if (this.panelBackgroundColor != themedColor2) {
                TextPaint textPaint2 = this.textPaint;
                this.panelBackgroundColor = themedColor2;
                textPaint2.setColor(themedColor2);
            }
            int themedColor3 = ChatActivity.this.getThemedColor("chat_goDownButtonCounterBackground");
            if (this.counterColor != themedColor3) {
                Paint paint = this.paint;
                this.counterColor = themedColor3;
                paint.setColor(themedColor3);
            }
            if (getParent() != null) {
                int measuredWidth = getMeasuredWidth();
                int measuredWidth2 = (getMeasuredWidth() - measuredWidth) / 2;
                if (this.rippleColor != ChatActivity.this.getThemedColor("chat_fieldOverlayText") || this.selectableBackground == null) {
                    int dp = AndroidUtilities.dp(60.0f);
                    int themedColor4 = ChatActivity.this.getThemedColor("chat_fieldOverlayText");
                    this.rippleColor = themedColor4;
                    Drawable createSimpleSelectorCircleDrawable = Theme.createSimpleSelectorCircleDrawable(dp, 0, ColorUtils.setAlphaComponent(themedColor4, 26));
                    this.selectableBackground = createSimpleSelectorCircleDrawable;
                    createSimpleSelectorCircleDrawable.setCallback(this);
                }
                int dp2 = getLeft() + measuredWidth2 <= 0 ? measuredWidth2 - AndroidUtilities.dp(20.0f) : measuredWidth2;
                int i = measuredWidth2 + measuredWidth;
                if (i > ((View) getParent()).getMeasuredWidth()) {
                    i += AndroidUtilities.dp(20.0f);
                }
                int i2 = measuredWidth / 2;
                this.selectableBackground.setBounds(dp2, (getMeasuredHeight() / 2) - i2, i, (getMeasuredHeight() / 2) + i2);
                this.selectableBackground.draw(canvas);
            }
            if (this.textLayout != null) {
                canvas.save();
                if (this.replaceProgress == 1.0f || this.textLayoutOut == null) {
                    canvas.translate((float) (((getMeasuredWidth() - this.layoutTextWidth) / 2) - (this.circleWidth / 2)), (float) ((getMeasuredHeight() - this.textLayout.getHeight()) / 2));
                    this.textLayout.draw(canvas);
                } else {
                    int alpha = this.layoutPaint.getAlpha();
                    canvas.save();
                    canvas.translate((float) (((getMeasuredWidth() - this.textLayoutOut.getWidth()) / 2) - (this.circleWidth / 2)), (float) ((getMeasuredHeight() - this.textLayout.getHeight()) / 2));
                    float f = -1.0f;
                    canvas.translate(0.0f, (this.animatedFromBottom ? -1.0f : 1.0f) * ((float) AndroidUtilities.dp(18.0f)) * this.replaceProgress);
                    float f2 = (float) alpha;
                    this.layoutPaint.setAlpha((int) ((1.0f - this.replaceProgress) * f2));
                    this.textLayoutOut.draw(canvas);
                    canvas.restore();
                    canvas.save();
                    canvas.translate((float) (((getMeasuredWidth() - this.layoutTextWidth) / 2) - (this.circleWidth / 2)), (float) ((getMeasuredHeight() - this.textLayout.getHeight()) / 2));
                    if (this.animatedFromBottom) {
                        f = 1.0f;
                    }
                    canvas.translate(0.0f, f * ((float) AndroidUtilities.dp(18.0f)) * (1.0f - this.replaceProgress));
                    this.layoutPaint.setAlpha((int) (f2 * this.replaceProgress));
                    this.textLayout.draw(canvas);
                    canvas.restore();
                    this.layoutPaint.setAlpha(alpha);
                }
                canvas.restore();
            }
            if (this.currentCounterString != null && staticLayout != null) {
                int ceil = (int) Math.ceil((double) staticLayout.getLineWidth(0));
                int measuredWidth3 = ((((getMeasuredWidth() - ceil) / 2) + ceil) - (this.circleWidth / 2)) + AndroidUtilities.dp(6.0f);
                this.rect.set((float) measuredWidth3, (float) ((getMeasuredHeight() / 2) - AndroidUtilities.dp(10.0f)), (float) (measuredWidth3 + this.circleWidth), (float) ((getMeasuredHeight() / 2) + AndroidUtilities.dp(10.0f)));
                canvas.drawRoundRect(this.rect, (float) AndroidUtilities.dp(10.0f), (float) AndroidUtilities.dp(10.0f), this.paint);
                canvas.drawText(this.currentCounterString, this.rect.centerX() - (((float) this.textWidth) / 2.0f), this.rect.top + ((float) AndroidUtilities.dp(14.5f)), this.textPaint);
            }
        }
    }

    /* access modifiers changed from: private */
    public void startMultiselect(int i) {
        int i2 = i - this.chatAdapter.messagesStartRow;
        if (i2 >= 0 && i2 < this.messages.size()) {
            MessageObject messageObject = this.messages.get(i2);
            final boolean z = this.selectedMessagesIds[0].get(messageObject.getId(), null) == null && this.selectedMessagesIds[1].get(messageObject.getId(), null) == null;
            final SparseArray sparseArray = new SparseArray();
            for (int i3 = 0; i3 < this.selectedMessagesIds[0].size(); i3++) {
                sparseArray.put(this.selectedMessagesIds[0].keyAt(i3), this.selectedMessagesIds[0].valueAt(i3));
            }
            for (int i4 = 0; i4 < this.selectedMessagesIds[1].size(); i4++) {
                sparseArray.put(this.selectedMessagesIds[1].keyAt(i4), this.selectedMessagesIds[1].valueAt(i4));
            }
            this.chatListView.startMultiselect(i, false, new RecyclerListView.onMultiSelectionChanged() { // from class: org.telegram.ui.ChatActivity.7
                boolean limitReached;

                @Override // org.telegram.ui.Components.RecyclerListView.onMultiSelectionChanged
                public void onSelectionChanged(int i5, boolean z2, float f, float f2) {
                    int i6 = i5 - ChatActivity.this.chatAdapter.messagesStartRow;
                    if (z) {
                        z2 = !z2;
                    }
                    if (i6 >= 0 && i6 < ChatActivity.this.messages.size()) {
                        MessageObject messageObject2 = ChatActivity.this.messages.get(i6);
                        if (z2 && (ChatActivity.this.selectedMessagesIds[0].indexOfKey(messageObject2.getId()) >= 0 || ChatActivity.this.selectedMessagesIds[1].indexOfKey(messageObject2.getId()) >= 0)) {
                            return;
                        }
                        if ((z2 || ChatActivity.this.selectedMessagesIds[0].indexOfKey(messageObject2.getId()) >= 0 || ChatActivity.this.selectedMessagesIds[1].indexOfKey(messageObject2.getId()) >= 0) && messageObject2.contentType == 0) {
                            if (!z2 || ChatActivity.this.selectedMessagesIds[0].size() + ChatActivity.this.selectedMessagesIds[1].size() < 100) {
                                this.limitReached = false;
                            } else {
                                this.limitReached = true;
                            }
                            RecyclerView.ViewHolder findViewHolderForAdapterPosition = ChatActivity.this.chatListView.findViewHolderForAdapterPosition(i5);
                            if (findViewHolderForAdapterPosition != null) {
                                View view = findViewHolderForAdapterPosition.itemView;
                                if (view instanceof ChatMessageCell) {
                                    ChatActivity.this.processRowSelect(view, false, f, f2);
                                    return;
                                }
                            }
                            ChatActivity.this.addToSelectedMessages(messageObject2, false);
                            ChatActivity.this.updateActionModeTitle();
                            ChatActivity.this.updateVisibleRows();
                        }
                    }
                }

                @Override // org.telegram.ui.Components.RecyclerListView.onMultiSelectionChanged
                public boolean canSelect(int i5) {
                    int i6 = i5 - ChatActivity.this.chatAdapter.messagesStartRow;
                    if (i6 < 0 || i6 >= ChatActivity.this.messages.size()) {
                        return false;
                    }
                    MessageObject messageObject2 = ChatActivity.this.messages.get(i6);
                    if (messageObject2.contentType != 0) {
                        return false;
                    }
                    if (!z && sparseArray.get(messageObject2.getId(), null) == null) {
                        return true;
                    }
                    if (!z || sparseArray.get(messageObject2.getId(), null) == null) {
                        return false;
                    }
                    return true;
                }

                @Override // org.telegram.ui.Components.RecyclerListView.onMultiSelectionChanged
                public int checkPosition(int i5, boolean z2) {
                    MessageObject.GroupedMessages groupedMessages;
                    int i6 = i5 - ChatActivity.this.chatAdapter.messagesStartRow;
                    if (i6 >= 0 && i6 < ChatActivity.this.messages.size()) {
                        MessageObject messageObject2 = ChatActivity.this.messages.get(i6);
                        if (messageObject2.contentType == 0 && messageObject2.hasValidGroupId() && (groupedMessages = (MessageObject.GroupedMessages) ChatActivity.this.groupedMessagesMap.get(messageObject2.getGroupId())) != null) {
                            ArrayList<MessageObject> arrayList = groupedMessages.messages;
                            return ChatActivity.this.chatAdapter.messagesStartRow + ChatActivity.this.messages.indexOf(arrayList.get(z2 ? 0 : arrayList.size() - 1));
                        }
                    }
                    return i5;
                }

                @Override // org.telegram.ui.Components.RecyclerListView.onMultiSelectionChanged
                public boolean limitReached() {
                    return this.limitReached;
                }

                @Override // org.telegram.ui.Components.RecyclerListView.onMultiSelectionChanged
                public void getPaddings(int[] iArr) {
                    iArr[0] = (int) ChatActivity.this.chatListViewPaddingTop;
                    iArr[1] = ChatActivity.this.blurredViewBottomOffset;
                }

                @Override // org.telegram.ui.Components.RecyclerListView.onMultiSelectionChanged
                public void scrollBy(int i5) {
                    ChatActivity.this.chatListView.scrollBy(0, i5);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$new$5() {
        ChatActivityEnterView chatActivityEnterView;
        View sendButton;
        if (getParentActivity() != null && this.fragmentView != null && (chatActivityEnterView = this.chatActivityEnterView) != null && (sendButton = chatActivityEnterView.getSendButton()) != null && this.chatActivityEnterView.getEditField().getText().length() >= 5) {
            SharedConfig.increaseScheduledOrNoSuoundHintShowed();
            if (this.scheduledOrNoSoundHint == null) {
                HintView hintView = new HintView(getParentActivity(), 4, this.themeDelegate);
                this.scheduledOrNoSoundHint = hintView;
                hintView.setShowingDuration(5000);
                this.scheduledOrNoSoundHint.setAlpha(0.0f);
                this.scheduledOrNoSoundHint.setVisibility(4);
                this.scheduledOrNoSoundHint.setText(LocaleController.getString("ScheduledOrNoSoundHint", R.string.ScheduledOrNoSoundHint));
                this.contentView.addView(this.scheduledOrNoSoundHint, LayoutHelper.createFrame(-2, -2.0f, 51, 10.0f, 0.0f, 10.0f, 0.0f));
            }
            this.scheduledOrNoSoundHint.showForView(sendButton, true);
        }
    }

    public ChatActivity(Bundle bundle) {
        super(bundle);
    }

    /* JADX WARNING: Removed duplicated region for block: B:163:0x0766  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0777  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x078d  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x07c8  */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x091f  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x09e1  */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0a12  */
    @Override // org.telegram.ui.ActionBar.BaseFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onFragmentCreate() {
        /*
        // Method dump skipped, instructions count: 2613
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.onFragmentCreate():boolean");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$6(MessagesStorage messagesStorage, long j, CountDownLatch countDownLatch) {
        this.currentChat = messagesStorage.getChat(j);
        countDownLatch.countDown();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$7(MessagesStorage messagesStorage, long j, CountDownLatch countDownLatch) {
        this.currentUser = messagesStorage.getUser(j);
        countDownLatch.countDown();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$8(MessagesStorage messagesStorage, int i, CountDownLatch countDownLatch) {
        this.currentEncryptedChat = messagesStorage.getEncryptedChat((long) i);
        countDownLatch.countDown();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$9(MessagesStorage messagesStorage, CountDownLatch countDownLatch) {
        this.currentUser = messagesStorage.getUser(this.currentEncryptedChat.user_id);
        countDownLatch.countDown();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$12() {
        this.chatInviteRunnable = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
        if (!ChatObject.isChannel(this.currentChat) || this.currentChat.megagroup) {
            builder.setMessage(LocaleController.getString("JoinByPeekGroupText", R.string.JoinByPeekGroupText));
            builder.setTitle(LocaleController.getString("JoinByPeekGroupTitle", R.string.JoinByPeekGroupTitle));
        } else {
            builder.setMessage(LocaleController.getString("JoinByPeekChannelText", R.string.JoinByPeekChannelText));
            builder.setTitle(LocaleController.getString("JoinByPeekChannelTitle", R.string.JoinByPeekChannelTitle));
        }
        builder.setPositiveButton(LocaleController.getString("JoinByPeekJoin", R.string.JoinByPeekJoin), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda27
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatActivity.this.lambda$onFragmentCreate$10(dialogInterface, i);
            }
        });
        builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda29
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatActivity.this.lambda$onFragmentCreate$11(dialogInterface, i);
            }
        });
        showDialog(builder.create());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$10(DialogInterface dialogInterface, int i) {
        UnreadCounterTextView unreadCounterTextView = this.bottomOverlayChatText;
        if (unreadCounterTextView != null) {
            unreadCounterTextView.callOnClick();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onFragmentCreate$11(DialogInterface dialogInterface, int i) {
        finishFragment();
    }

    private void fillInviterId(boolean z) {
        TLRPC$Chat tLRPC$Chat = this.currentChat;
        if (!(tLRPC$Chat == null || this.chatInfo == null || ChatObject.isNotInChat(tLRPC$Chat) || this.currentChat.creator)) {
            TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
            long j = tLRPC$ChatFull.inviterId;
            if (j != 0) {
                this.chatInviterId = j;
                return;
            }
            TLRPC$ChatParticipants tLRPC$ChatParticipants = tLRPC$ChatFull.participants;
            if (tLRPC$ChatParticipants != null) {
                TLRPC$ChatParticipant tLRPC$ChatParticipant = tLRPC$ChatParticipants.self_participant;
                if (tLRPC$ChatParticipant != null) {
                    this.chatInviterId = tLRPC$ChatParticipant.inviter_id;
                    return;
                }
                long clientUserId = getUserConfig().getClientUserId();
                int size = this.chatInfo.participants.participants.size();
                for (int i = 0; i < size; i++) {
                    TLRPC$ChatParticipant tLRPC$ChatParticipant2 = this.chatInfo.participants.participants.get(i);
                    if (tLRPC$ChatParticipant2.user_id == clientUserId) {
                        this.chatInviterId = tLRPC$ChatParticipant2.inviter_id;
                        return;
                    }
                }
            }
            if (z && this.chatInviterId == 0) {
                getMessagesController().checkChatInviter(this.currentChat.id, false);
            }
        }
    }

    private void hideUndoViews() {
        UndoView undoView = this.undoView;
        if (undoView != null) {
            undoView.hide(true, 0);
        }
        Bulletin bulletin = this.pinBulletin;
        if (bulletin != null) {
            bulletin.hide(false, 0L);
        }
        UndoView undoView2 = this.topUndoView;
        if (undoView2 != null) {
            undoView2.hide(true, 0);
        }
    }

    public int getOtherSameChatsDiff() {
        ArrayList<BaseFragment> arrayList;
        ActionBarLayout actionBarLayout = this.parentLayout;
        int i = 0;
        if (actionBarLayout == null || (arrayList = actionBarLayout.fragmentsStack) == null) {
            return 0;
        }
        int indexOf = arrayList.indexOf(this);
        if (indexOf == -1) {
            indexOf = this.parentLayout.fragmentsStack.size();
        }
        while (true) {
            if (i >= this.parentLayout.fragmentsStack.size()) {
                i = indexOf;
                break;
            }
            BaseFragment baseFragment = this.parentLayout.fragmentsStack.get(i);
            if (baseFragment != this && (baseFragment instanceof ChatActivity) && ((ChatActivity) baseFragment).dialog_id == this.dialog_id) {
                break;
            }
            i++;
        }
        return i - indexOf;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void onFragmentDestroy() {
        ArrayList<BaseFragment> arrayList;
        super.onFragmentDestroy();
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.onDestroy();
        }
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        if (chatAvatarContainer != null) {
            chatAvatarContainer.onDestroy();
        }
        MentionsContainerView mentionsContainerView = this.mentionContainer;
        if (!(mentionsContainerView == null || mentionsContainerView.getAdapter() == null)) {
            this.mentionContainer.getAdapter().onDestroy();
        }
        ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
        if (chatAttachAlert != null) {
            chatAttachAlert.dismissInternal();
        }
        getNotificationCenter().onAnimationFinish(this.transitionAnimationIndex);
        getNotificationCenter().onAnimationFinish(this.scrollAnimationIndex);
        getNotificationCenter().onAnimationFinish(this.scrollCallbackAnimationIndex);
        hideUndoViews();
        Runnable runnable = this.chatInviteRunnable;
        if (runnable != null) {
            AndroidUtilities.cancelRunOnUIThread(runnable);
            this.chatInviteRunnable = null;
        }
        getNotificationCenter().removePostponeNotificationsCallback(this.postponeNotificationsWhileLoadingCallback);
        getMessagesController().setLastCreatedDialogId(this.dialog_id, this.chatMode == 1, false);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagesDidLoad);
        NotificationCenter.getGlobalInstance().removeObserver(this, NotificationCenter.emojiLoaded);
        NotificationCenter.getGlobalInstance().removeObserver(this, NotificationCenter.invalidateMotionBackground);
        getNotificationCenter().removeObserver(this, NotificationCenter.didUpdateConnectionState);
        getNotificationCenter().removeObserver(this, NotificationCenter.updateInterfaces);
        getNotificationCenter().removeObserver(this, NotificationCenter.updateDefaultSendAsPeer);
        getNotificationCenter().removeObserver(this, NotificationCenter.didReceiveNewMessages);
        getNotificationCenter().removeObserver(this, NotificationCenter.closeChats);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagesRead);
        getNotificationCenter().removeObserver(this, NotificationCenter.threadMessagesRead);
        getNotificationCenter().removeObserver(this, NotificationCenter.commentsRead);
        getNotificationCenter().removeObserver(this, NotificationCenter.changeRepliesCounter);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagesDeleted);
        getNotificationCenter().removeObserver(this, NotificationCenter.historyCleared);
        getNotificationCenter().removeObserver(this, NotificationCenter.messageReceivedByServer);
        getNotificationCenter().removeObserver(this, NotificationCenter.messageReceivedByAck);
        getNotificationCenter().removeObserver(this, NotificationCenter.messageSendError);
        getNotificationCenter().removeObserver(this, NotificationCenter.chatInfoDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.didLoadChatInviter);
        getNotificationCenter().removeObserver(this, NotificationCenter.groupCallUpdated);
        getNotificationCenter().removeObserver(this, NotificationCenter.encryptedChatUpdated);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagesReadEncrypted);
        getNotificationCenter().removeObserver(this, NotificationCenter.removeAllMessagesFromDialog);
        getNotificationCenter().removeObserver(this, NotificationCenter.contactsDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagePlayingProgressDidChanged);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagePlayingDidReset);
        getNotificationCenter().removeObserver(this, NotificationCenter.screenshotTook);
        getNotificationCenter().removeObserver(this, NotificationCenter.blockedUsersDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.fileNewChunkAvailable);
        getNotificationCenter().removeObserver(this, NotificationCenter.didCreatedNewDeleteTask);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagePlayingDidStart);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagePlayingGoingToStop);
        getNotificationCenter().removeObserver(this, NotificationCenter.updateMessageMedia);
        getNotificationCenter().removeObserver(this, NotificationCenter.voiceTranscriptionUpdate);
        getNotificationCenter().removeObserver(this, NotificationCenter.replaceMessagesObjects);
        getNotificationCenter().removeObserver(this, NotificationCenter.notificationsSettingsUpdated);
        getNotificationCenter().removeObserver(this, NotificationCenter.replyMessagesDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.didReceivedWebpages);
        getNotificationCenter().removeObserver(this, NotificationCenter.didReceivedWebpagesInUpdates);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagesReadContent);
        getNotificationCenter().removeObserver(this, NotificationCenter.botInfoDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.botKeyboardDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.chatSearchResultsAvailable);
        getNotificationCenter().removeObserver(this, NotificationCenter.chatSearchResultsLoading);
        getNotificationCenter().removeObserver(this, NotificationCenter.messagePlayingPlayStateChanged);
        getNotificationCenter().removeObserver(this, NotificationCenter.didUpdateMessagesViews);
        getNotificationCenter().removeObserver(this, NotificationCenter.chatInfoCantLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.didLoadPinnedMessages);
        getNotificationCenter().removeObserver(this, NotificationCenter.peerSettingsDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.newDraftReceived);
        getNotificationCenter().removeObserver(this, NotificationCenter.userInfoDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.pinnedInfoDidLoad);
        NotificationCenter.getGlobalInstance().removeObserver(this, NotificationCenter.didSetNewWallpapper);
        NotificationCenter.getGlobalInstance().removeObserver(this, NotificationCenter.didApplyNewTheme);
        NotificationCenter.getGlobalInstance().removeObserver(this, NotificationCenter.goingToPreviewTheme);
        getNotificationCenter().removeObserver(this, NotificationCenter.channelRightsUpdated);
        getNotificationCenter().removeObserver(this, NotificationCenter.updateMentionsCount);
        getNotificationCenter().removeObserver(this, NotificationCenter.audioRecordTooShort);
        getNotificationCenter().removeObserver(this, NotificationCenter.didUpdatePollResults);
        getNotificationCenter().removeObserver(this, NotificationCenter.didUpdateReactions);
        getNotificationCenter().removeObserver(this, NotificationCenter.chatOnlineCountDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.videoLoadingStateChanged);
        getNotificationCenter().removeObserver(this, NotificationCenter.scheduledMessagesUpdated);
        getNotificationCenter().removeObserver(this, NotificationCenter.diceStickersDidLoad);
        getNotificationCenter().removeObserver(this, NotificationCenter.dialogDeleted);
        getNotificationCenter().removeObserver(this, NotificationCenter.chatAvailableReactionsUpdated);
        getNotificationCenter().removeObserver(this, NotificationCenter.didLoadSponsoredMessages);
        getNotificationCenter().removeObserver(this, NotificationCenter.didLoadSendAsPeers);
        getNotificationCenter().removeObserver(this, NotificationCenter.dialogsUnreadReactionsCounterChanged);
        if (this.currentEncryptedChat != null) {
            getNotificationCenter().removeObserver(this, NotificationCenter.didVerifyMessagesStickers);
        }
        NotificationCenter.getGlobalInstance().removeObserver(this, NotificationCenter.needSetDayNightTheme);
        if (this.chatMode == 0 && AndroidUtilities.isTablet()) {
            getNotificationCenter().postNotificationName(NotificationCenter.openedChatChanged, Long.valueOf(this.dialog_id), Boolean.TRUE);
        }
        if (this.currentUser != null) {
            MediaController.getInstance().stopMediaObserver();
        }
        Runnable runnable2 = this.unregisterFlagSecureNoforwards;
        if (runnable2 != null) {
            runnable2.run();
            this.unregisterFlagSecureNoforwards = null;
        }
        Runnable runnable3 = this.unregisterFlagSecurePasscode;
        if (runnable3 != null) {
            runnable3.run();
            this.unregisterFlagSecurePasscode = null;
        }
        if (this.currentUser != null) {
            getMessagesController().cancelLoadFullUser(this.currentUser.id);
        }
        AndroidUtilities.removeAdjustResize(getParentActivity(), this.classGuid);
        StickersAdapter stickersAdapter = this.stickersAdapter;
        if (stickersAdapter != null) {
            stickersAdapter.onDestroy();
        }
        ChatAttachAlert chatAttachAlert2 = this.chatAttachAlert;
        if (chatAttachAlert2 != null) {
            chatAttachAlert2.onDestroy();
        }
        AndroidUtilities.unlockOrientation(getParentActivity());
        if (ChatObject.isChannel(this.currentChat)) {
            getMessagesController().startShortPoll(this.currentChat, this.classGuid, true);
            TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
            if (!(tLRPC$ChatFull == null || tLRPC$ChatFull.linked_chat_id == 0)) {
                getMessagesController().startShortPoll(getMessagesController().getChat(Long.valueOf(this.chatInfo.linked_chat_id)), this.classGuid, true);
            }
        }
        TextSelectionHelper.ChatListTextSelectionHelper chatListTextSelectionHelper = this.textSelectionHelper;
        if (chatListTextSelectionHelper != null) {
            chatListTextSelectionHelper.clear();
        }
        ChatListItemAnimator chatListItemAnimator = this.chatListItemAnimator;
        if (chatListItemAnimator != null) {
            chatListItemAnimator.onDestroy();
        }
        PinchToZoomHelper pinchToZoomHelper = this.pinchToZoomHelper;
        if (pinchToZoomHelper != null) {
            pinchToZoomHelper.clear();
        }
        this.chatThemeBottomSheet = null;
        ActionBarLayout parentLayout = getParentLayout();
        if (!(parentLayout == null || (arrayList = parentLayout.fragmentsStack) == null)) {
            BackButtonMenu.clearPulledDialogs(this, arrayList.indexOf(this) - (!replacingChatActivity ? 1 : 0));
        }
        replacingChatActivity = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:314:0x0c36  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x0d6c  */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x0d93  */
    /* JADX WARNING: Removed duplicated region for block: B:337:0x10ad  */
    /* JADX WARNING: Removed duplicated region for block: B:340:0x1127  */
    /* JADX WARNING: Removed duplicated region for block: B:343:0x11d2  */
    /* JADX WARNING: Removed duplicated region for block: B:346:0x1228  */
    /* JADX WARNING: Removed duplicated region for block: B:354:0x141e  */
    /* JADX WARNING: Removed duplicated region for block: B:355:0x1420  */
    /* JADX WARNING: Removed duplicated region for block: B:358:0x1438  */
    /* JADX WARNING: Removed duplicated region for block: B:359:0x143b  */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x1493  */
    /* JADX WARNING: Removed duplicated region for block: B:363:0x14b2  */
    /* JADX WARNING: Removed duplicated region for block: B:366:0x1562  */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x1581  */
    /* JADX WARNING: Removed duplicated region for block: B:370:0x1698  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x16b7  */
    /* JADX WARNING: Removed duplicated region for block: B:374:0x1792  */
    /* JADX WARNING: Removed duplicated region for block: B:377:0x18d0  */
    /* JADX WARNING: Removed duplicated region for block: B:386:0x1910  */
    /* JADX WARNING: Removed duplicated region for block: B:394:0x19db  */
    /* JADX WARNING: Removed duplicated region for block: B:398:0x1b0c  */
    /* JADX WARNING: Removed duplicated region for block: B:408:0x1c4f  */
    /* JADX WARNING: Removed duplicated region for block: B:418:0x1f91  */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x2160  */
    /* JADX WARNING: Removed duplicated region for block: B:441:0x2216  */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x2226  */
    /* JADX WARNING: Removed duplicated region for block: B:446:0x227c  */
    /* JADX WARNING: Removed duplicated region for block: B:449:0x2299  */
    /* JADX WARNING: Removed duplicated region for block: B:456:0x22bf  */
    /* JADX WARNING: Removed duplicated region for block: B:459:0x22c8  */
    /* JADX WARNING: Removed duplicated region for block: B:460:0x22d7  */
    /* JADX WARNING: Removed duplicated region for block: B:463:0x2321  */
    @Override // org.telegram.ui.ActionBar.BaseFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View createView(final android.content.Context r39) {
        /*
        // Method dump skipped, instructions count: 9004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.createView(android.content.Context):android.view.View");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$14(View view, View view2) {
        ActionBarPopupWindow show = BackButtonMenu.show(this, view, this.dialog_id, this.themeDelegate);
        this.scrimPopupWindow = show;
        if (show == null) {
            return false;
        }
        show.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda111
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                ChatActivity.this.lambda$createView$13();
            }
        });
        this.chatListView.stopScroll();
        this.chatLayoutManager.setCanScrollVertically(false);
        dimBehindView(view, 0.3f);
        hideHints(false);
        UndoView undoView = this.topUndoView;
        if (undoView != null) {
            undoView.hide(true, 1);
        }
        UndoView undoView2 = this.undoView;
        if (undoView2 != null) {
            undoView2.hide(true, 1);
        }
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.getEditField().setAllowDrawCursor(false);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$13() {
        this.scrimPopupWindow = null;
        this.menuDeleteItem = null;
        this.scrimPopupWindowItems = null;
        this.chatLayoutManager.setCanScrollVertically(true);
        if (this.scrimPopupWindowHideDimOnDismiss) {
            dimBehindView(false);
        } else {
            this.scrimPopupWindowHideDimOnDismiss = true;
        }
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.getEditField().setAllowDrawCursor(true);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$15(View view, MotionEvent motionEvent) {
        ChatThemeBottomSheet chatThemeBottomSheet = this.chatThemeBottomSheet;
        if (chatThemeBottomSheet == null) {
            return false;
        }
        chatThemeBottomSheet.close();
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$16(int i, View view) {
        jumpToDate(i);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$20(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        if (!(tLObject instanceof TLRPC$messages_Messages)) {
            return;
        }
        if (!((TLRPC$messages_Messages) tLObject).messages.isEmpty()) {
            TLRPC$TL_messages_getHistory tLRPC$TL_messages_getHistory = new TLRPC$TL_messages_getHistory();
            tLRPC$TL_messages_getHistory.peer = getMessagesController().getInputPeer(this.dialog_id);
            tLRPC$TL_messages_getHistory.offset_date = this.startLoadFromDate + 86400;
            tLRPC$TL_messages_getHistory.limit = 1;
            getConnectionsManager().sendRequest(tLRPC$TL_messages_getHistory, new RequestDelegate(tLObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda225
                public final /* synthetic */ TLObject f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.telegram.tgnet.RequestDelegate
                public final void run(TLObject tLObject2, TLRPC$TL_error tLRPC$TL_error2) {
                    ChatActivity.this.lambda$createView$19(this.f$1, tLObject2, tLRPC$TL_error2);
                }
            });
            return;
        }
        this.actionBar.setSubtitle(LocaleController.getString("NoMessagesForThisDay", R.string.NoMessagesForThisDay));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$19(TLObject tLObject, TLObject tLObject2, TLRPC$TL_error tLRPC$TL_error) {
        int i;
        if (tLObject2 instanceof TLRPC$messages_Messages) {
            TLRPC$messages_Messages tLRPC$messages_Messages = (TLRPC$messages_Messages) tLObject2;
            if (!tLRPC$messages_Messages.messages.isEmpty()) {
                i = ((TLRPC$messages_Messages) tLObject).offset_id_offset - tLRPC$messages_Messages.offset_id_offset;
            } else {
                i = ((TLRPC$messages_Messages) tLObject).offset_id_offset;
            }
            AndroidUtilities.runOnUIThread(new Runnable(i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda161
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createView$18(this.f$1);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$17(int i) {
        this.actionBar.setSubtitle(LocaleController.formatPluralString("messages", i, new Object[0]));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$18(int i) {
        if (i != 0) {
            AndroidUtilities.runOnUIThread(new Runnable(i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda160
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createView$17(this.f$1);
                }
            });
        } else {
            this.actionBar.setSubtitle(LocaleController.getString("NoMessagesForThisDay", R.string.NoMessagesForThisDay));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$22(View view) {
        if (MessagesController.getInstance(this.currentAccount).isDialogMuted(this.dialog_id)) {
            updateTitleIcons(true);
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda124
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createView$21();
                }
            }, 150);
            this.headerItem.toggleSubMenu();
            BulletinFactory.createMuteBulletin(this, false, this.themeDelegate).show();
            return;
        }
        this.muteItem.openSwipeBack();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$21() {
        toggleMute(true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$25(TLRPC$Document tLRPC$Document) {
        this.animatingDocuments.put(tLRPC$Document, 0);
        SendMessagesHelper.getInstance(this.currentAccount).sendSticker(tLRPC$Document, null, this.dialog_id, null, null, null, null, true, 0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$26(TLRPC$Document tLRPC$Document) {
        this.animatingDocuments.put(tLRPC$Document, 0);
        SendMessagesHelper.getInstance(this.currentAccount).sendSticker(tLRPC$Document, null, this.dialog_id, null, null, null, null, true, 0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$27(View view) {
        if (this.floatingDateView.getAlpha() != 0.0f && !this.actionBar.isActionModeShowed() && this.reportType < 0) {
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(((long) this.floatingDateView.getCustomDate()) * 1000);
            int i = instance.get(1);
            int i2 = instance.get(2);
            int i3 = instance.get(5);
            instance.clear();
            instance.set(i, i2, i3);
            jumpToDate((int) (instance.getTime().getTime() / 1000));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$28(View view) {
        int i;
        this.wasManualScroll = true;
        if (isThreadChat()) {
            scrollToMessageId(this.threadMessageId, 0, true, 0, true, 0);
            return;
        }
        int i2 = this.currentPinnedMessageId;
        if (i2 != 0) {
            if (!this.pinnedMessageIds.isEmpty()) {
                ArrayList<Integer> arrayList = this.pinnedMessageIds;
                if (i2 == arrayList.get(arrayList.size() - 1).intValue()) {
                    i = this.pinnedMessageIds.get(0).intValue() + 1;
                    this.forceScrollToFirst = true;
                } else {
                    i = i2 - 1;
                    this.forceScrollToFirst = false;
                }
            } else {
                i = 0;
            }
            this.forceNextPinnedMessageId = i;
            scrollToMessageId(i2, 0, true, 0, true, !this.forceScrollToFirst ? -i : i);
            updateMessagesVisiblePart(false);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$29(View view) {
        openPinnedMessagesList(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$31(View view) {
        boolean z;
        TLRPC$UserFull tLRPC$UserFull;
        if (getParentActivity() != null) {
            TLRPC$Chat tLRPC$Chat = this.currentChat;
            if (tLRPC$Chat != null) {
                z = ChatObject.canPinMessages(tLRPC$Chat);
            } else {
                z = (this.currentEncryptedChat != null || (tLRPC$UserFull = this.userInfo) == null) ? false : tLRPC$UserFull.can_pin_message;
            }
            if (z) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
                builder.setTitle(LocaleController.getString("UnpinMessageAlertTitle", R.string.UnpinMessageAlertTitle));
                builder.setMessage(LocaleController.getString("UnpinMessageAlert", R.string.UnpinMessageAlert));
                builder.setPositiveButton(LocaleController.getString("UnpinMessage", R.string.UnpinMessage), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda25
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        ChatActivity.this.lambda$createView$30(dialogInterface, i);
                    }
                });
                builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
                showDialog(builder.create());
            } else if (!this.pinnedMessageIds.isEmpty()) {
                SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
                edit.putInt("pin_" + this.dialog_id, this.pinnedMessageIds.get(0).intValue()).commit();
                updatePinnedMessageView(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$30(DialogInterface dialogInterface, int i) {
        MessageObject messageObject = this.pinnedMessageObjects.get(Integer.valueOf(this.currentPinnedMessageId));
        if (messageObject == null) {
            messageObject = this.messagesDict[0].get(this.currentPinnedMessageId);
        }
        unpinMessage(messageObject);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$33(View view) {
        AlertsCreator.showBlockReportSpamAlert(this, this.dialog_id, this.currentUser, this.currentChat, this.currentEncryptedChat, this.reportSpamButton.getTag(R.id.object_tag) != null, this.chatInfo, new MessagesStorage.IntCallback() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda213
            @Override // org.telegram.messenger.MessagesStorage.IntCallback
            public final void run(int i) {
                ChatActivity.this.lambda$createView$32(i);
            }
        }, this.themeDelegate);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$32(int i) {
        if (i == 0) {
            updateTopPanel(true);
        } else {
            finishFragment();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$36(Context context, View view) {
        if (this.addToContactsButtonArchive) {
            getMessagesController().addDialogToFolder(this.dialog_id, 0, 0, 0);
            this.undoView.showWithAction(this.dialog_id, 23, (Runnable) null);
            SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
            edit.putBoolean("dialog_bar_archived" + this.dialog_id, false);
            edit.putBoolean("dialog_bar_block" + this.dialog_id, false);
            edit.putBoolean("dialog_bar_report" + this.dialog_id, false);
            edit.commit();
            updateTopPanel(false);
            getNotificationsController().clearDialogNotificationsSettings(this.dialog_id);
        } else if (this.addToContactsButton.getTag() != null && ((Integer) this.addToContactsButton.getTag()).intValue() == 4) {
            TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
            if (!(tLRPC$ChatFull == null || tLRPC$ChatFull.participants == null)) {
                LongSparseArray longSparseArray = new LongSparseArray();
                for (int i = 0; i < this.chatInfo.participants.participants.size(); i++) {
                    longSparseArray.put(this.chatInfo.participants.participants.get(i).user_id, null);
                }
                long j = this.chatInfo.id;
                InviteMembersBottomSheet inviteMembersBottomSheet = new InviteMembersBottomSheet(context, this.currentAccount, longSparseArray, this.chatInfo.id, this, this.themeDelegate);
                inviteMembersBottomSheet.setDelegate(new GroupCreateActivity.ContactsAddActivityDelegate(j) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda255
                    public final /* synthetic */ long f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.telegram.ui.GroupCreateActivity.ContactsAddActivityDelegate
                    public final void didSelectUsers(ArrayList arrayList, int i2) {
                        ChatActivity.this.lambda$createView$34(this.f$1, arrayList, i2);
                    }

                    public /* synthetic */ void needAddBot(TLRPC$User tLRPC$User) {
                        GroupCreateActivity.ContactsAddActivityDelegate.CC.$default$needAddBot(this, tLRPC$User);
                    }
                });
                inviteMembersBottomSheet.show();
            }
        } else if (this.addToContactsButton.getTag() != null) {
            shareMyContact(1, null);
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("user_id", this.currentUser.id);
            bundle.putBoolean("addContact", true);
            ContactAddActivity contactAddActivity = new ContactAddActivity(bundle);
            contactAddActivity.setDelegate(new ContactAddActivity.ContactAddActivityDelegate() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda253
                @Override // org.telegram.ui.ContactAddActivity.ContactAddActivityDelegate
                public final void didAddToContacts() {
                    ChatActivity.this.lambda$createView$35();
                }
            });
            presentFragment(contactAddActivity);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$34(long j, ArrayList arrayList, int i) {
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            getMessagesController().addUserToChat(j, (TLRPC$User) arrayList.get(i2), i, null, this, null);
        }
        getMessagesController().hidePeerSettingsBar(this.dialog_id, this.currentUser, this.currentChat);
        updateTopPanel(true);
        updateInfoTopView(true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$35() {
        this.undoView.showWithAction(this.dialog_id, 8, this.currentUser);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$37(View view) {
        long j = this.dialog_id;
        if (this.currentEncryptedChat != null) {
            j = this.currentUser.id;
        }
        getMessagesController().hidePeerSettingsBar(j, this.currentUser, this.currentChat);
        updateTopPanel(true);
        updateInfoTopView(true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$38(View view) {
        this.wasManualScroll = true;
        this.textSelectionHelper.cancelTextSelectionRunnable();
        int i = this.createUnreadMessageAfterId;
        if (i != 0) {
            scrollToMessageId(i, 0, false, this.returnToLoadIndex, true, 0);
            return;
        }
        int i2 = this.returnToMessageId;
        if (i2 > 0) {
            scrollToMessageId(i2, 0, true, this.returnToLoadIndex, true, 0);
            return;
        }
        scrollToLastMessage(false);
        if (!this.pinnedMessageIds.isEmpty()) {
            this.forceScrollToFirst = true;
            this.forceNextPinnedMessageId = this.pinnedMessageIds.get(0).intValue();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$41(View view) {
        this.scrimPopupWindow = ReadAllMentionsMenu.show(1, getParentActivity(), this.contentView, view, getResourceProvider(), new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda155
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$createView$39();
            }
        });
        dimBehindView((View) this.mentiondownButton, true);
        this.scrimPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda109
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                ChatActivity.this.lambda$createView$40();
            }
        });
        view.performHapticFeedback(0, 2);
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$39() {
        for (int i = 0; i < this.messages.size(); i++) {
            MessageObject messageObject = this.messages.get(i);
            if (messageObject.messageOwner.mentioned && !messageObject.isContentUnread()) {
                messageObject.setContentIsRead();
            }
        }
        this.newMentionsCount = 0;
        getMessagesController().markMentionsAsRead(this.dialog_id);
        this.hasAllMentionsLocal = true;
        showMentionDownButton(false, true);
        ActionBarPopupWindow actionBarPopupWindow = this.scrimPopupWindow;
        if (actionBarPopupWindow != null) {
            actionBarPopupWindow.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$40() {
        this.scrimPopupWindow = null;
        this.menuDeleteItem = null;
        this.scrimPopupWindowItems = null;
        this.chatLayoutManager.setCanScrollVertically(true);
        dimBehindView(false);
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.getEditField().setAllowDrawCursor(true);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$42(ContentPreviewViewer.ContentPreviewViewerDelegate contentPreviewViewerDelegate, View view, MotionEvent motionEvent) {
        ContentPreviewViewer instance = ContentPreviewViewer.getInstance();
        MentionsContainerView.MentionsListView listView = this.mentionContainer.getListView();
        RecyclerListView.OnItemClickListener onItemClickListener = this.mentionsOnItemClickListener;
        if (!this.mentionContainer.getAdapter().isStickers()) {
            contentPreviewViewerDelegate = null;
        }
        return instance.onTouch(motionEvent, listView, 0, onItemClickListener, contentPreviewViewerDelegate, this.themeDelegate);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$46(View view, int i) {
        char c;
        if (i != 0 && !this.mentionContainer.getAdapter().isBannedInline()) {
            int i2 = i - 1;
            Object item = this.mentionContainer.getAdapter().getItem(i2);
            int resultStartPosition = this.mentionContainer.getAdapter().getResultStartPosition();
            int resultLength = this.mentionContainer.getAdapter().getResultLength();
            MessageObject.SendAnimationData sendAnimationData = null;
            if (item instanceof TLRPC$TL_document) {
                if (this.chatMode != 0 || !checkSlowMode(view)) {
                    if (view instanceof StickerCell) {
                        sendAnimationData = ((StickerCell) view).getSendAnimationData();
                    }
                    TLRPC$TL_document tLRPC$TL_document = (TLRPC$TL_document) item;
                    Object itemParent = this.mentionContainer.getAdapter().getItemParent(i2);
                    if (this.chatMode == 1) {
                        AlertsCreator.createScheduleDatePickerDialog(getParentActivity(), this.dialog_id, new AlertsCreator.ScheduleDatePickerDelegate(tLRPC$TL_document, this.stickersAdapter.getQuery(), itemParent) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda240
                            public final /* synthetic */ TLRPC$TL_document f$1;
                            public final /* synthetic */ String f$2;
                            public final /* synthetic */ Object f$3;

                            {
                                this.f$1 = r2;
                                this.f$2 = r3;
                                this.f$3 = r4;
                            }

                            @Override // org.telegram.ui.Components.AlertsCreator.ScheduleDatePickerDelegate
                            public final void didSelectDate(boolean z, int i3) {
                                ChatActivity.this.lambda$createView$43(this.f$1, this.f$2, this.f$3, z, i3);
                            }
                        }, this.themeDelegate);
                    } else {
                        getSendMessagesHelper().sendSticker(tLRPC$TL_document, this.stickersAdapter.getQuery(), this.dialog_id, this.replyingMessageObject, getThreadMessage(), itemParent, sendAnimationData, true, 0);
                    }
                    hideFieldPanel(false);
                    this.chatActivityEnterView.addStickerToRecent(tLRPC$TL_document);
                    this.chatActivityEnterView.setFieldText("");
                }
            } else if (item instanceof TLRPC$Chat) {
                TLRPC$Chat tLRPC$Chat = (TLRPC$Chat) item;
                if (this.searchingForUser && this.searchContainer.getVisibility() == 0) {
                    searchUserMessages(null, tLRPC$Chat);
                } else if (tLRPC$Chat.username != null) {
                    this.chatActivityEnterView.replaceWithText(resultStartPosition, resultLength, "@" + tLRPC$Chat.username + " ", false);
                }
            } else if (item instanceof TLRPC$User) {
                TLRPC$User tLRPC$User = (TLRPC$User) item;
                if (this.searchingForUser && this.searchContainer.getVisibility() == 0) {
                    searchUserMessages(tLRPC$User, null);
                } else if (tLRPC$User.username != null) {
                    this.chatActivityEnterView.replaceWithText(resultStartPosition, resultLength, "@" + tLRPC$User.username + " ", false);
                } else {
                    SpannableString spannableString = new SpannableString(UserObject.getFirstName(tLRPC$User, false) + " ");
                    spannableString.setSpan(new URLSpanUserMention("" + tLRPC$User.id, 3), 0, spannableString.length(), 33);
                    this.chatActivityEnterView.replaceWithText(resultStartPosition, resultLength, spannableString, false);
                }
            } else if (item instanceof String) {
                if (!this.mentionContainer.getAdapter().isBotCommands()) {
                    this.chatActivityEnterView.replaceWithText(resultStartPosition, resultLength, item + " ", false);
                } else if (this.chatMode == 1) {
                    AlertsCreator.createScheduleDatePickerDialog(getParentActivity(), this.dialog_id, new AlertsCreator.ScheduleDatePickerDelegate(item) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda237
                        public final /* synthetic */ Object f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // org.telegram.ui.Components.AlertsCreator.ScheduleDatePickerDelegate
                        public final void didSelectDate(boolean z, int i3) {
                            ChatActivity.this.lambda$createView$44(this.f$1, z, i3);
                        }
                    }, this.themeDelegate);
                } else if (!checkSlowMode(view)) {
                    getSendMessagesHelper().sendMessage((String) item, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, false, null, null, null, true, 0, null);
                    this.chatActivityEnterView.setFieldText("");
                    hideFieldPanel(false);
                }
            } else if (item instanceof TLRPC$BotInlineResult) {
                if (this.chatActivityEnterView.getFieldText() == null) {
                    return;
                }
                if (this.chatMode == 1 || !checkSlowMode(view)) {
                    TLRPC$BotInlineResult tLRPC$BotInlineResult = (TLRPC$BotInlineResult) item;
                    if (this.currentEncryptedChat != null) {
                        if (!(tLRPC$BotInlineResult.send_message instanceof TLRPC$TL_botInlineMessageMediaAuto) || !"game".equals(tLRPC$BotInlineResult.type)) {
                            c = tLRPC$BotInlineResult.send_message instanceof TLRPC$TL_botInlineMessageMediaInvoice ? (char) 2 : 0;
                        } else {
                            c = 1;
                        }
                        if (c != 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
                            builder.setTitle(LocaleController.getString("SendMessageTitle", R.string.SendMessageTitle));
                            if (c == 1) {
                                builder.setMessage(LocaleController.getString("GameCantSendSecretChat", R.string.GameCantSendSecretChat));
                            } else {
                                builder.setMessage(LocaleController.getString("InvoiceCantSendSecretChat", R.string.InvoiceCantSendSecretChat));
                            }
                            builder.setNegativeButton(LocaleController.getString("OK", R.string.OK), null);
                            showDialog(builder.create());
                            return;
                        }
                    }
                    if ((tLRPC$BotInlineResult.type.equals("photo") && (tLRPC$BotInlineResult.photo != null || tLRPC$BotInlineResult.content != null)) || ((tLRPC$BotInlineResult.type.equals("gif") && (tLRPC$BotInlineResult.document != null || tLRPC$BotInlineResult.content != null)) || (tLRPC$BotInlineResult.type.equals(MediaStreamTrack.VIDEO_TRACK_KIND) && tLRPC$BotInlineResult.document != null))) {
                        ArrayList<Object> arrayList = new ArrayList<>(this.mentionContainer.getAdapter().getSearchResultBotContext());
                        this.botContextResults = arrayList;
                        PhotoViewer.getInstance().setParentActivity(getParentActivity(), this.themeDelegate);
                        PhotoViewer.getInstance().openPhotoForSelect(arrayList, this.mentionContainer.getAdapter().getItemPosition(i2), 3, false, this.botContextProvider, this);
                    } else if (this.chatMode == 1) {
                        AlertsCreator.createScheduleDatePickerDialog(getParentActivity(), this.dialog_id, new AlertsCreator.ScheduleDatePickerDelegate(tLRPC$BotInlineResult) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda239
                            public final /* synthetic */ TLRPC$BotInlineResult f$1;

                            {
                                this.f$1 = r2;
                            }

                            @Override // org.telegram.ui.Components.AlertsCreator.ScheduleDatePickerDelegate
                            public final void didSelectDate(boolean z, int i3) {
                                ChatActivity.this.lambda$createView$45(this.f$1, z, i3);
                            }
                        }, this.themeDelegate);
                    } else {
                        lambda$createView$45(tLRPC$BotInlineResult, true, 0);
                    }
                }
            } else if (item instanceof TLRPC$TL_inlineBotSwitchPM) {
                processInlineBotContextPM((TLRPC$TL_inlineBotSwitchPM) item);
            } else if (item instanceof MediaDataController.KeywordResult) {
                String str = ((MediaDataController.KeywordResult) item).emoji;
                this.chatActivityEnterView.addEmojiToRecent(str);
                this.chatActivityEnterView.replaceWithText(resultStartPosition, resultLength, str, true);
                this.mentionContainer.updateVisibility(false);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$43(TLRPC$TL_document tLRPC$TL_document, String str, Object obj, boolean z, int i) {
        SendMessagesHelper.getInstance(this.currentAccount).sendSticker(tLRPC$TL_document, str, this.dialog_id, this.replyingMessageObject, getThreadMessage(), obj, null, z, i);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$44(Object obj, boolean z, int i) {
        getSendMessagesHelper().sendMessage((String) obj, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, false, null, null, null, z, i, null);
        this.chatActivityEnterView.setFieldText("");
        hideFieldPanel(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$48(View view, int i) {
        boolean z = false;
        if (getParentActivity() != null && this.mentionContainer.getAdapter().isLongClickEnabled()) {
            Object item = this.mentionContainer.getAdapter().getItem(i);
            if (item instanceof String) {
                if (!this.mentionContainer.getAdapter().isBotCommands()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
                    builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
                    builder.setMessage(LocaleController.getString("ClearSearch", R.string.ClearSearch));
                    builder.setPositiveButton(LocaleController.getString("ClearButton", R.string.ClearButton).toUpperCase(), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda30
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            ChatActivity.this.lambda$createView$47(dialogInterface, i2);
                        }
                    });
                    builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
                    showDialog(builder.create());
                    return true;
                } else if (!URLSpanBotCommand.enabled) {
                    return false;
                } else {
                    this.chatActivityEnterView.setFieldText("");
                    ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
                    String str = (String) item;
                    TLRPC$Chat tLRPC$Chat = this.currentChat;
                    if (tLRPC$Chat != null && tLRPC$Chat.megagroup) {
                        z = true;
                    }
                    chatActivityEnterView.setCommand(null, str, true, z);
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$47(DialogInterface dialogInterface, int i) {
        this.mentionContainer.getAdapter().clearRecentHashtags();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$50(View view) {
        this.wasManualScroll = true;
        getMessagesController().getNextReactionMention(this.dialog_id, this.reactionsMentionCount, new Consumer() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda112
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ChatActivity.this.lambda$createView$49((Integer) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$49(Integer num) {
        if (num.intValue() == 0) {
            this.reactionsMentionCount = 0;
            updateReactionsMentionButton(true);
            getMessagesController().markReactionsAsRead(this.dialog_id);
            return;
        }
        updateReactionsMentionButton(true);
        scrollToMessageId(num.intValue(), 0, false, 0, true, 0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$53(View view) {
        this.scrimPopupWindow = ReadAllMentionsMenu.show(0, getParentActivity(), this.contentView, view, getResourceProvider(), new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda118
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$createView$51();
            }
        });
        dimBehindView((View) this.reactionsMentiondownButton, true);
        this.scrimPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda110
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                ChatActivity.this.lambda$createView$52();
            }
        });
        view.performHapticFeedback(0, 2);
        return false;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$51() {
        for (int i = 0; i < this.messages.size(); i++) {
            this.messages.get(i).markReactionsAsRead();
        }
        this.reactionsMentionCount = 0;
        updateReactionsMentionButton(true);
        getMessagesController().markReactionsAsRead(this.dialog_id);
        ActionBarPopupWindow actionBarPopupWindow = this.scrimPopupWindow;
        if (actionBarPopupWindow != null) {
            actionBarPopupWindow.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$52() {
        this.scrimPopupWindow = null;
        this.menuDeleteItem = null;
        this.scrimPopupWindowItems = null;
        this.chatLayoutManager.setCanScrollVertically(true);
        dimBehindView(false);
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.getEditField().setAllowDrawCursor(true);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$54(View view, int i) {
        getMediaDataController().jumpToSearchedMessage(this.classGuid, i);
        showMessagesSearchListView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$55(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            checkRecordLocked(false);
        }
        this.overlayView.getParent().requestDisallowInterceptTouchEvent(true);
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$57(View view) {
        ForwardingMessagesParams forwardingMessagesParams = this.forwardingMessages;
        if (forwardingMessagesParams != null && !forwardingMessagesParams.messages.isEmpty()) {
            SharedConfig.forwardingOptionsHintHintShowed();
            openForwardingPreview();
        } else if (this.replyingMessageObject == null || (isThreadChat() && this.replyingMessageObject.getId() == this.threadMessageId)) {
            MessageObject messageObject = this.editingMessageObject;
            if (messageObject == null) {
                return;
            }
            if (!messageObject.canEditMedia() || this.editingMessageObjectReqId != 0) {
                scrollToMessageId(this.editingMessageObject.getId(), 0, true, 0, true, 0);
                return;
            }
            if (this.chatAttachAlert == null) {
                createChatAttachView();
            }
            this.chatAttachAlert.setEditingMessageObject(this.editingMessageObject);
            openAttachMenu();
        } else {
            scrollToMessageId(this.replyingMessageObject.getId(), 0, true, 0, true, 0);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$58(View view) {
        ForwardingMessagesParams forwardingMessagesParams = this.forwardingMessages;
        if (forwardingMessagesParams == null || forwardingMessagesParams.messages.isEmpty()) {
            showFieldPanel(false, null, null, null, this.foundWebPage, true, 0, true, true);
        } else {
            openAnotherForward();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$59(View view) {
        MessageObject messageObject = this.editingMessageObject;
        if (messageObject != null) {
            scrollToMessageId(messageObject.getId(), 0, true, 0, true, 0);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$60(ChatActivityEnterTopView.EditViewButton editViewButton, FrameLayout frameLayout, View view) {
        MessageObject messageObject = this.editingMessageObject;
        if (messageObject != null && messageObject.canEditMedia() && this.editingMessageObjectReqId == 0) {
            if (editViewButton.isEditButton()) {
                openEditingMessageInPhotoEditor();
            } else {
                frameLayout.callOnClick();
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$61(ContentPreviewViewer.ContentPreviewViewerDelegate contentPreviewViewerDelegate, View view, MotionEvent motionEvent) {
        return ContentPreviewViewer.getInstance().onTouch(motionEvent, this.stickersListView, 0, this.stickersOnItemClickListener, contentPreviewViewerDelegate, this.themeDelegate);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$62(View view, MotionEvent motionEvent) {
        return getMediaDataController().getFoundMessageObjects().size() <= 1;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$63(View view) {
        if (getMediaDataController().getFoundMessageObjects().size() > 1) {
            HintView hintView = this.searchAsListHint;
            if (hintView != null) {
                hintView.hide();
            }
            toggleMesagesSearchListView();
            if (!SharedConfig.searchMessagesAsListUsed) {
                SharedConfig.setSearchMessagesAsListUsed(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$64(View view) {
        getMediaDataController().searchMessagesInChat(null, this.dialog_id, this.mergeDialogId, this.classGuid, 1, this.threadMessageId, this.searchingUserMessages, this.searchingChatMessages);
        showMessagesSearchListView(false);
        if (!SharedConfig.searchMessagesAsListUsed && SharedConfig.searchMessagesAsListHintShows < 3 && !this.searchAsListHintShown && Math.random() <= 0.25d) {
            showSearchAsListHint();
            this.searchAsListHintShown = true;
            SharedConfig.increaseSearchAsListHintShows();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$65(View view) {
        getMediaDataController().searchMessagesInChat(null, this.dialog_id, this.mergeDialogId, this.classGuid, 2, this.threadMessageId, this.searchingUserMessages, this.searchingChatMessages);
        showMessagesSearchListView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$66(View view) {
        MentionsContainerView mentionsContainerView = this.mentionContainer;
        if (mentionsContainerView != null) {
            mentionsContainerView.setReversed(true);
            this.mentionContainer.getAdapter().setSearchingMentions(true);
        }
        this.searchCalendarButton.setVisibility(8);
        this.searchUserButton.setVisibility(8);
        this.searchingForUser = true;
        this.searchingUserMessages = null;
        this.searchingChatMessages = null;
        this.searchItem.setSearchFieldHint(LocaleController.getString("SearchMembers", R.string.SearchMembers));
        this.searchItem.setSearchFieldCaption(LocaleController.getString("SearchFrom", R.string.SearchFrom));
        AndroidUtilities.showKeyboard(this.searchItem.getSearchField());
        this.searchItem.clearSearchText();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$67(View view) {
        if (getParentActivity() != null) {
            AndroidUtilities.hideKeyboard(this.searchItem.getSearchField());
            showDialog(AlertsCreator.createCalendarPickerDialog(getParentActivity(), 1375315200000L, new MessagesStorage.IntCallback() { // from class: org.telegram.ui.ChatActivity.41
                @Override // org.telegram.messenger.MessagesStorage.IntCallback
                public void run(int i) {
                    ChatActivity.this.jumpToDate(i);
                }
            }, this.themeDelegate).create());
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$72(Context context, View view) {
        String str;
        if (getParentActivity() != null && this.pullingDownOffset == 0.0f) {
            if (this.reportType >= 0) {
                showDialog(new ReportAlert(getParentActivity(), this.reportType) { // from class: org.telegram.ui.ChatActivity.44
                    @Override // org.telegram.ui.Components.ReportAlert
                    protected void onSend(int i, String str2) {
                        ArrayList arrayList = new ArrayList();
                        for (int i2 = 0; i2 < ChatActivity.this.selectedMessagesIds[0].size(); i2++) {
                            arrayList.add(Integer.valueOf(ChatActivity.this.selectedMessagesIds[0].keyAt(i2)));
                        }
                        ChatActivity chatActivity = ChatActivity.this;
                        TLRPC$User tLRPC$User = chatActivity.currentUser;
                        AlertsCreator.sendReport(tLRPC$User != null ? MessagesController.getInputPeer(tLRPC$User) : MessagesController.getInputPeer(chatActivity.currentChat), ChatActivity.this.reportType, str2, arrayList);
                        ChatActivity.this.finishFragment();
                        ChatActivity.this.chatActivityDelegate.onReport();
                    }
                });
                return;
            }
            boolean z = false;
            if (this.chatMode == 2) {
                finishFragment();
                ChatActivityDelegate chatActivityDelegate = this.chatActivityDelegate;
                if (this.bottomOverlayChatText.getTag() == null) {
                    z = true;
                }
                chatActivityDelegate.onUnpin(true, z);
                return;
            }
            TLRPC$User tLRPC$User = this.currentUser;
            if (tLRPC$User == null || !this.userBlocked) {
                if (UserObject.isReplyUser(tLRPC$User)) {
                    toggleMute(true);
                    return;
                }
                TLRPC$User tLRPC$User2 = this.currentUser;
                if (tLRPC$User2 == null || !tLRPC$User2.bot || (str = this.botUser) == null) {
                    if (ChatObject.isChannel(this.currentChat)) {
                        TLRPC$Chat tLRPC$Chat = this.currentChat;
                        if (!(tLRPC$Chat instanceof TLRPC$TL_channelForbidden)) {
                            if (!ChatObject.isNotInChat(tLRPC$Chat)) {
                                toggleMute(true);
                                return;
                            } else if (this.currentChat.join_request) {
                                showBottomOverlayProgress(true, true);
                                MessagesController.getInstance(this.currentAccount).addUserToChat(this.currentChat.id, UserConfig.getInstance(this.currentAccount).getCurrentUser(), 0, null, null, true, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda125
                                    @Override // java.lang.Runnable
                                    public final void run() {
                                        ChatActivity.this.lambda$createView$69();
                                    }
                                }, new MessagesController.ErrorDelegate(context) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda211
                                    public final /* synthetic */ Context f$1;

                                    {
                                        this.f$1 = r2;
                                    }

                                    @Override // org.telegram.messenger.MessagesController.ErrorDelegate
                                    public final boolean run(TLRPC$TL_error tLRPC$TL_error) {
                                        return ChatActivity.this.lambda$createView$70(this.f$1, tLRPC$TL_error);
                                    }
                                });
                                return;
                            } else {
                                Runnable runnable = this.chatInviteRunnable;
                                if (runnable != null) {
                                    AndroidUtilities.cancelRunOnUIThread(runnable);
                                    this.chatInviteRunnable = null;
                                }
                                showBottomOverlayProgress(true, true);
                                getMessagesController().addUserToChat(this.currentChat.id, getUserConfig().getCurrentUser(), 0, null, this, null);
                                NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.closeSearchByActiveAction, new Object[0]);
                                if (hasReportSpam() && this.reportSpamButton.getTag(R.id.object_tag) != null) {
                                    SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
                                    edit.putInt("dialog_bar_vis3" + this.dialog_id, 3).commit();
                                    getNotificationCenter().postNotificationName(NotificationCenter.peerSettingsDidLoad, Long.valueOf(this.dialog_id));
                                    return;
                                }
                                return;
                            }
                        }
                    }
                    TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
                    AlertsCreator.createClearOrDeleteDialogAlert(this, false, this.currentChat, this.currentUser, this.currentEncryptedChat != null, true, tLRPC$ChatFull != null && tLRPC$ChatFull.can_delete_channel, new MessagesStorage.BooleanCallback() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda212
                        @Override // org.telegram.messenger.MessagesStorage.BooleanCallback
                        public final void run(boolean z2) {
                            ChatActivity.this.lambda$createView$71(z2);
                        }
                    }, this.themeDelegate);
                    return;
                }
                if (str.length() != 0) {
                    getMessagesController().sendBotStart(this.currentUser, this.botUser);
                } else {
                    getSendMessagesHelper().sendMessage("/start", this.dialog_id, null, null, null, false, null, null, null, true, 0, null);
                }
                this.botUser = null;
                updateBottomOverlay();
            } else if (tLRPC$User.bot) {
                String str2 = this.botUser;
                this.botUser = null;
                getMessagesController().unblockPeer(this.currentUser.id);
                if (str2 == null || str2.length() == 0) {
                    getSendMessagesHelper().sendMessage("/start", this.dialog_id, null, null, null, false, null, null, null, true, 0, null);
                } else {
                    getMessagesController().sendBotStart(this.currentUser, str2);
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
                builder.setMessage(LocaleController.getString("AreYouSureUnblockContact", R.string.AreYouSureUnblockContact));
                builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda31
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        ChatActivity.this.lambda$createView$68(dialogInterface, i);
                    }
                });
                builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
                builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
                showDialog(builder.create());
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$68(DialogInterface dialogInterface, int i) {
        getMessagesController().unblockPeer(this.currentUser.id);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$69() {
        showBottomOverlayProgress(false, true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createView$70(Context context, TLRPC$TL_error tLRPC$TL_error) {
        SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
        edit.putLong("dialog_join_requested_time_" + this.dialog_id, System.currentTimeMillis()).commit();
        if (tLRPC$TL_error != null && "INVITE_REQUEST_SENT".equals(tLRPC$TL_error.text)) {
            JoinGroupAlert.showBulletin(context, this, ChatObject.isChannel(this.currentChat) && !this.currentChat.megagroup);
        }
        showBottomOverlayProgress(false, true);
        return false;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$71(boolean z) {
        NotificationCenter notificationCenter = getNotificationCenter();
        int i = NotificationCenter.closeChats;
        notificationCenter.removeObserver(this, i);
        getNotificationCenter().postNotificationName(i, new Object[0]);
        finishFragment();
        getNotificationCenter().postNotificationName(NotificationCenter.needDeleteDialog, Long.valueOf(this.dialog_id), this.currentUser, this.currentChat, Boolean.valueOf(z));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$73(View view) {
        this.undoView.showWithAction(this.dialog_id, 18, LocaleController.getString("BroadcastGroupInfo", R.string.BroadcastGroupInfo));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$74(View view) {
        int i;
        MessageObject messageObject = null;
        for (int i2 = 1; i2 >= 0; i2--) {
            if (messageObject == null && this.selectedMessagesIds[i2].size() != 0) {
                messageObject = this.messagesDict[i2].get(this.selectedMessagesIds[i2].keyAt(0));
            }
            this.selectedMessagesIds[i2].clear();
            this.selectedMessagesCanCopyIds[i2].clear();
            this.selectedMessagesCanStarIds[i2].clear();
        }
        hideActionMode();
        if (messageObject != null && ((i = messageObject.messageOwner.id) > 0 || (i < 0 && this.currentEncryptedChat != null))) {
            showFieldPanelForReply(messageObject);
        }
        updatePinnedMessageView(true);
        updateVisibleRows();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$75(View view) {
        openForward(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createView$76(float[] fArr) {
        fArr[1] = (float) (this.chatListView.getBottom() - this.blurredViewBottomOffset);
        fArr[0] = (((float) this.chatListView.getTop()) + this.chatListViewPaddingTop) - ((float) AndroidUtilities.dp(4.0f));
    }

    private void playReactionAnimation(Integer num) {
        ChatMessageCell findMessageCell;
        if (this.fragmentView != null && (findMessageCell = findMessageCell(num.intValue(), false)) != null) {
            TLRPC$TL_messagePeerReaction randomUnreadReaction = findMessageCell.getMessageObject().getRandomUnreadReaction();
            if (randomUnreadReaction != null && findMessageCell.reactionsLayoutInBubble.hasUnreadReactions) {
                ReactionsEffectOverlay.show(this, null, findMessageCell, 0.0f, 0.0f, randomUnreadReaction.reaction, this.currentAccount, !randomUnreadReaction.big ? 1 : 0);
                ReactionsEffectOverlay.startAnimation();
            }
            findMessageCell.markReactionsAsRead();
        }
    }

    /* access modifiers changed from: private */
    public void dimBehindView(View view, boolean z) {
        this.scrimView = view;
        dimBehindView(z ? 0.2f : 0.0f, (view == this.reactionsMentiondownButton || view == this.mentiondownButton) ? false : true);
    }

    private void dimBehindView(View view, float f) {
        this.scrimView = view;
        dimBehindView(f, (view == this.reactionsMentiondownButton || view == this.mentiondownButton) ? false : true);
    }

    public void dimBehindView(boolean z) {
        dimBehindView(z ? 0.2f : 0.0f, true);
    }

    private void dimBehindView(float f, boolean z) {
        ValueAnimator valueAnimator;
        boolean z2 = f > 0.0f;
        View view = this.scrimView;
        if (view instanceof ChatMessageCell) {
            ChatMessageCell chatMessageCell = (ChatMessageCell) view;
            chatMessageCell.setInvalidatesParent(z2);
            if (z2) {
                restartSticker(chatMessageCell);
            }
        }
        this.contentView.invalidate();
        this.chatListView.invalidate();
        AnimatorSet animatorSet = this.scrimAnimatorSet;
        if (animatorSet != null) {
            animatorSet.removeAllListeners();
            this.scrimAnimatorSet.cancel();
        }
        this.scrimAnimatorSet = new AnimatorSet();
        ArrayList arrayList = new ArrayList();
        float f2 = 1.0f;
        if (z2) {
            this.scrimViewAlpha = 1.0f;
            ValueAnimator valueAnimator2 = this.scrimViewAlphaAnimator;
            if (valueAnimator2 != null) {
                valueAnimator2.cancel();
            }
            valueAnimator = ValueAnimator.ofFloat(0.0f, f);
            arrayList.add(valueAnimator);
        } else {
            valueAnimator = ValueAnimator.ofFloat(this.scrimPaintAlpha, 0.0f);
            arrayList.add(valueAnimator);
        }
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda8
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator3) {
                ChatActivity.this.lambda$dimBehindView$77(valueAnimator3);
            }
        });
        if (!z2 || z) {
            FrameLayout frameLayout = this.pagedownButton;
            if (frameLayout != null) {
                Property property = View.ALPHA;
                float[] fArr = new float[1];
                fArr[0] = z2 ? 0.0f : 1.0f;
                arrayList.add(ObjectAnimator.ofFloat(frameLayout, property, fArr));
            }
            FrameLayout frameLayout2 = this.mentiondownButton;
            if (frameLayout2 != null) {
                Property property2 = View.ALPHA;
                float[] fArr2 = new float[1];
                fArr2[0] = z2 ? 0.0f : 1.0f;
                arrayList.add(ObjectAnimator.ofFloat(frameLayout2, property2, fArr2));
            }
            FrameLayout frameLayout3 = this.reactionsMentiondownButton;
            if (frameLayout3 != null) {
                Property property3 = View.ALPHA;
                float[] fArr3 = new float[1];
                if (z2) {
                    f2 = 0.0f;
                }
                fArr3[0] = f2;
                arrayList.add(ObjectAnimator.ofFloat(frameLayout3, property3, fArr3));
            }
        }
        this.scrimAnimatorSet.playTogether(arrayList);
        this.scrimAnimatorSet.setDuration(z2 ? 150 : 220);
        if (!z2) {
            this.scrimAnimatorSet.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.49
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    ChatActivity.this.scrimView = null;
                    ChatActivity.this.scrimViewReaction = null;
                    ChatActivity.this.contentView.invalidate();
                    ChatActivity.this.chatListView.invalidate();
                }
            });
        }
        if (this.scrimView != null && this.scrimViewAlpha <= 0.0f) {
            this.scrimView = null;
        }
        this.scrimAnimatorSet.start();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$dimBehindView$77(ValueAnimator valueAnimator) {
        this.scrimPaintAlpha = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        View view = this.fragmentView;
        if (view != null) {
            view.invalidate();
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public class PinnedMessageButton extends TextView {
        public PinnedMessageButton(ChatActivity chatActivity, Context context) {
            super(context);
            setSingleLine(true);
            setLines(1);
            setMaxLines(1);
            setEllipsize(TextUtils.TruncateAt.END);
            setTextColor(chatActivity.getThemedColor("featuredStickers_buttonText"));
            setBackground(Theme.AdaptiveRipple.filledRect(chatActivity.getThemedColor("featuredStickers_addButton"), 16.0f));
            setTextSize(1, 14.0f);
            setTypeface(AndroidUtilities.getTypeface(AndroidUtilities.TYPEFACE_ROBOTO_MEDIUM));
            setGravity(17);
            setPadding(AndroidUtilities.dp(14.0f), 0, AndroidUtilities.dp(14.0f), 0);
        }

        @Override // android.widget.TextView, android.view.View
        protected void onMeasure(int i, int i2) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i), (int) (((float) AndroidUtilities.displaySize.x) * 0.45f)), Integer.MIN_VALUE), i2);
        }
    }

    /* access modifiers changed from: private */
    public void updatePagedownButtonsPosition() {
        float animatedTop = ((float) this.chatActivityEnterView.getAnimatedTop()) + this.chatActivityEnterView.getTranslationY() + (this.chatActivityEnterTopView.getVisibility() == 0 ? this.chatActivityEnterTopView.getTranslationY() : 0.0f);
        FrameLayout frameLayout = this.pagedownButton;
        if (frameLayout != null) {
            frameLayout.setTranslationY((((float) AndroidUtilities.dp(100.0f)) * (1.0f - this.pagedownButtonEnterProgress)) + animatedTop);
        }
        FrameLayout frameLayout2 = this.mentiondownButton;
        if (frameLayout2 != null) {
            frameLayout2.setTranslationY(((((float) AndroidUtilities.dp(100.0f)) * (1.0f - this.mentionsButtonEnterProgress)) + animatedTop) - ((((float) AndroidUtilities.dp(72.0f)) * this.pagedownButtonEnterProgress) * this.mentionsButtonEnterProgress));
        }
        FrameLayout frameLayout3 = this.reactionsMentiondownButton;
        if (frameLayout3 != null) {
            frameLayout3.setTranslationY((animatedTop + (((float) AndroidUtilities.dp(100.0f)) * (1.0f - this.reactionsMentionButtonEnterProgress))) - ((((((float) AndroidUtilities.dp(50.0f)) + (((float) AndroidUtilities.dp(22.0f)) * this.pagedownButtonCounter.getEnterProgress())) * this.pagedownButtonEnterProgress) + (((float) AndroidUtilities.dp(72.0f)) * this.mentionsButtonEnterProgress)) * this.reactionsMentionButtonEnterProgress));
        }
    }

    private void updateReactionsMentionButton(boolean z) {
        if (this.reactionsMentiondownButtonCounter != null && getParentActivity() != null) {
            int i = this.reactionsMentionCount;
            boolean z2 = i > 0 && this.chatMode == 0;
            this.reactionsMentiondownButtonCounter.setCount(i, z);
            if (z2 && this.reactionsMentiondownButton.getTag() == null) {
                this.reactionsMentiondownButton.setTag(1);
                ValueAnimator valueAnimator = this.reactionsMentionButtonAnimation;
                if (valueAnimator != null) {
                    valueAnimator.removeAllListeners();
                    this.reactionsMentionButtonAnimation.cancel();
                    this.reactionsMentionButtonAnimation = null;
                }
                if (z) {
                    this.reactionsMentiondownButton.setVisibility(0);
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(this.reactionsMentionButtonEnterProgress, 1.0f);
                    this.reactionsMentionButtonAnimation = ofFloat;
                    ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda10
                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                            ChatActivity.this.lambda$updateReactionsMentionButton$78(valueAnimator2);
                        }
                    });
                    this.reactionsMentionButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.50
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            ChatActivity.this.reactionsMentionButtonEnterProgress = 1.0f;
                            ChatActivity.this.contentView.invalidate();
                        }
                    });
                    this.reactionsMentionButtonAnimation.setDuration(200L);
                    this.reactionsMentionButtonAnimation.start();
                    return;
                }
                this.reactionsMentiondownButton.setVisibility(0);
                this.reactionsMentionButtonEnterProgress = 1.0f;
                this.contentView.invalidate();
            } else if (!z2 && this.reactionsMentiondownButton.getTag() != null) {
                this.reactionsMentiondownButton.setTag(null);
                ValueAnimator valueAnimator2 = this.reactionsMentionButtonAnimation;
                if (valueAnimator2 != null) {
                    valueAnimator2.removeAllListeners();
                    this.reactionsMentionButtonAnimation.cancel();
                    this.reactionsMentionButtonAnimation = null;
                }
                if (z) {
                    this.reactionsMentiondownButton.setVisibility(0);
                    ValueAnimator ofFloat2 = ValueAnimator.ofFloat(this.reactionsMentionButtonEnterProgress, 0.0f);
                    this.reactionsMentionButtonAnimation = ofFloat2;
                    ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda13
                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator3) {
                            ChatActivity.this.lambda$updateReactionsMentionButton$79(valueAnimator3);
                        }
                    });
                    this.reactionsMentionButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.51
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            ChatActivity.this.reactionsMentiondownButton.setVisibility(4);
                            ChatActivity.this.reactionsMentionButtonEnterProgress = 0.0f;
                            ChatActivity.this.contentView.invalidate();
                        }
                    });
                    this.reactionsMentionButtonAnimation.setDuration(200L);
                    this.reactionsMentionButtonAnimation.start();
                    return;
                }
                this.reactionsMentiondownButton.setVisibility(4);
                this.reactionsMentionButtonEnterProgress = 0.0f;
                this.contentView.invalidate();
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateReactionsMentionButton$78(ValueAnimator valueAnimator) {
        this.reactionsMentionButtonEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.contentView.invalidate();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateReactionsMentionButton$79(ValueAnimator valueAnimator) {
        this.reactionsMentionButtonEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.contentView.invalidate();
    }

    private void openForwardingPreview() {
        TLRPC$TL_channels_sendAsPeers tLRPC$TL_channels_sendAsPeers;
        final boolean isKeyboardVisible = this.chatActivityEnterView.isKeyboardVisible();
        this.forwardingPreviewView = new ForwardingPreviewView(this.contentView.getContext(), this.forwardingMessages, this.currentUser, this.currentChat, this.currentAccount, this.themeDelegate) { // from class: org.telegram.ui.ChatActivity.52
            @Override // org.telegram.ui.Components.ForwardingPreviewView
            protected void onDismiss(boolean z) {
                ChatActivity.this.checkShowBlur(true);
                if (ChatActivity.this.forwardingMessages != null) {
                    ArrayList<MessageObject> arrayList = new ArrayList<>();
                    ChatActivity.this.forwardingMessages.getSelectedMessages(arrayList);
                    ChatActivity.this.showFieldPanelForForward(true, arrayList);
                }
                if (isKeyboardVisible && z) {
                    AndroidUtilities.runOnUIThread(new ChatActivity$52$$ExternalSyntheticLambda0(this), 50);
                }
                AndroidUtilities.requestAdjustResize(ChatActivity.this.getParentActivity(), ((BaseFragment) ChatActivity.this).classGuid);
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onDismiss$0() {
                ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                if (chatActivityEnterView != null) {
                    chatActivityEnterView.openKeyboard();
                }
            }

            @Override // org.telegram.ui.Components.ForwardingPreviewView
            protected void selectAnotherChat() {
                super.selectAnotherChat();
                dismiss(false);
                if (ChatActivity.this.forwardingMessages != null) {
                    int size = ChatActivity.this.forwardingMessages.messages.size();
                    int i = 0;
                    boolean z = false;
                    for (int i2 = 0; i2 < size; i2++) {
                        MessageObject messageObject = ChatActivity.this.forwardingMessages.messages.get(i2);
                        if (messageObject.isPoll()) {
                            if (i != 2) {
                                i = messageObject.isPublicPoll() ? 2 : 1;
                            }
                        } else if (messageObject.isInvoice()) {
                            z = true;
                        }
                        ChatActivity.this.selectedMessagesIds[0].put(messageObject.getId(), messageObject);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("onlySelect", true);
                    bundle.putInt("dialogsType", 3);
                    bundle.putInt("hasPoll", i);
                    bundle.putBoolean("hasInvoice", z);
                    bundle.putInt("messagesCount", ChatActivity.this.forwardingMessages.messages.size());
                    DialogsActivity dialogsActivity = new DialogsActivity(bundle);
                    dialogsActivity.setDelegate(ChatActivity.this);
                    ChatActivity.this.presentFragment(dialogsActivity);
                }
            }

            @Override // org.telegram.ui.Components.ForwardingPreviewView
            protected void didSendPressed() {
                super.didSendPressed();
                dismiss(true);
                ChatActivity.this.chatActivityEnterView.getSendButton().callOnClick();
            }
        };
        TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
        TLRPC$Peer tLRPC$Peer = tLRPC$ChatFull != null ? tLRPC$ChatFull.default_send_as : null;
        if (tLRPC$Peer == null && (tLRPC$TL_channels_sendAsPeers = this.sendAsPeersObj) != null && !tLRPC$TL_channels_sendAsPeers.peers.isEmpty()) {
            tLRPC$Peer = this.sendAsPeersObj.peers.get(0);
        }
        this.forwardingPreviewView.setSendAsPeer(tLRPC$Peer);
        checkShowBlur(true);
        this.contentView.addView(this.forwardingPreviewView);
        if (isKeyboardVisible) {
            this.chatActivityEnterView.showEmojiView();
            this.openKeyboardOnAttachMenuClose = true;
        }
        AndroidUtilities.setAdjustResizeToNothing(getParentActivity(), this.classGuid);
        this.fragmentView.requestLayout();
    }

    /* access modifiers changed from: private */
    public void animateToNextChat() {
        if (this.pullingDownDrawable != null) {
            addToPulledDialogsMyself();
            ChatPullingDownDrawable chatPullingDownDrawable = this.pullingDownDrawable;
            addToPulledDialogs(chatPullingDownDrawable.nextChat, chatPullingDownDrawable.nextDialogId, chatPullingDownDrawable.dialogFolderId, chatPullingDownDrawable.dialogFilterId);
            Bundle bundle = new Bundle();
            bundle.putLong("chat_id", this.pullingDownDrawable.getChatId());
            bundle.putInt("dialog_folder_id", this.pullingDownDrawable.dialogFolderId);
            bundle.putInt("dialog_filter_id", this.pullingDownDrawable.dialogFilterId);
            bundle.putBoolean("pulled", true);
            SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
            edit.remove("diditem" + this.pullingDownDrawable.nextDialogId).apply();
            ChatActivity chatActivity = new ChatActivity(bundle);
            chatActivity.setPullingDownTransition(true);
            replacingChatActivity = true;
            presentFragment(chatActivity, true);
        }
    }

    private void addToPulledDialogsMyself() {
        if (getParentLayout() != null) {
            BackButtonMenu.addToPulledDialogs(this, getParentLayout().fragmentsStack.indexOf(this), this.currentChat, this.currentUser, this.dialog_id, this.dialogFilterId, this.dialogFolderId);
        }
    }

    private void addToPulledDialogs(TLRPC$Chat tLRPC$Chat, long j, int i, int i2) {
        if (getParentLayout() != null) {
            BackButtonMenu.addToPulledDialogs(this, getParentLayout().fragmentsStack.indexOf(this), tLRPC$Chat, null, j, i, i2);
        }
    }

    private void setPullingDownTransition(boolean z) {
        this.fromPullingDownTransition = z;
    }

    /* access modifiers changed from: private */
    public void updateBulletinLayout() {
        Bulletin visibleBulletin = Bulletin.getVisibleBulletin();
        if (visibleBulletin != null && this.bulletinDelegate != null) {
            visibleBulletin.updatePosition();
        }
    }

    private void searchUserMessages(TLRPC$User tLRPC$User, TLRPC$Chat tLRPC$Chat) {
        String str;
        this.searchingUserMessages = tLRPC$User;
        this.searchingChatMessages = tLRPC$Chat;
        if (this.searchItem != null && this.mentionContainer != null) {
            if (tLRPC$User != null || tLRPC$Chat != null) {
                if (tLRPC$User != null) {
                    str = tLRPC$User.first_name;
                    if (TextUtils.isEmpty(str)) {
                        str = this.searchingUserMessages.last_name;
                    }
                } else {
                    str = tLRPC$Chat.title;
                }
                if (str != null) {
                    if (str.length() > 10) {
                        str = str.substring(0, 10);
                    }
                    this.searchingForUser = false;
                    String string = LocaleController.getString("SearchFrom", R.string.SearchFrom);
                    SpannableString spannableString = new SpannableString(string + " " + str);
                    spannableString.setSpan(new ForegroundColorSpan(getThemedColor("actionBarDefaultSubtitle")), string.length() + 1, spannableString.length(), 33);
                    this.searchItem.setSearchFieldCaption(spannableString);
                    this.mentionContainer.getAdapter().searchUsernameOrHashtag(null, 0, null, false, true);
                    this.searchItem.setSearchFieldHint(null);
                    this.searchItem.clearSearchText();
                    getMediaDataController().searchMessagesInChat("", this.dialog_id, this.mergeDialogId, this.classGuid, 0, this.threadMessageId, this.searchingUserMessages, this.searchingChatMessages);
                }
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v16, types: [java.lang.CharSequence] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updateInfoTopView(boolean r13) {
        /*
        // Method dump skipped, instructions count: 388
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.updateInfoTopView(boolean):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateInfoTopView$80(View view) {
        presentFragment(new PeopleNearbyActivity());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateInfoTopView$81(View view) {
        Bundle bundle = new Bundle();
        bundle.putLong("user_id", this.chatInviterId);
        presentFragment(new ProfileActivity(bundle));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateInfoTopView$82(View view, ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.topViewOffset = ((float) AndroidUtilities.dp(30.0f)) * floatValue;
        invalidateChatListViewTopPadding();
        view.setAlpha(floatValue);
    }

    private void openAnotherForward() {
        ArrayList<MessageObject> arrayList;
        String str;
        ForwardingMessagesParams forwardingMessagesParams = this.forwardingMessages;
        if (!(forwardingMessagesParams == null || (arrayList = forwardingMessagesParams.messages) == null)) {
            int size = arrayList.size();
            long j = 0;
            long j2 = 0;
            for (int i = 0; i < size; i++) {
                MessageObject messageObject = this.forwardingMessages.messages.get(i);
                if (j == 0) {
                    j2 = messageObject.getDialogId();
                    j = messageObject.getFromChatId();
                } else if (j != messageObject.getFromChatId()) {
                    break;
                }
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
            builder.setButtonsVertical(true);
            if (j2 > 0) {
                TLRPC$User user = getMessagesController().getUser(Long.valueOf(j2));
                if (user != null) {
                    str = LocaleController.formatString("CancelForwardPrivate", R.string.CancelForwardPrivate, LocaleController.formatPluralString("MessagesBold", this.forwardingMessages.messages.size(), new Object[0]), ContactsController.formatName(user.first_name, user.last_name));
                } else {
                    return;
                }
            } else {
                TLRPC$Chat chat = getMessagesController().getChat(Long.valueOf(-j2));
                if (chat != null) {
                    str = LocaleController.formatString("CancelForwardChat", R.string.CancelForwardChat, LocaleController.formatPluralString("MessagesBold", this.forwardingMessages.messages.size(), new Object[0]), chat.title);
                } else {
                    return;
                }
            }
            builder.setMessage(AndroidUtilities.replaceTags(str));
            builder.setTitle(LocaleController.formatPluralString("messages", this.forwardingMessages.messages.size(), new Object[0]));
            builder.setPositiveButton(LocaleController.getString("CancelForwarding", R.string.CancelForwarding), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda28
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ChatActivity.this.lambda$openAnotherForward$83(dialogInterface, i2);
                }
            });
            builder.setNegativeButton(LocaleController.getString("ShowForwardingOptions", R.string.ShowForwardingOptions), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda32
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ChatActivity.this.lambda$openAnotherForward$84(dialogInterface, i2);
                }
            });
            AlertDialog create = builder.create();
            showDialog(create);
            TextView textView = (TextView) create.getButton(-1);
            if (textView != null) {
                textView.setTextColor(getThemedColor("dialogTextRed2"));
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openAnotherForward$83(DialogInterface dialogInterface, int i) {
        if (this.forwardingMessages != null) {
            this.forwardingMessages = null;
        }
        showFieldPanel(false, null, null, null, this.foundWebPage, true, 0, true, true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openAnotherForward$84(DialogInterface dialogInterface, int i) {
        openForwardingPreview();
    }

    /* access modifiers changed from: private */
    public void openPinnedMessagesList(boolean z) {
        ActionBarLayout actionBarLayout;
        if (!(getParentActivity() == null || (actionBarLayout = this.parentLayout) == null || actionBarLayout.getLastFragment() != this || this.pinnedMessageIds.isEmpty())) {
            Bundle bundle = new Bundle();
            TLRPC$Chat tLRPC$Chat = this.currentChat;
            if (tLRPC$Chat != null) {
                bundle.putLong("chat_id", tLRPC$Chat.id);
            } else {
                bundle.putLong("user_id", this.currentUser.id);
            }
            bundle.putInt("chatMode", 2);
            ChatActivity chatActivity = new ChatActivity(bundle);
            chatActivity.pinnedMessageIds = new ArrayList<>(this.pinnedMessageIds);
            chatActivity.pinnedMessageObjects = new HashMap<>(this.pinnedMessageObjects);
            int size = this.pinnedMessageIds.size();
            for (int i = 0; i < size; i++) {
                Integer num = this.pinnedMessageIds.get(i);
                MessageObject messageObject = this.pinnedMessageObjects.get(num);
                MessageObject messageObject2 = this.messagesDict[0].get(num.intValue());
                if (messageObject == null) {
                    messageObject = messageObject2;
                } else if (messageObject2 != null) {
                    messageObject.mediaExists = messageObject2.mediaExists;
                    messageObject.attachPathExists = messageObject2.attachPathExists;
                }
                if (messageObject != null) {
                    chatActivity.pinnedMessageObjects.put(num, messageObject);
                }
            }
            chatActivity.loadedPinnedMessagesCount = this.loadedPinnedMessagesCount;
            chatActivity.totalPinnedMessagesCount = this.totalPinnedMessagesCount;
            chatActivity.pinnedEndReached = this.pinnedEndReached;
            chatActivity.userInfo = this.userInfo;
            chatActivity.chatInfo = this.chatInfo;
            chatActivity.chatActivityDelegate = new ChatActivityDelegate() { // from class: org.telegram.ui.ChatActivity.54
                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public /* synthetic */ void onReport() {
                    ChatActivityDelegate.CC.$default$onReport(this);
                }

                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public void openReplyMessage(int i2) {
                    ChatActivity.this.scrollToMessageId(i2, 0, true, 0, true, 0);
                }

                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public void openSearch(String str) {
                    ChatActivity.this.openSearchWithText(str);
                }

                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public void onUnpin(boolean z2, boolean z3) {
                    if (z2) {
                        ArrayList arrayList = new ArrayList(ChatActivity.this.pinnedMessageIds);
                        ArrayList arrayList2 = new ArrayList(ChatActivity.this.pinnedMessageObjects.values());
                        if (z3) {
                            SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(((BaseFragment) ChatActivity.this).currentAccount).edit();
                            edit.putInt("pin_" + ChatActivity.this.dialog_id, ((Integer) ChatActivity.this.pinnedMessageIds.get(0)).intValue()).commit();
                            ChatActivity.this.updatePinnedMessageView(true);
                        } else {
                            ChatActivity.this.getNotificationCenter().postNotificationName(NotificationCenter.didLoadPinnedMessages, Long.valueOf(ChatActivity.this.dialog_id), arrayList, Boolean.FALSE, null, null, 0, 0, Boolean.TRUE);
                        }
                        if (ChatActivity.this.pinBulletin != null) {
                            ChatActivity.this.pinBulletin.hide();
                        }
                        ChatActivity.this.showPinBulletin = true;
                        int access$31804 = ChatActivity.access$31804(ChatActivity.this);
                        int pinnedMessagesCount = ChatActivity.this.getPinnedMessagesCount();
                        ChatActivity chatActivity2 = ChatActivity.this;
                        chatActivity2.pinBulletin = BulletinFactory.createUnpinAllMessagesBulletin(chatActivity2, pinnedMessagesCount, z3, new ChatActivity$54$$ExternalSyntheticLambda1(this, z3, arrayList, arrayList2, pinnedMessagesCount, access$31804), new ChatActivity$54$$ExternalSyntheticLambda0(this, z3, access$31804), ChatActivity.this.themeDelegate);
                        return;
                    }
                    MessageObject messageObject3 = (MessageObject) ChatActivity.this.pinnedMessageObjects.get(Integer.valueOf(ChatActivity.this.currentPinnedMessageId));
                    if (messageObject3 == null) {
                        messageObject3 = (MessageObject) ChatActivity.this.messagesDict[0].get(ChatActivity.this.currentPinnedMessageId);
                    }
                    ChatActivity.this.unpinMessage(messageObject3);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$onUnpin$0(boolean z2, ArrayList arrayList, ArrayList arrayList2, int i2, int i3) {
                    if (z2) {
                        SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(((BaseFragment) ChatActivity.this).currentAccount).edit();
                        edit.remove("pin_" + ChatActivity.this.dialog_id).commit();
                        ChatActivity.this.updatePinnedMessageView(true);
                    } else {
                        ChatActivity.this.getNotificationCenter().postNotificationName(NotificationCenter.didLoadPinnedMessages, Long.valueOf(ChatActivity.this.dialog_id), arrayList, Boolean.TRUE, arrayList2, null, 0, Integer.valueOf(i2), Boolean.valueOf(ChatActivity.this.pinnedEndReached));
                    }
                    if (i3 == ChatActivity.this.pinBullerinTag) {
                        ChatActivity.this.pinBulletin = null;
                    }
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$onUnpin$1(boolean z2, int i2) {
                    if (!z2) {
                        MessagesController messagesController = ChatActivity.this.getMessagesController();
                        ChatActivity chatActivity2 = ChatActivity.this;
                        messagesController.unpinAllMessages(chatActivity2.currentChat, chatActivity2.currentUser);
                    }
                    if (i2 == ChatActivity.this.pinBullerinTag) {
                        ChatActivity.this.pinBulletin = null;
                    }
                }
            };
            if (z) {
                presentFragmentAsPreview(chatActivity);
                checkShowBlur(true);
                return;
            }
            presentFragment(chatActivity, false);
        }
    }

    /* access modifiers changed from: private */
    public void checkShowBlur(boolean z) {
        BluredView bluredView;
        BluredView bluredView2;
        ForwardingPreviewView forwardingPreviewView;
        ActionBarLayout actionBarLayout = this.parentLayout;
        boolean z2 = (actionBarLayout != null && actionBarLayout.isInPreviewMode() && !this.inPreviewMode) || ((forwardingPreviewView = this.forwardingPreviewView) != null && forwardingPreviewView.isShowing());
        if (z2 && ((bluredView2 = this.blurredView) == null || bluredView2.getTag() == null)) {
            BluredView bluredView3 = this.blurredView;
            if (bluredView3 == null) {
                AnonymousClass55 r7 = new BluredView(this.fragmentView.getContext(), this.fragmentView, this.themeDelegate) { // from class: org.telegram.ui.ChatActivity.55
                    @Override // android.view.View
                    public void setAlpha(float f) {
                        super.setAlpha(f);
                        ((BaseFragment) ChatActivity.this).fragmentView.invalidate();
                    }

                    @Override // android.view.View
                    public void setVisibility(int i) {
                        super.setVisibility(i);
                        ((BaseFragment) ChatActivity.this).fragmentView.invalidate();
                    }
                };
                this.blurredView = r7;
                this.contentView.addView(r7, LayoutHelper.createFrame(-1, -1.0f));
            } else {
                if (this.contentView.indexOfChild(bluredView3) != this.contentView.getChildCount() - 1) {
                    this.contentView.removeView(this.blurredView);
                    this.contentView.addView(this.blurredView);
                }
                this.blurredView.update();
                this.blurredView.setVisibility(0);
            }
            this.blurredView.setAlpha(0.0f);
            this.blurredView.animate().setListener(null).cancel();
            this.blurredView.animate().alpha(1.0f).setListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.56
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    super.onAnimationEnd(animator);
                    ChatActivity.this.chatListView.invalidate();
                    ((BaseFragment) ChatActivity.this).fragmentView.invalidate();
                }
            }).start();
            this.blurredView.setTag(1);
        } else if (!z2 && (bluredView = this.blurredView) != null && bluredView.getTag() != null) {
            this.blurredView.animate().setListener(null).cancel();
            this.blurredView.animate().setListener(new HideViewAfterAnimation(this.blurredView)).alpha(0.0f).start();
            this.blurredView.setTag(null);
            this.chatListView.invalidate();
            this.fragmentView.invalidate();
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected int getPreviewHeight() {
        if (this.chatMode == 2 && this.messages.size() == 2) {
            return getHeightForMessage(this.messages.get(0)) + AndroidUtilities.dp(80.0f) + ActionBar.getCurrentActionBarHeight();
        }
        return super.getPreviewHeight();
    }

    private void showProgressView(boolean z) {
        FrameLayout frameLayout = this.progressView;
        if (frameLayout != null) {
            int i = 0;
            if (!this.fragmentOpened) {
                this.animateTo = z;
                if (!z) {
                    i = 4;
                }
                frameLayout.setVisibility(i);
            } else if (z != this.animateTo) {
                this.animateTo = z;
                if (z) {
                    if (frameLayout.getVisibility() != 0) {
                        this.progressView.setVisibility(0);
                        this.progressView.setAlpha(0.0f);
                        this.progressView.setScaleX(0.3f);
                        this.progressView.setScaleY(0.3f);
                    }
                    this.progressView.animate().setListener(null).cancel();
                    this.progressView.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setDuration(150).start();
                    return;
                }
                frameLayout.animate().setListener(null).cancel();
                this.progressView.animate().alpha(0.0f).scaleX(0.3f).scaleY(0.3f).setDuration(150).setListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.57
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        ChatActivity.this.progressView.setVisibility(4);
                    }
                }).start();
            }
        }
    }

    private void hideInfoView() {
        if (this.distanceToPeer >= 0) {
            this.distanceToPeer = -1;
            SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
            edit.putInt("dialog_bar_distance" + this.dialog_id, -2).commit();
        }
        Animator animator = this.infoTopViewAnimator;
        if (animator != null) {
            animator.cancel();
        }
        ChatActionCell chatActionCell = this.infoTopView;
        if (chatActionCell != null && chatActionCell.getTag() != null) {
            this.infoTopView.setTag(null);
            final ChatActionCell chatActionCell2 = this.infoTopView;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 0.0f);
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(chatActionCell2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda15
                public final /* synthetic */ View f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ChatActivity.this.lambda$hideInfoView$85(this.f$1, valueAnimator);
                }
            });
            ofFloat.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.58
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator2) {
                    ChatActivity.this.topViewOffset = 0.0f;
                    if (animator2 == ChatActivity.this.infoTopViewAnimator) {
                        ViewGroup viewGroup = (ViewGroup) chatActionCell2.getParent();
                        if (viewGroup != null) {
                            viewGroup.removeView(chatActionCell2);
                        }
                        ChatActivity.this.infoTopView = null;
                        ChatActivity.this.infoTopViewAnimator = null;
                    }
                }
            });
            ofFloat.setDuration(150L);
            this.infoTopViewAnimator = ofFloat;
            ofFloat.start();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$hideInfoView$85(View view, ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.topViewOffset = ((float) AndroidUtilities.dp(30.0f)) * floatValue;
        invalidateChatListViewTopPadding();
        invalidateMessagesVisiblePart();
        view.setAlpha(floatValue);
    }

    /* access modifiers changed from: private */
    public void updateChatListViewTopPadding() {
        float f;
        RecyclerListView recyclerListView;
        BlurredFrameLayout blurredFrameLayout;
        int i;
        int measuredHeight;
        int paddingBottom;
        ChatActivityEnterView chatActivityEnterView;
        if (this.invalidateChatListViewTopPadding && this.chatListView != null) {
            if (this.fixedKeyboardHeight <= 0 || this.searchExpandProgress != 0.0f) {
                float max = Math.max(0.0f, ((float) AndroidUtilities.dp(48.0f)) + this.topChatPanelViewOffset);
                BlurredFrameLayout blurredFrameLayout2 = this.pinnedMessageView;
                float max2 = (blurredFrameLayout2 == null || blurredFrameLayout2.getVisibility() != 0) ? 0.0f : Math.max(0.0f, ((float) AndroidUtilities.dp(48.0f)) + this.pinnedMessageEnterOffset);
                ChatActivityMemberRequestsDelegate chatActivityMemberRequestsDelegate = this.pendingRequestsDelegate;
                MessageObject messageObject = null;
                View view = chatActivityMemberRequestsDelegate != null ? chatActivityMemberRequestsDelegate.getView() : null;
                float max3 = (view == null || view.getVisibility() != 0) ? 0.0f : Math.max(0.0f, ((float) view.getHeight()) + this.pendingRequestsDelegate.getViewEnterOffset());
                float f2 = this.chatListViewPaddingTop;
                float dp = ((float) (AndroidUtilities.dp(4.0f) + this.contentPaddingTop)) + max + max2 + max3;
                this.chatListViewPaddingTop = dp;
                float f3 = dp + ((float) this.blurredViewTopOffset);
                this.chatListViewPaddingTop = f3;
                this.chatListViewPaddingVisibleOffset = 0;
                this.chatListViewPaddingTop = f3 + this.contentPanTranslation + this.bottomPanelTranslationY;
                if (this.searchExpandProgress == 0.0f || this.chatActivityEnterView.getVisibility() != 0) {
                    f = 0.0f;
                } else {
                    float f4 = this.chatListViewPaddingTop;
                    f = this.searchExpandProgress * ((float) (this.chatActivityEnterView.getMeasuredHeight() - this.searchContainer.getMeasuredHeight()));
                    this.chatListViewPaddingTop = f4 - f;
                }
                if (this.bottomPanelTranslationY == 0.0f && !this.chatActivityEnterView.panelAnimationInProgress() && (this.contentView.getLayoutParams().height < 0 || (this.contentView.getKeyboardHeight() <= AndroidUtilities.dp(20.0f) && this.chatActivityEnterView.isPopupShowing()))) {
                    this.chatListViewPaddingTop += (float) ((this.contentView.getKeyboardHeight() > AndroidUtilities.dp(20.0f) || AndroidUtilities.isInMultiwindow || this.inBubbleMode) ? this.contentView.getKeyboardHeight() : this.chatActivityEnterView.getEmojiPadding());
                }
                if (!this.inPreviewMode && (chatActivityEnterView = this.chatActivityEnterView) != null) {
                    if (chatActivityEnterView.getAnimatedTop() != 0) {
                        this.chatListViewPaddingTop += (float) ((this.chatActivityEnterView.getHeightWithTopView() - AndroidUtilities.dp(51.0f)) - this.chatActivityEnterView.getAnimatedTop());
                    } else if (!this.chatActivityEnterView.panelAnimationInProgress()) {
                        float heightWithTopView = this.chatListViewPaddingTop + ((float) (this.chatActivityEnterView.getHeightWithTopView() - AndroidUtilities.dp(51.0f)));
                        this.chatListViewPaddingTop = heightWithTopView;
                        if (this.chatActivityEnterView.currentTopViewAnimation == null) {
                            this.chatListViewPaddingTop = heightWithTopView - this.chatListView.getTranslationY();
                        }
                    }
                }
                ChatActionCell chatActionCell = this.infoTopView;
                if (chatActionCell != null) {
                    chatActionCell.setTranslationY((this.chatListViewPaddingTop - ((float) AndroidUtilities.dp(30.0f))) + this.topViewOffset);
                    float f5 = this.chatListViewPaddingTop;
                    float f6 = this.topViewOffset;
                    this.chatListViewPaddingTop = f5 + f6;
                    this.chatListViewPaddingVisibleOffset = (int) (((float) this.chatListViewPaddingVisibleOffset) + f6);
                }
                ChatActionCell chatActionCell2 = this.floatingDateView;
                if (chatActionCell2 != null) {
                    chatActionCell2.setTranslationY((((this.chatListView.getTranslationY() - f) + this.chatListViewPaddingTop) + this.floatingDateViewOffset) - ((float) AndroidUtilities.dp(4.0f)));
                }
                int measuredHeight2 = (this.chatListView.getMeasuredHeight() * 2) / 3;
                RecyclerListView recyclerListView2 = this.chatListView;
                if (!(recyclerListView2 == null || this.chatLayoutManager == null || this.chatAdapter == null)) {
                    if (recyclerListView2.getPaddingTop() != measuredHeight2) {
                        int findFirstVisibleItemPosition = this.chatLayoutManager.findFirstVisibleItemPosition();
                        int findLastVisibleItemPosition = this.chatLayoutManager.findLastVisibleItemPosition();
                        if (findFirstVisibleItemPosition != -1) {
                            for (int i2 = findFirstVisibleItemPosition; i2 <= findLastVisibleItemPosition; i2++) {
                                View findViewByPosition = this.chatLayoutManager.findViewByPosition(i2);
                                if (findViewByPosition instanceof ChatMessageCell) {
                                    messageObject = ((ChatMessageCell) findViewByPosition).getMessageObject();
                                    measuredHeight = this.chatListView.getMeasuredHeight() - findViewByPosition.getBottom();
                                    paddingBottom = this.chatListView.getPaddingBottom();
                                } else if (findViewByPosition instanceof ChatActionCell) {
                                    messageObject = ((ChatActionCell) findViewByPosition).getMessageObject();
                                    measuredHeight = this.chatListView.getMeasuredHeight() - findViewByPosition.getBottom();
                                    paddingBottom = this.chatListView.getPaddingBottom();
                                }
                                i = measuredHeight - paddingBottom;
                                break;
                            }
                        }
                        i = 0;
                        this.chatListView.setPadding(0, measuredHeight2, 0, AndroidUtilities.dp(3.0f) + this.blurredViewBottomOffset);
                        if (!(findFirstVisibleItemPosition == -1 || messageObject == null)) {
                            this.chatAdapter.updateRowsSafe();
                            int indexOf = this.messages.indexOf(messageObject);
                            if (indexOf >= 0) {
                                this.chatLayoutManager.scrollToPositionWithOffset(this.chatAdapter.messagesStartRow + indexOf, i);
                            }
                        }
                        invalidateMessagesVisiblePart();
                    }
                    this.chatListView.setTopGlowOffset((int) ((this.chatListViewPaddingTop - ((float) this.chatListViewPaddingVisibleOffset)) - ((float) AndroidUtilities.dp(4.0f))));
                    if (f2 != this.chatListViewPaddingTop) {
                        int childCount = this.chatListView.getChildCount();
                        int i3 = 0;
                        while (true) {
                            if (i3 >= childCount) {
                                break;
                            }
                            View childAt = this.chatListView.getChildAt(i3);
                            if (this.chatListView.getChildAdapterPosition(childAt) == this.chatAdapter.getItemCount() - 1) {
                                float f7 = this.chatListViewPaddingTop;
                                if (isThreadChat() && (blurredFrameLayout = this.pinnedMessageView) != null && blurredFrameLayout.getVisibility() == 0) {
                                    f7 -= Math.max(0.0f, ((float) AndroidUtilities.dp(48.0f)) + this.pinnedMessageEnterOffset);
                                }
                                if (((float) childAt.getTop()) > f7) {
                                    this.chatListView.scrollBy(0, (int) (((float) childAt.getTop()) - f7));
                                }
                            } else {
                                i3++;
                            }
                        }
                    }
                    if (!isThreadChat() && !this.wasManualScroll && this.unreadMessageObject != null && (recyclerListView = this.chatListView) != null) {
                        recyclerListView.scrollBy(0, (int) (f2 - this.chatListViewPaddingTop));
                    }
                }
                this.invalidateChatListViewTopPadding = false;
            }
        }
    }

    /* access modifiers changed from: private */
    public void invalidateChatListViewTopPadding() {
        if (!this.invalidateChatListViewTopPadding) {
            this.invalidateChatListViewTopPadding = true;
            SizeNotifierFrameLayout sizeNotifierFrameLayout = this.contentView;
            if (sizeNotifierFrameLayout != null) {
                sizeNotifierFrameLayout.invalidate();
            }
            RecyclerListView recyclerListView = this.chatListView;
            if (recyclerListView != null) {
                recyclerListView.invalidate();
            }
        }
        float f = 0.0f;
        float max = this.contentPanTranslation + ((float) this.contentPaddingTop) + Math.max(0.0f, ((float) AndroidUtilities.dp(48.0f)) + this.topChatPanelViewOffset);
        BlurredFrameLayout blurredFrameLayout = this.pinnedMessageView;
        if (blurredFrameLayout != null) {
            float f2 = max + this.pinnedMessageEnterOffset;
            blurredFrameLayout.setTranslationY(f2);
            max = f2 + ((float) AndroidUtilities.dp(48.0f));
        }
        ChatActivityMemberRequestsDelegate chatActivityMemberRequestsDelegate = this.pendingRequestsDelegate;
        View view = chatActivityMemberRequestsDelegate != null ? chatActivityMemberRequestsDelegate.getView() : null;
        if (view != null) {
            view.setTranslationY(max + this.pendingRequestsDelegate.getViewEnterOffset());
        }
        if (this.fragmentContextView != null) {
            FragmentContextView fragmentContextView = this.fragmentLocationContextView;
            float dp = (fragmentContextView == null || fragmentContextView.getVisibility() != 0) ? 0.0f : ((float) AndroidUtilities.dp(36.0f)) + 0.0f;
            FragmentContextView fragmentContextView2 = this.fragmentContextView;
            fragmentContextView2.setTranslationY(this.contentPanTranslation + dp + fragmentContextView2.getTopPadding());
        }
        if (this.fragmentLocationContextView != null) {
            FragmentContextView fragmentContextView3 = this.fragmentContextView;
            if (fragmentContextView3 != null && fragmentContextView3.getVisibility() == 0) {
                f = 0.0f + ((float) AndroidUtilities.dp((float) this.fragmentContextView.getStyleHeight())) + this.fragmentContextView.getTopPadding();
            }
            FragmentContextView fragmentContextView4 = this.fragmentLocationContextView;
            fragmentContextView4.setTranslationY(this.contentPanTranslation + f + fragmentContextView4.getTopPadding());
        }
        BlurredFrameLayout blurredFrameLayout2 = this.topChatPanelView;
        if (blurredFrameLayout2 != null) {
            blurredFrameLayout2.setTranslationY(this.contentPanTranslation + ((float) this.contentPaddingTop) + this.topChatPanelViewOffset);
        }
        FrameLayout frameLayout = this.alertView;
        if (frameLayout != null && frameLayout.getVisibility() == 0) {
            this.alertView.setTranslationY((this.contentPanTranslation + ((float) this.contentPaddingTop)) - (((float) AndroidUtilities.dp(50.0f)) * (1.0f - this.alertViewEnterProgress)));
        }
        BlurredFrameLayout blurredFrameLayout3 = this.bottomOverlayChat;
        if (blurredFrameLayout3 != null) {
            blurredFrameLayout3.setTranslationY(this.bottomPanelTranslationYReverse);
        }
        BlurredFrameLayout blurredFrameLayout4 = this.bottomMessagesActionContainer;
        if (blurredFrameLayout4 != null) {
            blurredFrameLayout4.setTranslationY(this.bottomPanelTranslationYReverse);
        }
        UndoView undoView = this.undoView;
        if (undoView != null) {
            undoView.setAdditionalTranslationY((float) (this.chatActivityEnterView.getHeightWithTopView() - this.chatActivityEnterView.getAnimatedTop()));
        }
    }

    /* access modifiers changed from: private */
    public TextureView createTextureView(boolean z) {
        if (this.parentLayout == null) {
            return null;
        }
        AndroidUtilities.cancelRunOnUIThread(this.destroyTextureViewRunnable);
        if (this.videoPlayerContainer == null) {
            if (Build.VERSION.SDK_INT >= 21) {
                AnonymousClass59 r0 = new FrameLayout(getParentActivity()) { // from class: org.telegram.ui.ChatActivity.59
                    @Override // android.view.View
                    public void setTranslationY(float f) {
                        super.setTranslationY(f);
                        ChatActivity.this.contentView.invalidate();
                    }
                };
                this.videoPlayerContainer = r0;
                r0.setOutlineProvider(new ViewOutlineProvider(this) { // from class: org.telegram.ui.ChatActivity.60
                    @Override // android.view.ViewOutlineProvider
                    @TargetApi(R.styleable.MapAttrs_uiZoomGestures)
                    public void getOutline(View view, Outline outline) {
                        ImageReceiver imageReceiver = (ImageReceiver) view.getTag(R.id.parent_tag);
                        if (imageReceiver != null) {
                            int[] roundRadius = imageReceiver.getRoundRadius();
                            int i = 0;
                            for (int i2 = 0; i2 < 4; i2++) {
                                i = Math.max(i, roundRadius[i2]);
                            }
                            outline.setRoundRect(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight(), (float) i);
                            return;
                        }
                        int i3 = AndroidUtilities.roundPlayingMessageSize;
                        outline.setOval(0, 0, i3, i3);
                    }
                });
                this.videoPlayerContainer.setClipToOutline(true);
            } else {
                this.videoPlayerContainer = new FrameLayout(getParentActivity()) { // from class: org.telegram.ui.ChatActivity.61
                    RectF rect = new RectF();

                    @Override // android.view.View
                    protected void onSizeChanged(int i, int i2, int i3, int i4) {
                        super.onSizeChanged(i, i2, i3, i4);
                        ChatActivity.this.aspectPath.reset();
                        ImageReceiver imageReceiver = (ImageReceiver) getTag(R.id.parent_tag);
                        if (imageReceiver != null) {
                            int[] roundRadius = imageReceiver.getRoundRadius();
                            int i5 = 0;
                            for (int i6 = 0; i6 < 4; i6++) {
                                i5 = Math.max(i5, roundRadius[i6]);
                            }
                            this.rect.set(0.0f, 0.0f, (float) i, (float) i2);
                            ChatActivity.this.aspectPath.addRoundRect(this.rect, (float) AndroidUtilities.dp(4.0f), (float) AndroidUtilities.dp(4.0f), Path.Direction.CW);
                        } else {
                            float f = (float) (i / 2);
                            ChatActivity.this.aspectPath.addCircle(f, (float) (i2 / 2), f, Path.Direction.CW);
                        }
                        ChatActivity.this.aspectPath.toggleInverseFillType();
                    }

                    @Override // android.view.View
                    public void setTranslationY(float f) {
                        super.setTranslationY(f);
                        ChatActivity.this.contentView.invalidate();
                    }

                    @Override // android.view.View
                    public void setVisibility(int i) {
                        super.setVisibility(i);
                        if (i == 0) {
                            setLayerType(2, null);
                        }
                    }

                    @Override // android.view.View, android.view.ViewGroup
                    protected void dispatchDraw(Canvas canvas) {
                        super.dispatchDraw(canvas);
                        if (getTag() == null) {
                            canvas.drawPath(ChatActivity.this.aspectPath, ChatActivity.this.aspectPaint);
                        }
                    }
                };
                this.aspectPath = new Path();
                Paint paint = new Paint(1);
                this.aspectPaint = paint;
                paint.setColor(-16777216);
                this.aspectPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            }
            this.videoPlayerContainer.setWillNotDraw(false);
            AspectRatioFrameLayout aspectRatioFrameLayout = new AspectRatioFrameLayout(getParentActivity());
            this.aspectRatioFrameLayout = aspectRatioFrameLayout;
            aspectRatioFrameLayout.setBackgroundColor(0);
            if (z) {
                this.videoPlayerContainer.addView(this.aspectRatioFrameLayout, LayoutHelper.createFrame(-1, -1, 17));
            }
            TextureView textureView = new TextureView(getParentActivity());
            this.videoTextureView = textureView;
            textureView.setOpaque(false);
            this.aspectRatioFrameLayout.addView(this.videoTextureView, LayoutHelper.createFrame(-1, -1.0f));
        }
        ViewGroup viewGroup = (ViewGroup) this.videoPlayerContainer.getParent();
        if (!(viewGroup == null || viewGroup == this.contentView)) {
            viewGroup.removeView(this.videoPlayerContainer);
            viewGroup = null;
        }
        if (viewGroup == null) {
            SizeNotifierFrameLayout sizeNotifierFrameLayout = this.contentView;
            FrameLayout frameLayout = this.videoPlayerContainer;
            int i = AndroidUtilities.roundPlayingMessageSize;
            sizeNotifierFrameLayout.addView(frameLayout, 1, new FrameLayout.LayoutParams(i, i));
        }
        this.videoPlayerContainer.setTag(null);
        this.aspectRatioFrameLayout.setDrawingReady(false);
        return this.videoTextureView;
    }

    /* access modifiers changed from: private */
    /* renamed from: destroyTextureView */
    public void lambda$new$0() {
        FrameLayout frameLayout = this.videoPlayerContainer;
        if (frameLayout != null && frameLayout.getParent() != null) {
            this.chatListView.invalidateViews();
            this.aspectRatioFrameLayout.setDrawingReady(false);
            this.videoPlayerContainer.setTag(null);
            if (Build.VERSION.SDK_INT < 21) {
                this.videoPlayerContainer.setLayerType(0, null);
            }
            this.contentView.removeView(this.videoPlayerContainer);
        }
    }

    private boolean hasSelectedNoforwardsMessage() {
        TLRPC$Message tLRPC$Message;
        for (int i = 0; i < this.selectedMessagesIds.length; i++) {
            try {
                for (int i2 = 0; i2 < this.selectedMessagesIds[i].size(); i2++) {
                    MessageObject valueAt = this.selectedMessagesIds[i].valueAt(i2);
                    if (!(valueAt == null || (tLRPC$Message = valueAt.messageOwner) == null || !tLRPC$Message.noforwards)) {
                        return true;
                    }
                }
            } catch (Exception unused) {
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void openForward(boolean z) {
        String str;
        if (getMessagesController().isChatNoForwards(this.currentChat) || hasSelectedNoforwardsMessage()) {
            if (!getMessagesController().isChatNoForwards(this.currentChat)) {
                str = LocaleController.getString("ForwardsRestrictedInfoBot", R.string.ForwardsRestrictedInfoBot);
            } else if (!ChatObject.isChannel(this.currentChat) || this.currentChat.megagroup) {
                str = LocaleController.getString("ForwardsRestrictedInfoGroup", R.string.ForwardsRestrictedInfoGroup);
            } else {
                str = LocaleController.getString("ForwardsRestrictedInfoChannel", R.string.ForwardsRestrictedInfoChannel);
            }
            if (z) {
                if (this.fwdRestrictedTopHint == null) {
                    SizeNotifierFrameLayout sizeNotifierFrameLayout = (SizeNotifierFrameLayout) this.fragmentView;
                    int indexOfChild = sizeNotifierFrameLayout.indexOfChild(this.chatActivityEnterView);
                    if (indexOfChild != -1) {
                        HintView hintView = new HintView((Context) getParentActivity(), 7, true);
                        this.fwdRestrictedTopHint = hintView;
                        sizeNotifierFrameLayout.addView(hintView, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 12.0f, 0.0f, 12.0f, 0.0f));
                        this.fwdRestrictedTopHint.setAlpha(0.0f);
                        this.fwdRestrictedTopHint.setVisibility(4);
                    } else {
                        return;
                    }
                }
                this.fwdRestrictedTopHint.setText(str);
                this.fwdRestrictedTopHint.showForView(this.actionBar.getActionMode().getItem(11), true);
                return;
            }
            if (this.fwdRestrictedBottomHint == null) {
                SizeNotifierFrameLayout sizeNotifierFrameLayout2 = (SizeNotifierFrameLayout) this.fragmentView;
                int indexOfChild2 = sizeNotifierFrameLayout2.indexOfChild(this.chatActivityEnterView);
                if (indexOfChild2 != -1) {
                    HintView hintView2 = new HintView(getParentActivity(), 9);
                    this.fwdRestrictedBottomHint = hintView2;
                    sizeNotifierFrameLayout2.addView(hintView2, indexOfChild2 + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 12.0f, 0.0f, 12.0f, 0.0f));
                    this.fwdRestrictedBottomHint.setAlpha(0.0f);
                    this.fwdRestrictedBottomHint.setVisibility(4);
                } else {
                    return;
                }
            }
            this.fwdRestrictedBottomHint.setText(str);
            this.fwdRestrictedBottomHint.showForView(this.forwardButton, true);
            return;
        }
        int i = 0;
        boolean z2 = false;
        for (int i2 = 0; i2 < 2; i2++) {
            for (int i3 = 0; i3 < this.selectedMessagesIds[i2].size(); i3++) {
                MessageObject valueAt = this.selectedMessagesIds[i2].valueAt(i3);
                if (valueAt.isPoll()) {
                    i = valueAt.isPublicPoll() ? 2 : 1;
                    if (i == 2) {
                        break;
                    }
                } else if (valueAt.isInvoice()) {
                    z2 = true;
                }
            }
            if (i == 2) {
                break;
            }
        }
        Bundle bundle = new Bundle();
        bundle.putBoolean("onlySelect", true);
        bundle.putInt("dialogsType", 3);
        bundle.putInt("messagesCount", this.canForwardMessagesCount);
        bundle.putInt("hasPoll", i);
        bundle.putBoolean("hasInvoice", z2);
        DialogsActivity dialogsActivity = new DialogsActivity(bundle);
        dialogsActivity.setDelegate(this);
        presentFragment(dialogsActivity);
    }

    private void showBottomOverlayProgress(final boolean z, boolean z2) {
        if (z && this.bottomOverlayProgress.getTag() != null) {
            return;
        }
        if (z || this.bottomOverlayProgress.getTag() != null) {
            AnimatorSet animatorSet = this.bottomOverlayAnimation;
            int i = null;
            if (animatorSet != null) {
                animatorSet.cancel();
                this.bottomOverlayAnimation = null;
            }
            RadialProgressView radialProgressView = this.bottomOverlayProgress;
            if (z) {
                i = 1;
            }
            radialProgressView.setTag(i);
            float f = 0.0f;
            int i2 = 4;
            float f2 = 0.1f;
            if (z2) {
                this.bottomOverlayAnimation = new AnimatorSet();
                if (z) {
                    this.bottomOverlayProgress.setVisibility(0);
                    this.bottomOverlayAnimation.playTogether(ObjectAnimator.ofFloat(this.bottomOverlayChatText, View.SCALE_X, 0.1f), ObjectAnimator.ofFloat(this.bottomOverlayChatText, View.SCALE_Y, 0.1f), ObjectAnimator.ofFloat(this.bottomOverlayChatText, View.ALPHA, 0.0f), ObjectAnimator.ofFloat(this.bottomOverlayProgress, View.SCALE_X, 1.0f), ObjectAnimator.ofFloat(this.bottomOverlayProgress, View.SCALE_Y, 1.0f), ObjectAnimator.ofFloat(this.bottomOverlayProgress, View.ALPHA, 1.0f));
                    this.bottomOverlayAnimation.setStartDelay(200);
                } else {
                    this.bottomOverlayChatText.setVisibility(0);
                    this.bottomOverlayAnimation.playTogether(ObjectAnimator.ofFloat(this.bottomOverlayProgress, View.SCALE_X, 0.1f), ObjectAnimator.ofFloat(this.bottomOverlayProgress, View.SCALE_Y, 0.1f), ObjectAnimator.ofFloat(this.bottomOverlayProgress, View.ALPHA, 0.0f), ObjectAnimator.ofFloat(this.bottomOverlayChatText, View.SCALE_X, 1.0f), ObjectAnimator.ofFloat(this.bottomOverlayChatText, View.SCALE_Y, 1.0f), ObjectAnimator.ofFloat(this.bottomOverlayChatText, View.ALPHA, 1.0f));
                }
                this.bottomOverlayAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.62
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        if (ChatActivity.this.bottomOverlayAnimation != null && ChatActivity.this.bottomOverlayAnimation.equals(animator)) {
                            if (!z) {
                                ChatActivity.this.bottomOverlayProgress.setVisibility(4);
                            } else {
                                ChatActivity.this.bottomOverlayChatText.setVisibility(4);
                            }
                        }
                    }

                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationCancel(Animator animator) {
                        if (ChatActivity.this.bottomOverlayAnimation != null && ChatActivity.this.bottomOverlayAnimation.equals(animator)) {
                            ChatActivity.this.bottomOverlayAnimation = null;
                        }
                    }
                });
                this.bottomOverlayAnimation.setDuration(150L);
                this.bottomOverlayAnimation.start();
                return;
            }
            this.bottomOverlayProgress.setVisibility(z ? 0 : 4);
            this.bottomOverlayProgress.setScaleX(z ? 1.0f : 0.1f);
            this.bottomOverlayProgress.setScaleY(z ? 1.0f : 0.1f);
            this.bottomOverlayProgress.setAlpha(1.0f);
            UnreadCounterTextView unreadCounterTextView = this.bottomOverlayChatText;
            if (!z) {
                i2 = 0;
            }
            unreadCounterTextView.setVisibility(i2);
            this.bottomOverlayChatText.setScaleX(z ? 0.1f : 1.0f);
            UnreadCounterTextView unreadCounterTextView2 = this.bottomOverlayChatText;
            if (!z) {
                f2 = 1.0f;
            }
            unreadCounterTextView2.setScaleY(f2);
            UnreadCounterTextView unreadCounterTextView3 = this.bottomOverlayChatText;
            if (!z) {
                f = 1.0f;
            }
            unreadCounterTextView3.setAlpha(f);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: sendBotInlineResult */
    public void lambda$createView$45(TLRPC$BotInlineResult tLRPC$BotInlineResult, boolean z, int i) {
        MentionsContainerView mentionsContainerView = this.mentionContainer;
        if (mentionsContainerView != null) {
            long contextBotId = mentionsContainerView.getAdapter().getContextBotId();
            HashMap hashMap = new HashMap();
            hashMap.put("id", tLRPC$BotInlineResult.id);
            hashMap.put("query_id", "" + tLRPC$BotInlineResult.query_id);
            hashMap.put("bot", "" + contextBotId);
            hashMap.put("bot_name", this.mentionContainer.getAdapter().getContextBotName());
            SendMessagesHelper.prepareSendingBotContextResult(getAccountInstance(), tLRPC$BotInlineResult, hashMap, this.dialog_id, this.replyingMessageObject, getThreadMessage(), z, i);
            this.chatActivityEnterView.setFieldText("");
            hideFieldPanel(false);
            getMediaDataController().increaseInlineRaiting(contextBotId);
        }
    }

    private void checkBotCommands() {
        TLRPC$Chat tLRPC$Chat;
        r0 = false;
        r0 = false;
        boolean z = false;
        URLSpanBotCommand.enabled = false;
        TLRPC$User tLRPC$User = this.currentUser;
        if (tLRPC$User == null || !tLRPC$User.bot) {
            TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
            if (tLRPC$ChatFull instanceof TLRPC$TL_chatFull) {
                for (int i = 0; i < this.chatInfo.participants.participants.size(); i++) {
                    TLRPC$User user = getMessagesController().getUser(Long.valueOf(this.chatInfo.participants.participants.get(i).user_id));
                    if (user != null && user.bot) {
                        URLSpanBotCommand.enabled = true;
                        return;
                    }
                }
            } else if (tLRPC$ChatFull instanceof TLRPC$TL_channelFull) {
                if (!tLRPC$ChatFull.bot_info.isEmpty() && (tLRPC$Chat = this.currentChat) != null && tLRPC$Chat.megagroup) {
                    z = true;
                }
                URLSpanBotCommand.enabled = z;
            }
        } else {
            URLSpanBotCommand.enabled = !UserObject.isReplyUser(tLRPC$User);
        }
    }

    /* access modifiers changed from: private */
    public MessageObject.GroupedMessages getValidGroupedMessage(MessageObject messageObject) {
        if (messageObject.getGroupId() == 0) {
            return null;
        }
        MessageObject.GroupedMessages groupedMessages = this.groupedMessagesMap.get(messageObject.getGroupId());
        if (groupedMessages == null || (groupedMessages.messages.size() > 1 && groupedMessages.positions.get(messageObject) != null)) {
            return groupedMessages;
        }
        return null;
    }

    public void jumpToDate(int i) {
        int i2;
        TLRPC$Message tLRPC$Message;
        if (!this.messages.isEmpty()) {
            ArrayList<MessageObject> arrayList = this.messages;
            int i3 = 1;
            MessageObject messageObject = arrayList.get(arrayList.size() - 1);
            if ((this.messages.get(0).messageOwner.date >= i && messageObject.messageOwner.date <= i) || (messageObject.messageOwner.date >= i && this.endReached[0])) {
                for (int size = this.messages.size() - 1; size >= 0; size--) {
                    MessageObject messageObject2 = this.messages.get(size);
                    if (messageObject2.messageOwner.date >= i && messageObject2.getId() != 0) {
                        int id = messageObject2.getId();
                        if (messageObject2.getDialogId() != this.mergeDialogId) {
                            i3 = 0;
                        }
                        scrollToMessageId(id, 0, false, i3, true, 0);
                        return;
                    }
                }
            } else if (!DialogObject.isEncryptedDialog(this.dialog_id)) {
                int findLastVisibleItemPosition = this.chatLayoutManager.findLastVisibleItemPosition();
                int findFirstVisibleItemPosition = this.chatLayoutManager.findFirstVisibleItemPosition();
                while (true) {
                    if (findFirstVisibleItemPosition > findLastVisibleItemPosition) {
                        i2 = -1;
                        break;
                    } else if (findFirstVisibleItemPosition < this.chatAdapter.messagesStartRow || findFirstVisibleItemPosition >= this.chatAdapter.messagesEndRow || (tLRPC$Message = this.messages.get(findFirstVisibleItemPosition - this.chatAdapter.messagesStartRow).messageOwner) == null) {
                        findFirstVisibleItemPosition++;
                    } else {
                        int i4 = tLRPC$Message.date < i ? 1 : 0;
                        if (isSecretChat()) {
                            i4 ^= 1;
                        }
                        i2 = i4 ^ 1;
                    }
                }
                this.chatScrollHelper.setScrollDirection(i2);
                AlertDialog alertDialog = this.progressDialog;
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                updatePinnedListButton(false);
                AlertDialog alertDialog2 = new AlertDialog(getParentActivity(), 3, this.themeDelegate);
                this.progressDialog = alertDialog2;
                alertDialog2.setOnCancelListener(this.postponedScrollCancelListener);
                this.progressDialog.showDelayed(1000);
                int i5 = this.lastLoadIndex;
                this.postponedScrollToLastMessageQueryIndex = i5;
                this.waitingForLoad.add(Integer.valueOf(i5));
                this.postponedScrollMessageId = 0;
                this.postponedScrollIsCanceled = false;
                MessagesController messagesController = getMessagesController();
                long j = this.dialog_id;
                long j2 = this.mergeDialogId;
                int i6 = this.classGuid;
                int i7 = this.chatMode;
                int i8 = this.threadMessageId;
                int i9 = this.replyMaxReadId;
                int i10 = this.lastLoadIndex;
                this.lastLoadIndex = i10 + 1;
                messagesController.loadMessages(j, j2, false, 30, 0, i, true, 0, i6, 4, 0, i7, i8, i9, i10);
                this.floatingDateView.setAlpha(0.0f);
                this.floatingDateView.setTag(null);
            }
        }
    }

    public void processInlineBotContextPM(TLRPC$TL_inlineBotSwitchPM tLRPC$TL_inlineBotSwitchPM) {
        MentionsContainerView mentionsContainerView;
        TLRPC$User contextBotUser;
        if (tLRPC$TL_inlineBotSwitchPM != null && (mentionsContainerView = this.mentionContainer) != null && (contextBotUser = mentionsContainerView.getAdapter().getContextBotUser()) != null) {
            this.chatActivityEnterView.setFieldText("");
            long j = this.dialog_id;
            if (j == contextBotUser.id) {
                this.inlineReturn = j;
                getMessagesController().sendBotStart(this.currentUser, tLRPC$TL_inlineBotSwitchPM.start_param);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putLong("user_id", contextBotUser.id);
            bundle.putString("inline_query", tLRPC$TL_inlineBotSwitchPM.start_param);
            bundle.putLong("inline_return", this.dialog_id);
            if (getMessagesController().checkCanOpenChat(bundle, this)) {
                presentFragment(new ChatActivity(bundle));
            }
        }
    }

    private void createChatAttachView() {
        if (getParentActivity() != null && this.chatAttachAlert == null) {
            AnonymousClass63 r0 = new ChatAttachAlert(getParentActivity(), this, false, false, this.themeDelegate) { // from class: org.telegram.ui.ChatActivity.63
                @Override // org.telegram.ui.Components.ChatAttachAlert, org.telegram.ui.ActionBar.BottomSheet
                public void dismissInternal() {
                    if (ChatActivity.this.chatAttachAlert != null && ChatActivity.this.chatAttachAlert.isShowing()) {
                        AndroidUtilities.requestAdjustResize(ChatActivity.this.getParentActivity(), ((BaseFragment) ChatActivity.this).classGuid);
                    }
                    super.dismissInternal();
                    ChatActivity.this.onEditTextDialogClose(false, true);
                }

                @Override // org.telegram.ui.ActionBar.BottomSheet
                public void onDismissAnimationStart() {
                    ChatActivity.this.chatAttachAlert.setFocusable(false);
                    ChatActivity.this.chatActivityEnterView.getEditField().requestFocus();
                    if (ChatActivity.this.chatAttachAlert != null && ChatActivity.this.chatAttachAlert.isShowing()) {
                        AndroidUtilities.requestAdjustResize(ChatActivity.this.getParentActivity(), ((BaseFragment) ChatActivity.this).classGuid);
                    }
                    ChatActivity.this.onEditTextDialogClose(false, false);
                }
            };
            this.chatAttachAlert = r0;
            r0.setDelegate(new ChatAttachAlert.ChatAttachViewDelegate() { // from class: org.telegram.ui.ChatActivity.64
                @Override // org.telegram.ui.Components.ChatAttachAlert.ChatAttachViewDelegate
                public /* synthetic */ void openAvatarsSearch() {
                    ChatAttachAlert.ChatAttachViewDelegate.CC.$default$openAvatarsSearch(this);
                }

                @Override // org.telegram.ui.Components.ChatAttachAlert.ChatAttachViewDelegate
                public void didPressedButton(int i, boolean z, boolean z2, int i2, boolean z3) {
                    String str;
                    if (!(ChatActivity.this.getParentActivity() == null || ChatActivity.this.chatAttachAlert == null)) {
                        ChatActivity chatActivity = ChatActivity.this;
                        chatActivity.editingMessageObject = chatActivity.chatAttachAlert.getEditingMessageObject();
                        if (i == 8 || i == 7 || (i == 4 && !ChatActivity.this.chatAttachAlert.getPhotoLayout().getSelectedPhotos().isEmpty())) {
                            if (i != 8) {
                                ChatActivity.this.chatAttachAlert.dismiss(true);
                            }
                            HashMap<Object, Object> selectedPhotos = ChatActivity.this.chatAttachAlert.getPhotoLayout().getSelectedPhotos();
                            ArrayList<Object> selectedPhotosOrder = ChatActivity.this.chatAttachAlert.getPhotoLayout().getSelectedPhotosOrder();
                            if (!selectedPhotos.isEmpty()) {
                                for (int i3 = 0; ((double) i3) < Math.ceil((double) (((float) selectedPhotos.size()) / 10.0f)); i3++) {
                                    int i4 = i3 * 10;
                                    int min = Math.min(10, selectedPhotos.size() - i4);
                                    ArrayList arrayList = new ArrayList();
                                    for (int i5 = 0; i5 < min; i5++) {
                                        int i6 = i4 + i5;
                                        if (i6 < selectedPhotosOrder.size()) {
                                            MediaController.PhotoEntry photoEntry = (MediaController.PhotoEntry) selectedPhotos.get(selectedPhotosOrder.get(i6));
                                            SendMessagesHelper.SendingMediaInfo sendingMediaInfo = new SendMessagesHelper.SendingMediaInfo();
                                            boolean z4 = photoEntry.isVideo;
                                            if (z4 || (str = photoEntry.imagePath) == null) {
                                                String str2 = photoEntry.path;
                                                if (str2 != null) {
                                                    sendingMediaInfo.path = str2;
                                                }
                                            } else {
                                                sendingMediaInfo.path = str;
                                            }
                                            sendingMediaInfo.thumbPath = photoEntry.thumbPath;
                                            sendingMediaInfo.isVideo = z4;
                                            CharSequence charSequence = photoEntry.caption;
                                            sendingMediaInfo.caption = charSequence != null ? charSequence.toString() : null;
                                            sendingMediaInfo.entities = photoEntry.entities;
                                            sendingMediaInfo.masks = photoEntry.stickers;
                                            sendingMediaInfo.ttl = photoEntry.ttl;
                                            sendingMediaInfo.videoEditedInfo = photoEntry.editedInfo;
                                            sendingMediaInfo.canDeleteAfter = photoEntry.canDeleteAfter;
                                            arrayList.add(sendingMediaInfo);
                                            photoEntry.reset();
                                        }
                                    }
                                    if (i3 == 0) {
                                        ChatActivity.this.fillEditingMediaWithCaption(((SendMessagesHelper.SendingMediaInfo) arrayList.get(0)).caption, ((SendMessagesHelper.SendingMediaInfo) arrayList.get(0)).entities);
                                    }
                                    SendMessagesHelper.prepareSendingMedia(ChatActivity.this.getAccountInstance(), arrayList, ChatActivity.this.dialog_id, ChatActivity.this.replyingMessageObject, ChatActivity.this.getThreadMessage(), null, i == 4 || z3, z, ChatActivity.this.editingMessageObject, z2, i2);
                                }
                                ChatActivity.this.afterMessageSend();
                                ChatActivity.this.chatActivityEnterView.setFieldText("");
                            }
                            if (i2 != 0) {
                                if (ChatActivity.this.scheduledMessagesCount == -1) {
                                    ChatActivity.this.scheduledMessagesCount = 0;
                                }
                                ChatActivity.access$26912(ChatActivity.this, selectedPhotos.size());
                                ChatActivity.this.updateScheduledInterface(true);
                                return;
                            }
                            return;
                        }
                        if (ChatActivity.this.chatAttachAlert != null) {
                            ChatActivity.this.chatAttachAlert.dismissWithButtonClick(i);
                        }
                        ChatActivity.this.processSelectedAttach(i);
                    }
                }

                @Override // org.telegram.ui.Components.ChatAttachAlert.ChatAttachViewDelegate
                public void didSelectBot(TLRPC$User tLRPC$User) {
                    if (ChatActivity.this.chatActivityEnterView != null && tLRPC$User != null && !TextUtils.isEmpty(tLRPC$User.username)) {
                        ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                        chatActivityEnterView.setFieldText("@" + tLRPC$User.username + " ");
                        ChatActivity.this.chatActivityEnterView.openKeyboard();
                    }
                }

                @Override // org.telegram.ui.Components.ChatAttachAlert.ChatAttachViewDelegate
                public void onCameraOpened() {
                    ChatActivity.this.chatActivityEnterView.closeKeyboard();
                }

                @Override // org.telegram.ui.Components.ChatAttachAlert.ChatAttachViewDelegate
                public boolean needEnterComment() {
                    return ChatActivity.this.needEnterText();
                }

                @Override // org.telegram.ui.Components.ChatAttachAlert.ChatAttachViewDelegate
                public void doOnIdle(Runnable runnable) {
                    ChatActivity.this.lambda$openDiscussionMessageChat$227(runnable);
                }
            });
        }
    }

    public boolean needEnterText() {
        boolean isKeyboardVisible = this.chatActivityEnterView.isKeyboardVisible();
        if (isKeyboardVisible) {
            this.chatActivityEnterView.showEmojiView();
            this.openKeyboardOnAttachMenuClose = true;
        }
        AndroidUtilities.setAdjustResizeToNothing(getParentActivity(), this.classGuid);
        this.fragmentView.requestLayout();
        return isKeyboardVisible;
    }

    public void onEditTextDialogClose(boolean z, boolean z2) {
        if (this.openKeyboardOnAttachMenuClose) {
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda156
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$onEditTextDialogClose$86();
                }
            }, 50);
            if (z2) {
                this.openKeyboardOnAttachMenuClose = false;
            }
        }
        if (z) {
            AndroidUtilities.requestAdjustResize(getParentActivity(), this.classGuid);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onEditTextDialogClose$86() {
        this.chatActivityEnterView.openKeyboard();
    }

    /* renamed from: doOnIdle */
    public void lambda$openDiscussionMessageChat$227(Runnable runnable) {
        NotificationCenter.getInstance(this.currentAccount).doOnIdle(runnable);
    }

    public void performHistoryClear(boolean z, boolean z2) {
        this.clearingHistory = true;
        this.undoView.showWithAction(this.dialog_id, 0, new Runnable(z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda196
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$performHistoryClear$87(this.f$1);
            }
        }, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda122
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$performHistoryClear$88();
            }
        });
        this.chatAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$performHistoryClear$87(boolean z) {
        if (!this.pinnedMessageIds.isEmpty()) {
            SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
            edit.putInt("pin_" + this.dialog_id, this.pinnedMessageIds.get(0).intValue()).commit();
            this.pinnedMessageIds.clear();
            this.pinnedMessageObjects.clear();
            this.currentPinnedMessageId = 0;
            this.loadedPinnedMessagesCount = 0;
            this.totalPinnedMessagesCount = 0;
            updatePinnedMessageView(true);
        }
        getMessagesController().deleteDialog(this.dialog_id, 1, z);
        this.clearingHistory = false;
        clearHistory(false, null);
        this.chatAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$performHistoryClear$88() {
        this.clearingHistory = false;
        this.chatAdapter.notifyDataSetChanged();
    }

    public long getDialogId() {
        return this.dialog_id;
    }

    public int getDialogFolderId() {
        return this.dialogFolderId;
    }

    public int getDialogFilterId() {
        return this.dialogFilterId;
    }

    public boolean openedWithLivestream() {
        return this.livestream;
    }

    public UndoView getUndoView() {
        return this.undoView;
    }

    public long getMergeDialogId() {
        return this.mergeDialogId;
    }

    public boolean hasReportSpam() {
        BlurredFrameLayout blurredFrameLayout = this.topChatPanelView;
        return (blurredFrameLayout == null || blurredFrameLayout.getTag() != null || this.reportSpamButton.getVisibility() == 8) ? false : true;
    }

    public boolean isReport() {
        return this.reportType >= 0;
    }

    public void setChatInvite(TLRPC$ChatInvite tLRPC$ChatInvite) {
        this.chatInvite = tLRPC$ChatInvite;
    }

    public void setBotUser(String str) {
        if (this.inlineReturn != 0) {
            getMessagesController().sendBotStart(this.currentUser, str);
            return;
        }
        this.botUser = str;
        updateBottomOverlay();
    }

    /* access modifiers changed from: private */
    public void afterMessageSend() {
        hideFieldPanel(false);
        if (this.chatMode == 0) {
            getMediaDataController().cleanDraft(this.dialog_id, this.threadMessageId, true);
        }
    }

    private void toggleMesagesSearchListView() {
        RecyclerListView recyclerListView = this.messagesSearchListView;
        if (recyclerListView != null) {
            showMessagesSearchListView(recyclerListView.getTag() == null);
        }
    }

    /* access modifiers changed from: private */
    public void showMessagesSearchListView(final boolean z) {
        RecyclerListView recyclerListView = this.messagesSearchListView;
        if (recyclerListView == null) {
            return;
        }
        if (z && recyclerListView.getTag() != null) {
            return;
        }
        if (z || this.messagesSearchListView.getTag() != null) {
            AnimatorSet animatorSet = this.messagesSearchListViewAnimation;
            int i = null;
            if (animatorSet != null) {
                animatorSet.cancel();
                this.messagesSearchListViewAnimation = null;
            }
            if (z) {
                this.messagesSearchListView.setVisibility(0);
            }
            RecyclerListView recyclerListView2 = this.messagesSearchListView;
            if (z) {
                i = 1;
            }
            recyclerListView2.setTag(i);
            AnimatorSet animatorSet2 = new AnimatorSet();
            this.messagesSearchListViewAnimation = animatorSet2;
            Animator[] animatorArr = new Animator[1];
            RecyclerListView recyclerListView3 = this.messagesSearchListView;
            Property property = View.ALPHA;
            float[] fArr = new float[1];
            fArr[0] = z ? 1.0f : 0.0f;
            animatorArr[0] = ObjectAnimator.ofFloat(recyclerListView3, property, fArr);
            animatorSet2.playTogether(animatorArr);
            this.messagesSearchListViewAnimation.setInterpolator(CubicBezierInterpolator.EASE_IN);
            this.messagesSearchListViewAnimation.setDuration(180L);
            this.messagesSearchListViewAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.65
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    if (animator.equals(ChatActivity.this.messagesSearchListViewAnimation)) {
                        ChatActivity.this.messagesSearchListViewAnimation = null;
                        if (!z) {
                            ChatActivity.this.messagesSearchListView.setVisibility(8);
                        }
                    }
                }

                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationCancel(Animator animator) {
                    if (animator.equals(ChatActivity.this.messagesSearchListViewAnimation)) {
                        ChatActivity.this.messagesSearchListViewAnimation = null;
                    }
                }
            });
            this.messagesSearchListViewAnimation.start();
        }
    }

    public boolean playFirstUnreadVoiceMessage() {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null && chatActivityEnterView.isRecordingAudioVideo()) {
            return true;
        }
        for (int size = this.messages.size() - 1; size >= 0; size--) {
            MessageObject messageObject = this.messages.get(size);
            if ((messageObject.isVoice() || messageObject.isRoundVideo()) && messageObject.isContentUnread() && !messageObject.isOut()) {
                MediaController.getInstance().setVoiceMessagesPlaylist(MediaController.getInstance().playMessage(messageObject) ? createVoiceMessagesPlaylist(messageObject, true) : null, true);
                return true;
            }
        }
        if (Build.VERSION.SDK_INT < 23 || getParentActivity() == null || getParentActivity().checkSelfPermission("android.permission.RECORD_AUDIO") == 0) {
            return false;
        }
        getParentActivity().requestPermissions(new String[]{"android.permission.RECORD_AUDIO"}, 3);
        return true;
    }

    /* access modifiers changed from: private */
    public void openScheduledMessages() {
        ActionBarLayout actionBarLayout = this.parentLayout;
        if (actionBarLayout != null && actionBarLayout.getLastFragment() == this) {
            Bundle bundle = new Bundle();
            TLRPC$EncryptedChat tLRPC$EncryptedChat = this.currentEncryptedChat;
            if (tLRPC$EncryptedChat != null) {
                bundle.putInt("enc_id", tLRPC$EncryptedChat.id);
            } else {
                TLRPC$Chat tLRPC$Chat = this.currentChat;
                if (tLRPC$Chat != null) {
                    bundle.putLong("chat_id", tLRPC$Chat.id);
                } else {
                    bundle.putLong("user_id", this.currentUser.id);
                }
            }
            bundle.putInt("chatMode", 1);
            ChatActivity chatActivity = new ChatActivity(bundle);
            chatActivity.chatActivityDelegate = new ChatActivityDelegate() { // from class: org.telegram.ui.ChatActivity.66
                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public /* synthetic */ void onReport() {
                    ChatActivityDelegate.CC.$default$onReport(this);
                }

                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public /* synthetic */ void onUnpin(boolean z, boolean z2) {
                    ChatActivityDelegate.CC.$default$onUnpin(this, z, z2);
                }

                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public void openReplyMessage(int i) {
                    ChatActivity.this.scrollToMessageId(i, 0, true, 0, true, 0);
                }

                @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
                public void openSearch(String str) {
                    ChatActivity.this.openSearchWithText(str);
                }
            };
            presentFragment(chatActivity, false);
        }
    }

    private void initStickers() {
        if (this.chatActivityEnterView != null && getParentActivity() != null && this.stickersAdapter == null) {
            this.stickersListView.setPadding(AndroidUtilities.dp(18.0f), 0, AndroidUtilities.dp(18.0f), 0);
            RecyclerListView recyclerListView = this.stickersListView;
            StickersAdapter stickersAdapter = new StickersAdapter(getParentActivity(), new StickersAdapter.StickersAdapterDelegate() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda235
                @Override // org.telegram.ui.Adapters.StickersAdapter.StickersAdapterDelegate
                public final void needChangePanelVisibility(boolean z) {
                    ChatActivity.this.lambda$initStickers$89(z);
                }
            }, this.themeDelegate);
            this.stickersAdapter = stickersAdapter;
            recyclerListView.setAdapter(stickersAdapter);
            RecyclerListView recyclerListView2 = this.stickersListView;
            ChatActivity$$ExternalSyntheticLambda249 chatActivity$$ExternalSyntheticLambda249 = new RecyclerListView.OnItemClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda249
                @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListener
                public final void onItemClick(View view, int i) {
                    ChatActivity.this.lambda$initStickers$90(view, i);
                }
            };
            this.stickersOnItemClickListener = chatActivity$$ExternalSyntheticLambda249;
            recyclerListView2.setOnItemClickListener(chatActivity$$ExternalSyntheticLambda249);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$initStickers$89(final boolean z) {
        if (z) {
            int dp = this.stickersAdapter.isShowingKeywords() ? AndroidUtilities.dp(24.0f) : 0;
            if (dp != this.stickersListView.getPaddingTop() || this.stickersPanel.getTag() == null) {
                this.stickersListView.setPadding(AndroidUtilities.dp(18.0f), dp, AndroidUtilities.dp(18.0f), 0);
                this.stickersListView.scrollToPosition(0);
                ((FrameLayout.LayoutParams) this.stickersPanelArrow.getLayoutParams()).gravity = (this.chatActivityEnterView.isRtlText() ? 5 : 3) | 80;
                this.stickersPanelArrow.requestLayout();
            }
        }
        if (z && this.stickersPanel.getTag() != null) {
            return;
        }
        if (z || this.stickersPanel.getTag() != null) {
            if (z) {
                this.stickersPanel.setVisibility(this.allowStickersPanel ? 0 : 4);
                this.stickersPanel.setTag(1);
            } else {
                this.stickersPanel.setTag(null);
            }
            AnimatorSet animatorSet = this.runningAnimation;
            if (animatorSet != null) {
                animatorSet.cancel();
                this.runningAnimation = null;
            }
            if (this.stickersPanel.getVisibility() != 4) {
                AnimatorSet animatorSet2 = new AnimatorSet();
                this.runningAnimation = animatorSet2;
                Animator[] animatorArr = new Animator[1];
                FrameLayout frameLayout = this.stickersPanel;
                Property property = View.ALPHA;
                float[] fArr = new float[2];
                float f = 0.0f;
                fArr[0] = z ? 0.0f : 1.0f;
                if (z) {
                    f = 1.0f;
                }
                fArr[1] = f;
                animatorArr[0] = ObjectAnimator.ofFloat(frameLayout, property, fArr);
                animatorSet2.playTogether(animatorArr);
                this.runningAnimation.setDuration(150L);
                this.runningAnimationIndex = getNotificationCenter().setAnimationInProgress(this.runningAnimationIndex, null);
                this.runningAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.67
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        ChatActivity.this.getNotificationCenter().onAnimationFinish(ChatActivity.this.runningAnimationIndex);
                        if (ChatActivity.this.runningAnimation != null && ChatActivity.this.runningAnimation.equals(animator)) {
                            if (!z) {
                                ChatActivity.this.stickersAdapter.clearSearch();
                                ChatActivity.this.stickersPanel.setVisibility(8);
                                if (ContentPreviewViewer.getInstance().isVisible()) {
                                    ContentPreviewViewer.getInstance().close();
                                }
                                ContentPreviewViewer.getInstance().reset();
                            }
                            ChatActivity.this.runningAnimation = null;
                        }
                    }

                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationCancel(Animator animator) {
                        if (ChatActivity.this.runningAnimation != null && ChatActivity.this.runningAnimation.equals(animator)) {
                            ChatActivity.this.runningAnimation = null;
                        }
                    }
                });
                this.runningAnimation.start();
            } else if (!z) {
                this.stickersPanel.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$initStickers$90(View view, int i) {
        Object item = this.stickersAdapter.getItem(i);
        if (item instanceof String) {
            SpannableString spannableString = new SpannableString((String) item);
            Emoji.replaceEmoji(spannableString, this.chatActivityEnterView.getEditField().getPaint().getFontMetricsInt(), AndroidUtilities.dp(20.0f), false);
            this.chatActivityEnterView.setFieldText(spannableString, false);
        }
    }

    public void shareMyContact(int i, MessageObject messageObject) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
        builder.setTitle(LocaleController.getString("ShareYouPhoneNumberTitle", R.string.ShareYouPhoneNumberTitle));
        TLRPC$User tLRPC$User = this.currentUser;
        if (tLRPC$User == null) {
            builder.setMessage(LocaleController.getString("AreYouSureShareMyContactInfo", R.string.AreYouSureShareMyContactInfo));
        } else if (tLRPC$User.bot) {
            builder.setMessage(LocaleController.getString("AreYouSureShareMyContactInfoBot", R.string.AreYouSureShareMyContactInfoBot));
        } else {
            PhoneFormat instance = PhoneFormat.getInstance();
            TLRPC$User tLRPC$User2 = this.currentUser;
            builder.setMessage(AndroidUtilities.replaceTags(LocaleController.formatString("AreYouSureShareMyContactInfoUser", R.string.AreYouSureShareMyContactInfoUser, instance.format("+" + getUserConfig().getCurrentUser().phone), ContactsController.formatName(tLRPC$User2.first_name, tLRPC$User2.last_name))));
        }
        builder.setPositiveButton(LocaleController.getString("ShareContact", R.string.ShareContact), new DialogInterface.OnClickListener(i, messageObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda34
            public final /* synthetic */ int f$1;
            public final /* synthetic */ MessageObject f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                ChatActivity.this.lambda$shareMyContact$92(this.f$1, this.f$2, dialogInterface, i2);
            }
        });
        builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
        showDialog(builder.create());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$shareMyContact$92(int i, MessageObject messageObject, DialogInterface dialogInterface, int i2) {
        if (i == 1) {
            TLRPC$TL_contacts_acceptContact tLRPC$TL_contacts_acceptContact = new TLObject() { // from class: org.telegram.tgnet.TLRPC$TL_contacts_acceptContact
                public static int constructor = -130964977;
                public TLRPC$InputUser id;

                @Override // org.telegram.tgnet.TLObject
                public TLObject deserializeResponse(AbstractSerializedData abstractSerializedData, int i3, boolean z) {
                    return TLRPC$Updates.TLdeserialize(abstractSerializedData, i3, z);
                }

                @Override // org.telegram.tgnet.TLObject
                public void serializeToStream(AbstractSerializedData abstractSerializedData) {
                    abstractSerializedData.writeInt32(constructor);
                    this.id.serializeToStream(abstractSerializedData);
                }
            };
            tLRPC$TL_contacts_acceptContact.id = getMessagesController().getInputUser(this.currentUser);
            getConnectionsManager().sendRequest(tLRPC$TL_contacts_acceptContact, new RequestDelegate() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda219
                @Override // org.telegram.tgnet.RequestDelegate
                public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                    ChatActivity.this.lambda$shareMyContact$91(tLObject, tLRPC$TL_error);
                }
            });
            return;
        }
        SendMessagesHelper.getInstance(this.currentAccount).sendMessage(getUserConfig().getCurrentUser(), this.dialog_id, messageObject, getThreadMessage(), (TLRPC$ReplyMarkup) null, (HashMap<String, String>) null, true, 0);
        if (this.chatMode == 0) {
            moveScrollToLastMessage(false);
        }
        hideFieldPanel(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$shareMyContact$91(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        if (tLRPC$TL_error == null) {
            getMessagesController().processUpdates((TLRPC$Updates) tLObject, false);
        }
    }

    /* access modifiers changed from: private */
    public void showVoiceHint(boolean z, boolean z2) {
        ChatActivityEnterView chatActivityEnterView;
        int i;
        String str;
        if (getParentActivity() != null && this.fragmentView != null) {
            if ((!z || this.voiceHintTextView != null) && this.chatMode == 0 && (chatActivityEnterView = this.chatActivityEnterView) != null && chatActivityEnterView.getAudioVideoButtonContainer() != null && this.chatActivityEnterView.getAudioVideoButtonContainer().getVisibility() == 0) {
                if (this.voiceHintTextView == null) {
                    SizeNotifierFrameLayout sizeNotifierFrameLayout = (SizeNotifierFrameLayout) this.fragmentView;
                    int indexOfChild = sizeNotifierFrameLayout.indexOfChild(this.chatActivityEnterView);
                    if (indexOfChild != -1) {
                        HintView hintView = new HintView(getParentActivity(), 9, this.themeDelegate);
                        this.voiceHintTextView = hintView;
                        sizeNotifierFrameLayout.addView(hintView, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 10.0f, 0.0f, 10.0f, 0.0f));
                    } else {
                        return;
                    }
                }
                if (z) {
                    this.voiceHintTextView.hide();
                    return;
                }
                if (this.chatActivityEnterView.hasRecordVideo()) {
                    HintView hintView2 = this.voiceHintTextView;
                    if (z2) {
                        i = R.string.HoldToVideo;
                        str = "HoldToVideo";
                    } else {
                        i = R.string.HoldToAudio;
                        str = "HoldToAudio";
                    }
                    hintView2.setText(LocaleController.getString(str, i));
                } else {
                    this.voiceHintTextView.setText(LocaleController.getString("HoldToAudioOnly", R.string.HoldToAudioOnly));
                }
                this.voiceHintTextView.showForView(this.chatActivityEnterView.getAudioVideoButtonContainer(), true);
            }
        }
    }

    public boolean checkSlowMode(View view) {
        CharSequence slowModeTimer = this.chatActivityEnterView.getSlowModeTimer();
        if (slowModeTimer == null) {
            return false;
        }
        showSlowModeHint(view, true, slowModeTimer);
        return true;
    }

    /* access modifiers changed from: private */
    public void hideHints(boolean z) {
        if (!z) {
            HintView hintView = this.slowModeHint;
            if (hintView != null) {
                hintView.hide();
            }
            HintView hintView2 = this.searchAsListHint;
            if (hintView2 != null) {
                hintView2.hide();
            }
            HintView hintView3 = this.scheduledOrNoSoundHint;
            if (hintView3 != null) {
                hintView3.hide();
            }
        }
        HintView hintView4 = this.fwdRestrictedBottomHint;
        if (hintView4 != null) {
            hintView4.hide();
        }
        HintView hintView5 = this.fwdRestrictedTopHint;
        if (hintView5 != null) {
            hintView5.hide();
        }
        HintView hintView6 = this.noSoundHintView;
        if (hintView6 != null) {
            hintView6.hide();
        }
        HintView hintView7 = this.forwardHintView;
        if (hintView7 != null) {
            hintView7.hide();
        }
        HintView hintView8 = this.pollHintView;
        if (hintView8 != null) {
            hintView8.hide();
        }
        HintView hintView9 = this.timerHintView;
        if (hintView9 != null) {
            hintView9.hide();
        }
        ChecksHintView checksHintView = this.checksHintView;
        if (checksHintView != null) {
            checksHintView.hide();
        }
    }

    /* access modifiers changed from: private */
    public void showSlowModeHint(View view, boolean z, CharSequence charSequence) {
        HintView hintView;
        if (getParentActivity() != null && this.fragmentView != null) {
            if (z || ((hintView = this.slowModeHint) != null && hintView.getVisibility() == 0)) {
                this.slowModeHint.setText(AndroidUtilities.replaceTags(LocaleController.formatString("SlowModeHint", R.string.SlowModeHint, charSequence)));
                if (z) {
                    this.slowModeHint.showForView(view, true);
                }
            }
        }
    }

    public void showTimerHint() {
        String str;
        if (getParentActivity() != null && this.fragmentView != null && this.chatInfo != null) {
            if (this.timerHintView == null) {
                HintView hintView = new HintView(getParentActivity(), 7, true, this.themeDelegate);
                this.timerHintView = hintView;
                hintView.setAlpha(0.0f);
                this.timerHintView.setVisibility(4);
                this.timerHintView.setShowingDuration(4000);
                this.contentView.addView(this.timerHintView, LayoutHelper.createFrame(-2, -2.0f, 51, 19.0f, 0.0f, 19.0f, 0.0f));
            }
            int i = this.chatInfo.ttl_period;
            if (i > 86400) {
                str = LocaleController.formatPluralString("Days", i / 86400, new Object[0]);
            } else if (i >= 3600) {
                str = LocaleController.formatPluralString("Hours", i / 3600, new Object[0]);
            } else if (i >= 60) {
                str = LocaleController.formatPluralString("Minutes", i / 60, new Object[0]);
            } else {
                str = LocaleController.formatPluralString("Seconds", i, new Object[0]);
            }
            this.timerHintView.setText(LocaleController.formatString("AutoDeleteSetInfo", R.string.AutoDeleteSetInfo, str));
            this.timerHintView.showForView(this.avatarContainer.getTimeItem(), true);
        }
    }

    private void showSearchAsListHint() {
        if (getParentActivity() != null && this.fragmentView != null && this.searchCountText != null) {
            if (this.searchAsListHint == null) {
                HintView hintView = new HintView(getParentActivity(), 3, this.themeDelegate);
                this.searchAsListHint = hintView;
                hintView.setAlpha(0.0f);
                this.searchAsListHint.setVisibility(4);
                this.searchAsListHint.setText(LocaleController.getString("TapToViewAsList", R.string.TapToViewAsList));
                this.contentView.addView(this.searchAsListHint, LayoutHelper.createFrame(-2, -2.0f, 51, 19.0f, 0.0f, 19.0f, 0.0f));
            }
            this.searchAsListHint.showForView(this.searchCountText, true);
        }
    }

    private void showScheduledOrNoSoundHint() {
        TLRPC$ChatFull tLRPC$ChatFull;
        boolean z = UserObject.isUserSelf(this.currentUser) || ((tLRPC$ChatFull = this.chatInfo) != null && tLRPC$ChatFull.slowmode_next_send_date > 0 && this.chatMode == 0);
        if (SharedConfig.scheduledOrNoSoundHintShows < 3 && System.currentTimeMillis() % 4 == 0 && !z) {
            AndroidUtilities.cancelRunOnUIThread(this.showScheduledOrNoSoundRunnable);
            AndroidUtilities.runOnUIThread(this.showScheduledOrNoSoundRunnable, 200);
        }
    }

    /* access modifiers changed from: private */
    public void showMediaBannedHint() {
        SizeNotifierFrameLayout sizeNotifierFrameLayout;
        int indexOfChild;
        if (getParentActivity() != null && this.currentChat != null && this.fragmentView != null) {
            HintView hintView = this.mediaBanTooltip;
            if ((hintView == null || hintView.getVisibility() != 0) && (indexOfChild = (sizeNotifierFrameLayout = (SizeNotifierFrameLayout) this.fragmentView).indexOfChild(this.chatActivityEnterView)) != -1) {
                if (this.mediaBanTooltip == null) {
                    HintView hintView2 = new HintView(getParentActivity(), 9, this.themeDelegate);
                    this.mediaBanTooltip = hintView2;
                    hintView2.setVisibility(8);
                    sizeNotifierFrameLayout.addView(this.mediaBanTooltip, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 10.0f, 0.0f, 10.0f, 0.0f));
                }
                if (ChatObject.isActionBannedByDefault(this.currentChat, 7)) {
                    this.mediaBanTooltip.setText(LocaleController.getString("GlobalAttachMediaRestricted", R.string.GlobalAttachMediaRestricted));
                } else {
                    TLRPC$TL_chatBannedRights tLRPC$TL_chatBannedRights = this.currentChat.banned_rights;
                    if (tLRPC$TL_chatBannedRights != null) {
                        if (AndroidUtilities.isBannedForever(tLRPC$TL_chatBannedRights)) {
                            this.mediaBanTooltip.setText(LocaleController.getString("AttachMediaRestrictedForever", R.string.AttachMediaRestrictedForever));
                        } else {
                            this.mediaBanTooltip.setText(LocaleController.formatString("AttachMediaRestricted", R.string.AttachMediaRestricted, LocaleController.formatDateForBan((long) this.currentChat.banned_rights.until_date)));
                        }
                    } else {
                        return;
                    }
                }
                this.mediaBanTooltip.showForView(this.chatActivityEnterView.getSendButton(), true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showNoSoundHint() {
        ChatMessageCell chatMessageCell;
        MessageObject messageObject;
        AnimatedFileDrawable animation;
        if (!(this.scrollingChatListView || SharedConfig.noSoundHintShowed || this.chatListView == null || getParentActivity() == null || this.fragmentView == null)) {
            HintView hintView = this.noSoundHintView;
            if (hintView == null || hintView.getTag() == null) {
                if (this.noSoundHintView == null) {
                    SizeNotifierFrameLayout sizeNotifierFrameLayout = (SizeNotifierFrameLayout) this.fragmentView;
                    int indexOfChild = sizeNotifierFrameLayout.indexOfChild(this.chatActivityEnterView);
                    if (indexOfChild != -1) {
                        HintView hintView2 = new HintView(getParentActivity(), 0, this.themeDelegate);
                        this.noSoundHintView = hintView2;
                        hintView2.setShowingDuration(10000);
                        sizeNotifierFrameLayout.addView(this.noSoundHintView, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 19.0f, 0.0f, 19.0f, 0.0f));
                        this.noSoundHintView.setAlpha(0.0f);
                        this.noSoundHintView.setVisibility(4);
                    } else {
                        return;
                    }
                }
                int childCount = this.chatListView.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = this.chatListView.getChildAt(i);
                    if ((childAt instanceof ChatMessageCell) && (messageObject = (chatMessageCell = (ChatMessageCell) childAt).getMessageObject()) != null && messageObject.isVideo() && (animation = chatMessageCell.getPhotoImage().getAnimation()) != null && animation.getCurrentProgressMs() >= 3000 && this.noSoundHintView.showForMessageCell(chatMessageCell, true)) {
                        SharedConfig.setNoSoundHintShowed(true);
                        return;
                    }
                }
            }
        }
    }

    private void checkChecksHint() {
        if (getMessagesController().pendingSuggestions.contains("NEWCOMER_TICKS")) {
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda129
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.showChecksHint();
                }
            }, 1000);
        }
    }

    /* access modifiers changed from: private */
    public void showChecksHint() {
        ChatMessageCell chatMessageCell;
        MessageObject messageObject;
        if (!(this.scrollingChatListView || this.chatListView == null || getParentActivity() == null || this.fragmentView == null)) {
            ChecksHintView checksHintView = this.checksHintView;
            if (checksHintView == null || checksHintView.getTag() == null) {
                if (this.checksHintView == null) {
                    SizeNotifierFrameLayout sizeNotifierFrameLayout = (SizeNotifierFrameLayout) this.fragmentView;
                    int indexOfChild = sizeNotifierFrameLayout.indexOfChild(this.chatActivityEnterView);
                    if (indexOfChild != -1) {
                        ChecksHintView checksHintView2 = new ChecksHintView(getParentActivity(), this.themeDelegate);
                        this.checksHintView = checksHintView2;
                        sizeNotifierFrameLayout.addView(checksHintView2, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 10.0f, 0.0f, 10.0f, 0.0f));
                        this.checksHintView.setAlpha(0.0f);
                        this.checksHintView.setVisibility(4);
                    } else {
                        return;
                    }
                }
                int childCount = this.chatListView.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = this.chatListView.getChildAt(i);
                    if ((childAt instanceof ChatMessageCell) && (messageObject = (chatMessageCell = (ChatMessageCell) childAt).getMessageObject()) != null && messageObject.isOutOwner() && messageObject.isSent() && this.checksHintView.showForMessageCell(chatMessageCell, true)) {
                        getMessagesController().removeSuggestion(0, "NEWCOMER_TICKS");
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showForwardHint(ChatMessageCell chatMessageCell) {
        View view;
        if (!this.scrollingChatListView && this.chatListView != null && getParentActivity() != null && (view = this.fragmentView) != null) {
            if (this.forwardHintView == null) {
                SizeNotifierFrameLayout sizeNotifierFrameLayout = (SizeNotifierFrameLayout) view;
                int indexOfChild = sizeNotifierFrameLayout.indexOfChild(this.chatActivityEnterView);
                if (indexOfChild != -1) {
                    HintView hintView = new HintView(getParentActivity(), 1, this.themeDelegate);
                    this.forwardHintView = hintView;
                    sizeNotifierFrameLayout.addView(hintView, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 19.0f, 0.0f, 19.0f, 0.0f));
                    this.forwardHintView.setAlpha(0.0f);
                    this.forwardHintView.setVisibility(4);
                } else {
                    return;
                }
            }
            this.forwardHintView.showForMessageCell(chatMessageCell, true);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void showTextSelectionHint(org.telegram.messenger.MessageObject r9) {
        /*
            r8 = this;
            android.app.Activity r0 = r8.getParentActivity()
            if (r0 == 0) goto L_0x0097
            org.telegram.messenger.MessagesController r0 = r8.getMessagesController()
            long r1 = r9.getChatId()
            boolean r0 = r0.isChatNoForwards(r1)
            if (r0 != 0) goto L_0x0097
            org.telegram.tgnet.TLRPC$Message r0 = r9.messageOwner
            if (r0 == 0) goto L_0x001e
            boolean r0 = r0.noforwards
            if (r0 == 0) goto L_0x001e
            goto L_0x0097
        L_0x001e:
            java.util.ArrayList<org.telegram.messenger.MessageObject$TextLayoutBlock> r0 = r9.textLayoutBlocks
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0036
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0036
            java.lang.CharSequence r0 = r9.messageText
            java.util.ArrayList<org.telegram.messenger.MessageObject$TextLayoutBlock> r9 = r9.textLayoutBlocks
            int r9 = r9.size()
            if (r9 <= r2) goto L_0x0038
            r9 = 1
            goto L_0x0039
        L_0x0036:
            java.lang.CharSequence r0 = r9.caption
        L_0x0038:
            r9 = 0
        L_0x0039:
            if (r9 != 0) goto L_0x0047
            if (r0 == 0) goto L_0x0047
            int r9 = r0.length()
            r0 = 200(0xc8, float:2.8E-43)
            if (r9 <= r0) goto L_0x0046
            r1 = 1
        L_0x0046:
            r9 = r1
        L_0x0047:
            if (r9 == 0) goto L_0x0097
            int r9 = org.telegram.messenger.SharedConfig.textSelectionHintShows
            r0 = 2
            if (r9 > r0) goto L_0x0097
            boolean r9 = r8.textSelectionHintWasShowed
            if (r9 != 0) goto L_0x0097
            float r9 = r8.lastTouchY
            org.telegram.ui.Components.ChatActivityEnterView r0 = r8.chatActivityEnterView
            int r0 = r0.getTop()
            r1 = 1114636288(0x42700000, float:60.0)
            int r1 = org.telegram.messenger.AndroidUtilities.dp(r1)
            int r0 = r0 - r1
            float r0 = (float) r0
            int r9 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r9 <= 0) goto L_0x0067
            goto L_0x0097
        L_0x0067:
            r8.textSelectionHintWasShowed = r2
            org.telegram.messenger.SharedConfig.increaseTextSelectionHintShowed()
            org.telegram.ui.Components.TextSelectionHint r9 = r8.textSelectionHint
            if (r9 != 0) goto L_0x0092
            org.telegram.ui.ChatActivity$68 r9 = new org.telegram.ui.ChatActivity$68
            android.app.Activity r0 = r8.getParentActivity()
            org.telegram.ui.ChatActivity$ThemeDelegate r1 = r8.themeDelegate
            r9.<init>(r0, r1)
            r8.textSelectionHint = r9
            org.telegram.ui.Components.SizeNotifierFrameLayout r0 = r8.contentView
            r1 = -2
            r2 = 1113587712(0x42600000, float:56.0)
            r3 = 83
            r4 = 1090519040(0x41000000, float:8.0)
            r5 = 0
            r6 = 1090519040(0x41000000, float:8.0)
            r7 = 1090519040(0x41000000, float:8.0)
            android.widget.FrameLayout$LayoutParams r1 = org.telegram.ui.Components.LayoutHelper.createFrame(r1, r2, r3, r4, r5, r6, r7)
            r0.addView(r9, r1)
        L_0x0092:
            org.telegram.ui.Components.TextSelectionHint r9 = r8.textSelectionHint
            r9.show()
        L_0x0097:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.showTextSelectionHint(org.telegram.messenger.MessageObject):void");
    }

    private boolean showGifHint() {
        View view;
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null && chatActivityEnterView.getVisibility() == 0) {
            SharedPreferences globalMainSettings = MessagesController.getGlobalMainSettings();
            if (globalMainSettings.getBoolean("gifhint", false)) {
                return false;
            }
            globalMainSettings.edit().putBoolean("gifhint", true).commit();
            if (!(getParentActivity() == null || (view = this.fragmentView) == null || this.gifHintTextView != null)) {
                if (!this.allowContextBotPanelSecond) {
                    ChatActivityEnterView chatActivityEnterView2 = this.chatActivityEnterView;
                    if (chatActivityEnterView2 != null) {
                        chatActivityEnterView2.setOpenGifsTabFirst();
                    }
                    return false;
                }
                SizeNotifierFrameLayout sizeNotifierFrameLayout = (SizeNotifierFrameLayout) view;
                int indexOfChild = sizeNotifierFrameLayout.indexOfChild(this.chatActivityEnterView);
                if (indexOfChild == -1) {
                    return false;
                }
                this.chatActivityEnterView.setOpenGifsTabFirst();
                View view2 = new View(getParentActivity());
                this.emojiButtonRed = view2;
                view2.setBackgroundResource(R.drawable.redcircle);
                int i = indexOfChild + 1;
                sizeNotifierFrameLayout.addView(this.emojiButtonRed, i, LayoutHelper.createFrame(10, 10.0f, 83, 30.0f, 0.0f, 0.0f, 27.0f));
                HintView hintView = new HintView(getParentActivity(), 9, this.themeDelegate);
                this.gifHintTextView = hintView;
                hintView.setText(LocaleController.getString("TapHereGifs", R.string.TapHereGifs));
                sizeNotifierFrameLayout.addView(this.gifHintTextView, i, LayoutHelper.createFrame(-2, -2.0f, 83, 5.0f, 0.0f, 5.0f, 3.0f));
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(ObjectAnimator.ofFloat(this.gifHintTextView, View.ALPHA, 0.0f, 1.0f), ObjectAnimator.ofFloat(this.emojiButtonRed, View.ALPHA, 0.0f, 1.0f));
                animatorSet.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.69
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        AndroidUtilities.runOnUIThread(new ChatActivity$69$$ExternalSyntheticLambda0(this), 2000);
                    }

                    /* access modifiers changed from: private */
                    public /* synthetic */ void lambda$onAnimationEnd$0() {
                        if (ChatActivity.this.gifHintTextView != null) {
                            AnimatorSet animatorSet2 = new AnimatorSet();
                            animatorSet2.playTogether(ObjectAnimator.ofFloat(ChatActivity.this.gifHintTextView, View.ALPHA, 0.0f));
                            animatorSet2.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.69.1
                                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                                public void onAnimationEnd(Animator animator) {
                                    if (ChatActivity.this.gifHintTextView != null) {
                                        ChatActivity.this.gifHintTextView.setVisibility(8);
                                    }
                                }
                            });
                            animatorSet2.setDuration(300L);
                            animatorSet2.start();
                        }
                    }
                });
                animatorSet.setDuration(300L);
                animatorSet.start();
                View emojiButton = this.chatActivityEnterView.getEmojiButton();
                if (emojiButton != null) {
                    this.gifHintTextView.showForView(emojiButton, true);
                }
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void openAttachMenu() {
        if (getParentActivity() != null) {
            ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
            if (chatActivityEnterView == null || TextUtils.isEmpty(chatActivityEnterView.getSlowModeTimer())) {
                createChatAttachView();
                this.chatAttachAlert.getPhotoLayout().loadGalleryPhotos();
                int i = Build.VERSION.SDK_INT;
                if (i == 21 || i == 22) {
                    this.chatActivityEnterView.closeKeyboard();
                }
                TLRPC$Chat tLRPC$Chat = this.currentChat;
                if (tLRPC$Chat == null || ChatObject.hasAdminRights(tLRPC$Chat) || !this.currentChat.slowmode_enabled) {
                    this.chatAttachAlert.setMaxSelectedPhotos(-1, true);
                } else {
                    this.chatAttachAlert.setMaxSelectedPhotos(10, true);
                }
                this.chatAttachAlert.init();
                this.chatAttachAlert.getCommentTextView().setText(this.chatActivityEnterView.getFieldText());
                ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
                chatAttachAlert.parentThemeDelegate = this.themeDelegate;
                showDialog(chatAttachAlert);
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkAutoDownloadMessages(boolean z) {
        if (this.chatListView != null) {
            showNoSoundHint();
        }
    }

    /* access modifiers changed from: private */
    public void showFloatingDateView(boolean z) {
        ChatActionCell chatActionCell = this.floatingDateView;
        if (chatActionCell != null) {
            if (chatActionCell.getTag() == null) {
                AnimatorSet animatorSet = this.floatingDateAnimation;
                if (animatorSet != null) {
                    animatorSet.cancel();
                }
                this.floatingDateView.setTag(1);
                AnimatorSet animatorSet2 = new AnimatorSet();
                this.floatingDateAnimation = animatorSet2;
                animatorSet2.setDuration(150L);
                this.floatingDateAnimation.playTogether(ObjectAnimator.ofFloat(this.floatingDateView, View.ALPHA, 1.0f));
                this.floatingDateAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.70
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        if (animator.equals(ChatActivity.this.floatingDateAnimation)) {
                            ChatActivity.this.floatingDateAnimation = null;
                        }
                    }
                });
                this.floatingDateAnimation.start();
            }
            if (!z) {
                invalidateMessagesVisiblePart();
                this.hideDateDelay = 1000;
            }
        }
    }

    /* access modifiers changed from: private */
    public void hideFloatingDateView(boolean z) {
        if (this.floatingDateView.getTag() != null && !this.currentFloatingDateOnScreen) {
            if (!this.scrollingFloatingDate || this.currentFloatingTopIsNotMessage) {
                this.floatingDateView.setTag(null);
                if (z) {
                    AnimatorSet animatorSet = new AnimatorSet();
                    this.floatingDateAnimation = animatorSet;
                    animatorSet.setDuration(150L);
                    this.floatingDateAnimation.playTogether(ObjectAnimator.ofFloat(this.floatingDateView, View.ALPHA, 0.0f));
                    this.floatingDateAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.71
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            if (animator.equals(ChatActivity.this.floatingDateAnimation)) {
                                ChatActivity.this.floatingDateAnimation = null;
                            }
                        }
                    });
                    this.floatingDateAnimation.setStartDelay((long) this.hideDateDelay);
                    this.floatingDateAnimation.start();
                } else {
                    AnimatorSet animatorSet2 = this.floatingDateAnimation;
                    if (animatorSet2 != null) {
                        animatorSet2.cancel();
                        this.floatingDateAnimation = null;
                    }
                    this.floatingDateView.setAlpha(0.0f);
                }
                this.hideDateDelay = 500;
            }
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected void onRemoveFromParent() {
        MessageObject playingMessageObject = MediaController.getInstance().getPlayingMessageObject();
        if (playingMessageObject == null || !playingMessageObject.isVideo()) {
            MediaController.getInstance().setTextureView(this.videoTextureView, null, null, false);
        } else {
            MediaController.getInstance().cleanupPlayer(true, true);
        }
    }

    /* access modifiers changed from: protected */
    public void setIgnoreAttachOnPause(boolean z) {
        this.ignoreAttachOnPause = z;
    }

    public ChatActivityEnterView getChatActivityEnterViewForStickers() {
        TLRPC$Chat tLRPC$Chat;
        if (this.bottomOverlayChat.getVisibility() == 0 || ((tLRPC$Chat = this.currentChat) != null && !ChatObject.canSendStickers(tLRPC$Chat))) {
            return null;
        }
        return this.chatActivityEnterView;
    }

    public ChatActivityEnterView getChatActivityEnterView() {
        return this.chatActivityEnterView;
    }

    public boolean isKeyboardVisible() {
        return this.contentView.getKeyboardHeight() > AndroidUtilities.dp(20.0f);
    }

    /* access modifiers changed from: private */
    public void checkScrollForLoad(boolean z) {
        int i;
        GridLayoutManagerFixed gridLayoutManagerFixed = this.chatLayoutManager;
        if (gridLayoutManagerFixed != null && !this.paused && !this.chatAdapter.isFrozen) {
            int findFirstVisibleItemPosition = gridLayoutManagerFixed.findFirstVisibleItemPosition();
            if (findFirstVisibleItemPosition == -1) {
                i = 0;
            } else {
                i = Math.abs(this.chatLayoutManager.findLastVisibleItemPosition() - findFirstVisibleItemPosition) + 1;
            }
            if ((this.chatAdapter.getItemCount() - findFirstVisibleItemPosition) - i <= (z ? 25 : 5) && !this.loading) {
                boolean[] zArr = this.endReached;
                if (!zArr[0]) {
                    this.loading = true;
                    this.waitingForLoad.add(Integer.valueOf(this.lastLoadIndex));
                    if (this.messagesByDays.size() != 0) {
                        MessagesController messagesController = getMessagesController();
                        long j = this.dialog_id;
                        long j2 = this.mergeDialogId;
                        int i2 = this.maxMessageId[0];
                        boolean z2 = !this.cacheEndReached[0];
                        int i3 = this.minDate[0];
                        int i4 = this.classGuid;
                        int i5 = this.chatMode;
                        int i6 = this.threadMessageId;
                        int i7 = this.replyMaxReadId;
                        int i8 = this.lastLoadIndex;
                        this.lastLoadIndex = i8 + 1;
                        messagesController.loadMessages(j, j2, false, 50, i2, 0, z2, i3, i4, 0, 0, i5, i6, i7, i8);
                    } else {
                        MessagesController messagesController2 = getMessagesController();
                        long j3 = this.dialog_id;
                        long j4 = this.mergeDialogId;
                        boolean z3 = !this.cacheEndReached[0];
                        int i9 = this.minDate[0];
                        int i10 = this.classGuid;
                        int i11 = this.chatMode;
                        int i12 = this.threadMessageId;
                        int i13 = this.replyMaxReadId;
                        int i14 = this.lastLoadIndex;
                        this.lastLoadIndex = i14 + 1;
                        messagesController2.loadMessages(j3, j4, false, 50, 0, 0, z3, i9, i10, 0, 0, i11, i12, i13, i14);
                    }
                } else if (this.mergeDialogId != 0 && !zArr[1]) {
                    this.loading = true;
                    this.waitingForLoad.add(Integer.valueOf(this.lastLoadIndex));
                    MessagesController messagesController3 = getMessagesController();
                    long j5 = this.mergeDialogId;
                    int i15 = this.maxMessageId[1];
                    boolean z4 = !this.cacheEndReached[1];
                    int i16 = this.minDate[1];
                    int i17 = this.classGuid;
                    int i18 = this.chatMode;
                    int i19 = this.threadMessageId;
                    int i20 = this.replyMaxReadId;
                    int i21 = this.lastLoadIndex;
                    this.lastLoadIndex = i21 + 1;
                    messagesController3.loadMessages(j5, 0, false, 50, i15, 0, z4, i16, i17, 0, 0, i18, i19, i20, i21);
                }
            }
            if (i > 0 && !this.loadingForward && findFirstVisibleItemPosition <= 10) {
                if (this.mergeDialogId != 0 && !this.forwardEndReached[1]) {
                    this.waitingForLoad.add(Integer.valueOf(this.lastLoadIndex));
                    MessagesController messagesController4 = getMessagesController();
                    long j6 = this.mergeDialogId;
                    int i22 = this.minMessageId[1];
                    int i23 = this.maxDate[1];
                    int i24 = this.classGuid;
                    int i25 = this.chatMode;
                    int i26 = this.threadMessageId;
                    int i27 = this.replyMaxReadId;
                    int i28 = this.lastLoadIndex;
                    this.lastLoadIndex = i28 + 1;
                    messagesController4.loadMessages(j6, 0, false, 50, i22, 0, true, i23, i24, 1, 0, i25, i26, i27, i28);
                    this.loadingForward = true;
                } else if (!this.forwardEndReached[0]) {
                    this.waitingForLoad.add(Integer.valueOf(this.lastLoadIndex));
                    MessagesController messagesController5 = getMessagesController();
                    long j7 = this.dialog_id;
                    long j8 = this.mergeDialogId;
                    int i29 = this.minMessageId[0];
                    int i30 = this.maxDate[0];
                    int i31 = this.classGuid;
                    int i32 = this.chatMode;
                    int i33 = this.threadMessageId;
                    int i34 = this.replyMaxReadId;
                    int i35 = this.lastLoadIndex;
                    this.lastLoadIndex = i35 + 1;
                    messagesController5.loadMessages(j7, j8, false, 50, i29, 0, true, i30, i31, 1, 0, i32, i33, i34, i35);
                    this.loadingForward = true;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void processSelectedAttach(int i) {
        TLRPC$TL_chatBannedRights tLRPC$TL_chatBannedRights;
        boolean z = false;
        if (i == 0) {
            int i2 = Build.VERSION.SDK_INT;
            if (i2 < 23 || getParentActivity().checkSelfPermission("android.permission.CAMERA") == 0) {
                try {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File generatePicturePath = AndroidUtilities.generatePicturePath();
                    if (generatePicturePath != null) {
                        if (i2 >= 24) {
                            intent.putExtra("output", FileProvider.getUriForFile(getParentActivity(), "org.telegram.messenger.web.provider", generatePicturePath));
                            intent.addFlags(2);
                            intent.addFlags(1);
                        } else {
                            intent.putExtra("output", Uri.fromFile(generatePicturePath));
                        }
                        this.currentPicturePath = generatePicturePath.getAbsolutePath();
                    }
                    startActivityForResult(intent, 0);
                } catch (Exception e) {
                    FileLog.e(e);
                }
            } else {
                getParentActivity().requestPermissions(new String[]{"android.permission.CAMERA"}, 19);
            }
        } else if (i == 1) {
            if (Build.VERSION.SDK_INT < 23 || getParentActivity().checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == 0) {
                PhotoAlbumPickerActivity photoAlbumPickerActivity = new PhotoAlbumPickerActivity(PhotoAlbumPickerActivity.SELECT_TYPE_ALL, !ChatObject.isChannel(this.currentChat) || (tLRPC$TL_chatBannedRights = this.currentChat.banned_rights) == null || !tLRPC$TL_chatBannedRights.send_gifs, true, this);
                TLRPC$Chat tLRPC$Chat = this.currentChat;
                if (tLRPC$Chat == null || ChatObject.hasAdminRights(tLRPC$Chat) || !this.currentChat.slowmode_enabled) {
                    MessageObject messageObject = this.editingMessageObject;
                    int i3 = messageObject != null ? 1 : 0;
                    if (messageObject == null) {
                        z = true;
                    }
                    photoAlbumPickerActivity.setMaxSelectedPhotos(i3, z);
                } else {
                    photoAlbumPickerActivity.setMaxSelectedPhotos(10, true);
                }
                photoAlbumPickerActivity.setDelegate(new PhotoAlbumPickerActivity.PhotoAlbumPickerActivityDelegate() { // from class: org.telegram.ui.ChatActivity.72
                    @Override // org.telegram.ui.PhotoAlbumPickerActivity.PhotoAlbumPickerActivityDelegate
                    public void didSelectPhotos(ArrayList<SendMessagesHelper.SendingMediaInfo> arrayList, boolean z2, int i4) {
                    }

                    @Override // org.telegram.ui.PhotoAlbumPickerActivity.PhotoAlbumPickerActivityDelegate
                    public void startPhotoSelectActivity() {
                        try {
                            Intent intent2 = new Intent();
                            intent2.setType("video/*");
                            intent2.setAction("android.intent.action.GET_CONTENT");
                            intent2.putExtra("android.intent.extra.sizeLimit", FileLoader.DEFAULT_MAX_FILE_SIZE);
                            Intent intent3 = new Intent("android.intent.action.PICK");
                            intent3.setType("image/*");
                            Intent createChooser = Intent.createChooser(intent3, null);
                            createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", new Intent[]{intent2});
                            ChatActivity.this.startActivityForResult(createChooser, 1);
                        } catch (Exception e2) {
                            FileLog.e(e2);
                        }
                    }
                });
                presentFragment(photoAlbumPickerActivity);
                return;
            }
            try {
                getParentActivity().requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 4);
            } catch (Throwable unused) {
            }
        } else if (i == 2) {
            int i4 = Build.VERSION.SDK_INT;
            if (i4 < 23 || getParentActivity().checkSelfPermission("android.permission.CAMERA") == 0) {
                try {
                    Intent intent2 = new Intent("android.media.action.VIDEO_CAPTURE");
                    File generateVideoPath = AndroidUtilities.generateVideoPath();
                    if (generateVideoPath != null) {
                        if (i4 >= 24) {
                            intent2.putExtra("output", FileProvider.getUriForFile(getParentActivity(), "org.telegram.messenger.web.provider", generateVideoPath));
                            intent2.addFlags(2);
                            intent2.addFlags(1);
                        } else if (i4 >= 18) {
                            intent2.putExtra("output", Uri.fromFile(generateVideoPath));
                        }
                        intent2.putExtra("android.intent.extra.sizeLimit", FileLoader.DEFAULT_MAX_FILE_SIZE);
                        this.currentPicturePath = generateVideoPath.getAbsolutePath();
                    }
                    startActivityForResult(intent2, 2);
                } catch (Exception e2) {
                    FileLog.e(e2);
                }
            } else {
                try {
                    getParentActivity().requestPermissions(new String[]{"android.permission.CAMERA"}, 20);
                } catch (Throwable unused2) {
                }
            }
        }
    }

    public boolean allowSendGifs() {
        TLRPC$TL_chatBannedRights tLRPC$TL_chatBannedRights;
        return !ChatObject.isChannel(this.currentChat) || (tLRPC$TL_chatBannedRights = this.currentChat.banned_rights) == null || !tLRPC$TL_chatBannedRights.send_gifs;
    }

    public void openPollCreate(Boolean bool) {
        PollCreateActivity pollCreateActivity = new PollCreateActivity(this, bool);
        pollCreateActivity.setDelegate(new PollCreateActivity.PollCreateActivityDelegate() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda257
            @Override // org.telegram.ui.PollCreateActivity.PollCreateActivityDelegate
            public final void sendPoll(TLRPC$TL_messageMediaPoll tLRPC$TL_messageMediaPoll, HashMap hashMap, boolean z, int i) {
                ChatActivity.this.lambda$openPollCreate$93(tLRPC$TL_messageMediaPoll, hashMap, z, i);
            }
        });
        presentFragment(pollCreateActivity);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openPollCreate$93(TLRPC$TL_messageMediaPoll tLRPC$TL_messageMediaPoll, HashMap hashMap, boolean z, int i) {
        getSendMessagesHelper().sendMessage(tLRPC$TL_messageMediaPoll, this.dialog_id, this.replyingMessageObject, getThreadMessage(), (TLRPC$ReplyMarkup) null, (HashMap<String, String>) hashMap, z, i);
        afterMessageSend();
    }

    @Override // org.telegram.ui.Components.ChatAttachAlertDocumentLayout.DocumentSelectActivityDelegate
    public void didSelectFiles(ArrayList<String> arrayList, String str, ArrayList<MessageObject> arrayList2, boolean z, int i) {
        String str2;
        fillEditingMediaWithCaption(str, null);
        if (arrayList2.isEmpty() || TextUtils.isEmpty(str)) {
            str2 = str;
        } else {
            SendMessagesHelper.getInstance(this.currentAccount).sendMessage(str, this.dialog_id, null, null, null, true, null, null, null, true, 0, null);
            str2 = null;
        }
        getSendMessagesHelper().sendMessage(arrayList2, this.dialog_id, false, false, true, 0);
        SendMessagesHelper.prepareSendingDocuments(getAccountInstance(), arrayList, arrayList, null, str2, null, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, this.editingMessageObject, z, i);
        afterMessageSend();
    }

    @Override // org.telegram.ui.Components.ChatAttachAlertDocumentLayout.DocumentSelectActivityDelegate
    public void didSelectPhotos(ArrayList<SendMessagesHelper.SendingMediaInfo> arrayList, boolean z, int i) {
        fillEditingMediaWithCaption(arrayList.get(0).caption, arrayList.get(0).entities);
        SendMessagesHelper.prepareSendingMedia(getAccountInstance(), arrayList, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, true, false, this.editingMessageObject, z, i);
        afterMessageSend();
        if (i != 0) {
            if (this.scheduledMessagesCount == -1) {
                this.scheduledMessagesCount = 0;
            }
            this.scheduledMessagesCount += arrayList.size();
            updateScheduledInterface(true);
        }
    }

    public void didSelectSearchPhotos(ArrayList<SendMessagesHelper.SendingMediaInfo> arrayList, boolean z, int i) {
        boolean z2;
        if (!arrayList.isEmpty()) {
            int i2 = 0;
            while (true) {
                if (i2 >= arrayList.size()) {
                    z2 = false;
                    break;
                }
                SendMessagesHelper.SendingMediaInfo sendingMediaInfo = arrayList.get(i2);
                if (sendingMediaInfo.inlineResult == null && sendingMediaInfo.videoEditedInfo == null) {
                    z2 = true;
                    break;
                }
                i2++;
            }
            if (!z2 && !TextUtils.isEmpty(arrayList.get(0).caption)) {
                SendMessagesHelper.getInstance(this.currentAccount).sendMessage(arrayList.get(0).caption, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, false, arrayList.get(0).entities, null, null, z, i, null);
            }
            int i3 = 0;
            while (i3 < arrayList.size()) {
                SendMessagesHelper.SendingMediaInfo sendingMediaInfo2 = arrayList.get(i3);
                if (sendingMediaInfo2.inlineResult != null && sendingMediaInfo2.videoEditedInfo == null) {
                    SendMessagesHelper.prepareSendingBotContextResult(getAccountInstance(), sendingMediaInfo2.inlineResult, sendingMediaInfo2.params, this.dialog_id, this.replyingMessageObject, getThreadMessage(), z, i);
                    arrayList.remove(i3);
                    i3--;
                }
                i3++;
            }
            if (!arrayList.isEmpty()) {
                fillEditingMediaWithCaption(arrayList.get(0).caption, arrayList.get(0).entities);
                SendMessagesHelper.prepareSendingMedia(getAccountInstance(), arrayList, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, false, true, this.editingMessageObject, z, i);
                afterMessageSend();
                if (i != 0) {
                    if (this.scheduledMessagesCount == -1) {
                        this.scheduledMessagesCount = 0;
                    }
                    this.scheduledMessagesCount += arrayList.size();
                    updateScheduledInterface(true);
                }
            }
        }
    }

    @Override // org.telegram.ui.Components.ChatAttachAlertDocumentLayout.DocumentSelectActivityDelegate
    public void startDocumentSelectActivity() {
        try {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            if (Build.VERSION.SDK_INT >= 18) {
                intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
            }
            intent.setType("*/*");
            startActivityForResult(intent, 21);
        } catch (Exception e) {
            FileLog.e(e);
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public boolean dismissDialogOnPause(Dialog dialog) {
        return dialog != this.chatAttachAlert && super.dismissDialogOnPause(dialog);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x007e A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void searchLinks(java.lang.CharSequence r7, boolean r8) {
        /*
            r6 = this;
            org.telegram.tgnet.TLRPC$EncryptedChat r0 = r6.currentEncryptedChat
            if (r0 == 0) goto L_0x000c
            org.telegram.messenger.MessagesController r0 = r6.getMessagesController()
            int r0 = r0.secretWebpagePreview
            if (r0 == 0) goto L_0x0016
        L_0x000c:
            org.telegram.messenger.MessageObject r0 = r6.editingMessageObject
            if (r0 == 0) goto L_0x0017
            boolean r0 = r0.isWebpage()
            if (r0 != 0) goto L_0x0017
        L_0x0016:
            return
        L_0x0017:
            if (r8 == 0) goto L_0x009f
            org.telegram.tgnet.TLRPC$WebPage r0 = r6.foundWebPage
            if (r0 == 0) goto L_0x009f
            java.lang.String r0 = r0.url
            r1 = 0
            if (r0 == 0) goto L_0x0095
            int r0 = android.text.TextUtils.indexOf(r7, r0)
            r2 = 1
            r3 = -1
            if (r0 != r3) goto L_0x005c
            org.telegram.tgnet.TLRPC$WebPage r4 = r6.foundWebPage
            java.lang.String r4 = r4.display_url
            if (r4 == 0) goto L_0x0059
            int r0 = android.text.TextUtils.indexOf(r7, r4)
            if (r0 == r3) goto L_0x0046
            org.telegram.tgnet.TLRPC$WebPage r4 = r6.foundWebPage
            java.lang.String r4 = r4.display_url
            int r4 = r4.length()
            int r4 = r4 + r0
            int r5 = r7.length()
            if (r4 != r5) goto L_0x0046
            goto L_0x0047
        L_0x0046:
            r2 = 0
        L_0x0047:
            if (r0 == r3) goto L_0x005a
            if (r2 != 0) goto L_0x005a
            org.telegram.tgnet.TLRPC$WebPage r4 = r6.foundWebPage
            java.lang.String r4 = r4.display_url
            int r4 = r4.length()
            int r4 = r4 + r0
            char r4 = r7.charAt(r4)
            goto L_0x007c
        L_0x0059:
            r2 = 0
        L_0x005a:
            r4 = 0
            goto L_0x007c
        L_0x005c:
            org.telegram.tgnet.TLRPC$WebPage r4 = r6.foundWebPage
            java.lang.String r4 = r4.url
            int r4 = r4.length()
            int r4 = r4 + r0
            int r5 = r7.length()
            if (r4 != r5) goto L_0x006c
            goto L_0x006d
        L_0x006c:
            r2 = 0
        L_0x006d:
            if (r2 != 0) goto L_0x005a
            org.telegram.tgnet.TLRPC$WebPage r4 = r6.foundWebPage
            java.lang.String r4 = r4.url
            int r4 = r4.length()
            int r4 = r4 + r0
            char r4 = r7.charAt(r4)
        L_0x007c:
            if (r0 == r3) goto L_0x0095
            if (r2 != 0) goto L_0x0094
            r0 = 32
            if (r4 == r0) goto L_0x0094
            r0 = 44
            if (r4 == r0) goto L_0x0094
            r0 = 46
            if (r4 == r0) goto L_0x0094
            r0 = 33
            if (r4 == r0) goto L_0x0094
            r0 = 47
            if (r4 != r0) goto L_0x0095
        L_0x0094:
            return
        L_0x0095:
            r0 = 0
            r6.pendingLinkSearchString = r0
            r6.foundUrls = r0
            org.telegram.tgnet.TLRPC$WebPage r0 = r6.foundWebPage
            r6.showFieldPanelForWebPage(r1, r0, r1)
        L_0x009f:
            org.telegram.messenger.MessagesController r0 = r6.getMessagesController()
            org.telegram.messenger.DispatchQueue r1 = org.telegram.messenger.Utilities.searchQueue
            org.telegram.ui.ChatActivity$$ExternalSyntheticLambda170 r2 = new org.telegram.ui.ChatActivity$$ExternalSyntheticLambda170
            r2.<init>(r7, r0, r8)
            r1.postRunnable(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.searchLinks(java.lang.CharSequence, boolean):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$100(CharSequence charSequence, MessagesController messagesController, boolean z) {
        CharSequence charSequence2;
        URLSpanReplacement[] uRLSpanReplacementArr;
        boolean z2 = true;
        if (this.linkSearchRequestId != 0) {
            getConnectionsManager().cancelRequest(this.linkSearchRequestId, true);
            this.linkSearchRequestId = 0;
        }
        ArrayList<CharSequence> arrayList = null;
        try {
            Matcher matcher = AndroidUtilities.WEB_URL.matcher(charSequence);
            while (matcher.find()) {
                if (matcher.start() <= 0 || charSequence.charAt(matcher.start() - 1) != '@') {
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(charSequence.subSequence(matcher.start(), matcher.end()));
                }
            }
            if ((charSequence instanceof Spannable) && (uRLSpanReplacementArr = (URLSpanReplacement[]) ((Spannable) charSequence).getSpans(0, charSequence.length(), URLSpanReplacement.class)) != null && uRLSpanReplacementArr.length > 0) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                for (URLSpanReplacement uRLSpanReplacement : uRLSpanReplacementArr) {
                    arrayList.add(uRLSpanReplacement.getURL());
                }
            }
            if (!(arrayList == null || this.foundUrls == null || arrayList.size() != this.foundUrls.size())) {
                for (int i = 0; i < arrayList.size(); i++) {
                    if (!TextUtils.equals(arrayList.get(i), this.foundUrls.get(i))) {
                        z2 = false;
                    }
                }
                if (z2) {
                    return;
                }
            }
            this.foundUrls = arrayList;
        } catch (Exception e) {
            FileLog.e(e);
            String lowerCase = charSequence.toString().toLowerCase();
            if (charSequence.length() < 13 || (!lowerCase.contains("http://") && !lowerCase.contains("https://"))) {
                AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda148
                    @Override // java.lang.Runnable
                    public final void run() {
                        ChatActivity.this.lambda$searchLinks$95();
                    }
                });
                return;
            }
            charSequence2 = charSequence;
        }
        if (arrayList == null) {
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda153
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$searchLinks$94();
                }
            });
            return;
        }
        charSequence2 = TextUtils.join(" ", arrayList);
        if (this.currentEncryptedChat == null || messagesController.secretWebpagePreview != 2) {
            TLRPC$TL_messages_getWebPagePreview tLRPC$TL_messages_getWebPagePreview = new TLRPC$TL_messages_getWebPagePreview();
            if (charSequence2 instanceof String) {
                tLRPC$TL_messages_getWebPagePreview.message = (String) charSequence2;
            } else {
                tLRPC$TL_messages_getWebPagePreview.message = charSequence2.toString();
            }
            this.linkSearchRequestId = getConnectionsManager().sendRequest(tLRPC$TL_messages_getWebPagePreview, new RequestDelegate(tLRPC$TL_messages_getWebPagePreview) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda226
                public final /* synthetic */ TLRPC$TL_messages_getWebPagePreview f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.telegram.tgnet.RequestDelegate
                public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                    ChatActivity.this.lambda$searchLinks$99(this.f$1, tLObject, tLRPC$TL_error);
                }
            });
            getConnectionsManager().bindRequestToGuid(this.linkSearchRequestId, this.classGuid);
            return;
        }
        AndroidUtilities.runOnUIThread(new Runnable(messagesController, charSequence, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda178
            public final /* synthetic */ MessagesController f$1;
            public final /* synthetic */ CharSequence f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$searchLinks$97(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$94() {
        TLRPC$WebPage tLRPC$WebPage = this.foundWebPage;
        if (tLRPC$WebPage != null) {
            showFieldPanelForWebPage(false, tLRPC$WebPage, false);
            this.foundWebPage = null;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$95() {
        TLRPC$WebPage tLRPC$WebPage = this.foundWebPage;
        if (tLRPC$WebPage != null) {
            showFieldPanelForWebPage(false, tLRPC$WebPage, false);
            this.foundWebPage = null;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$97(MessagesController messagesController, CharSequence charSequence, boolean z) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
        builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
        builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), new DialogInterface.OnClickListener(messagesController, charSequence, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda39
            public final /* synthetic */ MessagesController f$1;
            public final /* synthetic */ CharSequence f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatActivity.this.lambda$searchLinks$96(this.f$1, this.f$2, this.f$3, dialogInterface, i);
            }
        });
        builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
        builder.setMessage(LocaleController.getString("SecretLinkPreviewAlert", R.string.SecretLinkPreviewAlert));
        showDialog(builder.create());
        messagesController.secretWebpagePreview = 0;
        MessagesController.getGlobalMainSettings().edit().putInt("secretWebpage2", messagesController.secretWebpagePreview).commit();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$96(MessagesController messagesController, CharSequence charSequence, boolean z, DialogInterface dialogInterface, int i) {
        messagesController.secretWebpagePreview = 1;
        MessagesController.getGlobalMainSettings().edit().putInt("secretWebpage2", getMessagesController().secretWebpagePreview).commit();
        this.foundUrls = null;
        searchLinks(charSequence, z);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$99(TLRPC$TL_messages_getWebPagePreview tLRPC$TL_messages_getWebPagePreview, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLRPC$TL_error, tLObject, tLRPC$TL_messages_getWebPagePreview) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda190
            public final /* synthetic */ TLRPC$TL_error f$1;
            public final /* synthetic */ TLObject f$2;
            public final /* synthetic */ TLRPC$TL_messages_getWebPagePreview f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$searchLinks$98(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$searchLinks$98(TLRPC$TL_error tLRPC$TL_error, TLObject tLObject, TLRPC$TL_messages_getWebPagePreview tLRPC$TL_messages_getWebPagePreview) {
        this.linkSearchRequestId = 0;
        if (tLRPC$TL_error != null) {
            return;
        }
        if (tLObject instanceof TLRPC$TL_messageMediaWebPage) {
            TLRPC$WebPage tLRPC$WebPage = ((TLRPC$TL_messageMediaWebPage) tLObject).webpage;
            this.foundWebPage = tLRPC$WebPage;
            if ((tLRPC$WebPage instanceof TLRPC$TL_webPage) || (tLRPC$WebPage instanceof TLRPC$TL_webPagePending)) {
                if (tLRPC$WebPage instanceof TLRPC$TL_webPagePending) {
                    this.pendingLinkSearchString = tLRPC$TL_messages_getWebPagePreview.message;
                }
                if (this.currentEncryptedChat != null && (tLRPC$WebPage instanceof TLRPC$TL_webPagePending)) {
                    tLRPC$WebPage.url = tLRPC$TL_messages_getWebPagePreview.message;
                }
                showFieldPanelForWebPage(true, tLRPC$WebPage, false);
            } else if (tLRPC$WebPage != null) {
                showFieldPanelForWebPage(false, tLRPC$WebPage, false);
                this.foundWebPage = null;
            }
        } else {
            TLRPC$WebPage tLRPC$WebPage2 = this.foundWebPage;
            if (tLRPC$WebPage2 != null) {
                showFieldPanelForWebPage(false, tLRPC$WebPage2, false);
                this.foundWebPage = null;
            }
        }
    }

    private void forwardMessages(ArrayList<MessageObject> arrayList, boolean z, boolean z2, boolean z3, int i) {
        if (arrayList != null && !arrayList.isEmpty()) {
            boolean z4 = false;
            boolean z5 = i != 0;
            if (this.chatMode == 1) {
                z4 = true;
            }
            if (z5 == z4) {
                this.waitingForSendingMessageLoad = true;
            }
            int sendMessage = getSendMessagesHelper().sendMessage(arrayList, this.dialog_id, z, z2, z3, i);
            AlertsCreator.showSendMediaAlert(sendMessage, this, this.themeDelegate);
            if (sendMessage != 0) {
                AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda139
                    @Override // java.lang.Runnable
                    public final void run() {
                        ChatActivity.this.lambda$forwardMessages$101();
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$forwardMessages$101() {
        this.waitingForSendingMessageLoad = false;
        hideFieldPanel(true);
    }

    public boolean shouldShowImport() {
        return this.openImport;
    }

    public void setOpenImport() {
        this.openImport = true;
    }

    private void checkBotKeyboard() {
        MessageObject messageObject;
        if (this.chatActivityEnterView != null && (messageObject = this.botButtons) != null && !this.userBlocked) {
            if (messageObject.messageOwner.reply_markup instanceof TLRPC$TL_replyKeyboardForceReply) {
                SharedPreferences mainSettings = MessagesController.getMainSettings(this.currentAccount);
                if (mainSettings.getInt("answered_" + this.dialog_id, 0) == this.botButtons.getId()) {
                    return;
                }
                if (this.replyingMessageObject == null || this.chatActivityEnterView.getFieldText() == null) {
                    MessageObject messageObject2 = this.botButtons;
                    this.botReplyButtons = messageObject2;
                    this.chatActivityEnterView.setButtons(messageObject2);
                    showFieldPanelForReply(this.botButtons);
                    return;
                }
                return;
            }
            MessageObject messageObject3 = this.replyingMessageObject;
            if (messageObject3 != null && this.botReplyButtons == messageObject3) {
                this.botReplyButtons = null;
                hideFieldPanel(true);
            }
            this.chatActivityEnterView.setButtons(this.botButtons);
        }
    }

    public void hideFieldPanel(boolean z) {
        showFieldPanel(false, null, null, null, null, true, 0, false, z);
    }

    public void hideFieldPanel(boolean z, int i, boolean z2) {
        showFieldPanel(false, null, null, null, null, z, i, false, z2);
    }

    public void showFieldPanelForWebPage(boolean z, TLRPC$WebPage tLRPC$WebPage, boolean z2) {
        showFieldPanel(z, null, null, null, tLRPC$WebPage, true, 0, z2, true);
    }

    public void showFieldPanelForForward(boolean z, ArrayList<MessageObject> arrayList) {
        showFieldPanel(z, null, null, arrayList, null, true, 0, false, true);
    }

    public void showFieldPanelForReply(MessageObject messageObject) {
        showFieldPanel(true, messageObject, null, null, null, true, 0, false, true);
    }

    public void showFieldPanelForEdit(boolean z, MessageObject messageObject) {
        showFieldPanel(z, null, messageObject, null, null, true, 0, false, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:135:0x0321  */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x0782  */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x07a9  */
    /* JADX WARNING: Removed duplicated region for block: B:352:0x080e  */
    /* JADX WARNING: Removed duplicated region for block: B:354:0x0811  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void showFieldPanel(boolean r30, org.telegram.messenger.MessageObject r31, org.telegram.messenger.MessageObject r32, java.util.ArrayList<org.telegram.messenger.MessageObject> r33, org.telegram.tgnet.TLRPC$WebPage r34, boolean r35, int r36, boolean r37, boolean r38) {
        /*
        // Method dump skipped, instructions count: 2490
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.showFieldPanel(boolean, org.telegram.messenger.MessageObject, org.telegram.messenger.MessageObject, java.util.ArrayList, org.telegram.tgnet.TLRPC$WebPage, boolean, int, boolean, boolean):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showFieldPanel$102() {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.openKeyboard();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showFieldPanel$103() {
        this.showTapForForwardingOptionsHit = !this.showTapForForwardingOptionsHit;
        this.replyObjectTextView.setPivotX(0.0f);
        this.replyObjectHintTextView.setPivotX(0.0f);
        if (this.showTapForForwardingOptionsHit) {
            this.replyObjectTextView.animate().alpha(0.0f).scaleX(0.98f).scaleY(0.98f).setDuration(150).start();
            this.replyObjectHintTextView.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setDuration(150).start();
        } else {
            this.replyObjectTextView.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setDuration(150).start();
            this.replyObjectHintTextView.animate().alpha(0.0f).scaleX(0.98f).scaleY(0.98f).setDuration(150).start();
        }
        AndroidUtilities.runOnUIThread(this.tapForForwardingOptionsHitRunnable, 6000);
    }

    private void moveScrollToLastMessage(boolean z) {
        if (this.chatListView != null && !this.messages.isEmpty() && !this.pinchToZoomHelper.isInOverlayMode()) {
            this.chatLayoutManager.scrollToPositionWithOffset(z ? getSponsoredMessagesCount() + 0 : 0, 0);
            this.chatListView.stopScroll();
        }
    }

    /* access modifiers changed from: private */
    public Runnable sendSecretMessageRead(MessageObject messageObject, boolean z) {
        int i;
        if (messageObject == null || messageObject.isOut() || !messageObject.isSecretMedia()) {
            return null;
        }
        TLRPC$Message tLRPC$Message = messageObject.messageOwner;
        if (tLRPC$Message.destroyTime != 0 || (i = tLRPC$Message.ttl) <= 0) {
            return null;
        }
        tLRPC$Message.destroyTime = i + getConnectionsManager().getCurrentTime();
        if (!z) {
            return new Runnable(messageObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda174
                public final /* synthetic */ MessageObject f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$sendSecretMessageRead$104(this.f$1);
                }
            };
        }
        if (this.currentEncryptedChat != null) {
            MessagesController messagesController = getMessagesController();
            long j = this.dialog_id;
            TLRPC$Message tLRPC$Message2 = messageObject.messageOwner;
            messagesController.markMessageAsRead(j, tLRPC$Message2.random_id, tLRPC$Message2.ttl);
        } else {
            getMessagesController().markMessageAsRead2(this.dialog_id, messageObject.getId(), null, messageObject.messageOwner.ttl, 0);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$sendSecretMessageRead$104(MessageObject messageObject) {
        if (this.currentEncryptedChat != null) {
            MessagesController messagesController = getMessagesController();
            long j = this.dialog_id;
            TLRPC$Message tLRPC$Message = messageObject.messageOwner;
            messagesController.markMessageAsRead(j, tLRPC$Message.random_id, tLRPC$Message.ttl);
            return;
        }
        getMessagesController().markMessageAsRead2(this.dialog_id, messageObject.getId(), null, messageObject.messageOwner.ttl, 0);
    }

    private void clearChatData() {
        this.messages.clear();
        this.messagesByDays.clear();
        this.waitingForLoad.clear();
        this.groupedMessagesMap.clear();
        this.threadMessageAdded = false;
        ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
        if (chatActivityAdapter != null) {
            showProgressView(chatActivityAdapter.botInfoRow < 0);
        }
        RecyclerListView recyclerListView = this.chatListView;
        if (recyclerListView != null) {
            recyclerListView.setEmptyView(null);
        }
        for (int i = 0; i < 2; i++) {
            this.messagesDict[i].clear();
            if (this.currentEncryptedChat == null) {
                this.maxMessageId[i] = Integer.MAX_VALUE;
                this.minMessageId[i] = Integer.MIN_VALUE;
            } else {
                this.maxMessageId[i] = Integer.MIN_VALUE;
                this.minMessageId[i] = Integer.MAX_VALUE;
            }
            this.maxDate[i] = Integer.MIN_VALUE;
            this.minDate[i] = 0;
            this.endReached[i] = false;
            this.cacheEndReached[i] = false;
            this.forwardEndReached[i] = true;
        }
        this.first = true;
        this.firstLoading = true;
        this.loading = true;
        this.loadingForward = false;
        this.waitingForReplyMessageLoad = false;
        this.startLoadFromMessageId = 0;
        this.showScrollToMessageError = false;
        this.last_message_id = 0;
        this.unreadMessageObject = null;
        this.createUnreadMessageAfterId = 0;
        this.createUnreadMessageAfterIdLoading = false;
        this.needSelectFromMessageId = false;
        ChatActivityAdapter chatActivityAdapter2 = this.chatAdapter;
        if (chatActivityAdapter2 != null) {
            chatActivityAdapter2.notifyDataSetChanged(false);
        }
    }

    public void scrollToLastMessage(boolean z) {
        if (!this.chatListView.isFastScrollAnimationRunning()) {
            this.forceNextPinnedMessageId = 0;
            this.nextScrollToMessageId = 0;
            this.forceScrollToFirst = false;
            this.chatScrollHelper.setScrollDirection(0);
            if (!this.forwardEndReached[0] || this.first_unread_id != 0 || this.startLoadFromMessageId != 0) {
                AlertDialog alertDialog = this.progressDialog;
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                updatePinnedListButton(false);
                AlertDialog alertDialog2 = new AlertDialog(getParentActivity(), 3, this.themeDelegate);
                this.progressDialog = alertDialog2;
                alertDialog2.setOnCancelListener(this.postponedScrollCancelListener);
                this.progressDialog.showDelayed(1000);
                this.postponedScrollToLastMessageQueryIndex = this.lastLoadIndex;
                this.postponedScrollMessageId = 0;
                this.postponedScrollIsCanceled = false;
                this.waitingForLoad.clear();
                this.waitingForLoad.add(Integer.valueOf(this.lastLoadIndex));
                MessagesController messagesController = getMessagesController();
                long j = this.dialog_id;
                long j2 = this.mergeDialogId;
                int i = this.classGuid;
                int i2 = this.chatMode;
                int i3 = this.threadMessageId;
                int i4 = this.replyMaxReadId;
                int i5 = this.lastLoadIndex;
                this.lastLoadIndex = i5 + 1;
                messagesController.loadMessages(j, j2, false, 30, 0, 0, true, 0, i, 0, 0, i2, i3, i4, i5);
            } else if (this.chatLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                this.canShowPagedownButton = false;
                updatePagedownButtonVisibility(true);
                removeSelectedMessageHighlight();
                updateVisibleRows();
            } else {
                this.chatAdapter.updateRowsSafe();
                this.chatScrollHelperCallback.scrollTo = null;
                int i6 = 0;
                if (z) {
                    while (i6 < this.messages.size() && this.messages.get(i6).isSponsored()) {
                        i6++;
                    }
                }
                this.chatScrollHelper.scrollToPosition(i6, 0, true, true);
            }
        }
    }

    public void updateTextureViewPosition(boolean z) {
        boolean z2;
        MessageObject playingMessageObject;
        if (this.fragmentView != null && !this.paused) {
            int childCount = this.chatListView.getChildCount();
            int i = 0;
            while (true) {
                if (i >= childCount) {
                    z2 = false;
                    break;
                }
                View childAt = this.chatListView.getChildAt(i);
                if (childAt instanceof ChatMessageCell) {
                    ChatMessageCell chatMessageCell = (ChatMessageCell) childAt;
                    MessageObject messageObject = chatMessageCell.getMessageObject();
                    if (this.videoPlayerContainer != null && ((messageObject.isRoundVideo() || messageObject.isVideo()) && MediaController.getInstance().isPlayingMessage(messageObject))) {
                        ImageReceiver photoImage = chatMessageCell.getPhotoImage();
                        this.videoPlayerContainer.setTranslationX(photoImage.getImageX() + chatMessageCell.getX());
                        this.videoPlayerContainer.setTranslationY(((chatMessageCell.getY() + photoImage.getImageY()) + this.chatListView.getY()) - ((float) this.videoPlayerContainer.getTop()));
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.videoPlayerContainer.getLayoutParams();
                        if (messageObject.isRoundVideo()) {
                            this.videoPlayerContainer.setTag(R.id.parent_tag, null);
                            int i2 = layoutParams.width;
                            int i3 = AndroidUtilities.roundPlayingMessageSize;
                            if (!(i2 == i3 && layoutParams.height == i3)) {
                                layoutParams.height = i3;
                                layoutParams.width = i3;
                                this.aspectRatioFrameLayout.setResizeMode(0);
                                this.videoPlayerContainer.setLayoutParams(layoutParams);
                            }
                            float f = ((float) (AndroidUtilities.roundPlayingMessageSize + (AndroidUtilities.roundMessageInset * 2))) / ((float) AndroidUtilities.roundPlayingMessageSize);
                            float imageWidth = chatMessageCell.getPhotoImage().getImageWidth() / ((float) AndroidUtilities.roundPlayingMessageSize);
                            if (this.videoPlayerContainer.getScaleX() != imageWidth) {
                                this.videoPlayerContainer.invalidate();
                                this.fragmentView.invalidate();
                            }
                            this.videoPlayerContainer.setPivotX(0.0f);
                            this.videoPlayerContainer.setPivotY(0.0f);
                            this.videoPlayerContainer.setScaleX(imageWidth);
                            this.videoPlayerContainer.setScaleY(imageWidth);
                            this.videoTextureView.setScaleX(f);
                            this.videoTextureView.setScaleY(f);
                        } else {
                            this.videoPlayerContainer.setTag(R.id.parent_tag, photoImage);
                            if (!(((float) layoutParams.width) == photoImage.getImageWidth() && ((float) layoutParams.height) == photoImage.getImageHeight())) {
                                this.aspectRatioFrameLayout.setResizeMode(3);
                                layoutParams.width = (int) photoImage.getImageWidth();
                                layoutParams.height = (int) photoImage.getImageHeight();
                                this.videoPlayerContainer.setLayoutParams(layoutParams);
                            }
                            this.videoTextureView.setScaleX(1.0f);
                            this.videoTextureView.setScaleY(1.0f);
                        }
                        this.fragmentView.invalidate();
                        this.videoPlayerContainer.invalidate();
                        z2 = true;
                    }
                }
                i++;
            }
            if (z && this.videoPlayerContainer != null && (playingMessageObject = MediaController.getInstance().getPlayingMessageObject()) != null && playingMessageObject.eventId == 0) {
                if (z2) {
                    MediaController.getInstance().setCurrentVideoVisible(true);
                    if (!playingMessageObject.isRoundVideo() && !this.scrollToVideo) {
                        this.chatListView.invalidate();
                    }
                } else if (!this.checkTextureViewPosition || !playingMessageObject.isVideo()) {
                    this.videoPlayerContainer.setTranslationY((float) ((-AndroidUtilities.roundPlayingMessageSize) - 100));
                    this.fragmentView.invalidate();
                    if (!playingMessageObject.isRoundVideo() && !playingMessageObject.isVideo()) {
                        return;
                    }
                    if (this.checkTextureViewPosition || PipRoundVideoView.getInstance() != null) {
                        MediaController.getInstance().setCurrentVideoVisible(false);
                    } else {
                        scrollToMessageId(playingMessageObject.getId(), 0, false, 0, true, 0);
                    }
                } else {
                    MediaController.getInstance().cleanupPlayer(true, true);
                }
            }
        }
    }

    public void invalidateMessagesVisiblePart() {
        this.invalidateMessagesVisiblePart = true;
        View view = this.fragmentView;
        if (view != null) {
            view.invalidate();
        }
    }

    private Integer findClosest(ArrayList<Integer> arrayList, int i, int[] iArr) {
        if (arrayList.isEmpty()) {
            return 0;
        }
        Integer num = arrayList.get(0);
        if (i >= num.intValue()) {
            iArr[0] = 0;
            return num;
        }
        int size = arrayList.size();
        int i2 = size - 1;
        Integer num2 = arrayList.get(i2);
        if (i <= num2.intValue()) {
            iArr[0] = i2;
            return num2;
        }
        int i3 = 0;
        int i4 = 0;
        while (i3 < size) {
            i4 = (i3 + size) / 2;
            Integer num3 = arrayList.get(i4);
            if (num3.intValue() == i) {
                iArr[0] = i4;
                return num3;
            } else if (i < num3.intValue()) {
                if (i4 > 0) {
                    int i5 = i4 - 1;
                    Integer num4 = arrayList.get(i5);
                    if (i > num4.intValue()) {
                        iArr[0] = i5;
                        return num4;
                    }
                }
                i3 = i4 + 1;
            } else if (i4 <= 0 || i >= arrayList.get(i4 - 1).intValue()) {
                size = i4;
            } else {
                iArr[0] = i4;
                return num3;
            }
        }
        iArr[0] = i4;
        return arrayList.get(i4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0384, code lost:
        if (r1.forcePlayEffect != false) goto L_0x0389;
     */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x0543  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0113  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void updateMessagesVisiblePart(boolean r49) {
        /*
        // Method dump skipped, instructions count: 2758
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.updateMessagesVisiblePart(boolean):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateMessagesVisiblePart$105(MessageObject messageObject) {
        if (messageObject.isVideo()) {
            openPhotoViewerForMessage(null, messageObject);
        } else {
            MediaController.getInstance().playMessage(messageObject);
        }
    }

    /* access modifiers changed from: private */
    public void inlineUpdate1() {
        int i = this.prevSetUnreadCount;
        int i2 = this.newUnreadMessageCount;
        if (i != i2) {
            this.prevSetUnreadCount = i2;
            this.pagedownButtonCounter.setCount(i2, this.openAnimationEnded);
        }
    }

    /* access modifiers changed from: private */
    public void inlineUpdate2() {
        int i = this.prevSetUnreadCount;
        int i2 = this.newUnreadMessageCount;
        if (i != i2) {
            this.prevSetUnreadCount = i2;
            this.pagedownButtonCounter.setCount(i2, true);
        }
    }

    /* access modifiers changed from: private */
    public void toggleMute(boolean z) {
        if (getMessagesController().isDialogMuted(this.dialog_id)) {
            getNotificationsController().muteDialog(this.dialog_id, false);
            if (!z) {
                BulletinFactory.createMuteBulletin(this, false, this.themeDelegate).show();
            }
        } else if (z) {
            getNotificationsController().muteDialog(this.dialog_id, true);
        } else {
            BottomSheet createMuteAlert = AlertsCreator.createMuteAlert(this, this.dialog_id, this.themeDelegate);
            createMuteAlert.setCalcMandatoryInsets(isKeyboardVisible());
            showDialog(createMuteAlert);
        }
    }

    private int getScrollOffsetForMessage(MessageObject messageObject) {
        return getScrollOffsetForMessage(getHeightForMessage(messageObject));
    }

    private int getScrollOffsetForMessage(int i) {
        return (int) Math.max((float) (-AndroidUtilities.dp(2.0f)), ((((float) (this.chatListView.getMeasuredHeight() - this.blurredViewBottomOffset)) - this.chatListViewPaddingTop) - ((float) i)) / 2.0f);
    }

    private int getHeightForMessage(MessageObject messageObject) {
        boolean z = true;
        if (this.dummyMessageCell == null) {
            this.dummyMessageCell = new ChatMessageCell(getParentActivity(), true, this.themeDelegate);
        }
        this.dummyMessageCell.isChat = this.currentChat != null || UserObject.isUserSelf(this.currentUser);
        ChatMessageCell chatMessageCell = this.dummyMessageCell;
        TLRPC$User tLRPC$User = this.currentUser;
        chatMessageCell.isBot = tLRPC$User != null && tLRPC$User.bot;
        if (!ChatObject.isChannel(this.currentChat) || !this.currentChat.megagroup) {
            z = false;
        }
        chatMessageCell.isMegagroup = z;
        return this.dummyMessageCell.computeHeight(messageObject, this.groupedMessagesMap.get(messageObject.getGroupId()));
    }

    /* access modifiers changed from: private */
    public void startMessageUnselect() {
        Runnable runnable = this.unselectRunnable;
        if (runnable != null) {
            AndroidUtilities.cancelRunOnUIThread(runnable);
        }
        ChatActivity$$ExternalSyntheticLambda136 chatActivity$$ExternalSyntheticLambda136 = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda136
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$startMessageUnselect$106();
            }
        };
        this.unselectRunnable = chatActivity$$ExternalSyntheticLambda136;
        AndroidUtilities.runOnUIThread(chatActivity$$ExternalSyntheticLambda136, 1000);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$startMessageUnselect$106() {
        this.highlightMessageId = ConnectionsManager.DEFAULT_DATACENTER_ID;
        updateVisibleRows();
        this.unselectRunnable = null;
    }

    /* access modifiers changed from: private */
    public void removeSelectedMessageHighlight() {
        Runnable runnable = this.unselectRunnable;
        if (runnable != null) {
            AndroidUtilities.cancelRunOnUIThread(runnable);
            this.unselectRunnable = null;
        }
        this.highlightMessageId = ConnectionsManager.DEFAULT_DATACENTER_ID;
    }

    /* JADX WARNING: Removed duplicated region for block: B:122:0x028d  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0158 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0196 A[LOOP:1: B:66:0x0116->B:90:0x0196, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void scrollToMessageId(int r25, int r26, boolean r27, int r28, boolean r29, int r30) {
        /*
        // Method dump skipped, instructions count: 717
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.scrollToMessageId(int, int, boolean, int, boolean, int):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$scrollToMessageId$107() {
        int i = this.nextScrollToMessageId;
        if (i != 0) {
            scrollToMessageId(i, this.nextScrollFromMessageId, this.nextScrollSelect, this.nextScrollLoadIndex, this.nextScrollForce, this.nextScrollForcePinnedMessageId);
            this.nextScrollToMessageId = 0;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$scrollToMessageId$108(DialogInterface dialogInterface) {
        showPinnedProgress(false);
    }

    private void showPinnedProgress(boolean z) {
        if (!z) {
            Runnable runnable = this.updatePinnedProgressRunnable;
            if (runnable != null) {
                AndroidUtilities.cancelRunOnUIThread(runnable);
            }
            this.updatePinnedProgressRunnable = null;
            this.pinnedProgressIsShowing = false;
            updatePinnedListButton(true);
        } else if (this.updatePinnedProgressRunnable == null) {
            ChatActivity$$ExternalSyntheticLambda152 chatActivity$$ExternalSyntheticLambda152 = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda152
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$showPinnedProgress$109();
                }
            };
            this.updatePinnedProgressRunnable = chatActivity$$ExternalSyntheticLambda152;
            AndroidUtilities.runOnUIThread(chatActivity$$ExternalSyntheticLambda152, 100);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showPinnedProgress$109() {
        this.pinnedProgressIsShowing = true;
        updatePinnedListButton(true);
    }

    /* access modifiers changed from: private */
    public void updatePagedownButtonVisibility(boolean z) {
        if (this.pagedownButton != null) {
            if (this.canShowPagedownButton && !this.textSelectionHelper.isSelectionMode() && !this.chatActivityEnterView.isRecordingAudioVideo()) {
                if (z && (this.openAnimationStartTime == 0 || SystemClock.elapsedRealtime() < this.openAnimationStartTime + 150)) {
                    z = false;
                }
                this.pagedownButtonShowedByScroll = false;
                if (this.pagedownButton.getTag() == null) {
                    ValueAnimator valueAnimator = this.pagedownButtonAnimation;
                    if (valueAnimator != null) {
                        valueAnimator.removeAllListeners();
                        this.pagedownButtonAnimation.cancel();
                        this.pagedownButtonAnimation = null;
                    }
                    this.pagedownButton.setTag(1);
                    if (z) {
                        this.pagedownButton.setVisibility(0);
                        ValueAnimator ofFloat = ValueAnimator.ofFloat(this.pagedownButtonEnterProgress, 1.0f);
                        this.pagedownButtonAnimation = ofFloat;
                        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda9
                            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                                ChatActivity.this.lambda$updatePagedownButtonVisibility$110(valueAnimator2);
                            }
                        });
                        this.pagedownButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.73
                            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                            public void onAnimationEnd(Animator animator) {
                                ChatActivity.this.pagedownButtonEnterProgress = 1.0f;
                                ChatActivity.this.contentView.invalidate();
                            }
                        });
                        this.pagedownButtonAnimation.setDuration(200L);
                        this.pagedownButtonAnimation.start();
                        return;
                    }
                    this.pagedownButtonEnterProgress = 1.0f;
                    this.contentView.invalidate();
                    this.pagedownButton.setVisibility(0);
                    return;
                }
                return;
            }
            this.returnToMessageId = 0;
            this.newUnreadMessageCount = 0;
            if (this.pagedownButton.getTag() != null) {
                this.pagedownButton.setTag(null);
                ValueAnimator valueAnimator2 = this.pagedownButtonAnimation;
                if (valueAnimator2 != null) {
                    valueAnimator2.removeAllListeners();
                    this.pagedownButtonAnimation.cancel();
                    this.pagedownButtonAnimation = null;
                }
                if (z) {
                    this.pagedownButton.setVisibility(0);
                    ValueAnimator ofFloat2 = ValueAnimator.ofFloat(this.pagedownButtonEnterProgress, 0.0f);
                    this.pagedownButtonAnimation = ofFloat2;
                    ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda12
                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator3) {
                            ChatActivity.this.lambda$updatePagedownButtonVisibility$111(valueAnimator3);
                        }
                    });
                    this.pagedownButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.74
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            ChatActivity.this.pagedownButtonEnterProgress = 0.0f;
                            ChatActivity.this.pagedownButton.setVisibility(4);
                            ChatActivity.this.contentView.invalidate();
                        }
                    });
                    this.pagedownButtonAnimation.setDuration(200L);
                    this.pagedownButtonAnimation.start();
                    return;
                }
                this.pagedownButtonEnterProgress = 0.0f;
                this.pagedownButton.setVisibility(4);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updatePagedownButtonVisibility$110(ValueAnimator valueAnimator) {
        this.pagedownButtonEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.contentView.invalidate();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updatePagedownButtonVisibility$111(ValueAnimator valueAnimator) {
        this.pagedownButtonEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.contentView.invalidate();
    }

    /* access modifiers changed from: private */
    public void showMentionDownButton(boolean z, boolean z2) {
        FrameLayout frameLayout = this.mentiondownButton;
        if (frameLayout != null) {
            if (!z) {
                this.returnToMessageId = 0;
                if (frameLayout.getTag() != null) {
                    this.mentiondownButton.setTag(null);
                    ValueAnimator valueAnimator = this.mentiondownButtonAnimation;
                    if (valueAnimator != null) {
                        valueAnimator.removeAllListeners();
                        this.mentiondownButtonAnimation.cancel();
                        this.mentiondownButtonAnimation = null;
                    }
                    if (z2) {
                        ValueAnimator ofFloat = ValueAnimator.ofFloat(this.mentionsButtonEnterProgress, 0.0f);
                        this.mentiondownButtonAnimation = ofFloat;
                        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda1
                            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                                ChatActivity.this.lambda$showMentionDownButton$113(valueAnimator2);
                            }
                        });
                        this.mentiondownButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.76
                            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                            public void onAnimationEnd(Animator animator) {
                                ChatActivity.this.mentionsButtonEnterProgress = 0.0f;
                                ChatActivity.this.mentiondownButton.setVisibility(4);
                                ChatActivity.this.contentView.invalidate();
                            }
                        });
                        this.mentiondownButtonAnimation.setDuration(200L);
                        this.mentiondownButtonAnimation.start();
                        return;
                    }
                    this.mentionsButtonEnterProgress = 0.0f;
                    this.mentiondownButton.setVisibility(4);
                }
            } else if (frameLayout.getTag() == null) {
                ValueAnimator valueAnimator2 = this.mentiondownButtonAnimation;
                if (valueAnimator2 != null) {
                    valueAnimator2.removeAllListeners();
                    this.mentiondownButtonAnimation.cancel();
                    this.mentiondownButtonAnimation = null;
                }
                if (z2) {
                    this.mentiondownButton.setVisibility(0);
                    this.mentiondownButton.setTag(1);
                    ValueAnimator ofFloat2 = ValueAnimator.ofFloat(this.mentionsButtonEnterProgress, 1.0f);
                    this.mentiondownButtonAnimation = ofFloat2;
                    ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda7
                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator3) {
                            ChatActivity.this.lambda$showMentionDownButton$112(valueAnimator3);
                        }
                    });
                    this.mentiondownButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.75
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            ChatActivity.this.mentionsButtonEnterProgress = 1.0f;
                            ChatActivity.this.contentView.invalidate();
                        }
                    });
                    this.mentiondownButtonAnimation.setDuration(200L);
                    this.mentiondownButtonAnimation.start();
                    return;
                }
                this.mentionsButtonEnterProgress = 1.0f;
                this.contentView.invalidate();
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showMentionDownButton$112(ValueAnimator valueAnimator) {
        this.mentionsButtonEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.contentView.invalidate();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showMentionDownButton$113(ValueAnimator valueAnimator) {
        this.mentionsButtonEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.contentView.invalidate();
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updateSecretStatus() {
        /*
        // Method dump skipped, instructions count: 392
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.updateSecretStatus():void");
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void onRequestPermissionsResultFragment(int i, String[] strArr, int[] iArr) {
        boolean z;
        ChatAttachAlert chatAttachAlert;
        ChatAttachAlert chatAttachAlert2;
        ChatAttachAlert chatAttachAlert3;
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.onRequestPermissionsResultFragment(i, strArr, iArr);
        }
        MentionsContainerView mentionsContainerView = this.mentionContainer;
        if (!(mentionsContainerView == null || mentionsContainerView.getAdapter() == null)) {
            this.mentionContainer.getAdapter().onRequestPermissionsResultFragment(i, strArr, iArr);
        }
        if (i == 4 && (chatAttachAlert3 = this.chatAttachAlert) != null) {
            chatAttachAlert3.getPhotoLayout().checkStorage();
        } else if ((i == 5 || i == 30) && (chatAttachAlert2 = this.chatAttachAlert) != null) {
            chatAttachAlert2.onRequestPermissionsResultFragment(i, strArr, iArr);
        } else {
            boolean z2 = true;
            if ((i == 17 || i == 18) && (chatAttachAlert = this.chatAttachAlert) != null) {
                ChatAttachAlertPhotoLayout photoLayout = chatAttachAlert.getPhotoLayout();
                if (iArr.length <= 0 || iArr[0] != 0) {
                    z2 = false;
                }
                photoLayout.checkCamera(z2);
                this.chatAttachAlert.getPhotoLayout().checkStorage();
            } else if (i == 21) {
                if (getParentActivity() != null && iArr != null && iArr.length != 0 && iArr[0] != 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
                    builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
                    builder.setMessage(LocaleController.getString("PermissionNoAudioVideoWithHint", R.string.PermissionNoAudioVideoWithHint));
                    builder.setNegativeButton(LocaleController.getString("PermissionOpenSettings", R.string.PermissionOpenSettings), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda33
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            ChatActivity.this.lambda$onRequestPermissionsResultFragment$114(dialogInterface, i2);
                        }
                    });
                    builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), null);
                    builder.show();
                }
            } else if (i == 19 && iArr != null && iArr.length > 0 && iArr[0] == 0) {
                processSelectedAttach(0);
            } else if (i == 20 && iArr != null && iArr.length > 0 && iArr[0] == 0) {
                processSelectedAttach(2);
            } else if (((i == 101 || i == 102) && this.currentUser != null) || (i == 103 && this.currentChat != null)) {
                int i2 = 0;
                while (true) {
                    if (i2 >= iArr.length) {
                        z = true;
                        break;
                    } else if (iArr[i2] != 0) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (iArr.length <= 0 || !z) {
                    VoIPHelper.permissionDenied(getParentActivity(), null, i);
                } else if (i == 103) {
                    VoIPHelper.startCall(this.currentChat, null, null, this.createGroupCall, getParentActivity(), this, getAccountInstance());
                } else {
                    TLRPC$User tLRPC$User = this.currentUser;
                    boolean z3 = i == 102;
                    TLRPC$UserFull tLRPC$UserFull = this.userInfo;
                    VoIPHelper.startCall(tLRPC$User, z3, tLRPC$UserFull != null && tLRPC$UserFull.video_calls_available, getParentActivity(), getMessagesController().getUserFull(this.currentUser.id), getAccountInstance());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onRequestPermissionsResultFragment$114(DialogInterface dialogInterface, int i) {
        try {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.parse("package:" + ApplicationLoader.applicationContext.getPackageName()));
            getParentActivity().startActivity(intent);
        } catch (Exception e) {
            FileLog.e(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x009d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void checkActionBarMenu(boolean r5) {
        /*
            r4 = this;
            org.telegram.tgnet.TLRPC$EncryptedChat r0 = r4.currentEncryptedChat
            r1 = 8
            r2 = 0
            if (r0 == 0) goto L_0x000b
            boolean r0 = r0 instanceof org.telegram.tgnet.TLRPC$TL_encryptedChat
            if (r0 == 0) goto L_0x0035
        L_0x000b:
            org.telegram.tgnet.TLRPC$Chat r0 = r4.currentChat
            if (r0 == 0) goto L_0x001f
            int r0 = r4.chatMode
            if (r0 != 0) goto L_0x0035
            int r0 = r4.threadMessageId
            if (r0 != 0) goto L_0x0035
            org.telegram.tgnet.TLRPC$ChatFull r0 = r4.chatInfo
            if (r0 == 0) goto L_0x0035
            int r0 = r0.ttl_period
            if (r0 == 0) goto L_0x0035
        L_0x001f:
            org.telegram.tgnet.TLRPC$User r0 = r4.currentUser
            if (r0 == 0) goto L_0x0044
            boolean r0 = org.telegram.messenger.UserObject.isDeleted(r0)
            if (r0 != 0) goto L_0x0035
            org.telegram.tgnet.TLRPC$EncryptedChat r0 = r4.currentEncryptedChat
            if (r0 != 0) goto L_0x0044
            org.telegram.tgnet.TLRPC$UserFull r0 = r4.userInfo
            if (r0 == 0) goto L_0x0035
            int r0 = r0.ttl_period
            if (r0 != 0) goto L_0x0044
        L_0x0035:
            android.view.View r0 = r4.timeItem2
            if (r0 == 0) goto L_0x003c
            r0.setVisibility(r1)
        L_0x003c:
            org.telegram.ui.Components.ChatAvatarContainer r0 = r4.avatarContainer
            if (r0 == 0) goto L_0x0052
            r0.hideTimeItem(r5)
            goto L_0x0052
        L_0x0044:
            android.view.View r0 = r4.timeItem2
            if (r0 == 0) goto L_0x004b
            r0.setVisibility(r2)
        L_0x004b:
            org.telegram.ui.Components.ChatAvatarContainer r0 = r4.avatarContainer
            if (r0 == 0) goto L_0x0052
            r0.showTimeItem(r5)
        L_0x0052:
            org.telegram.ui.Components.ChatAvatarContainer r0 = r4.avatarContainer
            if (r0 == 0) goto L_0x0073
            org.telegram.tgnet.TLRPC$EncryptedChat r3 = r4.currentEncryptedChat
            if (r3 == 0) goto L_0x0060
            int r3 = r3.ttl
            r0.setTime(r3, r5)
            goto L_0x0073
        L_0x0060:
            org.telegram.tgnet.TLRPC$UserFull r3 = r4.userInfo
            if (r3 == 0) goto L_0x006a
            int r3 = r3.ttl_period
            r0.setTime(r3, r5)
            goto L_0x0073
        L_0x006a:
            org.telegram.tgnet.TLRPC$ChatFull r3 = r4.chatInfo
            if (r3 == 0) goto L_0x0073
            int r3 = r3.ttl_period
            r0.setTime(r3, r5)
        L_0x0073:
            org.telegram.ui.ActionBar.ActionBarMenuSubItem r5 = r4.clearHistoryItem
            if (r5 == 0) goto L_0x00a1
            org.telegram.tgnet.TLRPC$ChatFull r5 = r4.chatInfo
            if (r5 == 0) goto L_0x00a1
            boolean r5 = r5.can_delete_channel
            if (r5 != 0) goto L_0x0098
            org.telegram.tgnet.TLRPC$Chat r5 = r4.currentChat
            boolean r5 = org.telegram.messenger.ChatObject.isChannel(r5)
            if (r5 == 0) goto L_0x0098
            org.telegram.tgnet.TLRPC$Chat r5 = r4.currentChat
            boolean r0 = r5.megagroup
            if (r0 == 0) goto L_0x0096
            java.lang.String r5 = r5.username
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 == 0) goto L_0x0096
            goto L_0x0098
        L_0x0096:
            r5 = 0
            goto L_0x0099
        L_0x0098:
            r5 = 1
        L_0x0099:
            org.telegram.ui.ActionBar.ActionBarMenuSubItem r0 = r4.clearHistoryItem
            if (r5 == 0) goto L_0x009e
            r1 = 0
        L_0x009e:
            r0.setVisibility(r1)
        L_0x00a1:
            r4.checkAndUpdateAvatar()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.checkActionBarMenu(boolean):void");
    }

    /* access modifiers changed from: private */
    public int getMessageType(MessageObject messageObject) {
        String str;
        String str2;
        if (messageObject == null) {
            return -1;
        }
        boolean z = false;
        boolean z2 = true;
        if (this.currentEncryptedChat == null) {
            if (messageObject.isEditing()) {
                return -1;
            }
            if (messageObject.getId() > 0 || !messageObject.isOut()) {
                if (messageObject.isAnimatedEmoji()) {
                    return 2;
                }
                int i = messageObject.type;
                if (i == 6) {
                    return -1;
                }
                if (i == 10 || i == 11) {
                    return messageObject.getId() == 0 ? -1 : 1;
                }
                if (messageObject.isVoice()) {
                    return 2;
                }
                if (messageObject.isSticker() || messageObject.isAnimatedSticker()) {
                    TLRPC$InputStickerSet inputStickerSet = messageObject.getInputStickerSet();
                    if (inputStickerSet instanceof TLRPC$TL_inputStickerSetID) {
                        if (!getMediaDataController().isStickerPackInstalled(inputStickerSet.id)) {
                            return 7;
                        }
                        return 9;
                    } else if (!(inputStickerSet instanceof TLRPC$TL_inputStickerSetShortName) || getMediaDataController().isStickerPackInstalled(inputStickerSet.short_name)) {
                        return 9;
                    } else {
                        return 7;
                    }
                } else {
                    if (!messageObject.isRoundVideo() && ((messageObject.messageOwner.media instanceof TLRPC$TL_messageMediaPhoto) || messageObject.getDocument() != null || messageObject.isMusic() || messageObject.isVideo())) {
                        if (!TextUtils.isEmpty(messageObject.messageOwner.attachPath) && new File(messageObject.messageOwner.attachPath).exists()) {
                            z = true;
                        }
                        if (z || !messageObject.mediaExists) {
                            z2 = z;
                        }
                        if (z2) {
                            if (!(messageObject.getDocument() == null || messageObject.isMusic() || (str2 = messageObject.getDocument().mime_type) == null)) {
                                if (messageObject.getDocumentName().toLowerCase().endsWith("attheme")) {
                                    return 10;
                                }
                                if (str2.endsWith("/xml")) {
                                    return 5;
                                }
                                if ((!messageObject.isNewGif() && str2.endsWith("/mp4")) || str2.endsWith("/png") || str2.endsWith("/jpg") || str2.endsWith("/jpeg")) {
                                    return 6;
                                }
                            }
                            return 4;
                        }
                    } else if (messageObject.type == 12) {
                        return 8;
                    } else {
                        if (messageObject.isMediaEmpty()) {
                            return 3;
                        }
                    }
                    return 2;
                }
            } else if (messageObject.isSendError()) {
                return !messageObject.isMediaEmpty() ? 0 : 20;
            } else {
                return -1;
            }
        } else if (messageObject.isSending()) {
            return -1;
        } else {
            if (messageObject.isAnimatedEmoji()) {
                return 2;
            }
            if (messageObject.type == 6) {
                return -1;
            }
            if (messageObject.isSendError()) {
                return !messageObject.isMediaEmpty() ? 0 : 20;
            }
            int i2 = messageObject.type;
            if (i2 == 10 || i2 == 11) {
                return (messageObject.getId() == 0 || messageObject.isSending()) ? -1 : 1;
            }
            if (messageObject.isVoice()) {
                return 2;
            }
            if (!messageObject.isAnimatedEmoji() && (messageObject.isSticker() || messageObject.isAnimatedSticker())) {
                TLRPC$InputStickerSet inputStickerSet2 = messageObject.getInputStickerSet();
                if ((inputStickerSet2 instanceof TLRPC$TL_inputStickerSetShortName) && !getMediaDataController().isStickerPackInstalled(inputStickerSet2.short_name)) {
                    return 7;
                }
            } else if (!messageObject.isRoundVideo() && ((messageObject.messageOwner.media instanceof TLRPC$TL_messageMediaPhoto) || messageObject.getDocument() != null || messageObject.isMusic() || messageObject.isVideo())) {
                if (!TextUtils.isEmpty(messageObject.messageOwner.attachPath) && new File(messageObject.messageOwner.attachPath).exists()) {
                    z = true;
                }
                if (z || !FileLoader.getInstance(this.currentAccount).getPathToMessage(messageObject.messageOwner).exists()) {
                    z2 = z;
                }
                if (z2) {
                    if (messageObject.getDocument() != null && (str = messageObject.getDocument().mime_type) != null && str.endsWith("text/xml")) {
                        return 5;
                    }
                    if (messageObject.messageOwner.ttl <= 0) {
                        return 4;
                    }
                }
            } else if (messageObject.type == 12) {
                return 8;
            } else {
                if (messageObject.isMediaEmpty()) {
                    return 3;
                }
            }
            return 2;
        }
    }

    /* access modifiers changed from: private */
    public void addToSelectedMessages(MessageObject messageObject, boolean z) {
        addToSelectedMessages(messageObject, z, true);
    }

    private void addToSelectedMessages(MessageObject messageObject, boolean z, boolean z2) {
        int i;
        final int i2;
        TLRPC$Chat tLRPC$Chat;
        String str;
        int i3;
        TLRPC$Message tLRPC$Message;
        TLRPC$Message tLRPC$Message2;
        int i4 = this.cantForwardMessagesCount;
        if (messageObject != null) {
            ArrayList<MessageObject> arrayList = this.threadMessageObjects;
            if (arrayList == null || !arrayList.contains(messageObject)) {
                char c = messageObject.getDialogId() == this.dialog_id ? (char) 0 : 1;
                if (z && messageObject.getGroupId() != 0) {
                    MessageObject.GroupedMessages groupedMessages = this.groupedMessagesMap.get(messageObject.getGroupId());
                    if (groupedMessages != null) {
                        boolean z3 = false;
                        int i5 = 0;
                        for (int i6 = 0; i6 < groupedMessages.messages.size(); i6++) {
                            if (this.selectedMessagesIds[c].indexOfKey(groupedMessages.messages.get(i6).getId()) < 0) {
                                i5 = i6;
                                z3 = true;
                            }
                        }
                        int i7 = 0;
                        while (i7 < groupedMessages.messages.size()) {
                            MessageObject messageObject2 = groupedMessages.messages.get(i7);
                            if (!z3) {
                                addToSelectedMessages(messageObject2, false, i7 == groupedMessages.messages.size() - 1);
                            } else if (this.selectedMessagesIds[c].indexOfKey(messageObject2.getId()) < 0) {
                                addToSelectedMessages(messageObject2, false, i7 == i5);
                            }
                            if (!TextUtils.isEmpty(messageObject2.caption)) {
                                showTextSelectionHint(messageObject);
                            }
                            i7++;
                        }
                        return;
                    }
                    return;
                } else if (this.selectedMessagesIds[c].indexOfKey(messageObject.getId()) >= 0) {
                    this.selectedMessagesIds[c].remove(messageObject.getId());
                    if (this.reportType < 0) {
                        if ((messageObject.type == 0 || messageObject.isAnimatedEmoji() || messageObject.caption != null) && ((tLRPC$Message2 = messageObject.messageOwner) == null || !tLRPC$Message2.noforwards)) {
                            this.selectedMessagesCanCopyIds[c].remove(messageObject.getId());
                        }
                        if (!messageObject.isAnimatedEmoji() && ((messageObject.isSticker() || messageObject.isAnimatedSticker()) && MessageObject.isStickerHasSet(messageObject.getDocument()))) {
                            this.selectedMessagesCanStarIds[c].remove(messageObject.getId());
                        }
                        if (messageObject.canEditMessage(this.currentChat)) {
                            this.canEditMessagesCount--;
                        }
                        if (!messageObject.canDeleteMessage(this.chatMode == 1, this.currentChat)) {
                            this.cantDeleteMessagesCount--;
                        }
                        boolean isChatNoForwards = getMessagesController().isChatNoForwards(this.currentChat);
                        if (this.chatMode == 1 || !messageObject.canForwardMessage() || isChatNoForwards) {
                            this.cantForwardMessagesCount--;
                        } else {
                            this.canForwardMessagesCount--;
                        }
                        if (messageObject.isMusic() && !isChatNoForwards) {
                            this.canSaveMusicCount--;
                        } else if (!messageObject.isDocument() || isChatNoForwards) {
                            this.cantSaveMessagesCount--;
                        } else {
                            this.canSaveDocumentsCount--;
                        }
                    }
                } else if (this.selectedMessagesIds[0].size() + this.selectedMessagesIds[1].size() >= 100) {
                    AndroidUtilities.shakeView(this.selectedMessagesCountTextView, 2.0f, 0);
                    Vibrator vibrator = (Vibrator) ApplicationLoader.applicationContext.getSystemService("vibrator");
                    if (vibrator != null) {
                        vibrator.vibrate(200);
                        return;
                    }
                    return;
                } else {
                    this.selectedMessagesIds[c].put(messageObject.getId(), messageObject);
                    if (this.reportType < 0) {
                        if ((messageObject.type == 0 || messageObject.isAnimatedEmoji() || messageObject.caption != null) && ((tLRPC$Message = messageObject.messageOwner) == null || !tLRPC$Message.noforwards)) {
                            this.selectedMessagesCanCopyIds[c].put(messageObject.getId(), messageObject);
                        }
                        if (!messageObject.isAnimatedEmoji() && ((messageObject.isSticker() || messageObject.isAnimatedSticker()) && MessageObject.isStickerHasSet(messageObject.getDocument()))) {
                            this.selectedMessagesCanStarIds[c].put(messageObject.getId(), messageObject);
                        }
                        if (messageObject.canEditMessage(this.currentChat)) {
                            this.canEditMessagesCount++;
                        }
                        if (!messageObject.canDeleteMessage(this.chatMode == 1, this.currentChat)) {
                            this.cantDeleteMessagesCount++;
                        }
                        boolean isChatNoForwards2 = getMessagesController().isChatNoForwards(this.currentChat);
                        if (this.chatMode == 1 || !messageObject.canForwardMessage() || isChatNoForwards2) {
                            this.cantForwardMessagesCount++;
                        } else {
                            this.canForwardMessagesCount++;
                        }
                        if (messageObject.isMusic() && !isChatNoForwards2) {
                            this.canSaveMusicCount++;
                        } else if (!messageObject.isDocument() || isChatNoForwards2) {
                            this.cantSaveMessagesCount++;
                        } else {
                            this.canSaveDocumentsCount++;
                        }
                        if (z) {
                            showTextSelectionHint(messageObject);
                        }
                    }
                }
            } else {
                return;
            }
        }
        AnimatorSet animatorSet = this.forwardButtonAnimation;
        if (animatorSet != null) {
            animatorSet.cancel();
            this.forwardButtonAnimation = null;
        }
        if (z2 && this.actionBar.isActionModeShowed() && this.reportType < 0) {
            int size = this.selectedMessagesIds[0].size() + this.selectedMessagesIds[1].size();
            if (size == 0) {
                hideActionMode();
                updatePinnedMessageView(true);
                return;
            }
            ActionBarMenuItem item = this.actionBar.createActionMode().getItem(25);
            ActionBarMenuItem item2 = this.actionBar.createActionMode().getItem(10);
            ActionBarMenuItem item3 = this.actionBar.createActionMode().getItem(22);
            final ActionBarMenuItem item4 = this.actionBar.createActionMode().getItem(23);
            ActionBarMenuItem item5 = this.actionBar.createActionMode().getItem(11);
            boolean z4 = getMessagesController().isChatNoForwards(this.currentChat) || hasSelectedNoforwardsMessage();
            float f = 0.5f;
            if ((i4 != 0 || this.cantForwardMessagesCount == 0) && (i4 == 0 || this.cantForwardMessagesCount != 0)) {
                if (item5 != null) {
                    item5.setEnabled(this.cantForwardMessagesCount == 0 || z4);
                    item5.setAlpha(this.cantForwardMessagesCount == 0 ? 1.0f : 0.5f);
                    if (z4) {
                        if (item5.getBackground() != null) {
                            this.forwardButton.setBackground(null);
                        }
                    } else if (item5.getBackground() == null) {
                        item5.setBackground(Theme.createSelectorDrawable(getThemedColor("actionBarActionModeDefaultSelector"), 3));
                    }
                }
                TextView textView = this.forwardButton;
                if (textView != null) {
                    textView.setEnabled(this.cantForwardMessagesCount == 0 || z4);
                    if (z4) {
                        if (this.forwardButton.getBackground() != null) {
                            this.forwardButton.setBackground(null);
                        }
                    } else if (this.forwardButton.getBackground() == null) {
                        this.forwardButton.setBackground(Theme.createSelectorDrawable(getThemedColor("actionBarActionModeDefaultSelector"), 3));
                    }
                    this.forwardButton.setAlpha(this.cantForwardMessagesCount == 0 ? 1.0f : 0.5f);
                }
            } else {
                this.forwardButtonAnimation = new AnimatorSet();
                ArrayList arrayList2 = new ArrayList();
                if (item5 != null) {
                    item5.setEnabled(this.cantForwardMessagesCount == 0 || z4);
                    Property property = View.ALPHA;
                    float[] fArr = new float[1];
                    fArr[0] = this.cantForwardMessagesCount == 0 ? 1.0f : 0.5f;
                    arrayList2.add(ObjectAnimator.ofFloat(item5, property, fArr));
                    if (z4 && item5.getBackground() != null) {
                        item5.setBackground(null);
                    } else if (item5.getBackground() == null) {
                        item5.setBackground(Theme.createSelectorDrawable(getThemedColor("actionBarActionModeDefaultSelector"), 5));
                    }
                }
                TextView textView2 = this.forwardButton;
                if (textView2 != null) {
                    textView2.setEnabled(this.cantForwardMessagesCount == 0 || z4);
                    if (z4 && this.forwardButton.getBackground() != null) {
                        this.forwardButton.setBackground(null);
                    } else if (this.forwardButton.getBackground() == null) {
                        this.forwardButton.setBackground(Theme.createSelectorDrawable(getThemedColor("actionBarActionModeDefaultSelector"), 3));
                    }
                    TextView textView3 = this.forwardButton;
                    Property property2 = View.ALPHA;
                    float[] fArr2 = new float[1];
                    if (this.cantForwardMessagesCount == 0) {
                        f = 1.0f;
                    }
                    fArr2[0] = f;
                    arrayList2.add(ObjectAnimator.ofFloat(textView3, property2, fArr2));
                }
                this.forwardButtonAnimation.playTogether(arrayList2);
                this.forwardButtonAnimation.setDuration(100L);
                this.forwardButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.77
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        ChatActivity.this.forwardButtonAnimation = null;
                    }
                });
                this.forwardButtonAnimation.start();
            }
            if (item != null) {
                int i8 = this.canSaveMusicCount;
                item.setVisibility((((i8 <= 0 || this.canSaveDocumentsCount != 0) && (i8 != 0 || this.canSaveDocumentsCount <= 0)) || this.cantSaveMessagesCount != 0) ? 8 : 0);
                if (this.canSaveMusicCount > 0) {
                    i3 = R.string.SaveToMusic;
                    str = "SaveToMusic";
                } else {
                    i3 = R.string.SaveToDownloads;
                    str = "SaveToDownloads";
                }
                item.setContentDescription(LocaleController.getString(str, i3));
            }
            int visibility = item2.getVisibility();
            int visibility2 = item3.getVisibility();
            item2.setVisibility((z4 || this.selectedMessagesCanCopyIds[0].size() + this.selectedMessagesCanCopyIds[1].size() == 0) ? 8 : 0);
            item3.setVisibility((!getMediaDataController().canAddStickerToFavorites() || this.selectedMessagesCanStarIds[0].size() + this.selectedMessagesCanStarIds[1].size() != size) ? 8 : 0);
            int visibility3 = item2.getVisibility();
            int visibility4 = item3.getVisibility();
            this.actionBar.createActionMode().getItem(12).setVisibility(this.cantDeleteMessagesCount == 0 ? 0 : 8);
            this.hasUnfavedSelected = false;
            int i9 = 0;
            while (true) {
                if (i9 >= 2) {
                    break;
                }
                int i10 = 0;
                while (true) {
                    if (i10 >= this.selectedMessagesCanStarIds[i9].size()) {
                        break;
                    } else if (!getMediaDataController().isStickerInFavorites(this.selectedMessagesCanStarIds[i9].valueAt(i10).getDocument())) {
                        this.hasUnfavedSelected = true;
                        break;
                    } else {
                        i10++;
                    }
                }
                if (this.hasUnfavedSelected) {
                    break;
                }
                i9++;
            }
            item3.setIcon(this.hasUnfavedSelected ? R.drawable.msg_fave : R.drawable.msg_unfave);
            final int i11 = (this.canEditMessagesCount == 1 && size == 1) ? 0 : 8;
            if (this.replyButton != null) {
                BlurredFrameLayout blurredFrameLayout = this.bottomOverlayChat;
                boolean z5 = (blurredFrameLayout == null || blurredFrameLayout.getVisibility() != 0) && ((tLRPC$Chat = this.currentChat) == null || ((!ChatObject.isNotInChat(tLRPC$Chat) || isThreadChat()) && ((!ChatObject.isChannel(this.currentChat) || ChatObject.canPost(this.currentChat) || this.currentChat.megagroup) && ChatObject.canSendMessages(this.currentChat))));
                if (this.chatMode == 1 || !z5 || !(this.selectedMessagesIds[0].size() == 0 || this.selectedMessagesIds[1].size() == 0)) {
                    i2 = 8;
                } else if (size == 1) {
                    i2 = 0;
                } else {
                    int i12 = 0;
                    i2 = 0;
                    long j = 0;
                    for (i = 2; i12 < i; i = 2) {
                        int size2 = this.selectedMessagesIds[i12].size();
                        int i13 = 0;
                        while (i13 < size2) {
                            long groupId = this.selectedMessagesIds[i12].valueAt(i13).getGroupId();
                            if (groupId == 0 || !(j == 0 || j == groupId)) {
                                i2 = 8;
                                break;
                            } else {
                                i13++;
                                j = groupId;
                            }
                        }
                        if (i2 == 8) {
                            break;
                        }
                        i12++;
                    }
                }
                if (this.threadMessageObjects != null && i2 == 0) {
                    int size3 = this.selectedMessagesIds[0].size();
                    for (int i14 = 0; i14 < size3; i14++) {
                        if (this.threadMessageObjects.contains(this.selectedMessagesIds[0].valueAt(i14))) {
                            i2 = 8;
                        }
                    }
                }
                if (this.replyButton.getVisibility() != i2) {
                    AnimatorSet animatorSet2 = this.replyButtonAnimation;
                    if (animatorSet2 != null) {
                        animatorSet2.cancel();
                    }
                    AnimatorSet animatorSet3 = new AnimatorSet();
                    this.replyButtonAnimation = animatorSet3;
                    if (i2 == 0) {
                        this.replyButton.setVisibility(i2);
                        this.replyButtonAnimation.playTogether(ObjectAnimator.ofFloat(this.replyButton, View.ALPHA, 1.0f), ObjectAnimator.ofFloat(this.replyButton, View.SCALE_Y, 1.0f));
                    } else {
                        animatorSet3.playTogether(ObjectAnimator.ofFloat(this.replyButton, View.ALPHA, 0.0f), ObjectAnimator.ofFloat(this.replyButton, View.SCALE_Y, 0.0f));
                    }
                    this.replyButtonAnimation.setDuration(100L);
                    this.replyButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.78
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            if (ChatActivity.this.replyButtonAnimation != null && ChatActivity.this.replyButtonAnimation.equals(animator) && i2 == 8) {
                                ChatActivity.this.replyButton.setVisibility(8);
                            }
                        }

                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationCancel(Animator animator) {
                            if (ChatActivity.this.replyButtonAnimation != null && ChatActivity.this.replyButtonAnimation.equals(animator)) {
                                ChatActivity.this.replyButtonAnimation = null;
                            }
                        }
                    });
                    this.replyButtonAnimation.start();
                }
            }
            if (item4 == null) {
                return;
            }
            if (visibility != visibility3 || visibility2 != visibility4) {
                if (i11 == 0) {
                    item4.setAlpha(1.0f);
                    item4.setScaleX(1.0f);
                } else {
                    item4.setAlpha(0.0f);
                    item4.setScaleX(0.0f);
                }
                item4.setVisibility(i11);
            } else if (item4.getVisibility() != i11) {
                AnimatorSet animatorSet4 = this.editButtonAnimation;
                if (animatorSet4 != null) {
                    animatorSet4.cancel();
                }
                this.editButtonAnimation = new AnimatorSet();
                item4.setPivotX((float) AndroidUtilities.dp(54.0f));
                item4.setPivotX((float) AndroidUtilities.dp(54.0f));
                if (i11 == 0) {
                    item4.setVisibility(i11);
                    this.editButtonAnimation.playTogether(ObjectAnimator.ofFloat(item4, View.ALPHA, 1.0f), ObjectAnimator.ofFloat(item4, View.SCALE_X, 1.0f));
                } else {
                    this.editButtonAnimation.playTogether(ObjectAnimator.ofFloat(item4, View.ALPHA, 0.0f), ObjectAnimator.ofFloat(item4, View.SCALE_X, 0.0f));
                }
                this.editButtonAnimation.setDuration(100L);
                this.editButtonAnimation.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.79
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        if (ChatActivity.this.editButtonAnimation != null && ChatActivity.this.editButtonAnimation.equals(animator) && i11 == 8) {
                            item4.setVisibility(8);
                        }
                    }

                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationCancel(Animator animator) {
                        if (ChatActivity.this.editButtonAnimation != null && ChatActivity.this.editButtonAnimation.equals(animator)) {
                            ChatActivity.this.editButtonAnimation = null;
                        }
                    }
                });
                this.editButtonAnimation.start();
            }
        }
    }

    /* access modifiers changed from: private */
    public void processRowSelect(View view, boolean z, float f, float f2) {
        MessageObject messageObject;
        if (view instanceof ChatMessageCell) {
            ChatMessageCell chatMessageCell = (ChatMessageCell) view;
            messageObject = chatMessageCell.getMessageObject();
            chatMessageCell.setLastTouchCoords(f, f2);
        } else {
            messageObject = view instanceof ChatActionCell ? ((ChatActionCell) view).getMessageObject() : null;
        }
        int messageType = getMessageType(messageObject);
        if (messageType >= 2 && messageType != 20) {
            addToSelectedMessages(messageObject, z);
            updateActionModeTitle();
            updateVisibleRows();
        }
    }

    /* access modifiers changed from: private */
    public void updateActionModeTitle() {
        if (this.reportType >= 0) {
            int size = this.selectedMessagesIds[0].size() + this.selectedMessagesIds[1].size();
            if (size == 0) {
                this.bottomOverlayChatText.setText(LocaleController.getString("ReportMessages", R.string.ReportMessages));
                this.bottomOverlayChatText.setAlpha(0.5f);
                this.bottomOverlayChatText.setEnabled(false);
                return;
            }
            this.bottomOverlayChatText.setText(LocaleController.formatString("ReportMessagesCount", R.string.ReportMessagesCount, LocaleController.formatPluralString("messages", size, new Object[0])).toUpperCase());
            this.bottomOverlayChatText.setAlpha(1.0f);
            this.bottomOverlayChatText.setEnabled(true);
        } else if (this.actionBar.isActionModeShowed()) {
            if (this.selectedMessagesIds[0].size() != 0 || this.selectedMessagesIds[1].size() != 0) {
                this.selectedMessagesCountTextView.setNumber(this.selectedMessagesIds[0].size() + this.selectedMessagesIds[1].size(), true);
            }
        }
    }

    private void updateTitle() {
        if (this.avatarContainer != null) {
            if (isThreadChat()) {
                if (!this.isComments) {
                    this.avatarContainer.setTitle(LocaleController.formatPluralString("Replies", this.threadMessageObject.getRepliesCount(), new Object[0]));
                } else if (this.threadMessageObject.hasReplies()) {
                    this.avatarContainer.setTitle(LocaleController.formatPluralString("Comments", this.threadMessageObject.getRepliesCount(), new Object[0]));
                } else {
                    this.avatarContainer.setTitle(LocaleController.getString("CommentsTitle", R.string.CommentsTitle));
                }
            } else if (UserObject.isReplyUser(this.currentUser)) {
                this.avatarContainer.setTitle(LocaleController.getString("RepliesTitle", R.string.RepliesTitle));
            } else {
                int i = this.chatMode;
                if (i == 1) {
                    if (UserObject.isUserSelf(this.currentUser)) {
                        this.avatarContainer.setTitle(LocaleController.getString("Reminders", R.string.Reminders));
                    } else {
                        this.avatarContainer.setTitle(LocaleController.getString("ScheduledMessages", R.string.ScheduledMessages));
                    }
                } else if (i == 2) {
                    this.avatarContainer.setTitle(LocaleController.formatPluralString("PinnedMessagesCount", getPinnedMessagesCount(), new Object[0]));
                } else {
                    TLRPC$Chat tLRPC$Chat = this.currentChat;
                    if (tLRPC$Chat != null) {
                        this.avatarContainer.setTitle(tLRPC$Chat.title, tLRPC$Chat.scam, tLRPC$Chat.fake, tLRPC$Chat.verified, false);
                    } else {
                        TLRPC$User tLRPC$User = this.currentUser;
                        if (tLRPC$User != null) {
                            if (tLRPC$User.self) {
                                this.avatarContainer.setTitle(LocaleController.getString("SavedMessages", R.string.SavedMessages));
                            } else if (MessagesController.isSupportUser(tLRPC$User) || getContactsController().contactsDict.get(Long.valueOf(this.currentUser.id)) != null || (getContactsController().contactsDict.size() == 0 && getContactsController().isLoadingContacts())) {
                                ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
                                String userName = UserObject.getUserName(this.currentUser);
                                TLRPC$User tLRPC$User2 = this.currentUser;
                                chatAvatarContainer.setTitle(userName, tLRPC$User2.scam, tLRPC$User2.fake, tLRPC$User2.verified, getMessagesController().isPremiumUser(this.currentUser));
                            } else if (!TextUtils.isEmpty(this.currentUser.phone)) {
                                ChatAvatarContainer chatAvatarContainer2 = this.avatarContainer;
                                PhoneFormat instance = PhoneFormat.getInstance();
                                chatAvatarContainer2.setTitle(instance.format("+" + this.currentUser.phone));
                            } else {
                                ChatAvatarContainer chatAvatarContainer3 = this.avatarContainer;
                                String userName2 = UserObject.getUserName(this.currentUser);
                                TLRPC$User tLRPC$User3 = this.currentUser;
                                chatAvatarContainer3.setTitle(userName2, tLRPC$User3.scam, tLRPC$User3.fake, tLRPC$User3.verified, getMessagesController().isPremiumUser(this.currentUser));
                            }
                        }
                    }
                }
            }
            setParentActivityTitle(this.avatarContainer.getTitleTextView().getText());
        }
    }

    /* access modifiers changed from: private */
    public int getPinnedMessagesCount() {
        return Math.max(this.loadedPinnedMessagesCount, this.totalPinnedMessagesCount);
    }

    private void updateBotButtons() {
        TLRPC$User tLRPC$User;
        boolean z;
        if (this.headerItem != null && (tLRPC$User = this.currentUser) != null && this.currentEncryptedChat == null && tLRPC$User.bot) {
            boolean z2 = false;
            if (this.botInfo.size() != 0) {
                boolean z3 = false;
                z = false;
                for (int i = 0; i < this.botInfo.size(); i++) {
                    TLRPC$BotInfo valueAt = this.botInfo.valueAt(i);
                    for (int i2 = 0; i2 < valueAt.commands.size(); i2++) {
                        TLRPC$TL_botCommand tLRPC$TL_botCommand = valueAt.commands.get(i2);
                        if (tLRPC$TL_botCommand.command.toLowerCase().equals("help")) {
                            z3 = true;
                        } else if (tLRPC$TL_botCommand.command.toLowerCase().equals("settings")) {
                            z = true;
                        }
                        if (!z || !z3) {
                        }
                    }
                }
                z2 = z3;
            } else {
                z = false;
            }
            if (z2) {
                this.headerItem.showSubItem(30);
            } else {
                this.headerItem.hideSubItem(30);
            }
            if (z) {
                this.headerItem.showSubItem(31);
            } else {
                this.headerItem.hideSubItem(31);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateTitleIcons() {
        updateTitleIcons(false);
    }

    private void updateTitleIcons(boolean z) {
        ActionBarMenuSubItem actionBarMenuSubItem;
        if (this.avatarContainer != null && this.chatMode == 0) {
            boolean isDialogMuted = getMessagesController().isDialogMuted(this.dialog_id);
            if (z) {
                isDialogMuted = !isDialogMuted;
            }
            Drawable drawable = null;
            Drawable themedDrawable = (UserObject.isReplyUser(this.currentUser) || isThreadChat() || !isDialogMuted) ? null : getThemedDrawable("drawableMuteIcon");
            ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
            if (this.currentEncryptedChat != null) {
                drawable = getThemedDrawable("drawableLockIcon");
            }
            chatAvatarContainer.setTitleIcons(drawable, themedDrawable);
            if (!z && (actionBarMenuSubItem = this.muteItem) != null) {
                if (isDialogMuted) {
                    actionBarMenuSubItem.getRightIcon().setVisibility(8);
                    this.muteItem.setTextAndIcon(LocaleController.getString("Unmute", R.string.Unmute), R.drawable.msg_mute);
                } else {
                    actionBarMenuSubItem.getRightIcon().setVisibility(0);
                    if (getMessagesController().isDialogNotificationsSoundEnabled(this.dialog_id)) {
                        this.muteItem.setTextAndIcon(LocaleController.getString("Mute", R.string.Mute), R.drawable.msg_unmute);
                    } else {
                        this.muteItem.setTextAndIcon(LocaleController.getString("Mute", R.string.Mute), R.drawable.msg_silent);
                    }
                }
            }
            ChatNotificationsPopupWrapper chatNotificationsPopupWrapper = this.chatNotificationsPopupWrapper;
            if (chatNotificationsPopupWrapper != null) {
                chatNotificationsPopupWrapper.lambda$update$10(this.dialog_id);
            }
        }
    }

    private void checkAndUpdateAvatar() {
        if (this.currentUser != null) {
            TLRPC$User user = getMessagesController().getUser(Long.valueOf(this.currentUser.id));
            if (user != null) {
                this.currentUser = user;
            } else {
                return;
            }
        } else if (this.currentChat != null) {
            TLRPC$Chat chat = getMessagesController().getChat(Long.valueOf(this.currentChat.id));
            if (chat != null) {
                this.currentChat = chat;
            } else {
                return;
            }
        }
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        if (chatAvatarContainer != null) {
            chatAvatarContainer.checkAndUpdateAvatar();
        }
    }

    public void openVideoEditor(String str, String str2) {
        if (getParentActivity() != null) {
            final Bitmap createVideoThumbnail = SendMessagesHelper.createVideoThumbnail(str, 1);
            PhotoViewer.getInstance().setParentActivity(getParentActivity(), this.themeDelegate);
            final ArrayList<Object> arrayList = new ArrayList<>();
            MediaController.PhotoEntry photoEntry = new MediaController.PhotoEntry(0, 0, 0, str, 0, true, 0, 0, 0);
            photoEntry.caption = str2;
            arrayList.add(photoEntry);
            PhotoViewer.getInstance().openPhotoForSelect(arrayList, 0, 0, false, new PhotoViewer.EmptyPhotoViewerProvider() { // from class: org.telegram.ui.ChatActivity.80
                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public boolean canScrollAway() {
                    return false;
                }

                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public ImageReceiver.BitmapHolder getThumbForPhoto(MessageObject messageObject, TLRPC$FileLocation tLRPC$FileLocation, int i) {
                    return new ImageReceiver.BitmapHolder(createVideoThumbnail, (String) null, 0);
                }

                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public void sendButtonPressed(int i, VideoEditedInfo videoEditedInfo, boolean z, int i2, boolean z2) {
                    ChatActivity.this.sendMedia((MediaController.PhotoEntry) arrayList.get(0), videoEditedInfo, z, i2, z2);
                }
            }, this);
            return;
        }
        fillEditingMediaWithCaption(str2, null);
        SendMessagesHelper.prepareSendingVideo(getAccountInstance(), str, null, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, null, 0, this.editingMessageObject, true, 0, false);
        afterMessageSend();
    }

    public boolean openPhotosEditor(ArrayList<SendMessagesHelper.SendingMediaInfo> arrayList, CharSequence charSequence) {
        String str;
        int i;
        final ArrayList<MediaController.PhotoEntry> arrayList2 = new ArrayList<>();
        int i2 = 0;
        while (true) {
            String str2 = null;
            if (i2 >= arrayList.size()) {
                break;
            }
            SendMessagesHelper.SendingMediaInfo sendingMediaInfo = arrayList.get(i2);
            String str3 = sendingMediaInfo.path;
            if (str3 != null) {
                str = str3;
            } else {
                if (sendingMediaInfo.uri != null) {
                    try {
                        File generatePicturePath = AndroidUtilities.generatePicturePath(isSecretChat(), "");
                        InputStream openInputStream = ApplicationLoader.applicationContext.getContentResolver().openInputStream(sendingMediaInfo.uri);
                        FileOutputStream fileOutputStream = new FileOutputStream(generatePicturePath);
                        byte[] bArr = new byte[8192];
                        while (true) {
                            int read = openInputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                            fileOutputStream.flush();
                        }
                        openInputStream.close();
                        fileOutputStream.close();
                        str2 = generatePicturePath.getAbsolutePath();
                    } catch (Exception unused) {
                    }
                }
                str = str2;
            }
            if (str != null) {
                try {
                    int attributeInt = new ExifInterface(str).getAttributeInt("Orientation", 1);
                    i = attributeInt != 3 ? attributeInt != 6 ? attributeInt != 8 ? 0 : 270 : 90 : 180;
                } catch (Exception e) {
                    FileLog.e(e);
                    i = 0;
                }
                MediaController.PhotoEntry photoEntry = new MediaController.PhotoEntry(0, 0, 0, str, i, sendingMediaInfo.isVideo, 0, 0, 0);
                if (i2 == arrayList.size() - 1 && charSequence != null) {
                    photoEntry.caption = charSequence;
                }
                arrayList2.add(photoEntry);
            }
            i2++;
        }
        if (arrayList2.isEmpty()) {
            return false;
        }
        if (getParentActivity() != null) {
            final boolean[] zArr = new boolean[arrayList2.size()];
            Arrays.fill(zArr, true);
            PhotoViewer.getInstance().setParentActivity(getParentActivity(), this.themeDelegate);
            PhotoViewer.getInstance().openPhotoForSelect(new ArrayList<>(arrayList2), arrayList2.size() - 1, 0, false, new PhotoViewer.EmptyPhotoViewerProvider() { // from class: org.telegram.ui.ChatActivity.81
                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public boolean canScrollAway() {
                    return false;
                }

                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public ImageReceiver.BitmapHolder getThumbForPhoto(MessageObject messageObject, TLRPC$FileLocation tLRPC$FileLocation, int i3) {
                    return null;
                }

                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public int setPhotoChecked(int i3, VideoEditedInfo videoEditedInfo) {
                    return i3;
                }

                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public void sendButtonPressed(int i3, VideoEditedInfo videoEditedInfo, boolean z, int i4, boolean z2) {
                    for (int size = arrayList2.size() - 1; size >= 0; size--) {
                        if (!zArr[size]) {
                            arrayList2.remove(size);
                        }
                    }
                    ChatActivity.this.sendPhotosGroup(arrayList2, z, i4, z2);
                }

                @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                public boolean isPhotoChecked(int i3) {
                    return zArr[i3];
                }
            }, this);
        } else {
            fillEditingMediaWithCaption(charSequence, null);
            sendPhotosGroup(arrayList2, false, 0, false);
            afterMessageSend();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void sendPhotosGroup(ArrayList<MediaController.PhotoEntry> arrayList, boolean z, int i, boolean z2) {
        String str;
        if (!arrayList.isEmpty()) {
            ArrayList arrayList2 = new ArrayList();
            Iterator<MediaController.PhotoEntry> it = arrayList.iterator();
            while (it.hasNext()) {
                MediaController.PhotoEntry next = it.next();
                SendMessagesHelper.SendingMediaInfo sendingMediaInfo = new SendMessagesHelper.SendingMediaInfo();
                boolean z3 = next.isVideo;
                if (z3 || (str = next.imagePath) == null) {
                    String str2 = next.path;
                    if (str2 != null) {
                        sendingMediaInfo.path = str2;
                    }
                } else {
                    sendingMediaInfo.path = str;
                }
                sendingMediaInfo.thumbPath = next.thumbPath;
                sendingMediaInfo.isVideo = z3;
                CharSequence charSequence = next.caption;
                sendingMediaInfo.caption = charSequence != null ? charSequence.toString() : null;
                sendingMediaInfo.entities = next.entities;
                sendingMediaInfo.masks = next.stickers;
                sendingMediaInfo.ttl = next.ttl;
                sendingMediaInfo.videoEditedInfo = next.editedInfo;
                sendingMediaInfo.canDeleteAfter = next.canDeleteAfter;
                arrayList2.add(sendingMediaInfo);
                next.reset();
            }
            fillEditingMediaWithCaption(((SendMessagesHelper.SendingMediaInfo) arrayList2.get(0)).caption, ((SendMessagesHelper.SendingMediaInfo) arrayList2.get(0)).entities);
            SendMessagesHelper.prepareSendingMedia(getAccountInstance(), arrayList2, this.dialog_id, null, getThreadMessage(), null, z2, true, null, z, i);
            afterMessageSend();
            this.chatActivityEnterView.setFieldText("");
        }
        if (i != 0) {
            if (this.scheduledMessagesCount == -1) {
                this.scheduledMessagesCount = 0;
            }
            this.scheduledMessagesCount += arrayList.size();
            updateScheduledInterface(true);
        }
    }

    private void openEditingMessageInPhotoEditor() {
        MessageObject messageObject = this.editingMessageObject;
        if (messageObject != null && messageObject.canEditMedia() && this.editingMessageObjectReqId == 0) {
            if (this.editingMessageObject.isPhoto() || this.editingMessageObject.isVideo()) {
                final MessageObject messageObject2 = this.editingMessageObject;
                File file = null;
                if (!TextUtils.isEmpty(messageObject2.messageOwner.attachPath)) {
                    File file2 = new File(messageObject2.messageOwner.attachPath);
                    if (file2.exists()) {
                        file = file2;
                    }
                }
                if (file == null) {
                    file = FileLoader.getInstance(this.currentAccount).getPathToMessage(messageObject2.messageOwner);
                }
                if (file.exists()) {
                    PhotoViewer.getInstance().setParentActivity(getParentActivity(), this.themeDelegate);
                    ArrayList<Object> arrayList = new ArrayList<>();
                    final MediaController.PhotoEntry photoEntry = new MediaController.PhotoEntry(0, 0, 0, file.getAbsolutePath(), 0, messageObject2.isVideo(), 0, 0, 0);
                    photoEntry.caption = this.chatActivityEnterView.getFieldText();
                    arrayList.add(photoEntry);
                    PhotoViewer.getInstance().openPhotoForSelect(arrayList, 0, 2, false, new PhotoViewer.EmptyPhotoViewerProvider() { // from class: org.telegram.ui.ChatActivity.82
                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public boolean allowSendingSubmenu() {
                            return false;
                        }

                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public boolean canCaptureMorePhotos() {
                            return false;
                        }

                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public PhotoViewer.PlaceProviderObject getPlaceForPhoto(MessageObject messageObject3, TLRPC$FileLocation tLRPC$FileLocation, int i, boolean z) {
                            return ChatActivity.this.getPlaceForPhoto(messageObject2, null, z, true);
                        }

                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public void sendButtonPressed(int i, VideoEditedInfo videoEditedInfo, boolean z, int i2, boolean z2) {
                            if (ChatActivity.this.editingMessageObject == messageObject2) {
                                MediaController.PhotoEntry photoEntry2 = photoEntry;
                                if (photoEntry2.isCropped || photoEntry2.isPainted || photoEntry2.isFiltered || videoEditedInfo != null) {
                                    ChatActivity.this.sendMedia(photoEntry2, videoEditedInfo, z, i2, z2);
                                } else {
                                    ChatActivity.this.chatActivityEnterView.doneEditingMessage();
                                }
                            }
                        }

                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public MessageObject getEditingMessageObject() {
                            MessageObject messageObject3 = ChatActivity.this.editingMessageObject;
                            MessageObject messageObject4 = messageObject2;
                            if (messageObject3 == messageObject4) {
                                return messageObject4;
                            }
                            return null;
                        }

                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public void onCaptionChanged(CharSequence charSequence) {
                            if (ChatActivity.this.editingMessageObject == messageObject2) {
                                ChatActivity.this.chatActivityEnterView.setFieldText(charSequence, true);
                            }
                        }

                        @Override // org.telegram.ui.PhotoViewer.EmptyPhotoViewerProvider, org.telegram.ui.PhotoViewer.PhotoViewerProvider
                        public boolean closeKeyboard() {
                            ChatActivity chatActivity = ChatActivity.this;
                            if (chatActivity.chatActivityEnterView == null || !chatActivity.isKeyboardVisible()) {
                                return false;
                            }
                            ChatActivity.this.chatActivityEnterView.closeKeyboard();
                            return true;
                        }
                    }, this);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0108 A[LOOP:0: B:3:0x000c->B:64:0x0108, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0085 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.telegram.ui.PhotoViewer.PlaceProviderObject getPlaceForPhoto(org.telegram.messenger.MessageObject r17, org.telegram.tgnet.TLRPC$FileLocation r18, boolean r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 269
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.getPlaceForPhoto(org.telegram.messenger.MessageObject, org.telegram.tgnet.TLRPC$FileLocation, boolean, boolean):org.telegram.ui.PhotoViewer$PlaceProviderObject");
    }

    private void showAttachmentError() {
        if (getParentActivity() != null) {
            BulletinFactory.of(this).createErrorBulletin(LocaleController.getString("UnsupportedAttachment", R.string.UnsupportedAttachment), this.themeDelegate).show();
        }
    }

    /* access modifiers changed from: private */
    public void fillEditingMediaWithCaption(CharSequence charSequence, ArrayList<TLRPC$MessageEntity> arrayList) {
        if (this.editingMessageObject != null) {
            if (!TextUtils.isEmpty(charSequence)) {
                MessageObject messageObject = this.editingMessageObject;
                messageObject.editingMessage = charSequence;
                messageObject.editingMessageEntities = arrayList;
                return;
            }
            ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
            if (chatActivityEnterView != null) {
                this.editingMessageObject.editingMessage = chatActivityEnterView.getFieldText();
                MessageObject messageObject2 = this.editingMessageObject;
                if (messageObject2.editingMessage == null && !TextUtils.isEmpty(messageObject2.messageOwner.message)) {
                    this.editingMessageObject.editingMessage = "";
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0083  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void sendUriAsDocument(android.net.Uri r24) {
        /*
            r23 = this;
            r1 = r23
            if (r24 != 0) goto L_0x0005
            return
        L_0x0005:
            java.lang.String r0 = r24.toString()
            java.lang.String r2 = "com.google.android.apps.photos.contentprovider"
            boolean r2 = r0.contains(r2)
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003b
            java.lang.String r2 = "/1/"
            java.lang.String[] r0 = r0.split(r2)     // Catch: Exception -> 0x0037
            r0 = r0[r3]     // Catch: Exception -> 0x0037
            java.lang.String r2 = "/ACTUAL"
            int r2 = r0.indexOf(r2)     // Catch: Exception -> 0x0037
            r5 = -1
            if (r2 == r5) goto L_0x0033
            java.lang.String r0 = r0.substring(r4, r2)     // Catch: Exception -> 0x0037
            java.lang.String r2 = "UTF-8"
            java.lang.String r0 = java.net.URLDecoder.decode(r0, r2)     // Catch: Exception -> 0x0037
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch: Exception -> 0x0037
            goto L_0x0035
        L_0x0033:
            r0 = r24
        L_0x0035:
            r8 = r0
            goto L_0x003d
        L_0x0037:
            r0 = move-exception
            org.telegram.messenger.FileLog.e(r0)
        L_0x003b:
            r8 = r24
        L_0x003d:
            java.lang.String r0 = org.telegram.messenger.AndroidUtilities.getPath(r8)
            boolean r2 = org.telegram.messenger.BuildVars.NO_SCOPED_STORAGE
            if (r2 != 0) goto L_0x0048
            r10 = r0
            r11 = r10
            goto L_0x0060
        L_0x0048:
            if (r0 != 0) goto L_0x005d
            java.lang.String r0 = r8.toString()
            java.lang.String r2 = "file"
            java.lang.String r2 = org.telegram.messenger.MediaController.copyFileToCache(r8, r2)
            if (r2 != 0) goto L_0x005a
            r23.showAttachmentError()
            return
        L_0x005a:
            r11 = r0
            r10 = r2
            goto L_0x005f
        L_0x005d:
            r10 = r0
            r11 = r10
        L_0x005f:
            r3 = 0
        L_0x0060:
            r0 = 0
            r1.fillEditingMediaWithCaption(r0, r0)
            if (r3 == 0) goto L_0x0083
            org.telegram.messenger.AccountInstance r5 = r23.getAccountInstance()
            r6 = 0
            r7 = 0
            r9 = 0
            r10 = 0
            long r11 = r1.dialog_id
            org.telegram.messenger.MessageObject r13 = r1.replyingMessageObject
            org.telegram.messenger.MessageObject r14 = r23.getThreadMessage()
            r15 = 0
            org.telegram.messenger.MessageObject r0 = r1.editingMessageObject
            r17 = 1
            r18 = 0
            r16 = r0
            org.telegram.messenger.SendMessagesHelper.prepareSendingDocument(r5, r6, r7, r8, r9, r10, r11, r13, r14, r15, r16, r17, r18)
            goto L_0x00a2
        L_0x0083:
            org.telegram.messenger.AccountInstance r9 = r23.getAccountInstance()
            r12 = 0
            r13 = 0
            r14 = 0
            long r2 = r1.dialog_id
            org.telegram.messenger.MessageObject r0 = r1.replyingMessageObject
            org.telegram.messenger.MessageObject r18 = r23.getThreadMessage()
            r19 = 0
            org.telegram.messenger.MessageObject r5 = r1.editingMessageObject
            r21 = 1
            r22 = 0
            r15 = r2
            r17 = r0
            r20 = r5
            org.telegram.messenger.SendMessagesHelper.prepareSendingDocument(r9, r10, r11, r12, r13, r14, r15, r17, r18, r19, r20, r21, r22)
        L_0x00a2:
            r1.hideFieldPanel(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.sendUriAsDocument(android.net.Uri):void");
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void onActivityResultFragment(int i, int i2, Intent intent) {
        String str;
        if (i2 != -1) {
            return;
        }
        if (i == 0 || i == 2) {
            createChatAttachView();
            ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
            if (chatAttachAlert != null) {
                chatAttachAlert.getPhotoLayout().onActivityResultFragment(i, intent, this.currentPicturePath);
            }
            this.currentPicturePath = null;
        } else if (i == 1) {
            if (intent == null || intent.getData() == null) {
                showAttachmentError();
                return;
            }
            Uri data = intent.getData();
            if (data.toString().contains(MediaStreamTrack.VIDEO_TRACK_KIND)) {
                try {
                    str = AndroidUtilities.getPath(data);
                } catch (Exception e) {
                    FileLog.e(e);
                    str = null;
                }
                if (str == null) {
                    showAttachmentError();
                }
                if (this.paused) {
                    this.startVideoEdit = str;
                } else {
                    openVideoEditor(str, null);
                }
            } else if (this.editingMessageObject == null && this.chatMode == 1) {
                AlertsCreator.createScheduleDatePickerDialog(getParentActivity(), this.dialog_id, new AlertsCreator.ScheduleDatePickerDelegate(data) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda236
                    public final /* synthetic */ Uri f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.telegram.ui.Components.AlertsCreator.ScheduleDatePickerDelegate
                    public final void didSelectDate(boolean z, int i3) {
                        ChatActivity.this.lambda$onActivityResultFragment$115(this.f$1, z, i3);
                    }
                }, this.themeDelegate);
            } else {
                fillEditingMediaWithCaption(null, null);
                SendMessagesHelper.prepareSendingPhoto(getAccountInstance(), null, data, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, null, null, null, 0, this.editingMessageObject, true, 0);
            }
            afterMessageSend();
        } else if (i == 21) {
            if (intent == null) {
                showAttachmentError();
                return;
            }
            if (intent.getData() != null) {
                sendUriAsDocument(intent.getData());
            } else if (intent.getClipData() != null) {
                ClipData clipData = intent.getClipData();
                for (int i3 = 0; i3 < clipData.getItemCount(); i3++) {
                    sendUriAsDocument(clipData.getItemAt(i3).getUri());
                }
            } else {
                showAttachmentError();
            }
            ChatAttachAlert chatAttachAlert2 = this.chatAttachAlert;
            if (chatAttachAlert2 != null) {
                chatAttachAlert2.dismiss();
            }
            afterMessageSend();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onActivityResultFragment$115(Uri uri, boolean z, int i) {
        fillEditingMediaWithCaption(null, null);
        SendMessagesHelper.prepareSendingPhoto(getAccountInstance(), null, uri, this.dialog_id, this.replyingMessageObject, getThreadMessage(), null, null, null, null, 0, this.editingMessageObject, z, i);
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void saveSelfArgs(Bundle bundle) {
        String str = this.currentPicturePath;
        if (str != null) {
            bundle.putString("path", str);
        }
    }

    public void restoreSelfArgs(Bundle bundle) {
        this.currentPicturePath = bundle.getString("path");
    }

    private void removeUnreadPlane(boolean z) {
        MessageObject messageObject = this.unreadMessageObject;
        if (messageObject != null) {
            if (z) {
                boolean[] zArr = this.forwardEndReached;
                zArr[1] = true;
                zArr[0] = true;
                this.first_unread_id = 0;
                this.last_message_id = 0;
            }
            this.createUnreadMessageAfterId = 0;
            this.createUnreadMessageAfterIdLoading = false;
            removeMessageObject(messageObject);
            this.unreadMessageObject = null;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 3078
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:66)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    @Override // org.telegram.messenger.NotificationCenter.NotificationCenterDelegate
    public void didReceivedNotification(int r54, int r55, java.lang.Object... r56) {
        /*
        // Method dump skipped, instructions count: 14032
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.didReceivedNotification(int, int, java.lang.Object[]):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didReceivedNotification$116() {
        getNotificationCenter().onAnimationFinish(this.transitionAnimationIndex);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didReceivedNotification$117() {
        getNotificationCenter().runDelayedNotifications();
        resumeDelayedFragmentAnimation();
        AndroidUtilities.cancelRunOnUIThread(this.fragmentTransitionRunnable);
        this.fragmentTransitionRunnable.run();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didReceivedNotification$118(MessageObject messageObject, int i) {
        this.delayedReadRunnable = null;
        messageObject.messageOwner.replies.read_max_id = i;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$didReceivedNotification$120(Theme.ThemeAccent themeAccent, Theme.ThemeInfo themeInfo, boolean z) {
        if (themeAccent != null) {
            Theme.ThemeAccent accent = themeInfo.getAccent(false);
            NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.needSetDayNightTheme, themeInfo, Boolean.FALSE, null, Integer.valueOf(themeAccent.id));
            if (z) {
                Theme.deleteThemeAccent(themeInfo, accent, true);
                return;
            }
            return;
        }
        NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.needSetDayNightTheme, themeInfo, Boolean.FALSE, null, -1);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didReceivedNotification$121(int i) {
        playReactionAnimation(Integer.valueOf(i));
    }

    private void checkSecretMessageForLocation(MessageObject messageObject) {
        if (messageObject.type == 4 && !this.locationAlertShown && !SharedConfig.isSecretMapPreviewSet()) {
            this.locationAlertShown = true;
            AlertsCreator.showSecretLocationAlert(getParentActivity(), this.currentAccount, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda119
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$checkSecretMessageForLocation$122();
                }
            }, true, this.themeDelegate);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$checkSecretMessageForLocation$122() {
        int childCount = this.chatListView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.chatListView.getChildAt(i);
            if (childAt instanceof ChatMessageCell) {
                ChatMessageCell chatMessageCell = (ChatMessageCell) childAt;
                if (chatMessageCell.getMessageObject().type == 4) {
                    chatMessageCell.forceResetMessageObject();
                }
            }
        }
    }

    private void loadSendAsPeers(boolean z) {
        TLRPC$Chat tLRPC$Chat;
        if (this.sendAsPeersObj == null && (tLRPC$Chat = this.currentChat) != null && ChatObject.canSendAsPeers(tLRPC$Chat) && this.chatActivityEnterView != null) {
            TLRPC$TL_channels_sendAsPeers sendAsPeers = getMessagesController().getSendAsPeers(this.dialog_id);
            this.sendAsPeersObj = sendAsPeers;
            if (sendAsPeers != null) {
                this.chatActivityEnterView.updateSendAsButton(z);
            }
        }
    }

    private void addSponsoredMessages(boolean z) {
        ArrayList<MessageObject> sponsoredMessages;
        if (!this.sponsoredMessagesAdded && this.chatMode == 0 && ChatObject.isChannel(this.currentChat) && this.forwardEndReached[0] && !getUserConfig().isPremium() && (sponsoredMessages = getMessagesController().getSponsoredMessages(this.dialog_id)) != null) {
            for (int i = 0; i < sponsoredMessages.size(); i++) {
                MessageObject messageObject = sponsoredMessages.get(i);
                messageObject.resetLayout();
                long peerId = MessageObject.getPeerId(messageObject.messageOwner.from_id);
                int i2 = messageObject.sponsoredChannelPost;
                if (i2 == 0) {
                    i2 = 0;
                }
                getMessagesController().ensureMessagesLoaded(peerId, i2, null);
            }
            this.sponsoredMessagesAdded = true;
            processNewMessages(sponsoredMessages);
        }
    }

    private void checkGroupCallJoin(boolean z) {
        TLRPC$ChatFull tLRPC$ChatFull;
        String str;
        ChatObject.Call call = this.groupCall;
        if (call == null || (str = this.voiceChatHash) == null || !this.openAnimationEnded) {
            if (!(this.voiceChatHash == null || !z || (tLRPC$ChatFull = this.chatInfo) == null || tLRPC$ChatFull.call != null || this.fragmentView == null || getParentActivity() == null)) {
                BulletinFactory.of(this).createSimpleBulletin(R.raw.linkbroken, LocaleController.getString("LinkHashExpired", R.string.LinkHashExpired)).show();
                this.voiceChatHash = null;
            }
            this.lastCallCheckFromServer = !this.openAnimationEnded;
            return;
        }
        VoIPHelper.startCall(this.currentChat, null, str, this.createGroupCall, Boolean.valueOf(!call.call.rtmp_stream), getParentActivity(), this, getAccountInstance());
        this.voiceChatHash = null;
    }

    private void checkWaitingForReplies() {
        TLRPC$MessageFwdHeader tLRPC$MessageFwdHeader;
        int i;
        MessageObject messageObject;
        TLRPC$MessageReplies tLRPC$MessageReplies;
        int indexOf;
        if (this.waitingForReplies.size() != 0) {
            int size = this.waitingForReplies.size();
            ArrayList arrayList = null;
            LongSparseArray longSparseArray = null;
            ArrayList arrayList2 = null;
            for (int i2 = 0; i2 < size; i2++) {
                MessageObject valueAt = this.waitingForReplies.valueAt(i2);
                if (valueAt.replyMessageObject != null) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(Integer.valueOf(this.waitingForReplies.keyAt(i2)));
                    if (!((valueAt.messageOwner.action instanceof TLRPC$TL_messageActionPinMessage) || (tLRPC$MessageFwdHeader = valueAt.replyMessageObject.messageOwner.fwd_from) == null || MessageObject.getPeerId(tLRPC$MessageFwdHeader.saved_from_peer) != this.dialog_id || (i = valueAt.replyMessageObject.messageOwner.fwd_from.channel_post) == 0 || (messageObject = this.messagesDict[0].get(i)) == null || (tLRPC$MessageReplies = messageObject.messageOwner.replies) == null)) {
                        tLRPC$MessageReplies.replies++;
                        messageObject.animateComments = true;
                        TLRPC$Message tLRPC$Message = valueAt.messageOwner;
                        TLRPC$Peer tLRPC$Peer = tLRPC$Message.from_id;
                        if (tLRPC$Peer == null) {
                            tLRPC$Peer = tLRPC$Message.peer_id;
                        }
                        int size2 = tLRPC$MessageReplies.recent_repliers.size();
                        int i3 = 0;
                        while (true) {
                            if (i3 >= size2) {
                                break;
                            } else if (MessageObject.getPeerId(messageObject.messageOwner.replies.recent_repliers.get(i3)) == MessageObject.getPeerId(tLRPC$Peer)) {
                                messageObject.messageOwner.replies.recent_repliers.remove(i3);
                                break;
                            } else {
                                i3++;
                            }
                        }
                        messageObject.messageOwner.replies.recent_repliers.add(0, tLRPC$Peer);
                        if (!valueAt.isOut()) {
                            messageObject.messageOwner.replies.max_id = valueAt.getId();
                        }
                        MessagesStorage messagesStorage = getMessagesStorage();
                        long j = this.currentChat.id;
                        int id = messageObject.getId();
                        TLRPC$MessageReplies tLRPC$MessageReplies2 = messageObject.messageOwner.replies;
                        messagesStorage.updateRepliesCount(j, id, tLRPC$MessageReplies2.recent_repliers, tLRPC$MessageReplies2.max_id, 1);
                        if (messageObject.hasValidGroupId()) {
                            MessageObject.GroupedMessages groupedMessages = this.groupedMessagesMap.get(messageObject.getGroupId());
                            if (groupedMessages != null) {
                                if (longSparseArray == null) {
                                    longSparseArray = new LongSparseArray();
                                }
                                longSparseArray.put(groupedMessages.groupId, groupedMessages);
                                int size3 = groupedMessages.messages.size();
                                for (int i4 = 0; i4 < size3; i4++) {
                                    groupedMessages.messages.get(i4).animateComments = true;
                                }
                            }
                        } else if (this.chatAdapter != null && (indexOf = this.messages.indexOf(messageObject)) >= 0) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                            }
                            arrayList2.add(Integer.valueOf(indexOf + this.chatAdapter.messagesStartRow));
                        }
                    }
                }
            }
            if (arrayList != null) {
                int size4 = arrayList.size();
                for (int i5 = 0; i5 < size4; i5++) {
                    this.waitingForReplies.remove(((Integer) arrayList.get(i5)).intValue());
                }
            }
            if (this.chatAdapter != null) {
                if (longSparseArray != null) {
                    int size5 = longSparseArray.size();
                    for (int i6 = 0; i6 < size5; i6++) {
                        MessageObject.GroupedMessages groupedMessages2 = (MessageObject.GroupedMessages) longSparseArray.valueAt(i6);
                        ArrayList<MessageObject> arrayList3 = groupedMessages2.messages;
                        int indexOf2 = this.messages.indexOf(arrayList3.get(arrayList3.size() - 1));
                        if (indexOf2 >= 0) {
                            ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
                            chatActivityAdapter.notifyItemRangeChanged(indexOf2 + chatActivityAdapter.messagesStartRow, groupedMessages2.messages.size());
                        }
                    }
                }
                if (arrayList2 != null) {
                    int size6 = arrayList2.size();
                    for (int i7 = 0; i7 < size6; i7++) {
                        this.chatAdapter.notifyItemChanged(((Integer) arrayList2.get(i7)).intValue());
                    }
                }
            }
        }
    }

    private void clearHistory(boolean z, TLRPC$TL_updates_channelDifferenceTooLong tLRPC$TL_updates_channelDifferenceTooLong) {
        TLRPC$User tLRPC$User;
        if (z) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("clear history by overwrite firstLoading=" + this.firstLoading + " minMessage=" + this.minMessageId[0] + " topMessage=" + tLRPC$TL_updates_channelDifferenceTooLong.dialog.top_message);
            }
            TLRPC$Dialog tLRPC$Dialog = tLRPC$TL_updates_channelDifferenceTooLong.dialog;
            int i = tLRPC$Dialog.top_message;
            int[] iArr = this.minMessageId;
            if (i > iArr[0]) {
                this.createUnreadMessageAfterId = Math.max(iArr[0] + 1, tLRPC$Dialog.read_inbox_max_id);
            }
            this.forwardEndReached[0] = false;
            this.hideForwardEndReached = false;
            ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
            if (chatActivityAdapter != null && chatActivityAdapter.loadingDownRow < 0) {
                this.chatAdapter.notifyItemInserted(0);
            }
            TLRPC$Dialog tLRPC$Dialog2 = tLRPC$TL_updates_channelDifferenceTooLong.dialog;
            int i2 = tLRPC$Dialog2.unread_count;
            this.newUnreadMessageCount = i2;
            this.newMentionsCount = tLRPC$Dialog2.unread_mentions_count;
            if (this.prevSetUnreadCount != i2) {
                CounterView counterView = this.pagedownButtonCounter;
                if (counterView != null) {
                    counterView.setCount(i2, this.openAnimationEnded);
                }
                this.prevSetUnreadCount = this.newUnreadMessageCount;
                updatePagedownButtonVisibility(true);
            }
            int i3 = this.newMentionsCount;
            int i4 = tLRPC$TL_updates_channelDifferenceTooLong.dialog.unread_mentions_count;
            if (i3 != i4) {
                this.newMentionsCount = i4;
                if (i4 <= 0) {
                    this.newMentionsCount = 0;
                    this.hasAllMentionsLocal = true;
                    showMentionDownButton(false, true);
                } else {
                    SimpleTextView simpleTextView = this.mentiondownButtonCounter;
                    if (simpleTextView != null) {
                        simpleTextView.setText(String.format("%d", Integer.valueOf(i4)));
                    }
                    showMentionDownButton(true, true);
                }
            }
            checkScrollForLoad(false);
            return;
        }
        this.messages.clear();
        this.waitingForLoad.clear();
        this.messagesByDays.clear();
        this.groupedMessagesMap.clear();
        this.threadMessageAdded = false;
        for (int i5 = 1; i5 >= 0; i5--) {
            this.messagesDict[i5].clear();
            if (this.currentEncryptedChat == null) {
                this.maxMessageId[i5] = Integer.MAX_VALUE;
                this.minMessageId[i5] = Integer.MIN_VALUE;
            } else {
                this.maxMessageId[i5] = Integer.MIN_VALUE;
                this.minMessageId[i5] = Integer.MAX_VALUE;
            }
            this.maxDate[i5] = Integer.MIN_VALUE;
            this.minDate[i5] = 0;
            this.selectedMessagesIds[i5].clear();
            this.selectedMessagesCanCopyIds[i5].clear();
            this.selectedMessagesCanStarIds[i5].clear();
        }
        hideActionMode();
        updatePinnedMessageView(true);
        if (this.botButtons != null) {
            this.botButtons = null;
            ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
            if (chatActivityEnterView != null) {
                chatActivityEnterView.setButtons(null, false);
            }
        }
        if (this.progressView != null) {
            showProgressView(false);
            this.chatListView.setEmptyView(this.emptyViewContainer);
        }
        ChatActivityAdapter chatActivityAdapter2 = this.chatAdapter;
        if (chatActivityAdapter2 != null) {
            chatActivityAdapter2.notifyDataSetChanged(false);
        }
        if (this.currentEncryptedChat == null && (tLRPC$User = this.currentUser) != null && tLRPC$User.bot && this.botUser == null) {
            this.botUser = "";
            updateBottomOverlay();
        }
    }

    public boolean processSwitchButton(TLRPC$TL_keyboardButtonSwitchInline tLRPC$TL_keyboardButtonSwitchInline) {
        if (this.inlineReturn == 0 || tLRPC$TL_keyboardButtonSwitchInline.same_peer || this.parentLayout == null) {
            return false;
        }
        String str = "@" + this.currentUser.username + " " + tLRPC$TL_keyboardButtonSwitchInline.query;
        if (this.inlineReturn == this.dialog_id) {
            this.inlineReturn = 0;
            this.chatActivityEnterView.setFieldText(str);
        } else {
            getMediaDataController().saveDraft(this.inlineReturn, 0, str, null, null, false);
            if (this.parentLayout.fragmentsStack.size() > 1) {
                ArrayList<BaseFragment> arrayList = this.parentLayout.fragmentsStack;
                BaseFragment baseFragment = arrayList.get(arrayList.size() - 2);
                if (!(baseFragment instanceof ChatActivity) || ((ChatActivity) baseFragment).dialog_id != this.inlineReturn) {
                    Bundle bundle = new Bundle();
                    if (DialogObject.isEncryptedDialog(this.inlineReturn)) {
                        bundle.putInt("enc_id", DialogObject.getEncryptedChatId(this.inlineReturn));
                    } else if (DialogObject.isUserDialog(this.inlineReturn)) {
                        bundle.putLong("user_id", this.inlineReturn);
                    } else {
                        bundle.putLong("chat_id", -this.inlineReturn);
                    }
                    addToPulledDialogsMyself();
                    presentFragment(new ChatActivity(bundle), true);
                } else {
                    finishFragment();
                }
            }
        }
        return true;
    }

    private void showGigagroupConvertAlert() {
        TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
        if (tLRPC$ChatFull != null && !this.paused) {
            TLRPC$Chat tLRPC$Chat = this.currentChat;
            if (tLRPC$Chat.creator && tLRPC$Chat.megagroup && !tLRPC$Chat.gigagroup && tLRPC$ChatFull.pending_suggestions.contains("CONVERT_GIGAGROUP") && this.visibleDialog == null) {
                AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda127
                    @Override // java.lang.Runnable
                    public final void run() {
                        ChatActivity.this.lambda$showGigagroupConvertAlert$125();
                    }
                }, 1000);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showGigagroupConvertAlert$125() {
        TLRPC$ChatFull tLRPC$ChatFull = this.chatInfo;
        if (tLRPC$ChatFull != null && !this.paused) {
            TLRPC$Chat tLRPC$Chat = this.currentChat;
            if (tLRPC$Chat.creator && tLRPC$Chat.megagroup && !tLRPC$Chat.gigagroup && tLRPC$ChatFull.pending_suggestions.contains("CONVERT_GIGAGROUP") && this.visibleDialog == null) {
                SharedPreferences notificationsSettings = MessagesController.getNotificationsSettings(this.currentAccount);
                int i = notificationsSettings.getInt("group_convert_time", 0);
                int i2 = BuildVars.DEBUG_PRIVATE_VERSION ? 120 : 604800;
                int currentTime = getConnectionsManager().getCurrentTime();
                if (Math.abs(currentTime - i) >= i2 && this.visibleDialog == null && getParentActivity() != null) {
                    notificationsSettings.edit().putInt("group_convert_time", currentTime).commit();
                    showDialog(AlertsCreator.createGigagroupConvertAlert(getParentActivity(), new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda24
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i3) {
                            ChatActivity.this.lambda$showGigagroupConvertAlert$123(dialogInterface, i3);
                        }
                    }, new DialogInterface.OnClickListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda26
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i3) {
                            ChatActivity.this.lambda$showGigagroupConvertAlert$124(dialogInterface, i3);
                        }
                    }).create());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showGigagroupConvertAlert$123(DialogInterface dialogInterface, int i) {
        showDialog(new GigagroupConvertAlert(getParentActivity(), this) { // from class: org.telegram.ui.ChatActivity.83
            @Override // org.telegram.ui.Components.GigagroupConvertAlert
            protected void onCovert() {
                MessagesController messagesController = ChatActivity.this.getMessagesController();
                Activity parentActivity = ChatActivity.this.getParentActivity();
                ChatActivity chatActivity = ChatActivity.this;
                messagesController.convertToGigaGroup(parentActivity, chatActivity.currentChat, chatActivity, new ChatActivity$83$$ExternalSyntheticLambda0(this));
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onCovert$0(boolean z) {
                if (z) {
                    ChatActivity.this.undoView.showWithAction(0L, 76, (Runnable) null);
                }
            }

            @Override // org.telegram.ui.Components.GigagroupConvertAlert
            protected void onCancel() {
                ChatActivity.this.undoView.showWithAction(0L, 75, (Runnable) null);
                ChatActivity.this.getMessagesController().removeSuggestion(ChatActivity.this.dialog_id, "CONVERT_GIGAGROUP");
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showGigagroupConvertAlert$124(DialogInterface dialogInterface, int i) {
        this.undoView.showWithAction(0L, 75, (Runnable) null);
    }

    private void addReplyMessageOwner(MessageObject messageObject, Integer num) {
        MessageObject messageObject2 = messageObject.replyMessageObject;
        if (messageObject2 != null) {
            int id = messageObject2.getId();
            ArrayList<Integer> arrayList = this.replyMessageOwners.get(id);
            if (arrayList == null) {
                arrayList = new ArrayList<>();
                this.replyMessageOwners.put(id, arrayList);
            }
            int id2 = messageObject.getId();
            if (!arrayList.contains(Integer.valueOf(id2))) {
                arrayList.add(Integer.valueOf(id2));
            }
            if (num.intValue() != 0) {
                arrayList.remove(num);
            }
        }
    }

    private void updateReplyMessageOwners(int i, MessageObject messageObject) {
        ArrayList<Integer> arrayList = this.replyMessageOwners.get(i);
        if (arrayList != null) {
            MessageObject messageObject2 = messageObject == null ? new MessageObject(this.currentAccount, new TLRPC$TL_messageEmpty(), false, false) : null;
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                MessageObject messageObject3 = this.messagesDict[0].get(arrayList.get(i2).intValue());
                if (messageObject3 != null) {
                    if (messageObject == null) {
                        messageObject3.replyMessageObject = messageObject2;
                    } else {
                        messageObject3.replyMessageObject = messageObject;
                    }
                    ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
                    if (chatActivityAdapter != null) {
                        chatActivityAdapter.updateRowWithMessageObject(messageObject3, true);
                    }
                }
            }
            if (messageObject == null) {
                this.replyMessageOwners.remove(i);
            }
        }
    }

    private void rotateMotionBackgroundDrawable() {
        MotionBackgroundDrawable motionBackgroundDrawable;
        Drawable wallpaperDrawable = this.themeDelegate.getWallpaperDrawable();
        View view = this.fragmentView;
        if (view != null) {
            wallpaperDrawable = ((SizeNotifierFrameLayout) view).getBackgroundImage();
        }
        if (wallpaperDrawable instanceof MotionBackgroundDrawable) {
            ((MotionBackgroundDrawable) wallpaperDrawable).switchToNextPosition();
        }
        Drawable themedDrawable = getThemedDrawable("drawableMsgOut");
        if ((themedDrawable instanceof Theme.MessageDrawable) && (motionBackgroundDrawable = ((Theme.MessageDrawable) themedDrawable).getMotionBackgroundDrawable()) != null) {
            motionBackgroundDrawable.switchToNextPosition();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v61, resolved type: java.util.Calendar */
    /* JADX DEBUG: Multi-variable search result rejected for r4v32, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v13, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r6v15 */
    /* JADX WARN: Type inference failed for: r6v16 */
    /* JADX WARN: Type inference failed for: r6v17 */
    /* JADX WARN: Type inference failed for: r4v33 */
    /* JADX WARN: Type inference failed for: r4v34 */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x0657  */
    /* JADX WARNING: Removed duplicated region for block: B:436:0x0701  */
    /* JADX WARNING: Removed duplicated region for block: B:439:0x070b  */
    /* JADX WARNING: Removed duplicated region for block: B:458:0x0764  */
    /* JADX WARNING: Removed duplicated region for block: B:552:0x099d  */
    /* JADX WARNING: Removed duplicated region for block: B:559:0x09ba  */
    /* JADX WARNING: Removed duplicated region for block: B:562:0x09c4  */
    /* JADX WARNING: Removed duplicated region for block: B:574:0x09e7  */
    /* JADX WARNING: Removed duplicated region for block: B:581:0x0a00  */
    /* JADX WARNING: Removed duplicated region for block: B:711:0x074b A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processNewMessages(java.util.ArrayList<org.telegram.messenger.MessageObject> r30) {
        /*
        // Method dump skipped, instructions count: 3079
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.processNewMessages(java.util.ArrayList):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processNewMessages$126(int i) {
        scrollToMessageId(i, 0, false, 0, true, 0);
    }

    private int getSponsoredMessagesCount() {
        int i = 0;
        while (i < this.messages.size() && this.messages.get(i).isSponsored()) {
            i++;
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b5, code lost:
        if (r9 == r39.groupedMessagesMap.get(r6.getGroupId())) goto L_0x00b7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x025b  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0276  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x02bd  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x02ca  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0342  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x0409  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x040c  */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x049e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:233:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x016f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processDeletedMessages(java.util.ArrayList<java.lang.Integer> r40, long r41) {
        /*
        // Method dump skipped, instructions count: 1187
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.processDeletedMessages(java.util.ArrayList, long):void");
    }

    private void replaceMessageObjects(ArrayList<MessageObject> arrayList, int i, boolean z) {
        ChatActivityAdapter chatActivityAdapter;
        int i2;
        MessageObject.GroupedMessages groupedMessages;
        int indexOf;
        ArrayList<TLRPC$PhotoSize> arrayList2;
        MessageObject messageObject;
        TLRPC$User tLRPC$User = null;
        LongSparseArray longSparseArray = null;
        int i3 = 0;
        while (i3 < arrayList.size()) {
            MessageObject messageObject2 = arrayList.get(i3);
            if (this.pinnedMessageObjects.get(Integer.valueOf(messageObject2.getId())) != null) {
                this.pinnedMessageObjects.put(Integer.valueOf(messageObject2.getId()), messageObject2);
            }
            MessageObject messageObject3 = this.messagesDict[i].get(messageObject2.getId());
            if (this.pinnedMessageObjects.containsKey(Integer.valueOf(messageObject2.getId()))) {
                this.pinnedMessageObjects.put(Integer.valueOf(messageObject2.getId()), messageObject2);
                if (messageObject2.getId() == this.currentPinnedMessageId) {
                    updatePinnedMessageView(true);
                }
            }
            if (i == 0 && this.repliesMessagesDict.indexOfKey(messageObject2.getId()) >= 0) {
                this.repliesMessagesDict.put(messageObject2.getId(), messageObject2);
            }
            if (messageObject3 != null && (!z || messageObject3.messageOwner.date == messageObject2.messageOwner.date)) {
                if (z) {
                    arrayList.remove(i3);
                    i3--;
                }
                addToPolls(messageObject2, messageObject3);
                if (messageObject2.type >= 0) {
                    MessageObject messageObject4 = messageObject3.replyMessageObject;
                    if (messageObject4 != null) {
                        messageObject2.replyMessageObject = messageObject4;
                        TLRPC$MessageAction tLRPC$MessageAction = messageObject2.messageOwner.action;
                        if (tLRPC$MessageAction instanceof TLRPC$TL_messageActionGameScore) {
                            messageObject2.generateGameMessageText(tLRPC$User);
                        } else if (tLRPC$MessageAction instanceof TLRPC$TL_messageActionPaymentSent) {
                            messageObject2.generatePaymentSentMessageText(tLRPC$User);
                        }
                    }
                    if (!messageObject3.isEditing()) {
                        if (messageObject3.getFileName().equals(messageObject2.getFileName())) {
                            messageObject2.messageOwner.attachPath = messageObject3.messageOwner.attachPath;
                            messageObject2.attachPathExists = messageObject3.attachPathExists;
                            messageObject2.mediaExists = messageObject3.mediaExists;
                        } else {
                            messageObject2.checkMediaExistance();
                        }
                    }
                    this.messagesDict[i].put(messageObject3.getId(), messageObject2);
                } else {
                    this.messagesDict[i].remove(messageObject3.getId());
                }
                int indexOf2 = this.messages.indexOf(messageObject3);
                if (indexOf2 >= 0) {
                    ArrayList<MessageObject> arrayList3 = this.messagesByDays.get(messageObject3.dateKey);
                    int indexOf3 = arrayList3 != null ? arrayList3.indexOf(messageObject3) : -1;
                    if (!(messageObject3.getGroupId() == 0 || (groupedMessages = this.groupedMessagesMap.get(messageObject3.getGroupId())) == null || (indexOf = groupedMessages.messages.indexOf(messageObject3)) < 0)) {
                        if (messageObject3.getGroupId() != messageObject2.getGroupId()) {
                            this.groupedMessagesMap.put(messageObject2.getGroupId(), groupedMessages);
                        }
                        if (messageObject2.isMusic() || messageObject2.isDocument() || ((arrayList2 = messageObject2.photoThumbs) != null && !arrayList2.isEmpty())) {
                            groupedMessages.messages.set(indexOf, messageObject2);
                            messageObject3 = messageObject3;
                            MessageObject.GroupedMessagePosition remove = groupedMessages.positions.remove(messageObject3);
                            if (remove != null) {
                                groupedMessages.positions.put(messageObject2, remove);
                            }
                            if (longSparseArray == null) {
                                longSparseArray = new LongSparseArray();
                            }
                            longSparseArray.put(groupedMessages.groupId, groupedMessages);
                        } else {
                            if (longSparseArray == null) {
                                longSparseArray = new LongSparseArray();
                            }
                            longSparseArray.put(groupedMessages.groupId, groupedMessages);
                            if (indexOf <= 0 || indexOf >= groupedMessages.messages.size() - 1) {
                                messageObject = messageObject3;
                            } else {
                                MessageObject.GroupedMessages groupedMessages2 = new MessageObject.GroupedMessages();
                                messageObject = messageObject3;
                                groupedMessages2.groupId = Utilities.random.nextLong();
                                ArrayList<MessageObject> arrayList4 = groupedMessages2.messages;
                                ArrayList<MessageObject> arrayList5 = groupedMessages.messages;
                                int i4 = indexOf + 1;
                                arrayList4.addAll(arrayList5.subList(i4, arrayList5.size()));
                                for (int i5 = 0; i5 < groupedMessages2.messages.size(); i5++) {
                                    groupedMessages2.messages.get(i5).localGroupId = groupedMessages2.groupId;
                                    groupedMessages.messages.remove(i4);
                                }
                                longSparseArray.put(groupedMessages2.groupId, groupedMessages2);
                                this.groupedMessagesMap.put(groupedMessages2.groupId, groupedMessages2);
                            }
                            groupedMessages.messages.remove(indexOf);
                            messageObject3 = messageObject;
                        }
                    }
                    if (messageObject2.type >= 0) {
                        messageObject2.stableId = messageObject3.stableId;
                        messageObject2.messageOwner.premiumEffectWasPlayed = messageObject3.messageOwner.premiumEffectWasPlayed;
                        messageObject2.forcePlayEffect = messageObject3.forcePlayEffect;
                        this.messages.set(indexOf2, messageObject2);
                        ChatActivityAdapter chatActivityAdapter2 = this.chatAdapter;
                        if (chatActivityAdapter2 != null) {
                            chatActivityAdapter2.updateRowAtPosition(chatActivityAdapter2.messagesStartRow + indexOf2);
                        }
                        if (indexOf3 >= 0) {
                            arrayList3.set(indexOf3, messageObject2);
                        }
                    } else {
                        this.messages.remove(indexOf2);
                        ChatActivityAdapter chatActivityAdapter3 = this.chatAdapter;
                        if (chatActivityAdapter3 != null) {
                            chatActivityAdapter3.notifyItemRemoved(chatActivityAdapter3.messagesStartRow + indexOf2);
                        }
                        if (indexOf3 >= 0) {
                            arrayList3.remove(indexOf3);
                            if (arrayList3.isEmpty()) {
                                this.messagesByDays.remove(messageObject3.dateKey);
                                this.messages.remove(indexOf2);
                                int i6 = this.chatAdapter.loadingUpRow;
                                int i7 = this.chatAdapter.loadingDownRow;
                                ChatActivityAdapter chatActivityAdapter4 = this.chatAdapter;
                                chatActivityAdapter4.notifyItemRemoved(chatActivityAdapter4.messagesStartRow + indexOf2);
                                if (this.messages.isEmpty()) {
                                    if (i6 >= 0) {
                                        i2 = 0;
                                        this.chatAdapter.notifyItemRemoved(0);
                                    } else {
                                        i2 = 0;
                                    }
                                    if (i7 >= 0) {
                                        this.chatAdapter.notifyItemRemoved(i2);
                                    }
                                    updateReplyMessageOwners(messageObject3.getId(), messageObject2);
                                }
                            }
                        }
                    }
                }
                updateReplyMessageOwners(messageObject3.getId(), messageObject2);
            }
            i3++;
            tLRPC$User = null;
        }
        if (longSparseArray != null) {
            for (int i8 = 0; i8 < longSparseArray.size(); i8++) {
                MessageObject.GroupedMessages groupedMessages3 = (MessageObject.GroupedMessages) longSparseArray.valueAt(i8);
                if (groupedMessages3.messages.isEmpty()) {
                    this.groupedMessagesMap.remove(groupedMessages3.groupId);
                } else {
                    groupedMessages3.calculate();
                    ArrayList<MessageObject> arrayList6 = groupedMessages3.messages;
                    int indexOf4 = this.messages.indexOf(arrayList6.get(arrayList6.size() - 1));
                    if (indexOf4 >= 0 && (chatActivityAdapter = this.chatAdapter) != null) {
                        chatActivityAdapter.notifyItemRangeChanged(indexOf4 + chatActivityAdapter.messagesStartRow, groupedMessages3.messages.size());
                        ChatListItemAnimator chatListItemAnimator = this.chatListItemAnimator;
                        if (chatListItemAnimator != null) {
                            chatListItemAnimator.groupWillChanged(groupedMessages3);
                        }
                    }
                }
            }
        }
    }

    private void migrateToNewChat(MessageObject messageObject) {
        BaseFragment baseFragment;
        ActionBarLayout actionBarLayout = this.parentLayout;
        if (actionBarLayout != null) {
            long j = messageObject.messageOwner.action.channel_id;
            if (actionBarLayout.fragmentsStack.size() > 0) {
                ArrayList<BaseFragment> arrayList = this.parentLayout.fragmentsStack;
                baseFragment = arrayList.get(arrayList.size() - 1);
            } else {
                baseFragment = null;
            }
            int indexOf = this.parentLayout.fragmentsStack.indexOf(this);
            ActionBarLayout actionBarLayout2 = this.parentLayout;
            if (indexOf <= 0 || (baseFragment instanceof ChatActivity) || (baseFragment instanceof ProfileActivity) || !this.currentChat.creator) {
                AndroidUtilities.runOnUIThread(new Runnable(baseFragment, messageObject, actionBarLayout2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda195
                    public final /* synthetic */ BaseFragment f$1;
                    public final /* synthetic */ MessageObject f$2;
                    public final /* synthetic */ ActionBarLayout f$3;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ChatActivity.this.lambda$migrateToNewChat$127(this.f$1, this.f$2, this.f$3);
                    }
                });
            } else {
                int size = actionBarLayout2.fragmentsStack.size() - 1;
                while (indexOf < size) {
                    BaseFragment baseFragment2 = actionBarLayout2.fragmentsStack.get(indexOf);
                    if (baseFragment2 instanceof ChatActivity) {
                        Bundle bundle = new Bundle();
                        bundle.putLong("chat_id", j);
                        actionBarLayout2.addFragmentToStack(new ChatActivity(bundle), indexOf);
                        baseFragment2.removeSelfFromStack();
                    } else if (baseFragment2 instanceof ProfileActivity) {
                        Bundle bundle2 = new Bundle();
                        bundle2.putLong("chat_id", j);
                        actionBarLayout2.addFragmentToStack(new ProfileActivity(bundle2), indexOf);
                        baseFragment2.removeSelfFromStack();
                    } else if (baseFragment2 instanceof ChatEditActivity) {
                        Bundle bundle3 = new Bundle();
                        bundle3.putLong("chat_id", j);
                        actionBarLayout2.addFragmentToStack(new ChatEditActivity(bundle3), indexOf);
                        baseFragment2.removeSelfFromStack();
                    } else if (baseFragment2 instanceof ChatUsersActivity) {
                        if (!((ChatUsersActivity) baseFragment2).hasSelectType()) {
                            Bundle arguments = baseFragment2.getArguments();
                            arguments.putLong("chat_id", j);
                            actionBarLayout2.addFragmentToStack(new ChatUsersActivity(arguments), indexOf);
                        }
                        baseFragment2.removeSelfFromStack();
                    }
                    indexOf++;
                }
            }
            AndroidUtilities.runOnUIThread(new Runnable(j) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda169
                public final /* synthetic */ long f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$migrateToNewChat$128(this.f$1);
                }
            }, 1000);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$migrateToNewChat$127(BaseFragment baseFragment, MessageObject messageObject, ActionBarLayout actionBarLayout) {
        if (baseFragment instanceof NotificationCenter.NotificationCenterDelegate) {
            getNotificationCenter().removeObserver((NotificationCenter.NotificationCenterDelegate) baseFragment, NotificationCenter.closeChats);
        }
        getNotificationCenter().postNotificationName(NotificationCenter.closeChats, new Object[0]);
        Bundle bundle = new Bundle();
        bundle.putLong("chat_id", messageObject.messageOwner.action.channel_id);
        actionBarLayout.addFragmentToStack(new ChatActivity(bundle), actionBarLayout.fragmentsStack.size() - 1);
        baseFragment.finishFragment();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$migrateToNewChat$128(long j) {
        getMessagesController().loadFullChat(j, 0, true);
    }

    private void addToPolls(MessageObject messageObject, MessageObject messageObject2) {
        long pollId = messageObject.getPollId();
        if (pollId != 0) {
            ArrayList<MessageObject> arrayList = this.polls.get(pollId);
            if (arrayList == null) {
                arrayList = new ArrayList<>();
                this.polls.put(pollId, arrayList);
            }
            arrayList.add(messageObject);
            if (messageObject2 != null) {
                arrayList.remove(messageObject2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showInfoHint(MessageObject messageObject, CharSequence charSequence, int i) {
        if (this.topUndoView != null) {
            ChatActivity$$ExternalSyntheticLambda163 chatActivity$$ExternalSyntheticLambda163 = new Runnable(i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda163
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$showInfoHint$129(this.f$1);
                }
            };
            this.topUndoView.showWithAction(0, 18, charSequence, chatActivity$$ExternalSyntheticLambda163, chatActivity$$ExternalSyntheticLambda163);
            this.hintMessageObject = messageObject;
            this.hintMessageType = i;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showInfoHint$129(int i) {
        ChatMessageCell chatMessageCell;
        MessageObject messageObject;
        RecyclerListView recyclerListView = this.chatListView;
        if (recyclerListView != null) {
            int childCount = recyclerListView.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = this.chatListView.getChildAt(i2);
                if ((childAt instanceof ChatMessageCell) && (messageObject = (chatMessageCell = (ChatMessageCell) childAt).getMessageObject()) != null && messageObject.equals(this.hintMessageObject)) {
                    chatMessageCell.showHintButton(true, true, i);
                }
            }
        }
        this.hintMessageObject = null;
    }

    /* access modifiers changed from: private */
    public void showPollSolution(MessageObject messageObject, TLRPC$PollResults tLRPC$PollResults) {
        CharSequence charSequence;
        if (tLRPC$PollResults != null && !TextUtils.isEmpty(tLRPC$PollResults.solution)) {
            if (!tLRPC$PollResults.solution_entities.isEmpty()) {
                charSequence = new SpannableStringBuilder(tLRPC$PollResults.solution);
                MessageObject.addEntitiesToText(charSequence, tLRPC$PollResults.solution_entities, false, true, true, false);
            } else {
                charSequence = tLRPC$PollResults.solution;
            }
            showInfoHint(messageObject, charSequence, 0);
        }
    }

    /* access modifiers changed from: private */
    public void updateSearchButtons(int i, int i2, int i3) {
        ImageView imageView = this.searchUpButton;
        if (imageView != null) {
            imageView.setEnabled((i & 1) != 0);
            this.searchDownButton.setEnabled((i & 2) != 0);
            ImageView imageView2 = this.searchUpButton;
            float f = 1.0f;
            imageView2.setAlpha(imageView2.isEnabled() ? 1.0f : 0.5f);
            ImageView imageView3 = this.searchDownButton;
            if (!imageView3.isEnabled()) {
                f = 0.5f;
            }
            imageView3.setAlpha(f);
            if (i3 < 0) {
                this.searchCountText.setCount("", 0, false);
            } else if (i3 == 0) {
                this.searchCountText.setCount(LocaleController.getString("NoResult", R.string.NoResult), 0, false);
            } else {
                int i4 = i2 + 1;
                this.searchCountText.setCount(LocaleController.formatString("OfCounted", R.string.OfCounted, Integer.valueOf(i4), Integer.valueOf(i3)), i4, true);
            }
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public boolean needDelayOpenAnimation() {
        if (this.chatMode != 1 && getParentLayout().fragmentsStack.size() > 1) {
            BaseFragment baseFragment = getParentLayout().fragmentsStack.get(getParentLayout().fragmentsStack.size() - 2);
            if ((baseFragment instanceof ChatActivity) && ((ChatActivity) baseFragment).isKeyboardVisible()) {
                return false;
            }
        }
        return this.firstLoading;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected void onBecomeFullyVisible() {
        this.isFullyVisible = true;
        super.onBecomeFullyVisible();
        if (this.showCloseChatDialogLater) {
            showDialog(this.closeChatDialog);
        }
        ActionBarLayout actionBarLayout = this.parentLayout;
        if (actionBarLayout != null && actionBarLayout.getDrawerLayoutContainer() != null) {
            this.parentLayout.getDrawerLayoutContainer().setBehindKeyboardColor(getThemedColor("windowBackgroundWhite"));
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected void onBecomeFullyHidden() {
        this.isFullyVisible = false;
        hideUndoViews();
        ActionBarLayout actionBarLayout = this.parentLayout;
        if (actionBarLayout != null && actionBarLayout.getDrawerLayoutContainer() != null) {
            this.parentLayout.getDrawerLayoutContainer().setBehindKeyboardColor(Theme.getColor("windowBackgroundWhite"));
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void saveKeyboardPositionBeforeTransition() {
        Runnable runnable = this.cancelFixedPositionRunnable;
        if (runnable != null) {
            AndroidUtilities.cancelRunOnUIThread(runnable);
        }
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView == null || this.contentView == null || chatActivityEnterView.getAdjustPanLayoutHelper() == null || this.chatActivityEnterView.getAdjustPanLayoutHelper().animationInProgress()) {
            this.fixedKeyboardHeight = -1;
        } else {
            this.fixedKeyboardHeight = this.contentView.getKeyboardHeight();
        }
    }

    public void removeKeyboardPositionBeforeTransition() {
        if (this.fixedKeyboardHeight > 0) {
            ChatActivity$$ExternalSyntheticLambda143 chatActivity$$ExternalSyntheticLambda143 = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda143
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$removeKeyboardPositionBeforeTransition$130();
                }
            };
            this.cancelFixedPositionRunnable = chatActivity$$ExternalSyntheticLambda143;
            AndroidUtilities.runOnUIThread(chatActivity$$ExternalSyntheticLambda143, 200);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$removeKeyboardPositionBeforeTransition$130() {
        this.cancelFixedPositionRunnable = null;
        this.fixedKeyboardHeight = -1;
        View view = this.fragmentView;
        if (view != null) {
            view.requestLayout();
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void onTransitionAnimationStart(boolean z, boolean z2) {
        int[] iArr;
        super.onTransitionAnimationStart(z, z2);
        if (z) {
            if (!this.fragmentOpened) {
                this.fragmentOpened = true;
                updateMessagesVisiblePart(false);
            }
            iArr = this.transitionAnimationIndex == 0 ? new int[]{NotificationCenter.dialogsNeedReload, NotificationCenter.closeChats, NotificationCenter.botKeyboardDidLoad, NotificationCenter.needDeleteDialog, NotificationCenter.messagesDidLoad} : new int[]{NotificationCenter.dialogsNeedReload, NotificationCenter.closeChats, NotificationCenter.botKeyboardDidLoad, NotificationCenter.needDeleteDialog};
            this.openAnimationEnded = false;
            if (!z2) {
                this.openAnimationStartTime = SystemClock.elapsedRealtime();
            }
        } else {
            iArr = UserObject.isUserSelf(this.currentUser) ? new int[]{NotificationCenter.dialogsNeedReload, NotificationCenter.closeChats, NotificationCenter.botKeyboardDidLoad, NotificationCenter.needDeleteDialog, NotificationCenter.mediaDidLoad} : null;
            ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
            if (chatActivityEnterView != null) {
                chatActivityEnterView.onBeginHide();
            }
        }
        checkShowBlur(true);
        this.transitionAnimationIndex = getNotificationCenter().setAnimationInProgress(this.transitionAnimationIndex, iArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:92:0x018e  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0196  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01a4  */
    @Override // org.telegram.ui.ActionBar.BaseFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onTransitionAnimationEnd(boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 585
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.onTransitionAnimationEnd(boolean, boolean):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$137(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda183
            public final /* synthetic */ TLObject f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$onTransitionAnimationEnd$136(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$136(TLObject tLObject) {
        if (tLObject != null) {
            TLRPC$TL_contacts_resolvedPeer tLRPC$TL_contacts_resolvedPeer = (TLRPC$TL_contacts_resolvedPeer) tLObject;
            if (!tLRPC$TL_contacts_resolvedPeer.users.isEmpty()) {
                TLRPC$User tLRPC$User = tLRPC$TL_contacts_resolvedPeer.users.get(0);
                if (tLRPC$User.bot && tLRPC$User.bot_attach_menu) {
                    TLRPC$TL_messages_getAttachMenuBot tLRPC$TL_messages_getAttachMenuBot = new TLRPC$TL_messages_getAttachMenuBot();
                    tLRPC$TL_messages_getAttachMenuBot.bot = MessagesController.getInstance(this.currentAccount).getInputUser(tLRPC$User.id);
                    ConnectionsManager.getInstance(this.currentAccount).sendRequest(tLRPC$TL_messages_getAttachMenuBot, new RequestDelegate(tLRPC$User) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda229
                        public final /* synthetic */ TLRPC$User f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // org.telegram.tgnet.RequestDelegate
                        public final void run(TLObject tLObject2, TLRPC$TL_error tLRPC$TL_error) {
                            ChatActivity.this.lambda$onTransitionAnimationEnd$135(this.f$1, tLObject2, tLRPC$TL_error);
                        }
                    });
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$135(TLRPC$User tLRPC$User, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLObject, tLRPC$User) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda188
            public final /* synthetic */ TLObject f$1;
            public final /* synthetic */ TLRPC$User f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$onTransitionAnimationEnd$134(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$134(TLObject tLObject, TLRPC$User tLRPC$User) {
        if (tLObject instanceof TLRPC$TL_attachMenuBotsBot) {
            TLRPC$TL_attachMenuBotsBot tLRPC$TL_attachMenuBotsBot = (TLRPC$TL_attachMenuBotsBot) tLObject;
            MessagesController.getInstance(this.currentAccount).putUsers(tLRPC$TL_attachMenuBotsBot.users, false);
            TLRPC$TL_attachMenuBot tLRPC$TL_attachMenuBot = tLRPC$TL_attachMenuBotsBot.bot;
            if (!MediaDataController.canShowAttachMenuBot(tLRPC$TL_attachMenuBot, getCurrentUser() != null ? getCurrentUser() : getCurrentChat())) {
                TLRPC$User tLRPC$User2 = this.currentUser;
                if (tLRPC$User2 != null && tLRPC$User2.bot && tLRPC$User.id == tLRPC$TL_attachMenuBot.bot_id) {
                    BulletinFactory.of(this).createErrorBulletin(LocaleController.getString((int) R.string.BotCantOpenAttachMenuSameBot)).show();
                } else if (tLRPC$User2 != null && tLRPC$User2.bot && tLRPC$User.id != tLRPC$TL_attachMenuBot.bot_id) {
                    BulletinFactory.of(this).createErrorBulletin(LocaleController.getString((int) R.string.BotCantOpenAttachMenuBot)).show();
                } else if (tLRPC$User2 == null || tLRPC$User2.bot) {
                    TLRPC$Chat tLRPC$Chat = this.currentChat;
                    if (tLRPC$Chat == null || ChatObject.isChannelAndNotMegaGroup(tLRPC$Chat)) {
                        TLRPC$Chat tLRPC$Chat2 = this.currentChat;
                        if (tLRPC$Chat2 != null && ChatObject.isChannelAndNotMegaGroup(tLRPC$Chat2)) {
                            BulletinFactory.of(this).createErrorBulletin(LocaleController.getString((int) R.string.BotCantOpenAttachMenuChannel)).show();
                            return;
                        }
                        return;
                    }
                    BulletinFactory.of(this).createErrorBulletin(LocaleController.getString((int) R.string.BotCantOpenAttachMenuGroup)).show();
                } else {
                    BulletinFactory.of(this).createErrorBulletin(LocaleController.getString((int) R.string.BotCantOpenAttachMenuUser)).show();
                }
            } else if (!tLRPC$TL_attachMenuBot.inactive) {
                openAttachBotLayout(tLRPC$User.id, this.attachMenuBotStartCommand);
            } else {
                AttachBotIntroTopView attachBotIntroTopView = new AttachBotIntroTopView(getParentActivity());
                attachBotIntroTopView.setColor(Theme.getColor("chat_attachContactIcon"));
                attachBotIntroTopView.setBackgroundColor(Theme.getColor("dialogTopBackground"));
                attachBotIntroTopView.setAttachBot(tLRPC$TL_attachMenuBot);
                new AlertDialog.Builder(getParentActivity()).setTopView(attachBotIntroTopView).setMessage(AndroidUtilities.replaceTags(LocaleController.formatString("BotRequestAttachPermission", R.string.BotRequestAttachPermission, UserObject.getUserName(tLRPC$User)))).setPositiveButton(LocaleController.getString((int) R.string.BotAddToMenu), new DialogInterface.OnClickListener(tLRPC$User) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda42
                    public final /* synthetic */ TLRPC$User f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        ChatActivity.this.lambda$onTransitionAnimationEnd$133(this.f$1, dialogInterface, i);
                    }
                }).setNegativeButton(LocaleController.getString((int) R.string.Cancel), null).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$133(TLRPC$User tLRPC$User, DialogInterface dialogInterface, int i) {
        TLRPC$TL_messages_toggleBotInAttachMenu tLRPC$TL_messages_toggleBotInAttachMenu = new TLRPC$TL_messages_toggleBotInAttachMenu();
        tLRPC$TL_messages_toggleBotInAttachMenu.bot = MessagesController.getInstance(this.currentAccount).getInputUser(tLRPC$User.id);
        tLRPC$TL_messages_toggleBotInAttachMenu.enabled = true;
        ConnectionsManager.getInstance(this.currentAccount).sendRequest(tLRPC$TL_messages_toggleBotInAttachMenu, new RequestDelegate(tLRPC$User) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda228
            public final /* synthetic */ TLRPC$User f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.telegram.tgnet.RequestDelegate
            public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                ChatActivity.this.lambda$onTransitionAnimationEnd$132(this.f$1, tLObject, tLRPC$TL_error);
            }
        }, 66);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$132(TLRPC$User tLRPC$User, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLRPC$TL_error, tLRPC$User) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda192
            public final /* synthetic */ TLRPC$TL_error f$1;
            public final /* synthetic */ TLRPC$User f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$onTransitionAnimationEnd$131(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onTransitionAnimationEnd$131(TLRPC$TL_error tLRPC$TL_error, TLRPC$User tLRPC$User) {
        if (tLRPC$TL_error == null) {
            MediaDataController.getInstance(this.currentAccount).loadAttachMenuBots(false, true);
            openAttachBotLayout(tLRPC$User.id, this.attachMenuBotStartCommand);
        }
    }

    public void openAttachBotLayout(long j, String str) {
        openAttachMenu();
        this.chatAttachAlert.showBotLayout(j, str);
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected void onDialogDismiss(Dialog dialog) {
        Dialog dialog2 = this.closeChatDialog;
        if (dialog2 != null && dialog == dialog2) {
            getMessagesController().deleteDialog(this.dialog_id, 0);
            ActionBarLayout actionBarLayout = this.parentLayout;
            if (actionBarLayout != null && !actionBarLayout.fragmentsStack.isEmpty()) {
                ArrayList<BaseFragment> arrayList = this.parentLayout.fragmentsStack;
                if (arrayList.get(arrayList.size() - 1) != this) {
                    ArrayList<BaseFragment> arrayList2 = this.parentLayout.fragmentsStack;
                    removeSelfFromStack();
                    arrayList2.get(arrayList2.size() - 1).finishFragment();
                    return;
                }
            }
            finishFragment();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r4.findItem(16908321) != null) goto L_0x0034;
     */
    @Override // org.telegram.ui.ActionBar.BaseFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean extendActionMode(android.view.Menu r4) {
        /*
            r3 = this;
            boolean r0 = org.telegram.ui.PhotoViewer.hasInstance()
            r1 = 16908321(0x1020021, float:2.3877321E-38)
            r2 = 1
            if (r0 == 0) goto L_0x0025
            org.telegram.ui.PhotoViewer r0 = org.telegram.ui.PhotoViewer.getInstance()
            boolean r0 = r0.isVisible()
            if (r0 == 0) goto L_0x0025
            org.telegram.ui.PhotoViewer r0 = org.telegram.ui.PhotoViewer.getInstance()
            int r0 = r0.getSelectiongLength()
            if (r0 == 0) goto L_0x0024
            android.view.MenuItem r0 = r4.findItem(r1)
            if (r0 != 0) goto L_0x0034
        L_0x0024:
            return r2
        L_0x0025:
            org.telegram.ui.Components.ChatActivityEnterView r0 = r3.chatActivityEnterView
            int r0 = r0.getSelectionLength()
            if (r0 == 0) goto L_0x0037
            android.view.MenuItem r0 = r4.findItem(r1)
            if (r0 != 0) goto L_0x0034
            goto L_0x0037
        L_0x0034:
            r3.fillActionModeMenu(r4)
        L_0x0037:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.extendActionMode(android.view.Menu):boolean");
    }

    public void fillActionModeMenu(Menu menu) {
        if (menu.findItem(R.id.menu_bold) == null) {
            if (Build.VERSION.SDK_INT >= 23) {
                menu.removeItem(16908341);
            }
            menu.add(R.id.menu_groupbolditalic, R.id.menu_spoiler, 6, LocaleController.getString("Spoiler", R.string.Spoiler));
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(LocaleController.getString("Bold", R.string.Bold));
            spannableStringBuilder.setSpan(new TypefaceSpan(AndroidUtilities.getTypeface(AndroidUtilities.TYPEFACE_ROBOTO_MEDIUM)), 0, spannableStringBuilder.length(), 33);
            menu.add(R.id.menu_groupbolditalic, R.id.menu_bold, 7, spannableStringBuilder);
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(LocaleController.getString("Italic", R.string.Italic));
            spannableStringBuilder2.setSpan(new TypefaceSpan(AndroidUtilities.getTypeface("fonts/ritalic.ttf")), 0, spannableStringBuilder2.length(), 33);
            menu.add(R.id.menu_groupbolditalic, R.id.menu_italic, 8, spannableStringBuilder2);
            SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(LocaleController.getString("Mono", R.string.Mono));
            spannableStringBuilder3.setSpan(new TypefaceSpan(Typeface.MONOSPACE), 0, spannableStringBuilder3.length(), 33);
            int i = 10;
            menu.add(R.id.menu_groupbolditalic, R.id.menu_mono, 9, spannableStringBuilder3);
            TLRPC$EncryptedChat tLRPC$EncryptedChat = this.currentEncryptedChat;
            if (tLRPC$EncryptedChat == null || AndroidUtilities.getPeerLayerVersion(tLRPC$EncryptedChat.layer) >= 101) {
                SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(LocaleController.getString("Strike", R.string.Strike));
                TextStyleSpan.TextStyleRun textStyleRun = new TextStyleSpan.TextStyleRun();
                textStyleRun.flags |= 8;
                spannableStringBuilder4.setSpan(new TextStyleSpan(textStyleRun), 0, spannableStringBuilder4.length(), 33);
                menu.add(R.id.menu_groupbolditalic, R.id.menu_strike, 10, spannableStringBuilder4);
                SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(LocaleController.getString("Underline", R.string.Underline));
                TextStyleSpan.TextStyleRun textStyleRun2 = new TextStyleSpan.TextStyleRun();
                textStyleRun2.flags |= 16;
                spannableStringBuilder5.setSpan(new TextStyleSpan(textStyleRun2), 0, spannableStringBuilder5.length(), 33);
                i = 12;
                menu.add(R.id.menu_groupbolditalic, R.id.menu_underline, 11, spannableStringBuilder5);
            }
            menu.add(R.id.menu_groupbolditalic, R.id.menu_link, i, LocaleController.getString("CreateLink", R.string.CreateLink));
            menu.add(R.id.menu_groupbolditalic, R.id.menu_regular, i + 1, LocaleController.getString("Regular", R.string.Regular));
        }
    }

    /* access modifiers changed from: private */
    public void updateScheduledInterface(boolean z) {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.updateScheduleButton(z);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0171  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void updateBottomOverlay() {
        /*
        // Method dump skipped, instructions count: 1153
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.updateBottomOverlay():void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateBottomOverlay$138(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.searchExpandProgress = floatValue;
        this.chatListView.setTranslationY(floatValue * ((float) (this.chatActivityEnterView.getMeasuredHeight() - this.searchContainer.getMeasuredHeight())));
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        chatActivityEnterView.setChatSearchExpandOffset(this.searchExpandProgress * ((float) (chatActivityEnterView.getMeasuredHeight() - this.searchContainer.getMeasuredHeight())));
        invalidateChatListViewTopPadding();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateBottomOverlay$139(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.searchExpandProgress = floatValue;
        this.chatListView.setTranslationY(floatValue * ((float) (this.chatActivityEnterView.getMeasuredHeight() - this.searchContainer.getMeasuredHeight())));
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        chatActivityEnterView.setChatSearchExpandOffset(this.searchExpandProgress * ((float) (chatActivityEnterView.getMeasuredHeight() - this.searchContainer.getMeasuredHeight())));
        invalidateChatListViewTopPadding();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateBottomOverlay$140() {
        this.chatActivityEnterView.openKeyboard();
    }

    public void updateReplyMessageHeader(boolean z) {
        String str;
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        if (!(chatAvatarContainer == null || this.threadMessageId == 0)) {
            if (!this.isComments) {
                chatAvatarContainer.setTitle(LocaleController.formatPluralString("Replies", this.threadMessageObject.getRepliesCount(), new Object[0]));
            } else if (this.threadMessageObject.hasReplies()) {
                this.avatarContainer.setTitle(LocaleController.formatPluralString("Comments", this.threadMessageObject.getRepliesCount(), new Object[0]));
            } else {
                this.avatarContainer.setTitle(LocaleController.getString("CommentsTitle", R.string.CommentsTitle));
            }
        }
        if (this.replyMessageHeaderObject != null) {
            if (this.threadMessageObject.getRepliesCount() != 0) {
                str = LocaleController.getString("DiscussionStarted", R.string.DiscussionStarted);
            } else if (this.isComments) {
                str = LocaleController.getString("NoComments", R.string.NoComments);
            } else {
                str = LocaleController.getString("NoReplies", R.string.NoReplies);
            }
            MessageObject messageObject = this.replyMessageHeaderObject;
            messageObject.messageOwner.message = str;
            messageObject.messageText = str;
            if (z) {
                this.chatAdapter.updateRowWithMessageObject(messageObject, true);
            }
        }
    }

    public void showAlert(String str, String str2) {
        FrameLayout frameLayout = this.alertView;
        if (frameLayout != null && str != null && str2 != null) {
            if (frameLayout.getTag() != null) {
                this.alertView.setTag(null);
                AnimatorSet animatorSet = this.alertViewAnimator;
                if (animatorSet != null) {
                    animatorSet.cancel();
                    this.alertViewAnimator = null;
                }
                if (this.alertView.getVisibility() != 0) {
                    this.alertViewEnterProgress = 0.0f;
                    invalidateChatListViewTopPadding();
                }
                this.alertView.setVisibility(0);
                this.alertViewAnimator = new AnimatorSet();
                ValueAnimator ofFloat = ValueAnimator.ofFloat(this.alertViewEnterProgress, 1.0f);
                ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda11
                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                        ChatActivity.this.lambda$showAlert$141(valueAnimator);
                    }
                });
                this.alertViewAnimator.playTogether(ofFloat);
                this.alertViewAnimator.setDuration(200L);
                this.alertViewAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.87
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        if (ChatActivity.this.alertViewAnimator != null && ChatActivity.this.alertViewAnimator.equals(animator)) {
                            ChatActivity.this.alertViewEnterProgress = 1.0f;
                            ChatActivity.this.invalidateChatListViewTopPadding();
                            ChatActivity.this.alertViewAnimator = null;
                        }
                    }

                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationCancel(Animator animator) {
                        if (ChatActivity.this.alertViewAnimator != null && ChatActivity.this.alertViewAnimator.equals(animator)) {
                            ChatActivity.this.alertViewAnimator = null;
                        }
                    }
                });
                this.alertViewAnimator.start();
            }
            this.alertNameTextView.setText(str);
            this.alertTextView.setText(Emoji.replaceEmoji(str2.replace('\n', ' '), this.alertTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(14.0f), false));
            Runnable runnable = this.hideAlertViewRunnable;
            if (runnable != null) {
                AndroidUtilities.cancelRunOnUIThread(runnable);
            }
            AnonymousClass88 r5 = new Runnable() { // from class: org.telegram.ui.ChatActivity.88
                @Override // java.lang.Runnable
                public void run() {
                    if (ChatActivity.this.hideAlertViewRunnable == this && ChatActivity.this.alertView.getTag() == null) {
                        ChatActivity.this.alertView.setTag(1);
                        if (ChatActivity.this.alertViewAnimator != null) {
                            ChatActivity.this.alertViewAnimator.cancel();
                            ChatActivity.this.alertViewAnimator = null;
                        }
                        ChatActivity.this.alertViewAnimator = new AnimatorSet();
                        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(ChatActivity.this.alertViewEnterProgress, 0.0f);
                        ofFloat2.addUpdateListener(new ChatActivity$88$$ExternalSyntheticLambda0(this));
                        ChatActivity.this.alertViewAnimator.playTogether(ofFloat2);
                        ChatActivity.this.alertViewAnimator.setDuration(200L);
                        ChatActivity.this.alertViewAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.88.1
                            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                            public void onAnimationEnd(Animator animator) {
                                if (ChatActivity.this.alertViewAnimator != null && ChatActivity.this.alertViewAnimator.equals(animator)) {
                                    ChatActivity.this.alertView.setVisibility(8);
                                    ChatActivity.this.alertViewEnterProgress = 0.0f;
                                    ChatActivity.this.invalidateChatListViewTopPadding();
                                    ChatActivity.this.alertViewAnimator = null;
                                }
                            }

                            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                            public void onAnimationCancel(Animator animator) {
                                if (ChatActivity.this.alertViewAnimator != null && ChatActivity.this.alertViewAnimator.equals(animator)) {
                                    ChatActivity.this.alertViewAnimator = null;
                                }
                            }
                        });
                        ChatActivity.this.alertViewAnimator.start();
                    }
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$run$0(ValueAnimator valueAnimator) {
                    ChatActivity.this.alertViewEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    ChatActivity.this.invalidateChatListViewTopPadding();
                }
            };
            this.hideAlertViewRunnable = r5;
            AndroidUtilities.runOnUIThread(r5, 3000);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showAlert$141(ValueAnimator valueAnimator) {
        this.alertViewEnterProgress = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidateChatListViewTopPadding();
    }

    private boolean hidePinnedMessageView(boolean z) {
        BlurredFrameLayout blurredFrameLayout = this.pinnedMessageView;
        if (blurredFrameLayout == null || blurredFrameLayout.getTag() != null) {
            return false;
        }
        int i = 0;
        while (true) {
            AnimatorSet[] animatorSetArr = this.pinnedNextAnimation;
            if (i >= animatorSetArr.length) {
                break;
            }
            if (animatorSetArr[i] != null) {
                animatorSetArr[i].cancel();
                this.pinnedNextAnimation[i] = null;
            }
            i++;
        }
        this.setPinnedTextTranslationX = false;
        this.pinnedMessageView.setTag(1);
        AnimatorSet animatorSet = this.pinnedMessageViewAnimator;
        if (animatorSet != null) {
            animatorSet.cancel();
            this.pinnedMessageViewAnimator = null;
        }
        if (z) {
            this.pinnedMessageViewAnimator = new AnimatorSet();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(this.pinnedMessageEnterOffset, (float) (-AndroidUtilities.dp(50.0f)));
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda2
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ChatActivity.this.lambda$hidePinnedMessageView$142(valueAnimator);
                }
            });
            this.pinnedMessageViewAnimator.playTogether(ofFloat);
            this.pinnedMessageViewAnimator.setDuration(200L);
            this.pinnedMessageViewAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.89
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    if (ChatActivity.this.pinnedMessageViewAnimator != null && ChatActivity.this.pinnedMessageViewAnimator.equals(animator)) {
                        ChatActivity.this.pinnedMessageView.setVisibility(8);
                        ChatActivity.this.pinnedMessageViewAnimator = null;
                    }
                }

                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationCancel(Animator animator) {
                    if (ChatActivity.this.pinnedMessageViewAnimator != null && ChatActivity.this.pinnedMessageViewAnimator.equals(animator)) {
                        ChatActivity.this.pinnedMessageViewAnimator = null;
                    }
                }
            });
            this.pinnedMessageViewAnimator.start();
        } else {
            this.pinnedMessageEnterOffset = (float) (-AndroidUtilities.dp(50.0f));
            this.pinnedMessageView.setVisibility(8);
            this.chatListView.invalidate();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$hidePinnedMessageView$142(ValueAnimator valueAnimator) {
        this.pinnedMessageEnterOffset = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidateChatListViewTopPadding();
        invalidateMessagesVisiblePart();
        this.chatListView.invalidate();
    }

    /* access modifiers changed from: private */
    public void updatePinnedMessageView(boolean z) {
        updatePinnedMessageView(z, 0);
    }

    private void updatePinnedListButton(boolean z) {
        if (!isThreadChat() && this.pinnedListButton != null) {
            boolean z2 = this.pinnedMessageIds.size() > 1 && !this.pinnedMessageButtonShown;
            boolean z3 = this.pinnedListButton.getTag() != null;
            boolean z4 = this.pinnedProgress.getTag() != null;
            boolean z5 = this.closePinned.getTag() != null;
            final boolean z6 = !z2 && !this.pinnedProgressIsShowing && !this.pinnedMessageButtonShown;
            final boolean z7 = z2 && !this.pinnedProgressIsShowing && !this.pinnedMessageButtonShown;
            final boolean z8 = this.pinnedProgressIsShowing && !this.pinnedMessageButtonShown;
            if (!(z3 == z2 && z4 == z8 && z5 == z6)) {
                AnimatorSet animatorSet = this.pinnedListAnimator;
                if (animatorSet != null) {
                    animatorSet.cancel();
                    this.pinnedListAnimator = null;
                }
                int i = 8;
                int i2 = 4;
                if (z) {
                    if (z2) {
                        this.pinnedListButton.setVisibility(0);
                    } else if (z6) {
                        this.closePinned.setVisibility(0);
                    }
                    if (z8) {
                        this.pinnedProgress.setVisibility(0);
                        this.pinnedProgress.setAlpha(0.0f);
                        this.pinnedProgress.setScaleX(0.4f);
                        this.pinnedProgress.setScaleY(0.4f);
                    }
                    AnimatorSet animatorSet2 = new AnimatorSet();
                    this.pinnedListAnimator = animatorSet2;
                    Animator[] animatorArr = new Animator[9];
                    ImageView imageView = this.pinnedListButton;
                    Property property = View.ALPHA;
                    float[] fArr = new float[1];
                    fArr[0] = z7 ? 1.0f : 0.0f;
                    animatorArr[0] = ObjectAnimator.ofFloat(imageView, property, fArr);
                    ImageView imageView2 = this.pinnedListButton;
                    Property property2 = View.SCALE_X;
                    float[] fArr2 = new float[1];
                    fArr2[0] = z7 ? 1.0f : 0.4f;
                    animatorArr[1] = ObjectAnimator.ofFloat(imageView2, property2, fArr2);
                    ImageView imageView3 = this.pinnedListButton;
                    Property property3 = View.SCALE_Y;
                    float[] fArr3 = new float[1];
                    fArr3[0] = z7 ? 1.0f : 0.4f;
                    animatorArr[2] = ObjectAnimator.ofFloat(imageView3, property3, fArr3);
                    ImageView imageView4 = this.closePinned;
                    Property property4 = View.ALPHA;
                    float[] fArr4 = new float[1];
                    fArr4[0] = z6 ? 1.0f : 0.0f;
                    animatorArr[3] = ObjectAnimator.ofFloat(imageView4, property4, fArr4);
                    ImageView imageView5 = this.closePinned;
                    Property property5 = View.SCALE_X;
                    float[] fArr5 = new float[1];
                    fArr5[0] = z6 ? 1.0f : 0.4f;
                    animatorArr[4] = ObjectAnimator.ofFloat(imageView5, property5, fArr5);
                    ImageView imageView6 = this.closePinned;
                    Property property6 = View.SCALE_Y;
                    float[] fArr6 = new float[1];
                    fArr6[0] = z6 ? 1.0f : 0.4f;
                    animatorArr[5] = ObjectAnimator.ofFloat(imageView6, property6, fArr6);
                    RadialProgressView radialProgressView = this.pinnedProgress;
                    Property property7 = View.ALPHA;
                    float[] fArr7 = new float[1];
                    fArr7[0] = !z8 ? 0.0f : 1.0f;
                    animatorArr[6] = ObjectAnimator.ofFloat(radialProgressView, property7, fArr7);
                    RadialProgressView radialProgressView2 = this.pinnedProgress;
                    Property property8 = View.SCALE_X;
                    float[] fArr8 = new float[1];
                    fArr8[0] = !z8 ? 0.4f : 1.0f;
                    animatorArr[7] = ObjectAnimator.ofFloat(radialProgressView2, property8, fArr8);
                    RadialProgressView radialProgressView3 = this.pinnedProgress;
                    Property property9 = View.SCALE_Y;
                    float[] fArr9 = new float[1];
                    fArr9[0] = !z8 ? 0.4f : 1.0f;
                    animatorArr[8] = ObjectAnimator.ofFloat(radialProgressView3, property9, fArr9);
                    animatorSet2.playTogether(animatorArr);
                    this.pinnedListAnimator.setInterpolator(CubicBezierInterpolator.EASE_OUT_QUINT);
                    this.pinnedListAnimator.setDuration(360L);
                    this.pinnedListAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.90
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            ChatActivity.this.pinnedListAnimator = null;
                            int i3 = 0;
                            ChatActivity.this.closePinned.setVisibility(z6 ? 0 : 4);
                            ChatActivity.this.pinnedListButton.setVisibility(z7 ? 0 : 4);
                            RadialProgressView radialProgressView4 = ChatActivity.this.pinnedProgress;
                            if (!z8) {
                                i3 = 4;
                            }
                            radialProgressView4.setVisibility(i3);
                        }
                    });
                    this.pinnedListAnimator.start();
                } else {
                    this.closePinned.setAlpha(z6 ? 1.0f : 0.0f);
                    this.closePinned.setScaleX(z6 ? 1.0f : 0.4f);
                    this.closePinned.setScaleY(z6 ? 1.0f : 0.4f);
                    this.closePinned.setVisibility(z6 ? 0 : 4);
                    this.pinnedListButton.setAlpha(z7 ? 1.0f : 0.0f);
                    this.pinnedListButton.setScaleX(z7 ? 1.0f : 0.4f);
                    this.pinnedListButton.setScaleY(z7 ? 1.0f : 0.4f);
                    ImageView imageView7 = this.pinnedListButton;
                    if (z7) {
                        i2 = 0;
                    }
                    imageView7.setVisibility(i2);
                    this.pinnedProgress.setAlpha(z8 ? 1.0f : 0.0f);
                    this.pinnedProgress.setScaleX(z8 ? 1.0f : 0.4f);
                    this.pinnedProgress.setScaleY(z8 ? 1.0f : 0.4f);
                    RadialProgressView radialProgressView4 = this.pinnedProgress;
                    if (z8) {
                        i = 0;
                    }
                    radialProgressView4.setVisibility(i);
                }
                this.closePinned.setTag(z6 ? 1 : null);
                this.pinnedListButton.setTag(z2 ? 1 : null);
                this.pinnedProgress.setTag(z8 ? 1 : null);
            }
            if (this.pinnedLineView == null) {
                return;
            }
            if (isThreadChat()) {
                this.pinnedLineView.set(0, 1, false);
            } else {
                this.pinnedLineView.set((this.pinnedMessageIds.size() - 1) - Collections.binarySearch(this.pinnedMessageIds, Integer.valueOf(this.currentPinnedMessageId), Comparator$CC.reverseOrder()), this.pinnedMessageIds.size(), z);
            }
        }
    }

    private TLRPC$KeyboardButton pinnedButton(MessageObject messageObject) {
        TLRPC$Message tLRPC$Message;
        TLRPC$ReplyMarkup tLRPC$ReplyMarkup;
        ArrayList<TLRPC$TL_keyboardButtonRow> arrayList;
        if (messageObject == null || (tLRPC$Message = messageObject.messageOwner) == null || (tLRPC$ReplyMarkup = tLRPC$Message.reply_markup) == null || (arrayList = tLRPC$ReplyMarkup.rows) == null || arrayList.size() != 1 || messageObject.messageOwner.reply_markup.rows.get(0) == null || messageObject.messageOwner.reply_markup.rows.get(0).buttons == null || messageObject.messageOwner.reply_markup.rows.get(0).buttons.size() != 1) {
            return null;
        }
        return messageObject.messageOwner.reply_markup.rows.get(0).buttons.get(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0099, code lost:
        if (r32.pinnedMessageIds.get(0).intValue() != r5.getInt("pin_" + r32.dialog_id, 0)) goto L_0x009b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x03bc  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x03be  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x03d2  */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x04c5  */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x04d2  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x04db  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x055f  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x0577  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x0615  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x0627  */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x062b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:379:0x0bad  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:392:0x0c06  */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x0c11  */
    /* JADX WARNING: Removed duplicated region for block: B:400:0x0cc5  */
    /* JADX WARNING: Removed duplicated region for block: B:401:0x0ccd  */
    /* JADX WARNING: Removed duplicated region for block: B:402:0x0cf4  */
    /* JADX WARNING: Removed duplicated region for block: B:423:0x0d70  */
    /* JADX WARNING: Removed duplicated region for block: B:429:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updatePinnedMessageView(boolean r33, int r34) {
        /*
        // Method dump skipped, instructions count: 3572
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.updatePinnedMessageView(boolean, int):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updatePinnedMessageView$143(TLRPC$KeyboardButton tLRPC$KeyboardButton, MessageObject messageObject, View view) {
        if (getParentActivity() == null) {
            return;
        }
        if (this.bottomOverlayChat.getVisibility() != 0 || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonSwitchInline) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonCallback) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonGame) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrl) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonBuy) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrlAuth) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUserProfile)) {
            this.chatActivityEnterView.didPressedBotButton(tLRPC$KeyboardButton, messageObject, messageObject);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$updatePinnedMessageView$144(TLRPC$KeyboardButton tLRPC$KeyboardButton, MessageObject messageObject, PinnedMessageButton pinnedMessageButton, View view) {
        if (getParentActivity() == null || ((this.bottomOverlayChat.getVisibility() == 0 && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonSwitchInline) && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonCallback) && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonGame) && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrl) && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonBuy) && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrlAuth) && !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUserProfile)) || !(tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrl))) {
            return false;
        }
        openClickableLink(null, tLRPC$KeyboardButton.url, true, null, messageObject);
        try {
            pinnedMessageButton.performHapticFeedback(0, 1);
        } catch (Exception unused) {
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public class TrackingWidthSimpleTextView extends SimpleTextView {
        private boolean trackWidth = true;

        public TrackingWidthSimpleTextView(Context context) {
            super(context);
        }

        public void setTrackWidth(boolean z) {
            this.trackWidth = z;
        }

        public boolean getTrackWidth() {
            return this.trackWidth;
        }

        @Override // org.telegram.ui.ActionBar.SimpleTextView
        protected boolean createLayout(int i) {
            boolean createLayout = super.createLayout(i);
            if (this.trackWidth && getVisibility() == 0) {
                ChatActivity.this.pinnedCounterTextViewX = getTextWidth() + AndroidUtilities.dp(4.0f);
                if (ChatActivity.this.pinnedCounterTextView != null) {
                    ChatActivity.this.pinnedCounterTextView.setTranslationX((float) ChatActivity.this.pinnedCounterTextViewX);
                }
            }
            return createLayout;
        }
    }

    private void updateTopPanel(boolean z) {
        boolean z2;
        boolean z3;
        TextView textView;
        String str;
        if (this.topChatPanelView != null && this.chatMode == 0) {
            SharedPreferences notificationsSettings = MessagesController.getNotificationsSettings(this.currentAccount);
            long j = this.dialog_id;
            TLRPC$EncryptedChat tLRPC$EncryptedChat = this.currentEncryptedChat;
            if (tLRPC$EncryptedChat != null) {
                boolean z4 = tLRPC$EncryptedChat.admin_id != getUserConfig().getClientUserId() && !getContactsController().isLoadingContacts() && getContactsController().contactsDict.get(Long.valueOf(this.currentUser.id)) == null;
                long j2 = this.currentUser.id;
                int i = notificationsSettings.getInt("dialog_bar_vis3" + j2, 0);
                if (z4 && (i == 1 || i == 3)) {
                    z4 = false;
                }
                z2 = z4;
                j = j2;
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("dialog_bar_vis3");
                sb.append(j);
                z2 = notificationsSettings.getInt(sb.toString(), 0) == 2;
            }
            boolean z5 = notificationsSettings.getBoolean("dialog_bar_share" + j, false);
            boolean z6 = notificationsSettings.getBoolean("dialog_bar_report" + j, false);
            boolean z7 = notificationsSettings.getBoolean("dialog_bar_block" + j, false);
            boolean z8 = notificationsSettings.getBoolean("dialog_bar_add" + j, false);
            boolean z9 = notificationsSettings.getBoolean("dialog_bar_archived" + this.dialog_id, false);
            boolean z10 = notificationsSettings.getBoolean("dialog_bar_location" + j, false);
            final String string = notificationsSettings.getString("dialog_bar_chat_with_admin_title" + j, null);
            final boolean z11 = notificationsSettings.getBoolean("dialog_bar_chat_with_channel" + j, false);
            final int i2 = notificationsSettings.getInt("dialog_bar_chat_with_date" + j, 0);
            boolean z12 = notificationsSettings.getBoolean("dialog_bar_invite" + j, false);
            boolean z13 = z12 ? true : z2;
            if (z6 || z7 || z10) {
                this.reportSpamButton.setVisibility(0);
            } else {
                this.reportSpamButton.setVisibility(8);
            }
            this.addToContactsButtonArchive = false;
            final TLRPC$User user = this.currentUser != null ? getMessagesController().getUser(Long.valueOf(this.currentUser.id)) : null;
            if (user == null || TextUtils.isEmpty(string)) {
                if (z12) {
                    String string2 = LocaleController.getString("GroupAddMembers", R.string.GroupAddMembers);
                    if (string2 != null) {
                        string2 = string2.toUpperCase();
                    }
                    this.addToContactsButton.setVisibility(0);
                    this.addToContactsButton.setText(string2);
                    this.addToContactsButton.setTag(4);
                    this.addToContactsButton.setTextColor(getThemedColor("chat_addContact"));
                    if (Build.VERSION.SDK_INT >= 21) {
                        Theme.setSelectorDrawableColor(this.addToContactsButton.getBackground(), 436207615 & getThemedColor("chat_addContact"), true);
                    }
                    this.reportSpamButton.setTag("chat_addContact");
                } else if (user != null) {
                    if (UserObject.isReplyUser(user)) {
                        this.addToContactsButton.setVisibility(8);
                    } else {
                        boolean z14 = user.contact;
                        if (!z14 && !user.self && z8) {
                            this.addContactItem.setVisibility(0);
                            this.addContactItem.setText(LocaleController.getString("AddToContacts", R.string.AddToContacts));
                            this.addToContactsButton.setVisibility(0);
                            if (z9) {
                                this.addToContactsButtonArchive = true;
                                this.addToContactsButton.setText(LocaleController.getString("Unarchive", R.string.Unarchive).toUpperCase());
                                this.addToContactsButton.setTag(3);
                            } else if (this.reportSpamButton.getVisibility() == 0) {
                                this.addToContactsButton.setText(LocaleController.getString("AddContactChat", R.string.AddContactChat));
                            } else {
                                this.addToContactsButton.setText(LocaleController.formatString("AddContactFullChat", R.string.AddContactFullChat, UserObject.getFirstName(user)).toUpperCase());
                            }
                            this.addToContactsButton.setTag(null);
                            this.addToContactsButton.setVisibility(0);
                        } else if (!z5 || user.self) {
                            if (z14 || user.self || z13) {
                                this.addContactItem.setVisibility(8);
                            } else {
                                this.addContactItem.setVisibility(0);
                                this.addContactItem.setText(LocaleController.getString("ShareMyContactInfo", R.string.ShareMyContactInfo));
                                this.addToContactsButton.setTag(2);
                            }
                            this.addToContactsButton.setVisibility(8);
                        } else {
                            this.addContactItem.setVisibility(0);
                            this.addToContactsButton.setVisibility(0);
                            this.addContactItem.setText(LocaleController.getString("ShareMyContactInfo", R.string.ShareMyContactInfo));
                            this.addToContactsButton.setText(LocaleController.getString("ShareMyPhone", R.string.ShareMyPhone).toUpperCase());
                            this.addToContactsButton.setTag(1);
                            this.addToContactsButton.setVisibility(0);
                        }
                    }
                    this.reportSpamButton.setText(LocaleController.getString("ReportSpamUser", R.string.ReportSpamUser));
                } else {
                    if (z10) {
                        this.reportSpamButton.setText(LocaleController.getString("ReportSpamLocation", R.string.ReportSpamLocation));
                        this.reportSpamButton.setTag(R.id.object_tag, 1);
                        this.reportSpamButton.setTextColor(getThemedColor("chat_addContact"));
                        if (Build.VERSION.SDK_INT >= 21) {
                            Theme.setSelectorDrawableColor(this.reportSpamButton.getBackground(), 436207615 & getThemedColor("chat_addContact"), true);
                        }
                        this.reportSpamButton.setTag("chat_addContact");
                    } else {
                        if (z9) {
                            this.addToContactsButtonArchive = true;
                            this.addToContactsButton.setText(LocaleController.getString("Unarchive", R.string.Unarchive).toUpperCase());
                            this.addToContactsButton.setTag(3);
                            this.addToContactsButton.setVisibility(0);
                            this.reportSpamButton.setText(LocaleController.getString("ReportSpam", R.string.ReportSpam));
                        } else {
                            this.addToContactsButton.setVisibility(8);
                            this.reportSpamButton.setText(LocaleController.getString("ReportSpamAndLeave", R.string.ReportSpamAndLeave));
                        }
                        this.reportSpamButton.setTag(R.id.object_tag, null);
                        this.reportSpamButton.setTextColor(getThemedColor("chat_reportSpam"));
                        if (Build.VERSION.SDK_INT >= 21) {
                            Theme.setSelectorDrawableColor(this.reportSpamButton.getBackground(), 436207615 & getThemedColor("chat_reportSpam"), true);
                        }
                        this.reportSpamButton.setTag("chat_reportSpam");
                    }
                    ActionBarMenuSubItem actionBarMenuSubItem = this.addContactItem;
                    if (actionBarMenuSubItem != null) {
                        actionBarMenuSubItem.setVisibility(8);
                    }
                }
                z3 = false;
            } else {
                if (this.chatWithAdminTextView == null) {
                    TextView textView2 = new TextView(this.topChatPanelView.getContext());
                    this.chatWithAdminTextView = textView2;
                    textView2.setGravity(16);
                    this.chatWithAdminTextView.setPadding(AndroidUtilities.dp(14.0f), 0, AndroidUtilities.dp(46.0f), 0);
                    this.chatWithAdminTextView.setBackground(Theme.createSelectorDrawable(getThemedColor("listSelectorSDK21"), 2));
                    this.topChatPanelView.addView(this.chatWithAdminTextView, 0, LayoutHelper.createFrame(-1, -1.0f, 0, 0.0f, 0.0f, 0.0f, 1.0f));
                    this.chatWithAdminTextView.setTextColor(Theme.getColor("windowBackgroundWhiteBlackText"));
                    this.chatWithAdminTextView.setTextSize(1, 14.0f);
                    this.chatWithAdminTextView.setOnClickListener(new View.OnClickListener() { // from class: org.telegram.ui.ChatActivity.95
                        @Override // android.view.View.OnClickListener
                        public void onClick(View view) {
                            AlertsCreator.showChatWithAdmin(ChatActivity.this, user, string, z11, i2);
                        }
                    });
                }
                if (z11) {
                    str = LocaleController.formatString("ChatWithGroupAdmin", R.string.ChatWithGroupAdmin, user.first_name, string);
                } else {
                    str = LocaleController.formatString("ChatWithChannelAdmin", R.string.ChatWithChannelAdmin, user.first_name, string);
                }
                this.reportSpamButton.setVisibility(8);
                this.addToContactsButton.setVisibility(8);
                this.chatWithAdminTextView.setText(AndroidUtilities.replaceTags(str));
                z3 = true;
            }
            TextView textView3 = this.chatWithAdminTextView;
            if (textView3 != null) {
                textView3.setVisibility(z3 ? 0 : 8);
            }
            if (this.userBlocked || (this.addToContactsButton.getVisibility() == 8 && this.reportSpamButton.getVisibility() == 8 && ((textView = this.chatWithAdminTextView) == null || textView.getVisibility() == 8))) {
                z13 = false;
            }
            if (z13) {
                if (this.topChatPanelView.getTag() != null) {
                    if (BuildVars.LOGS_ENABLED) {
                        FileLog.d("show spam button");
                    }
                    this.topChatPanelView.setTag(null);
                    this.topChatPanelView.setVisibility(0);
                    AnimatorSet animatorSet = this.reportSpamViewAnimator;
                    if (animatorSet != null) {
                        animatorSet.cancel();
                        this.reportSpamViewAnimator = null;
                    }
                    if (z) {
                        this.reportSpamViewAnimator = new AnimatorSet();
                        ValueAnimator ofFloat = ValueAnimator.ofFloat(this.topChatPanelViewOffset, 0.0f);
                        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda14
                            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                                ChatActivity.this.lambda$updateTopPanel$145(valueAnimator);
                            }
                        });
                        this.reportSpamViewAnimator.playTogether(ofFloat);
                        this.reportSpamViewAnimator.setDuration(200L);
                        this.reportSpamViewAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.96
                            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                            public void onAnimationEnd(Animator animator) {
                                if (ChatActivity.this.reportSpamViewAnimator != null && ChatActivity.this.reportSpamViewAnimator.equals(animator)) {
                                    ChatActivity.this.reportSpamViewAnimator = null;
                                }
                            }

                            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                            public void onAnimationCancel(Animator animator) {
                                if (ChatActivity.this.reportSpamViewAnimator != null && ChatActivity.this.reportSpamViewAnimator.equals(animator)) {
                                    ChatActivity.this.reportSpamViewAnimator = null;
                                }
                            }
                        });
                        this.reportSpamViewAnimator.start();
                    } else {
                        this.topChatPanelViewOffset = 0.0f;
                        invalidateChatListViewTopPadding();
                        invalidateMessagesVisiblePart();
                    }
                }
            } else if (this.topChatPanelView.getTag() == null) {
                if (BuildVars.LOGS_ENABLED) {
                    FileLog.d("hide spam button");
                }
                this.topChatPanelView.setTag(1);
                AnimatorSet animatorSet2 = this.reportSpamViewAnimator;
                if (animatorSet2 != null) {
                    animatorSet2.cancel();
                    this.reportSpamViewAnimator = null;
                }
                if (z) {
                    this.reportSpamViewAnimator = new AnimatorSet();
                    ValueAnimator ofFloat2 = ValueAnimator.ofFloat(this.topChatPanelViewOffset, (float) (-AndroidUtilities.dp(50.0f)));
                    ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda6
                        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                            ChatActivity.this.lambda$updateTopPanel$146(valueAnimator);
                        }
                    });
                    this.reportSpamViewAnimator.playTogether(ofFloat2);
                    this.reportSpamViewAnimator.setDuration(200L);
                    this.reportSpamViewAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.97
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            if (ChatActivity.this.reportSpamViewAnimator != null && ChatActivity.this.reportSpamViewAnimator.equals(animator)) {
                                ChatActivity.this.topChatPanelView.setVisibility(8);
                                ChatActivity.this.reportSpamViewAnimator = null;
                            }
                        }

                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationCancel(Animator animator) {
                            if (ChatActivity.this.reportSpamViewAnimator != null && ChatActivity.this.reportSpamViewAnimator.equals(animator)) {
                                ChatActivity.this.reportSpamViewAnimator = null;
                            }
                        }
                    });
                    this.reportSpamViewAnimator.start();
                } else {
                    this.topChatPanelViewOffset = (float) (-AndroidUtilities.dp(50.0f));
                    invalidateChatListViewTopPadding();
                    invalidateMessagesVisiblePart();
                }
            }
            checkListViewPaddings();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateTopPanel$145(ValueAnimator valueAnimator) {
        this.topChatPanelViewOffset = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidateChatListViewTopPadding();
        invalidateMessagesVisiblePart();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateTopPanel$146(ValueAnimator valueAnimator) {
        this.topChatPanelViewOffset = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidateChatListViewTopPadding();
        invalidateMessagesVisiblePart();
    }

    private void checkListViewPaddings() {
        MessageObject messageObject;
        if (this.wasManualScroll || (messageObject = this.unreadMessageObject) == null) {
            if (this.checkPaddingsRunnable == null) {
                ChatActivity$$ExternalSyntheticLambda141 chatActivity$$ExternalSyntheticLambda141 = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda141
                    @Override // java.lang.Runnable
                    public final void run() {
                        ChatActivity.this.lambda$checkListViewPaddings$147();
                    }
                };
                this.checkPaddingsRunnable = chatActivity$$ExternalSyntheticLambda141;
                AndroidUtilities.runOnUIThread(chatActivity$$ExternalSyntheticLambda141);
            }
        } else if (this.messages.indexOf(messageObject) >= 0) {
            this.fixPaddingsInLayout = true;
            View view = this.fragmentView;
            if (view != null) {
                view.requestLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$checkListViewPaddings$147() {
        this.checkPaddingsRunnable = null;
        invalidateChatListViewTopPadding();
        invalidateMessagesVisiblePart();
    }

    /* access modifiers changed from: private */
    public void checkRaiseSensors() {
        BlurredFrameLayout blurredFrameLayout;
        FrameLayout frameLayout;
        BlurredFrameLayout blurredFrameLayout2;
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView == null || !chatActivityEnterView.isStickersExpanded()) {
            TLRPC$Chat tLRPC$Chat = this.currentChat;
            if (tLRPC$Chat != null && !ChatObject.canSendMedia(tLRPC$Chat)) {
                MediaController.getInstance().setAllowStartRecord(false);
            } else if (ApplicationLoader.mainInterfacePaused || (((blurredFrameLayout = this.bottomOverlayChat) != null && blurredFrameLayout.getVisibility() == 0) || (((frameLayout = this.bottomOverlay) != null && frameLayout.getVisibility() == 0) || ((blurredFrameLayout2 = this.searchContainer) != null && blurredFrameLayout2.getVisibility() == 0)))) {
                MediaController.getInstance().setAllowStartRecord(false);
            } else {
                MediaController.getInstance().setAllowStartRecord(true);
            }
        } else {
            MediaController.getInstance().setAllowStartRecord(false);
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void dismissCurrentDialog() {
        ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
        if (chatAttachAlert == null || this.visibleDialog != chatAttachAlert) {
            super.dismissCurrentDialog();
            return;
        }
        chatAttachAlert.getPhotoLayout().closeCamera(false);
        this.chatAttachAlert.dismissInternal();
        this.chatAttachAlert.getPhotoLayout().hideCamera(true);
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected void setInPreviewMode(boolean z) {
        MessageObject messageObject;
        TLRPC$Message tLRPC$Message;
        super.setInPreviewMode(z);
        if (!(this.currentUser == null || this.audioCallIconItem == null)) {
            TLRPC$UserFull userFull = getMessagesController().getUserFull(this.currentUser.id);
            if (userFull == null || !userFull.phone_calls_available) {
                this.showAudioCallAsIcon = false;
                this.audioCallIconItem.setVisibility(8);
            } else {
                this.showAudioCallAsIcon = !this.inPreviewMode;
                this.audioCallIconItem.setVisibility(0);
            }
        }
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        if (chatAvatarContainer != null) {
            chatAvatarContainer.setOccupyStatusBar(!z);
            this.avatarContainer.setLayoutParams(LayoutHelper.createFrame(-2, -1.0f, 51, !z ? 56.0f : (float) (this.chatMode == 2 ? 10 : 0), 0.0f, 40.0f, 0.0f));
        }
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.setVisibility(!z ? 0 : 4);
        }
        ActionBar actionBar = this.actionBar;
        if (actionBar != null) {
            actionBar.setBackButtonDrawable(!z ? new BackDrawable(false) : null);
            ActionBarMenuItem actionBarMenuItem = this.headerItem;
            float f = 1.0f;
            if (actionBarMenuItem != null) {
                actionBarMenuItem.setAlpha(!z ? 1.0f : 0.0f);
            }
            ActionBarMenuItem actionBarMenuItem2 = this.attachItem;
            if (actionBarMenuItem2 != null) {
                if (z) {
                    f = 0.0f;
                }
                actionBarMenuItem2.setAlpha(f);
            }
        }
        RecyclerListView recyclerListView = this.chatListView;
        if (recyclerListView != null) {
            int childCount = recyclerListView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = this.chatListView.getChildAt(i);
                boolean z2 = childAt instanceof ChatMessageCell;
                if (z2) {
                    messageObject = ((ChatMessageCell) childAt).getMessageObject();
                } else {
                    messageObject = childAt instanceof ChatActionCell ? ((ChatActionCell) childAt).getMessageObject() : null;
                }
                if (messageObject != null && (tLRPC$Message = messageObject.messageOwner) != null && tLRPC$Message.media_unread && tLRPC$Message.mentioned) {
                    if (!messageObject.isVoice() && !messageObject.isRoundVideo()) {
                        int i2 = this.newMentionsCount - 1;
                        this.newMentionsCount = i2;
                        if (i2 <= 0) {
                            this.newMentionsCount = 0;
                            this.hasAllMentionsLocal = true;
                            showMentionDownButton(false, true);
                        } else {
                            this.mentiondownButtonCounter.setText(String.format("%d", Integer.valueOf(i2)));
                        }
                        getMessagesController().markMentionMessageAsRead(messageObject.getId(), ChatObject.isChannel(this.currentChat) ? this.currentChat.id : 0, this.dialog_id);
                        messageObject.setContentIsRead();
                    }
                    if (z2) {
                        ChatMessageCell chatMessageCell = (ChatMessageCell) childAt;
                        chatMessageCell.setHighlighted(false);
                        chatMessageCell.setHighlightedAnimated();
                    }
                }
            }
            this.chatListView.setItemAnimator(null);
        }
        updateBottomOverlay();
        updateSecretStatus();
        FragmentContextView fragmentContextView = this.fragmentContextView;
        if (fragmentContextView != null) {
            fragmentContextView.setEnabled(!z);
        }
        FragmentContextView fragmentContextView2 = this.fragmentLocationContextView;
        if (fragmentContextView2 != null) {
            fragmentContextView2.setEnabled(!z);
        }
        BlurredFrameLayout blurredFrameLayout = this.pinnedMessageView;
        if (blurredFrameLayout != null) {
            blurredFrameLayout.setEnabled(true ^ isInPreviewMode());
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void onResume() {
        ChatActivityEnterView chatActivityEnterView;
        MessageObject messageObject;
        boolean z;
        BackupImageView backupImageView;
        super.onResume();
        checkShowBlur(false);
        this.activityResumeTime = System.currentTimeMillis();
        if (this.openImport && getSendMessagesHelper().getImportingHistory(this.dialog_id) != null) {
            ImportingAlert importingAlert = new ImportingAlert(getParentActivity(), null, this, this.themeDelegate);
            importingAlert.setOnHideListener(new DialogInterface.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda49
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    ChatActivity.this.lambda$onResume$148(dialogInterface);
                }
            });
            showDialog(importingAlert);
            this.openImport = false;
        }
        checkAdjustResize();
        MediaController.getInstance().startRaiseToEarSensors(this);
        checkRaiseSensors();
        ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
        if (chatAttachAlert != null) {
            chatAttachAlert.onResume();
        }
        SizeNotifierFrameLayout sizeNotifierFrameLayout = this.contentView;
        if (sizeNotifierFrameLayout != null) {
            sizeNotifierFrameLayout.onResume();
        }
        checkChecksHint();
        AnonymousClass98 r1 = new Bulletin.Delegate() { // from class: org.telegram.ui.ChatActivity.98
            @Override // org.telegram.ui.Components.Bulletin.Delegate
            public /* synthetic */ void onHide(Bulletin bulletin) {
                Bulletin.Delegate.CC.$default$onHide(this, bulletin);
            }

            @Override // org.telegram.ui.Components.Bulletin.Delegate
            public /* synthetic */ void onOffsetChange(float f) {
                Bulletin.Delegate.CC.$default$onOffsetChange(this, f);
            }

            @Override // org.telegram.ui.Components.Bulletin.Delegate
            public /* synthetic */ void onShow(Bulletin bulletin) {
                Bulletin.Delegate.CC.$default$onShow(this, bulletin);
            }

            @Override // org.telegram.ui.Components.Bulletin.Delegate
            public int getBottomOffset(int i) {
                int i2;
                if (i == 1) {
                    return 0;
                }
                ChatActivityEnterView chatActivityEnterView2 = ChatActivity.this.chatActivityEnterView;
                if (chatActivityEnterView2 == null || chatActivityEnterView2.getVisibility() != 0) {
                    i2 = AndroidUtilities.dp(51.0f);
                } else if ((ChatActivity.this.contentView.getKeyboardHeight() >= AndroidUtilities.dp(20.0f) || !ChatActivity.this.chatActivityEnterView.isPopupShowing()) && !ChatActivity.this.chatActivityEnterView.panelAnimationInProgress()) {
                    i2 = ChatActivity.this.chatActivityEnterView.getHeight();
                } else {
                    i2 = ChatActivity.this.chatActivityEnterView.getHeight() + ChatActivity.this.chatActivityEnterView.getEmojiPadding();
                }
                if (ChatActivity.this.chatActivityEnterView.panelAnimationInProgress()) {
                    i2 = (int) (((float) i2) + (ChatActivity.this.bottomPanelTranslationY - ((float) ChatActivity.this.chatActivityEnterView.getEmojiPadding())));
                }
                return ((int) (((float) i2) + ChatActivity.this.contentPanTranslation)) - AndroidUtilities.dp(1.5f);
            }
        };
        this.bulletinDelegate = r1;
        Bulletin.addDelegate(this, r1);
        checkActionBarMenu(false);
        TLRPC$PhotoSize tLRPC$PhotoSize = this.replyImageLocation;
        if (!(tLRPC$PhotoSize == null || (backupImageView = this.replyImageView) == null)) {
            backupImageView.setImage(ImageLocation.getForObject(tLRPC$PhotoSize, this.replyImageLocationObject), "50_50", ImageLocation.getForObject(this.replyImageThumbLocation, this.replyImageLocationObject), "50_50_b", null, (long) this.replyImageSize, this.replyImageCacheType, this.replyingMessageObject);
        }
        if (!(this.pinnedImageLocation == null || this.pinnedMessageImageView == null)) {
            this.pinnedMessageImageView[0].setImage(ImageLocation.getForObject(this.pinnedImageLocation, this.pinnedImageLocationObject), "50_50", ImageLocation.getForObject(this.pinnedImageThumbLocation, this.pinnedImageLocationObject), "50_50_b", null, (long) this.pinnedImageSize, this.pinnedImageCacheType, this.pinnedMessageObjects.get(Integer.valueOf(this.currentPinnedMessageId)));
        }
        if (this.chatMode == 0) {
            getNotificationsController().setOpenedDialogId(this.dialog_id);
        }
        getMessagesController().setLastVisibleDialogId(this.dialog_id, this.chatMode == 1, true);
        if (this.scrollToTopOnResume) {
            if (!this.scrollToTopUnReadOnResume || (messageObject = this.scrollToMessage) == null) {
                moveScrollToLastMessage(false);
            } else if (this.chatListView != null) {
                int i = this.scrollToMessagePosition;
                if (i == -9000) {
                    i = getScrollOffsetForMessage(messageObject);
                } else if (i == -10000) {
                    i = -AndroidUtilities.dp(11.0f);
                } else {
                    z = true;
                    this.chatLayoutManager.scrollToPositionWithOffset(this.chatAdapter.messagesStartRow + this.messages.indexOf(this.scrollToMessage), i, z);
                }
                z = false;
                this.chatLayoutManager.scrollToPositionWithOffset(this.chatAdapter.messagesStartRow + this.messages.indexOf(this.scrollToMessage), i, z);
            }
            this.scrollToTopUnReadOnResume = false;
            this.scrollToTopOnResume = false;
            this.scrollToMessage = null;
        }
        this.paused = false;
        this.pausedOnLastMessage = false;
        checkScrollForLoad(false);
        if (this.wasPaused) {
            this.wasPaused = false;
            ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
            if (chatActivityAdapter != null) {
                chatActivityAdapter.notifyDataSetChanged(false);
            }
        }
        fixLayout();
        applyDraftMaybe(false);
        BlurredFrameLayout blurredFrameLayout = this.bottomOverlayChat;
        if (!(blurredFrameLayout == null || blurredFrameLayout.getVisibility() == 0 || this.actionBar.isSearchFieldVisible())) {
            this.chatActivityEnterView.setFieldFocused(true);
        }
        ChatActivityEnterView chatActivityEnterView2 = this.chatActivityEnterView;
        if (chatActivityEnterView2 != null) {
            chatActivityEnterView2.onResume();
        }
        if (this.currentUser != null) {
            this.chatEnterTime = System.currentTimeMillis();
            this.chatLeaveTime = 0;
        }
        if (this.startVideoEdit != null) {
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda140
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$onResume$149();
                }
            });
        }
        if (this.chatListView != null && ((chatActivityEnterView = this.chatActivityEnterView) == null || !chatActivityEnterView.isEditingMessage())) {
            this.chatListView.setOnItemLongClickListener(this.onItemLongClickListener);
            this.chatListView.setOnItemClickListener(this.onItemClickListener);
            this.chatListView.setLongClickable(true);
        }
        checkBotCommands();
        updateTitle();
        showGigagroupConvertAlert();
        if (this.pullingDownOffset != 0.0f) {
            this.pullingDownOffset = 0.0f;
            this.chatListView.invalidate();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onResume$148(DialogInterface dialogInterface) {
        FragmentContextView fragmentContextView = this.fragmentContextView;
        if (fragmentContextView != null) {
            fragmentContextView.checkImport(false);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onResume$149() {
        openVideoEditor(this.startVideoEdit, null);
        this.startVideoEdit = null;
    }

    public void checkAdjustResize() {
        if (this.reportType >= 0) {
            AndroidUtilities.requestAdjustNothing(getParentActivity(), this.classGuid);
        } else {
            AndroidUtilities.requestAdjustResize(getParentActivity(), this.classGuid);
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void finishFragment() {
        super.finishFragment();
        ActionBarPopupWindow actionBarPopupWindow = this.scrimPopupWindow;
        if (actionBarPopupWindow != null) {
            actionBarPopupWindow.setPauseNotifications(false);
            closeMenu();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:114:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x022c  */
    @Override // org.telegram.ui.ActionBar.BaseFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onPause() {
        /*
        // Method dump skipped, instructions count: 645
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.onPause():void");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:69:0x018d */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v7, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r2v4, types: [org.telegram.ui.Components.ChatActivityEnterView] */
    /* JADX WARN: Type inference failed for: r13v12, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r13v14, types: [android.text.SpannableStringBuilder, android.text.Spannable] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void applyDraftMaybe(boolean r13) {
        /*
        // Method dump skipped, instructions count: 481
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.applyDraftMaybe(boolean):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$applyDraftMaybe$150() {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.setFieldFocused(true);
            this.chatActivityEnterView.openKeyboard();
        }
    }

    private void updateInformationForScreenshotDetector() {
        if (this.currentUser != null) {
            if (this.currentEncryptedChat != null) {
                ArrayList<Long> arrayList = new ArrayList<>();
                RecyclerListView recyclerListView = this.chatListView;
                if (recyclerListView != null) {
                    int childCount = recyclerListView.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View childAt = this.chatListView.getChildAt(i);
                        MessageObject messageObject = null;
                        if (childAt instanceof ChatMessageCell) {
                            messageObject = ((ChatMessageCell) childAt).getMessageObject();
                        }
                        if (messageObject != null && messageObject.getId() < 0) {
                            long j = messageObject.messageOwner.random_id;
                            if (j != 0) {
                                arrayList.add(Long.valueOf(j));
                            }
                        }
                    }
                }
                MediaController.getInstance().setLastVisibleMessageIds(this.currentAccount, this.chatEnterTime, this.chatLeaveTime, this.currentUser, this.currentEncryptedChat, arrayList, 0);
                return;
            }
            SecretMediaViewer instance = SecretMediaViewer.getInstance();
            MessageObject currentMessageObject = instance.getCurrentMessageObject();
            if (!(currentMessageObject == null || currentMessageObject.isOut())) {
                MediaController.getInstance().setLastVisibleMessageIds(this.currentAccount, instance.getOpenTime(), instance.getCloseTime(), this.currentUser, null, null, currentMessageObject.getId());
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean fixLayoutInternal() {
        boolean z;
        MessageObject.GroupedMessages currentMessagesGroup;
        if (AndroidUtilities.isTablet() || ApplicationLoader.applicationContext.getResources().getConfiguration().orientation != 2) {
            this.selectedMessagesCountTextView.setTextSize(20);
        } else {
            this.selectedMessagesCountTextView.setTextSize(18);
        }
        int childCount = this.chatListView.getChildCount();
        HashMap hashMap = null;
        int i = 0;
        while (true) {
            z = true;
            if (i >= childCount) {
                break;
            }
            View childAt = this.chatListView.getChildAt(i);
            if ((childAt instanceof ChatMessageCell) && (currentMessagesGroup = ((ChatMessageCell) childAt).getCurrentMessagesGroup()) != null && currentMessagesGroup.hasSibling && !currentMessagesGroup.messages.isEmpty()) {
                if (hashMap == null) {
                    hashMap = new HashMap();
                }
                if (!hashMap.containsKey(Long.valueOf(currentMessagesGroup.groupId))) {
                    hashMap.put(Long.valueOf(currentMessagesGroup.groupId), currentMessagesGroup);
                    ArrayList<MessageObject> arrayList = currentMessagesGroup.messages;
                    int indexOf = this.messages.indexOf(arrayList.get(arrayList.size() - 1));
                    if (indexOf >= 0) {
                        ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
                        chatActivityAdapter.notifyItemRangeChanged(indexOf + chatActivityAdapter.messagesStartRow, currentMessagesGroup.messages.size());
                        this.chatListView.setItemAnimator(null);
                    }
                }
            }
            i++;
        }
        if (!AndroidUtilities.isTablet()) {
            return true;
        }
        if (!AndroidUtilities.isSmallTablet() || ApplicationLoader.applicationContext.getResources().getConfiguration().orientation != 1) {
            ActionBar actionBar = this.actionBar;
            ActionBarLayout actionBarLayout = this.parentLayout;
            if (!(actionBarLayout == null || actionBarLayout.fragmentsStack.isEmpty() || this.parentLayout.fragmentsStack.get(0) == this || this.parentLayout.fragmentsStack.size() == 1)) {
                z = false;
            }
            actionBar.setBackButtonDrawable(new BackDrawable(z));
        } else {
            this.actionBar.setBackButtonDrawable(new BackDrawable(false));
        }
        return false;
    }

    private void fixLayout() {
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        if (chatAvatarContainer != null) {
            chatAvatarContainer.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() { // from class: org.telegram.ui.ChatActivity.99
                @Override // android.view.ViewTreeObserver.OnPreDrawListener
                public boolean onPreDraw() {
                    if (ChatActivity.this.avatarContainer != null) {
                        ChatActivity.this.avatarContainer.getViewTreeObserver().removeOnPreDrawListener(this);
                    }
                    return ChatActivity.this.fixLayoutInternal();
                }
            });
        }
    }

    public boolean maybePlayVisibleVideo() {
        MessageObject messageObject;
        AnimatedFileDrawable animatedFileDrawable;
        ImageReceiver photoImage;
        AnimatedFileDrawable animation;
        ChatMessageCell messageCell;
        if (this.chatListView == null) {
            return false;
        }
        MessageObject playingMessageObject = MediaController.getInstance().getPlayingMessageObject();
        if (playingMessageObject != null && !playingMessageObject.isVideo()) {
            return false;
        }
        HintView hintView = this.noSoundHintView;
        ArrayList<MessageObject> arrayList = null;
        if (hintView == null || hintView.getTag() == null || (messageCell = this.noSoundHintView.getMessageCell()) == null) {
            animatedFileDrawable = null;
            messageObject = null;
        } else {
            ImageReceiver photoImage2 = messageCell.getPhotoImage();
            animatedFileDrawable = photoImage2.getAnimation();
            if (animatedFileDrawable != null) {
                messageObject = messageCell.getMessageObject();
                this.scrollToVideo = ((float) messageCell.getTop()) + photoImage2.getImageY2() > ((float) this.chatListView.getMeasuredHeight());
            } else {
                messageObject = null;
            }
        }
        if (messageObject == null) {
            int childCount = this.chatListView.getChildCount();
            int i = 0;
            while (true) {
                if (i >= childCount) {
                    break;
                }
                View childAt = this.chatListView.getChildAt(i);
                if (childAt instanceof ChatMessageCell) {
                    ChatMessageCell chatMessageCell = (ChatMessageCell) childAt;
                    MessageObject messageObject2 = chatMessageCell.getMessageObject();
                    boolean isRoundVideo = messageObject2.isRoundVideo();
                    if ((messageObject2.isVideo() || isRoundVideo) && messageObject2.videoEditedInfo == null && (animation = (photoImage = chatMessageCell.getPhotoImage()).getAnimation()) != null) {
                        float top = ((float) childAt.getTop()) + photoImage.getImageY();
                        float imageHeight = photoImage.getImageHeight() + top;
                        if (imageHeight >= 0.0f && top <= ((float) this.chatListView.getMeasuredHeight())) {
                            if (messageObject != null && top < 0.0f) {
                                break;
                            }
                            this.scrollToVideo = top < 0.0f || imageHeight > ((float) this.chatListView.getMeasuredHeight());
                            if (top >= 0.0f && imageHeight <= ((float) this.chatListView.getMeasuredHeight())) {
                                messageObject = messageObject2;
                                animatedFileDrawable = animation;
                                break;
                            }
                            messageObject = messageObject2;
                            animatedFileDrawable = animation;
                        }
                    }
                }
                i++;
            }
        }
        if (messageObject == null || MediaController.getInstance().isPlayingMessage(messageObject)) {
            return false;
        }
        hideHints(true);
        if (messageObject.isRoundVideo()) {
            boolean playMessage = MediaController.getInstance().playMessage(messageObject);
            MediaController instance = MediaController.getInstance();
            if (playMessage) {
                arrayList = createVoiceMessagesPlaylist(messageObject, false);
            }
            instance.setVoiceMessagesPlaylist(arrayList, false);
            return playMessage;
        }
        SharedConfig.setNoSoundHintShowed(true);
        messageObject.audioProgress = animatedFileDrawable.getCurrentProgress();
        messageObject.audioProgressMs = animatedFileDrawable.getCurrentProgressMs();
        animatedFileDrawable.stop();
        if (PhotoViewer.isPlayingMessageInPip(messageObject)) {
            PhotoViewer.getPipInstance().destroyPhotoViewer();
        }
        return MediaController.getInstance().playMessage(messageObject);
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public void onConfigurationChanged(Configuration configuration) {
        MessageObject playingMessageObject;
        fixLayout();
        Dialog dialog = this.visibleDialog;
        if (dialog instanceof DatePickerDialog) {
            dialog.dismiss();
        }
        closeMenu();
        if (AndroidUtilities.isTablet()) {
            return;
        }
        if (configuration.orientation == 2) {
            if ((!PhotoViewer.hasInstance() || !PhotoViewer.getInstance().isVisible()) && (playingMessageObject = MediaController.getInstance().getPlayingMessageObject()) != null && playingMessageObject.isVideo()) {
                PhotoViewer.getInstance().setParentActivity(getParentActivity(), this.themeDelegate);
                getFileLoader().setLoadingVideoForPlayer(playingMessageObject.getDocument(), false);
                MediaController.getInstance().cleanupPlayer(true, true, false, true);
                PhotoViewer instance = PhotoViewer.getInstance();
                int i = playingMessageObject.type;
                long j = 0;
                long j2 = i != 0 ? this.dialog_id : 0;
                if (i != 0) {
                    j = this.mergeDialogId;
                }
                if (instance.openPhoto(playingMessageObject, j2, j, this.photoViewerProvider, false)) {
                    PhotoViewer.getInstance().setParentChatActivity(this);
                }
                hideHints(false);
                MediaController.getInstance().resetGoingToShowMessageObject();
            }
        } else if (PhotoViewer.hasInstance() && PhotoViewer.getInstance().isOpenedFullScreenVideo()) {
            PhotoViewer.getInstance().injectVideoPlayerToMediaController();
            PhotoViewer.getInstance().closePhoto(false, true);
        }
    }

    /* access modifiers changed from: private */
    public void createDeleteMessagesAlert(MessageObject messageObject, MessageObject.GroupedMessages groupedMessages) {
        createDeleteMessagesAlert(messageObject, groupedMessages, 1);
    }

    private void createDeleteMessagesAlert(MessageObject messageObject, MessageObject.GroupedMessages groupedMessages, int i) {
        createDeleteMessagesAlert(messageObject, groupedMessages, i, false);
    }

    private void createDeleteMessagesAlert(MessageObject messageObject, MessageObject.GroupedMessages groupedMessages, int i, boolean z) {
        if (messageObject != null || this.selectedMessagesIds[0].size() + this.selectedMessagesIds[1].size() != 0) {
            AlertsCreator.createDeleteMessagesAlert(this, this.currentUser, this.currentChat, this.currentEncryptedChat, this.chatInfo, this.mergeDialogId, messageObject, this.selectedMessagesIds, groupedMessages, this.chatMode == 1, i, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda121
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createDeleteMessagesAlert$151();
                }
            }, z ? new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda157
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createDeleteMessagesAlert$152();
                }
            } : null, this.themeDelegate);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createDeleteMessagesAlert$151() {
        hideActionMode();
        updatePinnedMessageView(true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createDeleteMessagesAlert$152() {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public void hideActionMode() {
        ActionBar actionBar = this.actionBar;
        if (actionBar != null) {
            if (actionBar.isActionModeShowed()) {
                this.actionBar.hideActionMode();
            } else {
                return;
            }
        }
        this.cantDeleteMessagesCount = 0;
        this.canEditMessagesCount = 0;
        this.cantForwardMessagesCount = 0;
        this.canSaveMusicCount = 0;
        this.canSaveDocumentsCount = 0;
        this.cantSaveMessagesCount = 0;
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            EditTextCaption editField = chatActivityEnterView.getEditField();
            if (this.chatActivityEnterView.getVisibility() == 0) {
                editField.requestFocus();
            }
            editField.setAllowDrawCursor(true);
        }
        TextSelectionHelper.ChatListTextSelectionHelper chatListTextSelectionHelper = this.textSelectionHelper;
        if (chatListTextSelectionHelper != null) {
            chatListTextSelectionHelper.clear(true);
            this.textSelectionHelper.cancelAllAnimators();
        }
        TextSelectionHint textSelectionHint = this.textSelectionHint;
        if (textSelectionHint != null) {
            textSelectionHint.hide();
        }
        ChatActivityEnterView chatActivityEnterView2 = this.chatActivityEnterView;
        if (chatActivityEnterView2 != null) {
            chatActivityEnterView2.preventInput = false;
        }
        this.textSelectionHintWasShowed = false;
    }

    /* access modifiers changed from: private */
    public boolean createMenu(View view, boolean z, boolean z2, float f, float f2) {
        return createMenu(view, z, z2, f, f2, true);
    }

    private CharSequence getMessageCaption(MessageObject messageObject, MessageObject.GroupedMessages groupedMessages) {
        String restrictionReason = MessagesController.getRestrictionReason(messageObject.messageOwner.restriction_reason);
        if (!TextUtils.isEmpty(restrictionReason)) {
            return restrictionReason;
        }
        if (messageObject.isVoiceTranscriptionOpen() && !TranscribeButton.isTranscribing(messageObject)) {
            return messageObject.getVoiceTranscription();
        }
        CharSequence charSequence = messageObject.caption;
        if (charSequence != null) {
            return charSequence;
        }
        if (groupedMessages == null) {
            return null;
        }
        int size = groupedMessages.messages.size();
        CharSequence charSequence2 = null;
        for (int i = 0; i < size; i++) {
            CharSequence charSequence3 = groupedMessages.messages.get(i).caption;
            if (charSequence3 != null) {
                if (charSequence2 != null) {
                    return null;
                }
                charSequence2 = charSequence3;
            }
        }
        return charSequence2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v6, resolved type: org.telegram.ui.ActionBar.ActionBarPopupWindow$ActionBarPopupWindowLayout */
    /* JADX DEBUG: Multi-variable search result rejected for r1v7, resolved type: org.telegram.ui.ChatActivity$110 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v9, resolved type: org.telegram.ui.ActionBar.ActionBarPopupWindow */
    /* JADX DEBUG: Multi-variable search result rejected for r1v10, resolved type: org.telegram.ui.ActionBar.ActionBarPopupWindow */
    /* JADX DEBUG: Multi-variable search result rejected for r1v12, resolved type: org.telegram.ui.ActionBar.ActionBarPopupWindow */
    /* JADX DEBUG: Multi-variable search result rejected for r0v13, resolved type: org.telegram.ui.Components.UndoView */
    /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: org.telegram.ui.Components.UndoView */
    /* JADX DEBUG: Multi-variable search result rejected for r1v56, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v7, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r7v15 */
    /* JADX WARN: Type inference failed for: r11v5, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r11v9 */
    /* JADX WARN: Type inference failed for: r11v31 */
    /* JADX WARN: Type inference failed for: r7v69 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:487:0x0ae2, code lost:
        if (r0.edit_messages != false) goto L_0x0ae4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:1000:0x1ef3  */
    /* JADX WARNING: Removed duplicated region for block: B:1001:0x1ef8  */
    /* JADX WARNING: Removed duplicated region for block: B:1004:0x1f0b  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x024e  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0283 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x02c2  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x02d9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x02e5  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0309  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x030b  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0311  */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x037e  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x04f9 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x04fa  */
    /* JADX WARNING: Removed duplicated region for block: B:729:0x142f A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:733:0x1438  */
    /* JADX WARNING: Removed duplicated region for block: B:735:0x1441  */
    /* JADX WARNING: Removed duplicated region for block: B:845:0x162d  */
    /* JADX WARNING: Removed duplicated region for block: B:846:0x1630  */
    /* JADX WARNING: Removed duplicated region for block: B:849:0x166b  */
    /* JADX WARNING: Removed duplicated region for block: B:850:0x166d  */
    /* JADX WARNING: Removed duplicated region for block: B:853:0x1677  */
    /* JADX WARNING: Removed duplicated region for block: B:854:0x167e  */
    /* JADX WARNING: Removed duplicated region for block: B:857:0x1699  */
    /* JADX WARNING: Removed duplicated region for block: B:880:0x181c  */
    /* JADX WARNING: Removed duplicated region for block: B:996:0x1ee7  */
    /* JADX WARNING: Removed duplicated region for block: B:997:0x1eec  */
    /* JADX WARNING: Unknown variable types count: 2 */
    @android.annotation.SuppressLint({"ClickableViewAccessibility"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean createMenu(android.view.View r51, boolean r52, boolean r53, float r54, float r55, boolean r56) {
        /*
        // Method dump skipped, instructions count: 8580
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.createMenu(android.view.View, boolean, boolean, float, float, boolean):boolean");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$154(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda184
            public final /* synthetic */ TLObject f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$createMenu$153(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$153(TLObject tLObject) {
        if (tLObject instanceof TLRPC$TL_payments_paymentReceipt) {
            presentFragment(new PaymentFormActivity((TLRPC$TL_payments_paymentReceipt) tLObject));
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$155(ActionBarPopupWindow.ActionBarPopupWindowLayout actionBarPopupWindowLayout, View view) {
        actionBarPopupWindowLayout.getSwipeBack().closeForeground();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$157(ViewPager viewPager, int i, LinearLayout linearLayout, AtomicBoolean atomicBoolean, HorizontalScrollView horizontalScrollView, ReactionTabHolderView reactionTabHolderView, View view) {
        int currentItem = viewPager.getCurrentItem();
        if (i != currentItem) {
            ReactionTabHolderView reactionTabHolderView2 = (ReactionTabHolderView) linearLayout.getChildAt(currentItem);
            atomicBoolean.set(true);
            viewPager.setCurrentItem(i, true);
            float scrollX = (float) horizontalScrollView.getScrollX();
            float x = reactionTabHolderView.getX() - (((float) (horizontalScrollView.getWidth() - reactionTabHolderView.getWidth())) / 2.0f);
            ValueAnimator duration = ValueAnimator.ofFloat(0.0f, 1.0f).setDuration(150L);
            duration.setInterpolator(CubicBezierInterpolator.DEFAULT);
            duration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(horizontalScrollView, scrollX, x, reactionTabHolderView2, reactionTabHolderView) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda0
                public final /* synthetic */ HorizontalScrollView f$0;
                public final /* synthetic */ float f$1;
                public final /* synthetic */ float f$2;
                public final /* synthetic */ ReactionTabHolderView f$3;
                public final /* synthetic */ ReactionTabHolderView f$4;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ChatActivity.lambda$createMenu$156(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, valueAnimator);
                }
            });
            duration.start();
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$156(HorizontalScrollView horizontalScrollView, float f, float f2, ReactionTabHolderView reactionTabHolderView, ReactionTabHolderView reactionTabHolderView2, ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        horizontalScrollView.setScrollX((int) (f + ((f2 - f) * floatValue)));
        reactionTabHolderView.setOutlineProgress(1.0f - floatValue);
        reactionTabHolderView2.setOutlineProgress(floatValue);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$158(ReactedUsersListView reactedUsersListView, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("user_id", j);
        presentFragment(new ProfileActivity(bundle));
        closeMenu();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$159(ActionBarPopupWindow.ActionBarPopupWindowLayout actionBarPopupWindowLayout, int[] iArr, ReactedUsersListView reactedUsersListView, int i) {
        actionBarPopupWindowLayout.getSwipeBack().setNewForegroundHeight(iArr[0], AndroidUtilities.dp(52.0f) + i, true);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$160(ReactedUsersListView reactedUsersListView, ActionBarPopupWindow.ActionBarPopupWindowLayout actionBarPopupWindowLayout, int[] iArr, View view) {
        if (reactedUsersListView == null || reactedUsersListView.isLoaded) {
            actionBarPopupWindowLayout.getSwipeBack().openForeground(iArr[0]);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$161(MessageSeenView messageSeenView, View view, int i) {
        TLRPC$User tLRPC$User = messageSeenView.users.get(i);
        if (tLRPC$User != null) {
            Bundle bundle = new Bundle();
            bundle.putLong("user_id", tLRPC$User.id);
            presentFragment(new ProfileActivity(bundle));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$162(MessageObject messageObject, View view) {
        if (getMediaDataController().saveToRingtones(messageObject.getDocument())) {
            getUndoView().showWithAction(this.dialog_id, UndoView.ACTION_RINGTONE_ADDED, (Runnable) new Runnable() { // from class: org.telegram.ui.ChatActivity.105
                boolean clicked;

                @Override // java.lang.Runnable
                public void run() {
                    if (!this.clicked) {
                        this.clicked = true;
                        ChatActivity.this.presentFragment(new NotificationsSettingsActivity());
                    }
                }
            });
        }
        closeMenu(true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$163(View view) {
        closeMenu();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$168(boolean[] zArr, boolean[] zArr2, ImageView imageView, ImageView imageView2) {
        if (!zArr[0]) {
            zArr[0] = true;
            long[] jArr = {-1};
            ChatActivity$$ExternalSyntheticLambda205 chatActivity$$ExternalSyntheticLambda205 = new Runnable(jArr, zArr2, imageView, imageView2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda205
                public final /* synthetic */ long[] f$0;
                public final /* synthetic */ boolean[] f$1;
                public final /* synthetic */ ImageView f$2;
                public final /* synthetic */ ImageView f$3;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.lambda$createMenu$165(this.f$0, this.f$1, this.f$2, this.f$3);
                }
            };
            TLRPC$TL_messages_rateTranscribedAudio tLRPC$TL_messages_rateTranscribedAudio = new TLObject() { // from class: org.telegram.tgnet.TLRPC$TL_messages_rateTranscribedAudio
                public static int constructor = 2132608815;
                public boolean good;
                public int msg_id;
                public TLRPC$InputPeer peer;
                public long transcription_id;

                @Override // org.telegram.tgnet.TLObject
                public TLObject deserializeResponse(AbstractSerializedData abstractSerializedData, int i, boolean z) {
                    return TLRPC$Bool.TLdeserialize(abstractSerializedData, i, z);
                }

                @Override // org.telegram.tgnet.TLObject
                public void serializeToStream(AbstractSerializedData abstractSerializedData) {
                    abstractSerializedData.writeInt32(constructor);
                    this.peer.serializeToStream(abstractSerializedData);
                    abstractSerializedData.writeInt32(this.msg_id);
                    abstractSerializedData.writeInt64(this.transcription_id);
                    abstractSerializedData.writeBool(this.good);
                }
            };
            tLRPC$TL_messages_rateTranscribedAudio.msg_id = this.selectedObject.getId();
            tLRPC$TL_messages_rateTranscribedAudio.peer = getMessagesController().getInputPeer(this.selectedObject.messageOwner.peer_id);
            tLRPC$TL_messages_rateTranscribedAudio.transcription_id = this.selectedObject.messageOwner.voiceTranscriptionId;
            tLRPC$TL_messages_rateTranscribedAudio.good = zArr2[0];
            getConnectionsManager().sendRequest(tLRPC$TL_messages_rateTranscribedAudio, new RequestDelegate(chatActivity$$ExternalSyntheticLambda205, jArr) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda223
                public final /* synthetic */ Runnable f$1;
                public final /* synthetic */ long[] f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.telegram.tgnet.RequestDelegate
                public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                    ChatActivity.this.lambda$createMenu$167(this.f$1, this.f$2, tLObject, tLRPC$TL_error);
                }
            });
            AndroidUtilities.runOnUIThread(chatActivity$$ExternalSyntheticLambda205, 150);
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$165(long[] jArr, boolean[] zArr, ImageView imageView, ImageView imageView2) {
        jArr[0] = SystemClock.elapsedRealtime();
        if (!zArr[0]) {
            imageView = imageView2;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda18
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                ChatActivity.lambda$createMenu$164(CrossfadeDrawable.this, valueAnimator);
            }
        });
        ofFloat.setDuration(150L);
        ofFloat.setInterpolator(CubicBezierInterpolator.DEFAULT);
        ofFloat.start();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$164(CrossfadeDrawable crossfadeDrawable, ValueAnimator valueAnimator) {
        crossfadeDrawable.setProgress(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$167(Runnable runnable, long[] jArr, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.cancelRunOnUIThread(runnable);
        this.selectedObject.messageOwner.voiceTranscriptionRated = true;
        getMessagesStorage().updateMessageVoiceTranscriptionOpen(this.selectedObject.getDialogId(), this.selectedObject.getId(), this.selectedObject.messageOwner);
        ChatActivity$$ExternalSyntheticLambda154 chatActivity$$ExternalSyntheticLambda154 = new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda154
            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$createMenu$166();
            }
        };
        long j = 0;
        if (jArr[0] > 0) {
            j = Math.max(0L, 300 - (SystemClock.elapsedRealtime() - jArr[0]));
        }
        AndroidUtilities.runOnUIThread(chatActivity$$ExternalSyntheticLambda154, j);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$166() {
        closeMenu();
        BulletinFactory.of(this).createSimpleBulletin(R.raw.chats_infotip, LocaleController.getString("TranscriptionReportSent", R.string.TranscriptionReportSent)).show();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$169(boolean[] zArr, Runnable runnable, View view) {
        zArr[0] = true;
        runnable.run();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$170(boolean[] zArr, Runnable runnable, View view) {
        zArr[0] = false;
        runnable.run();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$171(View view) {
        if (this.contentView != null && getParentActivity() != null) {
            BottomSheet.Builder builder = new BottomSheet.Builder(this.contentView.getContext());
            builder.setCustomView(new SponsoredMessageInfoView(getParentActivity(), this.themeDelegate));
            builder.show();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$172(int i, ArrayList arrayList, View view) {
        if (this.selectedObject != null && i < arrayList.size()) {
            processSelectedOption(((Integer) arrayList.get(i)).intValue());
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean lambda$createMenu$173(View view, URLSpan uRLSpan) {
        didPressMessageUrl(uRLSpan, false, this.selectedObject, view instanceof ChatMessageCell ? (ChatMessageCell) view : null);
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$174(String[] strArr, String str, boolean z, ActionBarMenuSubItem actionBarMenuSubItem, AtomicBoolean atomicBoolean, AtomicReference atomicReference, String str2) {
        TLRPC$Chat tLRPC$Chat;
        strArr[0] = str2;
        if (strArr[0] != null && ((!strArr[0].equals(str) || strArr[0].equals("und")) && ((z && !RestrictedLanguagesSelectActivity.getRestrictedLanguages().contains(strArr[0])) || ((tLRPC$Chat = this.currentChat) != null && ((tLRPC$Chat.has_link || tLRPC$Chat.username != null) && ("uk".equals(strArr[0]) || "ru".equals(strArr[0]))))))) {
            actionBarMenuSubItem.setVisibility(0);
        }
        atomicBoolean.set(false);
        if (atomicReference.get() != null) {
            ((Runnable) atomicReference.get()).run();
            atomicReference.set(null);
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$175(AtomicBoolean atomicBoolean, AtomicReference atomicReference, Exception exc) {
        FileLog.e("mlkit: failed to detect language in message");
        atomicBoolean.set(false);
        if (atomicReference.get() != null) {
            ((Runnable) atomicReference.get()).run();
            atomicReference.set(null);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$177(int i, ArrayList arrayList, TLRPC$InputPeer tLRPC$InputPeer, int i2, String[] strArr, String str, CharSequence charSequence, boolean z, TranslateAlert.OnLinkPress onLinkPress, View view) {
        if (this.selectedObject != null && i < arrayList.size() && getParentActivity() != null) {
            TranslateAlert.showAlert(getParentActivity(), this, this.currentAccount, tLRPC$InputPeer, i2, strArr[0], str, charSequence, z, onLinkPress, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda117
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createMenu$176();
                }
            }).showDim(false);
            closeMenu(false);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$176() {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$createMenu$178(AtomicReference atomicReference) {
        if (atomicReference.get() != null) {
            ((Runnable) atomicReference.getAndSet(null)).run();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$180(int i, ArrayList arrayList, TLRPC$InputPeer tLRPC$InputPeer, int i2, String str, CharSequence charSequence, boolean z, TranslateAlert.OnLinkPress onLinkPress, View view) {
        if (this.selectedObject != null && i < arrayList.size() && getParentActivity() != null) {
            TranslateAlert.showAlert(getParentActivity(), this, this.currentAccount, tLRPC$InputPeer, i2, "und", str, charSequence, z, onLinkPress, new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda135
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createMenu$179();
                }
            }).showDim(false);
            closeMenu(false);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$179() {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$182(int i, int i2, boolean z, ReactionsContainerLayout reactionsContainerLayout) {
        ActionBarPopupWindow actionBarPopupWindow = this.scrimPopupWindow;
        if (actionBarPopupWindow != null && this.fragmentView != null && !actionBarPopupWindow.isShowing()) {
            this.scrimPopupWindow.showAtLocation(this.chatListView, 51, i, i2);
            if (z && reactionsContainerLayout != null) {
                reactionsContainerLayout.startEnterAnimation();
            }
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda150
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$createMenu$181();
                }
            }, 420);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$createMenu$181() {
        ActionBarMenuSubItem[] actionBarMenuSubItemArr = this.scrimPopupWindowItems;
        if (actionBarMenuSubItemArr != null && actionBarMenuSubItemArr.length > 0 && actionBarMenuSubItemArr[0] != null) {
            actionBarMenuSubItemArr[0].requestFocus();
            this.scrimPopupWindowItems[0].performAccessibilityAction(64, null);
            this.scrimPopupWindowItems[0].sendAccessibilityEvent(8);
        }
    }

    public void closeMenu() {
        closeMenu(true);
    }

    private void closeMenu(boolean z) {
        this.scrimPopupWindowHideDimOnDismiss = z;
        ActionBarPopupWindow actionBarPopupWindow = this.scrimPopupWindow;
        if (actionBarPopupWindow != null) {
            actionBarPopupWindow.dismiss();
        }
        if (!z) {
            ValueAnimator valueAnimator = this.scrimViewAlphaAnimator;
            if (valueAnimator != null) {
                valueAnimator.removeAllListeners();
                this.scrimViewAlphaAnimator.cancel();
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 0.0f);
            this.scrimViewAlphaAnimator = ofFloat;
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda4
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    ChatActivity.this.lambda$closeMenu$183(valueAnimator2);
                }
            });
            this.scrimViewAlphaAnimator.setDuration(150L);
            this.scrimViewAlphaAnimator.start();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$closeMenu$183(ValueAnimator valueAnimator) {
        this.scrimViewAlpha = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        SizeNotifierFrameLayout sizeNotifierFrameLayout = this.contentView;
        if (sizeNotifierFrameLayout != null) {
            sizeNotifierFrameLayout.invalidate();
            this.chatListView.invalidate();
        }
    }

    /* access modifiers changed from: private */
    public void selectReaction(final MessageObject messageObject, final ReactionsContainerLayout reactionsContainerLayout, final float f, final float f2, final TLRPC$TL_availableReaction tLRPC$TL_availableReaction, final boolean z, boolean z2) {
        int i;
        if (!isInScheduleMode()) {
            ReactionsEffectOverlay.removeCurrent(false);
            final boolean selectReaction = messageObject.selectReaction(tLRPC$TL_availableReaction.reaction, z2, z);
            final int id = messageObject.getId();
            if (this.groupedMessagesMap.get(messageObject.getGroupId()) != null) {
                MessageObject findMessageWithFlags = this.groupedMessagesMap.get(messageObject.getGroupId()).findMessageWithFlags(messageObject.shouldDrawReactionsInLayout() ? 9 : 10);
                if (findMessageWithFlags != null) {
                    id = findMessageWithFlags.getId();
                }
            }
            if (!selectReaction || z) {
                i = 1;
            } else {
                i = 1;
                ReactionsEffectOverlay.show(this, reactionsContainerLayout, findMessageCell(id, true), f, f2, tLRPC$TL_availableReaction.reaction, this.currentAccount, reactionsContainerLayout != null ? z2 ? 0 : 2 : 1);
            }
            if (selectReaction) {
                Object[] objArr = new Object[i];
                objArr[0] = tLRPC$TL_availableReaction.reaction;
                AndroidUtilities.makeAccessibilityAnnouncement(LocaleController.formatString("AccDescrYouReactedWith", R.string.AccDescrYouReactedWith, objArr));
            }
            SendMessagesHelper sendMessagesHelper = getSendMessagesHelper();
            String str = selectReaction ? tLRPC$TL_availableReaction.reaction : null;
            AnonymousClass111 r7 = new Runnable() { // from class: org.telegram.ui.ChatActivity.111
                @Override // java.lang.Runnable
                public void run() {
                    ChatActivity chatActivity = ChatActivity.this;
                    if (chatActivity.updateReactionRunnable != null) {
                        chatActivity.updateReactionRunnable = null;
                        if (z) {
                            chatActivity.lambda$openDiscussionMessageChat$227(new ChatActivity$111$$ExternalSyntheticLambda1(this, id, selectReaction, reactionsContainerLayout, f, f2, tLRPC$TL_availableReaction));
                        } else {
                            chatActivity.updateMessageAnimated(messageObject, true);
                            ReactionsEffectOverlay.startAnimation();
                        }
                        ChatActivity.this.closeMenu();
                    }
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$run$1(int i2, boolean z3, ReactionsContainerLayout reactionsContainerLayout2, float f3, float f4, TLRPC$TL_availableReaction tLRPC$TL_availableReaction2) {
                    AndroidUtilities.runOnUIThread(new ChatActivity$111$$ExternalSyntheticLambda0(this, i2, z3, reactionsContainerLayout2, f3, f4, tLRPC$TL_availableReaction2), 50);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void lambda$run$0(int i2, boolean z3, ReactionsContainerLayout reactionsContainerLayout2, float f3, float f4, TLRPC$TL_availableReaction tLRPC$TL_availableReaction2) {
                    ChatMessageCell findMessageCell = ChatActivity.this.findMessageCell(i2, true);
                    if (z3) {
                        ChatActivity chatActivity = ChatActivity.this;
                        ReactionsEffectOverlay.show(chatActivity, reactionsContainerLayout2, findMessageCell, f3, f4, tLRPC$TL_availableReaction2.reaction, ((BaseFragment) chatActivity).currentAccount, 1);
                        ReactionsEffectOverlay.startAnimation();
                    }
                }
            };
            this.updateReactionRunnable = r7;
            sendMessagesHelper.sendReaction(messageObject, str, z2, this, r7);
            if (z) {
                updateMessageAnimated(messageObject, true);
                this.updateReactionRunnable.run();
            }
            AndroidUtilities.runOnUIThread(this.updateReactionRunnable, 50);
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NotifyDataSetChanged"})
    public void updateMessageAnimated(MessageObject messageObject, boolean z) {
        if (this.chatAdapter != null) {
            getNotificationCenter().doOnIdle(new Runnable(messageObject, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda177
                public final /* synthetic */ MessageObject f$1;
                public final /* synthetic */ boolean f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$updateMessageAnimated$184(this.f$1, this.f$2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$updateMessageAnimated$184(MessageObject messageObject, boolean z) {
        if (this.fragmentView != null) {
            MessageObject.GroupedMessages groupedMessages = this.groupedMessagesMap.get(messageObject.getGroupId());
            if (groupedMessages != null) {
                ChatListItemAnimator chatListItemAnimator = this.chatListItemAnimator;
                if (chatListItemAnimator != null) {
                    chatListItemAnimator.groupWillChanged(groupedMessages);
                }
                for (int i = 0; i < groupedMessages.messages.size(); i++) {
                    groupedMessages.messages.get(i).forceUpdate = true;
                    if (z) {
                        groupedMessages.messages.get(i).reactionsChanged = true;
                    }
                }
                this.chatAdapter.notifyDataSetChanged(true);
                return;
            }
            int indexOf = this.messages.indexOf(messageObject);
            if (indexOf >= 0) {
                ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
                chatActivityAdapter.notifyItemChanged(chatActivityAdapter.messagesStartRow + indexOf);
            }
        }
    }

    public ChatMessageCell findMessageCell(int i, boolean z) {
        RecyclerListView recyclerListView = this.chatListView;
        if (recyclerListView == null) {
            return null;
        }
        int childCount = recyclerListView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = this.chatListView.getChildAt(i2);
            if ((this.chatListView.getChildAt(i2) instanceof ChatMessageCell) && ((ChatMessageCell) this.chatListView.getChildAt(i2)).getMessageObject().getId() == i) {
                if (z) {
                    if (childAt.getY() + ((float) childAt.getMeasuredHeight()) < (this.chatListViewPaddingTop - ((float) this.chatListViewPaddingVisibleOffset)) - ((float) AndroidUtilities.dp(4.0f)) || childAt.getY() > ((float) (this.chatListView.getMeasuredHeight() - this.blurredViewBottomOffset))) {
                        return null;
                    }
                }
                return (ChatMessageCell) this.chatListView.getChildAt(i2);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void startEditingMessageObject(MessageObject messageObject) {
        if (messageObject != null && getParentActivity() != null) {
            if (this.searchItem != null && this.actionBar.isSearchFieldVisible()) {
                this.actionBar.closeSearchField();
                this.chatActivityEnterView.setFieldFocused();
            }
            this.mentionContainer.getAdapter().setNeedBotContext(false);
            this.chatActivityEnterView.setVisibility(0);
            showFieldPanelForEdit(true, messageObject);
            updateBottomOverlay();
            checkEditTimer();
            this.chatActivityEnterView.setAllowStickersAndGifs(false, false, true);
            updatePinnedMessageView(true);
            updateVisibleRows();
            if (!messageObject.scheduled) {
                TLRPC$TL_messages_getMessageEditData tLRPC$TL_messages_getMessageEditData = new TLObject() { // from class: org.telegram.tgnet.TLRPC$TL_messages_getMessageEditData
                    public static int constructor = -39416522;
                    public int id;
                    public TLRPC$InputPeer peer;

                    @Override // org.telegram.tgnet.TLObject
                    public TLObject deserializeResponse(AbstractSerializedData abstractSerializedData, int i, boolean z) {
                        return TLRPC$TL_messages_messageEditData.TLdeserialize(abstractSerializedData, i, z);
                    }

                    @Override // org.telegram.tgnet.TLObject
                    public void serializeToStream(AbstractSerializedData abstractSerializedData) {
                        abstractSerializedData.writeInt32(constructor);
                        this.peer.serializeToStream(abstractSerializedData);
                        abstractSerializedData.writeInt32(this.id);
                    }
                };
                tLRPC$TL_messages_getMessageEditData.peer = getMessagesController().getInputPeer(this.dialog_id);
                tLRPC$TL_messages_getMessageEditData.id = messageObject.getId();
                this.editingMessageObjectReqId = getConnectionsManager().sendRequest(tLRPC$TL_messages_getMessageEditData, new RequestDelegate() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda216
                    @Override // org.telegram.tgnet.RequestDelegate
                    public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                        ChatActivity.this.lambda$startEditingMessageObject$186(tLObject, tLRPC$TL_error);
                    }
                });
                return;
            }
            this.chatActivityEnterView.showEditDoneProgress(false, true);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$startEditingMessageObject$186(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda185
            public final /* synthetic */ TLObject f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$startEditingMessageObject$185(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$startEditingMessageObject$185(TLObject tLObject) {
        this.editingMessageObjectReqId = 0;
        if (tLObject == null && getParentActivity() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
            builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
            builder.setMessage(LocaleController.getString("EditMessageError", R.string.EditMessageError));
            builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), null);
            showDialog(builder.create());
            ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
            if (chatActivityEnterView != null) {
                chatActivityEnterView.setEditingMessageObject(null, false);
                hideFieldPanel(true);
            }
        }
    }

    public void restartSticker(ChatMessageCell chatMessageCell) {
        MessagesController.EmojiSound emojiSound;
        MessageObject messageObject = chatMessageCell.getMessageObject();
        TLRPC$Document document = messageObject.getDocument();
        boolean isAnimatedEmoji = messageObject.isAnimatedEmoji();
        boolean z = true;
        if (!isAnimatedEmoji) {
            if (this.currentEncryptedChat != null && !messageObject.isOut()) {
                z = false;
            }
            if (!MessageObject.isAnimatedStickerDocument(document, z) || SharedConfig.loopStickers) {
                return;
            }
        }
        RLottieDrawable lottieAnimation = chatMessageCell.getPhotoImage().getLottieAnimation();
        if (lottieAnimation != null) {
            if (isAnimatedEmoji) {
                String stickerEmoji = messageObject.getStickerEmoji();
                if (EmojiData.isHeartEmoji(stickerEmoji)) {
                    HashMap<Integer, Integer> hashMap = new HashMap<>();
                    hashMap.put(1, 1);
                    hashMap.put(13, 0);
                    hashMap.put(59, 1);
                    hashMap.put(71, 0);
                    hashMap.put(Integer.valueOf((int) ConnectionsManager.RequestFlagNeedQuickAck), 1);
                    hashMap.put(140, 0);
                    lottieAnimation.setVibrationPattern(hashMap);
                } else if (EmojiData.isPeachEmoji(stickerEmoji)) {
                    HashMap<Integer, Integer> hashMap2 = new HashMap<>();
                    hashMap2.put(34, 1);
                    lottieAnimation.setVibrationPattern(hashMap2);
                } else if (EmojiData.isCofinEmoji(stickerEmoji)) {
                    HashMap<Integer, Integer> hashMap3 = new HashMap<>();
                    hashMap3.put(24, 0);
                    hashMap3.put(36, 0);
                    lottieAnimation.setVibrationPattern(hashMap3);
                }
                if (!(lottieAnimation.isRunning() || stickerEmoji == null || (emojiSound = getMessagesController().emojiSounds.get(stickerEmoji.replace("️", ""))) == null)) {
                    getMediaController().playEmojiSound(getAccountInstance(), stickerEmoji, emojiSound, false);
                }
            }
            lottieAnimation.restart();
        }
    }

    /* access modifiers changed from: private */
    public String getMessageContent(MessageObject messageObject, long j, boolean z) {
        TLRPC$Chat chat;
        String str = "";
        if (z) {
            long fromChatId = messageObject.getFromChatId();
            if (j != fromChatId) {
                if (fromChatId > 0) {
                    TLRPC$User user = getMessagesController().getUser(Long.valueOf(fromChatId));
                    if (user != null) {
                        str = ContactsController.formatName(user.first_name, user.last_name) + ":\n";
                    }
                } else if (fromChatId < 0 && (chat = getMessagesController().getChat(Long.valueOf(-fromChatId))) != null) {
                    str = chat.title + ":\n";
                }
            }
        }
        String restrictionReason = MessagesController.getRestrictionReason(messageObject.messageOwner.restriction_reason);
        if (!TextUtils.isEmpty(restrictionReason)) {
            return str + restrictionReason;
        } else if ((messageObject.type == 0 || messageObject.isAnimatedEmoji()) && messageObject.messageOwner.message != null) {
            return str + messageObject.messageOwner.message;
        } else {
            TLRPC$Message tLRPC$Message = messageObject.messageOwner;
            if (tLRPC$Message.media == null || tLRPC$Message.message == null) {
                return str + ((Object) messageObject.messageText);
            }
            return str + messageObject.messageOwner.message;
        }
    }

    /* access modifiers changed from: private */
    public void unpinMessage(MessageObject messageObject) {
        if (messageObject != null) {
            Bulletin bulletin = this.pinBulletin;
            if (bulletin != null) {
                bulletin.hide(false, 0L);
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(this.selectedObject);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Integer.valueOf(messageObject.getId()));
            int i = this.totalPinnedMessagesCount;
            getNotificationCenter().postNotificationName(NotificationCenter.didLoadPinnedMessages, Long.valueOf(this.dialog_id), arrayList2, Boolean.FALSE, 0, 0, 0, Integer.valueOf(this.totalPinnedMessagesCount - 1), Boolean.valueOf(this.pinnedEndReached));
            this.pinBulletin = BulletinFactory.createUnpinMessageBulletin(this, new Runnable(arrayList2, arrayList, i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda172
                public final /* synthetic */ ArrayList f$1;
                public final /* synthetic */ ArrayList f$2;
                public final /* synthetic */ int f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$unpinMessage$187(this.f$1, this.f$2, this.f$3);
                }
            }, new Runnable(messageObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda173
                public final /* synthetic */ MessageObject f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$unpinMessage$188(this.f$1);
                }
            }, this.themeDelegate).show();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$unpinMessage$187(ArrayList arrayList, ArrayList arrayList2, int i) {
        getNotificationCenter().postNotificationName(NotificationCenter.didLoadPinnedMessages, Long.valueOf(this.dialog_id), arrayList, Boolean.TRUE, arrayList2, 0, 0, Integer.valueOf(i), Boolean.valueOf(this.pinnedEndReached));
        this.pinBulletin = null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$unpinMessage$188(MessageObject messageObject) {
        getMessagesController().pinMessage(this.currentChat, this.currentUser, messageObject.getId(), true, false, false);
        this.pinBulletin = null;
    }

    public void openReportChat(int i) {
        Bundle bundle = new Bundle();
        if (DialogObject.isUserDialog(this.dialog_id)) {
            bundle.putLong("user_id", this.dialog_id);
        } else {
            bundle.putLong("chat_id", -this.dialog_id);
        }
        bundle.putInt("report", i);
        ChatActivity chatActivity = new ChatActivity(bundle);
        presentFragment(chatActivity);
        chatActivity.chatActivityDelegate = new ChatActivityDelegate() { // from class: org.telegram.ui.ChatActivity.112
            @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
            public /* synthetic */ void onUnpin(boolean z, boolean z2) {
                ChatActivityDelegate.CC.$default$onUnpin(this, z, z2);
            }

            @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
            public /* synthetic */ void openReplyMessage(int i2) {
                ChatActivityDelegate.CC.$default$openReplyMessage(this, i2);
            }

            @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
            public /* synthetic */ void openSearch(String str) {
                ChatActivityDelegate.CC.$default$openSearch(this, str);
            }

            @Override // org.telegram.ui.ChatActivity.ChatActivityDelegate
            public void onReport() {
                ChatActivity.this.undoView.showWithAction(0L, 74, (Runnable) null);
            }
        };
    }

    private void saveMessageToGallery(MessageObject messageObject) {
        String str = messageObject.messageOwner.attachPath;
        if (!TextUtils.isEmpty(str) && !new File(str).exists()) {
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            str = FileLoader.getInstance(this.currentAccount).getPathToMessage(messageObject.messageOwner).toString();
        }
        MediaController.saveFile(str, getParentActivity(), messageObject.isVideo() ? 1 : 0, null, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:234:0x0740, code lost:
        if (r0.exists() != false) goto L_0x0744;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processSelectedOption(int r23) {
        /*
        // Method dump skipped, instructions count: 2733
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.processSelectedOption(int):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$189(DialogInterface dialogInterface) {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$190(DialogInterface dialogInterface) {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$191() {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$192(boolean z, int i) {
        if (getParentActivity() != null && this.fragmentView != null && i > 0) {
            BulletinFactory.of(this).createDownloadBulletin(z ? BulletinFactory.FileType.AUDIOS : BulletinFactory.FileType.UNKNOWNS, i, this.themeDelegate).show();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$193(boolean z, boolean z2, boolean z3) {
        BulletinFactory.FileType fileType;
        if (getParentActivity() != null) {
            if (z) {
                fileType = BulletinFactory.FileType.PHOTO_TO_DOWNLOADS;
            } else if (z2) {
                fileType = BulletinFactory.FileType.VIDEO_TO_DOWNLOADS;
            } else if (z3) {
                fileType = BulletinFactory.FileType.GIF_TO_DOWNLOADS;
            } else {
                fileType = BulletinFactory.FileType.UNKNOWN;
            }
            BulletinFactory.of(this).createDownloadBulletin(fileType, this.themeDelegate).show();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$194(DialogInterface dialogInterface) {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$processSelectedOption$195(boolean[] zArr, View view) {
        zArr[1] = !zArr[1];
        ((CheckBoxCell) view).setChecked(zArr[1], true);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$processSelectedOption$196(boolean[] zArr, View view) {
        zArr[0] = !zArr[0];
        ((CheckBoxCell) view).setChecked(zArr[0], true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$198(int i, boolean[] zArr, DialogInterface dialogInterface, int i2) {
        getMessagesController().pinMessage(this.currentChat, this.currentUser, i, false, !zArr[1], zArr[0]);
        Bulletin createPinMessageBulletin = BulletinFactory.createPinMessageBulletin(this, this.themeDelegate);
        createPinMessageBulletin.show();
        Bulletin.Layout layout = createPinMessageBulletin.getLayout();
        layout.postDelayed(new Runnable(layout) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda114
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.f$0.performHapticFeedback(3, 2);
            }
        }, 550);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$200(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda186
            public final /* synthetic */ TLObject f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$processSelectedOption$199(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$199(TLObject tLObject) {
        if (tLObject != null) {
            TLRPC$TL_exportedMessageLink tLRPC$TL_exportedMessageLink = (TLRPC$TL_exportedMessageLink) tLObject;
            try {
                ((ClipboardManager) ApplicationLoader.applicationContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("label", tLRPC$TL_exportedMessageLink.link));
                if (BulletinFactory.canShowBulletin(this)) {
                    BulletinFactory.of(this).createCopyLinkBulletin(!isThreadChat() && tLRPC$TL_exportedMessageLink.link.contains("/c/"), this.themeDelegate).show();
                }
            } catch (Exception e) {
                FileLog.e(e);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$201() {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$202() {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$processSelectedOption$203(AlertDialog[] alertDialogArr) {
        try {
            alertDialogArr[0].dismiss();
        } catch (Throwable unused) {
        }
        alertDialogArr[0] = null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$205(AlertDialog[] alertDialogArr, int i) {
        if (alertDialogArr[0] != null) {
            alertDialogArr[0].setOnCancelListener(new DialogInterface.OnCancelListener(i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda21
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    ChatActivity.this.lambda$processSelectedOption$204(this.f$1, dialogInterface);
                }
            });
            showDialog(alertDialogArr[0]);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$204(int i, DialogInterface dialogInterface) {
        getConnectionsManager().cancelRequest(i, true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$206(DialogInterface dialogInterface) {
        dimBehindView(false);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$212(MessageObject messageObject, DialogInterface dialogInterface, int i) {
        AlertDialog[] alertDialogArr = {new AlertDialog(getParentActivity(), 3, this.themeDelegate)};
        TLRPC$TL_messages_editMessage tLRPC$TL_messages_editMessage = new TLRPC$TL_messages_editMessage();
        TLRPC$TL_inputMediaPoll tLRPC$TL_inputMediaPoll = new TLRPC$TL_inputMediaPoll();
        TLRPC$TL_poll tLRPC$TL_poll = new TLRPC$TL_poll();
        tLRPC$TL_inputMediaPoll.poll = tLRPC$TL_poll;
        TLRPC$Poll tLRPC$Poll = ((TLRPC$TL_messageMediaPoll) messageObject.messageOwner.media).poll;
        tLRPC$TL_poll.id = tLRPC$Poll.id;
        tLRPC$TL_poll.question = tLRPC$Poll.question;
        tLRPC$TL_poll.answers = tLRPC$Poll.answers;
        tLRPC$TL_poll.closed = true;
        tLRPC$TL_messages_editMessage.media = tLRPC$TL_inputMediaPoll;
        tLRPC$TL_messages_editMessage.peer = getMessagesController().getInputPeer(this.dialog_id);
        tLRPC$TL_messages_editMessage.id = messageObject.getId();
        tLRPC$TL_messages_editMessage.flags |= 16384;
        AndroidUtilities.runOnUIThread(new Runnable(alertDialogArr, getConnectionsManager().sendRequest(tLRPC$TL_messages_editMessage, new RequestDelegate(alertDialogArr, tLRPC$TL_messages_editMessage) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda231
            public final /* synthetic */ AlertDialog[] f$1;
            public final /* synthetic */ TLRPC$TL_messages_editMessage f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.telegram.tgnet.RequestDelegate
            public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                ChatActivity.this.lambda$processSelectedOption$209(this.f$1, this.f$2, tLObject, tLRPC$TL_error);
            }
        })) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda200
            public final /* synthetic */ AlertDialog[] f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$processSelectedOption$211(this.f$1, this.f$2);
            }
        }, 500);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$209(AlertDialog[] alertDialogArr, TLRPC$TL_messages_editMessage tLRPC$TL_messages_editMessage, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(alertDialogArr) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda207
            public final /* synthetic */ AlertDialog[] f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.lambda$processSelectedOption$207(this.f$0);
            }
        });
        if (tLRPC$TL_error == null) {
            getMessagesController().processUpdates((TLRPC$Updates) tLObject, false);
        } else {
            AndroidUtilities.runOnUIThread(new Runnable(tLRPC$TL_error, tLRPC$TL_messages_editMessage) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda191
                public final /* synthetic */ TLRPC$TL_error f$1;
                public final /* synthetic */ TLRPC$TL_messages_editMessage f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$processSelectedOption$208(this.f$1, this.f$2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$processSelectedOption$207(AlertDialog[] alertDialogArr) {
        try {
            alertDialogArr[0].dismiss();
        } catch (Throwable unused) {
        }
        alertDialogArr[0] = null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$208(TLRPC$TL_error tLRPC$TL_error, TLRPC$TL_messages_editMessage tLRPC$TL_messages_editMessage) {
        AlertsCreator.processError(this.currentAccount, tLRPC$TL_error, this, tLRPC$TL_messages_editMessage, new Object[0]);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$211(AlertDialog[] alertDialogArr, int i) {
        if (alertDialogArr[0] != null) {
            alertDialogArr[0].setOnCancelListener(new DialogInterface.OnCancelListener(i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda20
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    ChatActivity.this.lambda$processSelectedOption$210(this.f$1, dialogInterface);
                }
            });
            showDialog(alertDialogArr[0]);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$210(int i, DialogInterface dialogInterface) {
        getConnectionsManager().cancelRequest(i, true);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$215(TLRPC$TL_messages_sendScheduledMessages tLRPC$TL_messages_sendScheduledMessages, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        if (tLRPC$TL_error == null) {
            getMessagesController().processUpdates((TLRPC$Updates) tLObject, false);
            AndroidUtilities.runOnUIThread(new Runnable(tLRPC$TL_messages_sendScheduledMessages) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda193
                public final /* synthetic */ TLRPC$TL_messages_sendScheduledMessages f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$processSelectedOption$213(this.f$1);
                }
            });
        } else if (tLRPC$TL_error.text != null) {
            AndroidUtilities.runOnUIThread(new Runnable(tLRPC$TL_error) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda189
                public final /* synthetic */ TLRPC$TL_error f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$processSelectedOption$214(this.f$1);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$213(TLRPC$TL_messages_sendScheduledMessages tLRPC$TL_messages_sendScheduledMessages) {
        NotificationCenter.getInstance(this.currentAccount).postNotificationName(NotificationCenter.messagesDeleted, tLRPC$TL_messages_sendScheduledMessages.id, Long.valueOf(-this.dialog_id), Boolean.TRUE, Long.valueOf(this.dialog_id));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$214(TLRPC$TL_error tLRPC$TL_error) {
        if (tLRPC$TL_error.text.startsWith("SLOWMODE_WAIT_")) {
            AlertsCreator.showSimpleToast(this, LocaleController.getString("SlowmodeSendError", R.string.SlowmodeSendError));
        } else if (tLRPC$TL_error.text.equals("CHAT_SEND_MEDIA_FORBIDDEN")) {
            AlertsCreator.showSimpleToast(this, LocaleController.getString("AttachMediaRestrictedForever", R.string.AttachMediaRestrictedForever));
        } else {
            AlertsCreator.showSimpleToast(this, tLRPC$TL_error.text);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$216(MessageObject.GroupedMessages groupedMessages, MessageObject messageObject, boolean z, int i) {
        if (groupedMessages == null || groupedMessages.messages.isEmpty()) {
            SendMessagesHelper.getInstance(this.currentAccount).editMessage(messageObject, null, false, this, null, i);
        } else {
            SendMessagesHelper.getInstance(this.currentAccount).editMessage(groupedMessages.messages.get(0), null, false, this, null, i);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processSelectedOption$217(DialogInterface dialogInterface) {
        dimBehindView(false);
    }

    @Override // org.telegram.ui.DialogsActivity.DialogsActivityDelegate
    public void didSelectDialogs(DialogsActivity dialogsActivity, ArrayList<Long> arrayList, CharSequence charSequence, boolean z) {
        if (this.forwardingMessage != null || this.selectedMessagesIds[0].size() != 0 || this.selectedMessagesIds[1].size() != 0) {
            ArrayList<MessageObject> arrayList2 = new ArrayList<>();
            MessageObject messageObject = this.forwardingMessage;
            if (messageObject != null) {
                MessageObject.GroupedMessages groupedMessages = this.forwardingMessageGroup;
                if (groupedMessages != null) {
                    arrayList2.addAll(groupedMessages.messages);
                } else {
                    arrayList2.add(messageObject);
                }
                this.forwardingMessage = null;
                this.forwardingMessageGroup = null;
            } else {
                for (int i = 1; i >= 0; i--) {
                    ArrayList arrayList3 = new ArrayList();
                    for (int i2 = 0; i2 < this.selectedMessagesIds[i].size(); i2++) {
                        arrayList3.add(Integer.valueOf(this.selectedMessagesIds[i].keyAt(i2)));
                    }
                    Collections.sort(arrayList3);
                    for (int i3 = 0; i3 < arrayList3.size(); i3++) {
                        MessageObject messageObject2 = this.selectedMessagesIds[i].get(((Integer) arrayList3.get(i3)).intValue());
                        if (messageObject2 != null) {
                            arrayList2.add(messageObject2);
                        }
                    }
                    this.selectedMessagesCanCopyIds[i].clear();
                    this.selectedMessagesCanStarIds[i].clear();
                    this.selectedMessagesIds[i].clear();
                }
                hideActionMode();
                updatePinnedMessageView(true);
                updateVisibleRows();
            }
            if (arrayList.size() > 1 || arrayList.get(0).longValue() == getUserConfig().getClientUserId() || charSequence != null) {
                this.forwardingMessages = null;
                hideFieldPanel(false);
                for (int i4 = 0; i4 < arrayList.size(); i4++) {
                    long longValue = arrayList.get(i4).longValue();
                    if (charSequence != null) {
                        getSendMessagesHelper().sendMessage(charSequence.toString(), longValue, null, null, null, true, null, null, null, true, 0, null);
                    }
                    getSendMessagesHelper().sendMessage(arrayList2, longValue, false, false, true, 0);
                }
                dialogsActivity.finishFragment();
                if (arrayList.size() == 1) {
                    this.undoView.showWithAction(arrayList.get(0).longValue(), 53, Integer.valueOf(arrayList2.size()));
                } else {
                    this.undoView.showWithAction(0, 53, Integer.valueOf(arrayList2.size()), Integer.valueOf(arrayList.size()), (Runnable) null, (Runnable) null);
                }
            } else {
                long longValue2 = arrayList.get(0).longValue();
                if (longValue2 != this.dialog_id || this.chatMode == 2) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("scrollToTopOnResume", this.scrollToTopOnResume);
                    if (DialogObject.isEncryptedDialog(longValue2)) {
                        bundle.putInt("enc_id", DialogObject.getEncryptedChatId(longValue2));
                    } else {
                        if (DialogObject.isUserDialog(longValue2)) {
                            bundle.putLong("user_id", longValue2);
                        } else {
                            bundle.putLong("chat_id", -longValue2);
                        }
                        if (!getMessagesController().checkCanOpenChat(bundle, dialogsActivity)) {
                            return;
                        }
                    }
                    addToPulledDialogsMyself();
                    ChatActivity chatActivity = new ChatActivity(bundle);
                    if (presentFragment(chatActivity, true)) {
                        chatActivity.showFieldPanelForForward(true, arrayList2);
                        if (!AndroidUtilities.isTablet()) {
                            removeSelfFromStack();
                            return;
                        }
                        return;
                    }
                    dialogsActivity.finishFragment();
                    return;
                }
                dialogsActivity.finishFragment();
                moveScrollToLastMessage(false);
                showFieldPanelForForward(true, arrayList2);
                if (AndroidUtilities.isTablet()) {
                    hideActionMode();
                    updatePinnedMessageView(true);
                }
                updateVisibleRows();
            }
        }
    }

    public boolean checkRecordLocked(boolean z) {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView == null || !chatActivityEnterView.isRecordLocked()) {
            return false;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
        if (this.chatActivityEnterView.isInVideoMode()) {
            builder.setTitle(LocaleController.getString("DiscardVideoMessageTitle", R.string.DiscardVideoMessageTitle));
            builder.setMessage(LocaleController.getString("DiscardVideoMessageDescription", R.string.DiscardVideoMessageDescription));
        } else {
            builder.setTitle(LocaleController.getString("DiscardVoiceMessageTitle", R.string.DiscardVoiceMessageTitle));
            builder.setMessage(LocaleController.getString("DiscardVoiceMessageDescription", R.string.DiscardVoiceMessageDescription));
        }
        builder.setPositiveButton(LocaleController.getString("DiscardVoiceMessageAction", R.string.DiscardVoiceMessageAction), new DialogInterface.OnClickListener(z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda43
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatActivity.this.lambda$checkRecordLocked$218(this.f$1, dialogInterface, i);
            }
        });
        builder.setNegativeButton(LocaleController.getString("Continue", R.string.Continue), null);
        showDialog(builder.create());
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$checkRecordLocked$218(boolean z, DialogInterface dialogInterface, int i) {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView == null) {
            return;
        }
        if (z) {
            finishFragment();
        } else {
            chatActivityEnterView.cancelRecordingAudioVideo();
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public boolean onBackPressed() {
        if (ContentPreviewViewer.getInstance().isVisible()) {
            ContentPreviewViewer.getInstance().closeWithMenu();
            return false;
        }
        ForwardingPreviewView forwardingPreviewView = this.forwardingPreviewView;
        if (forwardingPreviewView != null && forwardingPreviewView.isShowing()) {
            this.forwardingPreviewView.dismiss(true);
            return false;
        } else if (this.messagesSearchListView.getTag() != null) {
            showMessagesSearchListView(false);
            return false;
        } else if (this.scrimPopupWindow != null) {
            closeMenu();
            return false;
        } else if (checkRecordLocked(false)) {
            return false;
        } else {
            if (this.textSelectionHelper.isSelectionMode()) {
                this.textSelectionHelper.clear();
                return false;
            }
            ActionBar actionBar = this.actionBar;
            if (actionBar == null || !actionBar.isActionModeShowed()) {
                ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
                if (chatActivityEnterView != null && chatActivityEnterView.isPopupShowing()) {
                    return !this.chatActivityEnterView.hidePopup(true);
                }
                ChatActivityEnterView chatActivityEnterView2 = this.chatActivityEnterView;
                if (chatActivityEnterView2 != null && chatActivityEnterView2.hasBotWebView() && this.chatActivityEnterView.botCommandsMenuIsShowing() && this.chatActivityEnterView.onBotWebViewBackPressed()) {
                    return false;
                }
                ChatActivityEnterView chatActivityEnterView3 = this.chatActivityEnterView;
                if (chatActivityEnterView3 == null || !chatActivityEnterView3.botCommandsMenuIsShowing()) {
                    if (this.backToPreviousFragment != null) {
                        ArrayList<BaseFragment> arrayList = this.parentLayout.fragmentsStack;
                        arrayList.add(arrayList.size() - 1, this.backToPreviousFragment);
                        this.backToPreviousFragment = null;
                    }
                    return true;
                }
                this.chatActivityEnterView.hideBotCommands();
                return false;
            }
            clearSelectionMode();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void clearSelectionMode() {
        for (int i = 1; i >= 0; i--) {
            this.selectedMessagesIds[i].clear();
            this.selectedMessagesCanCopyIds[i].clear();
            this.selectedMessagesCanStarIds[i].clear();
        }
        hideActionMode();
        updatePinnedMessageView(true);
        updateVisibleRows();
    }

    public void onListItemAnimatorTick() {
        invalidateMessagesVisiblePart();
        if (this.scrimView != null) {
            this.fragmentView.invalidate();
        }
    }

    public void setThreadMessages(ArrayList<MessageObject> arrayList, TLRPC$Chat tLRPC$Chat, int i, int i2, int i3) {
        this.threadMessageObjects = arrayList;
        boolean z = true;
        MessageObject messageObject = arrayList.get(arrayList.size() - 1);
        this.threadMessageObject = messageObject;
        this.replyingMessageObject = messageObject;
        this.threadMaxInboxReadId = i2;
        this.threadMaxOutboxReadId = i3;
        this.replyMaxReadId = Math.max(1, i2);
        this.threadMessageId = this.threadMessageObject.getId();
        this.replyOriginalMessageId = i;
        this.replyOriginalChat = tLRPC$Chat;
        TLRPC$MessageFwdHeader tLRPC$MessageFwdHeader = this.replyingMessageObject.messageOwner.fwd_from;
        if (tLRPC$MessageFwdHeader == null || tLRPC$MessageFwdHeader.channel_post == 0) {
            z = false;
        }
        this.isComments = z;
    }

    public void setHighlightMessageId(int i) {
        this.highlightMessageId = i;
    }

    public boolean isThreadChat() {
        return this.threadMessageObject != null;
    }

    public boolean isReplyChatComment() {
        return this.threadMessageObject != null && this.isComments;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0175  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void updateVisibleRows() {
        /*
        // Method dump skipped, instructions count: 409
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.updateVisibleRows():void");
    }

    /* access modifiers changed from: private */
    public void checkEditTimer() {
        MessageObject editingMessageObject;
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null && (editingMessageObject = chatActivityEnterView.getEditingMessageObject()) != null && !editingMessageObject.scheduled) {
            TLRPC$User tLRPC$User = this.currentUser;
            if (tLRPC$User == null || !tLRPC$User.self) {
                int abs = editingMessageObject.canEditMessageAnytime(this.currentChat) ? 360 : (getMessagesController().maxEditTime + 300) - Math.abs(getConnectionsManager().getCurrentTime() - editingMessageObject.messageOwner.date);
                if (abs > 0) {
                    if (abs <= 300) {
                        this.replyObjectTextView.setText(LocaleController.formatString("TimeToEdit", R.string.TimeToEdit, AndroidUtilities.formatShortDuration(abs)));
                    }
                    AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda132
                        @Override // java.lang.Runnable
                        public final void run() {
                            ChatActivity.this.checkEditTimer();
                        }
                    }, 1000);
                    return;
                }
                this.chatActivityEnterView.onEditTimeExpired();
                this.replyObjectTextView.setText(LocaleController.formatString("TimeToEditExpired", R.string.TimeToEditExpired, new Object[0]));
            }
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<MessageObject> createVoiceMessagesPlaylist(MessageObject messageObject, boolean z) {
        ArrayList<MessageObject> arrayList = new ArrayList<>();
        arrayList.add(messageObject);
        int id = messageObject.getId();
        messageObject.getDialogId();
        if (id != 0) {
            for (int size = this.messages.size() - 1; size >= 0; size--) {
                MessageObject messageObject2 = this.messages.get(size);
                if ((messageObject2.getDialogId() != this.mergeDialogId || messageObject.getDialogId() == this.mergeDialogId) && (((this.currentEncryptedChat == null && messageObject2.getId() > id) || (this.currentEncryptedChat != null && messageObject2.getId() < id)) && ((messageObject2.isVoice() || messageObject2.isRoundVideo()) && (!z || (messageObject2.isContentUnread() && !messageObject2.isOut()))))) {
                    arrayList.add(messageObject2);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void alertUserOpenError(MessageObject messageObject) {
        if (getParentActivity() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
            builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
            builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), null);
            if (messageObject.type == 3) {
                builder.setMessage(LocaleController.getString("NoPlayerInstalled", R.string.NoPlayerInstalled));
            } else {
                builder.setMessage(LocaleController.formatString("NoHandleAppInstalled", R.string.NoHandleAppInstalled, messageObject.getDocument().mime_type));
            }
            showDialog(builder.create());
        }
    }

    /* access modifiers changed from: private */
    public void openSearchWithText(String str) {
        ActionBarMenuItem actionBarMenuItem;
        if (!this.actionBar.isSearchFieldVisible()) {
            AndroidUtilities.updateViewVisibilityAnimated(this.avatarContainer, false, 0.95f, true);
            ActionBarMenuItem actionBarMenuItem2 = this.headerItem;
            if (actionBarMenuItem2 != null) {
                actionBarMenuItem2.setVisibility(8);
            }
            ActionBarMenuItem actionBarMenuItem3 = this.attachItem;
            if (actionBarMenuItem3 != null) {
                actionBarMenuItem3.setVisibility(8);
            }
            ActionBarMenuItem actionBarMenuItem4 = this.editTextItem;
            if (actionBarMenuItem4 != null) {
                actionBarMenuItem4.setVisibility(8);
            }
            if (this.threadMessageId == 0 && (actionBarMenuItem = this.searchItem) != null) {
                actionBarMenuItem.setVisibility(0);
            }
            ActionBarMenuItem actionBarMenuItem5 = this.searchIconItem;
            if (actionBarMenuItem5 != null && this.showSearchAsIcon) {
                actionBarMenuItem5.setVisibility(8);
            }
            ActionBarMenuItem actionBarMenuItem6 = this.audioCallIconItem;
            if (actionBarMenuItem6 != null && this.showAudioCallAsIcon) {
                actionBarMenuItem6.setVisibility(8);
            }
            this.searchItemVisible = true;
            updateSearchButtons(0, 0, -1);
            updateBottomOverlay();
        }
        if (this.threadMessageId == 0 && !UserObject.isReplyUser(this.currentUser)) {
            boolean z = str == null;
            this.openSearchKeyboard = z;
            ActionBarMenuItem actionBarMenuItem7 = this.searchItem;
            if (actionBarMenuItem7 != null) {
                actionBarMenuItem7.openSearch(z);
            }
        }
        if (str != null) {
            ActionBarMenuItem actionBarMenuItem8 = this.searchItem;
            if (actionBarMenuItem8 != null) {
                actionBarMenuItem8.setSearchFieldText(str, false);
            }
            getMediaDataController().searchMessagesInChat(str, this.dialog_id, this.mergeDialogId, this.classGuid, 0, this.threadMessageId, this.searchingUserMessages, this.searchingChatMessages);
        }
        updatePinnedMessageView(true);
    }

    @Override // org.telegram.ui.LocationActivity.LocationActivityDelegate
    public void didSelectLocation(TLRPC$MessageMedia tLRPC$MessageMedia, int i, boolean z, int i2) {
        getSendMessagesHelper().sendMessage(tLRPC$MessageMedia, this.dialog_id, this.replyingMessageObject, getThreadMessage(), (TLRPC$ReplyMarkup) null, (HashMap<String, String>) null, z, i2);
        if (this.chatMode == 0) {
            moveScrollToLastMessage(false);
        }
        if (i == 0 || i == 1) {
            afterMessageSend();
        }
        if (this.paused) {
            this.scrollToTopOnResume = true;
        }
    }

    public boolean isEditingMessageMedia() {
        ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
        return (chatAttachAlert == null || chatAttachAlert.getEditingMessageObject() == null) ? false : true;
    }

    public boolean isSecretChat() {
        return this.currentEncryptedChat != null;
    }

    public boolean canScheduleMessage() {
        BlurredFrameLayout blurredFrameLayout;
        return this.currentEncryptedChat == null && ((blurredFrameLayout = this.bottomOverlayChat) == null || blurredFrameLayout.getVisibility() != 0) && !isThreadChat();
    }

    public boolean isInScheduleMode() {
        return this.chatMode == 1;
    }

    public int getChatMode() {
        return this.chatMode;
    }

    public MessageObject getThreadMessage() {
        return this.threadMessageObject;
    }

    public MessageObject getReplyMessage() {
        return this.replyingMessageObject;
    }

    public int getThreadId() {
        return this.threadMessageId;
    }

    public long getInlineReturn() {
        return this.inlineReturn;
    }

    public TLRPC$User getCurrentUser() {
        return this.currentUser;
    }

    public TLRPC$Chat getCurrentChat() {
        return this.currentChat;
    }

    public TLRPC$EncryptedChat getCurrentEncryptedChat() {
        return this.currentEncryptedChat;
    }

    public TLRPC$ChatFull getCurrentChatInfo() {
        return this.chatInfo;
    }

    public ChatObject.Call getGroupCall() {
        ChatObject.Call call;
        if (this.chatMode != 0 || (call = this.groupCall) == null || !(call.call instanceof TLRPC$TL_groupCall)) {
            return null;
        }
        return call;
    }

    public TLRPC$UserFull getCurrentUserInfo() {
        return this.userInfo;
    }

    public void sendAudio(ArrayList<MessageObject> arrayList, CharSequence charSequence, boolean z, int i) {
        String str = null;
        fillEditingMediaWithCaption(charSequence, null);
        AccountInstance accountInstance = getAccountInstance();
        if (charSequence != null) {
            str = charSequence.toString();
        }
        SendMessagesHelper.prepareSendingAudioDocuments(accountInstance, arrayList, str, this.dialog_id, this.replyingMessageObject, getThreadMessage(), this.editingMessageObject, z, i);
        afterMessageSend();
    }

    public void sendContact(TLRPC$User tLRPC$User, boolean z, int i) {
        getSendMessagesHelper().sendMessage(tLRPC$User, this.dialog_id, this.replyingMessageObject, getThreadMessage(), (TLRPC$ReplyMarkup) null, (HashMap<String, String>) null, z, i);
        afterMessageSend();
    }

    public void sendPoll(TLRPC$TL_messageMediaPoll tLRPC$TL_messageMediaPoll, HashMap<String, String> hashMap, boolean z, int i) {
        getSendMessagesHelper().sendMessage(tLRPC$TL_messageMediaPoll, this.dialog_id, this.replyingMessageObject, getThreadMessage(), (TLRPC$ReplyMarkup) null, hashMap, z, i);
        afterMessageSend();
    }

    public void sendMedia(MediaController.PhotoEntry photoEntry, VideoEditedInfo videoEditedInfo, boolean z, int i, boolean z2) {
        if (photoEntry != null) {
            fillEditingMediaWithCaption(photoEntry.caption, photoEntry.entities);
            if (photoEntry.isVideo) {
                if (videoEditedInfo != null) {
                    SendMessagesHelper.prepareSendingVideo(getAccountInstance(), photoEntry.path, videoEditedInfo, this.dialog_id, this.replyingMessageObject, getThreadMessage(), photoEntry.caption, photoEntry.entities, photoEntry.ttl, this.editingMessageObject, z, i, z2);
                } else {
                    SendMessagesHelper.prepareSendingVideo(getAccountInstance(), photoEntry.path, null, this.dialog_id, this.replyingMessageObject, getThreadMessage(), photoEntry.caption, photoEntry.entities, photoEntry.ttl, this.editingMessageObject, z, i, z2);
                }
            } else if (photoEntry.imagePath != null) {
                SendMessagesHelper.prepareSendingPhoto(getAccountInstance(), photoEntry.imagePath, photoEntry.thumbPath, null, this.dialog_id, this.replyingMessageObject, getThreadMessage(), photoEntry.caption, photoEntry.entities, photoEntry.stickers, null, photoEntry.ttl, this.editingMessageObject, videoEditedInfo, z, i, z2);
            } else if (photoEntry.path != null) {
                SendMessagesHelper.prepareSendingPhoto(getAccountInstance(), photoEntry.path, photoEntry.thumbPath, null, this.dialog_id, this.replyingMessageObject, getThreadMessage(), photoEntry.caption, photoEntry.entities, photoEntry.stickers, null, photoEntry.ttl, this.editingMessageObject, videoEditedInfo, z, i, z2);
            }
            afterMessageSend();
        }
    }

    public void showOpenGameAlert(TLRPC$TL_game tLRPC$TL_game, MessageObject messageObject, String str, boolean z, long j) {
        String str2;
        TLRPC$User user = getMessagesController().getUser(Long.valueOf(j));
        String str3 = "";
        if (z) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
            builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
            if (user != null) {
                str3 = ContactsController.formatName(user.first_name, user.last_name);
            }
            builder.setMessage(LocaleController.formatString("BotPermissionGameAlert", R.string.BotPermissionGameAlert, str3));
            builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), new DialogInterface.OnClickListener(tLRPC$TL_game, messageObject, str, j) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda40
                public final /* synthetic */ TLRPC$TL_game f$1;
                public final /* synthetic */ MessageObject f$2;
                public final /* synthetic */ String f$3;
                public final /* synthetic */ long f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ChatActivity.this.lambda$showOpenGameAlert$219(this.f$1, this.f$2, this.f$3, this.f$4, dialogInterface, i);
                }
            });
            builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
            showDialog(builder.create());
        } else if (Build.VERSION.SDK_INT < 21 || AndroidUtilities.isTablet() || !WebviewActivity.supportWebview()) {
            Activity parentActivity = getParentActivity();
            String str4 = tLRPC$TL_game.short_name;
            if (!(user == null || (str2 = user.username) == null)) {
                str3 = str2;
            }
            WebviewActivity.openGameInBrowser(str, messageObject, parentActivity, str4, str3);
        } else {
            ArrayList<BaseFragment> arrayList = this.parentLayout.fragmentsStack;
            if (arrayList.get(arrayList.size() - 1) == this) {
                if (user != null && !TextUtils.isEmpty(user.username)) {
                    str3 = user.username;
                }
                presentFragment(new WebviewActivity(str, str3, tLRPC$TL_game.title, tLRPC$TL_game.short_name, messageObject));
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showOpenGameAlert$219(TLRPC$TL_game tLRPC$TL_game, MessageObject messageObject, String str, long j, DialogInterface dialogInterface, int i) {
        showOpenGameAlert(tLRPC$TL_game, messageObject, str, false, j);
        SharedPreferences.Editor edit = MessagesController.getNotificationsSettings(this.currentAccount).edit();
        edit.putBoolean("askgame_" + j, false).commit();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0058, code lost:
        if (r12 != r11.get(r11.size() - 1).id) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processLoadedDiscussionMessage(boolean r20, org.telegram.tgnet.TLRPC$TL_messages_discussionMessage r21, boolean r22, org.telegram.tgnet.TLRPC$messages_Messages r23, int r24, org.telegram.messenger.MessageObject r25, org.telegram.tgnet.TLRPC$TL_messages_getDiscussionMessage r26, org.telegram.tgnet.TLRPC$Chat r27, int r28, org.telegram.messenger.MessageObject r29) {
        /*
        // Method dump skipped, instructions count: 414
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.processLoadedDiscussionMessage(boolean, org.telegram.tgnet.TLRPC$TL_messages_discussionMessage, boolean, org.telegram.tgnet.TLRPC$messages_Messages, int, org.telegram.messenger.MessageObject, org.telegram.tgnet.TLRPC$TL_messages_getDiscussionMessage, org.telegram.tgnet.TLRPC$Chat, int, org.telegram.messenger.MessageObject):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processLoadedDiscussionMessage$221(boolean[] zArr, int i, ChatActivity chatActivity) {
        if (!zArr[0] && i == this.commentLoadingMessageId && this.isFullyVisible && !isFinishing()) {
            zArr[0] = true;
            AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda134
                @Override // java.lang.Runnable
                public final void run() {
                    ChatActivity.this.lambda$processLoadedDiscussionMessage$220();
                }
            }, 200);
            presentFragment(chatActivity);
            if (isKeyboardVisible() && !chatActivity.hideKeyboardOnShow()) {
                chatActivity.chatActivityEnterView.getEditField().requestFocus();
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processLoadedDiscussionMessage$220() {
        this.commentLoadingMessageId = 0;
        this.chatListView.invalidateViews();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$processLoadedDiscussionMessage$222(TLRPC$messages_Messages tLRPC$messages_Messages, long j, int i, int i2, int i3, int i4, ArrayList arrayList) {
        getMessagesController().processLoadedMessages(tLRPC$messages_Messages, tLRPC$messages_Messages.messages.size(), j, 0, 30, i > 0 ? i : i2, 0, false, i3, i4, 0, 0, 0, i > 0 ? 3 : 2, true, 0, ((MessageObject) arrayList.get(arrayList.size() - 1)).getId(), 1, false, 0, true);
    }

    /* access modifiers changed from: private */
    public void openDiscussionMessageChat(long j, MessageObject messageObject, int i, long j2, int i2, int i3, MessageObject messageObject2) {
        int i4 = i;
        TLRPC$Chat chat = getMessagesController().getChat(Long.valueOf(j));
        TLRPC$TL_messages_getDiscussionMessage tLRPC$TL_messages_getDiscussionMessage = new TLRPC$TL_messages_getDiscussionMessage();
        tLRPC$TL_messages_getDiscussionMessage.peer = MessagesController.getInputPeer(chat);
        tLRPC$TL_messages_getDiscussionMessage.msg_id = i4;
        if (BuildVars.LOGS_ENABLED) {
            FileLog.d("getDiscussionMessage chat = " + chat.id + " msg_id = " + i4);
        }
        this.commentLoadingMessageId = 0;
        this.savedDiscussionMessage = null;
        this.savedNoDiscussion = false;
        this.savedNoHistory = false;
        this.savedHistory = null;
        RecyclerListView recyclerListView = this.chatListView;
        if (recyclerListView != null) {
            recyclerListView.invalidateViews();
        }
        if (this.commentMessagesRequestId != -1) {
            getConnectionsManager().cancelRequest(this.commentMessagesRequestId, false);
        }
        if (this.commentRequestId != -1) {
            getConnectionsManager().cancelRequest(this.commentRequestId, false);
        }
        if (messageObject2 != null) {
            i4 = messageObject2.getId();
        }
        this.commentLoadingMessageId = i4;
        RecyclerListView recyclerListView2 = this.chatListView;
        if (recyclerListView2 != null) {
            recyclerListView2.invalidateViews();
        }
        int i5 = this.commentLoadingGuid + 1;
        this.commentLoadingGuid = i5;
        this.commentRequestId = getConnectionsManager().sendRequest(tLRPC$TL_messages_getDiscussionMessage, new RequestDelegate(i5, i2, j2, i3, messageObject2, tLRPC$TL_messages_getDiscussionMessage, chat, messageObject) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda221
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ int f$4;
            public final /* synthetic */ MessageObject f$5;
            public final /* synthetic */ TLRPC$TL_messages_getDiscussionMessage f$6;
            public final /* synthetic */ TLRPC$Chat f$7;
            public final /* synthetic */ MessageObject f$8;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
                this.f$5 = r7;
                this.f$6 = r8;
                this.f$7 = r9;
                this.f$8 = r10;
            }

            @Override // org.telegram.tgnet.RequestDelegate
            public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                ChatActivity.this.lambda$openDiscussionMessageChat$228(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, tLObject, tLRPC$TL_error);
            }
        });
        getConnectionsManager().bindRequestToGuid(this.commentRequestId, this.classGuid);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openDiscussionMessageChat$228(int i, int i2, long j, int i3, MessageObject messageObject, TLRPC$TL_messages_getDiscussionMessage tLRPC$TL_messages_getDiscussionMessage, TLRPC$Chat tLRPC$Chat, MessageObject messageObject2, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(new Runnable(i, i2, j, tLObject, i3, messageObject, tLRPC$TL_messages_getDiscussionMessage, tLRPC$Chat, messageObject2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda164
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ TLObject f$4;
            public final /* synthetic */ int f$5;
            public final /* synthetic */ MessageObject f$6;
            public final /* synthetic */ TLRPC$TL_messages_getDiscussionMessage f$7;
            public final /* synthetic */ TLRPC$Chat f$8;
            public final /* synthetic */ MessageObject f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
                this.f$5 = r7;
                this.f$6 = r8;
                this.f$7 = r9;
                this.f$8 = r10;
                this.f$9 = r11;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$openDiscussionMessageChat$226(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9);
            }
        }) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda171
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$openDiscussionMessageChat$227(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openDiscussionMessageChat$226(int i, int i2, long j, TLObject tLObject, int i3, MessageObject messageObject, TLRPC$TL_messages_getDiscussionMessage tLRPC$TL_messages_getDiscussionMessage, TLRPC$Chat tLRPC$Chat, MessageObject messageObject2) {
        if (i == this.commentLoadingGuid) {
            this.commentRequestId = -1;
            if (tLObject instanceof TLRPC$TL_messages_discussionMessage) {
                this.savedDiscussionMessage = (TLRPC$TL_messages_discussionMessage) tLObject;
                getMessagesController().putUsers(this.savedDiscussionMessage.users, false);
                getMessagesController().putChats(this.savedDiscussionMessage.chats, false);
            } else {
                this.savedNoDiscussion = true;
            }
            ArrayList arrayList = new ArrayList();
            TLRPC$TL_messages_discussionMessage tLRPC$TL_messages_discussionMessage = this.savedDiscussionMessage;
            if (!(tLRPC$TL_messages_discussionMessage == null || tLRPC$TL_messages_discussionMessage.messages == null)) {
                for (int i4 = 0; i4 < this.savedDiscussionMessage.messages.size(); i4++) {
                    TLRPC$Message tLRPC$Message = this.savedDiscussionMessage.messages.get(i4);
                    if (!(tLRPC$Message instanceof TLRPC$TL_messageEmpty)) {
                        arrayList.add(tLRPC$Message);
                    }
                }
            }
            if (arrayList.size() > 0) {
                TLRPC$Message tLRPC$Message2 = (TLRPC$Message) arrayList.get(0);
                TLRPC$TL_messages_getReplies tLRPC$TL_messages_getReplies = new TLRPC$TL_messages_getReplies();
                tLRPC$TL_messages_getReplies.peer = getMessagesController().getInputPeer(tLRPC$Message2.peer_id);
                tLRPC$TL_messages_getReplies.msg_id = tLRPC$Message2.id;
                tLRPC$TL_messages_getReplies.offset_date = 0;
                tLRPC$TL_messages_getReplies.limit = 30;
                if (i3 > 0) {
                    tLRPC$TL_messages_getReplies.offset_id = i3;
                    tLRPC$TL_messages_getReplies.add_offset = (-30) / 2;
                } else {
                    tLRPC$TL_messages_getReplies.offset_id = i2 == 0 ? 1 : i2;
                    tLRPC$TL_messages_getReplies.add_offset = (-30) + 10;
                }
                int i5 = 1 + this.commentMessagesLoadingGuid;
                this.commentMessagesLoadingGuid = i5;
                this.commentMessagesRequestId = getConnectionsManager().sendRequest(tLRPC$TL_messages_getReplies, new RequestDelegate(i5, i2, messageObject, tLRPC$TL_messages_getDiscussionMessage, tLRPC$Chat, i3, messageObject2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda222
                    public final /* synthetic */ int f$1;
                    public final /* synthetic */ int f$2;
                    public final /* synthetic */ MessageObject f$3;
                    public final /* synthetic */ TLRPC$TL_messages_getDiscussionMessage f$4;
                    public final /* synthetic */ TLRPC$Chat f$5;
                    public final /* synthetic */ int f$6;
                    public final /* synthetic */ MessageObject f$7;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                        this.f$5 = r6;
                        this.f$6 = r7;
                        this.f$7 = r8;
                    }

                    @Override // org.telegram.tgnet.RequestDelegate
                    public final void run(TLObject tLObject2, TLRPC$TL_error tLRPC$TL_error) {
                        ChatActivity.this.lambda$openDiscussionMessageChat$225(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, tLObject2, tLRPC$TL_error);
                    }
                });
                return;
            }
            this.savedNoHistory = true;
            processLoadedDiscussionMessage(this.savedNoDiscussion, this.savedDiscussionMessage, true, this.savedHistory, i2, messageObject, tLRPC$TL_messages_getDiscussionMessage, tLRPC$Chat, i3, messageObject2);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openDiscussionMessageChat$224(int i, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error, int i2, MessageObject messageObject, TLRPC$TL_messages_getDiscussionMessage tLRPC$TL_messages_getDiscussionMessage, TLRPC$Chat tLRPC$Chat, int i3, MessageObject messageObject2) {
        lambda$openDiscussionMessageChat$227(new Runnable(i, tLObject, tLRPC$TL_error, i2, messageObject, tLRPC$TL_messages_getDiscussionMessage, tLRPC$Chat, i3, messageObject2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda168
            public final /* synthetic */ int f$1;
            public final /* synthetic */ TLObject f$2;
            public final /* synthetic */ TLRPC$TL_error f$3;
            public final /* synthetic */ int f$4;
            public final /* synthetic */ MessageObject f$5;
            public final /* synthetic */ TLRPC$TL_messages_getDiscussionMessage f$6;
            public final /* synthetic */ TLRPC$Chat f$7;
            public final /* synthetic */ int f$8;
            public final /* synthetic */ MessageObject f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
                this.f$9 = r10;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$openDiscussionMessageChat$223(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openDiscussionMessageChat$225(int i, int i2, MessageObject messageObject, TLRPC$TL_messages_getDiscussionMessage tLRPC$TL_messages_getDiscussionMessage, TLRPC$Chat tLRPC$Chat, int i3, MessageObject messageObject2, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(i, tLObject, tLRPC$TL_error, i2, messageObject, tLRPC$TL_messages_getDiscussionMessage, tLRPC$Chat, i3, messageObject2) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda167
            public final /* synthetic */ int f$1;
            public final /* synthetic */ TLObject f$2;
            public final /* synthetic */ TLRPC$TL_error f$3;
            public final /* synthetic */ int f$4;
            public final /* synthetic */ MessageObject f$5;
            public final /* synthetic */ TLRPC$TL_messages_getDiscussionMessage f$6;
            public final /* synthetic */ TLRPC$Chat f$7;
            public final /* synthetic */ int f$8;
            public final /* synthetic */ MessageObject f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
                this.f$9 = r10;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$openDiscussionMessageChat$224(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$openDiscussionMessageChat$223(int i, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error, int i2, MessageObject messageObject, TLRPC$TL_messages_getDiscussionMessage tLRPC$TL_messages_getDiscussionMessage, TLRPC$Chat tLRPC$Chat, int i3, MessageObject messageObject2) {
        if (i == this.commentMessagesLoadingGuid) {
            this.commentMessagesRequestId = -1;
            if (tLObject != null) {
                this.savedHistory = (TLRPC$messages_Messages) tLObject;
            } else if ("CHANNEL_PRIVATE".equals(tLRPC$TL_error.text)) {
                if (getParentActivity() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
                    builder.setTitle(LocaleController.getString("AppName", R.string.AppName));
                    builder.setMessage(LocaleController.getString("JoinByPeekChannelText", R.string.JoinByPeekChannelText));
                    builder.setPositiveButton(LocaleController.getString("OK", R.string.OK), null);
                    showDialog(builder.create());
                }
                this.commentLoadingMessageId = 0;
                this.chatListView.invalidateViews();
                return;
            } else {
                this.savedNoHistory = true;
            }
            processLoadedDiscussionMessage(this.savedNoDiscussion, this.savedDiscussionMessage, this.savedNoHistory, this.savedHistory, i2, messageObject, tLRPC$TL_messages_getDiscussionMessage, tLRPC$Chat, i3, messageObject2);
        }
    }

    /* access modifiers changed from: private */
    public void openOriginalReplyChat(MessageObject messageObject) {
        if (UserObject.isUserSelf(this.currentUser)) {
            TLRPC$MessageFwdHeader tLRPC$MessageFwdHeader = messageObject.messageOwner.fwd_from;
            if (tLRPC$MessageFwdHeader.saved_from_peer.user_id == this.currentUser.id) {
                scrollToMessageId(tLRPC$MessageFwdHeader.saved_from_msg_id, messageObject.getId(), true, 0, true, 0);
                return;
            }
        }
        Bundle bundle = new Bundle();
        TLRPC$Peer tLRPC$Peer = messageObject.messageOwner.fwd_from.saved_from_peer;
        long j = tLRPC$Peer.channel_id;
        if (j != 0) {
            bundle.putLong("chat_id", j);
        } else {
            long j2 = tLRPC$Peer.chat_id;
            if (j2 != 0) {
                bundle.putLong("chat_id", j2);
            } else {
                long j3 = tLRPC$Peer.user_id;
                if (j3 != 0) {
                    bundle.putLong("user_id", j3);
                }
            }
        }
        bundle.putInt("message_id", messageObject.messageOwner.fwd_from.saved_from_msg_id);
        if (getMessagesController().checkCanOpenChat(bundle, this)) {
            presentFragment(new ChatActivity(bundle));
        }
    }

    public void showRequestUrlAlert(TLRPC$TL_urlAuthResultRequest tLRPC$TL_urlAuthResultRequest, TLRPC$TL_messages_requestUrlAuth tLRPC$TL_messages_requestUrlAuth, String str, boolean z) {
        if (getParentActivity() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity(), this.themeDelegate);
            builder.setTitle(LocaleController.getString("OpenUrlTitle", R.string.OpenUrlTitle));
            String string = LocaleController.getString("OpenUrlAlert2", R.string.OpenUrlAlert2);
            int indexOf = string.indexOf("%");
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(String.format(string, str));
            if (indexOf >= 0) {
                spannableStringBuilder.setSpan(new URLSpan(str), indexOf, str.length() + indexOf, 33);
            }
            builder.setMessage(spannableStringBuilder);
            builder.setMessageTextViewClickable(false);
            builder.setNegativeButton(LocaleController.getString("Cancel", R.string.Cancel), null);
            int i = 2;
            CheckBoxCell[] checkBoxCellArr = new CheckBoxCell[2];
            LinearLayout linearLayout = new LinearLayout(getParentActivity());
            linearLayout.setOrientation(1);
            TLRPC$User currentUser = getUserConfig().getCurrentUser();
            int i2 = 0;
            while (true) {
                if (i2 < (tLRPC$TL_urlAuthResultRequest.request_write_access ? 2 : 1)) {
                    checkBoxCellArr[i2] = new CheckBoxCell(getParentActivity(), 5, this.themeDelegate);
                    checkBoxCellArr[i2].setBackgroundDrawable(Theme.getSelectorDrawable(false));
                    checkBoxCellArr[i2].setMultiline(true);
                    checkBoxCellArr[i2].setTag(Integer.valueOf(i2));
                    if (i2 == 0) {
                        Object[] objArr = new Object[i];
                        objArr[0] = tLRPC$TL_urlAuthResultRequest.domain;
                        objArr[1] = ContactsController.formatName(currentUser.first_name, currentUser.last_name);
                        SpannableStringBuilder replaceTags = AndroidUtilities.replaceTags(LocaleController.formatString("OpenUrlOption1", R.string.OpenUrlOption1, objArr));
                        int indexOf2 = TextUtils.indexOf(replaceTags, tLRPC$TL_urlAuthResultRequest.domain);
                        if (indexOf2 >= 0) {
                            replaceTags.setSpan(new URLSpan(""), indexOf2, tLRPC$TL_urlAuthResultRequest.domain.length() + indexOf2, 33);
                        }
                        checkBoxCellArr[i2].setText(replaceTags, "", true, false);
                    } else {
                        checkBoxCellArr[i2].setText(AndroidUtilities.replaceTags(LocaleController.formatString("OpenUrlOption2", R.string.OpenUrlOption2, UserObject.getFirstName(tLRPC$TL_urlAuthResultRequest.bot))), "", true, false);
                    }
                    checkBoxCellArr[i2].setPadding(LocaleController.isRTL ? AndroidUtilities.dp(16.0f) : AndroidUtilities.dp(8.0f), 0, LocaleController.isRTL ? AndroidUtilities.dp(8.0f) : AndroidUtilities.dp(16.0f), 0);
                    linearLayout.addView(checkBoxCellArr[i2], LayoutHelper.createLinear(-1, -2));
                    checkBoxCellArr[i2].setOnClickListener(new View.OnClickListener(checkBoxCellArr) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda92
                        public final /* synthetic */ CheckBoxCell[] f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            ChatActivity.lambda$showRequestUrlAlert$229(this.f$0, view);
                        }
                    });
                    i2++;
                    i = 2;
                } else {
                    builder.setCustomViewOffset(12);
                    builder.setView(linearLayout);
                    builder.setPositiveButton(LocaleController.getString("Open", R.string.Open), new DialogInterface.OnClickListener(checkBoxCellArr, str, tLRPC$TL_messages_requestUrlAuth, tLRPC$TL_urlAuthResultRequest, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda44
                        public final /* synthetic */ CheckBoxCell[] f$1;
                        public final /* synthetic */ String f$2;
                        public final /* synthetic */ TLRPC$TL_messages_requestUrlAuth f$3;
                        public final /* synthetic */ TLRPC$TL_urlAuthResultRequest f$4;
                        public final /* synthetic */ boolean f$5;

                        {
                            this.f$1 = r2;
                            this.f$2 = r3;
                            this.f$3 = r4;
                            this.f$4 = r5;
                            this.f$5 = r6;
                        }

                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i3) {
                            ChatActivity.this.lambda$showRequestUrlAlert$234(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, dialogInterface, i3);
                        }
                    });
                    showDialog(builder.create());
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$showRequestUrlAlert$229(CheckBoxCell[] checkBoxCellArr, View view) {
        if (view.isEnabled()) {
            Integer num = (Integer) view.getTag();
            checkBoxCellArr[num.intValue()].setChecked(!checkBoxCellArr[num.intValue()].isChecked(), true);
            if (num.intValue() == 0 && checkBoxCellArr[1] != null) {
                if (checkBoxCellArr[num.intValue()].isChecked()) {
                    checkBoxCellArr[1].setEnabled(true);
                    return;
                }
                checkBoxCellArr[1].setChecked(false, true);
                checkBoxCellArr[1].setEnabled(false);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showRequestUrlAlert$234(CheckBoxCell[] checkBoxCellArr, String str, TLRPC$TL_messages_requestUrlAuth tLRPC$TL_messages_requestUrlAuth, TLRPC$TL_urlAuthResultRequest tLRPC$TL_urlAuthResultRequest, boolean z, DialogInterface dialogInterface, int i) {
        if (!checkBoxCellArr[0].isChecked()) {
            Browser.openUrl((Context) getParentActivity(), str, false);
            return;
        }
        AlertDialog[] alertDialogArr = new AlertDialog[1];
        alertDialogArr[0] = new AlertDialog(getParentActivity(), 3, this.themeDelegate);
        TLRPC$TL_messages_acceptUrlAuth tLRPC$TL_messages_acceptUrlAuth = new TLObject() { // from class: org.telegram.tgnet.TLRPC$TL_messages_acceptUrlAuth
            public static int constructor = -1322487515;
            public int button_id;
            public int flags;
            public int msg_id;
            public TLRPC$InputPeer peer;
            public String url;
            public boolean write_allowed;

            @Override // org.telegram.tgnet.TLObject
            public TLObject deserializeResponse(AbstractSerializedData abstractSerializedData, int i2, boolean z2) {
                return TLRPC$UrlAuthResult.TLdeserialize(abstractSerializedData, i2, z2);
            }

            @Override // org.telegram.tgnet.TLObject
            public void serializeToStream(AbstractSerializedData abstractSerializedData) {
                abstractSerializedData.writeInt32(constructor);
                int i2 = this.write_allowed ? this.flags | 1 : this.flags & -2;
                this.flags = i2;
                abstractSerializedData.writeInt32(i2);
                if ((this.flags & 2) != 0) {
                    this.peer.serializeToStream(abstractSerializedData);
                }
                if ((this.flags & 2) != 0) {
                    abstractSerializedData.writeInt32(this.msg_id);
                }
                if ((this.flags & 2) != 0) {
                    abstractSerializedData.writeInt32(this.button_id);
                }
                if ((this.flags & 4) != 0) {
                    abstractSerializedData.writeString(this.url);
                }
            }
        };
        String str2 = tLRPC$TL_messages_requestUrlAuth.url;
        if (str2 != null) {
            tLRPC$TL_messages_acceptUrlAuth.url = str2;
            tLRPC$TL_messages_acceptUrlAuth.flags |= 4;
        } else {
            tLRPC$TL_messages_acceptUrlAuth.button_id = tLRPC$TL_messages_requestUrlAuth.button_id;
            tLRPC$TL_messages_acceptUrlAuth.msg_id = tLRPC$TL_messages_requestUrlAuth.msg_id;
            tLRPC$TL_messages_acceptUrlAuth.peer = tLRPC$TL_messages_requestUrlAuth.peer;
            tLRPC$TL_messages_acceptUrlAuth.flags |= 2;
        }
        if (tLRPC$TL_urlAuthResultRequest.request_write_access) {
            tLRPC$TL_messages_acceptUrlAuth.write_allowed = checkBoxCellArr[1].isChecked();
        }
        try {
            alertDialogArr[0].dismiss();
        } catch (Throwable unused) {
        }
        alertDialogArr[0] = null;
        AndroidUtilities.runOnUIThread(new Runnable(alertDialogArr, getConnectionsManager().sendRequest(tLRPC$TL_messages_acceptUrlAuth, new RequestDelegate(str, tLRPC$TL_messages_requestUrlAuth, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda224
            public final /* synthetic */ String f$1;
            public final /* synthetic */ TLRPC$TL_messages_requestUrlAuth f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.telegram.tgnet.RequestDelegate
            public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
                ChatActivity.this.lambda$showRequestUrlAlert$231(this.f$1, this.f$2, this.f$3, tLObject, tLRPC$TL_error);
            }
        })) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda199
            public final /* synthetic */ AlertDialog[] f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$showRequestUrlAlert$233(this.f$1, this.f$2);
            }
        }, 500);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showRequestUrlAlert$231(String str, TLRPC$TL_messages_requestUrlAuth tLRPC$TL_messages_requestUrlAuth, boolean z, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(tLObject, str, tLRPC$TL_messages_requestUrlAuth, z) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda187
            public final /* synthetic */ TLObject f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ TLRPC$TL_messages_requestUrlAuth f$3;
            public final /* synthetic */ boolean f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$showRequestUrlAlert$230(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showRequestUrlAlert$230(TLObject tLObject, String str, TLRPC$TL_messages_requestUrlAuth tLRPC$TL_messages_requestUrlAuth, boolean z) {
        if (tLObject instanceof TLRPC$TL_urlAuthResultAccepted) {
            Browser.openUrl((Context) getParentActivity(), ((TLRPC$TL_urlAuthResultAccepted) tLObject).url, false);
        } else if (tLObject instanceof TLRPC$TL_urlAuthResultDefault) {
            Browser.openUrl((Context) getParentActivity(), str, false);
        } else {
            String str2 = tLRPC$TL_messages_requestUrlAuth.url;
            if (str2 != null) {
                AlertsCreator.showOpenUrlAlert(this, str2, false, z, this.themeDelegate);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showRequestUrlAlert$233(AlertDialog[] alertDialogArr, int i) {
        if (alertDialogArr[0] != null) {
            alertDialogArr[0].setOnCancelListener(new DialogInterface.OnCancelListener(i) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda22
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    ChatActivity.this.lambda$showRequestUrlAlert$232(this.f$1, dialogInterface);
                }
            });
            showDialog(alertDialogArr[0]);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showRequestUrlAlert$232(int i, DialogInterface dialogInterface) {
        getConnectionsManager().cancelRequest(i, true);
    }

    private void removeMessageObject(MessageObject messageObject) {
        int indexOf = this.messages.indexOf(messageObject);
        if (indexOf != -1) {
            this.messages.remove(indexOf);
            ChatActivityAdapter chatActivityAdapter = this.chatAdapter;
            if (chatActivityAdapter != null) {
                chatActivityAdapter.notifyItemRemoved(chatActivityAdapter.messagesStartRow + indexOf);
            }
        }
    }

    public void openVCard(TLRPC$User tLRPC$User, String str, String str2, String str3) {
        try {
            File sharingDirectory = AndroidUtilities.getSharingDirectory();
            sharingDirectory.mkdirs();
            File file = new File(sharingDirectory, "vcard.vcf");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(str);
            bufferedWriter.close();
            showDialog(new PhonebookShareAlert(this, null, tLRPC$User, null, file, str2, str3, this.themeDelegate));
        } catch (Exception e) {
            FileLog.e(e);
        }
    }

    /* access modifiers changed from: private */
    public void setCellSelectionBackground(MessageObject messageObject, ChatMessageCell chatMessageCell, int i, boolean z) {
        boolean z2;
        MessageObject.GroupedMessages validGroupedMessage = getValidGroupedMessage(messageObject);
        boolean z3 = false;
        if (validGroupedMessage != null) {
            int i2 = 0;
            while (true) {
                if (i2 >= validGroupedMessage.messages.size()) {
                    z2 = false;
                    break;
                } else if (this.selectedMessagesIds[i].indexOfKey(validGroupedMessage.messages.get(i2).getId()) < 0) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z2) {
                validGroupedMessage = null;
            }
        }
        chatMessageCell.setDrawSelectionBackground(validGroupedMessage == null);
        if (validGroupedMessage == null) {
            z3 = true;
        }
        chatMessageCell.setChecked(true, z3, z);
    }

    /* access modifiers changed from: private */
    public void openClickableLink(CharacterStyle characterStyle, String str, boolean z, ChatMessageCell chatMessageCell, MessageObject messageObject) {
        TLRPC$User tLRPC$User;
        String str2;
        TLRPC$Message tLRPC$Message;
        if (z) {
            BottomSheet.Builder builder = new BottomSheet.Builder(getParentActivity(), false, this.themeDelegate);
            int intValue = str.startsWith("video?") ? Utilities.parseInt((CharSequence) str).intValue() : -1;
            if (intValue >= 0) {
                builder.setTitle(AndroidUtilities.formatDuration(intValue, false));
            } else {
                try {
                    str2 = URLDecoder.decode(str.replaceAll("\\+", "%2b"), "UTF-8");
                } catch (Exception unused) {
                    str2 = str;
                }
                builder.setTitle(str2);
                builder.setTitleMultipleLines(true);
            }
            builder.setItems(getMessagesController().isChatNoForwards(this.currentChat) || (messageObject != null && (tLRPC$Message = messageObject.messageOwner) != null && tLRPC$Message.noforwards) ? new CharSequence[]{LocaleController.getString("Open", R.string.Open)} : new CharSequence[]{LocaleController.getString("Open", R.string.Open), LocaleController.getString("Copy", R.string.Copy)}, new DialogInterface.OnClickListener(str, characterStyle, messageObject, chatMessageCell, intValue) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda37
                public final /* synthetic */ String f$1;
                public final /* synthetic */ CharacterStyle f$2;
                public final /* synthetic */ MessageObject f$3;
                public final /* synthetic */ ChatMessageCell f$4;
                public final /* synthetic */ int f$5;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                    this.f$5 = r6;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ChatActivity.this.lambda$openClickableLink$235(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, dialogInterface, i);
                }
            });
            builder.setOnPreDismissListener(new DialogInterface.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda46
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    ChatActivity.lambda$openClickableLink$236(ChatMessageCell.this, dialogInterface);
                }
            });
            showDialog(builder.create());
        } else if (str.startsWith("@")) {
            String lowerCase = str.substring(1).toLowerCase();
            TLRPC$Chat tLRPC$Chat = this.currentChat;
            if ((tLRPC$Chat == null || TextUtils.isEmpty(tLRPC$Chat.username) || !lowerCase.equals(this.currentChat.username.toLowerCase())) && ((tLRPC$User = this.currentUser) == null || TextUtils.isEmpty(tLRPC$User.username) || !lowerCase.equals(this.currentUser.username.toLowerCase()))) {
                getMessagesController().openByUserName(lowerCase, this, 0);
                return;
            }
            Bundle bundle = new Bundle();
            TLRPC$Chat tLRPC$Chat2 = this.currentChat;
            if (tLRPC$Chat2 != null) {
                bundle.putLong("chat_id", tLRPC$Chat2.id);
            } else {
                TLRPC$User tLRPC$User2 = this.currentUser;
                if (tLRPC$User2 != null) {
                    bundle.putLong("user_id", tLRPC$User2.id);
                    if (this.currentEncryptedChat != null) {
                        bundle.putLong("dialog_id", this.dialog_id);
                    }
                }
            }
            ProfileActivity profileActivity = new ProfileActivity(bundle, this.avatarContainer.getSharedMediaPreloader());
            profileActivity.setPlayProfileAnimation(1);
            profileActivity.setChatInfo(this.chatInfo);
            profileActivity.setUserInfo(this.userInfo);
            presentFragment(profileActivity);
        } else if (!str.startsWith("#") && !str.startsWith("$")) {
            processExternalUrl(0, str, false);
        } else if (ChatObject.isChannel(this.currentChat)) {
            int i = this.chatMode;
            if (i == 1 || i == 2) {
                this.chatActivityDelegate.openSearch(str);
                finishFragment();
                return;
            }
            openSearchWithText(str);
        } else {
            DialogsActivity dialogsActivity = new DialogsActivity(null);
            dialogsActivity.setSearchString(str);
            presentFragment(dialogsActivity);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ea A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00eb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void lambda$openClickableLink$235(java.lang.String r7, android.text.style.CharacterStyle r8, org.telegram.messenger.MessageObject r9, org.telegram.ui.Cells.ChatMessageCell r10, int r11, android.content.DialogInterface r12, int r13) {
        /*
        // Method dump skipped, instructions count: 293
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.lambda$openClickableLink$235(java.lang.String, android.text.style.CharacterStyle, org.telegram.messenger.MessageObject, org.telegram.ui.Cells.ChatMessageCell, int, android.content.DialogInterface, int):void");
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$openClickableLink$236(ChatMessageCell chatMessageCell, DialogInterface dialogInterface) {
        if (chatMessageCell != null) {
            chatMessageCell.resetPressedLink(-1);
        }
    }

    /* access modifiers changed from: private */
    public void processExternalUrl(int i, String str, boolean z) {
        boolean z2;
        boolean z3 = false;
        boolean z4 = true;
        try {
            Uri parse = Uri.parse(str);
            String lowerCase = parse.getHost() != null ? parse.getHost().toLowerCase() : "";
            if ((this.currentEncryptedChat == null || getMessagesController().secretWebpagePreview == 1) && getMessagesController().authDomains.contains(lowerCase)) {
                SendMessagesHelper sendMessagesHelper = getSendMessagesHelper();
                if (!(i == 0 || i == 2)) {
                    z2 = false;
                    sendMessagesHelper.requestUrlAuth(str, this, z2);
                    return;
                }
                z2 = true;
                sendMessagesHelper.requestUrlAuth(str, this, z2);
                return;
            }
        } catch (Exception e) {
            FileLog.e(e);
        }
        if (z || AndroidUtilities.shouldShowUrlInAlert(str)) {
            if (i == 0 || i == 2) {
                AlertsCreator.showOpenUrlAlert(this, str, true, true, true, this.themeDelegate);
            } else if (i == 1) {
                AlertsCreator.showOpenUrlAlert(this, str, true, true, false, this.themeDelegate);
            }
        } else if (i == 0) {
            Browser.openUrl(getParentActivity(), str);
        } else if (i == 1) {
            Activity parentActivity = getParentActivity();
            if (this.inlineReturn != 0) {
                z4 = false;
            }
            Browser.openUrl((Context) parentActivity, str, z4, false);
        } else if (i == 2) {
            Activity parentActivity2 = getParentActivity();
            if (this.inlineReturn == 0) {
                z3 = true;
            }
            Browser.openUrl(parentActivity2, str, z3);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v20 */
    /* JADX WARN: Type inference failed for: r2v23 */
    /* JADX WARN: Type inference failed for: r2v24 */
    /* JADX WARN: Type inference failed for: r2v27 */
    /* JADX WARN: Type inference failed for: r2v31 */
    /* JADX WARN: Type inference failed for: r2v32 */
    /* JADX WARN: Type inference failed for: r2v34 */
    /* JADX WARN: Type inference failed for: r2v35 */
    /* JADX WARN: Type inference failed for: r2v36 */
    /* JADX WARN: Type inference failed for: r2v37 */
    /* JADX WARN: Type inference failed for: r2v38 */
    /* JADX WARN: Type inference failed for: r2v39 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Unknown variable types count: 8 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void didPressMessageUrl(android.text.style.CharacterStyle r23, boolean r24, org.telegram.messenger.MessageObject r25, org.telegram.ui.Cells.ChatMessageCell r26) {
        /*
        // Method dump skipped, instructions count: 788
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.didPressMessageUrl(android.text.style.CharacterStyle, boolean, org.telegram.messenger.MessageObject, org.telegram.ui.Cells.ChatMessageCell):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didPressMessageUrl$240(AlertDialog[] alertDialogArr, String str, ChatMessageCell chatMessageCell, TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        AndroidUtilities.runOnUIThread(new Runnable(alertDialogArr, tLObject, str, chatMessageCell) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda202
            public final /* synthetic */ AlertDialog[] f$1;
            public final /* synthetic */ TLObject f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ ChatMessageCell f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatActivity.this.lambda$didPressMessageUrl$239(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didPressMessageUrl$239(AlertDialog[] alertDialogArr, TLObject tLObject, String str, ChatMessageCell chatMessageCell) {
        try {
            alertDialogArr[0].dismiss();
        } catch (Throwable unused) {
        }
        alertDialogArr[0] = null;
        if (tLObject instanceof TLRPC$TL_payments_bankCardData) {
            if (getParentActivity() != null) {
                TLRPC$TL_payments_bankCardData tLRPC$TL_payments_bankCardData = (TLRPC$TL_payments_bankCardData) tLObject;
                BottomSheet.Builder builder = new BottomSheet.Builder(getParentActivity(), false, this.themeDelegate);
                ArrayList arrayList = new ArrayList();
                int size = tLRPC$TL_payments_bankCardData.open_urls.size();
                for (int i = 0; i < size; i++) {
                    arrayList.add(tLRPC$TL_payments_bankCardData.open_urls.get(i).name);
                }
                arrayList.add(LocaleController.getString("CopyCardNumber", R.string.CopyCardNumber));
                builder.setTitle(tLRPC$TL_payments_bankCardData.title);
                builder.setItems((CharSequence[]) arrayList.toArray(new CharSequence[0]), new DialogInterface.OnClickListener(tLRPC$TL_payments_bankCardData, str) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda41
                    public final /* synthetic */ TLRPC$TL_payments_bankCardData f$1;
                    public final /* synthetic */ String f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        ChatActivity.this.lambda$didPressMessageUrl$237(this.f$1, this.f$2, dialogInterface, i2);
                    }
                });
                builder.setOnPreDismissListener(new DialogInterface.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda47
                    @Override // android.content.DialogInterface.OnDismissListener
                    public final void onDismiss(DialogInterface dialogInterface) {
                        ChatActivity.lambda$didPressMessageUrl$238(ChatMessageCell.this, dialogInterface);
                    }
                });
                showDialog(builder.create());
            }
        } else if (chatMessageCell != null) {
            chatMessageCell.resetPressedLink(-1);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didPressMessageUrl$237(TLRPC$TL_payments_bankCardData tLRPC$TL_payments_bankCardData, String str, DialogInterface dialogInterface, int i) {
        if (i < tLRPC$TL_payments_bankCardData.open_urls.size()) {
            Browser.openUrl((Context) getParentActivity(), tLRPC$TL_payments_bankCardData.open_urls.get(i).url, this.inlineReturn == 0, false);
            return;
        }
        AndroidUtilities.addToClipboard(str);
        Toast.makeText(ApplicationLoader.applicationContext, LocaleController.getString("CardNumberCopied", R.string.CardNumberCopied), 0).show();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$didPressMessageUrl$238(ChatMessageCell chatMessageCell, DialogInterface dialogInterface) {
        if (chatMessageCell != null) {
            chatMessageCell.resetPressedLink(-1);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didPressMessageUrl$242(AlertDialog[] alertDialogArr, int i, ChatMessageCell chatMessageCell) {
        if (alertDialogArr[0] != null) {
            alertDialogArr[0].setOnCancelListener(new DialogInterface.OnCancelListener(i, chatMessageCell) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda23
                public final /* synthetic */ int f$1;
                public final /* synthetic */ ChatMessageCell f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    ChatActivity.this.lambda$didPressMessageUrl$241(this.f$1, this.f$2, dialogInterface);
                }
            });
            showDialog(alertDialogArr[0]);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$didPressMessageUrl$241(int i, ChatMessageCell chatMessageCell, DialogInterface dialogInterface) {
        getConnectionsManager().cancelRequest(i, true);
        chatMessageCell.resetPressedLink(-1);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void lambda$didPressMessageUrl$243(java.lang.String r4, android.content.DialogInterface r5, int r6) {
        /*
            r3 = this;
            r5 = 0
            r0 = 1
            if (r6 != 0) goto L_0x0008
            r3.processExternalUrl(r0, r4, r5)
            goto L_0x0049
        L_0x0008:
            if (r6 != r0) goto L_0x0049
            java.lang.String r6 = "mailto:"
            boolean r6 = r4.startsWith(r6)
            if (r6 == 0) goto L_0x0019
            r6 = 7
            java.lang.String r4 = r4.substring(r6)
            r5 = 1
            goto L_0x0027
        L_0x0019:
            java.lang.String r6 = "tel:"
            boolean r6 = r4.startsWith(r6)
            if (r6 == 0) goto L_0x0027
            r6 = 4
            java.lang.String r4 = r4.substring(r6)
            goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            org.telegram.messenger.AndroidUtilities.addToClipboard(r4)
            r4 = 0
            r1 = 0
            if (r5 == 0) goto L_0x0038
            org.telegram.ui.Components.UndoView r5 = r3.undoView
            r6 = 80
            r5.showWithAction(r1, r6, r4)
            goto L_0x0049
        L_0x0038:
            if (r0 == 0) goto L_0x0042
            org.telegram.ui.Components.UndoView r5 = r3.undoView
            r6 = 60
            r5.showWithAction(r1, r6, r4)
            goto L_0x0049
        L_0x0042:
            org.telegram.ui.Components.UndoView r5 = r3.undoView
            r6 = 59
            r5.showWithAction(r1, r6, r4)
        L_0x0049:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.lambda$didPressMessageUrl$243(java.lang.String, android.content.DialogInterface, int):void");
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$didPressMessageUrl$244(ChatMessageCell chatMessageCell, DialogInterface dialogInterface) {
        if (chatMessageCell != null) {
            chatMessageCell.resetPressedLink(-1);
        }
    }

    void openPhotoViewerForMessage(ChatMessageCell chatMessageCell, MessageObject messageObject) {
        ChatMessageCell chatMessageCell2;
        AnimatedFileDrawable animation;
        Bitmap animatedBitmap;
        if (chatMessageCell == null) {
            int childCount = this.chatListView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = this.chatListView.getChildAt(i);
                if (childAt instanceof ChatMessageCell) {
                    chatMessageCell2 = (ChatMessageCell) childAt;
                    if (chatMessageCell2.getMessageObject().equals(messageObject)) {
                        break;
                    }
                }
            }
        }
        chatMessageCell2 = chatMessageCell;
        if (messageObject.isVideo()) {
            sendSecretMessageRead(messageObject, true);
        }
        PhotoViewer.getInstance().setParentActivity(getParentActivity(), this.themeDelegate);
        MessageObject playingMessageObject = MediaController.getInstance().getPlayingMessageObject();
        if (!(chatMessageCell2 == null || playingMessageObject == null || !playingMessageObject.isVideo())) {
            getFileLoader().setLoadingVideoForPlayer(playingMessageObject.getDocument(), false);
            if (!(!playingMessageObject.equals(messageObject) || (animation = chatMessageCell2.getPhotoImage().getAnimation()) == null || this.videoTextureView == null || this.videoPlayerContainer.getTag() == null || (animatedBitmap = animation.getAnimatedBitmap()) == null)) {
                try {
                    Bitmap bitmap = this.videoTextureView.getBitmap(animatedBitmap.getWidth(), animatedBitmap.getHeight());
                    new Canvas(animatedBitmap).drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
                    bitmap.recycle();
                } catch (Throwable th) {
                    FileLog.e(th);
                }
            }
            MediaController.getInstance().cleanupPlayer(true, true, false, playingMessageObject.equals(messageObject));
        }
        if (this.chatMode != 1 || (!messageObject.isVideo() && messageObject.type != 1)) {
            PhotoViewer instance = PhotoViewer.getInstance();
            int i2 = messageObject.type;
            long j = 0;
            long j2 = i2 != 0 ? this.dialog_id : 0;
            if (i2 != 0) {
                j = this.mergeDialogId;
            }
            instance.openPhoto(messageObject, this, j2, j, this.photoViewerProvider);
        } else {
            PhotoViewer.getInstance().setParentChatActivity(this);
            ArrayList<MessageObject> arrayList = new ArrayList<>();
            int size = this.messages.size();
            for (int i3 = 0; i3 < size; i3++) {
                MessageObject messageObject2 = this.messages.get(i3);
                if (messageObject2.isVideo() || messageObject2.type == 1) {
                    arrayList.add(0, messageObject2);
                }
            }
            PhotoViewer.getInstance().openPhoto(arrayList, arrayList.indexOf(messageObject), this.dialog_id, 0, this.photoViewerProvider);
        }
        hideHints(false);
        MediaController.getInstance().resetGoingToShowMessageObject();
    }

    /* access modifiers changed from: private */
    public void updateMessageListAccessibilityVisibility() {
        ActionBarPopupWindow actionBarPopupWindow;
        if (this.currentEncryptedChat == null && Build.VERSION.SDK_INT >= 19) {
            RecyclerListView recyclerListView = this.chatListView;
            MentionsContainerView mentionsContainerView = this.mentionContainer;
            recyclerListView.setImportantForAccessibility(((mentionsContainerView == null || !mentionsContainerView.isOpen()) && ((actionBarPopupWindow = this.scrimPopupWindow) == null || !actionBarPopupWindow.isShowing())) ? 0 : 4);
        }
    }

    /* access modifiers changed from: private */
    public void markSponsoredAsRead(MessageObject messageObject) {
        if (messageObject.isSponsored() && !messageObject.viewsReloaded) {
            messageObject.viewsReloaded = true;
            TLRPC$TL_channels_viewSponsoredMessage tLRPC$TL_channels_viewSponsoredMessage = new TLObject() { // from class: org.telegram.tgnet.TLRPC$TL_channels_viewSponsoredMessage
                public static int constructor = -1095836780;
                public TLRPC$InputChannel channel;
                public byte[] random_id;

                @Override // org.telegram.tgnet.TLObject
                public TLObject deserializeResponse(AbstractSerializedData abstractSerializedData, int i, boolean z) {
                    return TLRPC$Bool.TLdeserialize(abstractSerializedData, i, z);
                }

                @Override // org.telegram.tgnet.TLObject
                public void serializeToStream(AbstractSerializedData abstractSerializedData) {
                    abstractSerializedData.writeInt32(constructor);
                    this.channel.serializeToStream(abstractSerializedData);
                    abstractSerializedData.writeByteArray(this.random_id);
                }
            };
            tLRPC$TL_channels_viewSponsoredMessage.channel = MessagesController.getInputChannel(this.currentChat);
            tLRPC$TL_channels_viewSponsoredMessage.random_id = messageObject.sponsoredId;
            getConnectionsManager().sendRequest(tLRPC$TL_channels_viewSponsoredMessage, ChatActivity$$ExternalSyntheticLambda232.INSTANCE);
            getMessagesController().markSponsoredAsRead(this.dialog_id, messageObject);
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public boolean canBeginSlide() {
        return this.swipeBackEnabled && this.chatActivityEnterView.swipeToBackEnabled() && this.pullingDownOffset == 0.0f;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public boolean isSwipeBackEnabled(MotionEvent motionEvent) {
        return this.swipeBackEnabled;
    }

    /*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
        java.lang.UnsupportedOperationException
        	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
        	at java.util.AbstractList.equals(AbstractList.java:519)
        	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
        	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
        	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
        	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
        */
    /* loaded from: classes3.dex */
    public class ChatActivityAdapter extends RecyclerAnimationScrollHelper.AnimatableAdapter {
        private int botInfoEmptyRow = -5;
        private int botInfoRow = -5;
        public ArrayList<MessageObject> frozenMessages = new ArrayList<>();
        private boolean isBot;
        public boolean isFrozen;
        private int loadingDownRow = -5;
        private int loadingUpRow = -5;
        private Context mContext;
        private int messagesEndRow;
        private int messagesStartRow;
        private int rowCount;

        @Override // org.telegram.ui.Components.RecyclerListView.SelectionAdapter
        public boolean isEnabled(RecyclerView.ViewHolder viewHolder) {
            return false;
        }

        public ChatActivityAdapter(Context context) {
            this.mContext = context;
            TLRPC$User tLRPC$User = ChatActivity.this.currentUser;
            this.isBot = tLRPC$User != null && tLRPC$User.bot;
            setHasStableIds(true);
        }

        public void updateRowsSafe() {
            int i = this.rowCount;
            int i2 = this.botInfoRow;
            int i3 = this.loadingUpRow;
            int i4 = this.loadingDownRow;
            int i5 = this.messagesStartRow;
            int i6 = this.messagesEndRow;
            updateRowsInternal();
            if (i != this.rowCount || i2 != this.botInfoRow || i3 != this.loadingUpRow || i4 != this.loadingDownRow || i5 != this.messagesStartRow || i6 != this.messagesEndRow) {
                notifyDataSetChanged(false);
            }
        }

        /* access modifiers changed from: private */
        public void updateRowsInternal() {
            TLRPC$User tLRPC$User;
            TLRPC$User tLRPC$User2;
            this.rowCount = 0;
            ArrayList<MessageObject> arrayList = this.isFrozen ? this.frozenMessages : ChatActivity.this.messages;
            if (!arrayList.isEmpty()) {
                if ((!ChatActivity.this.forwardEndReached[0] || (ChatActivity.this.mergeDialogId != 0 && !ChatActivity.this.forwardEndReached[1])) && !ChatActivity.this.hideForwardEndReached) {
                    int i = this.rowCount;
                    this.rowCount = i + 1;
                    this.loadingDownRow = i;
                } else {
                    this.loadingDownRow = -5;
                }
                int i2 = this.rowCount;
                this.messagesStartRow = i2;
                int size = i2 + arrayList.size();
                this.rowCount = size;
                this.messagesEndRow = size;
                if ((UserObject.isReplyUser(ChatActivity.this.currentUser) || ((tLRPC$User2 = ChatActivity.this.currentUser) != null && tLRPC$User2.bot && !MessagesController.isSupportUser(tLRPC$User2) && ChatActivity.this.chatMode == 0)) && ChatActivity.this.endReached[0]) {
                    int i3 = this.rowCount;
                    this.rowCount = i3 + 1;
                    this.botInfoRow = i3;
                } else {
                    this.botInfoRow = -5;
                }
                if (!ChatActivity.this.endReached[0] || (ChatActivity.this.mergeDialogId != 0 && !ChatActivity.this.endReached[1])) {
                    int i4 = this.rowCount;
                    this.rowCount = i4 + 1;
                    this.loadingUpRow = i4;
                    return;
                }
                this.loadingUpRow = -5;
                return;
            }
            this.loadingUpRow = -5;
            this.loadingDownRow = -5;
            this.messagesStartRow = -5;
            this.messagesEndRow = -5;
            if (UserObject.isReplyUser(ChatActivity.this.currentUser) || ((tLRPC$User = ChatActivity.this.currentUser) != null && tLRPC$User.bot && !MessagesController.isSupportUser(tLRPC$User) && ChatActivity.this.chatMode == 0)) {
                int i5 = this.rowCount;
                this.rowCount = i5 + 1;
                this.botInfoRow = i5;
                return;
            }
            this.botInfoRow = -5;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            this.botInfoEmptyRow = -5;
            if (ChatActivity.this.clearingHistory) {
                ChatActivity chatActivity = ChatActivity.this;
                TLRPC$User tLRPC$User = chatActivity.currentUser;
                if (tLRPC$User == null || !tLRPC$User.bot || chatActivity.chatMode != 0 || ((ChatActivity.this.botInfo.size() <= 0 || (((TLRPC$BotInfo) ChatActivity.this.botInfo.get(ChatActivity.this.currentUser.id)).description == null && ((TLRPC$BotInfo) ChatActivity.this.botInfo.get(ChatActivity.this.currentUser.id)).description_photo == null && ((TLRPC$BotInfo) ChatActivity.this.botInfo.get(ChatActivity.this.currentUser.id)).description_document == null)) && !UserObject.isReplyUser(ChatActivity.this.currentUser))) {
                    return 0;
                }
                this.botInfoEmptyRow = 0;
                return 1;
            } else if (ChatActivity.this.clearingHistory) {
                return 0;
            } else {
                return this.rowCount;
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public long getItemId(int i) {
            if (ChatActivity.this.clearingHistory && i == this.botInfoEmptyRow) {
                return 1;
            }
            ArrayList<MessageObject> arrayList = this.isFrozen ? this.frozenMessages : ChatActivity.this.messages;
            int i2 = this.messagesStartRow;
            if (i >= i2 && i < this.messagesEndRow) {
                return (long) arrayList.get(i - i2).stableId;
            }
            if (i == this.botInfoRow || i == this.botInfoEmptyRow) {
                return 1;
            }
            if (i == this.loadingUpRow) {
                return 2;
            }
            return i == this.loadingDownRow ? 3 : 4;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v4, resolved type: org.telegram.ui.Cells.BotHelpCell */
        /* JADX WARN: Multi-variable type inference failed */
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            ChatMessageCell chatMessageCell;
            ChatMessageCell chatMessageCell2;
            if (i == 0) {
                if (!ChatActivity.this.chatMessageCellsCache.isEmpty()) {
                    ChatActivity.this.chatMessageCellsCache.remove(0);
                    chatMessageCell2 = (View) ChatActivity.this.chatMessageCellsCache.get(0);
                } else {
                    chatMessageCell2 = new ChatMessageCell(this.mContext, true, ChatActivity.this.themeDelegate);
                }
                ChatMessageCell chatMessageCell3 = chatMessageCell2;
                chatMessageCell3.shouldCheckVisibleOnScreen = true;
                chatMessageCell3.setDelegate(new ChatMessageCell.ChatMessageCellDelegate() { // from class: org.telegram.ui.ChatActivity.ChatActivityAdapter.1
                    public boolean canDrawOutboundsContent() {
                        return false;
                    }

                    public void didPressHint(ChatMessageCell chatMessageCell4, int i2) {
                        if (i2 == 0) {
                            ChatActivity.this.showPollSolution(chatMessageCell4.getMessageObject(), ((TLRPC$TL_messageMediaPoll) chatMessageCell4.getMessageObject().messageOwner.media).results);
                        } else if (i2 == 1) {
                            MessageObject messageObject = chatMessageCell4.getMessageObject();
                            TLRPC$MessageFwdHeader tLRPC$MessageFwdHeader = messageObject.messageOwner.fwd_from;
                            if (tLRPC$MessageFwdHeader != null && !TextUtils.isEmpty(tLRPC$MessageFwdHeader.psa_type)) {
                                String string = LocaleController.getString("PsaMessageInfo_" + messageObject.messageOwner.fwd_from.psa_type);
                                if (TextUtils.isEmpty(string)) {
                                    string = LocaleController.getString("PsaMessageInfoDefault", R.string.PsaMessageInfoDefault);
                                }
                                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);
                                MessageObject.addLinks(false, spannableStringBuilder);
                                MessageObject.GroupedMessages currentMessagesGroup = chatMessageCell4.getCurrentMessagesGroup();
                                if (currentMessagesGroup != null) {
                                    int size = currentMessagesGroup.posArray.size();
                                    int i3 = 0;
                                    while (true) {
                                        if (i3 >= size) {
                                            break;
                                        } else if ((currentMessagesGroup.posArray.get(i3).flags & 1) != 0) {
                                            MessageObject messageObject2 = currentMessagesGroup.messages.get(i3);
                                            if (messageObject2 != messageObject) {
                                                int childCount = ChatActivity.this.chatListView.getChildCount();
                                                for (int i4 = 0; i4 < childCount; i4++) {
                                                    View childAt = ChatActivity.this.chatListView.getChildAt(i4);
                                                    if (childAt instanceof ChatMessageCell) {
                                                        ChatMessageCell chatMessageCell5 = (ChatMessageCell) childAt;
                                                        if (messageObject2.equals(chatMessageCell5.getMessageObject())) {
                                                            chatMessageCell4 = chatMessageCell5;
                                                        }
                                                    }
                                                }
                                                messageObject = messageObject2;
                                            }
                                        } else {
                                            i3++;
                                        }
                                    }
                                }
                                ChatActivity.this.showInfoHint(messageObject, spannableStringBuilder, 1);
                            } else {
                                return;
                            }
                        }
                        chatMessageCell4.showHintButton(false, true, i2);
                    }

                    public boolean shouldDrawThreadProgress(ChatMessageCell chatMessageCell4) {
                        MessageObject messageObject;
                        MessageObject.GroupedMessages currentMessagesGroup = chatMessageCell4.getCurrentMessagesGroup();
                        if (currentMessagesGroup == null || currentMessagesGroup.messages.isEmpty()) {
                            messageObject = chatMessageCell4.getMessageObject();
                        } else {
                            messageObject = currentMessagesGroup.messages.get(0);
                        }
                        if (messageObject != null && messageObject.getId() == ChatActivity.this.commentLoadingMessageId) {
                            return true;
                        }
                        return false;
                    }

                    public void didPressSideButton(ChatMessageCell chatMessageCell4) {
                        MessageObject.GroupedMessages groupedMessages;
                        TLRPC$Message tLRPC$Message;
                        TLRPC$TL_messageReplyHeader tLRPC$TL_messageReplyHeader;
                        int i2;
                        if (ChatActivity.this.getParentActivity() != null) {
                            ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                            if (chatActivityEnterView != null) {
                                chatActivityEnterView.closeKeyboard();
                            }
                            MessageObject messageObject = chatMessageCell4.getMessageObject();
                            if (ChatActivity.this.chatMode == 2) {
                                ChatActivity.this.chatActivityDelegate.openReplyMessage(messageObject.getId());
                                ChatActivity.this.finishFragment();
                            } else if ((!UserObject.isReplyUser(ChatActivity.this.currentUser) && !UserObject.isUserSelf(ChatActivity.this.currentUser)) || messageObject.messageOwner.fwd_from.saved_from_peer == null) {
                                ArrayList<MessageObject> arrayList = null;
                                if (!(messageObject.getGroupId() == 0 || (groupedMessages = (MessageObject.GroupedMessages) ChatActivity.this.groupedMessagesMap.get(messageObject.getGroupId())) == null)) {
                                    arrayList = groupedMessages.messages;
                                }
                                if (arrayList == null) {
                                    arrayList = new ArrayList<>();
                                    arrayList.add(messageObject);
                                }
                                ChatActivity chatActivity = ChatActivity.this;
                                Context context = ChatActivityAdapter.this.mContext;
                                ChatActivity chatActivity2 = ChatActivity.this;
                                chatActivity.showDialog(new ShareAlert(context, chatActivity2, arrayList, null, null, ChatObject.isChannel(chatActivity2.currentChat), null, null, false, false, ChatActivity.this.themeDelegate) { // from class: org.telegram.ui.ChatActivity.ChatActivityAdapter.1.1
                                    public void dismissInternal() {
                                        super.dismissInternal();
                                        AndroidUtilities.requestAdjustResize(ChatActivity.this.getParentActivity(), ((BaseFragment) ChatActivity.this).classGuid);
                                        if (ChatActivity.this.chatActivityEnterView.getVisibility() == 0) {
                                            ((BaseFragment) ChatActivity.this).fragmentView.requestLayout();
                                        }
                                    }

                                    protected void onSend(LongSparseArray<TLRPC$Dialog> longSparseArray, int i3) {
                                        if (longSparseArray.size() == 1) {
                                            ChatActivity.this.undoView.showWithAction(longSparseArray.valueAt(0).id, 53, Integer.valueOf(i3));
                                        } else {
                                            ChatActivity.this.undoView.showWithAction(0, 53, Integer.valueOf(i3), Integer.valueOf(longSparseArray.size()), (Runnable) null, (Runnable) null);
                                        }
                                    }
                                });
                                AndroidUtilities.setAdjustResizeToNothing(ChatActivity.this.getParentActivity(), ((BaseFragment) ChatActivity.this).classGuid);
                                ((BaseFragment) ChatActivity.this).fragmentView.requestLayout();
                            } else if (!UserObject.isReplyUser(ChatActivity.this.currentUser) || (tLRPC$TL_messageReplyHeader = (tLRPC$Message = messageObject.messageOwner).reply_to) == null || (i2 = tLRPC$TL_messageReplyHeader.reply_to_top_id) == 0) {
                                ChatActivity.this.openOriginalReplyChat(messageObject);
                            } else {
                                ChatActivity.this.openDiscussionMessageChat(tLRPC$TL_messageReplyHeader.reply_to_peer_id.channel_id, null, i2, 0, -1, tLRPC$Message.fwd_from.saved_from_msg_id, messageObject);
                            }
                        }
                    }

                    public boolean needPlayMessage(MessageObject messageObject) {
                        ArrayList<MessageObject> arrayList;
                        if (messageObject.isVoice() || messageObject.isRoundVideo()) {
                            boolean playMessage = MediaController.getInstance().playMessage(messageObject);
                            MediaController instance = MediaController.getInstance();
                            if (playMessage) {
                                arrayList = ChatActivity.this.createVoiceMessagesPlaylist(messageObject, false);
                            } else {
                                arrayList = null;
                            }
                            instance.setVoiceMessagesPlaylist(arrayList, false);
                            return playMessage;
                        } else if (!messageObject.isMusic()) {
                            return false;
                        } else {
                            MediaController instance2 = MediaController.getInstance();
                            ChatActivity chatActivity = ChatActivity.this;
                            return instance2.setPlaylist(chatActivity.messages, messageObject, chatActivity.mergeDialogId);
                        }
                    }

                    public void videoTimerReached() {
                        ChatActivity.this.showNoSoundHint();
                    }

                    public void didPressTime(ChatMessageCell chatMessageCell4) {
                        ChatActivity.this.undoView.showWithAction(ChatActivity.this.dialog_id, 47, (Runnable) null);
                    }

                    public void didPressChannelAvatar(ChatMessageCell chatMessageCell4, TLRPC$Chat tLRPC$Chat, int i2, float f, float f2) {
                        if (tLRPC$Chat != null) {
                            if (((BaseFragment) ChatActivity.this).actionBar.isActionModeShowed() || ChatActivity.this.reportType >= 0) {
                                ChatActivity.this.processRowSelect(chatMessageCell4, true, f, f2);
                            } else {
                                openChat(chatMessageCell4, tLRPC$Chat, i2);
                            }
                        }
                    }

                    public void didPressHiddenForward(ChatMessageCell chatMessageCell4) {
                        if (chatMessageCell4.getMessageObject().isImportedForward()) {
                            didPressTime(chatMessageCell4);
                        } else {
                            ChatActivity.this.showForwardHint(chatMessageCell4);
                        }
                    }

                    public void didPressOther(ChatMessageCell chatMessageCell4, float f, float f2) {
                        MessageObject messageObject = chatMessageCell4.getMessageObject();
                        if (messageObject.type == 16) {
                            TLRPC$User tLRPC$User = ChatActivity.this.currentUser;
                            if (tLRPC$User != null) {
                                boolean isVideoCall = messageObject.isVideoCall();
                                ChatActivity chatActivity = ChatActivity.this;
                                TLRPC$UserFull tLRPC$UserFull = chatActivity.userInfo;
                                VoIPHelper.startCall(tLRPC$User, isVideoCall, tLRPC$UserFull != null && tLRPC$UserFull.video_calls_available, chatActivity.getParentActivity(), ChatActivity.this.getMessagesController().getUserFull(ChatActivity.this.currentUser.id), ChatActivity.this.getAccountInstance());
                                return;
                            }
                            return;
                        }
                        ChatActivity.this.createMenu(chatMessageCell4, true, false, f, f2, messageObject.isMusic());
                    }

                    public void didPressUserAvatar(ChatMessageCell chatMessageCell4, TLRPC$User tLRPC$User, float f, float f2) {
                        if (((BaseFragment) ChatActivity.this).actionBar.isActionModeShowed() || ChatActivity.this.reportType >= 0) {
                            ChatActivity.this.processRowSelect(chatMessageCell4, true, f, f2);
                        } else {
                            openProfile(tLRPC$User);
                        }
                    }

                    public boolean didLongPressUserAvatar(ChatMessageCell chatMessageCell4, TLRPC$User tLRPC$User, float f, float f2) {
                        AvatarPreviewer.Data data;
                        if (isAvatarPreviewerEnabled()) {
                            ChatActivity chatActivity = ChatActivity.this;
                            int i2 = (chatActivity.currentChat == null || (chatActivity.bottomOverlayChat != null && ChatActivity.this.bottomOverlayChat.getVisibility() == 0) || (ChatActivity.this.bottomOverlay != null && ChatActivity.this.bottomOverlay.getVisibility() == 0)) ? 0 : 1;
                            AvatarPreviewer.MenuItem[] menuItemArr = new AvatarPreviewer.MenuItem[i2 + 2];
                            menuItemArr[0] = AvatarPreviewer.MenuItem.OPEN_PROFILE;
                            menuItemArr[1] = AvatarPreviewer.MenuItem.SEND_MESSAGE;
                            if (i2 != 0) {
                                menuItemArr[2] = AvatarPreviewer.MenuItem.MENTION;
                            }
                            TLRPC$UserFull userFull = ChatActivity.this.getMessagesController().getUserFull(tLRPC$User.id);
                            if (userFull != null) {
                                data = AvatarPreviewer.Data.of(userFull, menuItemArr);
                            } else {
                                data = AvatarPreviewer.Data.of(tLRPC$User, ((BaseFragment) ChatActivity.this).classGuid, menuItemArr);
                            }
                            if (AvatarPreviewer.canPreview(data)) {
                                AvatarPreviewer.getInstance().show((ViewGroup) ((BaseFragment) ChatActivity.this).fragmentView, data, new ChatActivity$ChatActivityAdapter$1$$ExternalSyntheticLambda3(this, chatMessageCell4, tLRPC$User));
                                return true;
                            }
                        }
                        return false;
                    }

                    /* access modifiers changed from: private */
                    public /* synthetic */ void lambda$didLongPressUserAvatar$0(ChatMessageCell chatMessageCell4, TLRPC$User tLRPC$User, AvatarPreviewer.MenuItem menuItem) {
                        int i2 = AnonymousClass115.$SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem[menuItem.ordinal()];
                        if (i2 == 1) {
                            openProfile(tLRPC$User);
                        } else if (i2 == 4) {
                            openDialog(chatMessageCell4, tLRPC$User);
                        } else if (i2 == 5) {
                            appendMention(tLRPC$User);
                        }
                    }

                    private void appendMention(TLRPC$User tLRPC$User) {
                        SpannableStringBuilder spannableStringBuilder;
                        ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                        if (chatActivityEnterView != null) {
                            CharSequence fieldText = chatActivityEnterView.getFieldText();
                            if (fieldText != null) {
                                spannableStringBuilder = new SpannableStringBuilder(fieldText);
                                if (fieldText.charAt(fieldText.length() - 1) != ' ') {
                                    spannableStringBuilder.append((CharSequence) " ");
                                }
                            } else {
                                spannableStringBuilder = new SpannableStringBuilder();
                            }
                            if (spannableStringBuilder.length() > 0 && spannableStringBuilder.charAt(spannableStringBuilder.length() - 1) != ' ') {
                                spannableStringBuilder.append(' ');
                            }
                            if (tLRPC$User.username != null) {
                                spannableStringBuilder.append((CharSequence) "@").append((CharSequence) tLRPC$User.username).append((CharSequence) " ");
                            } else {
                                String firstName = UserObject.getFirstName(tLRPC$User, false);
                                SpannableString spannableString = new SpannableString(firstName + " ");
                                spannableString.setSpan(new URLSpanUserMention("" + tLRPC$User.id, 3), 0, spannableString.length(), 33);
                                spannableStringBuilder.append((CharSequence) spannableString);
                            }
                            ChatActivity.this.chatActivityEnterView.setFieldText(spannableStringBuilder);
                            AndroidUtilities.runOnUIThread(new ChatActivity$ChatActivityAdapter$1$$ExternalSyntheticLambda0(this), 200);
                        }
                    }

                    /* access modifiers changed from: private */
                    public /* synthetic */ void lambda$appendMention$1() {
                        ChatActivity.this.chatActivityEnterView.openKeyboard();
                    }

                    public boolean didLongPressChannelAvatar(ChatMessageCell chatMessageCell4, TLRPC$Chat tLRPC$Chat, int i2, float f, float f2) {
                        AvatarPreviewer.Data data;
                        if (isAvatarPreviewerEnabled()) {
                            AvatarPreviewer.MenuItem[] menuItemArr = {AvatarPreviewer.MenuItem.OPEN_PROFILE};
                            ChatActivity chatActivity = ChatActivity.this;
                            TLRPC$Chat tLRPC$Chat2 = chatActivity.currentChat;
                            if (tLRPC$Chat2 == null || tLRPC$Chat2.id != tLRPC$Chat.id || chatActivity.isThreadChat()) {
                                menuItemArr = (AvatarPreviewer.MenuItem[]) Arrays.copyOf(menuItemArr, 2);
                                menuItemArr[1] = tLRPC$Chat.broadcast ? AvatarPreviewer.MenuItem.OPEN_CHANNEL : AvatarPreviewer.MenuItem.OPEN_GROUP;
                            }
                            TLRPC$ChatFull chatFull = ChatActivity.this.getMessagesController().getChatFull(tLRPC$Chat.id);
                            if (chatFull != null) {
                                data = AvatarPreviewer.Data.of(tLRPC$Chat, chatFull, menuItemArr);
                            } else {
                                data = AvatarPreviewer.Data.of(tLRPC$Chat, ((BaseFragment) ChatActivity.this).classGuid, menuItemArr);
                            }
                            if (AvatarPreviewer.canPreview(data)) {
                                AvatarPreviewer.getInstance().show((ViewGroup) ((BaseFragment) ChatActivity.this).fragmentView, data, new ChatActivity$ChatActivityAdapter$1$$ExternalSyntheticLambda2(this, tLRPC$Chat, chatMessageCell4));
                                return true;
                            }
                        }
                        return false;
                    }

                    /* access modifiers changed from: private */
                    public /* synthetic */ void lambda$didLongPressChannelAvatar$2(TLRPC$Chat tLRPC$Chat, ChatMessageCell chatMessageCell4, AvatarPreviewer.MenuItem menuItem) {
                        int i2 = AnonymousClass115.$SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem[menuItem.ordinal()];
                        if (i2 == 1) {
                            openProfile(tLRPC$Chat);
                        } else if (i2 == 2 || i2 == 3) {
                            openChat(chatMessageCell4, tLRPC$Chat, 0);
                        }
                    }

                    private void openProfile(TLRPC$User tLRPC$User) {
                        if (tLRPC$User != null && tLRPC$User.id != ChatActivity.this.getUserConfig().getClientUserId()) {
                            Bundle bundle = new Bundle();
                            bundle.putLong("user_id", tLRPC$User.id);
                            ProfileActivity profileActivity = new ProfileActivity(bundle);
                            TLRPC$User tLRPC$User2 = ChatActivity.this.currentUser;
                            profileActivity.setPlayProfileAnimation((tLRPC$User2 == null || tLRPC$User2.id != tLRPC$User.id) ? 0 : 1);
                            AndroidUtilities.setAdjustResizeToNothing(ChatActivity.this.getParentActivity(), ((BaseFragment) ChatActivity.this).classGuid);
                            ChatActivity.this.presentFragment(profileActivity);
                        }
                    }

                    private void openProfile(TLRPC$Chat tLRPC$Chat) {
                        if (tLRPC$Chat != null) {
                            Bundle bundle = new Bundle();
                            bundle.putLong("chat_id", tLRPC$Chat.id);
                            ChatActivity.this.presentFragment(new ProfileActivity(bundle));
                        }
                    }

                    private void openDialog(ChatMessageCell chatMessageCell4, TLRPC$User tLRPC$User) {
                        if (tLRPC$User != null) {
                            Bundle bundle = new Bundle();
                            bundle.putLong("user_id", tLRPC$User.id);
                            if (ChatActivity.this.getMessagesController().checkCanOpenChat(bundle, ChatActivity.this, chatMessageCell4.getMessageObject())) {
                                ChatActivity.this.presentFragment(new ChatActivity(bundle));
                            }
                        }
                    }

                    private void openChat(ChatMessageCell chatMessageCell4, TLRPC$Chat tLRPC$Chat, int i2) {
                        ChatActivity chatActivity = ChatActivity.this;
                        TLRPC$Chat tLRPC$Chat2 = chatActivity.currentChat;
                        if (tLRPC$Chat2 != null && tLRPC$Chat.id == tLRPC$Chat2.id) {
                            chatActivity.scrollToMessageId(i2, chatMessageCell4.getMessageObject().getId(), true, 0, true, 0);
                        } else if (tLRPC$Chat2 == null || tLRPC$Chat.id != tLRPC$Chat2.id || chatActivity.isThreadChat()) {
                            Bundle bundle = new Bundle();
                            bundle.putLong("chat_id", tLRPC$Chat.id);
                            if (i2 != 0) {
                                bundle.putInt("message_id", i2);
                            }
                            if (ChatActivity.this.getMessagesController().checkCanOpenChat(bundle, ChatActivity.this, chatMessageCell4.getMessageObject())) {
                                ChatActivity.this.presentFragment(new ChatActivity(bundle));
                            }
                        }
                    }

                    private boolean isAvatarPreviewerEnabled() {
                        TLRPC$Chat tLRPC$Chat;
                        return UserObject.isUserSelf(ChatActivity.this.currentUser) || ((tLRPC$Chat = ChatActivity.this.currentChat) != null && (!ChatObject.isChannel(tLRPC$Chat) || ChatActivity.this.currentChat.megagroup));
                    }

                    public void didPressBotButton(ChatMessageCell chatMessageCell4, TLRPC$KeyboardButton tLRPC$KeyboardButton) {
                        if (ChatActivity.this.getParentActivity() == null) {
                            return;
                        }
                        if (ChatActivity.this.bottomOverlayChat.getVisibility() != 0 || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonSwitchInline) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonCallback) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonGame) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrl) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonBuy) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrlAuth) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUserProfile)) {
                            ChatActivity.this.chatActivityEnterView.didPressedBotButton(tLRPC$KeyboardButton, chatMessageCell4.getMessageObject(), chatMessageCell4.getMessageObject());
                        }
                    }

                    public void needShowPremiumFeatures(String str) {
                        ChatActivity.this.presentFragment(new PremiumPreviewFragment(str));
                    }

                    public void didLongPressBotButton(ChatMessageCell chatMessageCell4, TLRPC$KeyboardButton tLRPC$KeyboardButton) {
                        if (ChatActivity.this.getParentActivity() == null) {
                            return;
                        }
                        if ((ChatActivity.this.bottomOverlayChat.getVisibility() != 0 || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonSwitchInline) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonCallback) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonGame) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrl) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonBuy) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrlAuth) || (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUserProfile)) && (tLRPC$KeyboardButton instanceof TLRPC$TL_keyboardButtonUrl)) {
                            ChatActivity.this.openClickableLink(null, tLRPC$KeyboardButton.url, true, chatMessageCell4, chatMessageCell4.getMessageObject());
                            try {
                                chatMessageCell4.performHapticFeedback(0, 1);
                            } catch (Exception unused) {
                            }
                        }
                    }

                    public void didPressReaction(ChatMessageCell chatMessageCell4, TLRPC$TL_reactionCount tLRPC$TL_reactionCount, boolean z) {
                        int i2;
                        float f;
                        if (ChatActivity.this.getParentActivity() != null) {
                            if (!z) {
                                ChatActivity.this.selectReaction(chatMessageCell4.getPrimaryMessageObject(), null, 0.0f, 0.0f, ChatActivity.this.getMediaDataController().getReactionsMap().get(tLRPC$TL_reactionCount.reaction), false, false);
                            } else if (!ChatObject.isChannelAndNotMegaGroup(ChatActivity.this.currentChat)) {
                                chatMessageCell4.performHapticFeedback(0);
                                AnonymousClass2 r11 = new FrameLayout(ChatActivity.this.getParentActivity()) { // from class: org.telegram.ui.ChatActivity.ChatActivityAdapter.1.2
                                    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
                                        if (keyEvent.getKeyCode() == 4 && keyEvent.getRepeatCount() == 0) {
                                            ChatActivity.this.closeMenu();
                                        }
                                        return super.dispatchKeyEvent(keyEvent);
                                    }

                                    protected void onMeasure(int i3, int i4) {
                                        int min = Math.min(View.MeasureSpec.getSize(i4), AndroidUtilities.dp(288.0f));
                                        if (min == 0) {
                                            min = AndroidUtilities.dp(288.0f);
                                        }
                                        super.onMeasure(i3, View.MeasureSpec.makeMeasureSpec(min, Integer.MIN_VALUE));
                                    }
                                };
                                r11.setLayoutParams(LayoutHelper.createFrame(-2, -2.0f));
                                Rect rect = new Rect();
                                Drawable mutate = ContextCompat.getDrawable(ChatActivity.this.getParentActivity(), R.drawable.popup_fixed_alert).mutate();
                                mutate.setColorFilter(new PorterDuffColorFilter(ChatActivity.this.getThemedColor("actionBarDefaultSubmenuBackground"), PorterDuff.Mode.MULTIPLY));
                                mutate.getPadding(rect);
                                r11.setBackground(mutate);
                                ReactionsLayoutInBubble.ReactionButton reactionButton = chatMessageCell4.getReactionButton(tLRPC$TL_reactionCount.reaction);
                                if (reactionButton != null) {
                                    int i3 = chatMessageCell4.reactionsLayoutInBubble.y;
                                    AndroidUtilities.dp(28.0f);
                                    float f2 = (float) (chatMessageCell4.reactionsLayoutInBubble.x + reactionButton.x);
                                    chatMessageCell4.getLocationInWindow(new int[2]);
                                    Activity parentActivity = ChatActivity.this.getParentActivity();
                                    ChatActivity chatActivity = ChatActivity.this;
                                    r11.addView(new ReactedUsersListView(parentActivity, chatActivity.themeDelegate, ((BaseFragment) chatActivity).currentAccount, chatMessageCell4.getPrimaryMessageObject(), tLRPC$TL_reactionCount, false).setOnProfileSelectedListener(new ChatActivity$ChatActivityAdapter$1$$ExternalSyntheticLambda4(this)), LayoutHelper.createFrame(240, -2.0f));
                                    ChatActivity.this.scrimPopupWindow = new ActionBarPopupWindow(r11, -2, -2) { // from class: org.telegram.ui.ChatActivity.ChatActivityAdapter.1.3
                                        public void dismiss() {
                                            super.dismiss();
                                            ChatActivity chatActivity2 = ChatActivity.this;
                                            if (chatActivity2.scrimPopupWindow == this) {
                                                chatActivity2.scrimPopupWindow = null;
                                                chatActivity2.menuDeleteItem = null;
                                                ChatActivity.this.scrimPopupWindowItems = null;
                                                ChatActivity.this.chatLayoutManager.setCanScrollVertically(true);
                                                if (ChatActivity.this.scrimPopupWindowHideDimOnDismiss) {
                                                    ChatActivity.this.dimBehindView(false);
                                                } else {
                                                    ChatActivity.this.scrimPopupWindowHideDimOnDismiss = true;
                                                }
                                                ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                                                if (chatActivityEnterView != null) {
                                                    chatActivityEnterView.getEditField().setAllowDrawCursor(true);
                                                }
                                            }
                                        }
                                    };
                                    ChatActivity.this.scrimPopupWindow.setPauseNotifications(true);
                                    ChatActivity.this.scrimPopupWindow.setDismissAnimationDuration(220);
                                    ChatActivity.this.scrimPopupWindow.setOutsideTouchable(true);
                                    ChatActivity.this.scrimPopupWindow.setClippingEnabled(true);
                                    ChatActivity.this.scrimPopupWindow.setAnimationStyle(R.style.PopupContextAnimation);
                                    ChatActivity.this.scrimPopupWindow.setFocusable(true);
                                    r11.measure(View.MeasureSpec.makeMeasureSpec(AndroidUtilities.dp(1000.0f), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(AndroidUtilities.dp(1000.0f), Integer.MIN_VALUE));
                                    ChatActivity.this.scrimPopupWindow.setInputMethodMode(2);
                                    ChatActivity.this.scrimPopupWindow.setSoftInputMode(0);
                                    ChatActivity.this.scrimPopupWindow.getContentView().setFocusableInTouchMode(true);
                                    int height = ChatActivity.this.contentView.getHeight();
                                    int measuredHeight = r11.getMeasuredHeight();
                                    int measureKeyboardHeight = ChatActivity.this.contentView.measureKeyboardHeight();
                                    if (measureKeyboardHeight > AndroidUtilities.dp(20.0f)) {
                                        height += measureKeyboardHeight;
                                    }
                                    int max = Math.max(AndroidUtilities.dp(6.0f), Math.min((ChatActivity.this.chatListView.getMeasuredWidth() - AndroidUtilities.dp(6.0f)) - r11.getMeasuredWidth(), (int) (f2 - ((float) AndroidUtilities.dp(28.0f)))));
                                    if (AndroidUtilities.isTablet()) {
                                        int[] iArr = new int[2];
                                        ((BaseFragment) ChatActivity.this).fragmentView.getLocationInWindow(iArr);
                                        max += iArr[0];
                                    }
                                    if (measuredHeight < height) {
                                        float f3 = (float) measuredHeight;
                                        float f4 = ((float) height) / 2.0f;
                                        if (f3 >= f4 || ChatActivity.this.chatListView.getY() + chatMessageCell4.getY() + ((float) chatMessageCell4.reactionsLayoutInBubble.y) + ((float) reactionButton.y) <= f4) {
                                            f = ChatActivity.this.chatListView.getY() + chatMessageCell4.getY() + ((float) chatMessageCell4.reactionsLayoutInBubble.y) + ((float) reactionButton.y) + ((float) reactionButton.height);
                                        } else {
                                            f = (((ChatActivity.this.chatListView.getY() + chatMessageCell4.getY()) + ((float) chatMessageCell4.reactionsLayoutInBubble.y)) + ((float) reactionButton.y)) - f3;
                                        }
                                        i2 = (int) f;
                                    } else {
                                        i2 = ((BaseFragment) ChatActivity.this).inBubbleMode ? 0 : AndroidUtilities.statusBarHeight;
                                    }
                                    ChatActivity chatActivity2 = ChatActivity.this;
                                    chatActivity2.scrimPopupWindow.showAtLocation(chatActivity2.chatListView, 51, ChatActivity.this.scrimPopupX = max, ChatActivity.this.scrimPopupY = i2);
                                    ChatActivity.this.chatListView.stopScroll();
                                    ChatActivity.this.chatLayoutManager.setCanScrollVertically(false);
                                    ChatActivity.this.scrimViewReaction = tLRPC$TL_reactionCount.reaction;
                                    ChatActivity.this.dimBehindView((View) chatMessageCell4, true);
                                    ChatActivity.this.hideHints(false);
                                    if (ChatActivity.this.topUndoView != null) {
                                        ChatActivity.this.topUndoView.hide(true, 1);
                                    }
                                    if (ChatActivity.this.undoView != null) {
                                        ChatActivity.this.undoView.hide(true, 1);
                                    }
                                    ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                                    if (chatActivityEnterView != null) {
                                        chatActivityEnterView.getEditField().setAllowDrawCursor(false);
                                    }
                                }
                            }
                        }
                    }

                    /* access modifiers changed from: private */
                    public /* synthetic */ void lambda$didPressReaction$3(ReactedUsersListView reactedUsersListView, long j) {
                        Bundle bundle = new Bundle();
                        bundle.putLong("user_id", j);
                        ChatActivity.this.presentFragment(new ProfileActivity(bundle));
                        ChatActivity.this.closeMenu();
                    }

                    public void didPressVoteButtons(ChatMessageCell chatMessageCell4, ArrayList<TLRPC$TL_pollAnswer> arrayList, int i2, int i3, int i4) {
                        int i5;
                        int i6;
                        int i7;
                        int i8;
                        if (i2 < 0 && !arrayList.isEmpty()) {
                            ChatActivity.this.getSendMessagesHelper().sendVote(chatMessageCell4.getMessageObject(), arrayList, null);
                        } else if (ChatActivity.this.getParentActivity() != null) {
                            if (ChatActivity.this.pollHintView == null) {
                                ChatActivity.this.pollHintView = new HintView(ChatActivity.this.getParentActivity(), 5, ChatActivity.this.themeDelegate);
                                ChatActivity.this.pollHintView.setAlpha(0.0f);
                                ChatActivity.this.pollHintView.setVisibility(4);
                                ChatActivity chatActivity = ChatActivity.this;
                                int indexOfChild = chatActivity.contentView.indexOfChild(chatActivity.chatActivityEnterView);
                                if (indexOfChild != -1) {
                                    ChatActivity chatActivity2 = ChatActivity.this;
                                    chatActivity2.contentView.addView(chatActivity2.pollHintView, indexOfChild + 1, LayoutHelper.createFrame(-2, -2.0f, 51, 19.0f, 0.0f, 19.0f, 0.0f));
                                } else {
                                    return;
                                }
                            }
                            if (!arrayList.isEmpty() || i2 >= 0) {
                                i6 = i3;
                                i5 = i4;
                            } else {
                                ArrayList<ChatMessageCell.PollButton> pollButtons = chatMessageCell4.getPollButtons();
                                int size = pollButtons.size();
                                int i9 = 0;
                                float f = 0.0f;
                                while (true) {
                                    if (i9 >= size) {
                                        i7 = i3;
                                        i8 = i4;
                                        break;
                                    }
                                    ChatMessageCell.PollButton pollButton = pollButtons.get(i9);
                                    float y = ((chatMessageCell4.getY() + ((float) pollButton.y)) - ((float) AndroidUtilities.dp(4.0f))) - ChatActivity.this.chatListViewPaddingTop;
                                    ChatActivity.this.pollHintX = pollButton.x + AndroidUtilities.dp(13.3f);
                                    ChatActivity.this.pollHintY = (pollButton.y - AndroidUtilities.dp(6.0f)) + i4;
                                    if (y > 0.0f) {
                                        i7 = ChatActivity.this.pollHintX;
                                        i8 = ChatActivity.this.pollHintY;
                                        f = 0.0f;
                                        break;
                                    }
                                    i9++;
                                    f = y;
                                }
                                if (f != 0.0f) {
                                    ChatActivity.this.chatListView.smoothScrollBy(0, (int) f);
                                    ChatActivity.this.pollHintCell = chatMessageCell4;
                                    return;
                                }
                                i6 = i7;
                                i5 = i8;
                            }
                            ChatActivity.this.pollHintView.showForMessageCell(chatMessageCell4, Integer.valueOf(i2), i6, i5, true);
                        }
                    }

                    public void didPressCancelSendButton(ChatMessageCell chatMessageCell4) {
                        MessageObject messageObject = chatMessageCell4.getMessageObject();
                        if (messageObject.messageOwner.send_state != 0) {
                            ChatActivity.this.getSendMessagesHelper().cancelSendingMessage(messageObject);
                        }
                    }

                    public void didLongPress(ChatMessageCell chatMessageCell4, float f, float f2) {
                        ChatActivity.this.createMenu(chatMessageCell4, false, false, f, f2);
                        ChatActivity chatActivity = ChatActivity.this;
                        chatActivity.startMultiselect(chatActivity.chatListView.getChildAdapterPosition(chatMessageCell4));
                    }

                    public boolean canPerformActions() {
                        return ((BaseFragment) ChatActivity.this).actionBar != null && !((BaseFragment) ChatActivity.this).actionBar.isActionModeShowed() && ChatActivity.this.reportType < 0 && !((BaseFragment) ChatActivity.this).inPreviewMode;
                    }

                    public void didPressUrl(ChatMessageCell chatMessageCell4, CharacterStyle characterStyle, boolean z) {
                        ChatActivity.this.didPressMessageUrl(characterStyle, z, chatMessageCell4.getMessageObject(), chatMessageCell4);
                    }

                    public void needOpenWebView(MessageObject messageObject, String str, String str2, String str3, String str4, int i2, int i3) {
                        try {
                            EmbedBottomSheet.show(ChatActivity.this.getParentActivity(), messageObject, ChatActivity.this.photoViewerProvider, str2, str3, str4, str, i2, i3, ChatActivity.this.isKeyboardVisible());
                        } catch (Throwable th) {
                            FileLog.e(th);
                        }
                    }

                    public void didPressReplyMessage(ChatMessageCell chatMessageCell4, int i2) {
                        if (UserObject.isReplyUser(ChatActivity.this.currentUser)) {
                            didPressSideButton(chatMessageCell4);
                            return;
                        }
                        MessageObject messageObject = chatMessageCell4.getMessageObject();
                        if (ChatActivity.this.chatMode == 2 || ChatActivity.this.chatMode == 1) {
                            ChatActivity.this.chatActivityDelegate.openReplyMessage(i2);
                            ChatActivity.this.finishFragment();
                            return;
                        }
                        ChatActivity.this.scrollToMessageId(i2, messageObject.getId(), true, messageObject.getDialogId() == ChatActivity.this.mergeDialogId ? 1 : 0, true, 0);
                    }

                    public void didPressViaBotNotInline(ChatMessageCell chatMessageCell4, long j) {
                        Bundle bundle = new Bundle();
                        bundle.putLong("user_id", j);
                        if (ChatActivity.this.getMessagesController().checkCanOpenChat(bundle, ChatActivity.this, chatMessageCell4.getMessageObject())) {
                            ChatActivity.this.presentFragment(new ChatActivity(bundle));
                        }
                    }

                    public void didPressViaBot(ChatMessageCell chatMessageCell4, String str) {
                        if (ChatActivity.this.bottomOverlayChat != null && ChatActivity.this.bottomOverlayChat.getVisibility() == 0) {
                            return;
                        }
                        if ((ChatActivity.this.bottomOverlay == null || ChatActivity.this.bottomOverlay.getVisibility() != 0) && ChatActivity.this.chatActivityEnterView != null && str != null && str.length() > 0) {
                            ChatActivityEnterView chatActivityEnterView = ChatActivity.this.chatActivityEnterView;
                            chatActivityEnterView.setFieldText("@" + str + " ");
                            ChatActivity.this.chatActivityEnterView.openKeyboard();
                        }
                    }

                    public void didStartVideoStream(MessageObject messageObject) {
                        if (messageObject.isVideo()) {
                            ChatActivity.this.sendSecretMessageRead(messageObject, true);
                        }
                    }

                    public void needReloadPolls() {
                        ChatActivity.this.invalidateMessagesVisiblePart();
                    }

                    /* JADX WARNING: Code restructure failed: missing block: B:94:0x022e, code lost:
                        if (r11.exists() != false) goto L_0x0232;
                     */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void didPressImage(org.telegram.ui.Cells.ChatMessageCell r11, float r12, float r13) {
                        /*
                        // Method dump skipped, instructions count: 766
                        */
                        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.ChatActivityAdapter.AnonymousClass1.didPressImage(org.telegram.ui.Cells.ChatMessageCell, float, float):void");
                    }

                    /* access modifiers changed from: private */
                    public /* synthetic */ void lambda$didPressImage$4(MessageObject messageObject) {
                        ChatActivity.this.getSendMessagesHelper().sendMessage(messageObject.getDiceEmoji(), ChatActivity.this.dialog_id, ChatActivity.this.replyingMessageObject, ChatActivity.this.getThreadMessage(), null, false, null, null, null, true, 0, null);
                    }

                    public void didPressInstantButton(ChatMessageCell chatMessageCell4, int i2) {
                        TLRPC$WebPage tLRPC$WebPage;
                        TLRPC$WebPage tLRPC$WebPage2;
                        MessageObject messageObject = chatMessageCell4.getMessageObject();
                        if (i2 == 8) {
                            PollVotesAlert.showForPoll(ChatActivity.this, messageObject);
                        } else if (i2 == 0) {
                            TLRPC$MessageMedia tLRPC$MessageMedia = messageObject.messageOwner.media;
                            if (tLRPC$MessageMedia != null && (tLRPC$WebPage2 = tLRPC$MessageMedia.webpage) != null && tLRPC$WebPage2.cached_page != null) {
                                ArticleViewer.getInstance().setParentActivity(ChatActivity.this.getParentActivity(), ChatActivity.this);
                                ArticleViewer.getInstance().open(messageObject);
                            }
                        } else if (i2 == 5) {
                            long j = messageObject.messageOwner.media.user_id;
                            TLRPC$User tLRPC$User = null;
                            if (j != 0) {
                                tLRPC$User = MessagesController.getInstance(((BaseFragment) ChatActivity.this).currentAccount).getUser(Long.valueOf(j));
                            }
                            ChatActivity chatActivity = ChatActivity.this;
                            TLRPC$MessageMedia tLRPC$MessageMedia2 = messageObject.messageOwner.media;
                            chatActivity.openVCard(tLRPC$User, tLRPC$MessageMedia2.vcard, tLRPC$MessageMedia2.first_name, tLRPC$MessageMedia2.last_name);
                        } else if (messageObject.isSponsored()) {
                            Bundle bundle = new Bundle();
                            if (messageObject.sponsoredChatInvite != null) {
                                ChatActivity chatActivity2 = ChatActivity.this;
                                Context context = ChatActivityAdapter.this.mContext;
                                TLRPC$ChatInvite tLRPC$ChatInvite = messageObject.sponsoredChatInvite;
                                String str = messageObject.sponsoredChatInviteHash;
                                ChatActivity chatActivity3 = ChatActivity.this;
                                chatActivity2.showDialog(new JoinGroupAlert(context, tLRPC$ChatInvite, str, chatActivity3, chatActivity3.themeDelegate));
                                return;
                            }
                            long peerId = MessageObject.getPeerId(messageObject.messageOwner.from_id);
                            if (peerId < 0) {
                                bundle.putLong("chat_id", -peerId);
                            } else {
                                bundle.putLong("user_id", peerId);
                            }
                            int i3 = messageObject.sponsoredChannelPost;
                            if (i3 != 0) {
                                bundle.putInt("message_id", i3);
                            }
                            String str2 = messageObject.botStartParam;
                            if (str2 != null) {
                                bundle.putString("inline_query", str2);
                            }
                            if (ChatActivity.this.getMessagesController().checkCanOpenChat(bundle, ChatActivity.this)) {
                                ChatActivity.this.presentFragment(new ChatActivity(bundle));
                            }
                        } else {
                            TLRPC$MessageMedia tLRPC$MessageMedia3 = messageObject.messageOwner.media;
                            if (tLRPC$MessageMedia3 != null && (tLRPC$WebPage = tLRPC$MessageMedia3.webpage) != null && !ChatActivity.this.openLinkInternally(tLRPC$WebPage.url, messageObject.getId())) {
                                Browser.openUrl(ChatActivity.this.getParentActivity(), messageObject.messageOwner.media.webpage.url);
                            }
                        }
                    }

                    public void didPressCommentButton(ChatMessageCell chatMessageCell4) {
                        MessageObject messageObject;
                        int i2;
                        long j;
                        MessageObject.GroupedMessages currentMessagesGroup = chatMessageCell4.getCurrentMessagesGroup();
                        if (currentMessagesGroup == null || currentMessagesGroup.messages.isEmpty()) {
                            messageObject = chatMessageCell4.getMessageObject();
                        } else {
                            messageObject = currentMessagesGroup.messages.get(0);
                        }
                        TLRPC$MessageReplies tLRPC$MessageReplies = messageObject.messageOwner.replies;
                        if (tLRPC$MessageReplies != null) {
                            int i3 = tLRPC$MessageReplies.read_max_id;
                            j = tLRPC$MessageReplies.channel_id;
                            i2 = i3;
                        } else {
                            j = 0;
                            i2 = -1;
                        }
                        ChatActivity chatActivity = ChatActivity.this;
                        chatActivity.openDiscussionMessageChat(chatActivity.currentChat.id, messageObject, messageObject.getId(), j, i2, 0, null);
                    }

                    public String getAdminRank(long j) {
                        if (!ChatObject.isChannel(ChatActivity.this.currentChat)) {
                            return null;
                        }
                        ChatActivity chatActivity = ChatActivity.this;
                        if (chatActivity.currentChat.megagroup) {
                            return chatActivity.getMessagesController().getAdminRank(ChatActivity.this.currentChat.id, j);
                        }
                        return null;
                    }

                    public boolean shouldRepeatSticker(MessageObject messageObject) {
                        return !ChatActivity.this.alreadyPlayedStickers.containsKey(messageObject);
                    }

                    public void setShouldNotRepeatSticker(MessageObject messageObject) {
                        ChatActivity.this.alreadyPlayedStickers.put(messageObject, Boolean.TRUE);
                    }

                    public TextSelectionHelper.ChatListTextSelectionHelper getTextSelectionHelper() {
                        return ChatActivity.this.textSelectionHelper;
                    }

                    public boolean hasSelectedMessages() {
                        return ChatActivity.this.selectedMessagesIds[0].size() + ChatActivity.this.selectedMessagesIds[1].size() > 0;
                    }

                    public void onDiceFinished() {
                        if (!ChatActivity.this.fireworksOverlay.isStarted()) {
                            ChatActivity.this.fireworksOverlay.start();
                            ChatActivity.this.fireworksOverlay.performHapticFeedback(3, 2);
                        }
                    }

                    public PinchToZoomHelper getPinchToZoomHelper() {
                        return ChatActivity.this.pinchToZoomHelper;
                    }

                    public boolean keyboardIsOpened() {
                        return ChatActivity.this.contentView.getKeyboardHeight() + ChatActivity.this.chatEmojiViewPadding >= AndroidUtilities.dp(20.0f);
                    }

                    public boolean isLandscape() {
                        return ChatActivity.this.contentView.getMeasuredWidth() > ChatActivity.this.contentView.getMeasuredHeight();
                    }

                    public void invalidateBlur() {
                        ChatActivity.this.contentView.invalidateBlur();
                    }

                    public boolean onAccessibilityAction(int i2, Bundle bundle) {
                        if (i2 != 16 && i2 != R.id.acc_action_small_button && i2 != R.id.acc_action_msg_options) {
                            return false;
                        }
                        if (((BaseFragment) ChatActivity.this).inPreviewMode) {
                            ChatActivity chatActivity = ChatActivity.this;
                            if (chatActivity.allowExpandPreviewByClick) {
                                if (((BaseFragment) chatActivity).parentLayout != null) {
                                    ((BaseFragment) ChatActivity.this).parentLayout.expandPreviewFragment();
                                }
                                return true;
                            }
                        }
                        return !canPerformActions();
                    }
                });
                chatMessageCell = chatMessageCell2;
                if (ChatActivity.this.currentEncryptedChat == null) {
                    chatMessageCell3.setAllowAssistant(true);
                    chatMessageCell = chatMessageCell2;
                }
            } else if (i == 1) {
                AnonymousClass2 r4 = new ChatActionCell(this, this.mContext, true, ChatActivity.this.themeDelegate) { // from class: org.telegram.ui.ChatActivity.ChatActivityAdapter.2
                    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
                        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
                        accessibilityNodeInfo.setVisibleToUser(true);
                    }
                };
                r4.setInvalidateColors(true);
                r4.setDelegate(new ChatActionCell.ChatActionCellDelegate() { // from class: org.telegram.ui.ChatActivity.ChatActivityAdapter.3
                    public /* synthetic */ void needOpenInviteLink(TLRPC$TL_chatInviteExported tLRPC$TL_chatInviteExported) {
                        ChatActionCell.ChatActionCellDelegate.CC.$default$needOpenInviteLink(this, tLRPC$TL_chatInviteExported);
                    }

                    public void didClickImage(ChatActionCell chatActionCell) {
                        MessageObject messageObject = chatActionCell.getMessageObject();
                        PhotoViewer.getInstance().setParentActivity(ChatActivity.this.getParentActivity(), ChatActivity.this.themeDelegate);
                        TLRPC$PhotoSize closestPhotoSizeWithSize = FileLoader.getClosestPhotoSizeWithSize(messageObject.photoThumbs, 640);
                        if (closestPhotoSizeWithSize != null) {
                            PhotoViewer.getInstance().openPhoto(closestPhotoSizeWithSize.location, ImageLocation.getForPhoto(closestPhotoSizeWithSize, messageObject.messageOwner.action.photo), ChatActivity.this.photoViewerProvider);
                            return;
                        }
                        PhotoViewer.getInstance().openPhoto(messageObject, (ChatActivity) null, 0, 0, ChatActivity.this.photoViewerProvider);
                    }

                    public boolean didLongPress(ChatActionCell chatActionCell, float f, float f2) {
                        return ChatActivity.this.createMenu(chatActionCell, false, false, f, f2);
                    }

                    public void needOpenUserProfile(long j) {
                        if (j < 0) {
                            Bundle bundle = new Bundle();
                            bundle.putLong("chat_id", -j);
                            if (ChatActivity.this.getMessagesController().checkCanOpenChat(bundle, ChatActivity.this)) {
                                ChatActivity.this.presentFragment(new ChatActivity(bundle));
                            }
                        } else if (j != ChatActivity.this.getUserConfig().getClientUserId()) {
                            Bundle bundle2 = new Bundle();
                            bundle2.putLong("user_id", j);
                            ChatActivity chatActivity = ChatActivity.this;
                            if (chatActivity.currentEncryptedChat != null && j == chatActivity.currentUser.id) {
                                bundle2.putLong("dialog_id", chatActivity.dialog_id);
                            }
                            ProfileActivity profileActivity = new ProfileActivity(bundle2);
                            TLRPC$User tLRPC$User = ChatActivity.this.currentUser;
                            profileActivity.setPlayProfileAnimation((tLRPC$User == null || tLRPC$User.id != j) ? 0 : 1);
                            ChatActivity.this.presentFragment(profileActivity);
                        }
                    }

                    public void didPressReplyMessage(ChatActionCell chatActionCell, int i2) {
                        MessageObject messageObject = chatActionCell.getMessageObject();
                        ChatActivity.this.scrollToMessageId(i2, messageObject.getId(), true, messageObject.getDialogId() == ChatActivity.this.mergeDialogId ? 1 : 0, true, 0);
                    }
                });
                chatMessageCell = r4;
            } else if (i == 2) {
                chatMessageCell = new ChatUnreadCell(this.mContext, ChatActivity.this.themeDelegate);
            } else if (i == 3) {
                BotHelpCell botHelpCell = new BotHelpCell(this.mContext, ChatActivity.this.themeDelegate);
                botHelpCell.setDelegate(new ChatActivity$ChatActivityAdapter$$ExternalSyntheticLambda0(this));
                chatMessageCell = botHelpCell;
            } else if (i == 4) {
                Context context = this.mContext;
                ChatActivity chatActivity = ChatActivity.this;
                chatMessageCell = new ChatLoadingCell(context, chatActivity.contentView, chatActivity.themeDelegate);
            } else {
                chatMessageCell = null;
            }
            chatMessageCell.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new RecyclerListView.Holder(chatMessageCell);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onCreateViewHolder$0(String str) {
            if (str.startsWith("@")) {
                ChatActivity.this.getMessagesController().openByUserName(str.substring(1), ChatActivity.this, 0);
            } else if (str.startsWith("#") || str.startsWith("$")) {
                DialogsActivity dialogsActivity = new DialogsActivity(null);
                dialogsActivity.setSearchString(str);
                ChatActivity.this.presentFragment(dialogsActivity);
            } else if (str.startsWith("/")) {
                ChatActivity.this.chatActivityEnterView.setCommand(null, str, false, false);
                if (ChatActivity.this.chatActivityEnterView.getFieldText() == null) {
                    ChatActivity.this.hideFieldPanel(false);
                }
            } else {
                ChatActivity.this.processExternalUrl(0, str, false);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:131:0x0242, code lost:
            if (r5.getSenderId() == r3.getSenderId()) goto L_0x0244;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x0244, code lost:
            r9 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:149:0x0296, code lost:
            if (org.telegram.messenger.MessageObject.getPeerId(r5) == org.telegram.messenger.MessageObject.getPeerId(r3.messageOwner.fwd_from.from_id)) goto L_0x0244;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:192:0x033d, code lost:
            if (r4.getSenderId() == r3.getSenderId()) goto L_0x033f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:193:0x033f, code lost:
            r14 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:210:0x0391, code lost:
            if (org.telegram.messenger.MessageObject.getPeerId(r4) == org.telegram.messenger.MessageObject.getPeerId(r3.messageOwner.fwd_from.from_id)) goto L_0x033f;
         */
        /* JADX WARNING: Removed duplicated region for block: B:105:0x01e0  */
        /* JADX WARNING: Removed duplicated region for block: B:153:0x02a0  */
        /* JADX WARNING: Removed duplicated region for block: B:223:0x03bf  */
        /* JADX WARNING: Removed duplicated region for block: B:225:0x03c2  */
        /* JADX WARNING: Removed duplicated region for block: B:228:0x03d2  */
        /* JADX WARNING: Removed duplicated region for block: B:229:0x03d4  */
        /* JADX WARNING: Removed duplicated region for block: B:238:0x03fd  */
        /* JADX WARNING: Removed duplicated region for block: B:241:0x040d  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x0126  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x012d  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x0197  */
        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onBindViewHolder(androidx.recyclerview.widget.RecyclerView.ViewHolder r22, int r23) {
            /*
            // Method dump skipped, instructions count: 1354
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.ChatActivityAdapter.onBindViewHolder(androidx.recyclerview.widget.RecyclerView$ViewHolder, int):void");
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemViewType(int i) {
            if (ChatActivity.this.clearingHistory && i == this.botInfoEmptyRow) {
                return 3;
            }
            int i2 = this.messagesStartRow;
            if (i >= i2 && i < this.messagesEndRow) {
                return (this.isFrozen ? this.frozenMessages : ChatActivity.this.messages).get(i - i2).contentType;
            } else if (i == this.botInfoRow) {
                return 3;
            } else {
                return 4;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:72:0x017c  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onViewAttachedToWindow(final androidx.recyclerview.widget.RecyclerView.ViewHolder r11) {
            /*
            // Method dump skipped, instructions count: 580
            */
            throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.ChatActivity.ChatActivityAdapter.onViewAttachedToWindow(androidx.recyclerview.widget.RecyclerView$ViewHolder):void");
        }

        public void updateRowAtPosition(int i) {
            int i2;
            if (ChatActivity.this.chatLayoutManager != null && !this.isFrozen) {
                int i3 = 0;
                if (!ChatActivity.this.wasManualScroll && ChatActivity.this.unreadMessageObject != null) {
                    int childCount = ChatActivity.this.chatListView.getChildCount();
                    int i4 = 0;
                    while (true) {
                        if (i4 >= childCount) {
                            break;
                        }
                        View childAt = ChatActivity.this.chatListView.getChildAt(i4);
                        if (!(childAt instanceof ChatMessageCell) || ((ChatMessageCell) childAt).getMessageObject() != ChatActivity.this.unreadMessageObject) {
                            i4++;
                        } else {
                            ChatActivity chatActivity = ChatActivity.this;
                            if (chatActivity.messages.indexOf(chatActivity.unreadMessageObject) >= 0) {
                                int i5 = this.messagesStartRow;
                                ChatActivity chatActivity2 = ChatActivity.this;
                                i2 = i5 + chatActivity2.messages.indexOf(chatActivity2.unreadMessageObject);
                                i3 = (ChatActivity.this.chatListView.getMeasuredHeight() - childAt.getBottom()) - ChatActivity.this.chatListView.getPaddingBottom();
                            }
                        }
                    }
                }
                i2 = -1;
                notifyItemChanged(i);
                if (i2 != -1) {
                    ChatActivity.this.chatLayoutManager.scrollToPositionWithOffset(i2, i3);
                }
            }
        }

        public void invalidateRowWithMessageObject(MessageObject messageObject) {
            int childCount = ChatActivity.this.chatListView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = ChatActivity.this.chatListView.getChildAt(i);
                if (childAt instanceof ChatMessageCell) {
                    ChatMessageCell chatMessageCell = (ChatMessageCell) childAt;
                    if (chatMessageCell.getMessageObject() == messageObject) {
                        chatMessageCell.invalidate();
                        return;
                    }
                }
            }
        }

        public View updateRowWithMessageObject(MessageObject messageObject, boolean z) {
            if (z) {
                int childCount = ChatActivity.this.chatListView.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = ChatActivity.this.chatListView.getChildAt(i);
                    if (childAt instanceof ChatMessageCell) {
                        ChatMessageCell chatMessageCell = (ChatMessageCell) childAt;
                        if (chatMessageCell.getMessageObject() == messageObject && !chatMessageCell.isAdminLayoutChanged()) {
                            chatMessageCell.setMessageObject(messageObject, chatMessageCell.getCurrentMessagesGroup(), chatMessageCell.isPinnedBottom(), chatMessageCell.isPinnedTop());
                            return chatMessageCell;
                        }
                    }
                }
            }
            int indexOf = (this.isFrozen ? this.frozenMessages : ChatActivity.this.messages).indexOf(messageObject);
            if (indexOf == -1) {
                return null;
            }
            updateRowAtPosition(indexOf + this.messagesStartRow);
            return null;
        }

        public void notifyDataSetChanged(boolean z) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify data set changed fragmentOpened=" + ChatActivity.this.fragmentOpened);
            }
            try {
                if (z) {
                    ChatActivity chatActivity = ChatActivity.this;
                    if (chatActivity.fragmentOpened) {
                        if (chatActivity.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                            ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
                        }
                        updateRowsInternal();
                        super.notifyDataSetChanged();
                        return;
                    }
                }
                super.notifyDataSetChanged();
                return;
            } catch (Exception e) {
                FileLog.e(e);
                return;
            }
            ChatActivity.this.chatListView.setItemAnimator(null);
            updateRowsInternal();
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyDataSetChanged() {
            notifyDataSetChanged(false);
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemChanged(int i) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item changed " + i);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            try {
                super.notifyItemChanged(i);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRangeChanged(int i, int i2) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item range changed " + i + ":" + i2);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            try {
                super.notifyItemRangeChanged(i, i2);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemInserted(int i) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item inserted " + i);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            try {
                super.notifyItemInserted(i);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemMoved(int i, int i2) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item moved" + i + ":" + i2);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            try {
                super.notifyItemMoved(i, i2);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRangeInserted(int i, int i2) {
            int i3;
            int i4;
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item range inserted" + i + ":" + i2);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            if (i == 1 && i2 > 0 && (i3 = i + i2) >= (i4 = this.messagesStartRow) && i3 < this.messagesEndRow) {
                MessageObject messageObject = ChatActivity.this.messages.get(i3 - i4);
                MessageObject messageObject2 = ChatActivity.this.messages.get((i3 - this.messagesStartRow) - 1);
                if ((ChatActivity.this.currentChat != null && messageObject.getFromChatId() == messageObject2.getFromChatId()) || (ChatActivity.this.currentUser != null && messageObject.isOutOwner() == messageObject2.isOutOwner())) {
                    notifyItemChanged(i);
                }
            }
            try {
                super.notifyItemRangeInserted(i, i2);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRemoved(int i) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item removed " + i);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            try {
                super.notifyItemRemoved(i);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimatableAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRangeRemoved(int i, int i2) {
            if (BuildVars.LOGS_ENABLED) {
                FileLog.d("notify item range removed" + i + ":" + i2);
            }
            if (!((BaseFragment) ChatActivity.this).fragmentBeginToShow) {
                ChatActivity.this.chatListView.setItemAnimator(null);
            } else if (ChatActivity.this.chatListView.getItemAnimator() != ChatActivity.this.chatListItemAnimator) {
                ChatActivity.this.chatListView.setItemAnimator(ChatActivity.this.chatListItemAnimator);
            }
            updateRowsInternal();
            try {
                super.notifyItemRangeRemoved(i, i2);
            } catch (Exception e) {
                FileLog.e(e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.telegram.ui.ChatActivity$115  reason: invalid class name */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass115 {
        static final /* synthetic */ int[] $SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem;

        static {
            int[] iArr = new int[AvatarPreviewer.MenuItem.values().length];
            $SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem = iArr;
            try {
                iArr[AvatarPreviewer.MenuItem.OPEN_PROFILE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem[AvatarPreviewer.MenuItem.OPEN_GROUP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem[AvatarPreviewer.MenuItem.OPEN_CHANNEL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem[AvatarPreviewer.MenuItem.SEND_MESSAGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$telegram$ui$AvatarPreviewer$MenuItem[AvatarPreviewer.MenuItem.MENTION.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean openLinkInternally(String str, int i) {
        if (!(this.currentChat == null || str == null)) {
            if (str.startsWith("tg:privatepost") || str.startsWith("tg://privatepost")) {
                Uri parse = Uri.parse(str.replace("tg:privatepost", "tg://telegram.org").replace("tg://privatepost", "tg://telegram.org"));
                int intValue = Utilities.parseInt((CharSequence) parse.getQueryParameter("post")).intValue();
                long longValue = Utilities.parseLong(parse.getQueryParameter("channel")).longValue();
                int intValue2 = Utilities.parseInt((CharSequence) parse.getQueryParameter("thread")).intValue();
                long j = this.currentChat.id;
                if (longValue == j && intValue != 0) {
                    if (intValue2 != 0) {
                        openDiscussionMessageChat(j, null, intValue2, 0, -1, 0, null);
                    } else {
                        this.showScrollToMessageError = true;
                        if (this.chatMode == 2) {
                            this.chatActivityDelegate.openReplyMessage(intValue);
                            finishFragment();
                        } else {
                            scrollToMessageId(intValue, i, true, 0, false, 0);
                        }
                    }
                    return true;
                }
            } else {
                String str2 = this.currentChat.username;
                if (str2 != null) {
                    String lowerCase = str2.toLowerCase();
                    if (publicMsgUrlPattern == null) {
                        publicMsgUrlPattern = Pattern.compile("(https://)?t.me/([0-9a-zA-Z_]+)/([0-9]+)");
                        voiceChatUrlPattern = Pattern.compile("(https://)?t.me/([0-9a-zA-Z_]+)\\?(voicechat+)");
                    }
                    Matcher matcher = publicMsgUrlPattern.matcher(str);
                    if (matcher.find(2) && matcher.find(3) && lowerCase.equals(matcher.group(2).toLowerCase())) {
                        Uri parse2 = Uri.parse(str);
                        int intValue3 = Utilities.parseInt((CharSequence) parse2.getQueryParameter("thread")).intValue();
                        int intValue4 = Utilities.parseInt((CharSequence) parse2.getQueryParameter("comment")).intValue();
                        if (intValue3 != 0 || intValue4 != 0) {
                            return false;
                        }
                        int parseInt = Integer.parseInt(matcher.group(3));
                        this.showScrollToMessageError = true;
                        if (this.chatMode == 2) {
                            this.chatActivityDelegate.openReplyMessage(parseInt);
                            finishFragment();
                        } else {
                            int timestampFromLink = LaunchActivity.getTimestampFromLink(parse2);
                            this.startFromVideoTimestamp = timestampFromLink;
                            if (timestampFromLink >= 0) {
                                this.startFromVideoMessageId = parseInt;
                            }
                            scrollToMessageId(parseInt, i, true, 0, false, 0);
                        }
                        return true;
                    } else if (str.startsWith("tg:resolve") || str.startsWith("tg://resolve")) {
                        Uri parse3 = Uri.parse(str.replace("tg:resolve", "tg://telegram.org").replace("tg://resolve", "tg://telegram.org"));
                        String lowerCase2 = parse3.getQueryParameter("domain").toLowerCase();
                        int intValue5 = Utilities.parseInt((CharSequence) parse3.getQueryParameter("post")).intValue();
                        int intValue6 = Utilities.parseInt((CharSequence) parse3.getQueryParameter("thread")).intValue();
                        int intValue7 = Utilities.parseInt((CharSequence) parse3.getQueryParameter("comment")).intValue();
                        if (!lowerCase.equals(lowerCase2) || intValue5 == 0 || intValue6 != 0 || intValue7 != 0) {
                            return false;
                        }
                        if (this.chatMode == 2) {
                            this.chatActivityDelegate.openReplyMessage(intValue5);
                            finishFragment();
                        } else {
                            scrollToMessageId(intValue5, i, true, 0, false, 0);
                        }
                        return true;
                    } else {
                        Matcher matcher2 = voiceChatUrlPattern.matcher(str);
                        try {
                            if (matcher2.find(2) && matcher2.find(3) && lowerCase.equals(matcher2.group(2).toLowerCase())) {
                                String queryParameter = Uri.parse(str).getQueryParameter("voicechat");
                                if (!TextUtils.isEmpty(queryParameter)) {
                                    this.voiceChatHash = queryParameter;
                                    checkGroupCallJoin(true);
                                    return true;
                                }
                            }
                        } catch (Exception e) {
                            FileLog.e(e);
                        }
                    }
                } else {
                    if (privateMsgUrlPattern == null) {
                        privateMsgUrlPattern = Pattern.compile("(https://)?t.me/c/([0-9]+)/([0-9]+)");
                    }
                    Matcher matcher3 = privateMsgUrlPattern.matcher(str);
                    if (matcher3.find(2) && matcher3.find(3)) {
                        long parseLong = Long.parseLong(matcher3.group(2));
                        int parseInt2 = Integer.parseInt(matcher3.group(3));
                        if (parseLong == this.currentChat.id && parseInt2 != 0) {
                            Uri parse4 = Uri.parse(str);
                            int intValue8 = Utilities.parseInt((CharSequence) parse4.getQueryParameter("thread")).intValue();
                            int intValue9 = Utilities.parseInt((CharSequence) parse4.getQueryParameter("comment")).intValue();
                            if (intValue8 != 0 || intValue9 != 0) {
                                return false;
                            }
                            this.showScrollToMessageError = true;
                            if (this.chatMode == 2) {
                                this.chatActivityDelegate.openReplyMessage(parseInt2);
                                finishFragment();
                            } else {
                                scrollToMessageId(parseInt2, i, true, 0, false, 0);
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected void setInMenuMode(boolean z) {
        super.setInMenuMode(z);
        ActionBar actionBar = this.actionBar;
        if (actionBar != null) {
            actionBar.createMenu().setVisibility(this.inMenuMode ? 8 : 0);
        }
    }

    public void setPreloadedSticker(TLRPC$Document tLRPC$Document, boolean z) {
        this.preloadedGreetingsSticker = tLRPC$Document;
        this.forceHistoryEmpty = z;
    }

    /* loaded from: classes3.dex */
    public class ChatScrollCallback extends RecyclerAnimationScrollHelper.AnimationCallback {
        private boolean lastBottom;
        private int lastItemOffset;
        private int lastPadding;
        private MessageObject scrollTo;

        public ChatScrollCallback() {
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimationCallback
        public void onStartAnimation() {
            super.onStartAnimation();
            ChatActivity chatActivity = ChatActivity.this;
            chatActivity.scrollCallbackAnimationIndex = chatActivity.getNotificationCenter().setAnimationInProgress(ChatActivity.this.scrollCallbackAnimationIndex, ChatActivity.allowedNotificationsDuringChatListAnimations);
            if (ChatActivity.this.pinchToZoomHelper.isInOverlayMode()) {
                ChatActivity.this.pinchToZoomHelper.finishZoom();
            }
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimationCallback
        public void onEndAnimation() {
            if (this.scrollTo != null) {
                ChatActivity.this.chatAdapter.updateRowsSafe();
                int indexOf = ChatActivity.this.chatAdapter.messagesStartRow + ChatActivity.this.messages.indexOf(this.scrollTo);
                if (indexOf >= 0) {
                    ChatActivity.this.chatLayoutManager.scrollToPositionWithOffset(indexOf, (int) (((float) (this.lastItemOffset + this.lastPadding)) - ChatActivity.this.chatListViewPaddingTop), this.lastBottom);
                }
            } else {
                ChatActivity.this.chatAdapter.updateRowsSafe();
                ChatActivity.this.chatLayoutManager.scrollToPositionWithOffset(0, 0, true);
            }
            this.scrollTo = null;
            ChatActivity.this.checkTextureViewPosition = true;
            ChatActivity.this.updateVisibleRows();
            AndroidUtilities.runOnUIThread(new ChatActivity$ChatScrollCallback$$ExternalSyntheticLambda0(this));
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$onEndAnimation$0() {
            ChatActivity.this.getNotificationCenter().onAnimationFinish(ChatActivity.this.scrollCallbackAnimationIndex);
        }

        @Override // org.telegram.ui.Components.RecyclerAnimationScrollHelper.AnimationCallback
        public void recycleView(View view) {
            if (view instanceof ChatMessageCell) {
                ChatActivity.this.chatMessageCellsCache.add((ChatMessageCell) view);
            }
        }
    }

    public static boolean isClickableLink(String str) {
        return str.startsWith("https://") || str.startsWith("@") || str.startsWith("#") || str.startsWith("$") || str.startsWith("video?");
    }

    public SimpleTextView getReplyNameTextView() {
        return this.replyNameTextView;
    }

    public SimpleTextView getReplyObjectTextView() {
        return this.replyObjectTextView;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public ArrayList<ThemeDescription> getThemeDescriptions() {
        if (this.isPauseOnThemePreview) {
            this.isPauseOnThemePreview = false;
            return null;
        }
        ChatActivity$$ExternalSyntheticLambda234 chatActivity$$ExternalSyntheticLambda234 = new ThemeDescription.ThemeDescriptionDelegate() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda234
            @Override // org.telegram.ui.ActionBar.ThemeDescription.ThemeDescriptionDelegate
            public final void didSetColor() {
                ChatActivity.this.lambda$getThemeDescriptions$246();
            }

            @Override // org.telegram.ui.ActionBar.ThemeDescription.ThemeDescriptionDelegate
            public /* synthetic */ void onAnimationProgress(float f) {
                ThemeDescription.ThemeDescriptionDelegate.CC.$default$onAnimationProgress(this, f);
            }
        };
        ArrayList<ThemeDescription> arrayList = new ArrayList<>();
        arrayList.add(new ThemeDescription(this.fragmentView, 0, null, null, null, null, "chat_wallpaper"));
        arrayList.add(new ThemeDescription(this.fragmentView, 0, null, null, null, null, "chat_wallpaper_gradient_to"));
        arrayList.add(new ThemeDescription(this.fragmentView, 0, null, null, null, null, "key_chat_wallpaper_gradient_to2"));
        arrayList.add(new ThemeDescription(this.fragmentView, 0, null, null, null, null, "key_chat_wallpaper_gradient_to3"));
        arrayList.add(new ThemeDescription(this.messagesSearchListView, ThemeDescription.FLAG_BACKGROUND, null, null, null, null, "windowBackgroundWhite"));
        if (this.reportType < 0) {
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_BACKGROUND, null, null, null, null, "actionBarDefault"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_ITEMSCOLOR, null, null, null, null, "actionBarDefaultIcon"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SELECTORCOLOR, null, null, null, null, "actionBarDefaultSelector"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_TITLECOLOR, null, null, null, null, "actionBarDefaultTitle"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SUBTITLECOLOR, null, null, null, null, "actionBarDefaultSubtitle"));
        } else {
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_BACKGROUND, null, null, null, null, "actionBarActionModeDefault"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_ITEMSCOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SELECTORCOLOR, null, null, null, null, "actionBarActionModeDefaultSelector"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_TITLECOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
            arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SUBTITLECOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
        }
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SUBMENUBACKGROUND, null, null, null, chatActivity$$ExternalSyntheticLambda234, "actionBarDefaultSubmenuBackground"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SUBMENUITEM, null, null, null, chatActivity$$ExternalSyntheticLambda234, "actionBarDefaultSubmenuItem"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_IMAGECOLOR | ThemeDescription.FLAG_AB_SUBMENUITEM, null, null, null, chatActivity$$ExternalSyntheticLambda234, "actionBarDefaultSubmenuItemIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_LISTGLOWCOLOR, null, null, null, null, "actionBarDefault"));
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer != null ? chatAvatarContainer.getTitleTextView() : null, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "actionBarDefaultTitle"));
        ChatAvatarContainer chatAvatarContainer2 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer2 != null ? chatAvatarContainer2.getTitleTextView() : null, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "actionBarDefaultSubtitle"));
        ChatAvatarContainer chatAvatarContainer3 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer3 != null ? chatAvatarContainer3.getSubtitleTextView() : null, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, (Class[]) null, new Paint[]{Theme.chat_statusPaint, Theme.chat_statusRecordPaint}, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_status", (Object) null));
        ChatAvatarContainer chatAvatarContainer4 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer4 != null ? chatAvatarContainer4.getSubtitleTextView() : null, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, (Class[]) null, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "actionBarDefaultSubtitle", (Object) null));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SELECTORCOLOR, null, null, null, null, "actionBarDefaultSelector"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SEARCH, null, null, null, null, "actionBarDefaultSearch"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SEARCHPLACEHOLDER, null, null, null, null, "actionBarDefaultSearchPlaceholder"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_SEARCHPLACEHOLDER, null, null, null, null, "actionBarDefaultSearchPlaceholder"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_AM_ITEMSCOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_AM_BACKGROUND, null, null, null, null, "actionBarActionModeDefault"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_AM_TOPBACKGROUND, null, null, null, null, "actionBarActionModeDefaultTop"));
        arrayList.add(new ThemeDescription(this.actionBar, ThemeDescription.FLAG_AB_AM_SELECTORCOLOR, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.selectedMessagesCountTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
        ChatAvatarContainer chatAvatarContainer5 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer5 != null ? chatAvatarContainer5.getTitleTextView() : null, 0, null, null, new Drawable[]{Theme.chat_muteIconDrawable}, null, "chat_muteIcon"));
        ChatAvatarContainer chatAvatarContainer6 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer6 != null ? chatAvatarContainer6.getTitleTextView() : null, 0, null, null, new Drawable[]{Theme.chat_lockIconDrawable}, null, "chat_lockIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, Theme.avatarDrawables, null, "avatar_text"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundRed"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundOrange"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundViolet"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundGreen"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundCyan"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundBlue"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_backgroundPink"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessageRed"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessageOrange"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessageViolet"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessageGreen"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessageCyan"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessageBlue"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "avatar_nameInMessagePink"));
        Theme.MessageDrawable messageDrawable = (Theme.MessageDrawable) getThemedDrawable("drawableMsgIn");
        Theme.MessageDrawable messageDrawable2 = (Theme.MessageDrawable) getThemedDrawable("drawableMsgInMedia");
        Theme.MessageDrawable messageDrawable3 = (Theme.MessageDrawable) getThemedDrawable("drawableMsgOut");
        Theme.MessageDrawable messageDrawable4 = (Theme.MessageDrawable) getThemedDrawable("drawableMsgOutMedia");
        Theme.MessageDrawable messageDrawable5 = (Theme.MessageDrawable) getThemedDrawable("drawableMsgOutSelected");
        Theme.MessageDrawable messageDrawable6 = (Theme.MessageDrawable) getThemedDrawable("drawableMsgOutMediaSelected");
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class, BotHelpCell.class}, null, new Drawable[]{messageDrawable, messageDrawable2}, null, "chat_inBubble"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{(Theme.MessageDrawable) getThemedDrawable("drawableMsgInSelected"), (Theme.MessageDrawable) getThemedDrawable("drawableMsgInMediaSelected")}, null, "chat_inBubbleSelected"));
        if (messageDrawable != null) {
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, messageDrawable.getShadowDrawables(), null, "chat_inBubbleShadow"));
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, messageDrawable2.getShadowDrawables(), null, "chat_inBubbleShadow"));
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, messageDrawable3.getShadowDrawables(), null, "chat_outBubbleShadow"));
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, messageDrawable4.getShadowDrawables(), null, "chat_outBubbleShadow"));
        }
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{messageDrawable3, messageDrawable4}, null, "chat_outBubble"));
        if (!this.themeDelegate.isThemeChangeAvailable()) {
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{messageDrawable3, messageDrawable4}, null, "chat_outBubbleGradient"));
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{messageDrawable3, messageDrawable4}, null, "chat_outBubbleGradient2"));
            arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{messageDrawable3, messageDrawable4}, null, "chat_outBubbleGradient3"));
        }
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{messageDrawable5, messageDrawable6}, null, "chat_outBubbleSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{messageDrawable5, messageDrawable6}, null, "chat_outBubbleGradientSelectedOverlay"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{ChatActionCell.class}, getThemedPaint("paintChatActionText"), null, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_LINKCOLOR, new Class[]{ChatActionCell.class}, getThemedPaint("paintChatActionText"), null, null, "chat_serviceLink"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_botCardDrawable, getThemedDrawable("drawableShareIcon"), getThemedDrawable("drawableReplyIcon"), getThemedDrawable("drawableBotInline"), getThemedDrawable("drawableBotLink"), getThemedDrawable("drawable_botInvite"), getThemedDrawable("drawableGoIcon"), getThemedDrawable("drawableCommentSticker")}, null, "chat_serviceIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class, ChatActionCell.class}, null, null, null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class, ChatActionCell.class}, null, null, null, "chat_serviceBackgroundSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class, BotHelpCell.class}, null, null, null, "chat_messageTextIn"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_messageTextOut"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_LINKCOLOR, new Class[]{ChatMessageCell.class, BotHelpCell.class}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messageLinkIn", (Object) null));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_LINKCOLOR, new Class[]{ChatMessageCell.class}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messageLinkOut", (Object) null));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgNoSoundDrawable}, null, "chat_mediaTimeText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutCheck")}, null, "chat_outSentCheck"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutCheckSelected")}, null, "chat_outSentCheckSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutCheckRead"), getThemedDrawable("drawableMsgOutHalfCheck")}, null, "chat_outSentCheckRead"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutCheckReadSelected"), getThemedDrawable("drawableMsgOutHalfCheckSelected")}, null, "chat_outSentCheckReadSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outSentClock"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outSentClockSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inSentClock"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inSentClockSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgMediaCheckDrawable, Theme.chat_msgMediaHalfCheckDrawable}, null, "chat_mediaSentCheck"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgStickerHalfCheck"), getThemedDrawable("drawableMsgStickerCheck"), getThemedDrawable("drawableMsgStickerClock"), getThemedDrawable("drawableMsgStickerViews"), getThemedDrawable("drawableMsgStickerReplies"), getThemedDrawable("drawableMsgStickerPinned")}, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_mediaSentClock"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutViews"), getThemedDrawable("drawableMsgOutReplies"), getThemedDrawable("drawableMsgOutPinned")}, null, "chat_outViews"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutViewsSelected"), getThemedDrawable("drawableMsgOutReplies"), getThemedDrawable("drawableMsgOutPinnedSelected")}, null, "chat_outViewsSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgInViewsDrawable, Theme.chat_msgInRepliesDrawable, Theme.chat_msgInPinnedDrawable}, null, "chat_inViews"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgInViewsSelectedDrawable, Theme.chat_msgInRepliesSelectedDrawable, Theme.chat_msgInPinnedSelectedDrawable}, null, "chat_inViewsSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgMediaViewsDrawable, Theme.chat_msgMediaRepliesDrawable, Theme.chat_msgMediaPinnedDrawable}, null, "chat_mediaViews"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutMenu")}, null, "chat_outMenu"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutMenuSelected")}, null, "chat_outMenuSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgInMenuDrawable}, null, "chat_inMenu"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgInMenuSelectedDrawable}, null, "chat_inMenuSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgMediaMenuDrawable}, null, "chat_mediaMenu"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutInstant")}, null, "chat_outInstant"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgInInstantDrawable, Theme.chat_commentDrawable, Theme.chat_commentArrowDrawable}, null, "chat_inInstant"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutCallAudio"), getThemedDrawable("drawableMsgOutCallVideo")}, null, "chat_outInstant"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{getThemedDrawable("drawableMsgOutCallAudioSelected"), getThemedDrawable("drawableMsgOutCallVideo")}, null, "chat_outInstant"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, Theme.chat_msgInCallDrawable, null, "chat_inInstant"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, Theme.chat_msgInCallSelectedDrawable, null, "chat_inInstantSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgCallUpGreenDrawable}, null, "chat_outUpCall"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgCallDownRedDrawable}, null, "chat_inUpCall"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgCallDownGreenDrawable}, null, "chat_inDownCall"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_msgErrorPaint, null, null, "chat_sentError"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_msgErrorDrawable}, null, "chat_sentErrorIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_selectedBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_durationPaint, null, null, "chat_previewDurationText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_gamePaint, null, null, "chat_previewGameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inPreviewInstantText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outPreviewInstantText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inPreviewInstantSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outPreviewInstantSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_deleteProgressPaint, null, null, "chat_secretTimeText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_stickerNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, getThemedPaint("paintChatBotButton"), null, null, "chat_botButtonText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_botProgressPaint, null, null, "chat_botProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, getThemedPaint("paintChatTimeBackground"), null, null, "chat_mediaTimeBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inForwardedNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outForwardedNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inPsaNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outPsaNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inViaBotNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outViaBotNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_stickerViaBotNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inReplyLine"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outReplyLine"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_stickerReplyLine"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inReplyNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outReplyNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_stickerReplyNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inReplyMessageText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outReplyMessageText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inReplyMediaMessageText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outReplyMediaMessageText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inReplyMediaMessageSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outReplyMediaMessageSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_stickerReplyMessageText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inPreviewLine"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outPreviewLine"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inSiteNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outSiteNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inContactNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outContactNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inContactPhoneText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inContactPhoneSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outContactPhoneText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outContactPhoneSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_mediaProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioSelectedProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioSelectedProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_mediaTimeText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inTimeText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outTimeText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inTimeSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_adminText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_adminSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAdminText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAdminSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outTimeSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioPerfomerText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioPerfomerSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioPerfomerText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioPerfomerSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioTitleText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioTitleText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioDurationText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioDurationText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioDurationSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioDurationSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioSeekbar"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioSeekbar"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioSeekbarSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioSeekbarSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioSeekbarFill"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inAudioCacheSeekbar"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioSeekbarFill"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outAudioCacheSeekbar"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inVoiceSeekbar"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outVoiceSeekbar"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inVoiceSeekbarSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outVoiceSeekbarSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inVoiceSeekbarFill"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outVoiceSeekbarFill"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileProgress"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileProgressSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileProgressSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileNameText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileInfoText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileInfoText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileInfoSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileInfoSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inFileBackgroundSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outFileBackgroundSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inVenueInfoText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outVenueInfoText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inVenueInfoSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outVenueInfoSelectedText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_mediaInfoText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_urlPaint, null, null, "chat_linkSelectBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_outUrlPaint, null, null, "chat_outLinkSelectBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, Theme.chat_textSearchSelectionPaint, null, null, "chat_textSelectBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outLoader"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outMediaIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outLoaderSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outMediaIconSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inLoader"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inMediaIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inLoaderSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inMediaIconSelected"));
        Drawable[][] drawableArr = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr[0][0], drawableArr[1][0], drawableArr[2][0], drawableArr[3][0]}, null, "chat_mediaLoaderPhoto"));
        Drawable[][] drawableArr2 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr2[0][0], drawableArr2[1][0], drawableArr2[2][0], drawableArr2[3][0]}, null, "chat_mediaLoaderPhotoIcon"));
        Drawable[][] drawableArr3 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr3[0][1], drawableArr3[1][1], drawableArr3[2][1], drawableArr3[3][1]}, null, "chat_mediaLoaderPhotoSelected"));
        Drawable[][] drawableArr4 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr4[0][1], drawableArr4[1][1], drawableArr4[2][1], drawableArr4[3][1]}, null, "chat_mediaLoaderPhotoIconSelected"));
        Drawable[][] drawableArr5 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr5[7][0], drawableArr5[8][0]}, null, "chat_outLoaderPhoto"));
        Drawable[][] drawableArr6 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr6[7][0], drawableArr6[8][0]}, null, "chat_outLoaderPhotoIcon"));
        Drawable[][] drawableArr7 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr7[7][1], drawableArr7[8][1]}, null, "chat_outLoaderPhotoSelected"));
        Drawable[][] drawableArr8 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr8[7][1], drawableArr8[8][1]}, null, "chat_outLoaderPhotoIconSelected"));
        Drawable[][] drawableArr9 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr9[10][0], drawableArr9[11][0]}, null, "chat_inLoaderPhoto"));
        Drawable[][] drawableArr10 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr10[10][0], drawableArr10[11][0]}, null, "chat_inLoaderPhotoIcon"));
        Drawable[][] drawableArr11 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr11[10][1], drawableArr11[11][1]}, null, "chat_inLoaderPhotoSelected"));
        Drawable[][] drawableArr12 = Theme.chat_photoStatesDrawables;
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{drawableArr12[10][1], drawableArr12[11][1]}, null, "chat_inLoaderPhotoIconSelected"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_photoStatesDrawables[12][0]}, null, "chat_inFileIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_photoStatesDrawables[12][1]}, null, "chat_inFileSelectedIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_contactDrawable[0]}, null, "chat_inContactBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_contactDrawable[0]}, null, "chat_inContactIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_contactDrawable[1]}, null, "chat_outContactBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_contactDrawable[1]}, null, "chat_outContactIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inLocationBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_locationDrawable[0]}, null, "chat_inLocationIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outLocationBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_locationDrawable[1]}, null, "chat_outLocationIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inPollCorrectAnswer"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outPollCorrectAnswer"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_inPollWrongAnswer"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, null, null, "chat_outPollWrongAnswer"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_pollHintDrawable[0]}, null, "chat_inPreviewInstantText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_pollHintDrawable[1]}, null, "chat_outPreviewInstantText"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_psaHelpDrawable[0]}, null, "chat_inViews"));
        arrayList.add(new ThemeDescription(this.chatListView, 0, new Class[]{ChatMessageCell.class}, null, new Drawable[]{Theme.chat_psaHelpDrawable[1]}, null, "chat_outViews"));
        if (!this.themeDelegate.isThemeChangeAvailable()) {
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, null, Theme.avatarDrawables, null, "avatar_text"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, Theme.dialogs_countPaint, null, null, "chats_unreadCounter"));
            TextPaint[] textPaintArr = Theme.dialogs_namePaint;
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, (String[]) null, new Paint[]{textPaintArr[0], textPaintArr[1], Theme.dialogs_searchNamePaint}, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chats_name"));
            TextPaint[] textPaintArr2 = Theme.dialogs_nameEncryptedPaint;
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, (String[]) null, new Paint[]{textPaintArr2[0], textPaintArr2[1], Theme.dialogs_searchNameEncryptedPaint}, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chats_secretName"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, null, new Drawable[]{Theme.dialogs_lockDrawable}, null, "chats_secretIcon"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, null, new Drawable[]{Theme.dialogs_scamDrawable, Theme.dialogs_fakeDrawable}, null, "chats_draft"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, Theme.dialogs_messagePaint[1], null, null, "chats_message_threeLines"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, Theme.dialogs_messageNamePaint, null, null, "chats_nameMessage_threeLines"));
            arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chats_nameMessage"));
            arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chats_attachMessage"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, (String[]) null, Theme.dialogs_messagePrintingPaint, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chats_actionMessage"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, Theme.dialogs_timePaint, null, null, "chats_date"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, null, new Drawable[]{Theme.dialogs_checkDrawable}, null, "chats_sentCheck"));
            arrayList.add(new ThemeDescription(this.messagesSearchListView, 0, new Class[]{DialogCell.class}, null, new Drawable[]{Theme.dialogs_checkReadDrawable, Theme.dialogs_halfCheckDrawable}, null, "chats_sentReadCheck"));
        }
        arrayList.add(new ThemeDescription(this.mentionContainer, 0, null, getThemedPaint("paintChatComposeBackground"), null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.mentionContainer, 0, null, null, new Drawable[]{Theme.chat_composeShadowDrawable}, null, "chat_messagePanelShadow"));
        arrayList.add(new ThemeDescription(this.mentionContainer, 0, null, null, new Drawable[]{Theme.chat_composeShadowRoundDrawable}, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.searchContainer, 0, null, getThemedPaint("paintChatComposeBackground"), null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.searchContainer, 0, null, null, new Drawable[]{Theme.chat_composeShadowDrawable}, null, "chat_messagePanelShadow"));
        arrayList.add(new ThemeDescription(this.bottomOverlay, 0, null, getThemedPaint("paintChatComposeBackground"), null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.bottomOverlay, 0, null, null, new Drawable[]{Theme.chat_composeShadowDrawable}, null, "chat_messagePanelShadow"));
        arrayList.add(new ThemeDescription(this.bottomOverlayChat, 0, null, getThemedPaint("paintChatComposeBackground"), null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.bottomOverlayChat, 0, null, null, new Drawable[]{Theme.chat_composeShadowDrawable}, null, "chat_messagePanelShadow"));
        arrayList.add(new ThemeDescription(this.bottomMessagesActionContainer, 0, null, getThemedPaint("paintChatComposeBackground"), null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.bottomMessagesActionContainer, 0, null, null, new Drawable[]{Theme.chat_composeShadowDrawable}, null, "chat_messagePanelShadow"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, null, getThemedPaint("paintChatComposeBackground"), null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, null, null, new Drawable[]{Theme.chat_composeShadowDrawable}, null, "chat_messagePanelShadow"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"messageEditText"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelText"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_CURSORCOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"messageEditText"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelCursor"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_HINTTEXTCOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"messageEditText"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelHint"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"sendButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelSend"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"sendButton"}, null, null, 24, null, "chat_messagePanelSend"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"botButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"botButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "listSelectorSDK21"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"notifyButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_IMAGECOLOR | ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatActivityEnterView.class}, new String[]{"scheduledButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"scheduledButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_recordedVoiceDot"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"scheduledButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "listSelectorSDK21"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"attachButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"attachButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "listSelectorSDK21"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"audioSendButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"videoSendButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"notifyButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelVideoFrame"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"notifyButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "listSelectorSDK21"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"videoTimelineView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelSend"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"micDrawable"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelVoicePressed"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"cameraDrawable"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelVoicePressed"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"sendDrawable"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelVoicePressed"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, null, null, null, null, "key_chat_messagePanelVoiceLock"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, null, null, null, "key_chat_messagePanelVoiceLockBackground"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"lockShadowDrawable"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "key_chat_messagePanelVoiceLockShadow"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"recordDeleteImageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "listSelectorSDK21"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{ChatActivityEnterView.class}, new String[]{"recordedAudioBackground"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_recordedVoiceBackground"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, null, null, null, null, "chat_recordTime"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, null, null, null, null, "chat_recordVoiceCancel"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"recordedAudioTimeTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelVoiceDuration"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, null, null, null, null, "chat_recordVoiceCancel"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"doneButtonProgress"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "contextProgressInner1"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"doneButtonProgress"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "contextProgressOuter1"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{ChatActivityEnterView.class}, new String[]{"cancelBotButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelCancelInlineBot"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, new Class[]{ChatActivityEnterView.class}, new String[]{"cancelBotButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "listSelectorSDK21"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"redDotPaint"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_recordedVoiceDot"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"paint"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelVoiceBackground"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"seekBarWaveform"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_recordedVoiceProgress"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"seekBarWaveform"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_recordedVoiceProgressInner"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, new String[]{"dotPaint"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_emojiPanelNewTrending"));
        arrayList.add(new ThemeDescription(this.chatActivityEnterView, 0, new Class[]{ChatActivityEnterView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_recordedVoicePlayPause"));
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView != null ? chatActivityEnterView.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelBackground"));
        ChatActivityEnterView chatActivityEnterView2 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView2 != null ? chatActivityEnterView2.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelShadowLine"));
        ChatActivityEnterView chatActivityEnterView3 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView3 != null ? chatActivityEnterView3.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelEmptyText"));
        ChatActivityEnterView chatActivityEnterView4 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView4 != null ? chatActivityEnterView4.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelIcon"));
        ChatActivityEnterView chatActivityEnterView5 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView5 != null ? chatActivityEnterView5.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelIconSelected"));
        ChatActivityEnterView chatActivityEnterView6 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView6 != null ? chatActivityEnterView6.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelStickerPackSelector"));
        ChatActivityEnterView chatActivityEnterView7 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView7 != null ? chatActivityEnterView7.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelBackspace"));
        ChatActivityEnterView chatActivityEnterView8 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView8 != null ? chatActivityEnterView8.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelTrendingTitle"));
        ChatActivityEnterView chatActivityEnterView9 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView9 != null ? chatActivityEnterView9.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelTrendingDescription"));
        ChatActivityEnterView chatActivityEnterView10 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView10 != null ? chatActivityEnterView10.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelBadgeText"));
        ChatActivityEnterView chatActivityEnterView11 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView11 != null ? chatActivityEnterView11.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelBadgeBackground"));
        ChatActivityEnterView chatActivityEnterView12 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView12 != null ? chatActivityEnterView12.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiBottomPanelIcon"));
        ChatActivityEnterView chatActivityEnterView13 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView13 != null ? chatActivityEnterView13.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiSearchIcon"));
        ChatActivityEnterView chatActivityEnterView14 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView14 != null ? chatActivityEnterView14.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelStickerSetNameHighlight"));
        ChatActivityEnterView chatActivityEnterView15 = this.chatActivityEnterView;
        arrayList.add(new ThemeDescription(chatActivityEnterView15 != null ? chatActivityEnterView15.getEmojiView() : null, 0, new Class[]{EmojiView.class}, (String[]) null, (Paint[]) null, (Drawable[]) null, chatActivity$$ExternalSyntheticLambda234, "chat_emojiPanelStickerPackSelectorLine"));
        ChatActivityEnterView chatActivityEnterView16 = this.chatActivityEnterView;
        if (chatActivityEnterView16 != null) {
            TrendingStickersAlert trendingStickersAlert = chatActivityEnterView16.getTrendingStickersAlert();
            if (trendingStickersAlert != null) {
                arrayList.addAll(trendingStickersAlert.getThemeDescriptions());
            }
            arrayList.add(new ThemeDescription((View) null, 0, (Class[]) null, (String[]) null, (Paint[]) null, new Drawable[]{this.chatActivityEnterView.getStickersArrowDrawable()}, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_messagePanelIcons"));
        }
        int i = 0;
        while (i < 2) {
            UndoView undoView = i == 0 ? this.undoView : this.topUndoView;
            arrayList.add(new ThemeDescription(undoView, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "undo_background"));
            arrayList.add(new ThemeDescription(undoView, 0, new Class[]{UndoView.class}, new String[]{"undoImageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_cancelColor"));
            arrayList.add(new ThemeDescription(undoView, 0, new Class[]{UndoView.class}, new String[]{"undoTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_cancelColor"));
            arrayList.add(new ThemeDescription(undoView, 0, new Class[]{UndoView.class}, new String[]{"infoTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_infoColor"));
            arrayList.add(new ThemeDescription(undoView, 0, new Class[]{UndoView.class}, new String[]{"subinfoTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_infoColor"));
            arrayList.add(new ThemeDescription(undoView, ThemeDescription.FLAG_LINKCOLOR, new Class[]{UndoView.class}, new String[]{"subinfoTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_cancelColor"));
            arrayList.add(new ThemeDescription(undoView, 0, new Class[]{UndoView.class}, new String[]{"textPaint"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_infoColor"));
            arrayList.add(new ThemeDescription(undoView, 0, new Class[]{UndoView.class}, new String[]{"progressPaint"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_infoColor"));
            arrayList.add(new ThemeDescription(undoView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{UndoView.class}, new String[]{"leftImageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "undo_infoColor"));
            i++;
        }
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_botKeyboardButtonText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_botKeyboardButtonBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_botKeyboardButtonBackgroundPressed"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_BACKGROUND | ThemeDescription.FLAG_CHECKTAG, new Class[]{FragmentContextView.class}, new String[]{"frameLayout"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "inappPlayerBackground"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{FragmentContextView.class}, new String[]{"playButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "inappPlayerPlayPause"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, new Class[]{FragmentContextView.class}, new String[]{"titleTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "inappPlayerTitle"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, new Class[]{FragmentContextView.class}, new String[]{"titleTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "inappPlayerPerformer"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_FASTSCROLL, new Class[]{FragmentContextView.class}, new String[]{"subtitleTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "inappPlayerClose"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{FragmentContextView.class}, new String[]{"closeButton"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "inappPlayerClose"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_BACKGROUND | ThemeDescription.FLAG_CHECKTAG, new Class[]{FragmentContextView.class}, new String[]{"frameLayout"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "returnToCallBackground"));
        arrayList.add(new ThemeDescription(this.fragmentView, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, new Class[]{FragmentContextView.class}, new String[]{"titleTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "returnToCallText"));
        arrayList.add(new ThemeDescription(this.pinnedLineView, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_topPanelLine"));
        arrayList.add(new ThemeDescription(this.pinnedLineView, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "windowBackgroundWhite"));
        arrayList.add(new ThemeDescription(this.pinnedCounterTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_topPanelTitle"));
        for (int i2 = 0; i2 < 2; i2++) {
            arrayList.add(new ThemeDescription(this.pinnedNameTextView[i2], ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_topPanelTitle"));
            arrayList.add(new ThemeDescription(this.pinnedMessageTextView[i2], ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_topPanelMessage"));
        }
        arrayList.add(new ThemeDescription(this.alertNameTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_topPanelTitle"));
        arrayList.add(new ThemeDescription(this.alertTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_topPanelMessage"));
        arrayList.add(new ThemeDescription(this.closePinned, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_topPanelClose"));
        arrayList.add(new ThemeDescription(this.pinnedListButton, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_topPanelClose"));
        arrayList.add(new ThemeDescription(this.closeReportSpam, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_topPanelClose"));
        arrayList.add(new ThemeDescription(this.topChatPanelView, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_topPanelBackground"));
        arrayList.add(new ThemeDescription(this.alertView, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_topPanelBackground"));
        arrayList.add(new ThemeDescription(this.pinnedMessageView, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_topPanelBackground"));
        arrayList.add(new ThemeDescription(this.addToContactsButton, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_addContact"));
        arrayList.add(new ThemeDescription(this.reportSpamButton, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, null, null, null, null, "chat_reportSpam"));
        arrayList.add(new ThemeDescription(this.reportSpamButton, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_CHECKTAG, null, null, null, null, "chat_addContact"));
        arrayList.add(new ThemeDescription(this.replyLineView, ThemeDescription.FLAG_BACKGROUND, null, null, null, null, "chat_replyPanelLine"));
        arrayList.add(new ThemeDescription(this.replyNameTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_replyPanelName"));
        arrayList.add(new ThemeDescription(this.replyObjectTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "windowBackgroundWhiteGrayText"));
        arrayList.add(new ThemeDescription(this.replyObjectHintTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "windowBackgroundWhiteGrayText"));
        arrayList.add(new ThemeDescription(this.replyIconImageView, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_replyPanelIcons"));
        arrayList.add(new ThemeDescription(this.replyCloseImageView, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_replyPanelClose"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_replyPanelName"));
        arrayList.add(new ThemeDescription(this.searchUpButton, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_searchPanelIcons"));
        arrayList.add(new ThemeDescription(this.searchUpButton, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.searchDownButton, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_searchPanelIcons"));
        arrayList.add(new ThemeDescription(this.searchDownButton, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.searchCalendarButton, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_searchPanelIcons"));
        arrayList.add(new ThemeDescription(this.searchCalendarButton, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.searchUserButton, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_searchPanelIcons"));
        arrayList.add(new ThemeDescription(this.searchUserButton, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.searchCountText, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_searchPanelText"));
        arrayList.add(new ThemeDescription(this.searchAsListTogglerView, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.replyButton, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
        arrayList.add(new ThemeDescription(this.replyButton, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.forwardButton, ThemeDescription.FLAG_TEXTCOLOR | ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "actionBarActionModeDefaultIcon"));
        arrayList.add(new ThemeDescription(this.forwardButton, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "actionBarActionModeDefaultSelector"));
        arrayList.add(new ThemeDescription(this.bottomOverlayText, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_secretChatStatusText"));
        arrayList.add(new ThemeDescription(this.bottomOverlayChatText, 0, null, null, null, null, "chat_fieldOverlayText"));
        arrayList.add(new ThemeDescription(this.bottomOverlayChatText, 0, null, null, null, null, "chat_goDownButtonCounterBackground"));
        arrayList.add(new ThemeDescription(this.bottomOverlayChatText, 0, null, null, null, null, "chat_messagePanelBackground"));
        arrayList.add(new ThemeDescription(this.bottomOverlayProgress, 0, null, null, null, null, "chat_fieldOverlayText"));
        arrayList.add(new ThemeDescription(this.bottomOverlayImage, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_fieldOverlayText"));
        arrayList.add(new ThemeDescription(this.bigEmptyView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.emptyView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.progressBar, ThemeDescription.FLAG_PROGRESSBAR, null, null, null, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.stickersPanelArrow, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_stickersHintPanel"));
        arrayList.add(new ThemeDescription(this.stickersListView, ThemeDescription.FLAG_BACKGROUNDFILTER, new Class[]{StickerCell.class}, null, null, null, "chat_stickersHintPanel"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_USEBACKGROUNDDRAWABLE, new Class[]{ChatUnreadCell.class}, new String[]{"backgroundLayout"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_unreadMessagesStartBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{ChatUnreadCell.class}, new String[]{"imageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_unreadMessagesStartArrowIcon"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{ChatUnreadCell.class}, new String[]{"textView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_unreadMessagesStartText"));
        arrayList.add(new ThemeDescription(this.progressView2, ThemeDescription.FLAG_SERVICEBACKGROUND, null, null, null, null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(this.emptyView, ThemeDescription.FLAG_SERVICEBACKGROUND, null, null, null, null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(this.bigEmptyView, ThemeDescription.FLAG_SERVICEBACKGROUND, null, null, null, null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_SERVICEBACKGROUND, new Class[]{ChatLoadingCell.class}, new String[]{"textView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(this.chatListView, ThemeDescription.FLAG_PROGRESSBAR, new Class[]{ChatLoadingCell.class}, new String[]{"textView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), ThemeDescription.FLAG_TEXTCOLOR, new Class[]{BotSwitchCell.class}, new String[]{"textView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_botSwitchToInlineText"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), ThemeDescription.FLAG_TEXTCOLOR, new Class[]{MentionCell.class}, new String[]{"nameTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "windowBackgroundWhiteBlackText"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), ThemeDescription.FLAG_TEXTCOLOR, new Class[]{MentionCell.class}, new String[]{"usernameTextView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "windowBackgroundWhiteGrayText3"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, new Drawable[]{Theme.chat_inlineResultFile, Theme.chat_inlineResultAudio, Theme.chat_inlineResultLocation}, null, "chat_inlineResultIcon"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, null, null, "windowBackgroundWhiteGrayText2"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, null, null, "windowBackgroundWhiteLinkText"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, null, null, "windowBackgroundWhiteBlackText"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, null, null, "chat_inAudioProgress"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, null, null, "chat_inAudioSelectedProgress"));
        arrayList.add(new ThemeDescription(this.mentionContainer.getListView(), 0, new Class[]{ContextLinkCell.class}, null, null, null, "divider"));
        arrayList.add(new ThemeDescription(this.gifHintTextView, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_gifSaveHintBackground"));
        arrayList.add(new ThemeDescription(this.gifHintTextView, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_gifSaveHintText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachMediaBanBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachMediaBanText"));
        arrayList.add(new ThemeDescription(this.noSoundHintView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{HintView.class}, new String[]{"textView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_gifSaveHintText"));
        arrayList.add(new ThemeDescription(this.noSoundHintView, ThemeDescription.FLAG_IMAGECOLOR, new Class[]{HintView.class}, new String[]{"imageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_gifSaveHintText"));
        arrayList.add(new ThemeDescription(this.noSoundHintView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{HintView.class}, new String[]{"arrowImageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_gifSaveHintBackground"));
        arrayList.add(new ThemeDescription(this.forwardHintView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{HintView.class}, new String[]{"textView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_gifSaveHintText"));
        arrayList.add(new ThemeDescription(this.forwardHintView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{HintView.class}, new String[]{"arrowImageView"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "chat_gifSaveHintBackground"));
        arrayList.add(new ThemeDescription(this.pagedownButtonCounter, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_goDownButtonCounterBackground"));
        arrayList.add(new ThemeDescription(this.pagedownButtonCounter, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_goDownButtonCounter"));
        arrayList.add(new ThemeDescription(this.pagedownButtonImage, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_goDownButton"));
        arrayList.add(new ThemeDescription(this.pagedownButtonImage, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "chat_goDownButtonShadow"));
        arrayList.add(new ThemeDescription(this.pagedownButtonImage, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_goDownButtonIcon"));
        arrayList.add(new ThemeDescription(this.mentiondownButtonCounter, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_goDownButtonCounterBackground"));
        arrayList.add(new ThemeDescription(this.mentiondownButtonCounter, ThemeDescription.FLAG_TEXTCOLOR, null, null, null, null, "chat_goDownButtonCounter"));
        arrayList.add(new ThemeDescription(this.mentiondownButtonImage, ThemeDescription.FLAG_BACKGROUNDFILTER, null, null, null, null, "chat_goDownButton"));
        arrayList.add(new ThemeDescription(this.mentiondownButtonImage, ThemeDescription.FLAG_BACKGROUNDFILTER | ThemeDescription.FLAG_DRAWABLESELECTEDSTATE, null, null, null, null, "chat_goDownButtonShadow"));
        arrayList.add(new ThemeDescription(this.mentiondownButtonImage, ThemeDescription.FLAG_IMAGECOLOR, null, null, null, null, "chat_goDownButtonIcon"));
        ChatAvatarContainer chatAvatarContainer7 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer7 != null ? chatAvatarContainer7.getTimeItem() : null, 0, null, null, null, null, "chat_secretTimerBackground"));
        ChatAvatarContainer chatAvatarContainer8 = this.avatarContainer;
        arrayList.add(new ThemeDescription(chatAvatarContainer8 != null ? chatAvatarContainer8.getTimeItem() : null, 0, null, null, null, null, "chat_secretTimerText"));
        arrayList.add(new ThemeDescription(this.floatingDateView, 0, null, null, null, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.floatingDateView, 0, null, null, null, null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(this.infoTopView, 0, null, null, null, null, "chat_serviceText"));
        arrayList.add(new ThemeDescription(this.infoTopView, 0, null, null, null, null, "chat_serviceBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachGalleryIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachGalleryBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachGalleryText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachAudioIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachAudioBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachAudioText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachFileIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachFileBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachFileText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachContactIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachContactBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachContactText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachLocationIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachLocationBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachLocationText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachPollIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachPollBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachPollText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, new Drawable[]{Theme.chat_attachEmptyDrawable}, null, "chat_attachEmptyImage"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_attachPhotoBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogBackgroundGray"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogTextGray2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogScrollGlow"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogGrayLine"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogCameraIcon"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "dialogButtonSelector"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "windowBackgroundWhiteLinkSelection"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "windowBackgroundWhiteInputField"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_outTextSelectionHighlight"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_inTextSelectionHighlight"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_TextSelectionCursor"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayGreen1"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayGreen2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayBlue1"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayBlue2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_topPanelGreen1"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_topPanelGreen2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_topPanelBlue1"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_topPanelBlue2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_topPanelGray"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayAlertGradientMuted"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayAlertGradientMuted2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayAlertGradientUnmuted"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayAlertGradientUnmuted2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_mutedByAdminGradient"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_mutedByAdminGradient2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_mutedByAdminGradient3"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "voipgroup_overlayAlertMutedByAdmin"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "kvoipgroup_overlayAlertMutedByAdmin2"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "windowBackgroundGray"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_outReactionButtonBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_inReactionButtonBackground"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_inReactionButtonText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_outReactionButtonText"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_inReactionButtonTextSelected"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, null, "chat_inReactionButtonTextSelected"));
        arrayList.add(new ThemeDescription(null, 0, null, null, null, chatActivity$$ExternalSyntheticLambda234, "chat_BlurAlpha"));
        if (this.chatActivityEnterView != null) {
            arrayList.add(new ThemeDescription(this.chatActivityEnterView.botCommandsMenuContainer.listView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{BotCommandsMenuView.BotCommandView.class}, new String[]{"description"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "windowBackgroundWhiteBlackText"));
            arrayList.add(new ThemeDescription(this.chatActivityEnterView.botCommandsMenuContainer.listView, ThemeDescription.FLAG_TEXTCOLOR, new Class[]{BotCommandsMenuView.BotCommandView.class}, new String[]{"command"}, (Paint[]) null, (Drawable[]) null, (ThemeDescription.ThemeDescriptionDelegate) null, "windowBackgroundWhiteGrayText"));
        }
        ChatActivityMemberRequestsDelegate chatActivityMemberRequestsDelegate = this.pendingRequestsDelegate;
        if (chatActivityMemberRequestsDelegate != null) {
            chatActivityMemberRequestsDelegate.fillThemeDescriptions(arrayList);
        }
        Iterator<ThemeDescription> it = arrayList.iterator();
        while (it.hasNext()) {
            it.next().resourcesProvider = this.themeDelegate;
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$getThemeDescriptions$246() {
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.updateColors();
        }
        Theme.refreshAttachButtonsColors();
        ChatAttachAlert chatAttachAlert = this.chatAttachAlert;
        if (chatAttachAlert != null) {
            chatAttachAlert.checkColors();
        }
        RecyclerListView recyclerListView = this.chatListView;
        int i = 0;
        if (recyclerListView != null) {
            int childCount = recyclerListView.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = this.chatListView.getChildAt(i2);
                if (childAt instanceof ChatMessageCell) {
                    ((ChatMessageCell) childAt).createSelectorDrawable(0);
                } else if (childAt instanceof ChatActionCell) {
                    ((ChatActionCell) childAt).setInvalidateColors(true);
                }
            }
        }
        RecyclerListView recyclerListView2 = this.messagesSearchListView;
        if (recyclerListView2 != null) {
            int childCount2 = recyclerListView2.getChildCount();
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt2 = this.messagesSearchListView.getChildAt(i3);
                if (childAt2 instanceof DialogCell) {
                    ((DialogCell) childAt2).update(0);
                }
            }
        }
        if (this.scrimPopupWindowItems != null) {
            while (true) {
                ActionBarMenuSubItem[] actionBarMenuSubItemArr = this.scrimPopupWindowItems;
                if (i >= actionBarMenuSubItemArr.length) {
                    break;
                }
                actionBarMenuSubItemArr[i].setColors(getThemedColor("actionBarDefaultSubmenuItem"), getThemedColor("actionBarDefaultSubmenuItemIcon"));
                this.scrimPopupWindowItems[i].setSelectorColor(getThemedColor("dialogButtonSelector"));
                i++;
            }
        }
        ActionBarPopupWindow actionBarPopupWindow = this.scrimPopupWindow;
        if (actionBarPopupWindow != null) {
            View contentView = actionBarPopupWindow.getContentView();
            contentView.setBackgroundColor(getThemedColor("actionBarDefaultSubmenuBackground"));
            contentView.invalidate();
        }
        InstantCameraView instantCameraView = this.instantCameraView;
        if (instantCameraView != null) {
            instantCameraView.invalidateBlur();
        }
        PinnedLineView pinnedLineView = this.pinnedLineView;
        if (pinnedLineView != null) {
            pinnedLineView.updateColors();
        }
        ChatActivityEnterTopView chatActivityEnterTopView = this.chatActivityEnterTopView;
        if (!(chatActivityEnterTopView == null || chatActivityEnterTopView.getEditView() == null)) {
            this.chatActivityEnterTopView.getEditView().updateColors();
        }
        ActionBarMenuItem actionBarMenuItem = this.headerItem;
        if (actionBarMenuItem != null) {
            actionBarMenuItem.updateColor();
        }
        setNavigationBarColor(getThemedColor("windowBackgroundGray"));
        FragmentContextView fragmentContextView = this.fragmentContextView;
        if (fragmentContextView != null) {
            fragmentContextView.updateColors();
        }
        ChatAvatarContainer chatAvatarContainer = this.avatarContainer;
        if (chatAvatarContainer != null) {
            chatAvatarContainer.updateColors();
        }
        BlurredFrameLayout blurredFrameLayout = this.pinnedMessageView;
        if (blurredFrameLayout != null) {
            blurredFrameLayout.backgroundColor = getThemedColor("chat_topPanelBackground");
        }
        BlurredFrameLayout blurredFrameLayout2 = this.topChatPanelView;
        if (blurredFrameLayout2 != null) {
            blurredFrameLayout2.backgroundColor = getThemedColor("chat_topPanelBackground");
        }
        SizeNotifierFrameLayout sizeNotifierFrameLayout = this.contentView;
        if (sizeNotifierFrameLayout != null) {
            sizeNotifierFrameLayout.invalidateBlurredViews();
        }
        ActionBarLayout actionBarLayout = this.parentLayout;
        if (!(actionBarLayout == null || actionBarLayout.getDrawerLayoutContainer() == null)) {
            this.parentLayout.getDrawerLayoutContainer().setBehindKeyboardColor(getThemedColor("windowBackgroundWhite"));
        }
    }

    public ChatAvatarContainer getAvatarContainer() {
        return this.avatarContainer;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected AnimatorSet onCustomTransitionAnimation(boolean z, final Runnable runnable) {
        if (!z || !this.fromPullingDownTransition || getParentLayout().fragmentsStack.size() <= 1) {
            return null;
        }
        BaseFragment baseFragment = getParentLayout().fragmentsStack.get(getParentLayout().fragmentsStack.size() - 2);
        if (!(baseFragment instanceof ChatActivity)) {
            return null;
        }
        this.wasManualScroll = true;
        final ChatActivity chatActivity = (ChatActivity) baseFragment;
        chatActivity.setTransitionToChatActivity(this);
        this.fragmentView.setAlpha(0.0f);
        this.contentView.setSkipBackgroundDrawing(true);
        this.avatarContainer.setTranslationY((float) AndroidUtilities.dp(8.0f));
        this.avatarContainer.getAvatarImageView().setAlpha(0.0f);
        this.avatarContainer.getAvatarImageView().setTranslationY((float) (-AndroidUtilities.dp(8.0f)));
        this.toPullingDownTransition = true;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ChatActivityEnterView chatActivityEnterView = this.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.measure(View.MeasureSpec.makeMeasureSpec(AndroidUtilities.displaySize.x, 1073741824), View.MeasureSpec.makeMeasureSpec(999999, Integer.MIN_VALUE));
        }
        FrameLayout frameLayout = this.bottomOverlay;
        if (frameLayout != null) {
            frameLayout.measure(View.MeasureSpec.makeMeasureSpec(AndroidUtilities.displaySize.x, 1073741824), View.MeasureSpec.makeMeasureSpec(999999, Integer.MIN_VALUE));
        }
        ChatActivityEnterView chatActivityEnterView2 = this.chatActivityEnterView;
        int measuredHeight = chatActivityEnterView2 == null ? 0 : chatActivityEnterView2.getMeasuredHeight();
        FrameLayout frameLayout2 = this.bottomOverlay;
        int max = Math.max(measuredHeight, frameLayout2 == null ? 0 : frameLayout2.getMeasuredHeight());
        ChatActivityEnterView chatActivityEnterView3 = chatActivity.chatActivityEnterView;
        int measuredHeight2 = chatActivityEnterView3 == null ? 0 : chatActivityEnterView3.getMeasuredHeight();
        FrameLayout frameLayout3 = this.bottomOverlay;
        this.pullingBottomOffset = (float) (-(Math.max(measuredHeight2, frameLayout3 == null ? 0 : frameLayout3.getMeasuredHeight()) - max));
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(chatActivity, chatActivity.fragmentContextView.getMeasuredHeight() != this.fragmentContextView.getMeasuredHeight()) { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda17
            public final /* synthetic */ ChatActivity f$1;
            public final /* synthetic */ boolean f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                ChatActivity.this.lambda$onCustomTransitionAnimation$247(this.f$1, this.f$2, valueAnimator);
            }
        });
        updateChatListViewTopPadding();
        AnimatorSet animatorSet = new AnimatorSet();
        this.fragmentTransition = animatorSet;
        animatorSet.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.ChatActivity.114
            int index;

            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                super.onAnimationStart(animator);
                this.index = NotificationCenter.getInstance(((BaseFragment) ChatActivity.this).currentAccount).setAnimationInProgress(this.index, null);
            }

            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                ChatActivity chatActivity2 = ChatActivity.this;
                chatActivity2.fragmentOpened = true;
                ((BaseFragment) chatActivity2).fragmentBeginToShow = true;
                ChatActivity.this.fragmentTransition = null;
                AndroidUtilities.runOnUIThread(new ChatActivity$114$$ExternalSyntheticLambda0(this), 32);
                super.onAnimationEnd(animator);
                ChatActivity.this.contentView.invalidate();
                ChatActivity.this.contentView.setSkipBackgroundDrawing(false);
                ChatActivity.this.toPullingDownTransition = false;
                chatActivity.setTransitionToChatProgress(0.0f);
                chatActivity.setTransitionToChatActivity(null);
                ((BaseFragment) ChatActivity.this).fragmentView.setAlpha(1.0f);
                runnable.run();
                ChatActivity.this.avatarContainer.setTranslationY(0.0f);
                chatActivity.avatarContainer.setTranslationY(0.0f);
                chatActivity.avatarContainer.getAvatarImageView().setTranslationY(0.0f);
                ChatActivity.this.avatarContainer.getAvatarImageView().setScaleX(1.0f);
                ChatActivity.this.avatarContainer.getAvatarImageView().setScaleY(1.0f);
                ChatActivity.this.avatarContainer.getAvatarImageView().setAlpha(1.0f);
                chatActivity.avatarContainer.getAvatarImageView().setScaleX(1.0f);
                chatActivity.avatarContainer.getAvatarImageView().setScaleY(1.0f);
                chatActivity.avatarContainer.getAvatarImageView().setAlpha(1.0f);
                chatActivity.pinnedMessageView.setAlpha(1.0f);
                chatActivity.topChatPanelView.setAlpha(1.0f);
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onAnimationEnd$0() {
                NotificationCenter.getInstance(((BaseFragment) ChatActivity.this).currentAccount).onAnimationFinish(this.index);
            }
        });
        this.fragmentTransition.setDuration(300L);
        this.fragmentTransition.setInterpolator(CubicBezierInterpolator.DEFAULT);
        this.fragmentTransition.playTogether(ofFloat);
        AndroidUtilities.runOnUIThread(this.fragmentTransitionRunnable, 200);
        return this.fragmentTransition;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onCustomTransitionAnimation$247(ChatActivity chatActivity, boolean z, ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        chatActivity.setTransitionToChatProgress(floatValue);
        float f = 1.0f - floatValue;
        float dp = ((float) AndroidUtilities.dp(8.0f)) * f;
        this.avatarContainer.setTranslationY(dp);
        this.avatarContainer.getAvatarImageView().setTranslationY(-dp);
        float f2 = ((float) (-AndroidUtilities.dp(8.0f))) * floatValue;
        chatActivity.avatarContainer.setTranslationY(f2);
        chatActivity.avatarContainer.getAvatarImageView().setTranslationY(-f2);
        float f3 = (floatValue * 0.2f) + 0.8f;
        this.avatarContainer.getAvatarImageView().setScaleX(f3);
        this.avatarContainer.getAvatarImageView().setScaleY(f3);
        this.avatarContainer.getAvatarImageView().setAlpha(floatValue);
        float f4 = (0.2f * f) + 0.8f;
        chatActivity.avatarContainer.getAvatarImageView().setScaleX(f4);
        chatActivity.avatarContainer.getAvatarImageView().setScaleY(f4);
        chatActivity.avatarContainer.getAvatarImageView().setAlpha(f);
        ChatActivityEnterView chatActivityEnterView = chatActivity.chatActivityEnterView;
        if (chatActivityEnterView != null) {
            chatActivityEnterView.setTranslationY((-this.pullingBottomOffset) * floatValue);
        }
        FrameLayout frameLayout = chatActivity.bottomOverlay;
        if (frameLayout != null) {
            frameLayout.setTranslationY((-this.pullingBottomOffset) * floatValue);
        }
        if (z) {
            chatActivity.fragmentContextView.setAlpha(f);
        }
        chatActivity.pinnedMessageView.setAlpha(f);
        chatActivity.topChatPanelView.setAlpha(f);
    }

    /* access modifiers changed from: private */
    public void setTransitionToChatActivity(ChatActivity chatActivity) {
        this.pullingDownAnimateToActivity = chatActivity;
    }

    /* access modifiers changed from: private */
    public void setTransitionToChatProgress(float f) {
        this.pullingDownAnimateProgress = f;
        this.fragmentView.invalidate();
        this.chatListView.invalidate();
    }

    /* access modifiers changed from: private */
    public void showChatThemeBottomSheet() {
        this.chatThemeBottomSheet = new ChatThemeBottomSheet(this, this.themeDelegate);
        this.chatListView.setOnInterceptTouchListener(ChatActivity$$ExternalSyntheticLambda246.INSTANCE);
        setChildrenEnabled(this.contentView, false);
        showDialog(this.chatThemeBottomSheet, new DialogInterface.OnDismissListener() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda54
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                ChatActivity.this.lambda$showChatThemeBottomSheet$249(dialogInterface);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showChatThemeBottomSheet$249(DialogInterface dialogInterface) {
        this.chatThemeBottomSheet = null;
        this.chatListView.setOnInterceptTouchListener(null);
        setChildrenEnabled(this.contentView, true);
        ChatThemeController.clearWallpaperThumbImages();
    }

    private void setChildrenEnabled(View view, boolean z) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                setChildrenEnabled(viewGroup.getChildAt(i), z);
            }
        }
        if (!(view == this.chatListView || view == this.contentView)) {
            view.setEnabled(z);
        }
    }

    private void checkThemeEmoticon() {
        TLRPC$ChatFull tLRPC$ChatFull;
        if (this.fragmentOpened) {
            String str = null;
            TLRPC$UserFull tLRPC$UserFull = this.userInfo;
            if (tLRPC$UserFull != null) {
                str = tLRPC$UserFull.theme_emoticon;
            }
            if (str == null && (tLRPC$ChatFull = this.chatInfo) != null) {
                str = tLRPC$ChatFull.theme_emoticon;
            }
            setChatThemeEmoticon(str);
        }
    }

    private void setChatThemeEmoticon(String str) {
        boolean z = false;
        ChatThemeController.getInstance(this.currentAccount).setDialogTheme(this.dialog_id, str, false);
        if (!TextUtils.isEmpty(str)) {
            ChatThemeController.requestChatTheme(str, new ResultCallback() { // from class: org.telegram.ui.ChatActivity$$ExternalSyntheticLambda233
                @Override // org.telegram.tgnet.ResultCallback
                public final void onComplete(Object obj) {
                    ChatActivity.this.lambda$setChatThemeEmoticon$250((EmojiThemes) obj);
                }

                @Override // org.telegram.tgnet.ResultCallback
                public /* synthetic */ void onError(TLRPC$TL_error tLRPC$TL_error) {
                    ResultCallback.CC.$default$onError(this, tLRPC$TL_error);
                }
            });
            return;
        }
        ThemeDelegate themeDelegate = this.themeDelegate;
        if (this.openAnimationStartTime != 0) {
            z = true;
        }
        themeDelegate.setCurrentTheme(null, z, null);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$setChatThemeEmoticon$250(EmojiThemes emojiThemes) {
        this.themeDelegate.setCurrentTheme(emojiThemes, this.openAnimationStartTime != 0, null);
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public int getNavigationBarColor() {
        return getThemedColor("windowBackgroundGray");
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public int getThemedColor(String str) {
        ThemeDelegate themeDelegate = this.themeDelegate;
        Integer color = themeDelegate != null ? themeDelegate.getColor(str) : null;
        return color != null ? color.intValue() : super.getThemedColor(str);
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public Drawable getThemedDrawable(String str) {
        Drawable drawable = this.themeDelegate.getDrawable(str);
        return drawable != null ? drawable : super.getThemedDrawable(str);
    }

    /* access modifiers changed from: private */
    public Paint getThemedPaint(String str) {
        Paint paint = this.themeDelegate.getPaint(str);
        return paint != null ? paint : Theme.getThemePaint(str);
    }

    public float getChatListViewPadding() {
        return this.chatListViewPaddingTop;
    }

    public FragmentContextView getFragmentContextView() {
        return this.fragmentContextView;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public Theme.ResourcesProvider getResourceProvider() {
        return this.themeDelegate;
    }

    /* loaded from: classes3.dex */
    public class ThemeDelegate implements Theme.ResourcesProvider, ChatActionCell.ThemeDelegate, ForwardingPreviewView.ResourcesDelegate {
        private final Matrix actionMatrix = new Matrix();
        private HashMap<String, Integer> animatingColors;
        Theme.MessageDrawable animatingMessageDrawable;
        Theme.MessageDrawable animatingMessageMediaDrawable;
        private Drawable backgroundDrawable;
        private List<EmojiThemes> cachedThemes;
        private EmojiThemes chatTheme;
        private HashMap<String, Integer> currentColors = new HashMap<>();
        private final HashMap<String, Drawable> currentDrawables = new HashMap<>();
        private final HashMap<String, Paint> currentPaints = new HashMap<>();
        int currentServiceColor;
        boolean drawSelectedGradient;
        boolean drawServiceGradient;
        private boolean isDark = Theme.getActiveTheme().isDark();
        private Paint paint = new Paint();
        private AnimatorSet patternAlphaAnimator;
        private ValueAnimator patternIntensityAnimator;
        private Bitmap serviceBitmap;
        private Bitmap serviceBitmapSource;
        private Canvas serviceCanvas;
        private BitmapShader serviceShader;
        private BitmapShader serviceShaderSource;
        Bitmap startServiceBitmap;
        int startServiceButtonColor;
        int startServiceColor;
        int startServiceIconColor;
        int startServiceLinkColor;
        int startServiceTextColor;
        private boolean useSourceShader;

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public /* synthetic */ int getColorOrDefault(String str) {
            return Theme.ResourcesProvider.CC.$default$getColorOrDefault(this, str);
        }

        ThemeDelegate() {
            boolean z = false;
            if (isThemeChangeAvailable()) {
                EmojiThemes dialogTheme = ChatThemeController.getInstance(((BaseFragment) ChatActivity.this).currentAccount).getDialogTheme(ChatActivity.this.dialog_id);
                this.chatTheme = dialogTheme;
                if (dialogTheme != null) {
                    setupChatTheme(dialogTheme, false, true);
                    z = true;
                }
            }
            if (z || ThemeEditorView.getInstance() != null) {
                AndroidUtilities.runOnUIThread(new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda3(this));
            } else {
                Theme.refreshThemeColors(true, true);
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$new$0() {
            NotificationCenter globalInstance = NotificationCenter.getGlobalInstance();
            int i = NotificationCenter.didSetNewTheme;
            Object[] objArr = new Object[3];
            boolean z = false;
            objArr[0] = Boolean.FALSE;
            objArr[1] = Boolean.TRUE;
            if (((BaseFragment) ChatActivity.this).parentLayout != null && !((BaseFragment) ChatActivity.this).parentLayout.isTransitionAnimationInProgress()) {
                z = true;
            }
            objArr[2] = Boolean.valueOf(z);
            globalInstance.postNotificationName(i, objArr);
        }

        public List<EmojiThemes> getCachedThemes() {
            return this.cachedThemes;
        }

        public void setCachedThemes(List<EmojiThemes> list) {
            this.cachedThemes = list;
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public Integer getColor(String str) {
            String fallbackKey;
            Integer num;
            if (this.chatTheme == null) {
                return Integer.valueOf(Theme.getColor(str));
            }
            HashMap<String, Integer> hashMap = this.animatingColors;
            if (hashMap != null && (num = hashMap.get(str)) != null) {
                return num;
            }
            Integer num2 = this.currentColors.get(str);
            if (num2 == null) {
                if ("chat_outBubbleGradient".equals(str) || "chat_outBubbleGradient2".equals(str) || "chat_outBubbleGradient3".equals(str)) {
                    num2 = this.currentColors.get("chat_outBubble");
                    if (num2 == null) {
                        num2 = Theme.getColorOrNull(str);
                    }
                    if (num2 == null) {
                        num2 = Integer.valueOf(Theme.getColor("chat_outBubble"));
                    }
                }
                if (num2 == null && (fallbackKey = Theme.getFallbackKey(str)) != null) {
                    num2 = this.currentColors.get(fallbackKey);
                }
            }
            return (num2 != null || this.chatTheme == null) ? num2 : Integer.valueOf(Theme.getDefaultColor(str));
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public Integer getCurrentColor(String str) {
            return getCurrentColor(str, false);
        }

        public Integer getCurrentColor(String str, boolean z) {
            HashMap<String, Integer> hashMap;
            if (this.chatTheme == null) {
                return Theme.getColorOrNull(str);
            }
            Integer num = null;
            if (!z && (hashMap = this.animatingColors) != null) {
                num = hashMap.get(str);
            }
            return num == null ? this.currentColors.get(str) : num;
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public void setAnimatedColor(String str, int i) {
            HashMap<String, Integer> hashMap = this.animatingColors;
            if (hashMap != null) {
                hashMap.put(str, Integer.valueOf(i));
            }
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public void applyServiceShaderMatrix(int i, int i2, float f, float f2) {
            Bitmap bitmap;
            BitmapShader bitmapShader;
            if (this.chatTheme == null || (bitmap = this.serviceBitmap) == null || (bitmapShader = this.serviceShader) == null) {
                Theme.ResourcesProvider.CC.$default$applyServiceShaderMatrix(this, i, i2, f, f2);
            } else if (this.useSourceShader) {
                Theme.applyServiceShaderMatrix(this.serviceBitmapSource, this.serviceShaderSource, this.actionMatrix, i, i2, f, f2);
            } else {
                Theme.applyServiceShaderMatrix(bitmap, bitmapShader, this.actionMatrix, i, i2, f, f2);
            }
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public boolean hasGradientService() {
            if (this.chatTheme != null) {
                return this.serviceShader != null;
            }
            return Theme.hasGradientService();
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public Drawable getDrawable(String str) {
            if (!this.currentDrawables.isEmpty()) {
                return this.currentDrawables.get(str);
            }
            return null;
        }

        @Override // org.telegram.ui.ActionBar.Theme.ResourcesProvider
        public Paint getPaint(String str) {
            if (this.chatTheme != null) {
                return this.currentPaints.get(str);
            }
            return null;
        }

        public boolean isThemeChangeAvailable() {
            ChatActivity chatActivity = ChatActivity.this;
            return chatActivity.currentChat == null && chatActivity.currentEncryptedChat == null && !chatActivity.currentUser.bot && chatActivity.dialog_id >= 0;
        }

        public EmojiThemes getCurrentTheme() {
            return this.chatTheme;
        }

        @Override // org.telegram.ui.Components.ForwardingPreviewView.ResourcesDelegate
        public Drawable getWallpaperDrawable() {
            Drawable drawable = this.backgroundDrawable;
            return drawable != null ? drawable : Theme.getCachedWallpaperNonBlocking();
        }

        @Override // org.telegram.ui.Components.ForwardingPreviewView.ResourcesDelegate
        public boolean isWallpaperMotion() {
            if (this.chatTheme != null) {
                return false;
            }
            return Theme.isWallpaperMotion();
        }

        public void setCurrentTheme(EmojiThemes emojiThemes, boolean z, Boolean bool) {
            if (((BaseFragment) ChatActivity.this).parentLayout != null) {
                boolean booleanValue = bool != null ? bool.booleanValue() : Theme.getActiveTheme().isDark();
                String str = null;
                String emoticon = emojiThemes != null ? emojiThemes.getEmoticon() : null;
                EmojiThemes emojiThemes2 = this.chatTheme;
                if (emojiThemes2 != null) {
                    str = emojiThemes2.getEmoticon();
                }
                if (!isThemeChangeAvailable()) {
                    return;
                }
                if (!TextUtils.equals(str, emoticon) || this.isDark != booleanValue) {
                    this.isDark = booleanValue;
                    Theme.ThemeInfo currentNightTheme = booleanValue ? Theme.getCurrentNightTheme() : Theme.getCurrentTheme();
                    ActionBarLayout.ThemeAnimationSettings themeAnimationSettings = new ActionBarLayout.ThemeAnimationSettings(currentNightTheme, currentNightTheme.currentAccentId, currentNightTheme.isDark(), !z);
                    int i = -1;
                    if (this.chatTheme == null) {
                        Drawable cachedWallpaperNonBlocking = Theme.getCachedWallpaperNonBlocking();
                        this.drawServiceGradient = cachedWallpaperNonBlocking instanceof MotionBackgroundDrawable;
                        initServiceMessageColors(cachedWallpaperNonBlocking);
                        this.startServiceTextColor = this.drawServiceGradient ? -1 : Theme.getColor("chat_serviceText");
                        this.startServiceLinkColor = this.drawServiceGradient ? -1 : Theme.getColor("chat_serviceLink");
                        this.startServiceButtonColor = this.drawServiceGradient ? -1 : Theme.getColor("chat_serviceLink");
                        this.startServiceIconColor = this.drawServiceGradient ? -1 : Theme.getColor("chat_serviceIcon");
                    } else if (this.drawServiceGradient) {
                        this.startServiceBitmap = ((MotionBackgroundDrawable) this.backgroundDrawable).getBitmap();
                    }
                    this.startServiceColor = this.currentServiceColor;
                    this.startServiceTextColor = this.drawServiceGradient ? -1 : getCurrentColorOrDefault("chat_serviceText", true);
                    this.startServiceLinkColor = this.drawServiceGradient ? -1 : getCurrentColorOrDefault("chat_serviceLink", true);
                    this.startServiceButtonColor = this.drawServiceGradient ? -1 : getCurrentColorOrDefault("chat_serviceLink", true);
                    if (!this.drawServiceGradient) {
                        i = getCurrentColorOrDefault("chat_serviceIcon", true);
                    }
                    this.startServiceIconColor = i;
                    if (emojiThemes != null) {
                        int i2 = AndroidUtilities.calcDrawableColor(this.backgroundDrawable)[0];
                        initDrawables();
                        initPaints();
                    }
                    themeAnimationSettings.applyTheme = false;
                    themeAnimationSettings.afterStartDescriptionsAddedRunnable = new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda6(this, emojiThemes, z);
                    if (z) {
                        themeAnimationSettings.animationProgress = new ActionBarLayout.ThemeAnimationSettings.onAnimationProgress() { // from class: org.telegram.ui.ChatActivity.ThemeDelegate.1
                            @Override // org.telegram.ui.ActionBar.ActionBarLayout.ThemeAnimationSettings.onAnimationProgress
                            public void setProgress(float f) {
                                ChatActivity.this.chatListView.invalidate();
                                ThemeDelegate themeDelegate = ThemeDelegate.this;
                                themeDelegate.animatingMessageDrawable.crossfadeProgress = f;
                                themeDelegate.animatingMessageMediaDrawable.crossfadeProgress = f;
                                themeDelegate.updateServiceMessageColor(f);
                            }
                        };
                        themeAnimationSettings.beforeAnimationRunnable = new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda5(this);
                        themeAnimationSettings.afterAnimationRunnable = new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda4(this);
                    } else {
                        SizeNotifierFrameLayout sizeNotifierFrameLayout = ChatActivity.this.contentView;
                        if (sizeNotifierFrameLayout != null) {
                            sizeNotifierFrameLayout.setBackgroundImage(Theme.getCachedWallpaper(), Theme.isWallpaperMotion());
                        }
                    }
                    themeAnimationSettings.onlyTopFragment = true;
                    themeAnimationSettings.resourcesProvider = this;
                    themeAnimationSettings.duration = 250;
                    ((BaseFragment) ChatActivity.this).parentLayout.animateThemedValues(themeAnimationSettings);
                    Runnable runnable = ChatActivity.this.onThemeChange;
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$setCurrentTheme$1(EmojiThemes emojiThemes, boolean z) {
            setupChatTheme(emojiThemes, z, false);
            initServiceMessageColors(this.backgroundDrawable);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$setCurrentTheme$2() {
            this.animatingColors = new HashMap<>();
            Theme.MessageDrawable messageDrawable = (Theme.MessageDrawable) ChatActivity.this.getThemedDrawable("drawableMsgOut");
            this.animatingMessageDrawable = messageDrawable;
            messageDrawable.crossfadeFromDrawable = ((BaseFragment) ChatActivity.this).parentLayout.messageDrawableOutStart;
            Theme.MessageDrawable messageDrawable2 = (Theme.MessageDrawable) ChatActivity.this.getThemedDrawable("drawableMsgOutMedia");
            this.animatingMessageMediaDrawable = messageDrawable2;
            messageDrawable2.crossfadeFromDrawable = ((BaseFragment) ChatActivity.this).parentLayout.messageDrawableOutMediaStart;
            this.animatingMessageDrawable.crossfadeProgress = 0.0f;
            this.animatingMessageMediaDrawable.crossfadeProgress = 0.0f;
            ChatActivity.this.updateMessagesVisiblePart(false);
            updateServiceMessageColor(0.0f);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$setCurrentTheme$3() {
            this.animatingMessageDrawable.crossfadeFromDrawable = null;
            this.animatingMessageMediaDrawable.crossfadeFromDrawable = null;
            this.animatingColors = null;
            updateServiceMessageColor(1.0f);
        }

        private void setupChatTheme(EmojiThemes emojiThemes, boolean z, boolean z2) {
            Theme.ThemeInfo themeInfo;
            this.chatTheme = emojiThemes;
            Drawable backgroundImage = ((BaseFragment) ChatActivity.this).fragmentView != null ? ((SizeNotifierFrameLayout) ((BaseFragment) ChatActivity.this).fragmentView).getBackgroundImage() : null;
            final MotionBackgroundDrawable motionBackgroundDrawable = backgroundImage instanceof MotionBackgroundDrawable ? (MotionBackgroundDrawable) backgroundImage : null;
            int phase = motionBackgroundDrawable != null ? motionBackgroundDrawable.getPhase() : 0;
            if (emojiThemes == null || emojiThemes.showAsDefaultStub) {
                Theme.getServiceMessageColor();
            }
            if (emojiThemes == null) {
                this.currentColors = new HashMap<>();
                this.currentPaints.clear();
                this.currentDrawables.clear();
                Drawable cachedWallpaperNonBlocking = Theme.getCachedWallpaperNonBlocking();
                if (cachedWallpaperNonBlocking instanceof MotionBackgroundDrawable) {
                    ((MotionBackgroundDrawable) cachedWallpaperNonBlocking).setPhase(phase);
                }
                this.backgroundDrawable = null;
                if (Theme.getActiveTheme().isDark() == this.isDark) {
                    themeInfo = Theme.getActiveTheme();
                } else {
                    SharedPreferences sharedPreferences = ApplicationLoader.applicationContext.getSharedPreferences("themeconfig", 0);
                    String str = "Blue";
                    String string = sharedPreferences.getString("lastDayTheme", str);
                    if (Theme.getTheme(string) != null && !Theme.getTheme(string).isDark()) {
                        str = string;
                    }
                    String str2 = "Dark Blue";
                    String string2 = sharedPreferences.getString("lastDarkTheme", str2);
                    if (Theme.getTheme(string2) != null && Theme.getTheme(string2).isDark()) {
                        str2 = string2;
                    }
                    themeInfo = this.isDark ? Theme.getTheme(str2) : Theme.getTheme(str);
                }
                Theme.applyTheme(themeInfo, false, this.isDark);
                return;
            }
            if (ApplicationLoader.applicationContext != null) {
                Theme.createChatResources(ApplicationLoader.applicationContext, false);
            }
            this.currentColors = emojiThemes.createColors(((BaseFragment) ChatActivity.this).currentAccount, this.isDark ? 1 : 0);
            this.backgroundDrawable = getBackgroundDrawableFromTheme(emojiThemes, phase);
            AnimatorSet animatorSet = this.patternAlphaAnimator;
            if (animatorSet != null) {
                animatorSet.cancel();
            }
            if (z) {
                this.patternAlphaAnimator = new AnimatorSet();
                if (motionBackgroundDrawable != null) {
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 0.0f);
                    ofFloat.addUpdateListener(new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda0(motionBackgroundDrawable));
                    ofFloat.addListener(new AnimatorListenerAdapter(this) { // from class: org.telegram.ui.ChatActivity.ThemeDelegate.2
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            super.onAnimationEnd(animator);
                            motionBackgroundDrawable.setPatternAlpha(1.0f);
                        }
                    });
                    ofFloat.setDuration(200L);
                    this.patternAlphaAnimator.playTogether(ofFloat);
                }
                Drawable drawable = this.backgroundDrawable;
                if (drawable instanceof MotionBackgroundDrawable) {
                    final MotionBackgroundDrawable motionBackgroundDrawable2 = (MotionBackgroundDrawable) drawable;
                    motionBackgroundDrawable2.setPatternAlpha(0.0f);
                    ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.0f, 1.0f);
                    ofFloat2.addUpdateListener(new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda1(motionBackgroundDrawable2));
                    ofFloat2.addListener(new AnimatorListenerAdapter(this) { // from class: org.telegram.ui.ChatActivity.ThemeDelegate.3
                        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator) {
                            super.onAnimationEnd(animator);
                            motionBackgroundDrawable2.setPatternAlpha(1.0f);
                        }
                    });
                    ofFloat2.setDuration(250L);
                    this.patternAlphaAnimator.playTogether(ofFloat2);
                }
                this.patternAlphaAnimator.start();
            }
            if (z2) {
                int i = AndroidUtilities.calcDrawableColor(this.backgroundDrawable)[0];
                initDrawables();
                initPaints();
                initServiceMessageColors(this.backgroundDrawable);
                updateServiceMessageColor(1.0f);
            }
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ void lambda$setupChatTheme$4(MotionBackgroundDrawable motionBackgroundDrawable, ValueAnimator valueAnimator) {
            motionBackgroundDrawable.setPatternAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ void lambda$setupChatTheme$5(MotionBackgroundDrawable motionBackgroundDrawable, ValueAnimator valueAnimator) {
            motionBackgroundDrawable.setPatternAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }

        private void initDrawables() {
            Drawable drawable;
            String themeDrawableColorKey;
            for (Map.Entry<String, Drawable> entry : Theme.getThemeDrawablesMap().entrySet()) {
                String key = entry.getKey();
                key.hashCode();
                char c = 65535;
                switch (key.hashCode()) {
                    case -2061232504:
                        if (key.equals("drawableMsgIn")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -2005320132:
                        if (key.equals("drawableMsgInMedia")) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1656383241:
                        if (key.equals("drawableMsgInMediaSelected")) {
                            c = 2;
                            break;
                        }
                        break;
                    case -1451465639:
                        if (key.equals("drawableMsgOutMedia")) {
                            c = 3;
                            break;
                        }
                        break;
                    case -1084641786:
                        if (key.equals("drawableMsgOutSelected")) {
                            c = 4;
                            break;
                        }
                        break;
                    case -8170988:
                        if (key.equals("drawableMsgOutMediaSelected")) {
                            c = 5;
                            break;
                        }
                        break;
                    case 300508483:
                        if (key.equals("drawableMsgInSelected")) {
                            c = 6;
                            break;
                        }
                        break;
                    case 526307915:
                        if (key.equals("drawableMsgOut")) {
                            c = 7;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        drawable = new Theme.MessageDrawable(0, false, false, this);
                        break;
                    case 1:
                        drawable = new Theme.MessageDrawable(1, false, false, this);
                        break;
                    case 2:
                        drawable = new Theme.MessageDrawable(1, false, true, this);
                        break;
                    case 3:
                        drawable = new Theme.MessageDrawable(1, true, false, this);
                        break;
                    case 4:
                        drawable = new Theme.MessageDrawable(0, true, true, this);
                        break;
                    case 5:
                        drawable = new Theme.MessageDrawable(1, true, true, this);
                        break;
                    case 6:
                        drawable = new Theme.MessageDrawable(0, false, true, this);
                        break;
                    case 7:
                        drawable = new Theme.MessageDrawable(0, true, false, this);
                        break;
                    default:
                        Drawable.ConstantState constantState = entry.getValue().getConstantState();
                        drawable = constantState != null ? constantState.newDrawable().mutate() : null;
                        if (!(drawable == null || (themeDrawableColorKey = Theme.getThemeDrawableColorKey(entry.getKey())) == null)) {
                            Integer color = getColor(themeDrawableColorKey);
                            if (color == null) {
                                color = Integer.valueOf(Theme.getColor(themeDrawableColorKey));
                            }
                            Theme.setDrawableColor(drawable, color.intValue());
                            break;
                        }
                        break;
                }
                if (drawable != null) {
                    this.currentDrawables.put(entry.getKey(), drawable);
                }
            }
        }

        private void initPaints() {
            Paint paint;
            for (Map.Entry<String, Paint> entry : Theme.getThemePaintsMap().entrySet()) {
                Paint value = entry.getValue();
                if (value instanceof TextPaint) {
                    paint = new TextPaint();
                    paint.setTextSize(value.getTextSize());
                    paint.setTypeface(value.getTypeface());
                } else {
                    paint = new Paint();
                }
                if ((value.getFlags() & 1) != 0) {
                    paint.setFlags(1);
                }
                String themePaintColorKey = Theme.getThemePaintColorKey(entry.getKey());
                if (themePaintColorKey != null) {
                    Integer color = getColor(themePaintColorKey);
                    if (color == null) {
                        color = Integer.valueOf(Theme.getColor(themePaintColorKey));
                    }
                    paint.setColor(color.intValue());
                }
                this.currentPaints.put(entry.getKey(), paint);
            }
        }

        private void initServiceMessageColors(Drawable drawable) {
            boolean z = false;
            int i = AndroidUtilities.calcDrawableColor(drawable)[0];
            Integer currentColor = getCurrentColor("chat_serviceBackground");
            Integer currentColor2 = getCurrentColor("chat_selectedBackground");
            if (currentColor == null) {
                currentColor = Integer.valueOf(i);
            }
            this.currentServiceColor = currentColor.intValue();
            if ((drawable instanceof MotionBackgroundDrawable) && SharedConfig.getDevicePerformanceClass() != 0) {
                z = true;
            }
            this.drawServiceGradient = z;
            this.drawSelectedGradient = z;
            if (z) {
                this.serviceBitmap = Bitmap.createBitmap(60, 80, Bitmap.Config.ARGB_8888);
                this.serviceBitmapSource = ((MotionBackgroundDrawable) drawable).getBitmap();
                Canvas canvas = new Canvas(this.serviceBitmap);
                this.serviceCanvas = canvas;
                canvas.drawBitmap(this.serviceBitmapSource, 0.0f, 0.0f, (Paint) null);
                Bitmap bitmap = this.serviceBitmap;
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                this.serviceShader = new BitmapShader(bitmap, tileMode, tileMode);
                Bitmap bitmap2 = this.serviceBitmapSource;
                Shader.TileMode tileMode2 = Shader.TileMode.CLAMP;
                this.serviceShaderSource = new BitmapShader(bitmap2, tileMode2, tileMode2);
                this.useSourceShader = true;
            } else {
                this.serviceBitmap = null;
                this.serviceShader = null;
            }
            Paint paint = getPaint("paintChatActionBackground");
            Paint paint2 = getPaint("paintChatActionBackgroundSelected");
            Paint paint3 = getPaint("paintChatMessageBackgroundSelected");
            if (paint != null) {
                if (this.drawServiceGradient) {
                    ColorMatrix colorMatrix = new ColorMatrix();
                    colorMatrix.setSaturation(((MotionBackgroundDrawable) drawable).getIntensity() >= 0 ? 1.8f : 0.5f);
                    paint.setAlpha(127);
                    paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
                    paint.setShader(this.serviceShaderSource);
                    paint2.setAlpha(127);
                    paint2.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
                    paint2.setShader(this.serviceShaderSource);
                } else {
                    paint.setColorFilter(null);
                    paint.setShader(null);
                    paint2.setColorFilter(null);
                    paint2.setShader(null);
                }
            }
            if (paint3 == null) {
                paint3 = new Paint(1);
                this.currentPaints.put("paintChatMessageBackgroundSelected", paint3);
            }
            if (this.drawSelectedGradient) {
                ColorMatrix colorMatrix2 = new ColorMatrix();
                AndroidUtilities.adjustSaturationColorMatrix(colorMatrix2, 2.5f);
                AndroidUtilities.multiplyBrightnessColorMatrix(colorMatrix2, 0.75f);
                paint3.setAlpha(64);
                paint3.setColorFilter(new ColorMatrixColorFilter(colorMatrix2));
                paint3.setShader(this.serviceShaderSource);
                return;
            }
            if (currentColor2 == null) {
                currentColor2 = getColor("chat_selectedBackground");
            }
            paint3.setColor(currentColor2.intValue());
            paint3.setColorFilter(null);
            paint3.setShader(null);
        }

        /* access modifiers changed from: private */
        public void updateServiceMessageColor(float f) {
            Bitmap bitmap;
            Bitmap bitmap2;
            if (!this.currentPaints.isEmpty()) {
                Paint paint = getPaint("paintChatActionBackground");
                Paint paint2 = getPaint("paintChatActionBackgroundSelected");
                Paint paint3 = getPaint("paintChatMessageBackgroundSelected");
                int i = this.currentServiceColor;
                int i2 = -1;
                int currentColorOrDefault = this.drawServiceGradient ? -1 : getCurrentColorOrDefault("chat_serviceText", true);
                int currentColorOrDefault2 = this.drawServiceGradient ? -1 : getCurrentColorOrDefault("chat_serviceLink", true);
                int currentColorOrDefault3 = this.drawServiceGradient ? -1 : getCurrentColorOrDefault("chat_serviceLink", true);
                if (!this.drawServiceGradient) {
                    i2 = getCurrentColorOrDefault("chat_serviceIcon", true);
                }
                if (f != 1.0f) {
                    i = ColorUtils.blendARGB(this.startServiceColor, i, f);
                    currentColorOrDefault = ColorUtils.blendARGB(this.startServiceTextColor, currentColorOrDefault, f);
                    currentColorOrDefault2 = ColorUtils.blendARGB(this.startServiceLinkColor, currentColorOrDefault2, f);
                    currentColorOrDefault3 = ColorUtils.blendARGB(this.startServiceButtonColor, currentColorOrDefault3, f);
                    i2 = ColorUtils.blendARGB(this.startServiceIconColor, i2, f);
                }
                if (paint != null && !this.drawServiceGradient) {
                    paint.setColor(i);
                    paint2.setColor(i);
                }
                Paint paint4 = getPaint("paintChatActionText");
                if (paint4 != null) {
                    ((TextPaint) paint4).linkColor = currentColorOrDefault2;
                    getPaint("paintChatActionText").setColor(currentColorOrDefault);
                    getPaint("paintChatBotButton").setColor(currentColorOrDefault3);
                }
                Theme.setDrawableColor(getDrawable("drawableMsgStickerCheck"), currentColorOrDefault);
                Theme.setDrawableColor(getDrawable("drawableMsgStickerClock"), currentColorOrDefault);
                Theme.setDrawableColor(getDrawable("drawableMsgStickerHalfCheck"), currentColorOrDefault);
                Theme.setDrawableColor(getDrawable("drawableMsgStickerPinned"), currentColorOrDefault);
                Theme.setDrawableColor(getDrawable("drawableMsgStickerReplies"), currentColorOrDefault);
                Theme.setDrawableColor(getDrawable("drawableMsgStickerViews"), currentColorOrDefault);
                Theme.setDrawableColor(getDrawable("drawableBotInline"), i2);
                Theme.setDrawableColor(getDrawable("drawableBotLink"), i2);
                Theme.setDrawableColor(getDrawable("drawable_botInvite"), i2);
                Theme.setDrawableColor(getDrawable("drawableCommentSticker"), i2);
                Theme.setDrawableColor(getDrawable("drawableGoIcon"), i2);
                Theme.setDrawableColor(getDrawable("drawableReplyIcon"), i2);
                Theme.setDrawableColor(getDrawable("drawableShareIcon"), i2);
                Canvas canvas = this.serviceCanvas;
                if (canvas != null && (bitmap = this.serviceBitmapSource) != null) {
                    if (f == 1.0f || (bitmap2 = this.startServiceBitmap) == null) {
                        this.useSourceShader = true;
                        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
                        if (paint != null) {
                            paint.setShader(this.serviceShaderSource);
                            paint2.setShader(this.serviceShaderSource);
                        }
                        if (paint3 != null) {
                            paint3.setShader(this.serviceShaderSource);
                            return;
                        }
                        return;
                    }
                    this.useSourceShader = false;
                    canvas.drawBitmap(bitmap2, 0.0f, 0.0f, (Paint) null);
                    this.paint.setAlpha((int) (f * 255.0f));
                    this.serviceCanvas.drawBitmap(this.serviceBitmapSource, 0.0f, 0.0f, this.paint);
                    if (paint != null) {
                        paint.setShader(this.serviceShader);
                        paint2.setShader(this.serviceShader);
                    }
                    if (paint3 != null) {
                        paint3.setShader(this.serviceShader);
                    }
                }
            }
        }

        private Drawable getBackgroundDrawableFromTheme(EmojiThemes emojiThemes, int i) {
            if (emojiThemes.showAsDefaultStub) {
                return Theme.createBackgroundDrawable(EmojiThemes.getDefaultThemeInfo(this.isDark), emojiThemes.getPreviewColors(((BaseFragment) ChatActivity.this).currentAccount, this.isDark ? 1 : 0), emojiThemes.getWallpaperLink(this.isDark ? 1 : 0), i).wallpaper;
            }
            Integer color = getColor("chat_wallpaper");
            Integer color2 = getColor("chat_wallpaper_gradient_to");
            Integer color3 = getColor("key_chat_wallpaper_gradient_to2");
            Integer color4 = getColor("key_chat_wallpaper_gradient_to3");
            if (color4 == null) {
                color4 = 0;
            }
            MotionBackgroundDrawable motionBackgroundDrawable = new MotionBackgroundDrawable();
            motionBackgroundDrawable.setPatternBitmap(emojiThemes.getWallpaper(this.isDark ? 1 : 0).settings.intensity);
            motionBackgroundDrawable.setColors(color.intValue(), color2.intValue(), color3.intValue(), color4.intValue(), 0, true);
            motionBackgroundDrawable.setPhase(i);
            int patternColor = motionBackgroundDrawable.getPatternColor();
            boolean z = this.isDark;
            emojiThemes.loadWallpaper(z ? 1 : 0, new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda7(this, emojiThemes, z, motionBackgroundDrawable, patternColor));
            return motionBackgroundDrawable;
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void lambda$getBackgroundDrawableFromTheme$7(EmojiThemes emojiThemes, boolean z, MotionBackgroundDrawable motionBackgroundDrawable, int i, Pair pair) {
            if (pair != null) {
                long longValue = ((Long) pair.first).longValue();
                Bitmap bitmap = (Bitmap) pair.second;
                EmojiThemes emojiThemes2 = this.chatTheme;
                if (emojiThemes2 != null && longValue == emojiThemes2.getTlTheme(this.isDark ? 1 : 0).id && bitmap != null) {
                    ValueAnimator valueAnimator = this.patternIntensityAnimator;
                    if (valueAnimator != null) {
                        valueAnimator.cancel();
                    }
                    motionBackgroundDrawable.setPatternBitmap(emojiThemes.getWallpaper(z ? 1 : 0).settings.intensity, bitmap);
                    motionBackgroundDrawable.setPatternColorFilter(i);
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                    this.patternIntensityAnimator = ofFloat;
                    ofFloat.addUpdateListener(new ChatActivity$ThemeDelegate$$ExternalSyntheticLambda2(motionBackgroundDrawable));
                    this.patternIntensityAnimator.setDuration(250L);
                    this.patternIntensityAnimator.start();
                }
            }
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ void lambda$getBackgroundDrawableFromTheme$6(MotionBackgroundDrawable motionBackgroundDrawable, ValueAnimator valueAnimator) {
            motionBackgroundDrawable.setPatternAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }

        private int getCurrentColorOrDefault(String str, boolean z) {
            Integer currentColor = getCurrentColor(str, z);
            if (currentColor == null) {
                currentColor = Integer.valueOf(Theme.getColor(str, null, z));
            }
            return currentColor.intValue();
        }
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected boolean allowPresentFragment() {
        return !this.inPreviewMode;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    protected boolean hideKeyboardOnShow() {
        MessageObject messageObject = this.threadMessageObject;
        if (messageObject == null || messageObject.getRepliesCount() != 0 || !ChatObject.canSendMessages(this.currentChat)) {
            return super.hideKeyboardOnShow();
        }
        return false;
    }

    @Override // org.telegram.ui.ActionBar.BaseFragment
    public boolean isLightStatusBar() {
        int i;
        if (this.reportType < 0) {
            return super.isLightStatusBar();
        }
        Theme.ResourcesProvider resourceProvider = getResourceProvider();
        if (resourceProvider != null) {
            i = resourceProvider.getColorOrDefault("actionBarActionModeDefault");
        } else {
            i = Theme.getColor("actionBarActionModeDefault", null, true);
        }
        if (ColorUtils.calculateLuminance(i) > 0.699999988079071d) {
            return true;
        }
        return false;
    }

    public MessageObject.GroupedMessages getGroup(long j) {
        return this.groupedMessagesMap.get(j);
    }
}
