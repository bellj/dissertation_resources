package org.telegram.ui;

import androidx.dynamicanimation.animation.DynamicAnimation;
import org.telegram.ui.PhotoViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoViewer$CaptionScrollView$$ExternalSyntheticLambda0 implements DynamicAnimation.OnAnimationUpdateListener {
    public final /* synthetic */ PhotoViewer.CaptionScrollView f$0;

    public /* synthetic */ PhotoViewer$CaptionScrollView$$ExternalSyntheticLambda0(PhotoViewer.CaptionScrollView captionScrollView) {
        this.f$0 = captionScrollView;
    }

    @Override // androidx.dynamicanimation.animation.DynamicAnimation.OnAnimationUpdateListener
    public final void onAnimationUpdate(DynamicAnimation dynamicAnimation, float f, float f2) {
        this.f$0.lambda$new$0(dynamicAnimation, f, f2);
    }
}
