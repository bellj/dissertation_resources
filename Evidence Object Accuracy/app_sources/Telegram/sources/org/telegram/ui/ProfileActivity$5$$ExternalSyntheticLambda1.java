package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.ProfileActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProfileActivity$5$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ProfileActivity.AnonymousClass5 f$0;

    public /* synthetic */ ProfileActivity$5$$ExternalSyntheticLambda1(ProfileActivity.AnonymousClass5 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onItemClick$11(dialogInterface, i);
    }
}
