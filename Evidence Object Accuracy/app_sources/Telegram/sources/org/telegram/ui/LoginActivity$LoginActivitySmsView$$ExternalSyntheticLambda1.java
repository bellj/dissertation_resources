package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivitySmsView$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivitySmsView f$0;

    public /* synthetic */ LoginActivity$LoginActivitySmsView$$ExternalSyntheticLambda1(LoginActivity.LoginActivitySmsView loginActivitySmsView) {
        this.f$0 = loginActivitySmsView;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onBackPressed$38(dialogInterface, i);
    }
}
