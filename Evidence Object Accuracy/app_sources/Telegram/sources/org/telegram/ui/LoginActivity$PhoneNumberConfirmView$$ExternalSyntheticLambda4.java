package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$PhoneNumberConfirmView$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.PhoneNumberConfirmView f$0;
    public final /* synthetic */ LoginActivity.PhoneNumberConfirmView.IConfirmDialogCallback f$1;

    public /* synthetic */ LoginActivity$PhoneNumberConfirmView$$ExternalSyntheticLambda4(LoginActivity.PhoneNumberConfirmView phoneNumberConfirmView, LoginActivity.PhoneNumberConfirmView.IConfirmDialogCallback iConfirmDialogCallback) {
        this.f$0 = phoneNumberConfirmView;
        this.f$1 = iConfirmDialogCallback;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$2(this.f$1, view);
    }
}
