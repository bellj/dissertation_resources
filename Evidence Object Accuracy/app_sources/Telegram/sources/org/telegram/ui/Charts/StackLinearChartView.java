package org.telegram.ui.Charts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import org.telegram.messenger.AndroidUtilities;
import org.telegram.ui.Charts.data.ChartData;
import org.telegram.ui.Charts.data.StackLinearChartData;
import org.telegram.ui.Charts.view_data.StackLinearViewData;

/* loaded from: classes3.dex */
public class StackLinearChartView<T extends StackLinearViewData> extends BaseChartView<StackLinearChartData, T> {
    private float[] mapPoints = new float[2];
    private Matrix matrix = new Matrix();
    Path ovalPath = new Path();
    boolean[] skipPoints;
    float[] startFromY;

    @Override // org.telegram.ui.Charts.BaseChartView
    public int findMaxValue(int i, int i2) {
        return 100;
    }

    @Override // org.telegram.ui.Charts.BaseChartView
    protected float getMinDistance() {
        return 0.1f;
    }

    public StackLinearChartView(Context context) {
        super(context);
        this.superDraw = true;
        this.useAlphaSignature = true;
        this.drawPointOnSelection = false;
    }

    @Override // org.telegram.ui.Charts.BaseChartView
    public T createLineViewData(ChartData.Line line) {
        return (T) new StackLinearViewData(line);
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x03a4  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x03f3  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x03fb  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x03fd  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0404 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0444  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x045a  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x045e  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0468  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0474  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x021d  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0251  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0261 A[ADDED_TO_REGION] */
    @Override // org.telegram.ui.Charts.BaseChartView
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void drawChart(android.graphics.Canvas r36) {
        /*
        // Method dump skipped, instructions count: 1574
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.Charts.StackLinearChartView.drawChart(android.graphics.Canvas):void");
    }

    private int quarterForPoint(float f, float f2) {
        float centerX = this.chartArea.centerX();
        float centerY = this.chartArea.centerY() + ((float) AndroidUtilities.dp(16.0f));
        if (f >= centerX && f2 <= centerY) {
            return 0;
        }
        if (f < centerX || f2 < centerY) {
            return (f >= centerX || f2 < centerY) ? 3 : 2;
        }
        return 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e4 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0112 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x015e  */
    @Override // org.telegram.ui.Charts.BaseChartView
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void drawPickerChart(android.graphics.Canvas r20) {
        /*
        // Method dump skipped, instructions count: 420
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.Charts.StackLinearChartView.drawPickerChart(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    @Override // org.telegram.ui.Charts.BaseChartView, android.view.View
    public void onDraw(Canvas canvas) {
        tick();
        drawChart(canvas);
        drawBottomLine(canvas);
        this.tmpN = this.horizontalLines.size();
        int i = 0;
        while (true) {
            this.tmpI = i;
            int i2 = this.tmpI;
            if (i2 < this.tmpN) {
                drawHorizontalLines(canvas, this.horizontalLines.get(i2));
                drawSignaturesToHorizontalLines(canvas, this.horizontalLines.get(this.tmpI));
                i = this.tmpI + 1;
            } else {
                drawBottomSignature(canvas);
                drawPicker(canvas);
                drawSelection(canvas);
                super.onDraw(canvas);
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0140  */
    @Override // org.telegram.ui.Charts.BaseChartView
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void fillTransitionParams(org.telegram.ui.Charts.view_data.TransitionParams r18) {
        /*
        // Method dump skipped, instructions count: 344
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.Charts.StackLinearChartView.fillTransitionParams(org.telegram.ui.Charts.view_data.TransitionParams):void");
    }
}
