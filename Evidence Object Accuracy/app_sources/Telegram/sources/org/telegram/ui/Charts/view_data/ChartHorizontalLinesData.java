package org.telegram.ui.Charts.view_data;

/* loaded from: classes3.dex */
public class ChartHorizontalLinesData {
    public int alpha;
    public int fixedAlpha;
    public int[] values;
    public String[] valuesStr;
    public String[] valuesStr2;

    public ChartHorizontalLinesData(int i, int i2, boolean z) {
        this(i, i2, z, 0.0f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ChartHorizontalLinesData(int r8, int r9, boolean r10, float r11) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.Charts.view_data.ChartHorizontalLinesData.<init>(int, int, boolean, float):void");
    }

    public static int lookupHeight(int i) {
        if (i > 100) {
            i = round(i);
        }
        return ((int) Math.ceil((double) (((float) i) / 5.0f))) * 5;
    }

    private static int round(int i) {
        if (((float) (i / 5)) % 10.0f == 0.0f) {
            return i;
        }
        return ((i / 10) + 1) * 10;
    }
}
