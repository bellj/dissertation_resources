package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.ThemeActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemeActivity$1$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ThemeActivity.AnonymousClass1 f$0;

    public /* synthetic */ ThemeActivity$1$$ExternalSyntheticLambda0(ThemeActivity.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onItemClick$0(dialogInterface, i);
    }
}
