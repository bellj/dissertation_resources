package org.telegram.ui;

import android.content.Context;
import android.view.View;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivitySmsView$$ExternalSyntheticLambda6 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivitySmsView f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ LoginActivity$LoginActivitySmsView$$ExternalSyntheticLambda6(LoginActivity.LoginActivitySmsView loginActivitySmsView, Context context) {
        this.f$0 = loginActivitySmsView;
        this.f$1 = context;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$7(this.f$1, view);
    }
}
