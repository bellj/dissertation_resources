package org.telegram.ui.Cells;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class DialogsEmptyCell$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public static final /* synthetic */ DialogsEmptyCell$$ExternalSyntheticLambda3 INSTANCE = new DialogsEmptyCell$$ExternalSyntheticLambda3();

    private /* synthetic */ DialogsEmptyCell$$ExternalSyntheticLambda3() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return DialogsEmptyCell.lambda$new$0(view, motionEvent);
    }
}
