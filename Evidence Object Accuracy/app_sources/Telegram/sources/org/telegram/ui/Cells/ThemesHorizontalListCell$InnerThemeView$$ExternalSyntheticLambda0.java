package org.telegram.ui.Cells;

import org.telegram.tgnet.TLObject;
import org.telegram.ui.Cells.ThemesHorizontalListCell;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemesHorizontalListCell$InnerThemeView$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ThemesHorizontalListCell.InnerThemeView f$0;
    public final /* synthetic */ TLObject f$1;

    public /* synthetic */ ThemesHorizontalListCell$InnerThemeView$$ExternalSyntheticLambda0(ThemesHorizontalListCell.InnerThemeView innerThemeView, TLObject tLObject) {
        this.f$0 = innerThemeView;
        this.f$1 = tLObject;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ThemesHorizontalListCell.InnerThemeView.$r8$lambda$Jd1CHk1eX9lMvdg4YIaremuRJYk(this.f$0, this.f$1);
    }
}
