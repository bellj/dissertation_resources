package org.telegram.ui.Cells;

import org.telegram.tgnet.RequestDelegate;
import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC$TL_error;
import org.telegram.ui.Cells.ThemesHorizontalListCell;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemesHorizontalListCell$InnerThemeView$$ExternalSyntheticLambda1 implements RequestDelegate {
    public final /* synthetic */ ThemesHorizontalListCell.InnerThemeView f$0;

    public /* synthetic */ ThemesHorizontalListCell$InnerThemeView$$ExternalSyntheticLambda1(ThemesHorizontalListCell.InnerThemeView innerThemeView) {
        this.f$0 = innerThemeView;
    }

    @Override // org.telegram.tgnet.RequestDelegate
    public final void run(TLObject tLObject, TLRPC$TL_error tLRPC$TL_error) {
        ThemesHorizontalListCell.InnerThemeView.$r8$lambda$fddPqOuVBiJytpNKVrusBtqrcbM(this.f$0, tLObject, tLRPC$TL_error);
    }
}
