package org.telegram.ui.Cells;

import android.content.DialogInterface;
import org.telegram.ui.Cells.AboutLinkCell;

/* loaded from: classes3.dex */
public final /* synthetic */ class AboutLinkCell$3$$ExternalSyntheticLambda1 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ AboutLinkCell.AnonymousClass3 f$0;

    public /* synthetic */ AboutLinkCell$3$$ExternalSyntheticLambda1(AboutLinkCell.AnonymousClass3 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        AboutLinkCell.AnonymousClass3.m672$r8$lambda$_2R1Ja3ZR092BMJdGrMgQdDLdg(this.f$0, dialogInterface);
    }
}
