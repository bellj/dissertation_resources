package org.telegram.ui.Cells;

import java.io.File;
import org.telegram.ui.Cells.ContextLinkCell;

/* loaded from: classes3.dex */
public final /* synthetic */ class ContextLinkCell$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ContextLinkCell.AnonymousClass1 f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ File f$3;
    public final /* synthetic */ boolean f$4;
    public final /* synthetic */ boolean f$5;

    public /* synthetic */ ContextLinkCell$1$$ExternalSyntheticLambda0(ContextLinkCell.AnonymousClass1 r1, int i, String str, File file, boolean z, boolean z2) {
        this.f$0 = r1;
        this.f$1 = i;
        this.f$2 = str;
        this.f$3 = file;
        this.f$4 = z;
        this.f$5 = z2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
    }
}
