package org.telegram.ui.Cells;

import android.animation.ValueAnimator;
import org.telegram.ui.Cells.ThemePreviewMessagesCell;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemePreviewMessagesCell$1$1$1$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ThemePreviewMessagesCell.AnonymousClass1.AnonymousClass1.AnonymousClass1 f$0;

    public /* synthetic */ ThemePreviewMessagesCell$1$1$1$$ExternalSyntheticLambda0(ThemePreviewMessagesCell.AnonymousClass1.AnonymousClass1.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        ThemePreviewMessagesCell.AnonymousClass1.AnonymousClass1.AnonymousClass1.$r8$lambda$pNTo0Usq5gJLiGCWCG4UwqVzeGU(this.f$0, valueAnimator);
    }
}
