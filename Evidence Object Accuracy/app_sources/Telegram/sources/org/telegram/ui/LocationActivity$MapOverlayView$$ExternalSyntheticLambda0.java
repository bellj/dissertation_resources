package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LocationActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LocationActivity$MapOverlayView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ LocationActivity.MapOverlayView f$0;
    public final /* synthetic */ LocationActivity.VenueLocation f$1;

    public /* synthetic */ LocationActivity$MapOverlayView$$ExternalSyntheticLambda0(LocationActivity.MapOverlayView mapOverlayView, LocationActivity.VenueLocation venueLocation) {
        this.f$0 = mapOverlayView;
        this.f$1 = venueLocation;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$addInfoView$1(this.f$1, view);
    }
}
