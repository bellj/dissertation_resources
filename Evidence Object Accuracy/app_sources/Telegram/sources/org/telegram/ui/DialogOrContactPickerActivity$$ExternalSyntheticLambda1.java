package org.telegram.ui;

import android.view.animation.Interpolator;

/* loaded from: classes3.dex */
public final /* synthetic */ class DialogOrContactPickerActivity$$ExternalSyntheticLambda1 implements Interpolator {
    public static final /* synthetic */ DialogOrContactPickerActivity$$ExternalSyntheticLambda1 INSTANCE = new DialogOrContactPickerActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ DialogOrContactPickerActivity$$ExternalSyntheticLambda1() {
    }

    @Override // android.animation.TimeInterpolator
    public final float getInterpolation(float f) {
        return DialogOrContactPickerActivity.m1398$r8$lambda$BNBuvOxpf9s1z0J3_qv_mDYDOg(f);
    }
}
