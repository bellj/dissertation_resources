package org.telegram.ui;

import org.telegram.ui.ActionBar.BaseFragment;
import org.telegram.ui.CalendarActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CalendarActivity$MonthView$2$$ExternalSyntheticLambda4 implements Runnable {
    public final /* synthetic */ CalendarActivity.MonthView.AnonymousClass2 f$0;
    public final /* synthetic */ BaseFragment f$1;
    public final /* synthetic */ CalendarActivity.PeriodDay f$2;

    public /* synthetic */ CalendarActivity$MonthView$2$$ExternalSyntheticLambda4(CalendarActivity.MonthView.AnonymousClass2 r1, BaseFragment baseFragment, CalendarActivity.PeriodDay periodDay) {
        this.f$0 = r1;
        this.f$1 = baseFragment;
        this.f$2 = periodDay;
    }

    @Override // java.lang.Runnable
    public final void run() {
        CalendarActivity.MonthView.AnonymousClass2.$r8$lambda$YJpwmKJK7rADqIGVyEVmZs6Fhpg(this.f$0, this.f$1, this.f$2);
    }
}
