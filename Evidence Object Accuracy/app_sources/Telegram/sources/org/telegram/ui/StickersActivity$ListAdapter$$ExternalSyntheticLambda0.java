package org.telegram.ui;

import android.content.DialogInterface;
import java.util.ArrayList;
import org.telegram.ui.StickersActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class StickersActivity$ListAdapter$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StickersActivity.ListAdapter f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ StickersActivity$ListAdapter$$ExternalSyntheticLambda0(StickersActivity.ListAdapter listAdapter, ArrayList arrayList, int i) {
        this.f$0 = listAdapter;
        this.f$1 = arrayList;
        this.f$2 = i;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$processSelectionMenu$0(this.f$1, this.f$2, dialogInterface, i);
    }
}
