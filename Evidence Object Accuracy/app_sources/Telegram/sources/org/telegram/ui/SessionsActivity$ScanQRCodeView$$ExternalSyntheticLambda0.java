package org.telegram.ui;

import android.view.View;
import org.telegram.ui.SessionsActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class SessionsActivity$ScanQRCodeView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ SessionsActivity.ScanQRCodeView f$0;

    public /* synthetic */ SessionsActivity$ScanQRCodeView$$ExternalSyntheticLambda0(SessionsActivity.ScanQRCodeView scanQRCodeView) {
        this.f$0 = scanQRCodeView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
