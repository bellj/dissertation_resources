package org.telegram.ui;

import org.telegram.ui.CacheControlActivity;
import org.telegram.ui.Components.SlideChooseView;

/* loaded from: classes3.dex */
public final /* synthetic */ class CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0 implements SlideChooseView.Callback {
    public static final /* synthetic */ CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0 INSTANCE = new CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0();

    private /* synthetic */ CacheControlActivity$ListAdapter$$ExternalSyntheticLambda0() {
    }

    @Override // org.telegram.ui.Components.SlideChooseView.Callback
    public final void onOptionSelected(int i) {
        CacheControlActivity.ListAdapter.$r8$lambda$w8jhfR6WU0k1_4Jjlofrf6Tvvo8(i);
    }

    @Override // org.telegram.ui.Components.SlideChooseView.Callback
    public /* synthetic */ void onTouchEnd() {
        SlideChooseView.Callback.CC.$default$onTouchEnd(this);
    }
}
