package org.telegram.ui;

import org.telegram.ui.CallLogActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CallLogActivity$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CallLogActivity.AnonymousClass2 f$0;
    public final /* synthetic */ CallLogActivity.CallLogRow f$1;

    public /* synthetic */ CallLogActivity$2$$ExternalSyntheticLambda0(CallLogActivity.AnonymousClass2 r1, CallLogActivity.CallLogRow callLogRow) {
        this.f$0 = r1;
        this.f$1 = callLogRow;
    }

    @Override // java.lang.Runnable
    public final void run() {
        CallLogActivity.AnonymousClass2.m662$r8$lambda$e0uiZkgemrmp7bj51jRN57XoN8(this.f$0, this.f$1);
    }
}
