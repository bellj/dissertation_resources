package org.telegram.ui;

import java.util.List;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$PhoneView$$ExternalSyntheticLambda12 implements Runnable {
    public final /* synthetic */ LoginActivity.PhoneView f$0;
    public final /* synthetic */ List f$1;

    public /* synthetic */ LoginActivity$PhoneView$$ExternalSyntheticLambda12(LoginActivity.PhoneView phoneView, List list) {
        this.f$0 = phoneView;
        this.f$1 = list;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$fillNumber$21(this.f$1);
    }
}
