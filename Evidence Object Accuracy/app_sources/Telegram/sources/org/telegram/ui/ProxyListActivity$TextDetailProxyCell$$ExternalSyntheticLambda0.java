package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ProxyListActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProxyListActivity$TextDetailProxyCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ProxyListActivity.TextDetailProxyCell f$0;

    public /* synthetic */ ProxyListActivity$TextDetailProxyCell$$ExternalSyntheticLambda0(ProxyListActivity.TextDetailProxyCell textDetailProxyCell) {
        this.f$0 = textDetailProxyCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
