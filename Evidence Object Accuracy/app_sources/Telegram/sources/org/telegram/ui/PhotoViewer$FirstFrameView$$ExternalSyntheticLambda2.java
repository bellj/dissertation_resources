package org.telegram.ui;

import android.graphics.Bitmap;
import org.telegram.ui.PhotoViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoViewer$FirstFrameView$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ PhotoViewer.FirstFrameView f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ Bitmap f$2;

    public /* synthetic */ PhotoViewer$FirstFrameView$$ExternalSyntheticLambda2(PhotoViewer.FirstFrameView firstFrameView, int i, Bitmap bitmap) {
        this.f$0 = firstFrameView;
        this.f$1 = i;
        this.f$2 = bitmap;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$checkFromPlayer$0(this.f$1, this.f$2);
    }
}
