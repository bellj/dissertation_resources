package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CallLogActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CallLogActivity$CallCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CallLogActivity.CallCell f$0;

    public /* synthetic */ CallLogActivity$CallCell$$ExternalSyntheticLambda0(CallLogActivity.CallCell callCell) {
        this.f$0 = callCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CallLogActivity.CallCell.m663$r8$lambda$KjTh4PYwuxuKYeQrDTw7q4R0UQ(this.f$0, view);
    }
}
