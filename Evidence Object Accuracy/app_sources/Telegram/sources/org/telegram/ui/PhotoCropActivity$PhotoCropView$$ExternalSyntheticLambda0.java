package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;
import org.telegram.ui.PhotoCropActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoCropActivity$PhotoCropView$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public final /* synthetic */ PhotoCropActivity.PhotoCropView f$0;

    public /* synthetic */ PhotoCropActivity$PhotoCropView$$ExternalSyntheticLambda0(PhotoCropActivity.PhotoCropView photoCropView) {
        this.f$0 = photoCropView;
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.f$0.lambda$init$0(view, motionEvent);
    }
}
