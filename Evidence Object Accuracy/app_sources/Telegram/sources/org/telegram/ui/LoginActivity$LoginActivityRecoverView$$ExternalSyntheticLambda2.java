package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityRecoverView f$0;

    public /* synthetic */ LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda2(LoginActivity.LoginActivityRecoverView loginActivityRecoverView) {
        this.f$0 = loginActivityRecoverView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$4(view);
    }
}
