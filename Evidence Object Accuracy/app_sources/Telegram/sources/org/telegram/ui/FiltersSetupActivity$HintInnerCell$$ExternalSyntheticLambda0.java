package org.telegram.ui;

import android.view.View;
import org.telegram.ui.FiltersSetupActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class FiltersSetupActivity$HintInnerCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ FiltersSetupActivity.HintInnerCell f$0;

    public /* synthetic */ FiltersSetupActivity$HintInnerCell$$ExternalSyntheticLambda0(FiltersSetupActivity.HintInnerCell hintInnerCell) {
        this.f$0 = hintInnerCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
