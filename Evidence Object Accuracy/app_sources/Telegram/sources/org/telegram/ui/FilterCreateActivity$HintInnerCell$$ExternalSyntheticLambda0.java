package org.telegram.ui;

import android.view.View;
import org.telegram.ui.FilterCreateActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class FilterCreateActivity$HintInnerCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ FilterCreateActivity.HintInnerCell f$0;

    public /* synthetic */ FilterCreateActivity$HintInnerCell$$ExternalSyntheticLambda0(FilterCreateActivity.HintInnerCell hintInnerCell) {
        this.f$0 = hintInnerCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
