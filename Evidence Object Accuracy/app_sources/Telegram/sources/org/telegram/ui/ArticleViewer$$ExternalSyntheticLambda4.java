package org.telegram.ui;

import android.view.View;
import android.view.WindowInsets;

/* loaded from: classes3.dex */
public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda4 implements View.OnApplyWindowInsetsListener {
    public static final /* synthetic */ ArticleViewer$$ExternalSyntheticLambda4 INSTANCE = new ArticleViewer$$ExternalSyntheticLambda4();

    private /* synthetic */ ArticleViewer$$ExternalSyntheticLambda4() {
    }

    @Override // android.view.View.OnApplyWindowInsetsListener
    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return ArticleViewer.$r8$lambda$jsHM7d39JyDsLTKMOEByaGUqSic(view, windowInsets);
    }
}
