package org.telegram.ui;

import android.view.View;
import org.telegram.ui.Components.OutlineTextContainerView;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityNewPasswordView$$ExternalSyntheticLambda3 implements View.OnFocusChangeListener {
    public final /* synthetic */ OutlineTextContainerView f$0;

    public /* synthetic */ LoginActivity$LoginActivityNewPasswordView$$ExternalSyntheticLambda3(OutlineTextContainerView outlineTextContainerView) {
        this.f$0 = outlineTextContainerView;
    }

    @Override // android.view.View.OnFocusChangeListener
    public final void onFocusChange(View view, boolean z) {
        LoginActivity.LoginActivityNewPasswordView.lambda$new$0(this.f$0, view, z);
    }
}
