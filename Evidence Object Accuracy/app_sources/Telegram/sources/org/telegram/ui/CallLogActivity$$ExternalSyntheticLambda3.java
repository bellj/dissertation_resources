package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class CallLogActivity$$ExternalSyntheticLambda3 implements View.OnTouchListener {
    public static final /* synthetic */ CallLogActivity$$ExternalSyntheticLambda3 INSTANCE = new CallLogActivity$$ExternalSyntheticLambda3();

    private /* synthetic */ CallLogActivity$$ExternalSyntheticLambda3() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return CallLogActivity.$r8$lambda$eZ0Kp0kBy9R5VaLHzktibQGtU38(view, motionEvent);
    }
}
