package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes3.dex */
public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda2 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda2 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda2();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda2() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Setter
    public final void set(Object obj, float f) {
        OutlineTextContainerView.m1172$r8$lambda$Iy0_pORNFqIjMB8UrkspSTChTM((OutlineTextContainerView) obj, f);
    }
}
