package org.telegram.ui.Components;

import androidx.dynamicanimation.animation.DynamicAnimation;
import org.telegram.ui.Components.ChatAttachAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlert$17$$ExternalSyntheticLambda1 implements DynamicAnimation.OnAnimationUpdateListener {
    public final /* synthetic */ ChatAttachAlert.AnonymousClass17 f$0;

    public /* synthetic */ ChatAttachAlert$17$$ExternalSyntheticLambda1(ChatAttachAlert.AnonymousClass17 r1) {
        this.f$0 = r1;
    }

    @Override // androidx.dynamicanimation.animation.DynamicAnimation.OnAnimationUpdateListener
    public final void onAnimationUpdate(DynamicAnimation dynamicAnimation, float f, float f2) {
        this.f$0.lambda$onAnimationEnd$0(dynamicAnimation, f, f2);
    }
}
