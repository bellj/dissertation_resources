package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.ThemeEditorView;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemeEditorView$EditorAlert$SearchField$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ThemeEditorView.EditorAlert.SearchField f$0;

    public /* synthetic */ ThemeEditorView$EditorAlert$SearchField$$ExternalSyntheticLambda0(ThemeEditorView.EditorAlert.SearchField searchField) {
        this.f$0 = searchField;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
