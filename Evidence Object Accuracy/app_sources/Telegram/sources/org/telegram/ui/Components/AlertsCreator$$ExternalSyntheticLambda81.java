package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda81 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda81 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda81();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda81() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createSoundFrequencyPickerDialog$69(view, motionEvent);
    }
}
