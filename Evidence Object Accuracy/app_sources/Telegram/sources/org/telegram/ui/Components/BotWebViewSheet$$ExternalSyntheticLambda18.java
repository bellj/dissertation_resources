package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda18 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda18 INSTANCE = new BotWebViewSheet$$ExternalSyntheticLambda18();

    private /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda18() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Getter
    public final float get(Object obj) {
        return BotWebViewSheet.$r8$lambda$OzktqmusNGHkZVwSdYetA7GCFNY((BotWebViewSheet) obj);
    }
}
