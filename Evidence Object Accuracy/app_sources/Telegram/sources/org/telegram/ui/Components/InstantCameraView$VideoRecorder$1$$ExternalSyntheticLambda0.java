package org.telegram.ui.Components;

import org.telegram.ui.Components.InstantCameraView;

/* loaded from: classes3.dex */
public final /* synthetic */ class InstantCameraView$VideoRecorder$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ InstantCameraView.VideoRecorder.AnonymousClass1 f$0;
    public final /* synthetic */ double f$1;

    public /* synthetic */ InstantCameraView$VideoRecorder$1$$ExternalSyntheticLambda0(InstantCameraView.VideoRecorder.AnonymousClass1 r1, double d) {
        this.f$0 = r1;
        this.f$1 = d;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
