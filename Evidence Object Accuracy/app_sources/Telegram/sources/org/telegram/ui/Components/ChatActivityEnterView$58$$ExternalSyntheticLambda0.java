package org.telegram.ui.Components;

import android.content.DialogInterface;
import org.telegram.ui.Components.ChatActivityEnterView;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivityEnterView$58$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ChatActivityEnterView.AnonymousClass58 f$0;

    public /* synthetic */ ChatActivityEnterView$58$$ExternalSyntheticLambda0(ChatActivityEnterView.AnonymousClass58 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onClearEmojiRecent$1(dialogInterface, i);
    }
}
