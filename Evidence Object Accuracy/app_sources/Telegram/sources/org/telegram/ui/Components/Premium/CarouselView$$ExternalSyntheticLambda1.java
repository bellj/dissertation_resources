package org.telegram.ui.Components.Premium;

import android.view.animation.Interpolator;

/* loaded from: classes3.dex */
public final /* synthetic */ class CarouselView$$ExternalSyntheticLambda1 implements Interpolator {
    public static final /* synthetic */ CarouselView$$ExternalSyntheticLambda1 INSTANCE = new CarouselView$$ExternalSyntheticLambda1();

    private /* synthetic */ CarouselView$$ExternalSyntheticLambda1() {
    }

    @Override // android.animation.TimeInterpolator
    public final float getInterpolation(float f) {
        return CarouselView.$r8$lambda$p7pzi3taq3VbfV64Br_tW5Qcbow(f);
    }
}
