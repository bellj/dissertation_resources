package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class EmbedBottomSheet$$ExternalSyntheticLambda4 implements View.OnTouchListener {
    public static final /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda4 INSTANCE = new EmbedBottomSheet$$ExternalSyntheticLambda4();

    private /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda4() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return EmbedBottomSheet.lambda$new$0(view, motionEvent);
    }
}
