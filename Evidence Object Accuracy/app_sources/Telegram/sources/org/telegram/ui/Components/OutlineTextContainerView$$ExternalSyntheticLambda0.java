package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/* loaded from: classes3.dex */
public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda0 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda0 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda0();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda0() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Getter
    public final float get(Object obj) {
        return OutlineTextContainerView.$r8$lambda$6APi0s4t84SUjNqHMh_kGWu4z5k((OutlineTextContainerView) obj);
    }
}
