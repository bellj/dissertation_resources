package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.ReactionsContainerLayout;

/* loaded from: classes3.dex */
public final /* synthetic */ class ReactionsContainerLayout$4$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ReactionsContainerLayout.AnonymousClass4 f$0;

    public /* synthetic */ ReactionsContainerLayout$4$$ExternalSyntheticLambda0(ReactionsContainerLayout.AnonymousClass4 r1) {
        this.f$0 = r1;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
