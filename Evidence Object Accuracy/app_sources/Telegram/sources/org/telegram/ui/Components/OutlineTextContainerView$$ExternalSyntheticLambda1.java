package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/* loaded from: classes3.dex */
public final /* synthetic */ class OutlineTextContainerView$$ExternalSyntheticLambda1 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda1 INSTANCE = new OutlineTextContainerView$$ExternalSyntheticLambda1();

    private /* synthetic */ OutlineTextContainerView$$ExternalSyntheticLambda1() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Getter
    public final float get(Object obj) {
        return OutlineTextContainerView.m1171$r8$lambda$GVuFq56zRyCxQ0KUn9_xj1FMqs((OutlineTextContainerView) obj);
    }
}
