package org.telegram.ui.Components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.os.Build;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.core.math.MathUtils;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.FloatPropertyCompat;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;
import java.util.ArrayList;
import java.util.List;
import org.telegram.messenger.AndroidUtilities;
import org.telegram.messenger.ApplicationLoader;
import org.telegram.messenger.R;
import org.telegram.ui.ActionBar.Theme;
import org.telegram.ui.Components.GestureDetectorFixDoubleTap;
import org.telegram.ui.Components.VideoForwardDrawable;
import org.telegram.ui.LaunchActivity;
import org.telegram.ui.PhotoViewer;

/* loaded from: classes3.dex */
public class PipVideoOverlay {
    private static final FloatPropertyCompat<PipVideoOverlay> PIP_X_PROPERTY = new SimpleFloatPropertyCompat("pipX", PipVideoOverlay$$ExternalSyntheticLambda11.INSTANCE, PipVideoOverlay$$ExternalSyntheticLambda13.INSTANCE);
    private static final FloatPropertyCompat<PipVideoOverlay> PIP_Y_PROPERTY = new SimpleFloatPropertyCompat("pipY", PipVideoOverlay$$ExternalSyntheticLambda10.INSTANCE, PipVideoOverlay$$ExternalSyntheticLambda12.INSTANCE);
    @SuppressLint({"StaticFieldLeak"})
    private static PipVideoOverlay instance = new PipVideoOverlay();
    private Float aspectRatio;
    private float bufferProgress;
    private boolean canLongClick;
    private View consumingChild;
    private FrameLayout contentFrameLayout;
    private ViewGroup contentView;
    private ValueAnimator controlsAnimator;
    private FrameLayout controlsView;
    private Runnable dismissControlsCallback = new Runnable() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda9
        @Override // java.lang.Runnable
        public final void run() {
            PipVideoOverlay.this.lambda$new$5();
        }
    };
    private GestureDetectorFixDoubleTap gestureDetector;
    private View innerView;
    private boolean isDismissing;
    private boolean isScrollDisallowed;
    private boolean isScrolling;
    private boolean isShowingControls;
    private boolean isVideoCompleted;
    private boolean isVisible;
    private Runnable longClickCallback = new Runnable() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda6
        @Override // java.lang.Runnable
        public final void run() {
            PipVideoOverlay.this.onLongClick();
        }
    };
    private float[] longClickStartPoint = new float[2];
    private int mVideoHeight;
    private int mVideoWidth;
    private float maxScaleFactor = 1.4f;
    private float minScaleFactor = 0.75f;
    private boolean onSideToDismiss;
    private EmbedBottomSheet parentSheet;
    private PhotoViewer photoViewer;
    private PipConfig pipConfig;
    private int pipHeight;
    private int pipWidth;
    private float pipX;
    private SpringAnimation pipXSpring;
    private float pipY;
    private SpringAnimation pipYSpring;
    private ImageView playPauseButton;
    private boolean postedDismissControls;
    private Runnable progressRunnable = new Runnable() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda8
        @Override // java.lang.Runnable
        public final void run() {
            PipVideoOverlay.this.lambda$new$4();
        }
    };
    private float scaleFactor = 1.0f;
    private ScaleGestureDetector scaleGestureDetector;
    private VideoForwardDrawable videoForwardDrawable = new VideoForwardDrawable(false);
    private float videoProgress;
    private VideoProgressView videoProgressView;
    private WindowManager.LayoutParams windowLayoutParams;
    private WindowManager windowManager;

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$static$1(PipVideoOverlay pipVideoOverlay, float f) {
        WindowManager.LayoutParams layoutParams = pipVideoOverlay.windowLayoutParams;
        pipVideoOverlay.pipX = f;
        layoutParams.x = (int) f;
        try {
            pipVideoOverlay.windowManager.updateViewLayout(pipVideoOverlay.contentView, layoutParams);
        } catch (IllegalArgumentException unused) {
            pipVideoOverlay.pipXSpring.cancel();
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$static$3(PipVideoOverlay pipVideoOverlay, float f) {
        WindowManager.LayoutParams layoutParams = pipVideoOverlay.windowLayoutParams;
        pipVideoOverlay.pipY = f;
        layoutParams.y = (int) f;
        try {
            pipVideoOverlay.windowManager.updateViewLayout(pipVideoOverlay.contentView, layoutParams);
        } catch (IllegalArgumentException unused) {
            pipVideoOverlay.pipYSpring.cancel();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$new$4() {
        VideoPlayer videoPlayer;
        PhotoViewer photoViewer = this.photoViewer;
        if (photoViewer != null && (videoPlayer = photoViewer.getVideoPlayer()) != null) {
            this.videoProgress = ((float) videoPlayer.getCurrentPosition()) / ((float) videoPlayer.getDuration());
            if (this.photoViewer == null) {
                this.bufferProgress = ((float) videoPlayer.getBufferedPosition()) / ((float) videoPlayer.getDuration());
            }
            this.videoProgressView.invalidate();
            AndroidUtilities.runOnUIThread(this.progressRunnable, 500);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$new$5() {
        PhotoViewer photoViewer = this.photoViewer;
        if (photoViewer == null || photoViewer.getVideoPlayerRewinder().rewindCount <= 0) {
            this.isShowingControls = false;
            toggleControls(false);
            this.postedDismissControls = false;
            return;
        }
        AndroidUtilities.runOnUIThread(this.dismissControlsCallback, 1500);
    }

    public static void onRewindCanceled() {
        instance.onRewindCanceledInternal();
    }

    private void onRewindCanceledInternal() {
        this.videoForwardDrawable.setShowing(false);
    }

    public static void onUpdateRewindProgressUi(long j, float f, boolean z) {
        instance.onUpdateRewindProgressUiInternal(j, f, z);
    }

    /* access modifiers changed from: private */
    public void onUpdateRewindProgressUiInternal(long j, float f, boolean z) {
        this.videoForwardDrawable.setTime(0);
        if (z) {
            this.videoProgress = f;
            VideoProgressView videoProgressView = this.videoProgressView;
            if (videoProgressView != null) {
                videoProgressView.invalidate();
            }
            FrameLayout frameLayout = this.controlsView;
            if (frameLayout != null) {
                frameLayout.invalidate();
            }
        }
    }

    public static void onRewindStart(boolean z) {
        instance.onRewindStartInternal(z);
    }

    private void onRewindStartInternal(boolean z) {
        this.videoForwardDrawable.setOneShootAnimation(false);
        this.videoForwardDrawable.setLeftSide(!z);
        this.videoForwardDrawable.setShowing(true);
        VideoProgressView videoProgressView = this.videoProgressView;
        if (videoProgressView != null) {
            videoProgressView.invalidate();
        }
        FrameLayout frameLayout = this.controlsView;
        if (frameLayout != null) {
            frameLayout.invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onLongClick() {
        PhotoViewer photoViewer = this.photoViewer;
        if (photoViewer != null && photoViewer.getVideoPlayer() != null && !this.isDismissing && !this.isVideoCompleted && !this.isScrolling && !this.scaleGestureDetector.isInProgress() && this.canLongClick) {
            VideoPlayer videoPlayer = this.photoViewer.getVideoPlayer();
            boolean z = false;
            if (this.longClickStartPoint[0] >= ((float) getSuggestedWidth()) * this.scaleFactor * 0.5f) {
                z = true;
            }
            long currentPosition = videoPlayer.getCurrentPosition();
            long duration = videoPlayer.getDuration();
            if (currentPosition != -9223372036854775807L && duration >= 15000) {
                this.photoViewer.getVideoPlayerRewinder().startRewind(videoPlayer, z, this.photoViewer.getCurrentVideoSpeed());
                if (!this.isShowingControls) {
                    this.isShowingControls = true;
                    toggleControls(true);
                    if (!this.postedDismissControls) {
                        AndroidUtilities.runOnUIThread(this.dismissControlsCallback, 1500);
                        this.postedDismissControls = true;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public PipConfig getPipConfig() {
        if (this.pipConfig == null) {
            Point point = AndroidUtilities.displaySize;
            this.pipConfig = new PipConfig(point.x, point.y);
        }
        return this.pipConfig;
    }

    public static boolean isVisible() {
        return instance.isVisible;
    }

    /* access modifiers changed from: private */
    public int getSuggestedWidth() {
        return getSuggestedWidth(getRatio());
    }

    private static int getSuggestedWidth(float f) {
        float min;
        float f2;
        if (f >= 1.0f) {
            Point point = AndroidUtilities.displaySize;
            min = (float) Math.min(point.x, point.y);
            f2 = 0.35f;
        } else {
            Point point2 = AndroidUtilities.displaySize;
            min = (float) Math.min(point2.x, point2.y);
            f2 = 0.6f;
        }
        return (int) (min * f2);
    }

    /* access modifiers changed from: private */
    public int getSuggestedHeight() {
        return getSuggestedHeight(getRatio());
    }

    private static int getSuggestedHeight(float f) {
        return (int) (((float) getSuggestedWidth(f)) * f);
    }

    private float getRatio() {
        if (this.aspectRatio == null) {
            this.aspectRatio = Float.valueOf(((float) this.mVideoHeight) / ((float) this.mVideoWidth));
            Point point = AndroidUtilities.displaySize;
            this.maxScaleFactor = ((float) (Math.min(point.x, point.y) - AndroidUtilities.dp(32.0f))) / ((float) getSuggestedWidth());
            this.videoForwardDrawable.setPlayScaleFactor(this.aspectRatio.floatValue() < 1.0f ? 0.6f : 0.45f);
        }
        return this.aspectRatio.floatValue();
    }

    /* access modifiers changed from: private */
    public void toggleControls(boolean z) {
        float[] fArr = new float[2];
        float f = 0.0f;
        fArr[0] = z ? 0.0f : 1.0f;
        if (z) {
            f = 1.0f;
        }
        fArr[1] = f;
        ValueAnimator duration = ValueAnimator.ofFloat(fArr).setDuration(200L);
        this.controlsAnimator = duration;
        duration.setInterpolator(CubicBezierInterpolator.DEFAULT);
        this.controlsAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda0
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                PipVideoOverlay.this.lambda$toggleControls$6(valueAnimator);
            }
        });
        this.controlsAnimator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.Components.PipVideoOverlay.1
            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                PipVideoOverlay.this.controlsAnimator = null;
            }
        });
        this.controlsAnimator.start();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$toggleControls$6(ValueAnimator valueAnimator) {
        this.controlsView.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    public static void dimissAndDestroy() {
        PipVideoOverlay pipVideoOverlay = instance;
        EmbedBottomSheet embedBottomSheet = pipVideoOverlay.parentSheet;
        if (embedBottomSheet != null) {
            embedBottomSheet.destroy();
        } else {
            PhotoViewer photoViewer = pipVideoOverlay.photoViewer;
            if (photoViewer != null) {
                photoViewer.destroyPhotoViewer();
            }
        }
        dismiss();
    }

    public static void dismiss() {
        dismiss(false);
    }

    public static void dismiss(boolean z) {
        instance.dismissInternal(z);
    }

    private void dismissInternal(boolean z) {
        if (!this.isDismissing) {
            this.isDismissing = true;
            ValueAnimator valueAnimator = this.controlsAnimator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            if (this.postedDismissControls) {
                AndroidUtilities.cancelRunOnUIThread(this.dismissControlsCallback);
                this.postedDismissControls = false;
            }
            SpringAnimation springAnimation = this.pipXSpring;
            if (springAnimation != null) {
                springAnimation.cancel();
                this.pipYSpring.cancel();
            }
            if (z) {
                AndroidUtilities.runOnUIThread(new Runnable() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda7
                    @Override // java.lang.Runnable
                    public final void run() {
                        PipVideoOverlay.this.onDismissedInternal();
                    }
                }, 100);
                return;
            }
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.setDuration(250L);
            animatorSet.setInterpolator(CubicBezierInterpolator.DEFAULT);
            animatorSet.playTogether(ObjectAnimator.ofFloat(this.contentView, View.ALPHA, 0.0f), ObjectAnimator.ofFloat(this.contentView, View.SCALE_X, 0.1f), ObjectAnimator.ofFloat(this.contentView, View.SCALE_Y, 0.1f));
            animatorSet.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.Components.PipVideoOverlay.2
                @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    PipVideoOverlay.this.onDismissedInternal();
                }
            });
            animatorSet.start();
        }
    }

    /* access modifiers changed from: private */
    public void onDismissedInternal() {
        try {
            if (this.controlsView.getParent() != null) {
                this.windowManager.removeViewImmediate(this.contentView);
            }
        } catch (IllegalArgumentException unused) {
        }
        this.videoProgressView = null;
        this.innerView = null;
        this.photoViewer = null;
        this.parentSheet = null;
        this.consumingChild = null;
        this.isScrolling = false;
        this.isVisible = false;
        this.isDismissing = false;
        this.canLongClick = false;
        cancelRewind();
        AndroidUtilities.cancelRunOnUIThread(this.longClickCallback);
    }

    /* access modifiers changed from: private */
    public void cancelRewind() {
        PhotoViewer photoViewer = this.photoViewer;
        if (photoViewer != null && photoViewer.getVideoPlayerRewinder().rewindCount > 0) {
            this.photoViewer.getVideoPlayerRewinder().cancelRewind();
        }
    }

    public static void updatePlayButton() {
        instance.updatePlayButtonInternal();
    }

    private void updatePlayButtonInternal() {
        VideoPlayer videoPlayer;
        PhotoViewer photoViewer = this.photoViewer;
        if (photoViewer != null && (videoPlayer = photoViewer.getVideoPlayer()) != null && this.playPauseButton != null) {
            AndroidUtilities.cancelRunOnUIThread(this.progressRunnable);
            if (videoPlayer.isPlaying()) {
                this.playPauseButton.setImageResource(R.drawable.pip_pause_large);
                AndroidUtilities.runOnUIThread(this.progressRunnable, 500);
            } else if (this.isVideoCompleted) {
                this.playPauseButton.setImageResource(R.drawable.pip_replay_large);
            } else {
                this.playPauseButton.setImageResource(R.drawable.pip_play_large);
            }
        }
    }

    public static void onVideoCompleted() {
        instance.onVideoCompletedInternal();
    }

    private void onVideoCompletedInternal() {
        VideoProgressView videoProgressView;
        if (this.isVisible && (videoProgressView = this.videoProgressView) != null) {
            this.isVideoCompleted = true;
            this.videoProgress = 0.0f;
            this.bufferProgress = 0.0f;
            if (videoProgressView != null) {
                videoProgressView.invalidate();
            }
            updatePlayButtonInternal();
            AndroidUtilities.cancelRunOnUIThread(this.progressRunnable);
            if (!this.isShowingControls) {
                toggleControls(true);
                AndroidUtilities.cancelRunOnUIThread(this.dismissControlsCallback);
            }
        }
    }

    public static void setBufferedProgress(float f) {
        PipVideoOverlay pipVideoOverlay = instance;
        pipVideoOverlay.bufferProgress = f;
        VideoProgressView videoProgressView = pipVideoOverlay.videoProgressView;
        if (videoProgressView != null) {
            videoProgressView.invalidate();
        }
    }

    public static void setParentSheet(EmbedBottomSheet embedBottomSheet) {
        instance.parentSheet = embedBottomSheet;
    }

    public static void setPhotoViewer(PhotoViewer photoViewer) {
        PipVideoOverlay pipVideoOverlay = instance;
        pipVideoOverlay.photoViewer = photoViewer;
        pipVideoOverlay.updatePlayButtonInternal();
    }

    public static Rect getPipRect(boolean z, float f) {
        Rect rect = new Rect();
        float f2 = 1.0f / f;
        PipVideoOverlay pipVideoOverlay = instance;
        if (!pipVideoOverlay.isVisible || z) {
            float pipX = pipVideoOverlay.getPipConfig().getPipX();
            float pipY = instance.getPipConfig().getPipY();
            float scaleFactor = instance.getPipConfig().getScaleFactor();
            rect.width = ((float) getSuggestedWidth(f2)) * scaleFactor;
            rect.height = ((float) getSuggestedHeight(f2)) * scaleFactor;
            if (pipX != -1.0f) {
                float f3 = rect.width;
                float f4 = pipX + (f3 / 2.0f);
                int i = AndroidUtilities.displaySize.x;
                rect.x = f4 >= ((float) i) / 2.0f ? (((float) i) - f3) - ((float) AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f);
            } else {
                rect.x = (((float) AndroidUtilities.displaySize.x) - rect.width) - ((float) AndroidUtilities.dp(16.0f));
            }
            if (pipY != -1.0f) {
                rect.y = MathUtils.clamp(pipY, (float) AndroidUtilities.dp(16.0f), ((float) (AndroidUtilities.displaySize.y - AndroidUtilities.dp(16.0f))) - rect.height) + ((float) AndroidUtilities.statusBarHeight);
            } else {
                rect.y = (float) (AndroidUtilities.dp(16.0f) + AndroidUtilities.statusBarHeight);
            }
            return rect;
        }
        rect.x = pipVideoOverlay.pipX;
        rect.y = pipVideoOverlay.pipY + ((float) AndroidUtilities.statusBarHeight);
        PipVideoOverlay pipVideoOverlay2 = instance;
        rect.width = (float) pipVideoOverlay2.pipWidth;
        rect.height = (float) pipVideoOverlay2.pipHeight;
        return rect;
    }

    public static boolean show(boolean z, Activity activity, View view, int i, int i2) {
        return show(z, activity, view, i, i2, false);
    }

    public static boolean show(boolean z, Activity activity, View view, int i, int i2, boolean z2) {
        return instance.showInternal(z, activity, view, i, i2, z2);
    }

    private boolean showInternal(boolean z, Activity activity, View view, int i, int i2, boolean z2) {
        Context context;
        if (this.isVisible) {
            return false;
        }
        this.isVisible = true;
        this.mVideoWidth = i;
        this.mVideoHeight = i2;
        this.aspectRatio = null;
        float pipX = getPipConfig().getPipX();
        float pipY = getPipConfig().getPipY();
        this.scaleFactor = getPipConfig().getScaleFactor();
        this.pipWidth = (int) (((float) getSuggestedWidth()) * this.scaleFactor);
        this.pipHeight = (int) (((float) getSuggestedHeight()) * this.scaleFactor);
        this.isShowingControls = false;
        this.pipXSpring = new SpringAnimation(this, PIP_X_PROPERTY).setSpring(new SpringForce().setDampingRatio(0.75f).setStiffness(650.0f)).addEndListener(new DynamicAnimation.OnAnimationEndListener() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda4
            @Override // androidx.dynamicanimation.animation.DynamicAnimation.OnAnimationEndListener
            public final void onAnimationEnd(DynamicAnimation dynamicAnimation, boolean z3, float f, float f2) {
                PipVideoOverlay.this.lambda$showInternal$7(dynamicAnimation, z3, f, f2);
            }
        });
        this.pipYSpring = new SpringAnimation(this, PIP_Y_PROPERTY).setSpring(new SpringForce().setDampingRatio(0.75f).setStiffness(650.0f)).addEndListener(new DynamicAnimation.OnAnimationEndListener() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda5
            @Override // androidx.dynamicanimation.animation.DynamicAnimation.OnAnimationEndListener
            public final void onAnimationEnd(DynamicAnimation dynamicAnimation, boolean z3, float f, float f2) {
                PipVideoOverlay.this.lambda$showInternal$8(dynamicAnimation, z3, f, f2);
            }
        });
        Context context2 = ApplicationLoader.applicationContext;
        final int scaledTouchSlop = ViewConfiguration.get(context2).getScaledTouchSlop();
        ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(context2, new ScaleGestureDetector.OnScaleGestureListener() { // from class: org.telegram.ui.Components.PipVideoOverlay.3
            @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
            public boolean onScale(ScaleGestureDetector scaleGestureDetector2) {
                PipVideoOverlay pipVideoOverlay = PipVideoOverlay.this;
                pipVideoOverlay.scaleFactor = MathUtils.clamp(pipVideoOverlay.scaleFactor * scaleGestureDetector2.getScaleFactor(), PipVideoOverlay.this.minScaleFactor, PipVideoOverlay.this.maxScaleFactor);
                PipVideoOverlay pipVideoOverlay2 = PipVideoOverlay.this;
                pipVideoOverlay2.pipWidth = (int) (((float) pipVideoOverlay2.getSuggestedWidth()) * PipVideoOverlay.this.scaleFactor);
                PipVideoOverlay pipVideoOverlay3 = PipVideoOverlay.this;
                pipVideoOverlay3.pipHeight = (int) (((float) pipVideoOverlay3.getSuggestedHeight()) * PipVideoOverlay.this.scaleFactor);
                AndroidUtilities.runOnUIThread(new PipVideoOverlay$3$$ExternalSyntheticLambda0(this));
                float focusX = scaleGestureDetector2.getFocusX();
                int i3 = AndroidUtilities.displaySize.x;
                float dp = focusX >= ((float) i3) / 2.0f ? (float) ((i3 - PipVideoOverlay.this.pipWidth) - AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f);
                if (!PipVideoOverlay.this.pipXSpring.isRunning()) {
                    PipVideoOverlay.this.pipXSpring.setStartValue(PipVideoOverlay.this.pipX).getSpring().setFinalPosition(dp);
                } else {
                    PipVideoOverlay.this.pipXSpring.getSpring().setFinalPosition(dp);
                }
                PipVideoOverlay.this.pipXSpring.start();
                float clamp = MathUtils.clamp(scaleGestureDetector2.getFocusY() - (((float) PipVideoOverlay.this.pipHeight) / 2.0f), (float) AndroidUtilities.dp(16.0f), (float) ((AndroidUtilities.displaySize.y - PipVideoOverlay.this.pipHeight) - AndroidUtilities.dp(16.0f)));
                if (!PipVideoOverlay.this.pipYSpring.isRunning()) {
                    PipVideoOverlay.this.pipYSpring.setStartValue(PipVideoOverlay.this.pipY).getSpring().setFinalPosition(clamp);
                } else {
                    PipVideoOverlay.this.pipYSpring.getSpring().setFinalPosition(clamp);
                }
                PipVideoOverlay.this.pipYSpring.start();
                return true;
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onScale$0() {
                PipVideoOverlay.this.contentView.invalidate();
                PipVideoOverlay.this.contentFrameLayout.requestLayout();
            }

            @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
            public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector2) {
                if (PipVideoOverlay.this.isScrolling) {
                    PipVideoOverlay.this.isScrolling = false;
                    PipVideoOverlay.this.canLongClick = false;
                    PipVideoOverlay.this.cancelRewind();
                    AndroidUtilities.cancelRunOnUIThread(PipVideoOverlay.this.longClickCallback);
                }
                PipVideoOverlay.this.isScrollDisallowed = true;
                PipVideoOverlay.this.windowLayoutParams.width = (int) (((float) PipVideoOverlay.this.getSuggestedWidth()) * PipVideoOverlay.this.maxScaleFactor);
                PipVideoOverlay.this.windowLayoutParams.height = (int) (((float) PipVideoOverlay.this.getSuggestedHeight()) * PipVideoOverlay.this.maxScaleFactor);
                PipVideoOverlay.this.windowManager.updateViewLayout(PipVideoOverlay.this.contentView, PipVideoOverlay.this.windowLayoutParams);
                return true;
            }

            @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
            public void onScaleEnd(ScaleGestureDetector scaleGestureDetector2) {
                if (PipVideoOverlay.this.pipXSpring.isRunning() || PipVideoOverlay.this.pipYSpring.isRunning()) {
                    final ArrayList arrayList = new ArrayList();
                    AnonymousClass1 r0 = new DynamicAnimation.OnAnimationEndListener() { // from class: org.telegram.ui.Components.PipVideoOverlay.3.1
                        @Override // androidx.dynamicanimation.animation.DynamicAnimation.OnAnimationEndListener
                        public void onAnimationEnd(DynamicAnimation dynamicAnimation, boolean z3, float f, float f2) {
                            dynamicAnimation.removeEndListener(this);
                            arrayList.add((SpringAnimation) dynamicAnimation);
                            if (arrayList.size() == 2) {
                                AnonymousClass3.this.updateLayout();
                            }
                        }
                    };
                    if (!PipVideoOverlay.this.pipXSpring.isRunning()) {
                        arrayList.add(PipVideoOverlay.this.pipXSpring);
                    } else {
                        PipVideoOverlay.this.pipXSpring.addEndListener(r0);
                    }
                    if (!PipVideoOverlay.this.pipYSpring.isRunning()) {
                        arrayList.add(PipVideoOverlay.this.pipYSpring);
                    } else {
                        PipVideoOverlay.this.pipYSpring.addEndListener(r0);
                    }
                } else {
                    updateLayout();
                }
            }

            /* access modifiers changed from: private */
            public void updateLayout() {
                PipVideoOverlay pipVideoOverlay = PipVideoOverlay.this;
                WindowManager.LayoutParams layoutParams = pipVideoOverlay.windowLayoutParams;
                int suggestedWidth = (int) (((float) PipVideoOverlay.this.getSuggestedWidth()) * PipVideoOverlay.this.scaleFactor);
                layoutParams.width = suggestedWidth;
                pipVideoOverlay.pipWidth = suggestedWidth;
                PipVideoOverlay pipVideoOverlay2 = PipVideoOverlay.this;
                WindowManager.LayoutParams layoutParams2 = pipVideoOverlay2.windowLayoutParams;
                int suggestedHeight = (int) (((float) PipVideoOverlay.this.getSuggestedHeight()) * PipVideoOverlay.this.scaleFactor);
                layoutParams2.height = suggestedHeight;
                pipVideoOverlay2.pipHeight = suggestedHeight;
                try {
                    PipVideoOverlay.this.windowManager.updateViewLayout(PipVideoOverlay.this.contentView, PipVideoOverlay.this.windowLayoutParams);
                } catch (IllegalArgumentException unused) {
                }
            }
        });
        this.scaleGestureDetector = scaleGestureDetector;
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 19) {
            scaleGestureDetector.setQuickScaleEnabled(false);
        }
        if (i3 >= 23) {
            this.scaleGestureDetector.setStylusScaleEnabled(false);
        }
        this.gestureDetector = new GestureDetectorFixDoubleTap(context2, new GestureDetectorFixDoubleTap.OnGestureListener() { // from class: org.telegram.ui.Components.PipVideoOverlay.4
            private float startPipX;
            private float startPipY;

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
            public boolean onDown(MotionEvent motionEvent) {
                if (PipVideoOverlay.this.isShowingControls) {
                    for (int i4 = 1; i4 < PipVideoOverlay.this.contentFrameLayout.getChildCount(); i4++) {
                        View childAt = PipVideoOverlay.this.contentFrameLayout.getChildAt(i4);
                        if (childAt.dispatchTouchEvent(motionEvent)) {
                            PipVideoOverlay.this.consumingChild = childAt;
                            return true;
                        }
                    }
                }
                this.startPipX = PipVideoOverlay.this.pipX;
                this.startPipY = PipVideoOverlay.this.pipY;
                return true;
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
            public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                if (PipVideoOverlay.this.controlsAnimator != null) {
                    return true;
                }
                if (PipVideoOverlay.this.postedDismissControls) {
                    AndroidUtilities.cancelRunOnUIThread(PipVideoOverlay.this.dismissControlsCallback);
                    PipVideoOverlay.this.postedDismissControls = false;
                }
                PipVideoOverlay pipVideoOverlay = PipVideoOverlay.this;
                pipVideoOverlay.isShowingControls = !pipVideoOverlay.isShowingControls;
                PipVideoOverlay pipVideoOverlay2 = PipVideoOverlay.this;
                pipVideoOverlay2.toggleControls(pipVideoOverlay2.isShowingControls);
                if (PipVideoOverlay.this.isShowingControls && !PipVideoOverlay.this.postedDismissControls) {
                    AndroidUtilities.runOnUIThread(PipVideoOverlay.this.dismissControlsCallback, 2500);
                    PipVideoOverlay.this.postedDismissControls = true;
                }
                return true;
            }

            /* JADX WARNING: Removed duplicated region for block: B:41:0x00ab  */
            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public boolean onDoubleTap(android.view.MotionEvent r15) {
                /*
                // Method dump skipped, instructions count: 259
                */
                throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.Components.PipVideoOverlay.AnonymousClass4.onDoubleTap(android.view.MotionEvent):boolean");
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
            public boolean onSingleTapUp(MotionEvent motionEvent) {
                if (!hasDoubleTap()) {
                    return onSingleTapConfirmed(motionEvent);
                }
                return super.onSingleTapUp(motionEvent);
            }

            @Override // org.telegram.ui.Components.GestureDetectorFixDoubleTap.OnGestureListener
            public boolean hasDoubleTap() {
                if (PipVideoOverlay.this.photoViewer == null || PipVideoOverlay.this.photoViewer.getVideoPlayer() == null || PipVideoOverlay.this.isDismissing || PipVideoOverlay.this.isVideoCompleted || PipVideoOverlay.this.isScrolling || PipVideoOverlay.this.scaleGestureDetector.isInProgress() || !PipVideoOverlay.this.canLongClick) {
                    return false;
                }
                VideoPlayer videoPlayer = PipVideoOverlay.this.photoViewer.getVideoPlayer();
                long currentPosition = videoPlayer.getCurrentPosition();
                long duration = videoPlayer.getDuration();
                if (currentPosition == -9223372036854775807L || duration < 15000) {
                    return false;
                }
                return true;
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                if (!PipVideoOverlay.this.isScrolling || PipVideoOverlay.this.isScrollDisallowed) {
                    return false;
                }
                SpringForce spring = PipVideoOverlay.this.pipXSpring.setStartVelocity(f).setStartValue(PipVideoOverlay.this.pipX).getSpring();
                float f3 = PipVideoOverlay.this.pipX + (((float) PipVideoOverlay.this.pipWidth) / 2.0f) + (f / 7.0f);
                int i4 = AndroidUtilities.displaySize.x;
                spring.setFinalPosition(f3 >= ((float) i4) / 2.0f ? (float) ((i4 - PipVideoOverlay.this.pipWidth) - AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f));
                PipVideoOverlay.this.pipXSpring.start();
                PipVideoOverlay.this.pipYSpring.setStartVelocity(f).setStartValue(PipVideoOverlay.this.pipY).getSpring().setFinalPosition(MathUtils.clamp(PipVideoOverlay.this.pipY + (f2 / 10.0f), (float) AndroidUtilities.dp(16.0f), (float) ((AndroidUtilities.displaySize.y - PipVideoOverlay.this.pipHeight) - AndroidUtilities.dp(16.0f))));
                PipVideoOverlay.this.pipYSpring.start();
                return true;
            }

            @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                int i4;
                if (!PipVideoOverlay.this.isScrolling && PipVideoOverlay.this.controlsAnimator == null && !PipVideoOverlay.this.isScrollDisallowed && (Math.abs(f) >= ((float) scaledTouchSlop) || Math.abs(f2) >= ((float) scaledTouchSlop))) {
                    PipVideoOverlay.this.isScrolling = true;
                    PipVideoOverlay.this.pipXSpring.cancel();
                    PipVideoOverlay.this.pipYSpring.cancel();
                    PipVideoOverlay.this.canLongClick = false;
                    PipVideoOverlay.this.cancelRewind();
                    AndroidUtilities.cancelRunOnUIThread(PipVideoOverlay.this.longClickCallback);
                }
                if (PipVideoOverlay.this.isScrolling) {
                    float f3 = PipVideoOverlay.this.pipX;
                    float rawX = (this.startPipX + motionEvent2.getRawX()) - motionEvent.getRawX();
                    PipVideoOverlay.this.pipY = (this.startPipY + motionEvent2.getRawY()) - motionEvent.getRawY();
                    if (rawX <= ((float) (-PipVideoOverlay.this.pipWidth)) * 0.25f || rawX >= ((float) AndroidUtilities.displaySize.x) - (((float) PipVideoOverlay.this.pipWidth) * 0.75f)) {
                        if (!PipVideoOverlay.this.onSideToDismiss) {
                            SpringForce spring = PipVideoOverlay.this.pipXSpring.setStartValue(f3).getSpring();
                            float f4 = rawX + (((float) PipVideoOverlay.this.pipWidth) / 2.0f);
                            int i5 = AndroidUtilities.displaySize.x;
                            if (f4 >= ((float) i5) / 2.0f) {
                                i4 = AndroidUtilities.dp(16.0f);
                            } else {
                                i5 = AndroidUtilities.dp(16.0f);
                                i4 = PipVideoOverlay.this.pipWidth;
                            }
                            spring.setFinalPosition((float) (i5 - i4));
                            PipVideoOverlay.this.pipXSpring.start();
                        }
                        PipVideoOverlay.this.onSideToDismiss = true;
                    } else if (PipVideoOverlay.this.onSideToDismiss) {
                        if (PipVideoOverlay.this.onSideToDismiss) {
                            PipVideoOverlay.this.pipXSpring.addEndListener(new PipVideoOverlay$4$$ExternalSyntheticLambda0(this, rawX));
                            PipVideoOverlay.this.pipXSpring.setStartValue(f3).getSpring().setFinalPosition(rawX);
                            PipVideoOverlay.this.pipXSpring.start();
                        }
                        PipVideoOverlay.this.onSideToDismiss = false;
                    } else {
                        if (PipVideoOverlay.this.pipXSpring.isRunning()) {
                            PipVideoOverlay.this.pipXSpring.getSpring().setFinalPosition(rawX);
                        } else {
                            PipVideoOverlay.this.windowLayoutParams.x = (int) PipVideoOverlay.this.pipX = rawX;
                            PipVideoOverlay.this.getPipConfig().setPipX(rawX);
                        }
                        PipVideoOverlay.this.windowLayoutParams.y = (int) PipVideoOverlay.this.pipY;
                        PipVideoOverlay.this.getPipConfig().setPipY(PipVideoOverlay.this.pipY);
                        PipVideoOverlay.this.windowManager.updateViewLayout(PipVideoOverlay.this.contentView, PipVideoOverlay.this.windowLayoutParams);
                    }
                }
                return true;
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onScroll$0(float f, DynamicAnimation dynamicAnimation, boolean z3, float f2, float f3) {
                if (!z3) {
                    SpringForce spring = PipVideoOverlay.this.pipXSpring.getSpring();
                    float f4 = f + (((float) PipVideoOverlay.this.pipWidth) / 2.0f);
                    int i4 = AndroidUtilities.displaySize.x;
                    spring.setFinalPosition(f4 >= ((float) i4) / 2.0f ? (float) ((i4 - PipVideoOverlay.this.pipWidth) - AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f));
                }
            }
        });
        this.contentFrameLayout = new FrameLayout(context2) { // from class: org.telegram.ui.Components.PipVideoOverlay.5
            private Path path = new Path();

            @Override // android.view.View, android.view.ViewGroup
            public boolean dispatchTouchEvent(MotionEvent motionEvent) {
                int actionMasked = motionEvent.getActionMasked();
                if (actionMasked == 0 || actionMasked == 5) {
                    if (motionEvent.getPointerCount() == 1) {
                        PipVideoOverlay.this.canLongClick = true;
                        PipVideoOverlay.this.longClickStartPoint = new float[]{motionEvent.getX(), motionEvent.getY()};
                        AndroidUtilities.runOnUIThread(PipVideoOverlay.this.longClickCallback, 500);
                    } else {
                        PipVideoOverlay.this.canLongClick = false;
                        PipVideoOverlay.this.cancelRewind();
                        AndroidUtilities.cancelRunOnUIThread(PipVideoOverlay.this.longClickCallback);
                    }
                }
                if (actionMasked == 1 || actionMasked == 3 || actionMasked == 6) {
                    PipVideoOverlay.this.canLongClick = false;
                    PipVideoOverlay.this.cancelRewind();
                    AndroidUtilities.cancelRunOnUIThread(PipVideoOverlay.this.longClickCallback);
                }
                if (PipVideoOverlay.this.consumingChild != null) {
                    MotionEvent obtain = MotionEvent.obtain(motionEvent);
                    obtain.offsetLocation(PipVideoOverlay.this.consumingChild.getX(), PipVideoOverlay.this.consumingChild.getY());
                    boolean dispatchTouchEvent = PipVideoOverlay.this.consumingChild.dispatchTouchEvent(motionEvent);
                    obtain.recycle();
                    if (actionMasked == 1 || actionMasked == 3 || actionMasked == 6) {
                        PipVideoOverlay.this.consumingChild = null;
                    }
                    if (dispatchTouchEvent) {
                        return true;
                    }
                }
                MotionEvent obtain2 = MotionEvent.obtain(motionEvent);
                obtain2.offsetLocation(motionEvent.getRawX() - motionEvent.getX(), motionEvent.getRawY() - motionEvent.getY());
                boolean onTouchEvent = PipVideoOverlay.this.scaleGestureDetector.onTouchEvent(obtain2);
                obtain2.recycle();
                boolean z3 = !PipVideoOverlay.this.scaleGestureDetector.isInProgress() && PipVideoOverlay.this.gestureDetector.onTouchEvent(motionEvent);
                if (actionMasked == 1 || actionMasked == 3 || actionMasked == 6) {
                    PipVideoOverlay.this.isScrolling = false;
                    PipVideoOverlay.this.isScrollDisallowed = false;
                    if (PipVideoOverlay.this.onSideToDismiss) {
                        PipVideoOverlay.this.onSideToDismiss = false;
                        PipVideoOverlay.dimissAndDestroy();
                    } else {
                        if (!PipVideoOverlay.this.pipXSpring.isRunning()) {
                            SpringForce spring = PipVideoOverlay.this.pipXSpring.setStartValue(PipVideoOverlay.this.pipX).getSpring();
                            float f = PipVideoOverlay.this.pipX + (((float) PipVideoOverlay.this.pipWidth) / 2.0f);
                            int i4 = AndroidUtilities.displaySize.x;
                            spring.setFinalPosition(f >= ((float) i4) / 2.0f ? (float) ((i4 - PipVideoOverlay.this.pipWidth) - AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f));
                            PipVideoOverlay.this.pipXSpring.start();
                        }
                        if (!PipVideoOverlay.this.pipYSpring.isRunning()) {
                            PipVideoOverlay.this.pipYSpring.setStartValue(PipVideoOverlay.this.pipY).getSpring().setFinalPosition(MathUtils.clamp(PipVideoOverlay.this.pipY, (float) AndroidUtilities.dp(16.0f), (float) ((AndroidUtilities.displaySize.y - PipVideoOverlay.this.pipHeight) - AndroidUtilities.dp(16.0f))));
                            PipVideoOverlay.this.pipYSpring.start();
                        }
                    }
                }
                if (onTouchEvent || z3) {
                    return true;
                }
                return false;
            }

            @Override // android.view.View
            protected void onConfigurationChanged(Configuration configuration) {
                AndroidUtilities.checkDisplaySize(getContext(), configuration);
                PipVideoOverlay.this.pipConfig = null;
                if (((float) PipVideoOverlay.this.pipWidth) != ((float) PipVideoOverlay.this.getSuggestedWidth()) * PipVideoOverlay.this.scaleFactor || ((float) PipVideoOverlay.this.pipHeight) != ((float) PipVideoOverlay.this.getSuggestedHeight()) * PipVideoOverlay.this.scaleFactor) {
                    WindowManager.LayoutParams layoutParams = PipVideoOverlay.this.windowLayoutParams;
                    PipVideoOverlay pipVideoOverlay = PipVideoOverlay.this;
                    layoutParams.width = pipVideoOverlay.pipWidth = (int) (((float) pipVideoOverlay.getSuggestedWidth()) * PipVideoOverlay.this.scaleFactor);
                    WindowManager.LayoutParams layoutParams2 = PipVideoOverlay.this.windowLayoutParams;
                    PipVideoOverlay pipVideoOverlay2 = PipVideoOverlay.this;
                    layoutParams2.height = pipVideoOverlay2.pipHeight = (int) (((float) pipVideoOverlay2.getSuggestedHeight()) * PipVideoOverlay.this.scaleFactor);
                    PipVideoOverlay.this.windowManager.updateViewLayout(PipVideoOverlay.this.contentView, PipVideoOverlay.this.windowLayoutParams);
                    SpringForce spring = PipVideoOverlay.this.pipXSpring.setStartValue(PipVideoOverlay.this.pipX).getSpring();
                    float suggestedWidth = PipVideoOverlay.this.pipX + ((((float) PipVideoOverlay.this.getSuggestedWidth()) * PipVideoOverlay.this.scaleFactor) / 2.0f);
                    int i4 = AndroidUtilities.displaySize.x;
                    spring.setFinalPosition(suggestedWidth >= ((float) i4) / 2.0f ? (((float) i4) - (((float) PipVideoOverlay.this.getSuggestedWidth()) * PipVideoOverlay.this.scaleFactor)) - ((float) AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f));
                    PipVideoOverlay.this.pipXSpring.start();
                    PipVideoOverlay.this.pipYSpring.setStartValue(PipVideoOverlay.this.pipY).getSpring().setFinalPosition(MathUtils.clamp(PipVideoOverlay.this.pipY, (float) AndroidUtilities.dp(16.0f), (((float) AndroidUtilities.displaySize.y) - (((float) PipVideoOverlay.this.getSuggestedHeight()) * PipVideoOverlay.this.scaleFactor)) - ((float) AndroidUtilities.dp(16.0f))));
                    PipVideoOverlay.this.pipYSpring.start();
                }
            }

            @Override // android.view.View
            public void draw(Canvas canvas) {
                if (Build.VERSION.SDK_INT >= 21) {
                    super.draw(canvas);
                    return;
                }
                canvas.save();
                canvas.clipPath(this.path);
                super.draw(canvas);
                canvas.restore();
            }

            @Override // android.view.View
            protected void onSizeChanged(int i4, int i5, int i6, int i7) {
                super.onSizeChanged(i4, i5, i6, i7);
                this.path.rewind();
                RectF rectF = AndroidUtilities.rectTmp;
                rectF.set(0.0f, 0.0f, (float) i4, (float) i5);
                this.path.addRoundRect(rectF, (float) AndroidUtilities.dp(10.0f), (float) AndroidUtilities.dp(10.0f), Path.Direction.CW);
            }
        };
        AnonymousClass6 r7 = new ViewGroup(context2) { // from class: org.telegram.ui.Components.PipVideoOverlay.6
            @Override // android.view.ViewGroup, android.view.View
            protected void onLayout(boolean z3, int i4, int i5, int i6, int i7) {
                PipVideoOverlay.this.contentFrameLayout.layout(0, 0, PipVideoOverlay.this.pipWidth, PipVideoOverlay.this.pipHeight);
            }

            @Override // android.view.View
            protected void onMeasure(int i4, int i5) {
                setMeasuredDimension(View.MeasureSpec.getSize(i4), View.MeasureSpec.getSize(i5));
                PipVideoOverlay.this.contentFrameLayout.measure(View.MeasureSpec.makeMeasureSpec(PipVideoOverlay.this.pipWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(PipVideoOverlay.this.pipHeight, 1073741824));
            }

            @Override // android.view.View
            public void draw(Canvas canvas) {
                canvas.save();
                canvas.scale(((float) PipVideoOverlay.this.pipWidth) / ((float) PipVideoOverlay.this.contentFrameLayout.getWidth()), ((float) PipVideoOverlay.this.pipHeight) / ((float) PipVideoOverlay.this.contentFrameLayout.getHeight()));
                super.draw(canvas);
                canvas.restore();
            }
        };
        this.contentView = r7;
        r7.addView(this.contentFrameLayout, LayoutHelper.createFrame(-1, -1.0f));
        if (i3 >= 21) {
            this.contentFrameLayout.setOutlineProvider(new ViewOutlineProvider(this) { // from class: org.telegram.ui.Components.PipVideoOverlay.7
                @Override // android.view.ViewOutlineProvider
                public void getOutline(View view2, Outline outline) {
                    outline.setRoundRect(0, 0, view2.getMeasuredWidth(), view2.getMeasuredHeight(), (float) AndroidUtilities.dp(10.0f));
                }
            });
            this.contentFrameLayout.setClipToOutline(true);
        }
        this.contentFrameLayout.setBackgroundColor(Theme.getColor("voipgroup_actionBar"));
        this.innerView = view;
        if (view.getParent() != null) {
            ((ViewGroup) this.innerView.getParent()).removeView(this.innerView);
        }
        this.contentFrameLayout.addView(this.innerView, LayoutHelper.createFrame(-1, -1.0f));
        this.videoForwardDrawable.setDelegate(new VideoForwardDrawable.VideoForwardDrawableDelegate() { // from class: org.telegram.ui.Components.PipVideoOverlay.8
            @Override // org.telegram.ui.Components.VideoForwardDrawable.VideoForwardDrawableDelegate
            public void onAnimationEnd() {
            }

            @Override // org.telegram.ui.Components.VideoForwardDrawable.VideoForwardDrawableDelegate
            public void invalidate() {
                PipVideoOverlay.this.controlsView.invalidate();
            }
        });
        AnonymousClass9 r72 = new FrameLayout(context2) { // from class: org.telegram.ui.Components.PipVideoOverlay.9
            @Override // android.view.View
            protected void onDraw(Canvas canvas) {
                if (PipVideoOverlay.this.videoForwardDrawable.isAnimating()) {
                    PipVideoOverlay.this.videoForwardDrawable.setBounds(getLeft(), getTop(), getRight(), getBottom());
                    PipVideoOverlay.this.videoForwardDrawable.draw(canvas);
                }
            }
        };
        this.controlsView = r72;
        r72.setWillNotDraw(false);
        this.controlsView.setAlpha(0.0f);
        View view2 = new View(context2);
        view2.setBackgroundColor(1275068416);
        this.controlsView.addView(view2, LayoutHelper.createFrame(-1, -1.0f));
        int dp = AndroidUtilities.dp(8.0f);
        ImageView imageView = new ImageView(context2);
        imageView.setImageResource(R.drawable.pip_video_close);
        imageView.setColorFilter(Theme.getColor("voipgroup_actionBarItems"), PorterDuff.Mode.MULTIPLY);
        imageView.setBackground(Theme.createSelectorDrawable(Theme.getColor("listSelectorSDK21")));
        imageView.setPadding(dp, dp, dp, dp);
        imageView.setOnClickListener(PipVideoOverlay$$ExternalSyntheticLambda3.INSTANCE);
        float f = (float) 38;
        float f2 = (float) 4;
        this.controlsView.addView(imageView, LayoutHelper.createFrame(38, f, 5, 0.0f, f2, f2, 0.0f));
        ImageView imageView2 = new ImageView(context2);
        imageView2.setImageResource(R.drawable.pip_video_expand);
        imageView2.setColorFilter(Theme.getColor("voipgroup_actionBarItems"), PorterDuff.Mode.MULTIPLY);
        imageView2.setBackground(Theme.createSelectorDrawable(Theme.getColor("listSelectorSDK21")));
        imageView2.setPadding(dp, dp, dp, dp);
        imageView2.setOnClickListener(new View.OnClickListener(z) { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda2
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                PipVideoOverlay.this.lambda$showInternal$10(this.f$1, view3);
            }
        });
        this.controlsView.addView(imageView2, LayoutHelper.createFrame(38, f, 5, 0.0f, f2, (float) 48, 0.0f));
        ImageView imageView3 = new ImageView(context2);
        this.playPauseButton = imageView3;
        imageView3.setColorFilter(Theme.getColor("voipgroup_actionBarItems"), PorterDuff.Mode.MULTIPLY);
        this.playPauseButton.setBackground(Theme.createSelectorDrawable(Theme.getColor("listSelectorSDK21")));
        this.playPauseButton.setOnClickListener(new View.OnClickListener() { // from class: org.telegram.ui.Components.PipVideoOverlay$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                PipVideoOverlay.this.lambda$showInternal$11(view3);
            }
        });
        this.playPauseButton.setVisibility(this.innerView instanceof WebView ? 8 : 0);
        this.controlsView.addView(this.playPauseButton, LayoutHelper.createFrame(38, 38, 17));
        VideoProgressView videoProgressView = new VideoProgressView(context2);
        this.videoProgressView = videoProgressView;
        this.controlsView.addView(videoProgressView, LayoutHelper.createFrame(-1, -1.0f));
        this.contentFrameLayout.addView(this.controlsView, LayoutHelper.createFrame(-1, -1.0f));
        if (z) {
            context = activity;
        } else {
            context = ApplicationLoader.applicationContext;
        }
        this.windowManager = (WindowManager) context.getSystemService("window");
        WindowManager.LayoutParams createWindowLayoutParams = createWindowLayoutParams(z);
        this.windowLayoutParams = createWindowLayoutParams;
        int i4 = this.pipWidth;
        createWindowLayoutParams.width = i4;
        createWindowLayoutParams.height = this.pipHeight;
        if (pipX != -1.0f) {
            float f3 = pipX + (((float) i4) / 2.0f);
            int i5 = AndroidUtilities.displaySize.x;
            float dp2 = f3 >= ((float) i5) / 2.0f ? (float) ((i5 - i4) - AndroidUtilities.dp(16.0f)) : (float) AndroidUtilities.dp(16.0f);
            this.pipX = dp2;
            createWindowLayoutParams.x = (int) dp2;
        } else {
            float dp3 = (float) ((AndroidUtilities.displaySize.x - i4) - AndroidUtilities.dp(16.0f));
            this.pipX = dp3;
            createWindowLayoutParams.x = (int) dp3;
        }
        if (pipY != -1.0f) {
            WindowManager.LayoutParams layoutParams = this.windowLayoutParams;
            float clamp = MathUtils.clamp(pipY, (float) AndroidUtilities.dp(16.0f), (float) ((AndroidUtilities.displaySize.y - AndroidUtilities.dp(16.0f)) - this.pipHeight));
            this.pipY = clamp;
            layoutParams.y = (int) clamp;
        } else {
            WindowManager.LayoutParams layoutParams2 = this.windowLayoutParams;
            float dp4 = (float) AndroidUtilities.dp(16.0f);
            this.pipY = dp4;
            layoutParams2.y = (int) dp4;
        }
        WindowManager.LayoutParams layoutParams3 = this.windowLayoutParams;
        layoutParams3.dimAmount = 0.0f;
        layoutParams3.flags = 520;
        if (z2) {
            this.windowManager.addView(this.contentView, layoutParams3);
            return true;
        }
        this.contentView.setAlpha(0.0f);
        this.contentView.setScaleX(0.1f);
        this.contentView.setScaleY(0.1f);
        this.windowManager.addView(this.contentView, this.windowLayoutParams);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(250L);
        animatorSet.setInterpolator(CubicBezierInterpolator.DEFAULT);
        animatorSet.playTogether(ObjectAnimator.ofFloat(this.contentView, View.ALPHA, 1.0f), ObjectAnimator.ofFloat(this.contentView, View.SCALE_X, 1.0f), ObjectAnimator.ofFloat(this.contentView, View.SCALE_Y, 1.0f));
        animatorSet.start();
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showInternal$7(DynamicAnimation dynamicAnimation, boolean z, float f, float f2) {
        getPipConfig().setPipX(f);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showInternal$8(DynamicAnimation dynamicAnimation, boolean z, float f, float f2) {
        getPipConfig().setPipY(f);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showInternal$10(boolean z, View view) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 21) {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) view.getContext().getSystemService("activity")).getRunningAppProcesses();
            if (!runningAppProcesses.isEmpty() && runningAppProcesses.get(0).importance != 100) {
                z2 = false;
            }
        }
        if (z || (z2 && LaunchActivity.isResumed)) {
            EmbedBottomSheet embedBottomSheet = this.parentSheet;
            if (embedBottomSheet != null) {
                embedBottomSheet.exitFromPip();
                return;
            }
            PhotoViewer photoViewer = this.photoViewer;
            if (photoViewer != null) {
                photoViewer.exitFromPip();
                return;
            }
            return;
        }
        view.getClass();
        LaunchActivity.onResumeStaticCallback = new ChatActivityEnterView$$ExternalSyntheticLambda33(view);
        Context context = ApplicationLoader.applicationContext;
        Intent intent = new Intent(context, LaunchActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$showInternal$11(View view) {
        VideoPlayer videoPlayer;
        PhotoViewer photoViewer = this.photoViewer;
        if (photoViewer != null && (videoPlayer = photoViewer.getVideoPlayer()) != null) {
            if (videoPlayer.isPlaying()) {
                videoPlayer.pause();
            } else {
                videoPlayer.play();
            }
            updatePlayButton();
        }
    }

    @SuppressLint({"WrongConstant"})
    private WindowManager.LayoutParams createWindowLayoutParams(boolean z) {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = 51;
        layoutParams.format = -3;
        if (z || !AndroidUtilities.checkInlinePermissions(ApplicationLoader.applicationContext)) {
            layoutParams.type = 99;
        } else if (Build.VERSION.SDK_INT >= 26) {
            layoutParams.type = 2038;
        } else {
            layoutParams.type = 2003;
        }
        layoutParams.flags = 520;
        return layoutParams;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public final class VideoProgressView extends View {
        private Paint bufferPaint = new Paint();
        private Paint progressPaint = new Paint();

        public VideoProgressView(Context context) {
            super(context);
            this.progressPaint.setColor(-1);
            this.progressPaint.setStyle(Paint.Style.STROKE);
            this.progressPaint.setStrokeCap(Paint.Cap.ROUND);
            this.progressPaint.setStrokeWidth((float) AndroidUtilities.dp(2.0f));
            this.bufferPaint.setColor(this.progressPaint.getColor());
            this.bufferPaint.setAlpha((int) (((float) this.progressPaint.getAlpha()) * 0.3f));
            this.bufferPaint.setStyle(Paint.Style.STROKE);
            this.bufferPaint.setStrokeCap(Paint.Cap.ROUND);
            this.bufferPaint.setStrokeWidth((float) AndroidUtilities.dp(2.0f));
        }

        @Override // android.view.View
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            int width = getWidth();
            int dp = AndroidUtilities.dp(10.0f);
            float f = (float) ((width - dp) - dp);
            int i = ((int) (PipVideoOverlay.this.videoProgress * f)) + dp;
            float height = (float) (getHeight() - AndroidUtilities.dp(8.0f));
            if (PipVideoOverlay.this.bufferProgress != 0.0f) {
                float f2 = (float) dp;
                canvas.drawLine(f2, height, f2 + (f * PipVideoOverlay.this.bufferProgress), height, this.bufferPaint);
            }
            canvas.drawLine((float) dp, height, (float) i, height, this.progressPaint);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static final class PipConfig {
        private SharedPreferences mPrefs;

        private PipConfig(int i, int i2) {
            Context context = ApplicationLoader.applicationContext;
            this.mPrefs = context.getSharedPreferences("pip_layout_" + i + "_" + i2, 0);
        }

        /* access modifiers changed from: private */
        public void setPipX(float f) {
            this.mPrefs.edit().putFloat("x", f).apply();
        }

        /* access modifiers changed from: private */
        public void setPipY(float f) {
            this.mPrefs.edit().putFloat("y", f).apply();
        }

        /* access modifiers changed from: private */
        public float getScaleFactor() {
            return this.mPrefs.getFloat("scale_factor", 1.0f);
        }

        /* access modifiers changed from: private */
        public float getPipX() {
            return this.mPrefs.getFloat("x", -1.0f);
        }

        /* access modifiers changed from: private */
        public float getPipY() {
            return this.mPrefs.getFloat("y", -1.0f);
        }
    }
}
