package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivityBotWebViewButton$$ExternalSyntheticLambda1 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ ChatActivityBotWebViewButton$$ExternalSyntheticLambda1 INSTANCE = new ChatActivityBotWebViewButton$$ExternalSyntheticLambda1();

    private /* synthetic */ ChatActivityBotWebViewButton$$ExternalSyntheticLambda1() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Setter
    public final void set(Object obj, float f) {
        ((ChatActivityBotWebViewButton) obj).setProgress(f);
    }
}
