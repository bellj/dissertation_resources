package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlertPhotoLayoutPreview;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlertPhotoLayoutPreview$PreviewGroupsView$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ ChatAttachAlertPhotoLayoutPreview.PreviewGroupsView f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ ChatAttachAlertPhotoLayoutPreview$PreviewGroupsView$$ExternalSyntheticLambda2(ChatAttachAlertPhotoLayoutPreview.PreviewGroupsView previewGroupsView, int i) {
        this.f$0 = previewGroupsView;
        this.f$1 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onTouchEvent$4(this.f$1);
    }
}
