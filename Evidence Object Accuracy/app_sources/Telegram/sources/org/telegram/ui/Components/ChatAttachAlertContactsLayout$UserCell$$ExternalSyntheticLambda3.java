package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlertContactsLayout;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlertContactsLayout$UserCell$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ChatAttachAlertContactsLayout.UserCell f$0;
    public final /* synthetic */ ChatAttachAlertContactsLayout.UserCell.CharSequenceCallback f$1;

    public /* synthetic */ ChatAttachAlertContactsLayout$UserCell$$ExternalSyntheticLambda3(ChatAttachAlertContactsLayout.UserCell userCell, ChatAttachAlertContactsLayout.UserCell.CharSequenceCallback charSequenceCallback) {
        this.f$0 = userCell;
        this.f$1 = charSequenceCallback;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$setData$1(this.f$1);
    }
}
