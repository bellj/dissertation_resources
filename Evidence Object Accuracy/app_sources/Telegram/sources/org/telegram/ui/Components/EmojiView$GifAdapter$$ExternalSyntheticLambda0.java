package org.telegram.ui.Components;

import org.telegram.tgnet.TLObject;
import org.telegram.ui.Components.EmojiView;

/* loaded from: classes3.dex */
public final /* synthetic */ class EmojiView$GifAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ EmojiView.GifAdapter f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ boolean f$3;
    public final /* synthetic */ boolean f$4;
    public final /* synthetic */ boolean f$5;
    public final /* synthetic */ String f$6;
    public final /* synthetic */ TLObject f$7;

    public /* synthetic */ EmojiView$GifAdapter$$ExternalSyntheticLambda0(EmojiView.GifAdapter gifAdapter, String str, String str2, boolean z, boolean z2, boolean z3, String str3, TLObject tLObject) {
        this.f$0 = gifAdapter;
        this.f$1 = str;
        this.f$2 = str2;
        this.f$3 = z;
        this.f$4 = z2;
        this.f$5 = z3;
        this.f$6 = str3;
        this.f$7 = tLObject;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$search$2(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
    }
}
