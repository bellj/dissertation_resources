package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.voip.RTMPStreamPipOverlay;

/* loaded from: classes3.dex */
public final /* synthetic */ class RTMPStreamPipOverlay$8$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ RTMPStreamPipOverlay.AnonymousClass8 f$0;

    public /* synthetic */ RTMPStreamPipOverlay$8$$ExternalSyntheticLambda0(RTMPStreamPipOverlay.AnonymousClass8 r1) {
        this.f$0 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onFrameResolutionChanged$1();
    }
}
