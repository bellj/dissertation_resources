package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.FilterTabsView;

/* loaded from: classes3.dex */
public final /* synthetic */ class FilterTabsView$4$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ FilterTabsView.AnonymousClass4 f$0;

    public /* synthetic */ FilterTabsView$4$$ExternalSyntheticLambda0(FilterTabsView.AnonymousClass4 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        FilterTabsView.AnonymousClass4.$r8$lambda$7qHrpxDS_loj5mK3Loi6eMa2yw4(this.f$0, valueAnimator);
    }
}
