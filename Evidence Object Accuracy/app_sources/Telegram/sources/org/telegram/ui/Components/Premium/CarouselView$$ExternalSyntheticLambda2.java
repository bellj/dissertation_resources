package org.telegram.ui.Components.Premium;

import j$.util.function.ToIntFunction;
import org.telegram.ui.Components.Premium.CarouselView;

/* loaded from: classes3.dex */
public final /* synthetic */ class CarouselView$$ExternalSyntheticLambda2 implements ToIntFunction {
    public static final /* synthetic */ CarouselView$$ExternalSyntheticLambda2 INSTANCE = new CarouselView$$ExternalSyntheticLambda2();

    private /* synthetic */ CarouselView$$ExternalSyntheticLambda2() {
    }

    @Override // j$.util.function.ToIntFunction
    public final int applyAsInt(Object obj) {
        return CarouselView.m1216$r8$lambda$ecKrWGreUT24pyr8sjYT6lCGbc((CarouselView.DrawingObject) obj);
    }
}
