package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;
import org.telegram.ui.Components.ShareAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda5 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ShareAlert.ShareSearchAdapter f$0;

    public /* synthetic */ ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda5(ShareAlert.ShareSearchAdapter shareSearchAdapter) {
        this.f$0 = shareSearchAdapter;
    }

    @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListener
    public final void onItemClick(View view, int i) {
        this.f$0.lambda$onCreateViewHolder$5(view, i);
    }
}
