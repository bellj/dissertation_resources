package org.telegram.ui.Components;

import androidx.dynamicanimation.animation.DynamicAnimation;
import org.telegram.ui.Components.BotWebViewMenuContainer;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewMenuContainer$6$$ExternalSyntheticLambda0 implements DynamicAnimation.OnAnimationEndListener {
    public final /* synthetic */ BotWebViewMenuContainer.AnonymousClass6 f$0;

    public /* synthetic */ BotWebViewMenuContainer$6$$ExternalSyntheticLambda0(BotWebViewMenuContainer.AnonymousClass6 r1) {
        this.f$0 = r1;
    }

    @Override // androidx.dynamicanimation.animation.DynamicAnimation.OnAnimationEndListener
    public final void onAnimationEnd(DynamicAnimation dynamicAnimation, boolean z, float f, float f2) {
        BotWebViewMenuContainer.AnonymousClass6.$r8$lambda$Qld6GQee4Eq701m1THxWaXpTO0o(this.f$0, dynamicAnimation, z, f, f2);
    }
}
