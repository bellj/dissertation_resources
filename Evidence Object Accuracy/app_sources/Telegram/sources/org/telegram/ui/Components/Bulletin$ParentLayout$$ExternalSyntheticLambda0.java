package org.telegram.ui.Components;

import org.telegram.ui.Components.Bulletin;

/* loaded from: classes3.dex */
public final /* synthetic */ class Bulletin$ParentLayout$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Bulletin.ParentLayout f$0;
    public final /* synthetic */ float f$1;

    public /* synthetic */ Bulletin$ParentLayout$$ExternalSyntheticLambda0(Bulletin.ParentLayout parentLayout, float f) {
        this.f$0 = parentLayout;
        this.f$1 = f;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onTouchEvent$0(this.f$1);
    }
}
