package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class EmptyTextProgressView$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public static final /* synthetic */ EmptyTextProgressView$$ExternalSyntheticLambda0 INSTANCE = new EmptyTextProgressView$$ExternalSyntheticLambda0();

    private /* synthetic */ EmptyTextProgressView$$ExternalSyntheticLambda0() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return EmptyTextProgressView.lambda$new$0(view, motionEvent);
    }
}
