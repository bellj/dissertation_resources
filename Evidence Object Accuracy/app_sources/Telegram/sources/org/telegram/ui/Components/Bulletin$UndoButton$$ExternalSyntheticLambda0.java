package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.Bulletin;

/* loaded from: classes3.dex */
public final /* synthetic */ class Bulletin$UndoButton$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Bulletin.UndoButton f$0;

    public /* synthetic */ Bulletin$UndoButton$$ExternalSyntheticLambda0(Bulletin.UndoButton undoButton) {
        this.f$0 = undoButton;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
