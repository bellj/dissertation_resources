package org.telegram.ui.Components;

import android.view.animation.Interpolator;

/* loaded from: classes3.dex */
public final /* synthetic */ class SharedMediaLayout$$ExternalSyntheticLambda5 implements Interpolator {
    public static final /* synthetic */ SharedMediaLayout$$ExternalSyntheticLambda5 INSTANCE = new SharedMediaLayout$$ExternalSyntheticLambda5();

    private /* synthetic */ SharedMediaLayout$$ExternalSyntheticLambda5() {
    }

    @Override // android.animation.TimeInterpolator
    public final float getInterpolation(float f) {
        return SharedMediaLayout.lambda$static$1(f);
    }
}
