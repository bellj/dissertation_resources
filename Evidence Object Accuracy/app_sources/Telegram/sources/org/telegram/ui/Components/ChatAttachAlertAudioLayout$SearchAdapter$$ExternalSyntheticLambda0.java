package org.telegram.ui.Components;

import java.util.ArrayList;
import org.telegram.ui.Components.ChatAttachAlertAudioLayout;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlertAudioLayout$SearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ChatAttachAlertAudioLayout.SearchAdapter f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ ArrayList f$3;

    public /* synthetic */ ChatAttachAlertAudioLayout$SearchAdapter$$ExternalSyntheticLambda0(ChatAttachAlertAudioLayout.SearchAdapter searchAdapter, int i, String str, ArrayList arrayList) {
        this.f$0 = searchAdapter;
        this.f$1 = i;
        this.f$2 = str;
        this.f$3 = arrayList;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$updateSearchResults$2(this.f$1, this.f$2, this.f$3);
    }
}
