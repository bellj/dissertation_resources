package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.BotWebViewSheet;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewSheet$2$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BotWebViewSheet.AnonymousClass2 f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ int f$2;

    public /* synthetic */ BotWebViewSheet$2$$ExternalSyntheticLambda0(BotWebViewSheet.AnonymousClass2 r1, int i, int i2) {
        this.f$0 = r1;
        this.f$1 = i;
        this.f$2 = i2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        BotWebViewSheet.AnonymousClass2.m963$r8$lambda$8YOQ3_lReMoTHksIocZhYnXw1A(this.f$0, this.f$1, this.f$2, valueAnimator);
    }
}
