package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

/* loaded from: classes3.dex */
public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda107 implements NumberPicker.Formatter {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda107 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda107();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda107() {
    }

    @Override // org.telegram.ui.Components.NumberPicker.Formatter
    public final String format(int i) {
        return AlertsCreator.lambda$createSoundFrequencyPickerDialog$67(i);
    }
}
