package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda76 implements View.OnTouchListener {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda76 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda76();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda76() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return AlertsCreator.lambda$createDatePickerDialog$56(view, motionEvent);
    }
}
