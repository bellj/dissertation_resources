package org.telegram.ui.Components.voip;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.voip.VoIPPiPView;

/* loaded from: classes3.dex */
public final /* synthetic */ class VoIPPiPView$FloatingView$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ float f$0;
    public final /* synthetic */ float f$1;
    public final /* synthetic */ VoIPPiPView f$2;

    public /* synthetic */ VoIPPiPView$FloatingView$$ExternalSyntheticLambda1(float f, float f2, VoIPPiPView voIPPiPView) {
        this.f$0 = f;
        this.f$1 = f2;
        this.f$2 = voIPPiPView;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        VoIPPiPView.FloatingView.$r8$lambda$7Q2agij4YQf7qlZZRqhOcvIOxCg(this.f$0, this.f$1, this.f$2, valueAnimator);
    }
}
