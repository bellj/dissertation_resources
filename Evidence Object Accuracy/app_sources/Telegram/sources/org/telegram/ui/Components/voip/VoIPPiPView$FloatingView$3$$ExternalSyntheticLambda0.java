package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.voip.VoIPPiPView;

/* loaded from: classes3.dex */
public final /* synthetic */ class VoIPPiPView$FloatingView$3$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ VoIPPiPView.FloatingView.AnonymousClass3 f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ VoIPPiPView$FloatingView$3$$ExternalSyntheticLambda0(VoIPPiPView.FloatingView.AnonymousClass3 r1, boolean z) {
        this.f$0 = r1;
        this.f$1 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        VoIPPiPView.FloatingView.AnonymousClass3.m1370$r8$lambda$iGhzrQPDlTOeOW_Jrz8inBcOeQ(this.f$0, this.f$1);
    }
}
