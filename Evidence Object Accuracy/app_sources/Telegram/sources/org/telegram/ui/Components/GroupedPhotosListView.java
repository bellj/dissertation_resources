package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.List;
import org.telegram.messenger.AndroidUtilities;
import org.telegram.messenger.ImageLocation;
import org.telegram.messenger.ImageReceiver;
import org.telegram.messenger.MessageObject;
import org.telegram.tgnet.ConnectionsManager;
import org.telegram.tgnet.TLRPC$PageBlock;
import org.telegram.tgnet.TLRPC$PhotoSize;

/* loaded from: classes3.dex */
public class GroupedPhotosListView extends View implements GestureDetector.OnGestureListener {
    private boolean animateAllLine;
    private boolean animateBackground = true;
    private int animateToDX;
    private int animateToDXStart;
    private int animateToItem = -1;
    private boolean animateToItemFast;
    private boolean animationsEnabled = true;
    private Paint backgroundPaint = new Paint();
    private long currentGroupId;
    private int currentImage;
    private float currentItemProgress = 1.0f;
    private ArrayList<Object> currentObjects = new ArrayList<>();
    public ArrayList<ImageLocation> currentPhotos = new ArrayList<>();
    private GroupedPhotosListViewDelegate delegate;
    private float drawAlpha;
    private int drawDx;
    private GestureDetector gestureDetector;
    private boolean hasPhotos;
    private ValueAnimator hideAnimator;
    private boolean ignoreChanges;
    private ArrayList<ImageReceiver> imagesToDraw = new ArrayList<>();
    private int itemHeight;
    private int itemSpacing;
    private int itemWidth;
    private int itemY;
    private long lastUpdateTime;
    private float moveLineProgress;
    private boolean moving;
    private int nextImage;
    private float nextItemProgress = 0.0f;
    private int nextPhotoScrolling = -1;
    private Scroller scroll;
    private boolean scrolling;
    private ValueAnimator showAnimator;
    private boolean stopedScrolling;
    private ArrayList<ImageReceiver> unusedReceivers = new ArrayList<>();

    /* loaded from: classes3.dex */
    public interface GroupedPhotosListViewDelegate {
        long getAvatarsDialogId();

        int getCurrentAccount();

        int getCurrentIndex();

        ArrayList<MessageObject> getImagesArr();

        ArrayList<ImageLocation> getImagesArrLocations();

        List<TLRPC$PageBlock> getPageBlockArr();

        Object getParentObject();

        int getSlideshowMessageId();

        void onShowAnimationStart();

        void onStopScrolling();

        void setCurrentIndex(int i);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onShowPress(MotionEvent motionEvent) {
    }

    public GroupedPhotosListView(Context context, int i) {
        super(context);
        this.gestureDetector = new GestureDetector(context, this);
        this.scroll = new Scroller(context);
        this.itemWidth = AndroidUtilities.dp(42.0f);
        this.itemHeight = AndroidUtilities.dp(56.0f);
        this.itemSpacing = AndroidUtilities.dp(1.0f);
        this.itemY = i;
        this.backgroundPaint.setColor(2130706432);
    }

    public void clear() {
        this.currentPhotos.clear();
        this.currentObjects.clear();
        this.imagesToDraw.clear();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v11, types: [org.telegram.messenger.MessageObject] */
    /* JADX WARN: Type inference failed for: r11v13 */
    /* JADX WARN: Type inference failed for: r11v17 */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x025a  */
    /* JADX WARNING: Removed duplicated region for block: B:193:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void fillList() {
        /*
        // Method dump skipped, instructions count: 941
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.ui.Components.GroupedPhotosListView.fillList():void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$fillList$0(ValueAnimator valueAnimator) {
        this.drawAlpha = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidate();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$fillList$1(ValueAnimator valueAnimator) {
        this.drawAlpha = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidate();
    }

    public void setMoveProgress(float f) {
        if (!this.scrolling && this.animateToItem < 0) {
            if (f > 0.0f) {
                this.nextImage = this.currentImage - 1;
            } else {
                this.nextImage = this.currentImage + 1;
            }
            int i = this.nextImage;
            if (i < 0 || i >= this.currentPhotos.size()) {
                this.currentItemProgress = 1.0f;
            } else {
                this.currentItemProgress = 1.0f - Math.abs(f);
            }
            this.nextItemProgress = 1.0f - this.currentItemProgress;
            this.moving = f != 0.0f;
            invalidate();
            if (this.currentPhotos.isEmpty()) {
                return;
            }
            if (f < 0.0f && this.currentImage == this.currentPhotos.size() - 1) {
                return;
            }
            if (f <= 0.0f || this.currentImage != 0) {
                int i2 = (int) (f * ((float) (this.itemWidth + this.itemSpacing)));
                this.drawDx = i2;
                fillImages(true, i2);
            }
        }
    }

    private ImageReceiver getFreeReceiver() {
        ImageReceiver imageReceiver;
        if (this.unusedReceivers.isEmpty()) {
            imageReceiver = new ImageReceiver(this);
        } else {
            imageReceiver = this.unusedReceivers.get(0);
            this.unusedReceivers.remove(0);
        }
        this.imagesToDraw.add(imageReceiver);
        imageReceiver.setCurrentAccount(this.delegate.getCurrentAccount());
        return imageReceiver;
    }

    private void fillImages(boolean z, int i) {
        int i2;
        int i3;
        Object obj;
        Object obj2;
        if (!z && !this.imagesToDraw.isEmpty()) {
            this.unusedReceivers.addAll(this.imagesToDraw);
            this.imagesToDraw.clear();
            this.moving = false;
            this.moveLineProgress = 1.0f;
            this.currentItemProgress = 1.0f;
            this.nextItemProgress = 0.0f;
        }
        invalidate();
        if (getMeasuredWidth() != 0 && !this.currentPhotos.isEmpty()) {
            int measuredWidth = getMeasuredWidth();
            int measuredWidth2 = (getMeasuredWidth() / 2) - (this.itemWidth / 2);
            if (z) {
                int size = this.imagesToDraw.size();
                int i4 = 0;
                i3 = Integer.MIN_VALUE;
                i2 = ConnectionsManager.DEFAULT_DATACENTER_ID;
                while (i4 < size) {
                    ImageReceiver imageReceiver = this.imagesToDraw.get(i4);
                    int param = imageReceiver.getParam();
                    int i5 = this.itemWidth;
                    int i6 = ((param - this.currentImage) * (this.itemSpacing + i5)) + measuredWidth2 + i;
                    if (i6 > measuredWidth || i6 + i5 < 0) {
                        this.unusedReceivers.add(imageReceiver);
                        this.imagesToDraw.remove(i4);
                        size--;
                        i4--;
                    }
                    i2 = Math.min(i2, param - 1);
                    i3 = Math.max(i3, param + 1);
                    i4++;
                }
            } else {
                i3 = this.currentImage;
                i2 = i3 - 1;
            }
            if (i3 != Integer.MIN_VALUE) {
                int size2 = this.currentPhotos.size();
                while (i3 < size2) {
                    int i7 = ((i3 - this.currentImage) * (this.itemWidth + this.itemSpacing)) + measuredWidth2 + i;
                    if (i7 >= measuredWidth) {
                        break;
                    }
                    ImageLocation imageLocation = this.currentPhotos.get(i3);
                    ImageReceiver freeReceiver = getFreeReceiver();
                    freeReceiver.setImageCoords((float) i7, (float) this.itemY, (float) this.itemWidth, (float) this.itemHeight);
                    if (this.currentObjects.get(0) instanceof MessageObject) {
                        obj2 = this.currentObjects.get(i3);
                    } else if (this.currentObjects.get(0) instanceof TLRPC$PageBlock) {
                        obj2 = this.delegate.getParentObject();
                    } else {
                        obj2 = "avatar_" + this.delegate.getAvatarsDialogId();
                    }
                    freeReceiver.setImage(null, null, imageLocation, "80_80", 0, null, obj2, 1);
                    freeReceiver.setParam(i3);
                    i3++;
                }
            }
            if (i2 != Integer.MAX_VALUE) {
                while (i2 >= 0) {
                    int i8 = this.itemWidth;
                    int i9 = ((i2 - this.currentImage) * (this.itemSpacing + i8)) + measuredWidth2 + i + i8;
                    if (i9 <= 0) {
                        break;
                    }
                    ImageLocation imageLocation2 = this.currentPhotos.get(i2);
                    ImageReceiver freeReceiver2 = getFreeReceiver();
                    freeReceiver2.setImageCoords((float) i9, (float) this.itemY, (float) this.itemWidth, (float) this.itemHeight);
                    if (this.currentObjects.get(0) instanceof MessageObject) {
                        obj = this.currentObjects.get(i2);
                    } else if (this.currentObjects.get(0) instanceof TLRPC$PageBlock) {
                        obj = this.delegate.getParentObject();
                    } else {
                        obj = "avatar_" + this.delegate.getAvatarsDialogId();
                    }
                    freeReceiver2.setImage(null, null, imageLocation2, "80_80", 0, null, obj, 1);
                    freeReceiver2.setParam(i2);
                    i2--;
                }
            }
            ValueAnimator valueAnimator = this.showAnimator;
            if (valueAnimator != null && !valueAnimator.isStarted()) {
                this.showAnimator.start();
            }
        }
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        if (!this.scroll.isFinished()) {
            this.scroll.abortAnimation();
        }
        this.animateToItem = -1;
        this.animateToItemFast = false;
        return true;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int currentIndex = this.delegate.getCurrentIndex();
        ArrayList<ImageLocation> imagesArrLocations = this.delegate.getImagesArrLocations();
        ArrayList<MessageObject> imagesArr = this.delegate.getImagesArr();
        List<TLRPC$PageBlock> pageBlockArr = this.delegate.getPageBlockArr();
        stopScrolling();
        int size = this.imagesToDraw.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            ImageReceiver imageReceiver = this.imagesToDraw.get(i);
            if (imageReceiver.isInsideImage(motionEvent.getX(), motionEvent.getY())) {
                int param = imageReceiver.getParam();
                if (param < 0 || param >= this.currentObjects.size()) {
                    return true;
                }
                if (imagesArr != null && !imagesArr.isEmpty()) {
                    int indexOf = imagesArr.indexOf((MessageObject) this.currentObjects.get(param));
                    if (currentIndex == indexOf) {
                        return true;
                    }
                    this.moveLineProgress = 1.0f;
                    this.animateAllLine = true;
                    this.delegate.setCurrentIndex(indexOf);
                } else if (pageBlockArr != null && !pageBlockArr.isEmpty()) {
                    int indexOf2 = pageBlockArr.indexOf((TLRPC$PageBlock) this.currentObjects.get(param));
                    if (currentIndex == indexOf2) {
                        return true;
                    }
                    this.moveLineProgress = 1.0f;
                    this.animateAllLine = true;
                    this.delegate.setCurrentIndex(indexOf2);
                } else if (imagesArrLocations != null && !imagesArrLocations.isEmpty()) {
                    int indexOf3 = imagesArrLocations.indexOf((ImageLocation) this.currentObjects.get(param));
                    if (currentIndex == indexOf3) {
                        return true;
                    }
                    this.moveLineProgress = 1.0f;
                    this.animateAllLine = true;
                    this.delegate.setCurrentIndex(indexOf3);
                }
            } else {
                i++;
            }
        }
        return false;
    }

    private void updateAfterScroll() {
        int i;
        int i2;
        int i3;
        int i4 = this.drawDx;
        int abs = Math.abs(i4);
        int i5 = this.itemWidth;
        int i6 = this.itemSpacing;
        int i7 = -1;
        if (abs > (i5 / 2) + i6) {
            if (i4 > 0) {
                i3 = i4 - ((i5 / 2) + i6);
                i2 = 1;
            } else {
                i3 = i4 + (i5 / 2) + i6;
                i2 = -1;
            }
            i = i2 + (i3 / (i5 + (i6 * 2)));
        } else {
            i = 0;
        }
        this.nextPhotoScrolling = this.currentImage - i;
        int currentIndex = this.delegate.getCurrentIndex();
        ArrayList<ImageLocation> imagesArrLocations = this.delegate.getImagesArrLocations();
        ArrayList<MessageObject> imagesArr = this.delegate.getImagesArr();
        List<TLRPC$PageBlock> pageBlockArr = this.delegate.getPageBlockArr();
        int i8 = this.nextPhotoScrolling;
        if (currentIndex != i8 && i8 >= 0 && i8 < this.currentPhotos.size()) {
            Object obj = this.currentObjects.get(this.nextPhotoScrolling);
            if (imagesArr != null && !imagesArr.isEmpty()) {
                i7 = imagesArr.indexOf((MessageObject) obj);
            } else if (pageBlockArr != null && !pageBlockArr.isEmpty()) {
                i7 = pageBlockArr.indexOf((TLRPC$PageBlock) obj);
            } else if (imagesArrLocations != null && !imagesArrLocations.isEmpty()) {
                i7 = imagesArrLocations.indexOf((ImageLocation) obj);
            }
            if (i7 >= 0) {
                this.ignoreChanges = true;
                this.delegate.setCurrentIndex(i7);
            }
        }
        if (!this.scrolling) {
            this.scrolling = true;
            this.stopedScrolling = false;
        }
        fillImages(true, this.drawDx);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.drawDx = (int) (((float) this.drawDx) - f);
        int minScrollX = getMinScrollX();
        int maxScrollX = getMaxScrollX();
        int i = this.drawDx;
        if (i < minScrollX) {
            this.drawDx = minScrollX;
        } else if (i > maxScrollX) {
            this.drawDx = maxScrollX;
        }
        updateAfterScroll();
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.scroll.abortAnimation();
        if (this.currentPhotos.size() < 10) {
            return false;
        }
        this.scroll.fling(this.drawDx, 0, Math.round(f), 0, getMinScrollX(), getMaxScrollX(), 0, 0);
        return false;
    }

    private void stopScrolling() {
        this.scrolling = false;
        if (!this.scroll.isFinished()) {
            this.scroll.abortAnimation();
        }
        int i = this.nextPhotoScrolling;
        if (i >= 0 && i < this.currentObjects.size()) {
            this.stopedScrolling = true;
            this.animateToItemFast = false;
            int i2 = this.nextPhotoScrolling;
            this.animateToItem = i2;
            this.nextImage = i2;
            this.animateToDX = (this.currentImage - i2) * (this.itemWidth + this.itemSpacing);
            this.animateToDXStart = this.drawDx;
            this.moveLineProgress = 1.0f;
            this.nextPhotoScrolling = -1;
            GroupedPhotosListViewDelegate groupedPhotosListViewDelegate = this.delegate;
            if (groupedPhotosListViewDelegate != null) {
                groupedPhotosListViewDelegate.onStopScrolling();
            }
        }
        invalidate();
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (!this.currentPhotos.isEmpty() && getAlpha() == 1.0f) {
            if (this.gestureDetector.onTouchEvent(motionEvent) || super.onTouchEvent(motionEvent)) {
                z = true;
            }
            if (this.scrolling && motionEvent.getAction() == 1 && this.scroll.isFinished()) {
                stopScrolling();
            }
        }
        return z;
    }

    private int getMinScrollX() {
        return (-((this.currentPhotos.size() - this.currentImage) - 1)) * (this.itemWidth + (this.itemSpacing * 2));
    }

    private int getMaxScrollX() {
        return this.currentImage * (this.itemWidth + (this.itemSpacing * 2));
    }

    @Override // android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        fillImages(false, 0);
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        int i;
        int i2;
        TLRPC$PhotoSize tLRPC$PhotoSize;
        TLRPC$PhotoSize tLRPC$PhotoSize2;
        if (this.hasPhotos || !this.imagesToDraw.isEmpty()) {
            float f = this.drawAlpha;
            if (!this.animateBackground) {
                f = this.hasPhotos ? 1.0f : 0.0f;
            }
            this.backgroundPaint.setAlpha((int) (f * 127.0f));
            canvas.drawRect(0.0f, 0.0f, (float) getMeasuredWidth(), (float) getMeasuredHeight(), this.backgroundPaint);
            if (!this.imagesToDraw.isEmpty()) {
                int size = this.imagesToDraw.size();
                int i3 = this.drawDx;
                int i4 = (int) (((float) this.itemWidth) * 2.0f);
                int dp = AndroidUtilities.dp(8.0f);
                ImageLocation imageLocation = this.currentPhotos.get(this.currentImage);
                if (imageLocation == null || (tLRPC$PhotoSize2 = imageLocation.photoSize) == null) {
                    i = this.itemHeight;
                } else {
                    i = Math.max(this.itemWidth, (int) (((float) tLRPC$PhotoSize2.w) * (((float) this.itemHeight) / ((float) tLRPC$PhotoSize2.h))));
                }
                int min = Math.min(i4, i);
                float f2 = (float) (dp * 2);
                float f3 = this.currentItemProgress;
                int i5 = (int) (f2 * f3);
                int i6 = this.itemWidth;
                int i7 = i6 + ((int) (((float) (min - i6)) * f3)) + i5;
                int i8 = this.nextImage;
                if (i8 < 0 || i8 >= this.currentPhotos.size()) {
                    i2 = this.itemWidth;
                } else {
                    ImageLocation imageLocation2 = this.currentPhotos.get(this.nextImage);
                    if (imageLocation2 == null || (tLRPC$PhotoSize = imageLocation2.photoSize) == null) {
                        i2 = this.itemHeight;
                    } else {
                        i2 = Math.max(this.itemWidth, (int) (((float) tLRPC$PhotoSize.w) * (((float) this.itemHeight) / ((float) tLRPC$PhotoSize.h))));
                    }
                }
                int min2 = Math.min(i4, i2);
                float f4 = this.nextItemProgress;
                int i9 = (int) (f2 * f4);
                float f5 = (float) i3;
                int i10 = this.itemWidth;
                int i11 = (int) (f5 + (((float) (((min2 + i9) - i10) / 2)) * f4 * ((float) (this.nextImage > this.currentImage ? -1 : 1))));
                int i12 = i10 + ((int) (((float) (min2 - i10)) * f4)) + i9;
                int measuredWidth = (getMeasuredWidth() - i7) / 2;
                for (int i13 = 0; i13 < size; i13++) {
                    ImageReceiver imageReceiver = this.imagesToDraw.get(i13);
                    int param = imageReceiver.getParam();
                    int i14 = this.currentImage;
                    if (param == i14) {
                        imageReceiver.setImageX(measuredWidth + i11 + (i5 / 2));
                        imageReceiver.setImageWidth(i7 - i5);
                    } else {
                        int i15 = this.nextImage;
                        if (i15 < i14) {
                            if (param >= i14) {
                                imageReceiver.setImageX(measuredWidth + i7 + this.itemSpacing + (((imageReceiver.getParam() - this.currentImage) - 1) * (this.itemWidth + this.itemSpacing)) + i11);
                            } else if (param <= i15) {
                                int i16 = this.itemWidth;
                                int i17 = this.itemSpacing;
                                imageReceiver.setImageX((((((imageReceiver.getParam() - this.currentImage) + 1) * (i16 + i17)) + measuredWidth) - (i17 + i12)) + i11);
                            } else {
                                imageReceiver.setImageX(((imageReceiver.getParam() - this.currentImage) * (this.itemWidth + this.itemSpacing)) + measuredWidth + i11);
                            }
                        } else if (param < i14) {
                            imageReceiver.setImageX(((imageReceiver.getParam() - this.currentImage) * (this.itemWidth + this.itemSpacing)) + measuredWidth + i11);
                        } else if (param <= i15) {
                            imageReceiver.setImageX(measuredWidth + i7 + this.itemSpacing + (((imageReceiver.getParam() - this.currentImage) - 1) * (this.itemWidth + this.itemSpacing)) + i11);
                        } else {
                            int i18 = this.itemWidth;
                            int i19 = this.itemSpacing;
                            imageReceiver.setImageX(measuredWidth + i7 + this.itemSpacing + (((imageReceiver.getParam() - this.currentImage) - 2) * (i18 + i19)) + i19 + i12 + i11);
                        }
                        if (param == this.nextImage) {
                            imageReceiver.setImageWidth(i12 - i9);
                            imageReceiver.setImageX((int) (imageReceiver.getImageX() + ((float) (i9 / 2))));
                        } else {
                            imageReceiver.setImageWidth(this.itemWidth);
                        }
                    }
                    imageReceiver.setAlpha(this.drawAlpha);
                    imageReceiver.setRoundRadius(AndroidUtilities.dp(2.0f));
                    imageReceiver.draw(canvas);
                }
                long currentTimeMillis = System.currentTimeMillis();
                long j = currentTimeMillis - this.lastUpdateTime;
                if (j > 17) {
                    j = 17;
                }
                this.lastUpdateTime = currentTimeMillis;
                int i20 = this.animateToItem;
                if (i20 >= 0) {
                    float f6 = this.moveLineProgress;
                    if (f6 > 0.0f) {
                        float f7 = (float) j;
                        float f8 = f6 - (f7 / (this.animateToItemFast ? 100.0f : 200.0f));
                        this.moveLineProgress = f8;
                        if (i20 == this.currentImage) {
                            float f9 = this.currentItemProgress;
                            if (f9 < 1.0f) {
                                float f10 = f9 + (f7 / 200.0f);
                                this.currentItemProgress = f10;
                                if (f10 > 1.0f) {
                                    this.currentItemProgress = 1.0f;
                                }
                            }
                            int i21 = this.animateToDXStart;
                            this.drawDx = i21 + ((int) Math.ceil((double) (this.currentItemProgress * ((float) (this.animateToDX - i21)))));
                        } else {
                            CubicBezierInterpolator cubicBezierInterpolator = CubicBezierInterpolator.EASE_OUT;
                            float interpolation = cubicBezierInterpolator.getInterpolation(1.0f - f8);
                            this.nextItemProgress = interpolation;
                            if (this.stopedScrolling) {
                                float f11 = this.currentItemProgress;
                                if (f11 > 0.0f) {
                                    float f12 = f11 - (f7 / 200.0f);
                                    this.currentItemProgress = f12;
                                    if (f12 < 0.0f) {
                                        this.currentItemProgress = 0.0f;
                                    }
                                }
                                int i22 = this.animateToDXStart;
                                this.drawDx = i22 + ((int) Math.ceil((double) (interpolation * ((float) (this.animateToDX - i22)))));
                            } else {
                                this.currentItemProgress = cubicBezierInterpolator.getInterpolation(this.moveLineProgress);
                                this.drawDx = (int) Math.ceil((double) (this.nextItemProgress * ((float) this.animateToDX)));
                            }
                        }
                        if (this.moveLineProgress <= 0.0f) {
                            this.currentImage = this.animateToItem;
                            this.moveLineProgress = 1.0f;
                            this.currentItemProgress = 1.0f;
                            this.nextItemProgress = 0.0f;
                            this.moving = false;
                            this.stopedScrolling = false;
                            this.drawDx = 0;
                            this.animateToItem = -1;
                            this.animateToItemFast = false;
                        }
                    }
                    fillImages(true, this.drawDx);
                    invalidate();
                }
                if (this.scrolling) {
                    float f13 = this.currentItemProgress;
                    if (f13 > 0.0f) {
                        float f14 = f13 - (((float) j) / 200.0f);
                        this.currentItemProgress = f14;
                        if (f14 < 0.0f) {
                            this.currentItemProgress = 0.0f;
                        }
                        invalidate();
                    }
                }
                if (!this.scroll.isFinished()) {
                    if (this.scroll.computeScrollOffset()) {
                        this.drawDx = this.scroll.getCurrX();
                        updateAfterScroll();
                        invalidate();
                    }
                    if (this.scroll.isFinished()) {
                        stopScrolling();
                    }
                }
            }
        }
    }

    public void setDelegate(GroupedPhotosListViewDelegate groupedPhotosListViewDelegate) {
        this.delegate = groupedPhotosListViewDelegate;
    }

    public boolean hasPhotos() {
        ValueAnimator valueAnimator;
        return this.hasPhotos && this.hideAnimator == null && (this.drawAlpha > 0.0f || !this.animateBackground || ((valueAnimator = this.showAnimator) != null && valueAnimator.isStarted()));
    }

    public void setAnimationsEnabled(boolean z) {
        if (this.animationsEnabled != z) {
            this.animationsEnabled = z;
            if (!z) {
                ValueAnimator valueAnimator = this.showAnimator;
                if (valueAnimator != null) {
                    valueAnimator.cancel();
                    this.showAnimator = null;
                }
                ValueAnimator valueAnimator2 = this.hideAnimator;
                if (valueAnimator2 != null) {
                    valueAnimator2.cancel();
                    this.hideAnimator = null;
                }
                this.drawAlpha = 0.0f;
                invalidate();
            }
        }
    }

    public void setAnimateBackground(boolean z) {
        this.animateBackground = z;
    }

    public void reset() {
        this.hasPhotos = false;
        if (this.animationsEnabled) {
            this.drawAlpha = 0.0f;
        }
    }
}
