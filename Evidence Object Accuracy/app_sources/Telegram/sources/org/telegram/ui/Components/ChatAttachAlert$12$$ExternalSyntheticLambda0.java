package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.ChatAttachAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlert$12$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatAttachAlert.AnonymousClass12 f$0;

    public /* synthetic */ ChatAttachAlert$12$$ExternalSyntheticLambda0(ChatAttachAlert.AnonymousClass12 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$afterTextChanged$0(valueAnimator);
    }
}
