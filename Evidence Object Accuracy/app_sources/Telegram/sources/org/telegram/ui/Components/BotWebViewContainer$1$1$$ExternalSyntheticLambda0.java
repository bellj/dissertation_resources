package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.BotWebViewContainer;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewContainer$1$1$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ BotWebViewContainer.AnonymousClass1.AnonymousClass1 f$0;

    public /* synthetic */ BotWebViewContainer$1$1$$ExternalSyntheticLambda0(BotWebViewContainer.AnonymousClass1.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        BotWebViewContainer.AnonymousClass1.AnonymousClass1.$r8$lambda$CecDRMloyjOK052qViQRhv5ku4s(this.f$0, valueAnimator);
    }
}
