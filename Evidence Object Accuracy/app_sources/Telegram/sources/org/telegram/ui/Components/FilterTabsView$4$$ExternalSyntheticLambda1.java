package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.FilterTabsView;

/* loaded from: classes3.dex */
public final /* synthetic */ class FilterTabsView$4$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ FilterTabsView.TabView f$0;

    public /* synthetic */ FilterTabsView$4$$ExternalSyntheticLambda1(FilterTabsView.TabView tabView) {
        this.f$0 = tabView;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        FilterTabsView.AnonymousClass4.$r8$lambda$xM13lshM6xwbpnoUNazowudLaqI(this.f$0, valueAnimator);
    }
}
