package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.PhotoFilterView;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoFilterView$ToolsAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PhotoFilterView.ToolsAdapter f$0;

    public /* synthetic */ PhotoFilterView$ToolsAdapter$$ExternalSyntheticLambda0(PhotoFilterView.ToolsAdapter toolsAdapter) {
        this.f$0 = toolsAdapter;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$1(view);
    }
}
