package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

/* loaded from: classes3.dex */
public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda108 implements NumberPicker.Formatter {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda108 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda108();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda108() {
    }

    @Override // org.telegram.ui.Components.NumberPicker.Formatter
    public final String format(int i) {
        return AlertsCreator.lambda$createCalendarPickerDialog$80(i);
    }
}
