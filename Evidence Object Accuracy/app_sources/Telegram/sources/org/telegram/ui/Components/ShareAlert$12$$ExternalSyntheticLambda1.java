package org.telegram.ui.Components;

import org.telegram.messenger.AndroidUtilities;
import org.telegram.ui.Components.ShareAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class ShareAlert$12$$ExternalSyntheticLambda1 implements AndroidUtilities.IntColorCallback {
    public final /* synthetic */ ShareAlert.AnonymousClass12 f$0;

    public /* synthetic */ ShareAlert$12$$ExternalSyntheticLambda1(ShareAlert.AnonymousClass12 r1) {
        this.f$0 = r1;
    }

    @Override // org.telegram.messenger.AndroidUtilities.IntColorCallback
    public final void run(int i) {
        this.f$0.lambda$hidePopup$2(i);
    }
}
