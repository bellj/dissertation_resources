package org.telegram.ui.Components;

import java.util.ArrayList;
import org.telegram.ui.Components.GroupVoipInviteAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class GroupVoipInviteAlert$SearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GroupVoipInviteAlert.SearchAdapter f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ ArrayList f$2;

    public /* synthetic */ GroupVoipInviteAlert$SearchAdapter$$ExternalSyntheticLambda0(GroupVoipInviteAlert.SearchAdapter searchAdapter, int i, ArrayList arrayList) {
        this.f$0 = searchAdapter;
        this.f$1 = i;
        this.f$2 = arrayList;
    }

    @Override // java.lang.Runnable
    public final void run() {
        GroupVoipInviteAlert.SearchAdapter.m1126$r8$lambda$4nZ20ICSvnJLPBoi2FJXb2kTBE(this.f$0, this.f$1, this.f$2);
    }
}
