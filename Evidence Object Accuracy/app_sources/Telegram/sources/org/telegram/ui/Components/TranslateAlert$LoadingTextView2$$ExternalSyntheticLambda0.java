package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.TranslateAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class TranslateAlert$LoadingTextView2$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ TranslateAlert.LoadingTextView2 f$0;

    public /* synthetic */ TranslateAlert$LoadingTextView2$$ExternalSyntheticLambda0(TranslateAlert.LoadingTextView2 loadingTextView2) {
        this.f$0 = loadingTextView2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$loaded$1(valueAnimator);
    }
}
