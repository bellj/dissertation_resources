package org.telegram.ui.Components;

import android.webkit.PermissionRequest;
import androidx.core.util.Consumer;
import org.telegram.ui.Components.BotWebViewContainer;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewContainer$4$$ExternalSyntheticLambda2 implements Consumer {
    public final /* synthetic */ BotWebViewContainer.AnonymousClass4 f$0;
    public final /* synthetic */ PermissionRequest f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ BotWebViewContainer$4$$ExternalSyntheticLambda2(BotWebViewContainer.AnonymousClass4 r1, PermissionRequest permissionRequest, String str) {
        this.f$0 = r1;
        this.f$1 = permissionRequest;
        this.f$2 = str;
    }

    @Override // androidx.core.util.Consumer
    public final void accept(Object obj) {
        BotWebViewContainer.AnonymousClass4.m946$r8$lambda$SmZArWQXHbVcaU4R7PI_mHK34g(this.f$0, this.f$1, this.f$2, (Boolean) obj);
    }
}
