package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.ChatAttachAlertPollLayout;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlertPollLayout$ListAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ChatAttachAlertPollLayout.ListAdapter f$0;

    public /* synthetic */ ChatAttachAlertPollLayout$ListAdapter$$ExternalSyntheticLambda0(ChatAttachAlertPollLayout.ListAdapter listAdapter) {
        this.f$0 = listAdapter;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
