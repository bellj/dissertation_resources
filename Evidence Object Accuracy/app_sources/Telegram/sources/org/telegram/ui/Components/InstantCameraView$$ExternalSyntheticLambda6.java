package org.telegram.ui.Components;

import java.util.Comparator;
import org.telegram.messenger.camera.Size;

/* loaded from: classes3.dex */
public final /* synthetic */ class InstantCameraView$$ExternalSyntheticLambda6 implements Comparator {
    public static final /* synthetic */ InstantCameraView$$ExternalSyntheticLambda6 INSTANCE = new InstantCameraView$$ExternalSyntheticLambda6();

    private /* synthetic */ InstantCameraView$$ExternalSyntheticLambda6() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return InstantCameraView.lambda$chooseOptimalSize$2((Size) obj, (Size) obj2);
    }
}
