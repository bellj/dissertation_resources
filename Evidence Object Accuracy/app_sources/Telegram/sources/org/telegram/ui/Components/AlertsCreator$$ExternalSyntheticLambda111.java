package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

/* loaded from: classes3.dex */
public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda111 implements NumberPicker.Formatter {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda111 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda111();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda111() {
    }

    @Override // org.telegram.ui.Components.NumberPicker.Formatter
    public final String format(int i) {
        return AlertsCreator.lambda$createDatePickerDialog$59(i);
    }
}
