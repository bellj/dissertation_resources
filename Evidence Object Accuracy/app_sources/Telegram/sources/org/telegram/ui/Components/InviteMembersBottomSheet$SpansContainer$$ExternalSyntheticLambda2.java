package org.telegram.ui.Components;

import org.telegram.ui.Components.InviteMembersBottomSheet;

/* loaded from: classes3.dex */
public final /* synthetic */ class InviteMembersBottomSheet$SpansContainer$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ InviteMembersBottomSheet.SpansContainer f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ InviteMembersBottomSheet$SpansContainer$$ExternalSyntheticLambda2(InviteMembersBottomSheet.SpansContainer spansContainer, int i) {
        this.f$0 = spansContainer;
        this.f$1 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onMeasure$2(this.f$1);
    }
}
