package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class EmbedBottomSheet$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda5 INSTANCE = new EmbedBottomSheet$$ExternalSyntheticLambda5();

    private /* synthetic */ EmbedBottomSheet$$ExternalSyntheticLambda5() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return EmbedBottomSheet.lambda$new$1(view, motionEvent);
    }
}
