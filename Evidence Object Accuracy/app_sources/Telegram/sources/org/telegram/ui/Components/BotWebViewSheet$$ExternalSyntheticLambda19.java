package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda19 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda19 INSTANCE = new BotWebViewSheet$$ExternalSyntheticLambda19();

    private /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda19() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Setter
    public final void set(Object obj, float f) {
        BotWebViewSheet.$r8$lambda$CPy87Zl1ZXcMFanswDb43SNMzW0((BotWebViewSheet) obj, f);
    }
}
