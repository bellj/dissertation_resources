package org.telegram.ui.Components;

import org.telegram.ui.Components.ChatAttachAlertContactsLayout;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlertContactsLayout$UserCell$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ ChatAttachAlertContactsLayout.UserCell f$0;
    public final /* synthetic */ CharSequence f$1;

    public /* synthetic */ ChatAttachAlertContactsLayout$UserCell$$ExternalSyntheticLambda2(ChatAttachAlertContactsLayout.UserCell userCell, CharSequence charSequence) {
        this.f$0 = userCell;
        this.f$1 = charSequence;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$setData$0(this.f$1);
    }
}
