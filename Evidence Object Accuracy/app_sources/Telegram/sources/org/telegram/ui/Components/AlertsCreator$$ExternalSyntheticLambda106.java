package org.telegram.ui.Components;

import org.telegram.ui.Components.NumberPicker;

/* loaded from: classes3.dex */
public final /* synthetic */ class AlertsCreator$$ExternalSyntheticLambda106 implements NumberPicker.Formatter {
    public static final /* synthetic */ AlertsCreator$$ExternalSyntheticLambda106 INSTANCE = new AlertsCreator$$ExternalSyntheticLambda106();

    private /* synthetic */ AlertsCreator$$ExternalSyntheticLambda106() {
    }

    @Override // org.telegram.ui.Components.NumberPicker.Formatter
    public final String format(int i) {
        return AlertsCreator.lambda$createCalendarPickerDialog$77(i);
    }
}
