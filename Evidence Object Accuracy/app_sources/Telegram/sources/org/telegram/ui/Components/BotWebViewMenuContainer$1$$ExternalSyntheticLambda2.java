package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.BotWebViewMenuContainer;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewMenuContainer$1$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ BotWebViewMenuContainer.AnonymousClass1 f$0;

    public /* synthetic */ BotWebViewMenuContainer$1$$ExternalSyntheticLambda2(BotWebViewMenuContainer.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        BotWebViewMenuContainer.AnonymousClass1.$r8$lambda$sdsn4887fBmOtPyhUe0N9g6tJJU(this.f$0, view);
    }
}
