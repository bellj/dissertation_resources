package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda1 INSTANCE = new ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda1();

    private /* synthetic */ ChatAttachAlertDocumentLayout$$ExternalSyntheticLambda1() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatAttachAlertDocumentLayout.lambda$new$0(view, motionEvent);
    }
}
