package org.telegram.ui.Components.Premium.GLIcon;

import org.telegram.ui.Components.Premium.GLIcon.GLIconTextureView;

/* loaded from: classes3.dex */
public final /* synthetic */ class GLIconTextureView$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GLIconTextureView.AnonymousClass1 f$0;
    public final /* synthetic */ float f$1;
    public final /* synthetic */ float f$2;

    public /* synthetic */ GLIconTextureView$1$$ExternalSyntheticLambda0(GLIconTextureView.AnonymousClass1 r1, float f, float f2) {
        this.f$0 = r1;
        this.f$1 = f;
        this.f$2 = f2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        GLIconTextureView.AnonymousClass1.$r8$lambda$jds_Jdn3oxzqp2KHvqBXbqjMnSs(this.f$0, this.f$1, this.f$2);
    }
}
