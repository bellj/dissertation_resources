package org.telegram.ui.Components;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProximitySheet$$ExternalSyntheticLambda1 implements View.OnTouchListener {
    public static final /* synthetic */ ProximitySheet$$ExternalSyntheticLambda1 INSTANCE = new ProximitySheet$$ExternalSyntheticLambda1();

    private /* synthetic */ ProximitySheet$$ExternalSyntheticLambda1() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ProximitySheet.$r8$lambda$8JSgfDriR7Sm_A4W_RpmFrk2Nc0(view, motionEvent);
    }
}
