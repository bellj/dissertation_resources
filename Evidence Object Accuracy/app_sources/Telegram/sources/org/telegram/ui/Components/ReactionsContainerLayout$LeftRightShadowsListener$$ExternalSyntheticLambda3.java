package org.telegram.ui.Components;

import org.telegram.ui.Components.ReactionsContainerLayout;

/* loaded from: classes3.dex */
public final /* synthetic */ class ReactionsContainerLayout$LeftRightShadowsListener$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ ReactionsContainerLayout.LeftRightShadowsListener f$0;

    public /* synthetic */ ReactionsContainerLayout$LeftRightShadowsListener$$ExternalSyntheticLambda3(ReactionsContainerLayout.LeftRightShadowsListener leftRightShadowsListener) {
        this.f$0 = leftRightShadowsListener;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onScrolled$3();
    }
}
