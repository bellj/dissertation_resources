package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.RecyclerListView;
import org.telegram.ui.Components.ThemeEditorView;

/* loaded from: classes3.dex */
public final /* synthetic */ class ThemeEditorView$EditorAlert$$ExternalSyntheticLambda5 implements RecyclerListView.OnItemClickListener {
    public final /* synthetic */ ThemeEditorView.EditorAlert f$0;

    public /* synthetic */ ThemeEditorView$EditorAlert$$ExternalSyntheticLambda5(ThemeEditorView.EditorAlert editorAlert) {
        this.f$0 = editorAlert;
    }

    @Override // org.telegram.ui.Components.RecyclerListView.OnItemClickListener
    public final void onItemClick(View view, int i) {
        this.f$0.lambda$new$0(view, i);
    }
}
