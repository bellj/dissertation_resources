package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.TrendingStickersAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class TrendingStickersAlert$AlertContainerView$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ TrendingStickersAlert.AlertContainerView f$0;

    public /* synthetic */ TrendingStickersAlert$AlertContainerView$$ExternalSyntheticLambda0(TrendingStickersAlert.AlertContainerView alertContainerView) {
        this.f$0 = alertContainerView;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setStatusBarVisible$0(valueAnimator);
    }
}
