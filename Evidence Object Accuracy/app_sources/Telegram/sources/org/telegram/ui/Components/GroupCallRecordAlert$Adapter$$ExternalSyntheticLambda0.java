package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.GroupCallRecordAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class GroupCallRecordAlert$Adapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ GroupCallRecordAlert.Adapter f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ GroupCallRecordAlert$Adapter$$ExternalSyntheticLambda0(GroupCallRecordAlert.Adapter adapter, int i) {
        this.f$0 = adapter;
        this.f$1 = i;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GroupCallRecordAlert.Adapter.$r8$lambda$LoVAikEs9tdkJHuwVhLe8qTTCQ0(this.f$0, this.f$1, view);
    }
}
