package org.telegram.ui.Components;

import android.view.View;
import android.widget.TextView;
import org.telegram.ui.Components.AudioPlayerAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class AudioPlayerAlert$9$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AudioPlayerAlert.AnonymousClass9 f$0;
    public final /* synthetic */ TextView f$1;

    public /* synthetic */ AudioPlayerAlert$9$$ExternalSyntheticLambda0(AudioPlayerAlert.AnonymousClass9 r1, TextView textView) {
        this.f$0 = r1;
        this.f$1 = textView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$createTextView$0(this.f$1, view);
    }
}
