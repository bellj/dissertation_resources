package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.AudioPlayerAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class AudioPlayerAlert$ClippingTextViewSwitcher$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AudioPlayerAlert.ClippingTextViewSwitcher f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ AudioPlayerAlert$ClippingTextViewSwitcher$$ExternalSyntheticLambda0(AudioPlayerAlert.ClippingTextViewSwitcher clippingTextViewSwitcher, int i) {
        this.f$0 = clippingTextViewSwitcher;
        this.f$1 = i;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$setText$0(this.f$1, valueAnimator);
    }
}
