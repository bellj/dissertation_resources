package org.telegram.ui.Components;

import org.telegram.ui.Components.AlertsCreator;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda55 implements AlertsCreator.ScheduleDatePickerDelegate {
    public static final /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda55 INSTANCE = new ChatActivityEnterView$$ExternalSyntheticLambda55();

    private /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda55() {
    }

    @Override // org.telegram.ui.Components.AlertsCreator.ScheduleDatePickerDelegate
    public final void didSelectDate(boolean z, int i) {
        ChatActivityEnterView.lambda$new$18(z, i);
    }
}
