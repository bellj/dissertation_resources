package org.telegram.ui.Components;

import android.animation.ValueAnimator;
import org.telegram.ui.Components.PhotoViewerCaptionEnterView;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoViewerCaptionEnterView$2$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ PhotoViewerCaptionEnterView.AnonymousClass2 f$0;

    public /* synthetic */ PhotoViewerCaptionEnterView$2$$ExternalSyntheticLambda0(PhotoViewerCaptionEnterView.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        PhotoViewerCaptionEnterView.AnonymousClass2.$r8$lambda$Kpzvncy5_cmP5zKv9lNHlpeDwJY(this.f$0, valueAnimator);
    }
}
