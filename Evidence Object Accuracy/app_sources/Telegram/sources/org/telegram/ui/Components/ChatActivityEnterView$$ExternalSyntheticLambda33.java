package org.telegram.ui.Components;

import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivityEnterView$$ExternalSyntheticLambda33 implements Runnable {
    public final /* synthetic */ View f$0;

    public /* synthetic */ ChatActivityEnterView$$ExternalSyntheticLambda33(View view) {
        this.f$0 = view;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.callOnClick();
    }
}
