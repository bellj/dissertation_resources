package org.telegram.ui.Components;

import org.telegram.ui.Components.ShareAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ShareAlert.ShareSearchAdapter f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ ShareAlert$ShareSearchAdapter$$ExternalSyntheticLambda0(ShareAlert.ShareSearchAdapter shareSearchAdapter, int i, String str) {
        this.f$0 = shareSearchAdapter;
        this.f$1 = i;
        this.f$2 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$searchDialogs$3(this.f$1, this.f$2);
    }
}
