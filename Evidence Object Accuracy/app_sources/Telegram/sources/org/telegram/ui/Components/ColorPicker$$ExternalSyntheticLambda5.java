package org.telegram.ui.Components;

import android.view.KeyEvent;
import android.widget.TextView;

/* loaded from: classes3.dex */
public final /* synthetic */ class ColorPicker$$ExternalSyntheticLambda5 implements TextView.OnEditorActionListener {
    public static final /* synthetic */ ColorPicker$$ExternalSyntheticLambda5 INSTANCE = new ColorPicker$$ExternalSyntheticLambda5();

    private /* synthetic */ ColorPicker$$ExternalSyntheticLambda5() {
    }

    @Override // android.widget.TextView.OnEditorActionListener
    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return ColorPicker.m1081$r8$lambda$Zc7eIW9hmbbuwjjRymCs8IqW4Q(textView, i, keyEvent);
    }
}
