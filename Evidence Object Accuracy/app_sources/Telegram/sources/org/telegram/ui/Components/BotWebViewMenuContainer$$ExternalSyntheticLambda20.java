package org.telegram.ui.Components;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewMenuContainer$$ExternalSyntheticLambda20 implements SimpleFloatPropertyCompat.Getter {
    public static final /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda20 INSTANCE = new BotWebViewMenuContainer$$ExternalSyntheticLambda20();

    private /* synthetic */ BotWebViewMenuContainer$$ExternalSyntheticLambda20() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Getter
    public final float get(Object obj) {
        return BotWebViewMenuContainer.$r8$lambda$AvzuPBoviy0aKUVCvG219Wo6Mo8((BotWebViewMenuContainer) obj);
    }
}
