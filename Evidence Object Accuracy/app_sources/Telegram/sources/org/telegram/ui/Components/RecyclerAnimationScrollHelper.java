package org.telegram.ui.Components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.SparseArray;
import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.telegram.messenger.BuildVars;
import org.telegram.messenger.MessagesController;
import org.telegram.tgnet.ConnectionsManager;
import org.telegram.ui.Cells.ChatMessageCell;
import org.telegram.ui.Components.RecyclerListView;

/* loaded from: classes3.dex */
public class RecyclerAnimationScrollHelper {
    private AnimationCallback animationCallback;
    private ValueAnimator animator;
    private LinearLayoutManager layoutManager;
    private HashMap<Long, View> oldStableIds = new HashMap<>();
    public SparseArray<View> positionToOldView = new SparseArray<>();
    private RecyclerListView recyclerView;
    private int scrollDirection;
    private ScrollListener scrollListener;

    /* loaded from: classes3.dex */
    public static class AnimationCallback {
        public void onEndAnimation() {
            throw null;
        }

        public void onStartAnimation() {
        }

        public void recycleView(View view) {
            throw null;
        }
    }

    /* loaded from: classes3.dex */
    public interface ScrollListener {
        void onScroll();
    }

    public RecyclerAnimationScrollHelper(RecyclerListView recyclerListView, LinearLayoutManager linearLayoutManager) {
        this.recyclerView = recyclerListView;
        this.layoutManager = linearLayoutManager;
    }

    public void scrollToPosition(int i, int i2, boolean z, boolean z2) {
        RecyclerListView recyclerListView = this.recyclerView;
        if (recyclerListView.fastScrollAnimationRunning) {
            return;
        }
        if (recyclerListView.getItemAnimator() != null && this.recyclerView.getItemAnimator().isRunning()) {
            return;
        }
        if (!z2 || this.scrollDirection == -1) {
            this.layoutManager.scrollToPositionWithOffset(i, i2, z);
            return;
        }
        int childCount = this.recyclerView.getChildCount();
        if (childCount == 0 || !MessagesController.getGlobalMainSettings().getBoolean("view_animations", true)) {
            this.layoutManager.scrollToPositionWithOffset(i, i2, z);
            return;
        }
        final boolean z3 = this.scrollDirection == 0;
        this.recyclerView.setScrollEnabled(false);
        final ArrayList arrayList = new ArrayList();
        this.positionToOldView.clear();
        final RecyclerView.Adapter adapter = this.recyclerView.getAdapter();
        this.oldStableIds.clear();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.recyclerView.getChildAt(i3);
            arrayList.add(childAt);
            this.positionToOldView.put(this.layoutManager.getPosition(childAt), childAt);
            if (adapter != null && adapter.hasStableIds()) {
                this.oldStableIds.put(Long.valueOf(((RecyclerView.LayoutParams) childAt.getLayoutParams()).mViewHolder.getItemId()), childAt);
            }
            if (childAt instanceof ChatMessageCell) {
                ((ChatMessageCell) childAt).setAnimationRunning(true, true);
            }
        }
        this.recyclerView.prepareForFastScroll();
        final AnimatableAdapter animatableAdapter = null;
        if (adapter instanceof AnimatableAdapter) {
            animatableAdapter = (AnimatableAdapter) adapter;
        }
        this.layoutManager.scrollToPositionWithOffset(i, i2, z);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        this.recyclerView.stopScroll();
        this.recyclerView.setVerticalScrollBarEnabled(false);
        AnimationCallback animationCallback = this.animationCallback;
        if (animationCallback != null) {
            animationCallback.onStartAnimation();
        }
        this.recyclerView.fastScrollAnimationRunning = true;
        if (animatableAdapter != null) {
            animatableAdapter.onAnimationStart();
        }
        this.recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.telegram.ui.Components.RecyclerAnimationScrollHelper.1
            @Override // android.view.View.OnLayoutChangeListener
            public void onLayoutChange(View view, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11) {
                int height;
                long j;
                View view2;
                final ArrayList arrayList2 = new ArrayList();
                RecyclerAnimationScrollHelper.this.recyclerView.stopScroll();
                int childCount2 = RecyclerAnimationScrollHelper.this.recyclerView.getChildCount();
                int i12 = 0;
                int i13 = 0;
                int i14 = 0;
                int i15 = 0;
                boolean z4 = false;
                for (int i16 = 0; i16 < childCount2; i16++) {
                    View childAt2 = RecyclerAnimationScrollHelper.this.recyclerView.getChildAt(i16);
                    arrayList2.add(childAt2);
                    if (childAt2.getTop() < i13) {
                        i13 = childAt2.getTop();
                    }
                    if (childAt2.getBottom() > i14) {
                        i14 = childAt2.getBottom();
                    }
                    if (childAt2 instanceof ChatMessageCell) {
                        ((ChatMessageCell) childAt2).setAnimationRunning(true, false);
                    }
                    RecyclerView.Adapter adapter2 = adapter;
                    if (adapter2 != null && adapter2.hasStableIds()) {
                        long itemId = adapter.getItemId(RecyclerAnimationScrollHelper.this.recyclerView.getChildAdapterPosition(childAt2));
                        if (RecyclerAnimationScrollHelper.this.oldStableIds.containsKey(Long.valueOf(itemId)) && (view2 = (View) RecyclerAnimationScrollHelper.this.oldStableIds.get(Long.valueOf(itemId))) != null) {
                            if (view2 instanceof ChatMessageCell) {
                                ((ChatMessageCell) view2).setAnimationRunning(false, false);
                            }
                            arrayList.remove(view2);
                            if (RecyclerAnimationScrollHelper.this.animationCallback != null) {
                                RecyclerAnimationScrollHelper.this.animationCallback.recycleView(view2);
                            }
                            int top = childAt2.getTop() - view2.getTop();
                            if (top != 0) {
                                i15 = top;
                            }
                            z4 = true;
                        }
                    }
                }
                RecyclerAnimationScrollHelper.this.oldStableIds.clear();
                Iterator it = arrayList.iterator();
                int i17 = ConnectionsManager.DEFAULT_DATACENTER_ID;
                int i18 = 0;
                while (it.hasNext()) {
                    View view3 = (View) it.next();
                    int bottom = view3.getBottom();
                    int top2 = view3.getTop();
                    if (bottom > i18) {
                        i18 = bottom;
                    }
                    if (top2 < i17) {
                        i17 = top2;
                    }
                    if (view3.getParent() == null) {
                        RecyclerAnimationScrollHelper.this.recyclerView.addView(view3);
                        RecyclerAnimationScrollHelper.this.layoutManager.ignoreView(view3);
                    }
                    if (view3 instanceof ChatMessageCell) {
                        ((ChatMessageCell) view3).setAnimationRunning(true, true);
                    }
                }
                if (i17 != Integer.MAX_VALUE) {
                    i12 = i17;
                }
                if (arrayList.isEmpty()) {
                    height = Math.abs(i15);
                } else {
                    if (!z3) {
                        i18 = RecyclerAnimationScrollHelper.this.recyclerView.getHeight() - i12;
                    }
                    height = (z3 ? -i13 : i14 - RecyclerAnimationScrollHelper.this.recyclerView.getHeight()) + i18;
                }
                if (RecyclerAnimationScrollHelper.this.animator != null) {
                    RecyclerAnimationScrollHelper.this.animator.removeAllListeners();
                    RecyclerAnimationScrollHelper.this.animator.cancel();
                }
                RecyclerAnimationScrollHelper.this.animator = ValueAnimator.ofFloat(0.0f, 1.0f);
                RecyclerAnimationScrollHelper.this.animator.addUpdateListener(new RecyclerAnimationScrollHelper$1$$ExternalSyntheticLambda0(this, arrayList, z3, height, arrayList2));
                RecyclerAnimationScrollHelper.this.animator.addListener(new AnimatorListenerAdapter() { // from class: org.telegram.ui.Components.RecyclerAnimationScrollHelper.1.1
                    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        if (RecyclerAnimationScrollHelper.this.animator != null) {
                            RecyclerAnimationScrollHelper.this.recyclerView.fastScrollAnimationRunning = false;
                            Iterator it2 = arrayList.iterator();
                            while (it2.hasNext()) {
                                View view4 = (View) it2.next();
                                if (view4 instanceof ChatMessageCell) {
                                    ((ChatMessageCell) view4).setAnimationRunning(false, true);
                                }
                                view4.setTranslationY(0.0f);
                                RecyclerAnimationScrollHelper.this.layoutManager.stopIgnoringView(view4);
                                RecyclerAnimationScrollHelper.this.recyclerView.removeView(view4);
                                if (RecyclerAnimationScrollHelper.this.animationCallback != null) {
                                    RecyclerAnimationScrollHelper.this.animationCallback.recycleView(view4);
                                }
                            }
                            RecyclerAnimationScrollHelper.this.recyclerView.setScrollEnabled(true);
                            RecyclerAnimationScrollHelper.this.recyclerView.setVerticalScrollBarEnabled(true);
                            if (BuildVars.DEBUG_PRIVATE_VERSION) {
                                if (RecyclerAnimationScrollHelper.this.recyclerView.mChildHelper.getChildCount() != RecyclerAnimationScrollHelper.this.recyclerView.getChildCount()) {
                                    throw new RuntimeException("views count in child helper must be quals views count in recycler view");
                                } else if (RecyclerAnimationScrollHelper.this.recyclerView.mChildHelper.getHiddenChildCount() != 0) {
                                    throw new RuntimeException("hidden child count must be 0");
                                }
                            }
                            int childCount3 = RecyclerAnimationScrollHelper.this.recyclerView.getChildCount();
                            for (int i19 = 0; i19 < childCount3; i19++) {
                                View childAt3 = RecyclerAnimationScrollHelper.this.recyclerView.getChildAt(i19);
                                if (childAt3 instanceof ChatMessageCell) {
                                    ((ChatMessageCell) childAt3).setAnimationRunning(false, false);
                                }
                                childAt3.setTranslationY(0.0f);
                            }
                            Iterator it3 = arrayList2.iterator();
                            while (it3.hasNext()) {
                                View view5 = (View) it3.next();
                                if (view5 instanceof ChatMessageCell) {
                                    ((ChatMessageCell) view5).setAnimationRunning(false, false);
                                }
                                view5.setTranslationY(0.0f);
                            }
                            AnimatableAdapter animatableAdapter2 = animatableAdapter;
                            if (animatableAdapter2 != null) {
                                animatableAdapter2.onAnimationEnd();
                            }
                            if (RecyclerAnimationScrollHelper.this.animationCallback != null) {
                                RecyclerAnimationScrollHelper.this.animationCallback.onEndAnimation();
                            }
                            RecyclerAnimationScrollHelper.this.positionToOldView.clear();
                            RecyclerAnimationScrollHelper.this.animator = null;
                        }
                    }
                });
                RecyclerAnimationScrollHelper.this.recyclerView.removeOnLayoutChangeListener(this);
                if (z4) {
                    j = 600;
                } else {
                    long measuredHeight = (long) (((((float) height) / ((float) RecyclerAnimationScrollHelper.this.recyclerView.getMeasuredHeight())) + 1.0f) * 200.0f);
                    if (measuredHeight < 300) {
                        measuredHeight = 300;
                    }
                    j = Math.min(measuredHeight, 1300L);
                }
                RecyclerAnimationScrollHelper.this.animator.setDuration(j);
                RecyclerAnimationScrollHelper.this.animator.setInterpolator(CubicBezierInterpolator.EASE_OUT_QUINT);
                RecyclerAnimationScrollHelper.this.animator.start();
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void lambda$onLayoutChange$0(ArrayList arrayList2, boolean z4, int i4, ArrayList arrayList3, ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                int size = arrayList2.size();
                for (int i5 = 0; i5 < size; i5++) {
                    View view = (View) arrayList2.get(i5);
                    float y = view.getY();
                    if (view.getY() + ((float) view.getMeasuredHeight()) >= 0.0f && y <= ((float) RecyclerAnimationScrollHelper.this.recyclerView.getMeasuredHeight())) {
                        if (z4) {
                            view.setTranslationY(((float) (-i4)) * floatValue);
                        } else {
                            view.setTranslationY(((float) i4) * floatValue);
                        }
                    }
                }
                int size2 = arrayList3.size();
                for (int i6 = 0; i6 < size2; i6++) {
                    View view2 = (View) arrayList3.get(i6);
                    if (z4) {
                        view2.setTranslationY(((float) i4) * (1.0f - floatValue));
                    } else {
                        view2.setTranslationY(((float) (-i4)) * (1.0f - floatValue));
                    }
                }
                RecyclerAnimationScrollHelper.this.recyclerView.invalidate();
                if (RecyclerAnimationScrollHelper.this.scrollListener != null) {
                    RecyclerAnimationScrollHelper.this.scrollListener.onScroll();
                }
            }
        });
    }

    public void cancel() {
        ValueAnimator valueAnimator = this.animator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        clear();
    }

    private void clear() {
        this.recyclerView.setVerticalScrollBarEnabled(true);
        RecyclerListView recyclerListView = this.recyclerView;
        recyclerListView.fastScrollAnimationRunning = false;
        RecyclerView.Adapter adapter = recyclerListView.getAdapter();
        if (adapter instanceof AnimatableAdapter) {
            ((AnimatableAdapter) adapter).onAnimationEnd();
        }
        this.animator = null;
        int childCount = this.recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.recyclerView.getChildAt(i);
            childAt.setTranslationY(0.0f);
            if (childAt instanceof ChatMessageCell) {
                ((ChatMessageCell) childAt).setAnimationRunning(false, false);
            }
        }
    }

    public void setScrollDirection(int i) {
        this.scrollDirection = i;
    }

    public void setScrollListener(ScrollListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    public void setAnimationCallback(AnimationCallback animationCallback) {
        this.animationCallback = animationCallback;
    }

    /* loaded from: classes3.dex */
    public static abstract class AnimatableAdapter extends RecyclerListView.SelectionAdapter {
        public boolean animationRunning;
        private ArrayList<Integer> rangeInserted = new ArrayList<>();
        private ArrayList<Integer> rangeRemoved = new ArrayList<>();
        private boolean shouldNotifyDataSetChanged;

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyDataSetChanged() {
            if (!this.animationRunning) {
                super.notifyDataSetChanged();
            } else {
                this.shouldNotifyDataSetChanged = true;
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemInserted(int i) {
            if (!this.animationRunning) {
                super.notifyItemInserted(i);
                return;
            }
            this.rangeInserted.add(Integer.valueOf(i));
            this.rangeInserted.add(1);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRangeInserted(int i, int i2) {
            if (!this.animationRunning) {
                super.notifyItemRangeInserted(i, i2);
                return;
            }
            this.rangeInserted.add(Integer.valueOf(i));
            this.rangeInserted.add(Integer.valueOf(i2));
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRemoved(int i) {
            if (!this.animationRunning) {
                super.notifyItemRemoved(i);
                return;
            }
            this.rangeRemoved.add(Integer.valueOf(i));
            this.rangeRemoved.add(1);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRangeRemoved(int i, int i2) {
            if (!this.animationRunning) {
                super.notifyItemRangeRemoved(i, i2);
                return;
            }
            this.rangeRemoved.add(Integer.valueOf(i));
            this.rangeRemoved.add(Integer.valueOf(i2));
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemChanged(int i) {
            if (!this.animationRunning) {
                super.notifyItemChanged(i);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public void notifyItemRangeChanged(int i, int i2) {
            if (!this.animationRunning) {
                super.notifyItemRangeChanged(i, i2);
            }
        }

        public void onAnimationStart() {
            this.animationRunning = true;
            this.shouldNotifyDataSetChanged = false;
            this.rangeInserted.clear();
            this.rangeRemoved.clear();
        }

        public void onAnimationEnd() {
            this.animationRunning = false;
            if (this.shouldNotifyDataSetChanged || !this.rangeInserted.isEmpty() || !this.rangeRemoved.isEmpty()) {
                notifyDataSetChanged();
            }
        }
    }
}
