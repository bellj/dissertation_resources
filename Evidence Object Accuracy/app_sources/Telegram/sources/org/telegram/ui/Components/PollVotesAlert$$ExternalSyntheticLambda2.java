package org.telegram.ui.Components;

import java.util.Comparator;
import org.telegram.ui.Components.PollVotesAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class PollVotesAlert$$ExternalSyntheticLambda2 implements Comparator {
    public static final /* synthetic */ PollVotesAlert$$ExternalSyntheticLambda2 INSTANCE = new PollVotesAlert$$ExternalSyntheticLambda2();

    private /* synthetic */ PollVotesAlert$$ExternalSyntheticLambda2() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return PollVotesAlert.lambda$updateButtons$5((PollVotesAlert.Button) obj, (PollVotesAlert.Button) obj2);
    }
}
