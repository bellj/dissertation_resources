package org.telegram.ui.Components;

import androidx.core.util.Consumer;
import org.telegram.ui.Components.Bulletin;

/* loaded from: classes3.dex */
public final /* synthetic */ class Bulletin$2$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ Bulletin.AnonymousClass2 f$0;

    public /* synthetic */ Bulletin$2$$ExternalSyntheticLambda0(Bulletin.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    @Override // androidx.core.util.Consumer
    public final void accept(Object obj) {
        this.f$0.lambda$onLayoutChange$1((Float) obj);
    }
}
