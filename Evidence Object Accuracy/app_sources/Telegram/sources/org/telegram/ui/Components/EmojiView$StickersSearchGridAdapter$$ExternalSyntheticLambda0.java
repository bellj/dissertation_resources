package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.EmojiView;

/* loaded from: classes3.dex */
public final /* synthetic */ class EmojiView$StickersSearchGridAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ EmojiView.StickersSearchGridAdapter f$0;

    public /* synthetic */ EmojiView$StickersSearchGridAdapter$$ExternalSyntheticLambda0(EmojiView.StickersSearchGridAdapter stickersSearchGridAdapter) {
        this.f$0 = stickersSearchGridAdapter;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onCreateViewHolder$0(view);
    }
}
