package org.telegram.ui.Components.voip;

import org.telegram.ui.Components.SimpleFloatPropertyCompat;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes3.dex */
public final /* synthetic */ class RTMPStreamPipOverlay$$ExternalSyntheticLambda8 implements SimpleFloatPropertyCompat.Setter {
    public static final /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda8 INSTANCE = new RTMPStreamPipOverlay$$ExternalSyntheticLambda8();

    private /* synthetic */ RTMPStreamPipOverlay$$ExternalSyntheticLambda8() {
    }

    @Override // org.telegram.ui.Components.SimpleFloatPropertyCompat.Setter
    public final void set(Object obj, float f) {
        RTMPStreamPipOverlay.lambda$static$1((RTMPStreamPipOverlay) obj, f);
    }
}
