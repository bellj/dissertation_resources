package org.telegram.ui.Components;

import android.view.View;
import android.view.WindowInsets;

/* loaded from: classes3.dex */
public final /* synthetic */ class BotWebViewSheet$$ExternalSyntheticLambda1 implements View.OnApplyWindowInsetsListener {
    public static final /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda1 INSTANCE = new BotWebViewSheet$$ExternalSyntheticLambda1();

    private /* synthetic */ BotWebViewSheet$$ExternalSyntheticLambda1() {
    }

    @Override // android.view.View.OnApplyWindowInsetsListener
    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return BotWebViewSheet.m956$r8$lambda$E38SqSFZwX5hrCa4JMphjXj214(view, windowInsets);
    }
}
