package org.telegram.ui.Components;

import java.util.ArrayList;
import org.telegram.ui.Components.GroupVoipInviteAlert;

/* loaded from: classes3.dex */
public final /* synthetic */ class GroupVoipInviteAlert$SearchAdapter$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ GroupVoipInviteAlert.SearchAdapter f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ int f$2;
    public final /* synthetic */ ArrayList f$3;

    public /* synthetic */ GroupVoipInviteAlert$SearchAdapter$$ExternalSyntheticLambda3(GroupVoipInviteAlert.SearchAdapter searchAdapter, String str, int i, ArrayList arrayList) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
        this.f$2 = i;
        this.f$3 = arrayList;
    }

    @Override // java.lang.Runnable
    public final void run() {
        GroupVoipInviteAlert.SearchAdapter.$r8$lambda$Fv1oi_u___PWbcmcedFAJ4NyRkA(this.f$0, this.f$1, this.f$2, this.f$3);
    }
}
