package org.telegram.ui.Components;

import android.view.View;
import org.telegram.ui.Components.EmojiView;

/* loaded from: classes3.dex */
public final /* synthetic */ class EmojiView$SearchField$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ EmojiView.SearchField f$0;

    public /* synthetic */ EmojiView$SearchField$$ExternalSyntheticLambda0(EmojiView.SearchField searchField) {
        this.f$0 = searchField;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
