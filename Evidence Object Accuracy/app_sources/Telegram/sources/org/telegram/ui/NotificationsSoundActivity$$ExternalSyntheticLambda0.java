package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class NotificationsSoundActivity$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public static final /* synthetic */ NotificationsSoundActivity$$ExternalSyntheticLambda0 INSTANCE = new NotificationsSoundActivity$$ExternalSyntheticLambda0();

    private /* synthetic */ NotificationsSoundActivity$$ExternalSyntheticLambda0() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return NotificationsSoundActivity.lambda$createView$0(view, motionEvent);
    }
}
