package org.telegram.ui;

import org.telegram.ui.GroupStickersActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class GroupStickersActivity$SearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GroupStickersActivity.SearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ GroupStickersActivity$SearchAdapter$$ExternalSyntheticLambda0(GroupStickersActivity.SearchAdapter searchAdapter, String str) {
        this.f$0 = searchAdapter;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onSearchStickers$2(this.f$1);
    }
}
