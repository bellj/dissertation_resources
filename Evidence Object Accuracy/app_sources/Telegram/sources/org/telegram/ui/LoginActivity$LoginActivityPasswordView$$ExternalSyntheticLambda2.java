package org.telegram.ui;

import android.content.Context;
import android.view.View;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityPasswordView$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityPasswordView f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ LoginActivity$LoginActivityPasswordView$$ExternalSyntheticLambda2(LoginActivity.LoginActivityPasswordView loginActivityPasswordView, Context context) {
        this.f$0 = loginActivityPasswordView;
        this.f$1 = context;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$6(this.f$1, view);
    }
}
