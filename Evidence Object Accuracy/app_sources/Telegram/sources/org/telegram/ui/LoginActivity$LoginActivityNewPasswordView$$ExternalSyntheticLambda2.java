package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityNewPasswordView$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityNewPasswordView f$0;

    public /* synthetic */ LoginActivity$LoginActivityNewPasswordView$$ExternalSyntheticLambda2(LoginActivity.LoginActivityNewPasswordView loginActivityNewPasswordView) {
        this.f$0 = loginActivityNewPasswordView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$3(view);
    }
}
