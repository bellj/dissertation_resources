package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityRecoverView f$0;

    public /* synthetic */ LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda0(LoginActivity.LoginActivityRecoverView loginActivityRecoverView) {
        this.f$0 = loginActivityRecoverView;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$new$3(dialogInterface, i);
    }
}
