package org.telegram.ui;

import android.view.View;
import org.telegram.ui.PasscodeActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class PasscodeActivity$RLottieImageHolderView$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ PasscodeActivity.RLottieImageHolderView f$0;

    public /* synthetic */ PasscodeActivity$RLottieImageHolderView$$ExternalSyntheticLambda0(PasscodeActivity.RLottieImageHolderView rLottieImageHolderView) {
        this.f$0 = rLottieImageHolderView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
