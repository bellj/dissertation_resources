package org.telegram.ui;

import org.telegram.ui.DialogsActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class DialogsActivity$9$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ DialogsActivity.ViewPage f$0;

    public /* synthetic */ DialogsActivity$9$$ExternalSyntheticLambda0(DialogsActivity.ViewPage viewPage) {
        this.f$0 = viewPage;
    }

    @Override // java.lang.Runnable
    public final void run() {
        DialogsActivity.AnonymousClass9.lambda$onLayoutChildren$0(this.f$0);
    }
}
