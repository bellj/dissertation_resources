package org.telegram.ui.ActionBar;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class BottomSheet$$ExternalSyntheticLambda5 implements View.OnTouchListener {
    public static final /* synthetic */ BottomSheet$$ExternalSyntheticLambda5 INSTANCE = new BottomSheet$$ExternalSyntheticLambda5();

    private /* synthetic */ BottomSheet$$ExternalSyntheticLambda5() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return BottomSheet.lambda$onCreate$2(view, motionEvent);
    }
}
