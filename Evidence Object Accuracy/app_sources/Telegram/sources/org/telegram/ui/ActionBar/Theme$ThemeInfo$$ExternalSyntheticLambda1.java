package org.telegram.ui.ActionBar;

import java.io.File;
import org.telegram.ui.ActionBar.Theme;

/* loaded from: classes3.dex */
public final /* synthetic */ class Theme$ThemeInfo$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Theme.ThemeInfo f$0;
    public final /* synthetic */ File f$1;

    public /* synthetic */ Theme$ThemeInfo$$ExternalSyntheticLambda1(Theme.ThemeInfo themeInfo, File file) {
        this.f$0 = themeInfo;
        this.f$1 = file;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$didReceivedNotification$0(this.f$1);
    }
}
