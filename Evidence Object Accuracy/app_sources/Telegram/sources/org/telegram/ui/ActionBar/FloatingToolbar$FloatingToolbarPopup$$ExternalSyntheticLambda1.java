package org.telegram.ui.ActionBar;

import android.view.View;
import android.widget.AdapterView;
import org.telegram.ui.ActionBar.FloatingToolbar;

/* loaded from: classes3.dex */
public final /* synthetic */ class FloatingToolbar$FloatingToolbarPopup$$ExternalSyntheticLambda1 implements AdapterView.OnItemClickListener {
    public final /* synthetic */ FloatingToolbar.FloatingToolbarPopup f$0;
    public final /* synthetic */ FloatingToolbar.FloatingToolbarPopup.OverflowPanel f$1;

    public /* synthetic */ FloatingToolbar$FloatingToolbarPopup$$ExternalSyntheticLambda1(FloatingToolbar.FloatingToolbarPopup floatingToolbarPopup, FloatingToolbar.FloatingToolbarPopup.OverflowPanel overflowPanel) {
        this.f$0 = floatingToolbarPopup;
        this.f$1 = overflowPanel;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.f$0.lambda$createOverflowPanel$1(this.f$1, adapterView, view, i, j);
    }
}
