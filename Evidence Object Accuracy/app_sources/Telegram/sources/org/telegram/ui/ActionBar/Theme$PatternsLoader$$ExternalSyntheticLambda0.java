package org.telegram.ui.ActionBar;

import java.util.ArrayList;
import org.telegram.ui.ActionBar.Theme;

/* loaded from: classes3.dex */
public final /* synthetic */ class Theme$PatternsLoader$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Theme.PatternsLoader f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ Theme$PatternsLoader$$ExternalSyntheticLambda0(Theme.PatternsLoader patternsLoader, ArrayList arrayList) {
        this.f$0 = patternsLoader;
        this.f$1 = arrayList;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$new$1(this.f$1);
    }
}
