package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CallLogActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CallLogActivity$GroupCallCell$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CallLogActivity.GroupCallCell f$0;

    public /* synthetic */ CallLogActivity$GroupCallCell$$ExternalSyntheticLambda0(CallLogActivity.GroupCallCell groupCallCell) {
        this.f$0 = groupCallCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CallLogActivity.GroupCallCell.m665$r8$lambda$OD32mbLf8K3g0ZJCVmOV_dVeIg(this.f$0, view);
    }
}
