package org.telegram.ui;

import android.view.View;
import org.telegram.ui.PassportActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class PassportActivity$PhoneConfirmationView$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ PassportActivity.PhoneConfirmationView f$0;

    public /* synthetic */ PassportActivity$PhoneConfirmationView$$ExternalSyntheticLambda2(PassportActivity.PhoneConfirmationView phoneConfirmationView) {
        this.f$0 = phoneConfirmationView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
