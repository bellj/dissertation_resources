package org.telegram.ui;

import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProfileActivity$ListAdapter$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ ProfileActivity f$0;

    public /* synthetic */ ProfileActivity$ListAdapter$$ExternalSyntheticLambda2(ProfileActivity profileActivity) {
        this.f$0 = profileActivity;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ProfileActivity.access$25500(this.f$0, view);
    }
}
