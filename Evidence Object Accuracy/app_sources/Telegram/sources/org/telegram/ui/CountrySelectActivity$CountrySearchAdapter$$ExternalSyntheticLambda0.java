package org.telegram.ui;

import org.telegram.ui.CountrySelectActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CountrySelectActivity$CountrySearchAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CountrySelectActivity.CountrySearchAdapter f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ CountrySelectActivity$CountrySearchAdapter$$ExternalSyntheticLambda0(CountrySelectActivity.CountrySearchAdapter countrySearchAdapter, String str) {
        this.f$0 = countrySearchAdapter;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        CountrySelectActivity.CountrySearchAdapter.$r8$lambda$MHrJ4deumg9PEOwj2Qc7UmUOoBI(this.f$0, this.f$1);
    }
}
