package org.telegram.ui;

import org.telegram.ui.InviteContactsActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class InviteContactsActivity$InviteAdapter$1$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ InviteContactsActivity.InviteAdapter.AnonymousClass1 f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ InviteContactsActivity$InviteAdapter$1$$ExternalSyntheticLambda1(InviteContactsActivity.InviteAdapter.AnonymousClass1 r1, String str) {
        this.f$0 = r1;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$1(this.f$1);
    }
}
