package org.telegram.ui;

import java.util.Comparator;
import org.telegram.ui.ProfileActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ProfileActivity$SearchAdapter$$ExternalSyntheticLambda86 implements Comparator {
    public final /* synthetic */ ProfileActivity.SearchAdapter f$0;

    public /* synthetic */ ProfileActivity$SearchAdapter$$ExternalSyntheticLambda86(ProfileActivity.SearchAdapter searchAdapter) {
        this.f$0 = searchAdapter;
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return this.f$0.lambda$new$83(obj, obj2);
    }
}
