package org.telegram.ui;

import java.util.ArrayList;
import org.telegram.ui.InviteContactsActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class InviteContactsActivity$InviteAdapter$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ InviteContactsActivity.InviteAdapter f$0;
    public final /* synthetic */ ArrayList f$1;
    public final /* synthetic */ ArrayList f$2;

    public /* synthetic */ InviteContactsActivity$InviteAdapter$$ExternalSyntheticLambda0(InviteContactsActivity.InviteAdapter inviteAdapter, ArrayList arrayList, ArrayList arrayList2) {
        this.f$0 = inviteAdapter;
        this.f$1 = arrayList;
        this.f$2 = arrayList2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$updateSearchResults$0(this.f$1, this.f$2);
    }
}
