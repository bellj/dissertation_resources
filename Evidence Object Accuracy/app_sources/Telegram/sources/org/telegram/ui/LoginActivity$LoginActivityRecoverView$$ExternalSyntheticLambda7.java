package org.telegram.ui;

import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda7 implements Runnable {
    public final /* synthetic */ LoginActivity.LoginActivityRecoverView f$0;

    public /* synthetic */ LoginActivity$LoginActivityRecoverView$$ExternalSyntheticLambda7(LoginActivity.LoginActivityRecoverView loginActivityRecoverView) {
        this.f$0 = loginActivityRecoverView;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onPasscodeError$6();
    }
}
