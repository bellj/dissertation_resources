package org.telegram.ui;

import org.telegram.ui.Components.NumberPicker;

/* loaded from: classes3.dex */
public final /* synthetic */ class PasscodeActivity$$ExternalSyntheticLambda21 implements NumberPicker.Formatter {
    public static final /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda21 INSTANCE = new PasscodeActivity$$ExternalSyntheticLambda21();

    private /* synthetic */ PasscodeActivity$$ExternalSyntheticLambda21() {
    }

    @Override // org.telegram.ui.Components.NumberPicker.Formatter
    public final String format(int i) {
        return PasscodeActivity.lambda$createView$3(i);
    }
}
