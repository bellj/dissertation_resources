package org.telegram.ui;

import android.view.View;
import org.telegram.ui.ManageLinksActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ManageLinksActivity$LinkCell$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ ManageLinksActivity.LinkCell f$0;

    public /* synthetic */ ManageLinksActivity$LinkCell$$ExternalSyntheticLambda3(ManageLinksActivity.LinkCell linkCell) {
        this.f$0 = linkCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$3(view);
    }
}
