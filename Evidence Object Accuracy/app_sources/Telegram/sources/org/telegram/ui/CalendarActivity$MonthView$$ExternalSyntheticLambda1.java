package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CalendarActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CalendarActivity$MonthView$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ CalendarActivity.MonthView f$0;

    public /* synthetic */ CalendarActivity$MonthView$$ExternalSyntheticLambda1(CalendarActivity.MonthView monthView) {
        this.f$0 = monthView;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return CalendarActivity.MonthView.m655$r8$lambda$SinsKIpLjv7rvEXKdKyy2N9aKo(this.f$0, view);
    }
}
