package org.telegram.ui;

import android.view.View;
import org.telegram.ui.CalendarActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class CalendarActivity$MonthView$2$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ CalendarActivity.MonthView.AnonymousClass2 f$0;
    public final /* synthetic */ CalendarActivity.PeriodDay f$1;

    public /* synthetic */ CalendarActivity$MonthView$2$$ExternalSyntheticLambda2(CalendarActivity.MonthView.AnonymousClass2 r1, CalendarActivity.PeriodDay periodDay) {
        this.f$0 = r1;
        this.f$1 = periodDay;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CalendarActivity.MonthView.AnonymousClass2.m658$r8$lambda$kWqnThtxKdHmhnH5hD3Fv0GX3Y(this.f$0, this.f$1, view);
    }
}
