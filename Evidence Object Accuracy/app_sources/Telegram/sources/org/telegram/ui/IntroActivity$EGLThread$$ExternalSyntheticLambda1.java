package org.telegram.ui;

import org.telegram.messenger.GenericProvider;
import org.telegram.ui.IntroActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class IntroActivity$EGLThread$$ExternalSyntheticLambda1 implements GenericProvider {
    public static final /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda1 INSTANCE = new IntroActivity$EGLThread$$ExternalSyntheticLambda1();

    private /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda1() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return IntroActivity.EGLThread.$r8$lambda$QDeu0WtK547Luy1HPTcEfAHtANs((Void) obj);
    }
}
