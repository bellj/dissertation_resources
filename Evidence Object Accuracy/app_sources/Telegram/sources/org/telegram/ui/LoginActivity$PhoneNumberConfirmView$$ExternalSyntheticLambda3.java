package org.telegram.ui;

import android.view.View;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$PhoneNumberConfirmView$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ LoginActivity.PhoneNumberConfirmView f$0;

    public /* synthetic */ LoginActivity$PhoneNumberConfirmView$$ExternalSyntheticLambda3(LoginActivity.PhoneNumberConfirmView phoneNumberConfirmView) {
        this.f$0 = phoneNumberConfirmView;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(view);
    }
}
