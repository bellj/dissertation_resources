package org.telegram.ui.Adapters;

import android.util.LongSparseArray;
import java.util.ArrayList;
import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC$TL_messages_getStickers;
import org.telegram.ui.Adapters.StickersSearchAdapter;

/* loaded from: classes3.dex */
public final /* synthetic */ class StickersSearchAdapter$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ StickersSearchAdapter.AnonymousClass1 f$0;
    public final /* synthetic */ TLRPC$TL_messages_getStickers f$1;
    public final /* synthetic */ TLObject f$2;
    public final /* synthetic */ ArrayList f$3;
    public final /* synthetic */ LongSparseArray f$4;

    public /* synthetic */ StickersSearchAdapter$1$$ExternalSyntheticLambda0(StickersSearchAdapter.AnonymousClass1 r1, TLRPC$TL_messages_getStickers tLRPC$TL_messages_getStickers, TLObject tLObject, ArrayList arrayList, LongSparseArray longSparseArray) {
        this.f$0 = r1;
        this.f$1 = tLRPC$TL_messages_getStickers;
        this.f$2 = tLObject;
        this.f$3 = arrayList;
        this.f$4 = longSparseArray;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StickersSearchAdapter.AnonymousClass1.$r8$lambda$xS9G8qJJuC49G1xR_vLjruTYB7E(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
