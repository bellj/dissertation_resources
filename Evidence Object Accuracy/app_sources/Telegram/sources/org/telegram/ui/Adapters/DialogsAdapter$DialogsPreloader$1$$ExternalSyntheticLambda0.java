package org.telegram.ui.Adapters;

import org.telegram.ui.Adapters.DialogsAdapter;

/* loaded from: classes3.dex */
public final /* synthetic */ class DialogsAdapter$DialogsPreloader$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ DialogsAdapter.DialogsPreloader.AnonymousClass1 f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ DialogsAdapter$DialogsPreloader$1$$ExternalSyntheticLambda0(DialogsAdapter.DialogsPreloader.AnonymousClass1 r1, long j) {
        this.f$0 = r1;
        this.f$1 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        DialogsAdapter.DialogsPreloader.AnonymousClass1.m610$r8$lambda$3FZmIc7iTrW5y3bKNV1GMqz07U(this.f$0, this.f$1);
    }
}
