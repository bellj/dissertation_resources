package org.telegram.ui.Adapters;

import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.messenger.MediaDataController;
import org.telegram.ui.Adapters.StickersSearchAdapter;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes3.dex */
public final /* synthetic */ class StickersSearchAdapter$1$$ExternalSyntheticLambda2 implements MediaDataController.KeywordResultCallback {
    public final /* synthetic */ StickersSearchAdapter.AnonymousClass1 f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ HashMap f$2;

    public /* synthetic */ StickersSearchAdapter$1$$ExternalSyntheticLambda2(StickersSearchAdapter.AnonymousClass1 r1, int i, HashMap hashMap) {
        this.f$0 = r1;
        this.f$1 = i;
        this.f$2 = hashMap;
    }

    @Override // org.telegram.messenger.MediaDataController.KeywordResultCallback
    public final void run(ArrayList arrayList, String str) {
        StickersSearchAdapter.AnonymousClass1.$r8$lambda$4h3Cj8g9h07lNQujGLY9v7blH7I(this.f$0, this.f$1, this.f$2, arrayList, str);
    }
}
