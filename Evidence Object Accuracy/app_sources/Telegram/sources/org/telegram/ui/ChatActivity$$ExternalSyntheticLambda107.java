package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivity$$ExternalSyntheticLambda107 implements View.OnTouchListener {
    public static final /* synthetic */ ChatActivity$$ExternalSyntheticLambda107 INSTANCE = new ChatActivity$$ExternalSyntheticLambda107();

    private /* synthetic */ ChatActivity$$ExternalSyntheticLambda107() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ChatActivity.lambda$createView$23(view, motionEvent);
    }
}
