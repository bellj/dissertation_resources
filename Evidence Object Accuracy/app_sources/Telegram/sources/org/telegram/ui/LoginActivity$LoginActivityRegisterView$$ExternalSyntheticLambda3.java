package org.telegram.ui;

import android.content.DialogInterface;
import org.telegram.ui.LoginActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class LoginActivity$LoginActivityRegisterView$$ExternalSyntheticLambda3 implements DialogInterface.OnClickListener {
    public final /* synthetic */ LoginActivity.LoginActivityRegisterView f$0;

    public /* synthetic */ LoginActivity$LoginActivityRegisterView$$ExternalSyntheticLambda3(LoginActivity.LoginActivityRegisterView loginActivityRegisterView) {
        this.f$0 = loginActivityRegisterView;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$showTermsOfService$3(dialogInterface, i);
    }
}
