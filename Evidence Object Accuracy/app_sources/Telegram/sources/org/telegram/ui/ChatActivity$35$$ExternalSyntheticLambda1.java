package org.telegram.ui;

import android.animation.ValueAnimator;
import org.telegram.ui.ChatActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class ChatActivity$35$$ExternalSyntheticLambda1 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ChatActivity.AnonymousClass35 f$0;

    public /* synthetic */ ChatActivity$35$$ExternalSyntheticLambda1(ChatActivity.AnonymousClass35 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$checkAnimation$0(valueAnimator);
    }
}
