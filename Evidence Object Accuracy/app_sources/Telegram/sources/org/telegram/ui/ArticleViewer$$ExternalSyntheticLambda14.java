package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ArticleViewer$$ExternalSyntheticLambda14 implements View.OnTouchListener {
    public static final /* synthetic */ ArticleViewer$$ExternalSyntheticLambda14 INSTANCE = new ArticleViewer$$ExternalSyntheticLambda14();

    private /* synthetic */ ArticleViewer$$ExternalSyntheticLambda14() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ArticleViewer.$r8$lambda$Dfh_V4ZA8sJra68LplVPjWXYupM(view, motionEvent);
    }
}
