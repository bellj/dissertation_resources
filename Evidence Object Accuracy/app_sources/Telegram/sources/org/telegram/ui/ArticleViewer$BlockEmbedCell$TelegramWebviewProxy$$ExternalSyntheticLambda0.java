package org.telegram.ui;

import org.telegram.ui.ArticleViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class ArticleViewer$BlockEmbedCell$TelegramWebviewProxy$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ArticleViewer.BlockEmbedCell.TelegramWebviewProxy f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ String f$2;

    public /* synthetic */ ArticleViewer$BlockEmbedCell$TelegramWebviewProxy$$ExternalSyntheticLambda0(ArticleViewer.BlockEmbedCell.TelegramWebviewProxy telegramWebviewProxy, String str, String str2) {
        this.f$0 = telegramWebviewProxy;
        this.f$1 = str;
        this.f$2 = str2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ArticleViewer.BlockEmbedCell.TelegramWebviewProxy.$r8$lambda$IHjeoOiOuEa6rDeLfMlKacw1zGc(this.f$0, this.f$1, this.f$2);
    }
}
