package org.telegram.ui;

import org.telegram.messenger.GenericProvider;
import org.telegram.ui.IntroActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class IntroActivity$EGLThread$$ExternalSyntheticLambda2 implements GenericProvider {
    public static final /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda2 INSTANCE = new IntroActivity$EGLThread$$ExternalSyntheticLambda2();

    private /* synthetic */ IntroActivity$EGLThread$$ExternalSyntheticLambda2() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return IntroActivity.EGLThread.$r8$lambda$eKf2_0se0Hd5XceA3YvVrFAtlzA((Void) obj);
    }
}
