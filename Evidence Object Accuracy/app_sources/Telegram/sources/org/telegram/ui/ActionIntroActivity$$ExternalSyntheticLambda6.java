package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class ActionIntroActivity$$ExternalSyntheticLambda6 implements View.OnTouchListener {
    public static final /* synthetic */ ActionIntroActivity$$ExternalSyntheticLambda6 INSTANCE = new ActionIntroActivity$$ExternalSyntheticLambda6();

    private /* synthetic */ ActionIntroActivity$$ExternalSyntheticLambda6() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return ActionIntroActivity.$r8$lambda$_rX8FzoEMbTJSa3AZ8TBqco66KY(view, motionEvent);
    }
}
