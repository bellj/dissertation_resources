package org.telegram.ui;

import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes3.dex */
public final /* synthetic */ class IdenticonActivity$$ExternalSyntheticLambda0 implements View.OnTouchListener {
    public static final /* synthetic */ IdenticonActivity$$ExternalSyntheticLambda0 INSTANCE = new IdenticonActivity$$ExternalSyntheticLambda0();

    private /* synthetic */ IdenticonActivity$$ExternalSyntheticLambda0() {
    }

    @Override // android.view.View.OnTouchListener
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return IdenticonActivity.$r8$lambda$zplM1XIK58D7qXS3s_vxkpkm9Dw(view, motionEvent);
    }
}
