package org.telegram.ui;

import org.telegram.tgnet.TLRPC$StickerSetCovered;
import org.telegram.ui.ArchivedStickersActivity;
import org.telegram.ui.Cells.ArchivedStickerSetCell;

/* loaded from: classes3.dex */
public final /* synthetic */ class ArchivedStickersActivity$ListAdapter$$ExternalSyntheticLambda0 implements ArchivedStickerSetCell.OnCheckedChangeListener {
    public final /* synthetic */ ArchivedStickersActivity.ListAdapter f$0;
    public final /* synthetic */ TLRPC$StickerSetCovered f$1;

    public /* synthetic */ ArchivedStickersActivity$ListAdapter$$ExternalSyntheticLambda0(ArchivedStickersActivity.ListAdapter listAdapter, TLRPC$StickerSetCovered tLRPC$StickerSetCovered) {
        this.f$0 = listAdapter;
        this.f$1 = tLRPC$StickerSetCovered;
    }

    @Override // org.telegram.ui.Cells.ArchivedStickerSetCell.OnCheckedChangeListener
    public final void onCheckedChanged(ArchivedStickerSetCell archivedStickerSetCell, boolean z) {
        ArchivedStickersActivity.ListAdapter.$r8$lambda$BVL_C_QrlbEOA6BTGsWWRBfkEfk(this.f$0, this.f$1, archivedStickerSetCell, z);
    }
}
