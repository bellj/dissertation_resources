package org.telegram.ui;

import android.view.View;
import org.telegram.ui.StatisticActivity;

/* loaded from: classes3.dex */
public final /* synthetic */ class StatisticActivity$BaseChartCell$$ExternalSyntheticLambda4 implements View.OnClickListener {
    public final /* synthetic */ StatisticActivity.BaseChartCell f$0;

    public /* synthetic */ StatisticActivity$BaseChartCell$$ExternalSyntheticLambda4(StatisticActivity.BaseChartCell baseChartCell) {
        this.f$0 = baseChartCell;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$3(view);
    }
}
