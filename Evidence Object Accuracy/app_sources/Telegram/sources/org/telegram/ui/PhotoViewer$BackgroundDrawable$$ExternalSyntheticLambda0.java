package org.telegram.ui;

import org.telegram.ui.PhotoViewer;

/* loaded from: classes3.dex */
public final /* synthetic */ class PhotoViewer$BackgroundDrawable$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ PhotoViewer.BackgroundDrawable f$0;

    public /* synthetic */ PhotoViewer$BackgroundDrawable$$ExternalSyntheticLambda0(PhotoViewer.BackgroundDrawable backgroundDrawable) {
        this.f$0 = backgroundDrawable;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$setAlpha$0();
    }
}
