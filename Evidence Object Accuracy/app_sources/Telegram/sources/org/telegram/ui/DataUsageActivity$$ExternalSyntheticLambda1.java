package org.telegram.ui;

import android.view.animation.Interpolator;

/* loaded from: classes3.dex */
public final /* synthetic */ class DataUsageActivity$$ExternalSyntheticLambda1 implements Interpolator {
    public static final /* synthetic */ DataUsageActivity$$ExternalSyntheticLambda1 INSTANCE = new DataUsageActivity$$ExternalSyntheticLambda1();

    private /* synthetic */ DataUsageActivity$$ExternalSyntheticLambda1() {
    }

    @Override // android.animation.TimeInterpolator
    public final float getInterpolation(float f) {
        return DataUsageActivity.lambda$static$0(f);
    }
}
