package org.telegram.messenger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.telegram.tgnet.TLRPC$Chat;
import org.telegram.tgnet.TLRPC$User;

/* loaded from: classes.dex */
public class AutoMessageHeardReceiver extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        ApplicationLoader.postInitApplication();
        long longExtra = intent.getLongExtra("dialog_id", 0);
        int intExtra = intent.getIntExtra("max_id", 0);
        int intExtra2 = intent.getIntExtra("currentAccount", 0);
        if (longExtra != 0 && intExtra != 0 && UserConfig.isValidAccount(intExtra2)) {
            AccountInstance instance = AccountInstance.getInstance(intExtra2);
            if (DialogObject.isUserDialog(longExtra)) {
                if (instance.getMessagesController().getUser(Long.valueOf(longExtra)) == null) {
                    Utilities.globalQueue.postRunnable(new Runnable(longExtra, intExtra2, intExtra) { // from class: org.telegram.messenger.AutoMessageHeardReceiver$$ExternalSyntheticLambda1
                        public final /* synthetic */ long f$1;
                        public final /* synthetic */ int f$2;
                        public final /* synthetic */ int f$3;

                        {
                            this.f$1 = r2;
                            this.f$2 = r4;
                            this.f$3 = r5;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            AutoMessageHeardReceiver.$r8$lambda$ZqPi5_gsPuFWg2HbrcpAVRwcQmc(AccountInstance.this, this.f$1, this.f$2, this.f$3);
                        }
                    });
                    return;
                }
            } else if (DialogObject.isChatDialog(longExtra) && instance.getMessagesController().getChat(Long.valueOf(-longExtra)) == null) {
                Utilities.globalQueue.postRunnable(new Runnable(longExtra, intExtra2, intExtra) { // from class: org.telegram.messenger.AutoMessageHeardReceiver$$ExternalSyntheticLambda0
                    public final /* synthetic */ long f$1;
                    public final /* synthetic */ int f$2;
                    public final /* synthetic */ int f$3;

                    {
                        this.f$1 = r2;
                        this.f$2 = r4;
                        this.f$3 = r5;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AutoMessageHeardReceiver.$r8$lambda$5mcOaJzRjv0uBDb84diOnrSzdew(AccountInstance.this, this.f$1, this.f$2, this.f$3);
                    }
                });
                return;
            }
            MessagesController.getInstance(intExtra2).markDialogAsRead(longExtra, intExtra, intExtra, 0, false, 0, 0, true, 0);
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$onReceive$1(AccountInstance accountInstance, long j, int i, int i2) {
        AndroidUtilities.runOnUIThread(new Runnable(accountInstance.getMessagesStorage().getUserSync(j), i, j, i2) { // from class: org.telegram.messenger.AutoMessageHeardReceiver$$ExternalSyntheticLambda3
            public final /* synthetic */ TLRPC$User f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AutoMessageHeardReceiver.$r8$lambda$yqVE2vR3_qgW8Few0u9cHKfvIm8(AccountInstance.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$onReceive$0(AccountInstance accountInstance, TLRPC$User tLRPC$User, int i, long j, int i2) {
        accountInstance.getMessagesController().putUser(tLRPC$User, true);
        MessagesController.getInstance(i).markDialogAsRead(j, i2, i2, 0, false, 0, 0, true, 0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$onReceive$3(AccountInstance accountInstance, long j, int i, int i2) {
        AndroidUtilities.runOnUIThread(new Runnable(accountInstance.getMessagesStorage().getChatSync(-j), i, j, i2) { // from class: org.telegram.messenger.AutoMessageHeardReceiver$$ExternalSyntheticLambda2
            public final /* synthetic */ TLRPC$Chat f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AutoMessageHeardReceiver.$r8$lambda$2TAUSwQZ4sOAMAsBsM0_s6T5d_k(AccountInstance.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$onReceive$2(AccountInstance accountInstance, TLRPC$Chat tLRPC$Chat, int i, long j, int i2) {
        accountInstance.getMessagesController().putChat(tLRPC$Chat, true);
        MessagesController.getInstance(i).markDialogAsRead(j, i2, i2, 0, false, 0, 0, true, 0);
    }
}
