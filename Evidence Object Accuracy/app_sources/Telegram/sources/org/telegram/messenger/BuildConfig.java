package org.telegram.messenger;

/* loaded from: classes.dex */
public final class BuildConfig {
    public static final String APPLICATION_ID = "org.telegram.messenger.web";
    public static final String BUILD_TYPE = "standalone";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "afat";
    public static final int VERSION_CODE = 27219;
    public static final String VERSION_NAME = "8.8.5";
}
