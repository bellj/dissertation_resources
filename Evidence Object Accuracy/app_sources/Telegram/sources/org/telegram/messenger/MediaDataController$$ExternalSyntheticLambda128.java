package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.tgnet.TLRPC$TL_topPeer;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda128 implements Comparator {
    public static final /* synthetic */ MediaDataController$$ExternalSyntheticLambda128 INSTANCE = new MediaDataController$$ExternalSyntheticLambda128();

    private /* synthetic */ MediaDataController$$ExternalSyntheticLambda128() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return MediaDataController.lambda$increaseInlineRaiting$112((TLRPC$TL_topPeer) obj, (TLRPC$TL_topPeer) obj2);
    }
}
