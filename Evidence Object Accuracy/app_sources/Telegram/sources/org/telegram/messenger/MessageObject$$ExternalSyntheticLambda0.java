package org.telegram.messenger;

import java.util.Comparator;

/* loaded from: classes.dex */
public final /* synthetic */ class MessageObject$$ExternalSyntheticLambda0 implements Comparator {
    public static final /* synthetic */ MessageObject$$ExternalSyntheticLambda0 INSTANCE = new MessageObject$$ExternalSyntheticLambda0();

    private /* synthetic */ MessageObject$$ExternalSyntheticLambda0() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return MessageObject.lambda$handleFoundWords$1((String) obj, (String) obj2);
    }
}
