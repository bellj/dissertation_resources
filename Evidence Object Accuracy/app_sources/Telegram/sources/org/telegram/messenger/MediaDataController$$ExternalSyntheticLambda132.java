package org.telegram.messenger;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda132 implements GenericProvider {
    public static final /* synthetic */ MediaDataController$$ExternalSyntheticLambda132 INSTANCE = new MediaDataController$$ExternalSyntheticLambda132();

    private /* synthetic */ MediaDataController$$ExternalSyntheticLambda132() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return MediaDataController.lambda$getEntities$141((Void) obj);
    }
}
