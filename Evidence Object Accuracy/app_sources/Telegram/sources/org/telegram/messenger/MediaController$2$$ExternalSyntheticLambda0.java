package org.telegram.messenger;

import org.telegram.messenger.MediaController;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaController$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ MediaController.AnonymousClass2 f$0;
    public final /* synthetic */ double f$1;

    public /* synthetic */ MediaController$2$$ExternalSyntheticLambda0(MediaController.AnonymousClass2 r1, double d) {
        this.f$0 = r1;
        this.f$1 = d;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$2(this.f$1);
    }
}
