package org.telegram.messenger;

import android.os.SystemClock;
import android.util.SparseIntArray;
import java.util.LinkedList;

/* loaded from: classes.dex */
public class DispatchQueuePoolMainThreadSync {
    private LinkedList<DispatchQueueMainThreadSync> busyQueues = new LinkedList<>();
    private SparseIntArray busyQueuesMap = new SparseIntArray();
    private Runnable cleanupRunnable = new Runnable() { // from class: org.telegram.messenger.DispatchQueuePoolMainThreadSync.1
        @Override // java.lang.Runnable
        public void run() {
            if (!DispatchQueuePoolMainThreadSync.this.queues.isEmpty()) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                int size = DispatchQueuePoolMainThreadSync.this.queues.size();
                int i = 0;
                while (i < size) {
                    DispatchQueueMainThreadSync dispatchQueueMainThreadSync = (DispatchQueueMainThreadSync) DispatchQueuePoolMainThreadSync.this.queues.get(i);
                    if (dispatchQueueMainThreadSync.getLastTaskTime() < elapsedRealtime - 30000) {
                        dispatchQueueMainThreadSync.recycle();
                        DispatchQueuePoolMainThreadSync.this.queues.remove(i);
                        DispatchQueuePoolMainThreadSync.access$110(DispatchQueuePoolMainThreadSync.this);
                        i--;
                        size--;
                    }
                    i++;
                }
            }
            if (!DispatchQueuePoolMainThreadSync.this.queues.isEmpty() || !DispatchQueuePoolMainThreadSync.this.busyQueues.isEmpty()) {
                AndroidUtilities.runOnUIThread(this, 30000);
                DispatchQueuePoolMainThreadSync.this.cleanupScheduled = true;
                return;
            }
            DispatchQueuePoolMainThreadSync.this.cleanupScheduled = false;
        }
    };
    private boolean cleanupScheduled;
    private int createdCount;
    private int guid;
    private int maxCount;
    private LinkedList<DispatchQueueMainThreadSync> queues = new LinkedList<>();
    private int totalTasksCount;

    static /* synthetic */ int access$110(DispatchQueuePoolMainThreadSync dispatchQueuePoolMainThreadSync) {
        int i = dispatchQueuePoolMainThreadSync.createdCount;
        dispatchQueuePoolMainThreadSync.createdCount = i - 1;
        return i;
    }

    public DispatchQueuePoolMainThreadSync(int i) {
        this.maxCount = i;
        this.guid = Utilities.random.nextInt();
    }

    public void execute(Runnable runnable) {
        DispatchQueueMainThreadSync dispatchQueueMainThreadSync;
        if (!this.busyQueues.isEmpty() && (this.totalTasksCount / 2 <= this.busyQueues.size() || (this.queues.isEmpty() && this.createdCount >= this.maxCount))) {
            dispatchQueueMainThreadSync = this.busyQueues.remove(0);
        } else if (this.queues.isEmpty()) {
            dispatchQueueMainThreadSync = new DispatchQueueMainThreadSync("DispatchQueuePool" + this.guid + "_" + Utilities.random.nextInt());
            dispatchQueueMainThreadSync.setPriority(10);
            this.createdCount = this.createdCount + 1;
        } else {
            dispatchQueueMainThreadSync = this.queues.remove(0);
        }
        if (!this.cleanupScheduled) {
            AndroidUtilities.runOnUIThread(this.cleanupRunnable, 30000);
            this.cleanupScheduled = true;
        }
        this.totalTasksCount++;
        this.busyQueues.add(dispatchQueueMainThreadSync);
        this.busyQueuesMap.put(dispatchQueueMainThreadSync.index, this.busyQueuesMap.get(dispatchQueueMainThreadSync.index, 0) + 1);
        dispatchQueueMainThreadSync.postRunnable(new Runnable(runnable, dispatchQueueMainThreadSync) { // from class: org.telegram.messenger.DispatchQueuePoolMainThreadSync$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$1;
            public final /* synthetic */ DispatchQueueMainThreadSync f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DispatchQueuePoolMainThreadSync.$r8$lambda$Sr4_KaXezFVY5mEVylNnHtItEb8(DispatchQueuePoolMainThreadSync.this, this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$execute$1(Runnable runnable, DispatchQueueMainThreadSync dispatchQueueMainThreadSync) {
        runnable.run();
        AndroidUtilities.runOnUIThread(new Runnable(dispatchQueueMainThreadSync) { // from class: org.telegram.messenger.DispatchQueuePoolMainThreadSync$$ExternalSyntheticLambda1
            public final /* synthetic */ DispatchQueueMainThreadSync f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DispatchQueuePoolMainThreadSync.m108$r8$lambda$Mq_Vu0idWXzaF3yun9QVuICih0(DispatchQueuePoolMainThreadSync.this, this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$execute$0(DispatchQueueMainThreadSync dispatchQueueMainThreadSync) {
        this.totalTasksCount--;
        int i = this.busyQueuesMap.get(dispatchQueueMainThreadSync.index) - 1;
        if (i == 0) {
            this.busyQueuesMap.delete(dispatchQueueMainThreadSync.index);
            this.busyQueues.remove(dispatchQueueMainThreadSync);
            this.queues.add(dispatchQueueMainThreadSync);
            return;
        }
        this.busyQueuesMap.put(dispatchQueueMainThreadSync.index, i);
    }
}
