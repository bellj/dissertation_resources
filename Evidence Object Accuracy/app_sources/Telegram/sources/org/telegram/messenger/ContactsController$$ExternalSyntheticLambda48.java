package org.telegram.messenger;

import java.util.Comparator;

/* loaded from: classes.dex */
public final /* synthetic */ class ContactsController$$ExternalSyntheticLambda48 implements Comparator {
    public static final /* synthetic */ ContactsController$$ExternalSyntheticLambda48 INSTANCE = new ContactsController$$ExternalSyntheticLambda48();

    private /* synthetic */ ContactsController$$ExternalSyntheticLambda48() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return ContactsController.lambda$buildContactsSectionsArrays$44((String) obj, (String) obj2);
    }
}
