package org.telegram.messenger;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda130 implements GenericProvider {
    public static final /* synthetic */ MediaDataController$$ExternalSyntheticLambda130 INSTANCE = new MediaDataController$$ExternalSyntheticLambda130();

    private /* synthetic */ MediaDataController$$ExternalSyntheticLambda130() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return MediaDataController.lambda$getEntities$142((Void) obj);
    }
}
