package org.telegram.messenger;

import org.telegram.messenger.MediaController;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaController$5$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ MediaController.AnonymousClass5 f$0;
    public final /* synthetic */ MessageObject f$1;

    public /* synthetic */ MediaController$5$$ExternalSyntheticLambda1(MediaController.AnonymousClass5 r1, MessageObject messageObject) {
        this.f$0 = r1;
        this.f$1 = messageObject;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$1(this.f$1);
    }
}
