package org.telegram.messenger;

import android.app.IntentService;
import android.content.Intent;

/* loaded from: classes.dex */
public class NotificationRepeat extends IntentService {
    public NotificationRepeat() {
        super("NotificationRepeat");
    }

    @Override // android.app.IntentService
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            int intExtra = intent.getIntExtra("currentAccount", UserConfig.selectedAccount);
            if (UserConfig.isValidAccount(intExtra)) {
                AndroidUtilities.runOnUIThread(new Runnable(intExtra) { // from class: org.telegram.messenger.NotificationRepeat$$ExternalSyntheticLambda0
                    public final /* synthetic */ int f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        NotificationRepeat.$r8$lambda$NPsTyGhnRdEjRseN8EQhOzyrt_0(this.f$0);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$onHandleIntent$0(int i) {
        NotificationsController.getInstance(i).repeatNotificationMaybe();
    }
}
