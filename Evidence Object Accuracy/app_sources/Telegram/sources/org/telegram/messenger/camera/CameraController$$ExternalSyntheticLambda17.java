package org.telegram.messenger.camera;

import java.util.Comparator;

/* loaded from: classes.dex */
public final /* synthetic */ class CameraController$$ExternalSyntheticLambda17 implements Comparator {
    public static final /* synthetic */ CameraController$$ExternalSyntheticLambda17 INSTANCE = new CameraController$$ExternalSyntheticLambda17();

    private /* synthetic */ CameraController$$ExternalSyntheticLambda17() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return CameraController.m525$r8$lambda$UQHiOp_k_ixnHDWCsEPYvxHGU8((Size) obj, (Size) obj2);
    }
}
