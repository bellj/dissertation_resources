package org.telegram.messenger;

/* loaded from: classes.dex */
public final /* synthetic */ class AndroidUtilities$$ExternalSyntheticLambda13 implements GenericProvider {
    public static final /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda13 INSTANCE = new AndroidUtilities$$ExternalSyntheticLambda13();

    private /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda13() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return AndroidUtilities.lambda$formatSpannableSimple$7((Integer) obj);
    }
}
