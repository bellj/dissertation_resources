package org.telegram.messenger;

import java.util.Comparator;
import org.telegram.tgnet.TLRPC$Message;

/* loaded from: classes.dex */
public final /* synthetic */ class SecretChatHelper$$ExternalSyntheticLambda25 implements Comparator {
    public static final /* synthetic */ SecretChatHelper$$ExternalSyntheticLambda25 INSTANCE = new SecretChatHelper$$ExternalSyntheticLambda25();

    private /* synthetic */ SecretChatHelper$$ExternalSyntheticLambda25() {
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return SecretChatHelper.lambda$resendMessages$13((TLRPC$Message) obj, (TLRPC$Message) obj2);
    }
}
