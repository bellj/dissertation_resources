package org.telegram.messenger;

import android.content.Intent;
import org.telegram.messenger.NotificationBadge;

/* loaded from: classes.dex */
public final /* synthetic */ class NotificationBadge$SonyHomeBadger$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Intent f$0;

    public /* synthetic */ NotificationBadge$SonyHomeBadger$$ExternalSyntheticLambda0(Intent intent) {
        this.f$0 = intent;
    }

    @Override // java.lang.Runnable
    public final void run() {
        NotificationBadge.SonyHomeBadger.$r8$lambda$v76Vp1ajGJiZXyZ1DMOn3Fy7iTg(this.f$0);
    }
}
