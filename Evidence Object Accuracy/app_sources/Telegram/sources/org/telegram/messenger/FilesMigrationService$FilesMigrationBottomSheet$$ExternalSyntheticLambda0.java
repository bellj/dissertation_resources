package org.telegram.messenger;

import android.view.View;
import org.telegram.messenger.FilesMigrationService;

/* loaded from: classes.dex */
public final /* synthetic */ class FilesMigrationService$FilesMigrationBottomSheet$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ FilesMigrationService.FilesMigrationBottomSheet f$0;

    public /* synthetic */ FilesMigrationService$FilesMigrationBottomSheet$$ExternalSyntheticLambda0(FilesMigrationService.FilesMigrationBottomSheet filesMigrationBottomSheet) {
        this.f$0 = filesMigrationBottomSheet;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        FilesMigrationService.FilesMigrationBottomSheet.$r8$lambda$vlejTVbJlvan7_tGi8V43Yhdjbk(this.f$0, view);
    }
}
