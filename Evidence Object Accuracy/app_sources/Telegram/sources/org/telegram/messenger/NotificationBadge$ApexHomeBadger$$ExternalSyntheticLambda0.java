package org.telegram.messenger;

import android.content.Intent;
import org.telegram.messenger.NotificationBadge;

/* loaded from: classes.dex */
public final /* synthetic */ class NotificationBadge$ApexHomeBadger$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Intent f$0;

    public /* synthetic */ NotificationBadge$ApexHomeBadger$$ExternalSyntheticLambda0(Intent intent) {
        this.f$0 = intent;
    }

    @Override // java.lang.Runnable
    public final void run() {
        NotificationBadge.ApexHomeBadger.$r8$lambda$KoidL0YGA46UDNNFEr9zy_rM67M(this.f$0);
    }
}
