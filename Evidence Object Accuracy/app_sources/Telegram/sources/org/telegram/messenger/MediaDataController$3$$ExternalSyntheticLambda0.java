package org.telegram.messenger;

import java.util.ArrayList;
import org.telegram.messenger.MediaDataController;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaDataController$3$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ MediaDataController.AnonymousClass3 f$0;
    public final /* synthetic */ ArrayList f$1;

    public /* synthetic */ MediaDataController$3$$ExternalSyntheticLambda0(MediaDataController.AnonymousClass3 r1, ArrayList arrayList) {
        this.f$0 = r1;
        this.f$1 = arrayList;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
