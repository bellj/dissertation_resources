package org.telegram.messenger;

import org.telegram.messenger.SendMessagesHelper;

/* loaded from: classes.dex */
public final /* synthetic */ class SendMessagesHelper$ImportingHistory$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SendMessagesHelper.ImportingHistory.AnonymousClass2 f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ SendMessagesHelper$ImportingHistory$2$$ExternalSyntheticLambda0(SendMessagesHelper.ImportingHistory.AnonymousClass2 r1, String str) {
        this.f$0 = r1;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
