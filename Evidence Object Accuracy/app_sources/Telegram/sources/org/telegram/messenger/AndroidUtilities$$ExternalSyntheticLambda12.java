package org.telegram.messenger;

/* loaded from: classes.dex */
public final /* synthetic */ class AndroidUtilities$$ExternalSyntheticLambda12 implements GenericProvider {
    public static final /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda12 INSTANCE = new AndroidUtilities$$ExternalSyntheticLambda12();

    private /* synthetic */ AndroidUtilities$$ExternalSyntheticLambda12() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return AndroidUtilities.lambda$formatSpannable$8((Integer) obj);
    }
}
