package org.telegram.messenger;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda129 implements GenericProvider {
    public static final /* synthetic */ MediaDataController$$ExternalSyntheticLambda129 INSTANCE = new MediaDataController$$ExternalSyntheticLambda129();

    private /* synthetic */ MediaDataController$$ExternalSyntheticLambda129() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return MediaDataController.lambda$getEntities$140((Void) obj);
    }
}
