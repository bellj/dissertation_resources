package org.telegram.messenger;

import org.telegram.messenger.SendMessagesHelper;

/* loaded from: classes.dex */
public final /* synthetic */ class SendMessagesHelper$LocationProvider$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SendMessagesHelper.LocationProvider f$0;

    public /* synthetic */ SendMessagesHelper$LocationProvider$$ExternalSyntheticLambda0(SendMessagesHelper.LocationProvider locationProvider) {
        this.f$0 = locationProvider;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$start$0();
    }
}
