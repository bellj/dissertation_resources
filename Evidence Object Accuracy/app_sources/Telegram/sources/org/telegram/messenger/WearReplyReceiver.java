package org.telegram.messenger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.core.app.RemoteInput;
import org.telegram.tgnet.TLRPC$Chat;
import org.telegram.tgnet.TLRPC$User;

/* loaded from: classes.dex */
public class WearReplyReceiver extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        ApplicationLoader.postInitApplication();
        Bundle resultsFromIntent = RemoteInput.getResultsFromIntent(intent);
        if (resultsFromIntent != null) {
            CharSequence charSequence = resultsFromIntent.getCharSequence(NotificationsController.EXTRA_VOICE_REPLY);
            if (!TextUtils.isEmpty(charSequence)) {
                long longExtra = intent.getLongExtra("dialog_id", 0);
                int intExtra = intent.getIntExtra("max_id", 0);
                int intExtra2 = intent.getIntExtra("currentAccount", 0);
                if (longExtra != 0 && intExtra != 0 && UserConfig.isValidAccount(intExtra2)) {
                    AccountInstance instance = AccountInstance.getInstance(intExtra2);
                    if (DialogObject.isUserDialog(longExtra)) {
                        if (instance.getMessagesController().getUser(Long.valueOf(longExtra)) == null) {
                            Utilities.globalQueue.postRunnable(new Runnable(instance, longExtra, charSequence, intExtra) { // from class: org.telegram.messenger.WearReplyReceiver$$ExternalSyntheticLambda0
                                public final /* synthetic */ AccountInstance f$1;
                                public final /* synthetic */ long f$2;
                                public final /* synthetic */ CharSequence f$3;
                                public final /* synthetic */ int f$4;

                                {
                                    this.f$1 = r2;
                                    this.f$2 = r3;
                                    this.f$3 = r5;
                                    this.f$4 = r6;
                                }

                                @Override // java.lang.Runnable
                                public final void run() {
                                    WearReplyReceiver.this.lambda$onReceive$1(this.f$1, this.f$2, this.f$3, this.f$4);
                                }
                            });
                            return;
                        }
                    } else if (DialogObject.isChatDialog(longExtra) && instance.getMessagesController().getChat(Long.valueOf(-longExtra)) == null) {
                        Utilities.globalQueue.postRunnable(new Runnable(instance, longExtra, charSequence, intExtra) { // from class: org.telegram.messenger.WearReplyReceiver$$ExternalSyntheticLambda1
                            public final /* synthetic */ AccountInstance f$1;
                            public final /* synthetic */ long f$2;
                            public final /* synthetic */ CharSequence f$3;
                            public final /* synthetic */ int f$4;

                            {
                                this.f$1 = r2;
                                this.f$2 = r3;
                                this.f$3 = r5;
                                this.f$4 = r6;
                            }

                            @Override // java.lang.Runnable
                            public final void run() {
                                WearReplyReceiver.this.lambda$onReceive$3(this.f$1, this.f$2, this.f$3, this.f$4);
                            }
                        });
                        return;
                    }
                    sendMessage(instance, charSequence, longExtra, intExtra);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onReceive$1(AccountInstance accountInstance, long j, CharSequence charSequence, int i) {
        AndroidUtilities.runOnUIThread(new Runnable(accountInstance, accountInstance.getMessagesStorage().getUserSync(j), charSequence, j, i) { // from class: org.telegram.messenger.WearReplyReceiver$$ExternalSyntheticLambda3
            public final /* synthetic */ AccountInstance f$1;
            public final /* synthetic */ TLRPC$User f$2;
            public final /* synthetic */ CharSequence f$3;
            public final /* synthetic */ long f$4;
            public final /* synthetic */ int f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r7;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WearReplyReceiver.this.lambda$onReceive$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onReceive$0(AccountInstance accountInstance, TLRPC$User tLRPC$User, CharSequence charSequence, long j, int i) {
        accountInstance.getMessagesController().putUser(tLRPC$User, true);
        sendMessage(accountInstance, charSequence, j, i);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onReceive$3(AccountInstance accountInstance, long j, CharSequence charSequence, int i) {
        AndroidUtilities.runOnUIThread(new Runnable(accountInstance, accountInstance.getMessagesStorage().getChatSync(-j), charSequence, j, i) { // from class: org.telegram.messenger.WearReplyReceiver$$ExternalSyntheticLambda2
            public final /* synthetic */ AccountInstance f$1;
            public final /* synthetic */ TLRPC$Chat f$2;
            public final /* synthetic */ CharSequence f$3;
            public final /* synthetic */ long f$4;
            public final /* synthetic */ int f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r7;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WearReplyReceiver.this.lambda$onReceive$2(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$onReceive$2(AccountInstance accountInstance, TLRPC$Chat tLRPC$Chat, CharSequence charSequence, long j, int i) {
        accountInstance.getMessagesController().putChat(tLRPC$Chat, true);
        sendMessage(accountInstance, charSequence, j, i);
    }

    private void sendMessage(AccountInstance accountInstance, CharSequence charSequence, long j, int i) {
        accountInstance.getSendMessagesHelper().sendMessage(charSequence.toString(), j, null, null, null, true, null, null, null, true, 0, null);
        accountInstance.getMessagesController().markDialogAsRead(j, i, i, 0, false, 0, 0, true, 0);
    }
}
