package org.telegram.messenger;

import org.telegram.messenger.MediaController;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaController$MediaLoader$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ MediaController.MediaLoader f$0;

    public /* synthetic */ MediaController$MediaLoader$$ExternalSyntheticLambda2(MediaController.MediaLoader mediaLoader) {
        this.f$0 = mediaLoader;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$checkIfFinished$4();
    }
}
