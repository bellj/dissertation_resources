package org.telegram.messenger;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaDataController$$ExternalSyntheticLambda131 implements GenericProvider {
    public static final /* synthetic */ MediaDataController$$ExternalSyntheticLambda131 INSTANCE = new MediaDataController$$ExternalSyntheticLambda131();

    private /* synthetic */ MediaDataController$$ExternalSyntheticLambda131() {
    }

    @Override // org.telegram.messenger.GenericProvider
    public final Object provide(Object obj) {
        return MediaDataController.lambda$getEntities$139((Void) obj);
    }
}
