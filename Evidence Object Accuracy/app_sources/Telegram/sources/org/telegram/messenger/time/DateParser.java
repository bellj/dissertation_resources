package org.telegram.messenger.time;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* loaded from: classes.dex */
public interface DateParser {
    @Override // org.telegram.messenger.time.DatePrinter
    Locale getLocale();

    @Override // org.telegram.messenger.time.DatePrinter
    String getPattern();

    @Override // org.telegram.messenger.time.DatePrinter
    TimeZone getTimeZone();

    Date parse(String str) throws ParseException;

    Date parse(String str, ParsePosition parsePosition);

    Object parseObject(String str) throws ParseException;

    @Override // org.telegram.messenger.time.DateParser
    Object parseObject(String str, ParsePosition parsePosition);
}
