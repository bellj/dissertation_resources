package org.telegram.messenger;

import android.content.Intent;
import org.telegram.messenger.NotificationBadge;

/* loaded from: classes.dex */
public final /* synthetic */ class NotificationBadge$AdwHomeBadger$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Intent f$0;

    public /* synthetic */ NotificationBadge$AdwHomeBadger$$ExternalSyntheticLambda0(Intent intent) {
        this.f$0 = intent;
    }

    @Override // java.lang.Runnable
    public final void run() {
        NotificationBadge.AdwHomeBadger.m454$r8$lambda$NSH99O0DPsZGlaeleZLjuMr5Qw(this.f$0);
    }
}
