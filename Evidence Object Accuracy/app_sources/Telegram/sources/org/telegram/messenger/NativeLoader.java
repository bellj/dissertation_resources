package org.telegram.messenger;

/* loaded from: classes.dex */
public class NativeLoader {
    private static final String LIB_NAME = "tmessages.42";
    private static final String LIB_SO_NAME = "libtmessages.42.so";
    private static final int LIB_VERSION = 42;
    private static final String LOCALE_LIB_SO_NAME = "libtmessages.42loc.so";
    private static volatile boolean nativeLoaded = false;
    private String crashPath = "";

    private static native void init(String str, boolean z);

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0036 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.io.File getNativeLibraryDir(android.content.Context r4) {
        /*
            r0 = 0
            if (r4 == 0) goto L_0x001f
            java.io.File r1 = new java.io.File     // Catch: all -> 0x001b
            java.lang.Class<android.content.pm.ApplicationInfo> r2 = android.content.pm.ApplicationInfo.class
            java.lang.String r3 = "nativeLibraryDir"
            java.lang.reflect.Field r2 = r2.getField(r3)     // Catch: all -> 0x001b
            android.content.pm.ApplicationInfo r3 = r4.getApplicationInfo()     // Catch: all -> 0x001b
            java.lang.Object r2 = r2.get(r3)     // Catch: all -> 0x001b
            java.lang.String r2 = (java.lang.String) r2     // Catch: all -> 0x001b
            r1.<init>(r2)     // Catch: all -> 0x001b
            goto L_0x0020
        L_0x001b:
            r1 = move-exception
            r1.printStackTrace()
        L_0x001f:
            r1 = r0
        L_0x0020:
            if (r1 != 0) goto L_0x002f
            java.io.File r1 = new java.io.File
            android.content.pm.ApplicationInfo r4 = r4.getApplicationInfo()
            java.lang.String r4 = r4.dataDir
            java.lang.String r2 = "lib"
            r1.<init>(r4, r2)
        L_0x002f:
            boolean r4 = r1.isDirectory()
            if (r4 == 0) goto L_0x0036
            return r1
        L_0x0036:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.NativeLoader.getNativeLibraryDir(android.content.Context):java.io.File");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:45:0x00c6 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:4:0x0009 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v1 */
    /* JADX WARN: Type inference failed for: r6v2, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.util.zip.ZipFile] */
    /* JADX WARN: Type inference failed for: r6v5, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r6v9, types: [java.lang.Throwable, java.lang.Exception] */
    /* JADX WARN: Type inference failed for: r6v10, types: [java.io.File[]] */
    /* JADX WARN: Type inference failed for: r2v7, types: [int] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.io.File] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @android.annotation.SuppressLint({"UnsafeDynamicallyLoadedCode", "SetWorldReadable"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean loadFromZip(android.content.Context r5, java.io.File r6, java.io.File r7, java.lang.String r8) {
        /*
        // Method dump skipped, instructions count: 222
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.NativeLoader.loadFromZip(android.content.Context, java.io.File, java.io.File, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d5 A[Catch: all -> 0x001c, TryCatch #6 {, blocks: (B:4:0x0003, B:60:0x00f4, B:62:0x00fd, B:9:0x000a, B:11:0x0015, B:15:0x0020, B:16:0x0023, B:19:0x0030, B:22:0x003b, B:25:0x0046, B:28:0x0051, B:31:0x005c, B:34:0x0067, B:36:0x006d, B:39:0x0084, B:40:0x0089, B:42:0x0091, B:45:0x009b, B:47:0x00b6, B:49:0x00ba, B:50:0x00bf, B:53:0x00cb, B:54:0x00d1, B:56:0x00d5, B:57:0x00e9), top: B:75:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00f0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f4 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @android.annotation.SuppressLint({"UnsafeDynamicallyLoadedCode"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void initNativeLibs(android.content.Context r7) {
        /*
        // Method dump skipped, instructions count: 261
        */
        throw new UnsupportedOperationException("Method not decompiled: org.telegram.messenger.NativeLoader.initNativeLibs(android.content.Context):void");
    }
}
