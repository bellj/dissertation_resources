package org.telegram.messenger;

import org.telegram.messenger.MediaController;

/* loaded from: classes.dex */
public final /* synthetic */ class MediaController$5$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ String f$0;
    public final /* synthetic */ float f$1;

    public /* synthetic */ MediaController$5$$ExternalSyntheticLambda0(String str, float f) {
        this.f$0 = str;
        this.f$1 = f;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MediaController.AnonymousClass5.lambda$run$0(this.f$0, this.f$1);
    }
}
