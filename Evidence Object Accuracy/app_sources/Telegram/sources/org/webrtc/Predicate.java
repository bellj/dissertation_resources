package org.webrtc;

/* loaded from: classes3.dex */
public interface Predicate<T> {
    Predicate<T> and(Predicate<? super T> predicate);

    Predicate<T> negate();

    Predicate<T> or(Predicate<? super T> predicate);

    boolean test(T t);

    /* renamed from: org.webrtc.Predicate$-CC  reason: invalid class name */
    /* loaded from: classes3.dex */
    public final /* synthetic */ class CC {
        /* JADX WARN: Incorrect args count in method signature: (Lorg/webrtc/Predicate<-TT;>;)Lorg/webrtc/Predicate<TT;>; */
        public static Predicate $default$or(Predicate _this, Predicate predicate) {
            return new Predicate<T>(predicate) { // from class: org.webrtc.Predicate.1
                final /* synthetic */ Predicate val$other;

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate and(Predicate predicate2) {
                    return CC.$default$and(this, predicate2);
                }

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate negate() {
                    return CC.$default$negate(this);
                }

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate or(Predicate predicate2) {
                    return CC.$default$or(this, predicate2);
                }

                {
                    this.val$other = r2;
                }

                @Override // org.webrtc.Predicate
                public boolean test(T t) {
                    return Predicate.this.test(t) || this.val$other.test(t);
                }
            };
        }

        /* JADX WARN: Incorrect args count in method signature: (Lorg/webrtc/Predicate<-TT;>;)Lorg/webrtc/Predicate<TT;>; */
        public static Predicate $default$and(Predicate _this, Predicate predicate) {
            return new Predicate<T>(predicate) { // from class: org.webrtc.Predicate.2
                final /* synthetic */ Predicate val$other;

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate and(Predicate predicate2) {
                    return CC.$default$and(this, predicate2);
                }

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate negate() {
                    return CC.$default$negate(this);
                }

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate or(Predicate predicate2) {
                    return CC.$default$or(this, predicate2);
                }

                {
                    this.val$other = r2;
                }

                @Override // org.webrtc.Predicate
                public boolean test(T t) {
                    return Predicate.this.test(t) && this.val$other.test(t);
                }
            };
        }

        /* JADX WARN: Incorrect args count in method signature: ()Lorg/webrtc/Predicate<TT;>; */
        public static Predicate $default$negate(Predicate _this) {
            return new Predicate<T>() { // from class: org.webrtc.Predicate.3
                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return CC.$default$and(this, predicate);
                }

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate negate() {
                    return CC.$default$negate(this);
                }

                @Override // org.webrtc.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return CC.$default$or(this, predicate);
                }

                @Override // org.webrtc.Predicate
                public boolean test(T t) {
                    return !Predicate.this.test(t);
                }
            };
        }
    }
}
