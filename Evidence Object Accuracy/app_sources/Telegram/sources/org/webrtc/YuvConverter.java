package org.webrtc;

import android.graphics.Matrix;
import android.opengl.GLES20;
import java.nio.ByteBuffer;
import org.telegram.messenger.FileLog;
import org.webrtc.GlGenericDrawer;
import org.webrtc.ThreadUtils;
import org.webrtc.VideoFrame;

/* loaded from: classes3.dex */
public class YuvConverter {
    private static final String FRAGMENT_SHADER = "uniform vec2 xUnit;\nuniform vec4 coeffs;\n\nvoid main() {\n  gl_FragColor.r = coeffs.a + dot(coeffs.rgb,\n      sample(tc - 1.5 * xUnit).rgb);\n  gl_FragColor.g = coeffs.a + dot(coeffs.rgb,\n      sample(tc - 0.5 * xUnit).rgb);\n  gl_FragColor.b = coeffs.a + dot(coeffs.rgb,\n      sample(tc + 0.5 * xUnit).rgb);\n  gl_FragColor.a = coeffs.a + dot(coeffs.rgb,\n      sample(tc + 1.5 * xUnit).rgb);\n}\n";
    private final GlGenericDrawer drawer;
    private final GlTextureFrameBuffer i420TextureFrameBuffer;
    private final ShaderCallbacks shaderCallbacks;
    private final ThreadUtils.ThreadChecker threadChecker;
    private final VideoFrameDrawer videoFrameDrawer;

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static class ShaderCallbacks implements GlGenericDrawer.ShaderCallbacks {
        private static final float[] uCoeffs = {-0.148223f, -0.290993f, 0.439216f, 0.501961f};
        private static final float[] vCoeffs = {0.439216f, -0.367788f, -0.0714274f, 0.501961f};
        private static final float[] yCoeffs = {0.256788f, 0.504129f, 0.0979059f, 0.0627451f};
        private float[] coeffs;
        private int coeffsLoc;
        private float stepSize;
        private int xUnitLoc;

        private ShaderCallbacks() {
        }

        public void setPlaneY() {
            this.coeffs = yCoeffs;
            this.stepSize = 1.0f;
        }

        public void setPlaneU() {
            this.coeffs = uCoeffs;
            this.stepSize = 2.0f;
        }

        public void setPlaneV() {
            this.coeffs = vCoeffs;
            this.stepSize = 2.0f;
        }

        @Override // org.webrtc.GlGenericDrawer.ShaderCallbacks
        public void onNewShader(GlShader glShader) {
            this.xUnitLoc = glShader.getUniformLocation("xUnit");
            this.coeffsLoc = glShader.getUniformLocation("coeffs");
        }

        @Override // org.webrtc.GlGenericDrawer.ShaderCallbacks
        public void onPrepareShader(GlShader glShader, float[] fArr, int i, int i2, int i3, int i4) {
            GLES20.glUniform4fv(this.coeffsLoc, 1, this.coeffs, 0);
            int i5 = this.xUnitLoc;
            float f = this.stepSize;
            float f2 = (float) i;
            GLES20.glUniform2f(i5, (fArr[0] * f) / f2, (f * fArr[1]) / f2);
        }
    }

    public YuvConverter() {
        this(new VideoFrameDrawer());
    }

    public YuvConverter(VideoFrameDrawer videoFrameDrawer) {
        ThreadUtils.ThreadChecker threadChecker = new ThreadUtils.ThreadChecker();
        this.threadChecker = threadChecker;
        this.i420TextureFrameBuffer = new GlTextureFrameBuffer(6408);
        ShaderCallbacks shaderCallbacks = new ShaderCallbacks();
        this.shaderCallbacks = shaderCallbacks;
        this.drawer = new GlGenericDrawer(FRAGMENT_SHADER, shaderCallbacks);
        this.videoFrameDrawer = videoFrameDrawer;
        threadChecker.detachThread();
    }

    public VideoFrame.I420Buffer convert(VideoFrame.TextureBuffer textureBuffer) {
        ByteBuffer byteBuffer;
        int i;
        Exception e;
        this.threadChecker.checkIsOnValidThread();
        VideoFrame.TextureBuffer textureBuffer2 = (VideoFrame.TextureBuffer) this.videoFrameDrawer.prepareBufferForViewportSize(textureBuffer, textureBuffer.getWidth(), textureBuffer.getHeight());
        int width = textureBuffer2.getWidth();
        int height = textureBuffer2.getHeight();
        int i2 = ((width + 7) / 8) * 8;
        int i3 = (height + 1) / 2;
        int i4 = height + i3;
        ByteBuffer nativeAllocateByteBuffer = JniCommon.nativeAllocateByteBuffer(i2 * i4);
        int i5 = i2 / 4;
        Matrix matrix = new Matrix();
        matrix.preTranslate(0.5f, 0.5f);
        matrix.preScale(1.0f, -1.0f);
        matrix.preTranslate(-0.5f, -0.5f);
        try {
            this.i420TextureFrameBuffer.setSize(i5, i4);
            GLES20.glBindFramebuffer(36160, this.i420TextureFrameBuffer.getFrameBufferId());
            GlUtil.checkNoGLES2Error("glBindFramebuffer");
            this.shaderCallbacks.setPlaneY();
            byteBuffer = nativeAllocateByteBuffer;
        } catch (Exception e2) {
            e = e2;
            byteBuffer = nativeAllocateByteBuffer;
        }
        try {
            VideoFrameDrawer.drawTexture(this.drawer, textureBuffer2, matrix, width, height, width, height, 0, 0, i5, height, false);
            this.shaderCallbacks.setPlaneU();
            VideoFrameDrawer.drawTexture(this.drawer, textureBuffer2, matrix, width, height, width, height, 0, height, i5 / 2, i3, false);
            this.shaderCallbacks.setPlaneV();
            VideoFrameDrawer.drawTexture(this.drawer, textureBuffer2, matrix, width, height, width, height, i5 / 2, height, i5 / 2, i3, false);
            GLES20.glReadPixels(0, 0, this.i420TextureFrameBuffer.getWidth(), this.i420TextureFrameBuffer.getHeight(), 6408, 5121, byteBuffer);
            GlUtil.checkNoGLES2Error("YuvConverter.convert");
            i = 0;
            try {
                GLES20.glBindFramebuffer(36160, 0);
            } catch (Exception e3) {
                e = e3;
                FileLog.e(e);
                int i6 = (i2 * height) + i;
                int i7 = i2 / 2;
                int i8 = i6 + i7;
                byteBuffer.position(i);
                byteBuffer.limit(i6);
                ByteBuffer slice = byteBuffer.slice();
                byteBuffer.position(i6);
                int i9 = ((i3 - 1) * i2) + i7;
                byteBuffer.limit(i6 + i9);
                ByteBuffer slice2 = byteBuffer.slice();
                byteBuffer.position(i8);
                byteBuffer.limit(i8 + i9);
                ByteBuffer slice3 = byteBuffer.slice();
                textureBuffer2.release();
                return JavaI420Buffer.wrap(width, height, slice, i2, slice2, i2, slice3, i2, new Runnable(byteBuffer) { // from class: org.webrtc.YuvConverter$$ExternalSyntheticLambda0
                    public final /* synthetic */ ByteBuffer f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        JniCommon.nativeFreeByteBuffer(this.f$0);
                    }
                });
            }
        } catch (Exception e4) {
            e = e4;
            i = 0;
            FileLog.e(e);
            int i6 = (i2 * height) + i;
            int i7 = i2 / 2;
            int i8 = i6 + i7;
            byteBuffer.position(i);
            byteBuffer.limit(i6);
            ByteBuffer slice = byteBuffer.slice();
            byteBuffer.position(i6);
            int i9 = ((i3 - 1) * i2) + i7;
            byteBuffer.limit(i6 + i9);
            ByteBuffer slice2 = byteBuffer.slice();
            byteBuffer.position(i8);
            byteBuffer.limit(i8 + i9);
            ByteBuffer slice3 = byteBuffer.slice();
            textureBuffer2.release();
            return JavaI420Buffer.wrap(width, height, slice, i2, slice2, i2, slice3, i2, new Runnable(byteBuffer) { // from class: org.webrtc.YuvConverter$$ExternalSyntheticLambda0
                public final /* synthetic */ ByteBuffer f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    JniCommon.nativeFreeByteBuffer(this.f$0);
                }
            });
        }
        int i6 = (i2 * height) + i;
        int i7 = i2 / 2;
        int i8 = i6 + i7;
        byteBuffer.position(i);
        byteBuffer.limit(i6);
        ByteBuffer slice = byteBuffer.slice();
        byteBuffer.position(i6);
        int i9 = ((i3 - 1) * i2) + i7;
        byteBuffer.limit(i6 + i9);
        ByteBuffer slice2 = byteBuffer.slice();
        byteBuffer.position(i8);
        byteBuffer.limit(i8 + i9);
        ByteBuffer slice3 = byteBuffer.slice();
        textureBuffer2.release();
        return JavaI420Buffer.wrap(width, height, slice, i2, slice2, i2, slice3, i2, new Runnable(byteBuffer) { // from class: org.webrtc.YuvConverter$$ExternalSyntheticLambda0
            public final /* synthetic */ ByteBuffer f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JniCommon.nativeFreeByteBuffer(this.f$0);
            }
        });
    }

    public void release() {
        this.threadChecker.checkIsOnValidThread();
        this.drawer.release();
        this.i420TextureFrameBuffer.release();
        this.videoFrameDrawer.release();
        this.threadChecker.detachThread();
    }
}
