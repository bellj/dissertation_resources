package org.aspectj.runtime.reflect;

/* loaded from: classes.dex */
abstract class MemberSignatureImpl extends SignatureImpl {
    /* access modifiers changed from: package-private */
    public MemberSignatureImpl(int i, String str, Class cls) {
        super(i, str, cls);
    }
}
