package androidx.core.os;

import java.util.Locale;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface LocaleListInterface {
    Locale get(int i);

    Object getLocaleList();

    int size();
}
