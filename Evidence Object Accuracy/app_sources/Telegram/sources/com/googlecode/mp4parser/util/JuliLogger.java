package com.googlecode.mp4parser.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/* loaded from: classes.dex */
public class JuliLogger extends Logger {
    Logger logger;

    public JuliLogger(String str) {
        this.logger = Logger.getLogger(str);
    }

    @Override // com.googlecode.mp4parser.util.Logger
    public void logDebug(String str) {
        this.logger.log(Level.FINE, str);
    }
}
