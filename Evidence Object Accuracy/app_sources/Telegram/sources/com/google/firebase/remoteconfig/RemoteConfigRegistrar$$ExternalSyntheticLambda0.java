package com.google.firebase.remoteconfig;

import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;

/* loaded from: classes.dex */
public final /* synthetic */ class RemoteConfigRegistrar$$ExternalSyntheticLambda0 implements ComponentFactory {
    public static final /* synthetic */ RemoteConfigRegistrar$$ExternalSyntheticLambda0 INSTANCE = new RemoteConfigRegistrar$$ExternalSyntheticLambda0();

    private /* synthetic */ RemoteConfigRegistrar$$ExternalSyntheticLambda0() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return RemoteConfigRegistrar.m49$r8$lambda$LTtV3PapFgaQQj1YKug8GCA2c4(componentContainer);
    }
}
