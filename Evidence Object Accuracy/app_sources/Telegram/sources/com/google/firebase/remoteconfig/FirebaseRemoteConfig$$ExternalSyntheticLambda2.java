package com.google.firebase.remoteconfig;

import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.internal.ConfigFetchHandler;

/* loaded from: classes.dex */
public final /* synthetic */ class FirebaseRemoteConfig$$ExternalSyntheticLambda2 implements SuccessContinuation {
    public static final /* synthetic */ FirebaseRemoteConfig$$ExternalSyntheticLambda2 INSTANCE = new FirebaseRemoteConfig$$ExternalSyntheticLambda2();

    private /* synthetic */ FirebaseRemoteConfig$$ExternalSyntheticLambda2() {
    }

    @Override // com.google.android.gms.tasks.SuccessContinuation
    public final Task then(Object obj) {
        return FirebaseRemoteConfig.m46$r8$lambda$JYPoCswQMeSK7ONQc9hg6EWz1E((ConfigFetchHandler.FetchResponse) obj);
    }
}
