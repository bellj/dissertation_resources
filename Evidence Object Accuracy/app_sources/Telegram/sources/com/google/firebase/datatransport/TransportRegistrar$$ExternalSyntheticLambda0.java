package com.google.firebase.datatransport;

import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;

/* loaded from: classes.dex */
public final /* synthetic */ class TransportRegistrar$$ExternalSyntheticLambda0 implements ComponentFactory {
    public static final /* synthetic */ TransportRegistrar$$ExternalSyntheticLambda0 INSTANCE = new TransportRegistrar$$ExternalSyntheticLambda0();

    private /* synthetic */ TransportRegistrar$$ExternalSyntheticLambda0() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return TransportRegistrar.$r8$lambda$cPZPPfWZLxVwhtSgzJNU9TpSidE(componentContainer);
    }
}
