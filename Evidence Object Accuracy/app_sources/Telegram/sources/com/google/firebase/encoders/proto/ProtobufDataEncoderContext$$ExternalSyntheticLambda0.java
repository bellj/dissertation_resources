package com.google.firebase.encoders.proto;

import com.google.firebase.encoders.ObjectEncoder;
import com.google.firebase.encoders.ObjectEncoderContext;
import java.util.Map;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes.dex */
public final /* synthetic */ class ProtobufDataEncoderContext$$ExternalSyntheticLambda0 implements ObjectEncoder {
    public static final /* synthetic */ ProtobufDataEncoderContext$$ExternalSyntheticLambda0 INSTANCE = new ProtobufDataEncoderContext$$ExternalSyntheticLambda0();

    private /* synthetic */ ProtobufDataEncoderContext$$ExternalSyntheticLambda0() {
    }

    @Override // com.google.firebase.encoders.ObjectEncoder
    public final void encode(Object obj, Object obj2) {
        ProtobufDataEncoderContext.$r8$lambda$oWQEqgEgcHEnNz1JmQZ0roIcWwY((Map.Entry) obj, (ObjectEncoderContext) obj2);
    }
}
