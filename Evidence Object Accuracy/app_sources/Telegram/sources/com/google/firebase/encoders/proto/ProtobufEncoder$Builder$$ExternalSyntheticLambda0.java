package com.google.firebase.encoders.proto;

import com.google.firebase.encoders.ObjectEncoder;
import com.google.firebase.encoders.ObjectEncoderContext;
import com.google.firebase.encoders.proto.ProtobufEncoder;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes.dex */
public final /* synthetic */ class ProtobufEncoder$Builder$$ExternalSyntheticLambda0 implements ObjectEncoder {
    public static final /* synthetic */ ProtobufEncoder$Builder$$ExternalSyntheticLambda0 INSTANCE = new ProtobufEncoder$Builder$$ExternalSyntheticLambda0();

    private /* synthetic */ ProtobufEncoder$Builder$$ExternalSyntheticLambda0() {
    }

    @Override // com.google.firebase.encoders.ObjectEncoder
    public final void encode(Object obj, Object obj2) {
        ProtobufEncoder.Builder.lambda$static$0(obj, (ObjectEncoderContext) obj2);
    }
}
