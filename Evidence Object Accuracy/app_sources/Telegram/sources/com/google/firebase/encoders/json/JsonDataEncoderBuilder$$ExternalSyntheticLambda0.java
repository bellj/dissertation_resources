package com.google.firebase.encoders.json;

import com.google.firebase.encoders.ObjectEncoder;
import com.google.firebase.encoders.ObjectEncoderContext;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes.dex */
public final /* synthetic */ class JsonDataEncoderBuilder$$ExternalSyntheticLambda0 implements ObjectEncoder {
    public static final /* synthetic */ JsonDataEncoderBuilder$$ExternalSyntheticLambda0 INSTANCE = new JsonDataEncoderBuilder$$ExternalSyntheticLambda0();

    private /* synthetic */ JsonDataEncoderBuilder$$ExternalSyntheticLambda0() {
    }

    @Override // com.google.firebase.encoders.ObjectEncoder
    public final void encode(Object obj, Object obj2) {
        JsonDataEncoderBuilder.lambda$static$0(obj, (ObjectEncoderContext) obj2);
    }
}
