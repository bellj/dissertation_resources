package com.google.firebase.abt.component;

import com.google.firebase.components.ComponentContainer;
import com.google.firebase.components.ComponentFactory;

/* loaded from: classes.dex */
public final /* synthetic */ class AbtRegistrar$$ExternalSyntheticLambda0 implements ComponentFactory {
    public static final /* synthetic */ AbtRegistrar$$ExternalSyntheticLambda0 INSTANCE = new AbtRegistrar$$ExternalSyntheticLambda0();

    private /* synthetic */ AbtRegistrar$$ExternalSyntheticLambda0() {
    }

    @Override // com.google.firebase.components.ComponentFactory
    public final Object create(ComponentContainer componentContainer) {
        return AbtRegistrar.$r8$lambda$3USTY8KnDrWGbMR1g4fhgVI5TdM(componentContainer);
    }
}
