package com.google.firebase.appindexing;

import com.google.firebase.appindexing.internal.zzb;

/* compiled from: com.google.firebase:firebase-appindexing@@20.0.0 */
/* loaded from: classes.dex */
public class Action$Metadata$Builder {
    private boolean zza = true;

    public final zzb zza() {
        return new zzb(this.zza, null, null, null, false);
    }
}
