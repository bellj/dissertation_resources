package com.google.firebase.appindexing.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.icing.zzaa;
import com.google.android.gms.internal.icing.zzak;

/* compiled from: com.google.firebase:firebase-appindexing@@20.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzq extends zzs {
    final /* synthetic */ zzc[] zza;

    /* access modifiers changed from: package-private */
    public zzq(zzt zzt, zzc[] zzcArr) {
        this.zza = zzcArr;
    }

    @Override // com.google.firebase.appindexing.internal.zzs
    protected final void zza(zzaa zzaa) throws RemoteException {
        zzaa.zze(new zzak(this), this.zza);
    }
}
