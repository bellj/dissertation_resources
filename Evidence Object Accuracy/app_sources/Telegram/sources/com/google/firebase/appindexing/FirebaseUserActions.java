package com.google.firebase.appindexing;

import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.internal.zzt;
import java.lang.ref.WeakReference;
import javax.annotation.concurrent.GuardedBy;

/* compiled from: com.google.firebase:firebase-appindexing@@20.0.0 */
/* loaded from: classes.dex */
public abstract class FirebaseUserActions {
    @GuardedBy("FirebaseUserActions.class")
    private static WeakReference<FirebaseUserActions> zza;

    public static synchronized FirebaseUserActions getInstance(Context context) {
        FirebaseUserActions firebaseUserActions;
        synchronized (FirebaseUserActions.class) {
            Preconditions.checkNotNull(context);
            WeakReference<FirebaseUserActions> weakReference = zza;
            if (weakReference == null) {
                firebaseUserActions = null;
            } else {
                firebaseUserActions = weakReference.get();
            }
            if (firebaseUserActions != null) {
                return firebaseUserActions;
            }
            zzt zzt = new zzt(context.getApplicationContext());
            zza = new WeakReference<>(zzt);
            return zzt;
        }
    }

    public abstract Task<Void> end(Action action);
}
