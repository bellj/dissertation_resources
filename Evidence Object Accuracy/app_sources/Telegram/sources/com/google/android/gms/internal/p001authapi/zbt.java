package com.google.android.gms.internal.p001authapi;

import android.os.IBinder;

/* compiled from: com.google.android.gms:play-services-auth@@19.2.0 */
/* renamed from: com.google.android.gms.internal.auth-api.zbt  reason: invalid package */
/* loaded from: classes.dex */
public final class zbt extends zba {
    /* access modifiers changed from: package-private */
    public zbt(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
    }
}
