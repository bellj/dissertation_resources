package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzid$zzl extends zzeo.zzc<zzid$zzl, zza> {
    private static final zzid$zzl zzf;
    private static volatile zzgj<zzid$zzl> zzg;
    private zzew<zzb> zzd = zzeo.zzl();
    private byte zze = 2;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo<zzb, zza> implements zzgb {
        private static final zzb zzg;
        private static volatile zzgj<zzb> zzh;
        private int zzc;
        private int zzd;
        private int zze;
        private zzew<zzid$zzk> zzf = zzeo.zzl();

        private zzb() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzb, zza> implements zzgb {
            private zza() {
                super(zzb.zzg);
            }

            /* synthetic */ zza(zzic zzic) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x005d;
                    case 2: goto L_0x0057;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zzh
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zzh     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zzg     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zzh = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zzg
                return r2
            L_0x0033:
                r2 = 5
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                r3 = 2
                java.lang.String r4 = "zze"
                r2[r3] = r4
                r3 = 3
                java.lang.String r4 = "zzf"
                r2[r3] = r4
                r3 = 4
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzk> r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzk.class
                r2[r3] = r4
                java.lang.String r3 = "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0001\u0000\u0001င\u0000\u0002င\u0001\u0003\u001b"
                com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zzg
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0057:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb$zza
                r2.<init>(r3)
                return r2
            L_0x005d:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzb zzb = new zzb();
            zzg = zzb;
            zzeo.zza(zzb.class, zzb);
        }
    }

    private zzid$zzl() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzd<zzid$zzl, zza> {
        private zza() {
            super(zzid$zzl.zzf);
        }

        /* synthetic */ zza(zzic zzic) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzl>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r3, java.lang.Object r4, java.lang.Object r5) {
        /*
            r2 = this;
            int[] r5 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
            r0 = 1
            int r3 = r3 - r0
            r3 = r5[r3]
            r5 = 0
            r1 = 0
            switch(r3) {
                case 1: goto L_0x0056;
                case 2: goto L_0x0050;
                case 3: goto L_0x003c;
                case 4: goto L_0x0039;
                case 5: goto L_0x001f;
                case 6: goto L_0x0018;
                case 7: goto L_0x0011;
                default: goto L_0x000b;
            }
        L_0x000b:
            java.lang.UnsupportedOperationException r3 = new java.lang.UnsupportedOperationException
            r3.<init>()
            throw r3
        L_0x0011:
            if (r4 != 0) goto L_0x0014
            r0 = 0
        L_0x0014:
            byte r3 = (byte) r0
            r2.zze = r3
            return r1
        L_0x0018:
            byte r3 = r2.zze
            java.lang.Byte r3 = java.lang.Byte.valueOf(r3)
            return r3
        L_0x001f:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzl> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzg
            if (r3 != 0) goto L_0x0038
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzl> r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.class
            monitor-enter(r4)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzl> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzg     // Catch: all -> 0x0035
            if (r3 != 0) goto L_0x0033
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x0035
            com.google.android.gms.internal.mlkit_language_id.zzid$zzl r5 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzf     // Catch: all -> 0x0035
            r3.<init>(r5)     // Catch: all -> 0x0035
            com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzg = r3     // Catch: all -> 0x0035
        L_0x0033:
            monitor-exit(r4)     // Catch: all -> 0x0035
            goto L_0x0038
        L_0x0035:
            r3 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x0035
            throw r3
        L_0x0038:
            return r3
        L_0x0039:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzl r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzf
            return r3
        L_0x003c:
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r4 = "zzd"
            r3[r5] = r4
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zzb> r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzb.class
            r3[r0] = r4
            java.lang.String r4 = "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b"
            com.google.android.gms.internal.mlkit_language_id.zzid$zzl r5 = com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zzf
            java.lang.Object r3 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r5, r4, r3)
            return r3
        L_0x0050:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzl$zza
            r3.<init>(r1)
            return r3
        L_0x0056:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzl r3 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzl
            r3.<init>()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzl.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzid$zzl zzid_zzl = new zzid$zzl();
        zzf = zzid_zzl;
        zzeo.zza(zzid$zzl.class, zzid_zzl);
    }
}
