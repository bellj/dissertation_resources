package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import org.telegram.tgnet.ConnectionsManager;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzao extends zzeo<zzy$zzao, zzc> implements zzgb {
    private static final zzex<Integer, zza> zzg = new zzbk();
    private static final zzex<Integer, zzb> zzi = new zzbj();
    private static final zzy$zzao zzk;
    private static volatile zzgj<zzy$zzao> zzl;
    private int zzc;
    private zzy$zzaf zzd;
    private zzci$zza zze;
    private zzeu zzf = zzeo.zzk();
    private zzeu zzh = zzeo.zzk();
    private zzy$zzae zzj;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zza implements zzet {
        FORMAT_UNKNOWN(0),
        FORMAT_CODE_128(1),
        FORMAT_CODE_39(2),
        FORMAT_CODE_93(4),
        FORMAT_CODABAR(8),
        FORMAT_DATA_MATRIX(16),
        FORMAT_EAN_13(32),
        FORMAT_EAN_8(64),
        FORMAT_ITF(ConnectionsManager.RequestFlagNeedQuickAck),
        FORMAT_QR_CODE(256),
        FORMAT_UPC_A(512),
        FORMAT_UPC_E(1024),
        FORMAT_PDF417(2048),
        FORMAT_AZTEC(4096);
        
        private final int zzp;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzp;
        }

        public static zzev zzb() {
            return zzbm.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzp + " name=" + name() + '>';
        }

        zza(int i) {
            this.zzp = i;
        }

        static {
            new zzbl();
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zzb implements zzet {
        TYPE_UNKNOWN(0),
        TYPE_CONTACT_INFO(1),
        TYPE_EMAIL(2),
        TYPE_ISBN(3),
        TYPE_PHONE(4),
        TYPE_PRODUCT(5),
        TYPE_SMS(6),
        TYPE_TEXT(7),
        TYPE_URL(8),
        TYPE_WIFI(9),
        TYPE_GEO(10),
        TYPE_CALENDAR_EVENT(11),
        TYPE_DRIVER_LICENSE(12);
        
        private final int zzo;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzo;
        }

        public static zzev zzb() {
            return zzbn.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzo + " name=" + name() + '>';
        }

        zzb(int i) {
            this.zzo = i;
        }

        static {
            new zzbo();
        }
    }

    private zzy$zzao() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzc extends zzeo.zzb<zzy$zzao, zzc> implements zzgb {
        private zzc() {
            super(zzy$zzao.zzk);
        }

        /* synthetic */ zzc(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzao>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0071;
                case 2: goto L_0x006b;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzao> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzl
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzao> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzao> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzl     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzao r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzk     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzl = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzao r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzk
            return r2
        L_0x0033:
            r2 = 8
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zza.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 6
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzb.zzb()
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001e\u0004\u001e\u0005ဉ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzao r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zzk
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x006b:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzao$zzc r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzao$zzc
            r2.<init>(r3)
            return r2
        L_0x0071:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzao r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzao
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzao.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.mlkit_language_id.zzbk, com.google.android.gms.internal.mlkit_language_id.zzex<java.lang.Integer, com.google.android.gms.internal.mlkit_language_id.zzy$zzao$zza>] */
    /* JADX WARN: Type inference failed for: r0v1, types: [com.google.android.gms.internal.mlkit_language_id.zzex<java.lang.Integer, com.google.android.gms.internal.mlkit_language_id.zzy$zzao$zzb>, com.google.android.gms.internal.mlkit_language_id.zzbj] */
    static {
        zzy$zzao zzy_zzao = new zzy$zzao();
        zzk = zzy_zzao;
        zzeo.zza(zzy$zzao.class, zzy_zzao);
    }
}
