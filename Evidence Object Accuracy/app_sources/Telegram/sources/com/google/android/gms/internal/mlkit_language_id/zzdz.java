package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public abstract class zzdz {
    /* access modifiers changed from: package-private */
    public static zzdz zza(byte[] bArr, int i, int i2, boolean z) {
        zzeb zzeb = new zzeb(bArr, i2);
        try {
            zzeb.zza(i2);
            return zzeb;
        } catch (zzez e) {
            throw new IllegalArgumentException(e);
        }
    }

    public abstract int zza();

    public abstract int zza(int i) throws zzez;

    private zzdz() {
    }
}
