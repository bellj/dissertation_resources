package com.google.android.gms.internal.play_billing;

import java.util.AbstractMap;

/* compiled from: com.android.billingclient:billing@@5.0.0 */
/* loaded from: classes.dex */
final class zzab extends zzu {
    final /* synthetic */ zzac zza;

    /* access modifiers changed from: package-private */
    public zzab(zzac zzac) {
        this.zza = zzac;
    }

    @Override // java.util.List
    public final /* bridge */ /* synthetic */ Object get(int i) {
        zzm.zza(i, this.zza.zzc, "index");
        zzac zzac = this.zza;
        int i2 = i + i;
        Object obj = zzac.zzb[i2];
        obj.getClass();
        Object obj2 = zzac.zzb[i2 + 1];
        obj2.getClass();
        return new AbstractMap.SimpleImmutableEntry(obj, obj2);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.zza.zzc;
    }

    @Override // com.google.android.gms.internal.play_billing.zzr
    public final boolean zzf() {
        return true;
    }
}
