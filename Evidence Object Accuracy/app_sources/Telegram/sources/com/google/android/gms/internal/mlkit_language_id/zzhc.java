package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzhc implements zzhf {
    private final /* synthetic */ zzdn zza;

    /* access modifiers changed from: package-private */
    public zzhc(zzdn zzdn) {
        this.zza = zzdn;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzhf
    public final int zza() {
        return this.zza.zza();
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzhf
    public final byte zza(int i) {
        return this.zza.zza(i);
    }
}
