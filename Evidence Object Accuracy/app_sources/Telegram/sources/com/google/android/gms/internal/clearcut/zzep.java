package com.google.android.gms.internal.clearcut;

import java.util.Map;

/* JADX WARN: Incorrect field signature: TK; */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzep implements Comparable<zzep>, Map.Entry<K, V> {
    private V value;
    private final /* synthetic */ zzei zzos;
    private final Comparable zzov;

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: K */
    /* JADX WARN: Multi-variable type inference failed */
    /* access modifiers changed from: package-private */
    public zzep(zzei zzei, K k, V v) {
        this.zzos = zzei;
        this.zzov = k;
        this.value = v;
    }

    /* access modifiers changed from: package-private */
    public zzep(zzei zzei, Map.Entry<K, V> entry) {
        this(zzei, (Comparable) entry.getKey(), entry.getValue());
    }

    private static boolean equals(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(zzep zzep) {
        return ((Comparable) getKey()).compareTo((Comparable) zzep.getKey());
    }

    @Override // java.lang.Object, java.util.Map.Entry
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return equals(this.zzov, entry.getKey()) && equals(this.value, entry.getValue());
    }

    @Override // java.util.Map.Entry
    public final /* synthetic */ Object getKey() {
        return this.zzov;
    }

    @Override // java.util.Map.Entry
    public final V getValue() {
        return this.value;
    }

    @Override // java.lang.Object, java.util.Map.Entry
    public final int hashCode() {
        Comparable comparable = this.zzov;
        int i = 0;
        int hashCode = comparable == null ? 0 : comparable.hashCode();
        V v = this.value;
        if (v != 0) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        this.zzos.zzdu();
        V v2 = this.value;
        this.value = v;
        return v2;
    }

    @Override // java.lang.Object
    public final String toString() {
        String valueOf = String.valueOf(this.zzov);
        String valueOf2 = String.valueOf(this.value);
        StringBuilder sb = new StringBuilder(valueOf.length() + 1 + valueOf2.length());
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        return sb.toString();
    }
}
