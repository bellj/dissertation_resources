package com.google.android.gms.internal.mlkit_language_id;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzv extends WeakReference<Throwable> {
    private final int zza;

    public zzv(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.zza = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.zza;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == zzv.class) {
            if (this == obj) {
                return true;
            }
            zzv zzv = (zzv) obj;
            if (this.zza == zzv.zza && get() == zzv.get()) {
                return true;
            }
        }
        return false;
    }
}
