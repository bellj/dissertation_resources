package com.google.android.gms.internal.vision;

import org.telegram.tgnet.ConnectionsManager;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
final class zzmj extends zzme {
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0061, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b6, code lost:
        return -1;
     */
    @Override // com.google.android.gms.internal.vision.zzme
    /* Code decompiled incorrectly, please refer to instructions dump. */
    final int zza(int r16, byte[] r17, int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 219
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzmj.zza(int, byte[], int, int):int");
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzme
    public final String zzb(byte[] bArr, int i, int i2) throws zzjk {
        if ((i | i2 | ((bArr.length - i) - i2)) >= 0) {
            int i3 = i + i2;
            char[] cArr = new char[i2];
            int i4 = 0;
            while (i < i3) {
                byte zza = zzma.zza(bArr, (long) i);
                if (!zzmf.zza(zza)) {
                    break;
                }
                i++;
                zzmf.zza(zza, cArr, i4);
                i4++;
            }
            int i5 = i4;
            while (i < i3) {
                int i6 = i + 1;
                byte zza2 = zzma.zza(bArr, (long) i);
                if (zzmf.zza(zza2)) {
                    int i7 = i5 + 1;
                    zzmf.zza(zza2, cArr, i5);
                    while (i6 < i3) {
                        byte zza3 = zzma.zza(bArr, (long) i6);
                        if (!zzmf.zza(zza3)) {
                            break;
                        }
                        i6++;
                        zzmf.zza(zza3, cArr, i7);
                        i7++;
                    }
                    i = i6;
                    i5 = i7;
                } else if (zzmf.zzb(zza2)) {
                    if (i6 < i3) {
                        zzmf.zza(zza2, zzma.zza(bArr, (long) i6), cArr, i5);
                        i = i6 + 1;
                        i5++;
                    } else {
                        throw zzjk.zzh();
                    }
                } else if (zzmf.zzc(zza2)) {
                    if (i6 < i3 - 1) {
                        int i8 = i6 + 1;
                        zzmf.zza(zza2, zzma.zza(bArr, (long) i6), zzma.zza(bArr, (long) i8), cArr, i5);
                        i = i8 + 1;
                        i5++;
                    } else {
                        throw zzjk.zzh();
                    }
                } else if (i6 < i3 - 2) {
                    int i9 = i6 + 1;
                    int i10 = i9 + 1;
                    zzmf.zza(zza2, zzma.zza(bArr, (long) i6), zzma.zza(bArr, (long) i9), zzma.zza(bArr, (long) i10), cArr, i5);
                    i5 = i5 + 1 + 1;
                    i = i10 + 1;
                } else {
                    throw zzjk.zzh();
                }
            }
            return new String(cArr, 0, i5);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzme
    public final int zza(CharSequence charSequence, byte[] bArr, int i, int i2) {
        char c;
        long j;
        long j2;
        long j3;
        int i3;
        char charAt;
        long j4 = (long) i;
        long j5 = ((long) i2) + j4;
        int length = charSequence.length();
        if (length > i2 || bArr.length - i2 < i) {
            char charAt2 = charSequence.charAt(length - 1);
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt2);
            sb.append(" at index ");
            sb.append(i + i2);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i4 = 0;
        while (true) {
            c = 128;
            j = 1;
            if (i4 >= length || (charAt = charSequence.charAt(i4)) >= 128) {
                break;
            }
            zzma.zza(bArr, j4, (byte) charAt);
            i4++;
            j4 = 1 + j4;
        }
        if (i4 == length) {
            return (int) j4;
        }
        while (i4 < length) {
            char charAt3 = charSequence.charAt(i4);
            if (charAt3 >= c || j4 >= j5) {
                if (charAt3 < 2048 && j4 <= j5 - 2) {
                    long j6 = j4 + j;
                    zzma.zza(bArr, j4, (byte) ((charAt3 >>> 6) | 960));
                    zzma.zza(bArr, j6, (byte) ((charAt3 & '?') | ConnectionsManager.RequestFlagNeedQuickAck));
                    j2 = j6 + j;
                    j3 = j;
                } else if ((charAt3 < 55296 || 57343 < charAt3) && j4 <= j5 - 3) {
                    long j7 = j4 + j;
                    zzma.zza(bArr, j4, (byte) ((charAt3 >>> '\f') | 480));
                    long j8 = j7 + j;
                    zzma.zza(bArr, j7, (byte) (((charAt3 >>> 6) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                    zzma.zza(bArr, j8, (byte) ((charAt3 & '?') | ConnectionsManager.RequestFlagNeedQuickAck));
                    j2 = j8 + 1;
                    j3 = 1;
                } else if (j4 <= j5 - 4) {
                    int i5 = i4 + 1;
                    if (i5 != length) {
                        char charAt4 = charSequence.charAt(i5);
                        if (Character.isSurrogatePair(charAt3, charAt4)) {
                            int codePoint = Character.toCodePoint(charAt3, charAt4);
                            long j9 = j4 + 1;
                            zzma.zza(bArr, j4, (byte) ((codePoint >>> 18) | 240));
                            long j10 = j9 + 1;
                            zzma.zza(bArr, j9, (byte) (((codePoint >>> 12) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            long j11 = j10 + 1;
                            zzma.zza(bArr, j10, (byte) (((codePoint >>> 6) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            j3 = 1;
                            j2 = j11 + 1;
                            zzma.zza(bArr, j11, (byte) ((codePoint & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            i4 = i5;
                        } else {
                            i4 = i5;
                        }
                    }
                    throw new zzmg(i4 - 1, length);
                } else if (55296 > charAt3 || charAt3 > 57343 || ((i3 = i4 + 1) != length && Character.isSurrogatePair(charAt3, charSequence.charAt(i3)))) {
                    StringBuilder sb2 = new StringBuilder(46);
                    sb2.append("Failed writing ");
                    sb2.append(charAt3);
                    sb2.append(" at index ");
                    sb2.append(j4);
                    throw new ArrayIndexOutOfBoundsException(sb2.toString());
                } else {
                    throw new zzmg(i4, length);
                }
                i4++;
                c = 128;
                j4 = j2;
                j = j3;
            } else {
                long j12 = j4 + j;
                zzma.zza(bArr, j4, (byte) charAt3);
                j3 = j;
                j2 = j12;
            }
            i4++;
            c = 128;
            j4 = j2;
            j = j3;
        }
        return (int) j4;
    }

    private static int zza(byte[] bArr, int i, long j, int i2) {
        if (i2 == 0) {
            return zzmd.zzb(i);
        }
        if (i2 == 1) {
            return zzmd.zzb(i, zzma.zza(bArr, j));
        }
        if (i2 == 2) {
            return zzmd.zzb(i, zzma.zza(bArr, j), zzma.zza(bArr, j + 1));
        }
        throw new AssertionError();
    }
}
