package com.google.android.gms.maps;

import android.os.RemoteException;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.zzaq;

/* compiled from: com.google.android.gms:play-services-maps@@17.0.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzaf extends zzaq {
    final /* synthetic */ OnMapReadyCallback zza;

    /* access modifiers changed from: package-private */
    public zzaf(zzag zzag, OnMapReadyCallback onMapReadyCallback) {
        this.zza = onMapReadyCallback;
    }

    @Override // com.google.android.gms.maps.internal.zzar
    public final void zzb(IGoogleMapDelegate iGoogleMapDelegate) throws RemoteException {
        this.zza.onMapReady(new GoogleMap(iGoogleMapDelegate));
    }
}
