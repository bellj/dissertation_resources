package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zaax extends zabm {
    private WeakReference<zaar> zaa;

    /* access modifiers changed from: package-private */
    public zaax(zaar zaar) {
        this.zaa = new WeakReference<>(zaar);
    }

    @Override // com.google.android.gms.common.api.internal.zabm
    public final void zaa() {
        zaar zaar = this.zaa.get();
        if (zaar != null) {
            zaar.zae();
        }
    }
}
