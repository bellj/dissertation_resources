package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzeh extends zzee<zzeo.zzf> {
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzee
    public final boolean zza(zzfz zzfz) {
        return zzfz instanceof zzeo.zzc;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzee
    public final zzej<zzeo.zzf> zza(Object obj) {
        return ((zzeo.zzc) obj).zzc;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzee
    public final zzej<zzeo.zzf> zzb(Object obj) {
        zzeo.zzc zzc = (zzeo.zzc) obj;
        if (zzc.zzc.zzc()) {
            zzc.zzc = (zzej) zzc.zzc.clone();
        }
        return zzc.zzc;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzee
    public final void zzc(Object obj) {
        zza(obj).zzb();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzee
    public final int zza(Map.Entry<?, ?> entry) {
        zzeo.zzf zzf = (zzeo.zzf) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzee
    public final void zza(zzib zzib, Map.Entry<?, ?> entry) throws IOException {
        zzeo.zzf zzf = (zzeo.zzf) entry.getKey();
        throw new NoSuchMethodError();
    }
}
