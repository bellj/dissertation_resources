package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zziw;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public abstract class zziq<T extends zziw<T>> {
    /* access modifiers changed from: package-private */
    public abstract int zza(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    public abstract zziu<T> zza(Object obj);

    /* access modifiers changed from: package-private */
    public abstract Object zza(zzio zzio, zzkk zzkk, int i);

    /* access modifiers changed from: package-private */
    public abstract void zza(zzmr zzmr, Map.Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzkk zzkk);

    /* access modifiers changed from: package-private */
    public abstract zziu<T> zzb(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zzc(Object obj);
}
