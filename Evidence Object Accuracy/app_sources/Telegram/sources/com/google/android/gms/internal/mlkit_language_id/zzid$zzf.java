package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzid$zzf extends zzeo<zzid$zzf, zza> implements zzgb {
    private static final zzid$zzf zzo;
    private static volatile zzgj<zzid$zzf> zzp;
    private int zzc;
    private zzid$zzb zzd;
    private zzid$zzi zze;
    private zzid$zzd zzf;
    private int zzg;
    private zzid$zzc zzh;
    private zzid$zzl zzi;
    private long zzj;
    private long zzk;
    private boolean zzl;
    private int zzm;
    private byte zzn = 2;

    private zzid$zzf() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzid$zzf, zza> implements zzgb {
        private zza() {
            super(zzid$zzf.zzo);
        }

        /* synthetic */ zza(zzic zzic) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzf>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r3, java.lang.Object r4, java.lang.Object r5) {
        /*
            r2 = this;
            int[] r5 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
            r0 = 1
            int r3 = r3 - r0
            r3 = r5[r3]
            r5 = 0
            r1 = 0
            switch(r3) {
                case 1: goto L_0x008f;
                case 2: goto L_0x0089;
                case 3: goto L_0x003c;
                case 4: goto L_0x0039;
                case 5: goto L_0x001f;
                case 6: goto L_0x0018;
                case 7: goto L_0x0011;
                default: goto L_0x000b;
            }
        L_0x000b:
            java.lang.UnsupportedOperationException r3 = new java.lang.UnsupportedOperationException
            r3.<init>()
            throw r3
        L_0x0011:
            if (r4 != 0) goto L_0x0014
            r0 = 0
        L_0x0014:
            byte r3 = (byte) r0
            r2.zzn = r3
            return r1
        L_0x0018:
            byte r3 = r2.zzn
            java.lang.Byte r3 = java.lang.Byte.valueOf(r3)
            return r3
        L_0x001f:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzf> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zzp
            if (r3 != 0) goto L_0x0038
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzf> r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzf.class
            monitor-enter(r4)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzf> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zzp     // Catch: all -> 0x0035
            if (r3 != 0) goto L_0x0033
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x0035
            com.google.android.gms.internal.mlkit_language_id.zzid$zzf r5 = com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zzo     // Catch: all -> 0x0035
            r3.<init>(r5)     // Catch: all -> 0x0035
            com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zzp = r3     // Catch: all -> 0x0035
        L_0x0033:
            monitor-exit(r4)     // Catch: all -> 0x0035
            goto L_0x0038
        L_0x0035:
            r3 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x0035
            throw r3
        L_0x0038:
            return r3
        L_0x0039:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzf r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zzo
            return r3
        L_0x003c:
            r3 = 12
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r4 = "zzc"
            r3[r5] = r4
            java.lang.String r4 = "zzd"
            r3[r0] = r4
            r4 = 2
            java.lang.String r5 = "zze"
            r3[r4] = r5
            r4 = 3
            java.lang.String r5 = "zzg"
            r3[r4] = r5
            r4 = 4
            com.google.android.gms.internal.mlkit_language_id.zzev r5 = com.google.android.gms.internal.mlkit_language_id.zzij.zzb()
            r3[r4] = r5
            r4 = 5
            java.lang.String r5 = "zzh"
            r3[r4] = r5
            r4 = 6
            java.lang.String r5 = "zzi"
            r3[r4] = r5
            r4 = 7
            java.lang.String r5 = "zzj"
            r3[r4] = r5
            r4 = 8
            java.lang.String r5 = "zzk"
            r3[r4] = r5
            r4 = 9
            java.lang.String r5 = "zzl"
            r3[r4] = r5
            r4 = 10
            java.lang.String r5 = "zzm"
            r3[r4] = r5
            r4 = 11
            java.lang.String r5 = "zzf"
            r3[r4] = r5
            java.lang.String r4 = "\u0001\n\u0000\u0001\u0001\n\n\u0000\u0000\u0001\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဌ\u0003\u0004ဉ\u0004\u0005ᐉ\u0005\u0006ဂ\u0006\u0007ဂ\u0007\bဇ\b\tင\t\nဉ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzid$zzf r5 = com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zzo
            java.lang.Object r3 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r5, r4, r3)
            return r3
        L_0x0089:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzf$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzf$zza
            r3.<init>(r1)
            return r3
        L_0x008f:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzf r3 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzf
            r3.<init>()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzf.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzid$zzf zzid_zzf = new zzid$zzf();
        zzo = zzid_zzf;
        zzeo.zza(zzid$zzf.class, zzid_zzf);
    }
}
