package com.google.android.gms.common.api.internal;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public abstract class zaay {
    private final zaaw zaa;

    /* access modifiers changed from: protected */
    public zaay(zaaw zaaw) {
        this.zaa = zaaw;
    }

    protected abstract void zaa();

    public final void zaa(zaaz zaaz) {
        zaaz.zaa(zaaz).lock();
        try {
            if (zaaz.zab(zaaz) == this.zaa) {
                zaa();
            }
        } finally {
            zaaz.zaa(zaaz).unlock();
        }
    }
}
