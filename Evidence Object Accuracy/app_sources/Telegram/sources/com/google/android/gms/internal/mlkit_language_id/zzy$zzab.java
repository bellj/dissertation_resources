package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzab extends zzeo<zzy$zzab, zza> implements zzgb {
    private static final zzy$zzab zzj;
    private static volatile zzgj<zzy$zzab> zzk;
    private int zzc;
    private long zzd;
    private long zze;
    private long zzf;
    private long zzg;
    private long zzh;
    private long zzi;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzab, zza> implements zzgb {
        private zza() {
            super(zzy$zzab.zzj);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    static {
        zzy$zzab zzy_zzab = new zzy$zzab();
        zzj = zzy_zzab;
        zzeo.zza(zzy$zzab.class, zzy_zzab);
    }

    private zzy$zzab() {
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzab>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0067;
                case 2: goto L_0x0061;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzab> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zzk
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzab> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzab.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzab> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zzk     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzab r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zzj     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zzk = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzab r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zzj
            return r2
        L_0x0033:
            r2 = 7
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဃ\u0000\u0002ဃ\u0001\u0003ဃ\u0002\u0004ဃ\u0003\u0005ဃ\u0004\u0006ဃ\u0005"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzab r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zzj
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0061:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzab$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzab$zza
            r2.<init>(r3)
            return r2
        L_0x0067:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzab r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzab
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzab.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
