package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzfk extends zzfj {
    private zzfk() {
        super();
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfj
    final void zza(Object obj, long j) {
        zzb(obj, j).b_();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzfj
    final <E> void zza(Object obj, Object obj2, long j) {
        zzew<E> zzb = zzb(obj, j);
        zzew<E> zzb2 = zzb(obj2, j);
        int size = zzb.size();
        int size2 = zzb2.size();
        zzew<E> zzew = zzb;
        zzew = zzb;
        if (size > 0 && size2 > 0) {
            boolean zza = zzb.zza();
            zzew<E> zzew2 = zzb;
            if (!zza) {
                zzew2 = zzb.zzb(size2 + size);
            }
            zzew2.addAll(zzb2);
            zzew = zzew2;
        }
        if (size > 0) {
            zzb2 = zzew;
        }
        zzhn.zza(obj, j, zzb2);
    }

    private static <E> zzew<E> zzb(Object obj, long j) {
        return (zzew) zzhn.zzf(obj, j);
    }
}
