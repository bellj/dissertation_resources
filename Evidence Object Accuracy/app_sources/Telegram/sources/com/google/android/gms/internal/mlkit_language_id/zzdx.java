package com.google.android.gms.internal.mlkit_language_id;

import java.io.IOException;
import java.nio.charset.Charset;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public class zzdx extends zzdu {
    protected final byte[] zzb;

    /* access modifiers changed from: package-private */
    public zzdx(byte[] bArr) {
        bArr.getClass();
        this.zzb = bArr;
    }

    protected int zze() {
        return 0;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    public byte zza(int i) {
        return this.zzb[i];
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    public byte zzb(int i) {
        return this.zzb[i];
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    public int zza() {
        return this.zzb.length;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    public final zzdn zza(int i, int i2) {
        int zzb = zzdn.zzb(0, i2, zza());
        if (zzb == 0) {
            return zzdn.zza;
        }
        return new zzdq(this.zzb, zze(), zzb);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    public final void zza(zzdk zzdk) throws IOException {
        zzdk.zza(this.zzb, zze(), zza());
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    protected final String zza(Charset charset) {
        return new String(this.zzb, zze(), zza(), charset);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    public final boolean zzc() {
        int zze = zze();
        return zzhp.zza(this.zzb, zze, zza() + zze);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn, java.lang.Object
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzdn) || zza() != ((zzdn) obj).zza()) {
            return false;
        }
        if (zza() == 0) {
            return true;
        }
        if (!(obj instanceof zzdx)) {
            return obj.equals(this);
        }
        zzdx zzdx = (zzdx) obj;
        int zzd = zzd();
        int zzd2 = zzdx.zzd();
        if (zzd == 0 || zzd2 == 0 || zzd == zzd2) {
            return zza(zzdx, 0, zza());
        }
        return false;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdu
    final boolean zza(zzdn zzdn, int i, int i2) {
        if (i2 > zzdn.zza()) {
            int zza = zza();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(zza);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 > zzdn.zza()) {
            int zza2 = zzdn.zza();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(zza2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(zzdn instanceof zzdx)) {
            return zzdn.zza(0, i2).equals(zza(0, i2));
        } else {
            zzdx zzdx = (zzdx) zzdn;
            byte[] bArr = this.zzb;
            byte[] bArr2 = zzdx.zzb;
            int zze = zze() + i2;
            int zze2 = zze();
            int zze3 = zzdx.zze();
            while (zze2 < zze) {
                if (bArr[zze2] != bArr2[zze3]) {
                    return false;
                }
                zze2++;
                zze3++;
            }
            return true;
        }
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzdn
    protected final int zza(int i, int i2, int i3) {
        return zzeq.zza(i, this.zzb, zze(), i3);
    }
}
