package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import com.google.android.gms.internal.mlkit_language_id.zzeo.zzb;
import j$.util.concurrent.ConcurrentHashMap;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public abstract class zzeo<MessageType extends zzeo<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzde<MessageType, BuilderType> {
    private static Map<Object, zzeo<?, ?>> zzd = new ConcurrentHashMap();
    protected zzhg zzb = zzhg.zza();
    private int zzc = -1;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    protected static class zza<T extends zzeo<T, ?>> extends zzdj<T> {
        public zza(T t) {
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static abstract class zzc<MessageType extends zzc<MessageType, BuilderType>, BuilderType extends zzd<MessageType, BuilderType>> extends zzeo<MessageType, BuilderType> implements zzgb {
        protected zzej<zzf> zzc = zzej.zza();
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* loaded from: classes.dex */
    public static final class zze {
        public static final int zza = 1;
        public static final int zzb = 2;
        public static final int zzc = 3;
        public static final int zzd = 4;
        public static final int zze = 5;
        public static final int zzf = 6;
        public static final int zzg = 7;
        private static final /* synthetic */ int[] zzh = {1, 2, 3, 4, 5, 6, 7};

        public static int[] zza() {
            return (int[]) zzh.clone();
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    static final class zzf implements zzel<zzf> {
        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final int zza() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final zzhv zzb() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final zzhy zzc() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final boolean zzd() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final boolean zze() {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final zzfy zza(zzfy zzfy, zzfz zzfz) {
            throw new NoSuchMethodError();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzel
        public final zzgf zza(zzgf zzgf, zzgf zzgf2) {
            throw new NoSuchMethodError();
        }

        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static abstract class zzd<MessageType extends zzc<MessageType, BuilderType>, BuilderType extends zzd<MessageType, BuilderType>> extends zzb<MessageType, BuilderType> implements zzgb {
        /* access modifiers changed from: protected */
        public zzd(MessageType messagetype) {
            super(messagetype);
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo.zzb
        public void zzc() {
            super.zzc();
            MessageType messagetype = this.zza;
            ((zzc) messagetype).zzc = (zzej) ((zzc) messagetype).zzc.clone();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo.zzb
        public /* synthetic */ zzeo zzd() {
            return (zzc) zzf();
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo.zzb, com.google.android.gms.internal.mlkit_language_id.zzfy
        public /* synthetic */ zzfz zzf() {
            if (this.zzb) {
                return (zzc) this.zza;
            }
            ((zzc) this.zza).zzc.zzb();
            return (zzc) super.zzf();
        }
    }

    public String toString() {
        return zzga.zza(this, super.toString());
    }

    public int hashCode() {
        int i = this.zza;
        if (i != 0) {
            return i;
        }
        int zza2 = zzgk.zza().zza((zzgk) this).zza(this);
        this.zza = zza2;
        return zza2;
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static abstract class zzb<MessageType extends zzeo<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdh<MessageType, BuilderType> {
        protected MessageType zza;
        protected boolean zzb = false;
        private final MessageType zzc;

        /* access modifiers changed from: protected */
        public zzb(MessageType messagetype) {
            this.zzc = messagetype;
            this.zza = (MessageType) ((zzeo) messagetype.zza(zze.zzd, null, null));
        }

        /* access modifiers changed from: protected */
        public void zzc() {
            MessageType messagetype = (MessageType) ((zzeo) this.zza.zza(zze.zzd, null, null));
            zza(messagetype, this.zza);
            this.zza = messagetype;
        }

        /* renamed from: zzd */
        public MessageType zzf() {
            if (this.zzb) {
                return this.zza;
            }
            MessageType messagetype = this.zza;
            zzgk.zza().zza((zzgk) messagetype).zzb(messagetype);
            this.zzb = true;
            return this.zza;
        }

        /* renamed from: zze */
        public final MessageType zzg() {
            MessageType messagetype = (MessageType) ((zzeo) zzf());
            if (messagetype.zzi()) {
                return messagetype;
            }
            throw new zzhe(messagetype);
        }

        public final BuilderType zza(MessageType messagetype) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            zza(this.zza, messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzgk.zza().zza((zzgk) messagetype).zzb(messagetype, messagetype2);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.google.android.gms.internal.mlkit_language_id.zzeo$zzb<MessageType extends com.google.android.gms.internal.mlkit_language_id.zzeo<MessageType, BuilderType>, BuilderType extends com.google.android.gms.internal.mlkit_language_id.zzeo$zzb<MessageType, BuilderType>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzdh
        protected final /* synthetic */ zzdh zza(zzde zzde) {
            return zza((zzb<MessageType, BuilderType>) ((zzeo) zzde));
        }

        @Override // com.google.android.gms.internal.mlkit_language_id.zzgb
        public final /* synthetic */ zzfz zzn() {
            return this.zzc;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.google.android.gms.internal.mlkit_language_id.zzeo$zzb */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Object
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zzb zzb = (zzb) this.zzc.zza(zze.zze, null, null);
            zzb.zza((zzb) ((zzeo) zzf()));
            return zzb;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return zzgk.zza().zza((zzgk) this).zza(this, (zzeo) obj);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzeo<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> BuilderType zzh() {
        return (BuilderType) ((zzb) zza(zze.zze, (Object) null, (Object) null));
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzeo<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> BuilderType zza(MessageType messagetype) {
        return (BuilderType) zzh().zza(messagetype);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgb
    public final boolean zzi() {
        return zza(this, true);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzde
    final int zzg() {
        return this.zzc;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzde
    final void zza(int i) {
        this.zzc = i;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfz
    public final void zza(zzea zzea) throws IOException {
        zzgk.zza().zza((zzgk) this).zza((zzgp) this, (zzib) zzed.zza(zzea));
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfz
    public final int zzj() {
        if (this.zzc == -1) {
            this.zzc = zzgk.zza().zza((zzgk) this).zzd(this);
        }
        return this.zzc;
    }

    /* access modifiers changed from: package-private */
    public static <T extends zzeo<?, ?>> T zza(Class<T> cls) {
        zzeo<?, ?> zzeo = zzd.get(cls);
        if (zzeo == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                zzeo = zzd.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (zzeo == null) {
            zzeo = (T) ((zzeo) ((zzeo) zzhn.zza(cls)).zza(zze.zzf, (Object) null, (Object) null));
            if (zzeo != null) {
                zzd.put(cls, zzeo);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) zzeo;
    }

    /* access modifiers changed from: protected */
    public static <T extends zzeo<?, ?>> void zza(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    /* access modifiers changed from: protected */
    public static Object zza(zzfz zzfz, String str, Object[] objArr) {
        return new zzgm(zzfz, str, objArr);
    }

    /* access modifiers changed from: package-private */
    public static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static final <T extends zzeo<T, ?>> boolean zza(T t, boolean z) {
        byte byteValue = ((Byte) t.zza(zze.zza, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzc2 = zzgk.zza().zza((zzgk) t).zzc(t);
        if (z) {
            t.zza(zze.zzb, zzc2 ? t : null, null);
        }
        return zzc2;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.mlkit_language_id.zzeu, com.google.android.gms.internal.mlkit_language_id.zzer] */
    /* access modifiers changed from: protected */
    public static zzeu zzk() {
        return zzer.zzd();
    }

    /* access modifiers changed from: protected */
    public static <E> zzew<E> zzl() {
        return zzgn.zzd();
    }

    /* access modifiers changed from: protected */
    public static <E> zzew<E> zza(zzew<E> zzew) {
        int size = zzew.size();
        return zzew.zzb(size == 0 ? 10 : size << 1);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfz
    public final /* synthetic */ zzfy zzm() {
        zzb zzb2 = (zzb) zza(zze.zze, (Object) null, (Object) null);
        zzb2.zza((zzb) this);
        return zzb2;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgb
    public final /* synthetic */ zzfz zzn() {
        return (zzeo) zza(zze.zzf, (Object) null, (Object) null);
    }
}
