package com.google.android.gms.internal.clearcut;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzeu implements zzev {
    private final /* synthetic */ zzbb zzox;

    /* access modifiers changed from: package-private */
    public zzeu(zzbb zzbb) {
        this.zzox = zzbb;
    }

    @Override // com.google.android.gms.internal.clearcut.zzev
    public final int size() {
        return this.zzox.size();
    }

    @Override // com.google.android.gms.internal.clearcut.zzev
    public final byte zzj(int i) {
        return this.zzox.zzj(i);
    }
}
