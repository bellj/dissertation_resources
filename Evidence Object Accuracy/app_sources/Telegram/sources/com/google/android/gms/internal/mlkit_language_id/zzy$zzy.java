package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzy extends zzeo<zzy$zzy, zza> implements zzgb {
    private static final zzy$zzy zzi;
    private static volatile zzgj<zzy$zzy> zzj;
    private int zzc;
    private zzy$zzaf zzd;
    private zzy$zzam zze;
    private zzew<zzb> zzf = zzeo.zzl();
    private zzew<zzb> zzg = zzeo.zzl();
    private long zzh;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo<zzb, zza> implements zzgb {
        private static final zzb zzf;
        private static volatile zzgj<zzb> zzg;
        private int zzc;
        private int zzd;
        private zzeu zze = zzeo.zzk();

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* renamed from: com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb$zzb  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public enum EnumC0001zzb implements zzet {
            UNKNOWN_DATA_TYPE(0),
            TYPE_FLOAT32(1),
            TYPE_INT32(2),
            TYPE_BYTE(3),
            TYPE_LONG(4);
            
            private final int zzg;

            @Override // com.google.android.gms.internal.mlkit_language_id.zzet
            public final int zza() {
                return this.zzg;
            }

            public static zzev zzb() {
                return zzaf.zza;
            }

            @Override // java.lang.Enum, java.lang.Object
            public final String toString() {
                return "<" + EnumC0001zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
            }

            EnumC0001zzb(int i) {
                this.zzg = i;
            }

            static {
                new zzae();
            }
        }

        private zzb() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzb, zza> implements zzgb {
            private zza() {
                super(zzb.zzf);
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x005a;
                    case 2: goto L_0x0054;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zzg
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zzg     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zzf     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zzg = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zzf
                return r2
            L_0x0033:
                r2 = 4
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                r3 = 2
                com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.EnumC0001zzb.zzb()
                r2[r3] = r4
                r3 = 3
                java.lang.String r4 = "zze"
                r2[r3] = r4
                java.lang.String r3 = "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဌ\u0000\u0002\u0016"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zzf
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0054:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb$zza
                r2.<init>(r3)
                return r2
            L_0x005a:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzb zzb = new zzb();
            zzf = zzb;
            zzeo.zza(zzb.class, zzb);
        }
    }

    private zzy$zzy() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzy, zza> implements zzgb {
        private zza() {
            super(zzy$zzy.zzi);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzy>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r3, java.lang.Object r4, java.lang.Object r5) {
        /*
            r2 = this;
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb> r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.class
            int[] r5 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r0 = 1
            int r3 = r3 - r0
            r3 = r5[r3]
            r5 = 0
            switch(r3) {
                case 1: goto L_0x006b;
                case 2: goto L_0x0065;
                case 3: goto L_0x0035;
                case 4: goto L_0x0032;
                case 5: goto L_0x0018;
                case 6: goto L_0x0013;
                case 7: goto L_0x0012;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.lang.UnsupportedOperationException r3 = new java.lang.UnsupportedOperationException
            r3.<init>()
            throw r3
        L_0x0012:
            return r5
        L_0x0013:
            java.lang.Byte r3 = java.lang.Byte.valueOf(r0)
            return r3
        L_0x0018:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzy> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzj
            if (r3 != 0) goto L_0x0031
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzy> r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.class
            monitor-enter(r4)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzy> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzj     // Catch: all -> 0x002e
            if (r3 != 0) goto L_0x002c
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002e
            com.google.android.gms.internal.mlkit_language_id.zzy$zzy r5 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzi     // Catch: all -> 0x002e
            r3.<init>(r5)     // Catch: all -> 0x002e
            com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzj = r3     // Catch: all -> 0x002e
        L_0x002c:
            monitor-exit(r4)     // Catch: all -> 0x002e
            goto L_0x0031
        L_0x002e:
            r3 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x002e
            throw r3
        L_0x0031:
            return r3
        L_0x0032:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzy r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzi
            return r3
        L_0x0035:
            r3 = 8
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r5 = 0
            java.lang.String r1 = "zzc"
            r3[r5] = r1
            java.lang.String r5 = "zzd"
            r3[r0] = r5
            r5 = 2
            java.lang.String r0 = "zze"
            r3[r5] = r0
            r5 = 3
            java.lang.String r0 = "zzf"
            r3[r5] = r0
            r5 = 4
            r3[r5] = r4
            r5 = 5
            java.lang.String r0 = "zzg"
            r3[r5] = r0
            r5 = 6
            r3[r5] = r4
            r4 = 7
            java.lang.String r5 = "zzh"
            r3[r4] = r5
            java.lang.String r4 = "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003\u001b\u0004\u001b\u0005ဃ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzy r5 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzi
            java.lang.Object r3 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r5, r4, r3)
            return r3
        L_0x0065:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zza
            r3.<init>(r5)
            return r3
        L_0x006b:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzy r3 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzy
            r3.<init>()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzy zzy_zzy = new zzy$zzy();
        zzi = zzy_zzy;
        zzeo.zza(zzy$zzy.class, zzy_zzy);
    }
}
