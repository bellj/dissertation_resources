package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzbg extends zzeo<zzy$zzbg, zzb> implements zzgb {
    private static final zzex<Integer, zza> zzf = new zzcc();
    private static final zzex<Integer, zza> zzh = new zzce();
    private static final zzex<Integer, zza> zzj = new zzcd();
    private static final zzy$zzbg zzl;
    private static volatile zzgj<zzy$zzbg> zzm;
    private int zzc;
    private long zzd;
    private zzeu zze = zzeo.zzk();
    private zzeu zzg = zzeo.zzk();
    private zzeu zzi = zzeo.zzk();
    private int zzk;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zza implements zzet {
        UNKNOWN_ERROR(0),
        NO_CONNECTION(1),
        RPC_ERROR(2),
        RPC_RETURNED_INVALID_RESULT(3),
        RPC_RETURNED_MALFORMED_RESULT(4),
        RPC_EXPONENTIAL_BACKOFF_FAILED(5),
        DIRECTORY_CREATION_FAILED(10),
        FILE_WRITE_FAILED_DISK_FULL(11),
        FILE_WRITE_FAILED(12),
        FILE_READ_FAILED(13),
        FILE_READ_RETURNED_INVALID_DATA(14),
        FILE_READ_RETURNED_MALFORMED_DATA(15);
        
        private final int zzn;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzn;
        }

        public static zzev zzb() {
            return zzcf.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzn + " name=" + name() + '>';
        }

        zza(int i) {
            this.zzn = i;
        }

        static {
            new zzcg();
        }
    }

    private zzy$zzbg() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo.zzb<zzy$zzbg, zzb> implements zzgb {
        private zzb() {
            super(zzy$zzbg.zzl);
        }

        /* synthetic */ zzb(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbg>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0079;
                case 2: goto L_0x0073;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbg> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zzm
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbg> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbg> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zzm     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbg r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zzl     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zzm = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbg r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zzl
            return r2
        L_0x0033:
            r2 = 9
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zza.zzb()
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 5
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zza.zzb()
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 7
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zza.zzb()
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0003\u0000\u0001ဃ\u0000\u0002\u001e\u0003\u001e\u0004\u001e\u0005င\u0001"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbg r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zzl
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0073:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbg$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbg$zzb
            r2.<init>(r3)
            return r2
        L_0x0079:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbg r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbg
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzbg.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.mlkit_language_id.zzcc, com.google.android.gms.internal.mlkit_language_id.zzex<java.lang.Integer, com.google.android.gms.internal.mlkit_language_id.zzy$zzbg$zza>] */
    /* JADX WARN: Type inference failed for: r0v1, types: [com.google.android.gms.internal.mlkit_language_id.zzce, com.google.android.gms.internal.mlkit_language_id.zzex<java.lang.Integer, com.google.android.gms.internal.mlkit_language_id.zzy$zzbg$zza>] */
    /* JADX WARN: Type inference failed for: r0v2, types: [com.google.android.gms.internal.mlkit_language_id.zzcd, com.google.android.gms.internal.mlkit_language_id.zzex<java.lang.Integer, com.google.android.gms.internal.mlkit_language_id.zzy$zzbg$zza>] */
    static {
        zzy$zzbg zzy_zzbg = new zzy$zzbg();
        zzl = zzy_zzbg;
        zzeo.zza(zzy$zzbg.class, zzy_zzbg);
    }
}
