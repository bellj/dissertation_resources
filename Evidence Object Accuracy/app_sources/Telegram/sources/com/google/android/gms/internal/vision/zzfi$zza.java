package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zza extends zzjb<zzfi$zza, zza> implements zzkm {
    private static final zzfi$zza zzf;
    private static volatile zzkx<zzfi$zza> zzg;
    private int zzc;
    private String zzd = "";
    private String zze = "";

    private zzfi$zza() {
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class zza extends zzjb.zzb<zzfi$zza, zza> implements zzkm {
        private zza() {
            super(zzfi$zza.zzf);
        }

        public final zza zza(String str) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zza) this.zza).zza(str);
            return this;
        }

        public final zza zzb(String str) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zza) this.zza).zzb(str);
            return this;
        }

        /* synthetic */ zza(zzfk zzfk) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    /* access modifiers changed from: private */
    public final void zzb(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    public static zza zza() {
        return zzf.zzj();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zza>, com.google.android.gms.internal.vision.zzjb$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzjb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.vision.zzfk.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0053;
                case 2: goto L_0x004d;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zza> r2 = com.google.android.gms.internal.vision.zzfi$zza.zzg
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zza> r3 = com.google.android.gms.internal.vision.zzfi$zza.class
            monitor-enter(r3)
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zza> r2 = com.google.android.gms.internal.vision.zzfi$zza.zzg     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.vision.zzjb$zza r2 = new com.google.android.gms.internal.vision.zzjb$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zza r4 = com.google.android.gms.internal.vision.zzfi$zza.zzf     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zza.zzg = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.vision.zzfi$zza r2 = com.google.android.gms.internal.vision.zzfi$zza.zzf
            return r2
        L_0x0033:
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001"
            com.google.android.gms.internal.vision.zzfi$zza r4 = com.google.android.gms.internal.vision.zzfi$zza.zzf
            java.lang.Object r2 = com.google.android.gms.internal.vision.zzjb.zza(r4, r3, r2)
            return r2
        L_0x004d:
            com.google.android.gms.internal.vision.zzfi$zza$zza r2 = new com.google.android.gms.internal.vision.zzfi$zza$zza
            r2.<init>(r3)
            return r2
        L_0x0053:
            com.google.android.gms.internal.vision.zzfi$zza r2 = new com.google.android.gms.internal.vision.zzfi$zza
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfi$zza.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzfi$zza zzfi_zza = new zzfi$zza();
        zzf = zzfi_zza;
        zzjb.zza(zzfi$zza.class, zzfi_zza);
    }
}
