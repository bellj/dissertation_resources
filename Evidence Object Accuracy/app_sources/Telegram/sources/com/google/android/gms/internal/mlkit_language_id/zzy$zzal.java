package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzal extends zzeo<zzy$zzal, zza> implements zzgb {
    private static final zzy$zzal zzl;
    private static volatile zzgj<zzy$zzal> zzm;
    private int zzc;
    private String zzd = "";
    private String zze = "";
    private int zzf;
    private String zzg = "";
    private String zzh = "";
    private int zzi;
    private long zzj;
    private boolean zzk;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zzb implements zzet {
        TYPE_UNKNOWN(0),
        CUSTOM(1),
        AUTOML_IMAGE_LABELING(2),
        BASE_TRANSLATE(3),
        CUSTOM_OBJECT_DETECTION(4),
        CUSTOM_IMAGE_LABELING(5),
        BASE_ENTITY_EXTRACTION(6);
        
        private final int zzi;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzi;
        }

        public static zzev zzb() {
            return zzbe.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        zzb(int i) {
            this.zzi = i;
        }

        static {
            new zzbd();
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zzc implements zzet {
        SOURCE_UNKNOWN(0),
        APP_ASSET(1),
        LOCAL(2),
        CLOUD(3),
        SDK_BUILT_IN(4),
        URI(5);
        
        private final int zzh;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzh;
        }

        public static zzev zzb() {
            return zzbf.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzh + " name=" + name() + '>';
        }

        zzc(int i) {
            this.zzh = i;
        }

        static {
            new zzbg();
        }
    }

    private zzy$zzal() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzal, zza> implements zzgb {
        private zza() {
            super(zzy$zzal.zzl);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzal>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0083;
                case 2: goto L_0x007d;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzal> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzm
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzal> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzal> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzm     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzal r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzl     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzm = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzal r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzl
            return r2
        L_0x0033:
            r2 = 11
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzc.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 8
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzb.zzb()
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 10
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဌ\u0002\u0004ဈ\u0003\u0005ဈ\u0004\u0006ဌ\u0005\u0007ဃ\u0006\bဇ\u0007"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzal r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zzl
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x007d:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzal$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzal$zza
            r2.<init>(r3)
            return r2
        L_0x0083:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzal r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzal
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzal.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzal zzy_zzal = new zzy$zzal();
        zzl = zzy_zzal;
        zzeo.zza(zzy$zzal.class, zzy_zzal);
    }
}
