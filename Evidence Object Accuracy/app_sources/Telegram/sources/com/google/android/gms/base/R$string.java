package com.google.android.gms.base;

/* loaded from: classes.dex */
public final class R$string {
    public static final int common_google_play_services_enable_button = 2131629307;
    public static final int common_google_play_services_enable_text = 2131629308;
    public static final int common_google_play_services_enable_title = 2131629309;
    public static final int common_google_play_services_install_button = 2131629310;
    public static final int common_google_play_services_install_text = 2131629311;
    public static final int common_google_play_services_install_title = 2131629312;
    public static final int common_google_play_services_notification_channel_name = 2131629313;
    public static final int common_google_play_services_notification_ticker = 2131629314;
    public static final int common_google_play_services_unsupported_text = 2131629316;
    public static final int common_google_play_services_update_button = 2131629317;
    public static final int common_google_play_services_update_text = 2131629318;
    public static final int common_google_play_services_update_title = 2131629319;
    public static final int common_google_play_services_updating_text = 2131629320;
    public static final int common_google_play_services_wear_update_text = 2131629321;
    public static final int common_open_on_phone = 2131629322;
}
