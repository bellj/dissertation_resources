package com.google.android.gms.internal.mlkit_common;

/* compiled from: com.google.mlkit:common@@17.0.0 */
/* loaded from: classes.dex */
final class zzaf<E> extends zzaa<E> {
    private final zzad<E> zza;

    /* access modifiers changed from: package-private */
    public zzaf(zzad<E> zzad, int i) {
        super(zzad.size(), i);
        this.zza = zzad;
    }

    @Override // com.google.android.gms.internal.mlkit_common.zzaa
    protected final E zza(int i) {
        return this.zza.get(i);
    }
}
