package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public class zzfe {
    private volatile zzfz zzc;
    private volatile zzdn zzd;

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfe)) {
            return false;
        }
        zzfe zzfe = (zzfe) obj;
        zzfz zzfz = this.zzc;
        zzfz zzfz2 = zzfe.zzc;
        if (zzfz == null && zzfz2 == null) {
            return zzc().equals(zzfe.zzc());
        }
        if (zzfz != null && zzfz2 != null) {
            return zzfz.equals(zzfz2);
        }
        if (zzfz != null) {
            return zzfz.equals(zzfe.zzb(zzfz.zzn()));
        }
        return zzb(zzfz2.zzn()).equals(zzfz2);
    }

    private final zzfz zzb(zzfz zzfz) {
        if (this.zzc == null) {
            synchronized (this) {
                if (this.zzc == null) {
                    try {
                        this.zzc = zzfz;
                        this.zzd = zzdn.zza;
                    } catch (zzez unused) {
                        this.zzc = zzfz;
                        this.zzd = zzdn.zza;
                    }
                }
            }
        }
        return this.zzc;
    }

    public final zzfz zza(zzfz zzfz) {
        zzfz zzfz2 = this.zzc;
        this.zzd = null;
        this.zzc = zzfz;
        return zzfz2;
    }

    public final int zzb() {
        if (this.zzd != null) {
            return this.zzd.zza();
        }
        if (this.zzc != null) {
            return this.zzc.zzj();
        }
        return 0;
    }

    public final zzdn zzc() {
        if (this.zzd != null) {
            return this.zzd;
        }
        synchronized (this) {
            if (this.zzd != null) {
                return this.zzd;
            }
            if (this.zzc == null) {
                this.zzd = zzdn.zza;
            } else {
                this.zzd = this.zzc.zze();
            }
            return this.zzd;
        }
    }

    static {
        zzef.zza();
    }
}
