package com.google.android.gms.internal.mlkit_language_id;

import java.io.IOException;
import java.util.List;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzgr {
    private static final Class<?> zza = zzd();
    private static final zzhh<?, ?> zzb = zza(false);
    private static final zzhh<?, ?> zzc = zza(true);
    private static final zzhh<?, ?> zzd = new zzhj();

    public static void zza(Class<?> cls) {
        Class<?> cls2;
        if (!zzeo.class.isAssignableFrom(cls) && (cls2 = zza) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void zza(int i, List<Double> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzg(i, list, z);
        }
    }

    public static void zzb(int i, List<Float> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzf(i, list, z);
        }
    }

    public static void zzc(int i, List<Long> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzc(i, list, z);
        }
    }

    public static void zzd(int i, List<Long> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzd(i, list, z);
        }
    }

    public static void zze(int i, List<Long> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzn(i, list, z);
        }
    }

    public static void zzf(int i, List<Long> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zze(i, list, z);
        }
    }

    public static void zzg(int i, List<Long> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzl(i, list, z);
        }
    }

    public static void zzh(int i, List<Integer> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zza(i, list, z);
        }
    }

    public static void zzi(int i, List<Integer> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzj(i, list, z);
        }
    }

    public static void zzj(int i, List<Integer> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzm(i, list, z);
        }
    }

    public static void zzk(int i, List<Integer> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzb(i, list, z);
        }
    }

    public static void zzl(int i, List<Integer> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzk(i, list, z);
        }
    }

    public static void zzm(int i, List<Integer> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzh(i, list, z);
        }
    }

    public static void zzn(int i, List<Boolean> list, zzib zzib, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzi(i, list, z);
        }
    }

    public static void zza(int i, List<String> list, zzib zzib) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zza(i, list);
        }
    }

    public static void zzb(int i, List<zzdn> list, zzib zzib) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzb(i, list);
        }
    }

    public static void zza(int i, List<?> list, zzib zzib, zzgp zzgp) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zza(i, list, zzgp);
        }
    }

    public static void zzb(int i, List<?> list, zzib zzib, zzgp zzgp) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzib.zzb(i, list, zzgp);
        }
    }

    /* access modifiers changed from: package-private */
    public static int zza(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfn) {
            zzfn zzfn = (zzfn) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zzd(zzfn.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zzd(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zza(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return zza(list) + (list.size() * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zzb(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfn) {
            zzfn zzfn = (zzfn) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zze(zzfn.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zze(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zzb(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzb(list) + (size * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zzc(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfn) {
            zzfn zzfn = (zzfn) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zzf(zzfn.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zzf(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zzc(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzc(list) + (size * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zzd(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzer) {
            zzer zzer = (zzer) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zzk(zzer.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zzk(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zzd(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzd(list) + (size * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zze(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzer) {
            zzer zzer = (zzer) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zzf(zzer.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zzf(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zze(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zze(list) + (size * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zzf(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzer) {
            zzer zzer = (zzer) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zzg(zzer.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zzg(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zzf(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzf(list) + (size * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zzg(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzer) {
            zzer zzer = (zzer) list;
            i = 0;
            while (i2 < size) {
                i += zzea.zzh(zzer.zza(i2));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += zzea.zzh(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public static int zzg(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzg(list) + (size * zzea.zze(i));
    }

    /* access modifiers changed from: package-private */
    public static int zzh(List<?> list) {
        return list.size() << 2;
    }

    /* access modifiers changed from: package-private */
    public static int zzh(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzea.zzi(i, 0);
    }

    /* access modifiers changed from: package-private */
    public static int zzi(List<?> list) {
        return list.size() << 3;
    }

    /* access modifiers changed from: package-private */
    public static int zzi(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzea.zzg(i, 0L);
    }

    /* access modifiers changed from: package-private */
    public static int zzj(List<?> list) {
        return list.size();
    }

    /* access modifiers changed from: package-private */
    public static int zzj(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzea.zzb(i, true);
    }

    /* access modifiers changed from: package-private */
    public static int zza(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int zze = zzea.zze(i) * size;
        if (list instanceof zzfg) {
            zzfg zzfg = (zzfg) list;
            while (i4 < size) {
                Object zza2 = zzfg.zza(i4);
                if (zza2 instanceof zzdn) {
                    i3 = zzea.zzb((zzdn) zza2);
                } else {
                    i3 = zzea.zzb((String) zza2);
                }
                zze += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof zzdn) {
                    i2 = zzea.zzb((zzdn) obj);
                } else {
                    i2 = zzea.zzb((String) obj);
                }
                zze += i2;
                i4++;
            }
        }
        return zze;
    }

    /* access modifiers changed from: package-private */
    public static int zza(int i, Object obj, zzgp zzgp) {
        if (obj instanceof zzfe) {
            return zzea.zza(i, (zzfe) obj);
        }
        return zzea.zzb(i, (zzfz) obj, zzgp);
    }

    /* access modifiers changed from: package-private */
    public static int zza(int i, List<?> list, zzgp zzgp) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zze = zzea.zze(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof zzfe) {
                i2 = zzea.zza((zzfe) obj);
            } else {
                i2 = zzea.zza((zzfz) obj, zzgp);
            }
            zze += i2;
        }
        return zze;
    }

    /* access modifiers changed from: package-private */
    public static int zzb(int i, List<zzdn> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zze = size * zzea.zze(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            zze += zzea.zzb(list.get(i2));
        }
        return zze;
    }

    /* access modifiers changed from: package-private */
    public static int zzb(int i, List<zzfz> list, zzgp zzgp) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzea.zzc(i, list.get(i3), zzgp);
        }
        return i2;
    }

    public static zzhh<?, ?> zza() {
        return zzb;
    }

    public static zzhh<?, ?> zzb() {
        return zzc;
    }

    public static zzhh<?, ?> zzc() {
        return zzd;
    }

    private static zzhh<?, ?> zza(boolean z) {
        try {
            Class<?> zze = zze();
            if (zze == null) {
                return null;
            }
            return (zzhh) zze.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zzd() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zze() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public static boolean zza(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public static <T> void zza(zzfs zzfs, T t, T t2, long j) {
        zzhn.zza(t, j, zzfs.zza(zzhn.zzf(t, j), zzhn.zzf(t2, j)));
    }

    /* access modifiers changed from: package-private */
    public static <T, FT extends zzel<FT>> void zza(zzee<FT> zzee, T t, T t2) {
        zzej<FT> zza2 = zzee.zza(t2);
        if (!zza2.zza.isEmpty()) {
            zzee.zzb(t).zza(zza2);
        }
    }

    /* access modifiers changed from: package-private */
    public static <T, UT, UB> void zza(zzhh<UT, UB> zzhh, T t, T t2) {
        zzhh.zza(t, zzhh.zzb(zzhh.zza(t), zzhh.zza(t2)));
    }
}
