package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zacg implements Runnable {
    private final /* synthetic */ zace zaa;

    /* access modifiers changed from: package-private */
    public zacg(zace zace) {
        this.zaa = zace;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.zaa.zah.zaa(new ConnectionResult(4));
    }
}
