package com.google.android.gms.internal.mlkit_language_id;

import java.util.Comparator;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzdp implements Comparator<zzdn> {
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(zzdn zzdn, zzdn zzdn2) {
        zzdn zzdn3 = zzdn;
        zzdn zzdn4 = zzdn2;
        zzds zzds = (zzds) zzdn3.iterator();
        zzds zzds2 = (zzds) zzdn4.iterator();
        while (zzds.hasNext() && zzds2.hasNext()) {
            int m = zzdp$$ExternalSyntheticBackport0.m(zzdn.zzb(zzds.zza()), zzdn.zzb(zzds2.zza()));
            if (m != 0) {
                return m;
            }
        }
        return zzdp$$ExternalSyntheticBackport0.m(zzdn3.zza(), zzdn4.zza());
    }
}
