package com.google.android.gms.internal.vision;

import java.io.IOException;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
final class zzlw extends zzlu<zzlx, zzlx> {
    private static void zza(Object obj, zzlx zzlx) {
        ((zzjb) obj).zzb = zzlx;
    }

    final void zzd(Object obj) {
        ((zzjb) obj).zzb.zzc();
    }

    final /* synthetic */ int zzf(Object obj) {
        return ((zzlx) obj).zze();
    }

    final /* synthetic */ int zze(Object obj) {
        return ((zzlx) obj).zzd();
    }

    final /* synthetic */ Object zzc(Object obj, Object obj2) {
        zzlx zzlx = (zzlx) obj;
        zzlx zzlx2 = (zzlx) obj2;
        if (zzlx2.equals(zzlx.zza())) {
            return zzlx;
        }
        return zzlx.zza(zzlx, zzlx2);
    }

    final /* synthetic */ void zzb(Object obj, zzmr zzmr) throws IOException {
        ((zzlx) obj).zza(zzmr);
    }

    final /* synthetic */ void zza(Object obj, zzmr zzmr) throws IOException {
        ((zzlx) obj).zzb(zzmr);
    }

    final /* synthetic */ void zzb(Object obj, Object obj2) {
        zza(obj, (zzlx) obj2);
    }

    final /* synthetic */ Object zzb(Object obj) {
        return ((zzjb) obj).zzb;
    }

    final /* bridge */ /* synthetic */ void zza(Object obj, Object obj2) {
        zza(obj, (zzlx) obj2);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzlu
    public final /* synthetic */ zzlx zza() {
        return zzlx.zzb();
    }

    final /* synthetic */ void zza(Object obj, int i, zzht zzht) {
        ((zzlx) obj).zza((i << 3) | 2, zzht);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zzlu
    public final /* synthetic */ void zza(Object obj, int i, long j) {
        ((zzlx) obj).zza(i << 3, Long.valueOf(j));
    }
}
