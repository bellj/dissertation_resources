package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/* compiled from: com.google.android.gms:play-services-auth@@19.2.0 */
/* loaded from: classes.dex */
public final class zbn {
    private static zbn zbd;
    final Storage zba;

    private zbn(Context context) {
        Storage instance = Storage.getInstance(context);
        this.zba = instance;
        instance.getSavedDefaultGoogleSignInAccount();
        instance.getSavedDefaultGoogleSignInOptions();
    }

    public static synchronized zbn zbc(Context context) {
        zbn zbf;
        synchronized (zbn.class) {
            zbf = zbf(context.getApplicationContext());
        }
        return zbf;
    }

    private static synchronized zbn zbf(Context context) {
        synchronized (zbn.class) {
            zbn zbn = zbd;
            if (zbn != null) {
                return zbn;
            }
            zbn zbn2 = new zbn(context);
            zbd = zbn2;
            return zbn2;
        }
    }

    public final synchronized void zbd() {
        this.zba.clear();
    }

    public final synchronized void zbe(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        this.zba.saveDefaultGoogleSignInAccount(googleSignInAccount, googleSignInOptions);
    }
}
