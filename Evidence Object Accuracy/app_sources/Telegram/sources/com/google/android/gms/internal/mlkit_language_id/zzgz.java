package com.google.android.gms.internal.mlkit_language_id;

import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* JADX WARN: Incorrect field signature: TK; */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzgz implements Comparable<zzgz>, Map.Entry<K, V> {
    private final Comparable zza;
    private V zzb;
    private final /* synthetic */ zzgq zzc;

    /* access modifiers changed from: package-private */
    public zzgz(zzgq zzgq, Map.Entry<K, V> entry) {
        this(zzgq, (Comparable) entry.getKey(), entry.getValue());
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: K */
    /* JADX WARN: Multi-variable type inference failed */
    /* access modifiers changed from: package-private */
    public zzgz(zzgq zzgq, K k, V v) {
        this.zzc = zzgq;
        this.zza = k;
        this.zzb = v;
    }

    @Override // java.util.Map.Entry
    public final V getValue() {
        return this.zzb;
    }

    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        this.zzc.zzf();
        V v2 = this.zzb;
        this.zzb = v;
        return v2;
    }

    @Override // java.lang.Object, java.util.Map.Entry
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return zza(this.zza, entry.getKey()) && zza(this.zzb, entry.getValue());
    }

    @Override // java.lang.Object, java.util.Map.Entry
    public final int hashCode() {
        Comparable comparable = this.zza;
        int i = 0;
        int hashCode = comparable == null ? 0 : comparable.hashCode();
        V v = this.zzb;
        if (v != 0) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    @Override // java.lang.Object
    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        String valueOf2 = String.valueOf(this.zzb);
        StringBuilder sb = new StringBuilder(valueOf.length() + 1 + valueOf2.length());
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        return sb.toString();
    }

    private static boolean zza(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @Override // java.util.Map.Entry
    public final /* synthetic */ Object getKey() {
        return this.zza;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(zzgz zzgz) {
        return ((Comparable) getKey()).compareTo((Comparable) zzgz.getKey());
    }
}
