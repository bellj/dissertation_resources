package com.google.android.gms.internal.mlkit_language_id;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzgc<T> implements zzgp<T> {
    private final zzfz zza;
    private final zzhh<?, ?> zzb;
    private final boolean zzc;
    private final zzee<?> zzd;

    private zzgc(zzhh<?, ?> zzhh, zzee<?> zzee, zzfz zzfz) {
        this.zzb = zzhh;
        this.zzc = zzee.zza(zzfz);
        this.zzd = zzee;
        this.zza = zzfz;
    }

    /* access modifiers changed from: package-private */
    public static <T> zzgc<T> zza(zzhh<?, ?> zzhh, zzee<?> zzee, zzfz zzfz) {
        return new zzgc<>(zzhh, zzee, zzfz);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final boolean zza(T t, T t2) {
        if (!this.zzb.zza(t).equals(this.zzb.zza(t2))) {
            return false;
        }
        if (this.zzc) {
            return this.zzd.zza(t).equals(this.zzd.zza(t2));
        }
        return true;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final int zza(T t) {
        int hashCode = this.zzb.zza(t).hashCode();
        return this.zzc ? (hashCode * 53) + this.zzd.zza(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final void zzb(T t, T t2) {
        zzgr.zza(this.zzb, t, t2);
        if (this.zzc) {
            zzgr.zza(this.zzd, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final void zza(T t, zzib zzib) throws IOException {
        Iterator<Map.Entry<?, Object>> zzd = this.zzd.zza(t).zzd();
        while (zzd.hasNext()) {
            Map.Entry<?, Object> next = zzd.next();
            zzel zzel = (zzel) next.getKey();
            if (zzel.zzc() != zzhy.MESSAGE || zzel.zzd() || zzel.zze()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzfc) {
                zzib.zza(zzel.zza(), (Object) ((zzfc) next).zza().zzc());
            } else {
                zzib.zza(zzel.zza(), next.getValue());
            }
        }
        zzhh<?, ?> zzhh = this.zzb;
        zzhh.zzb((zzhh<?, ?>) zzhh.zza(t), zzib);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final void zzb(T t) {
        this.zzb.zzb(t);
        this.zzd.zzc(t);
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final boolean zzc(T t) {
        return this.zzd.zza(t).zzf();
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzgp
    public final int zzd(T t) {
        zzhh<?, ?> zzhh = this.zzb;
        int zzc = zzhh.zzc(zzhh.zza(t)) + 0;
        return this.zzc ? zzc + this.zzd.zza(t).zzg() : zzc;
    }
}
