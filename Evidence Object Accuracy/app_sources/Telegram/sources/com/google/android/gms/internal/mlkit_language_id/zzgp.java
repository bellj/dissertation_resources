package com.google.android.gms.internal.mlkit_language_id;

import java.io.IOException;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface zzgp<T> {
    int zza(T t);

    void zza(T t, zzib zzib) throws IOException;

    boolean zza(T t, T t2);

    void zzb(T t);

    void zzb(T t, T t2);

    boolean zzc(T t);

    int zzd(T t);
}
