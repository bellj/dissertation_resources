package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface zzlf {
    <T> zzlc<T> zza(Class<T> cls);
}
