package com.google.android.gms.stats;

/* loaded from: classes.dex */
final class zzb implements Runnable {
    private final /* synthetic */ WakeLock zzp;

    /* access modifiers changed from: package-private */
    public zzb(WakeLock wakeLock) {
        this.zzp = wakeLock;
    }

    @Override // java.lang.Runnable
    public final void run() {
        WakeLock.zza(this.zzp, 0);
    }
}
