package com.google.android.gms.internal.play_billing;

/* compiled from: com.android.billingclient:billing@@5.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzs extends zzo {
    private final zzu zza;

    /* access modifiers changed from: package-private */
    public zzs(zzu zzu, int i) {
        super(zzu.size(), i);
        this.zza = zzu;
    }

    @Override // com.google.android.gms.internal.play_billing.zzo
    protected final Object zza(int i) {
        return this.zza.get(i);
    }
}
