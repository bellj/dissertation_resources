package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.zzcg;
import org.telegram.messenger.R;

/* loaded from: classes.dex */
public final class zzge$zzs extends zzcg<zzge$zzs, zza> implements zzdq {
    private static final zzge$zzs zzbfc;
    private static volatile zzdz<zzge$zzs> zzbg;
    private int zzbb;
    private int zzbfa = -1;
    private int zzbfb;

    /* loaded from: classes.dex */
    public static final class zza extends zzcg.zza<zzge$zzs, zza> implements zzdq {
        private zza() {
            super(zzge$zzs.zzbfc);
        }

        /* synthetic */ zza(zzgf zzgf) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public enum zzb implements zzcj {
        UNKNOWN_MOBILE_SUBTYPE(0),
        GPRS(1),
        EDGE(2),
        UMTS(3),
        CDMA(4),
        EVDO_0(5),
        EVDO_A(6),
        RTT(7),
        HSDPA(8),
        HSUPA(9),
        HSPA(10),
        IDEN(11),
        EVDO_B(12),
        LTE(13),
        EHRPD(14),
        HSPAP(15),
        GSM(16),
        TD_SCDMA(17),
        IWLAN(18),
        LTE_CA(19),
        COMBINED(100);
        
        private static final zzck<zzb> zzbq = new zzgo();
        private final int value;

        zzb(int i) {
            this.value = i;
        }

        public static zzb zzaz(int i) {
            if (i == 100) {
                return COMBINED;
            }
            switch (i) {
                case 0:
                    return UNKNOWN_MOBILE_SUBTYPE;
                case 1:
                    return GPRS;
                case 2:
                    return EDGE;
                case 3:
                    return UMTS;
                case 4:
                    return CDMA;
                case 5:
                    return EVDO_0;
                case 6:
                    return EVDO_A;
                case 7:
                    return RTT;
                case 8:
                    return HSDPA;
                case 9:
                    return HSUPA;
                case 10:
                    return HSPA;
                case 11:
                    return IDEN;
                case 12:
                    return EVDO_B;
                case 13:
                    return LTE;
                case 14:
                    return EHRPD;
                case 15:
                    return HSPAP;
                case 16:
                    return GSM;
                case 17:
                    return TD_SCDMA;
                case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                    return IWLAN;
                case R.styleable.MapAttrs_uiTiltGestures /* 19 */:
                    return LTE_CA;
                default:
                    return null;
            }
        }

        public static zzck<zzb> zzd() {
            return zzbq;
        }

        @Override // com.google.android.gms.internal.clearcut.zzcj
        public final int zzc() {
            return this.value;
        }
    }

    /* loaded from: classes.dex */
    public enum zzc implements zzcj {
        NONE(-1),
        MOBILE(0),
        WIFI(1),
        MOBILE_MMS(2),
        MOBILE_SUPL(3),
        MOBILE_DUN(4),
        MOBILE_HIPRI(5),
        WIMAX(6),
        BLUETOOTH(7),
        DUMMY(8),
        ETHERNET(9),
        MOBILE_FOTA(10),
        MOBILE_IMS(11),
        MOBILE_CBS(12),
        WIFI_P2P(13),
        MOBILE_IA(14),
        MOBILE_EMERGENCY(15),
        PROXY(16),
        VPN(17);
        
        private static final zzck<zzc> zzbq = new zzgp();
        private final int value;

        zzc(int i) {
            this.value = i;
        }

        public static zzc zzba(int i) {
            switch (i) {
                case -1:
                    return NONE;
                case 0:
                    return MOBILE;
                case 1:
                    return WIFI;
                case 2:
                    return MOBILE_MMS;
                case 3:
                    return MOBILE_SUPL;
                case 4:
                    return MOBILE_DUN;
                case 5:
                    return MOBILE_HIPRI;
                case 6:
                    return WIMAX;
                case 7:
                    return BLUETOOTH;
                case 8:
                    return DUMMY;
                case 9:
                    return ETHERNET;
                case 10:
                    return MOBILE_FOTA;
                case 11:
                    return MOBILE_IMS;
                case 12:
                    return MOBILE_CBS;
                case 13:
                    return WIFI_P2P;
                case 14:
                    return MOBILE_IA;
                case 15:
                    return MOBILE_EMERGENCY;
                case 16:
                    return PROXY;
                case 17:
                    return VPN;
                default:
                    return null;
            }
        }

        public static zzck<zzc> zzd() {
            return zzbq;
        }

        @Override // com.google.android.gms.internal.clearcut.zzcj
        public final int zzc() {
            return this.value;
        }
    }

    static {
        zzge$zzs zzge_zzs = new zzge$zzs();
        zzbfc = zzge_zzs;
        zzcg.zza(zzge$zzs.class, zzge_zzs);
    }

    private zzge$zzs() {
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.clearcut.zzdz<com.google.android.gms.internal.clearcut.zzge$zzs>, com.google.android.gms.internal.clearcut.zzcg$zzb] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.clearcut.zzcg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.clearcut.zzgf.zzba
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0061;
                case 2: goto L_0x005b;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.clearcut.zzdz<com.google.android.gms.internal.clearcut.zzge$zzs> r2 = com.google.android.gms.internal.clearcut.zzge$zzs.zzbg
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.clearcut.zzge$zzs> r3 = com.google.android.gms.internal.clearcut.zzge$zzs.class
            monitor-enter(r3)
            com.google.android.gms.internal.clearcut.zzdz<com.google.android.gms.internal.clearcut.zzge$zzs> r2 = com.google.android.gms.internal.clearcut.zzge$zzs.zzbg     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.clearcut.zzcg$zzb r2 = new com.google.android.gms.internal.clearcut.zzcg$zzb     // Catch: all -> 0x002c
            com.google.android.gms.internal.clearcut.zzge$zzs r4 = com.google.android.gms.internal.clearcut.zzge$zzs.zzbfc     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.clearcut.zzge$zzs.zzbg = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.clearcut.zzge$zzs r2 = com.google.android.gms.internal.clearcut.zzge$zzs.zzbfc
            return r2
        L_0x0033:
            r2 = 5
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzbb"
            r2[r3] = r0
            java.lang.String r3 = "zzbfa"
            r2[r4] = r3
            r3 = 2
            com.google.android.gms.internal.clearcut.zzck r4 = com.google.android.gms.internal.clearcut.zzge$zzs.zzc.zzd()
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzbfb"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.clearcut.zzck r4 = com.google.android.gms.internal.clearcut.zzge$zzs.zzb.zzd()
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\f\u0001"
            com.google.android.gms.internal.clearcut.zzge$zzs r4 = com.google.android.gms.internal.clearcut.zzge$zzs.zzbfc
            java.lang.Object r2 = com.google.android.gms.internal.clearcut.zzcg.zza(r4, r3, r2)
            return r2
        L_0x005b:
            com.google.android.gms.internal.clearcut.zzge$zzs$zza r2 = new com.google.android.gms.internal.clearcut.zzge$zzs$zza
            r2.<init>(r3)
            return r2
        L_0x0061:
            com.google.android.gms.internal.clearcut.zzge$zzs r2 = new com.google.android.gms.internal.clearcut.zzge$zzs
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.zzge$zzs.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
