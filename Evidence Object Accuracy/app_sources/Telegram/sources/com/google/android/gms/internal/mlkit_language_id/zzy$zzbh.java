package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import org.telegram.tgnet.ConnectionsManager;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzbh extends zzeo<zzy$zzbh, zza> implements zzgb {
    private static final zzy$zzbh zzo;
    private static volatile zzgj<zzy$zzbh> zzp;
    private int zzc;
    private String zzd = "";
    private String zze = "";
    private String zzf = "";
    private String zzg = "";
    private String zzh = "";
    private String zzi = "";
    private String zzj = "";
    private zzew<String> zzk = zzeo.zzl();
    private String zzl = "";
    private boolean zzm;
    private boolean zzn;

    private zzy$zzbh() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzbh, zza> implements zzgb {
        private zza() {
            super(zzy$zzbh.zzo);
        }

        public final zza zza(String str) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zza(str);
            return this;
        }

        public final zza zzb(String str) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zzb(str);
            return this;
        }

        public final zza zzc(String str) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zzc(str);
            return this;
        }

        public final zza zzd(String str) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zzd(str);
            return this;
        }

        public final zza zza(Iterable<String> iterable) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zza(iterable);
            return this;
        }

        public final zza zze(String str) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zze(str);
            return this;
        }

        public final zza zza(boolean z) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zza(true);
            return this;
        }

        public final zza zzb(boolean z) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzbh) this.zza).zzb(true);
            return this;
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    /* access modifiers changed from: private */
    public final void zzb(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    /* access modifiers changed from: private */
    public final void zzc(String str) {
        str.getClass();
        this.zzc |= 8;
        this.zzg = str;
    }

    public final String zza() {
        return this.zzh;
    }

    /* access modifiers changed from: private */
    public final void zzd(String str) {
        str.getClass();
        this.zzc |= 16;
        this.zzh = str;
    }

    /* access modifiers changed from: private */
    public final void zza(Iterable<String> iterable) {
        zzew<String> zzew = this.zzk;
        if (!zzew.zza()) {
            this.zzk = zzeo.zza(zzew);
        }
        zzde.zza(iterable, this.zzk);
    }

    /* access modifiers changed from: private */
    public final void zze(String str) {
        str.getClass();
        this.zzc |= ConnectionsManager.RequestFlagNeedQuickAck;
        this.zzl = str;
    }

    /* access modifiers changed from: private */
    public final void zza(boolean z) {
        this.zzc |= 256;
        this.zzm = true;
    }

    /* access modifiers changed from: private */
    public final void zzb(boolean z) {
        this.zzc |= 512;
        this.zzn = true;
    }

    public static zza zzb() {
        return zzo.zzh();
    }

    public static zza zza(zzy$zzbh zzy_zzbh) {
        return zzo.zza(zzy_zzbh);
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbh>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0085;
                case 2: goto L_0x007f;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbh> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zzp
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbh> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbh> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zzp     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbh r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zzo     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zzp = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbh r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zzo
            return r2
        L_0x0033:
            r2 = 12
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzl"
            r2[r3] = r4
            r3 = 10
            java.lang.String r4 = "zzm"
            r2[r3] = r4
            r3 = 11
            java.lang.String r4 = "zzn"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u000b\u0000\u0001\u0001\u000b\u000b\u0000\u0001\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004ဈ\u0003\u0005ဈ\u0004\u0006ဈ\u0005\u0007ဈ\u0006\b\u001a\tဈ\u0007\nဇ\b\u000bဇ\t"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbh r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zzo
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x007f:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbh$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbh$zza
            r2.<init>(r3)
            return r2
        L_0x0085:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbh r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbh
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzbh.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public static zzy$zzbh zzc() {
        return zzo;
    }

    static {
        zzy$zzbh zzy_zzbh = new zzy$zzbh();
        zzo = zzy_zzbh;
        zzeo.zza(zzy$zzbh.class, zzy_zzbh);
    }
}
