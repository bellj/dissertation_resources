package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzak extends zzeo<zzy$zzak, zzb> implements zzgb {
    private static final zzy$zzak zzk;
    private static volatile zzgj<zzy$zzak> zzl;
    private int zzc;
    private zzy$zzam zzd;
    private long zze;
    private int zzf;
    private long zzg;
    private int zzh;
    private long zzi;
    private zzeu zzj = zzeo.zzk();

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zza implements zzet {
        UNKNOWN_STATUS(0),
        EXPLICITLY_REQUESTED(1),
        IMPLICITLY_REQUESTED(2),
        MODEL_INFO_RETRIEVAL_SUCCEEDED(3),
        MODEL_INFO_RETRIEVAL_FAILED(4),
        SCHEDULED(5),
        DOWNLOADING(6),
        SUCCEEDED(7),
        FAILED(8),
        LIVE(9),
        UPDATE_AVAILABLE(10),
        DOWNLOADED(11),
        STARTED(12);
        
        private final int zzo;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzo;
        }

        public static zzev zzb() {
            return zzbb.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzo + " name=" + name() + '>';
        }

        zza(int i) {
            this.zzo = i;
        }

        static {
            new zzbc();
        }
    }

    private zzy$zzak() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo.zzb<zzy$zzak, zzb> implements zzgb {
        private zzb() {
            super(zzy$zzak.zzk);
        }

        /* synthetic */ zzb(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzak>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x007d;
                case 2: goto L_0x0077;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzak> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zzl
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzak> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzak> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zzl     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzak r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zzk     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zzl = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzak r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zzk
            return r2
        L_0x0033:
            r2 = 10
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzai.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 7
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zza.zzb()
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0001\u0000\u0001ဉ\u0000\u0002ဃ\u0001\u0003ဌ\u0002\u0004ဃ\u0003\u0005ဌ\u0004\u0006ဂ\u0005\u0007\u0016"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzak r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zzk
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0077:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzak$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzak$zzb
            r2.<init>(r3)
            return r2
        L_0x007d:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzak r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzak
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzak.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzak zzy_zzak = new zzy$zzak();
        zzk = zzy_zzak;
        zzeo.zza(zzy$zzak.class, zzy_zzak);
    }
}
