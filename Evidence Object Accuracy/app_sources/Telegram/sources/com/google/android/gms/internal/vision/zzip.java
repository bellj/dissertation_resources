package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.telegram.messenger.R;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
final class zzip extends zziq<zzjb.zzf> {
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final boolean zza(zzkk zzkk) {
        return zzkk instanceof zzjb.zzc;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final zziu<zzjb.zzf> zza(Object obj) {
        return ((zzjb.zzc) obj).zzc;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final zziu<zzjb.zzf> zzb(Object obj) {
        return ((zzjb.zzc) obj).zza();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final void zzc(Object obj) {
        zza(obj).zzb();
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final int zza(Map.Entry<?, ?> entry) {
        return ((zzjb.zzf) entry.getKey()).zzb;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final void zza(zzmr zzmr, Map.Entry<?, ?> entry) throws IOException {
        zzjb.zzf zzf = (zzjb.zzf) entry.getKey();
        if (zzf.zzd) {
            switch (zzis.zza[zzf.zzc.ordinal()]) {
                case 1:
                    zzle.zza(zzf.zzb, (List<Double>) ((List) entry.getValue()), zzmr, false);
                    return;
                case 2:
                    zzle.zzb(zzf.zzb, (List<Float>) ((List) entry.getValue()), zzmr, false);
                    return;
                case 3:
                    zzle.zzc(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 4:
                    zzle.zzd(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 5:
                    zzle.zzh(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 6:
                    zzle.zzf(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 7:
                    zzle.zzk(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 8:
                    zzle.zzn(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 9:
                    zzle.zzi(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 10:
                    zzle.zzl(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 11:
                    zzle.zzg(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 12:
                    zzle.zzj(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 13:
                    zzle.zze(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 14:
                    zzle.zzh(zzf.zzb, (List) entry.getValue(), zzmr, false);
                    return;
                case 15:
                    zzle.zzb(zzf.zzb, (List) entry.getValue(), zzmr);
                    return;
                case 16:
                    zzle.zza(zzf.zzb, (List) entry.getValue(), zzmr);
                    return;
                case 17:
                    List list = (List) entry.getValue();
                    if (list != null && !list.isEmpty()) {
                        zzle.zzb(zzf.zzb, (List) entry.getValue(), zzmr, zzky.zza().zza((Class) list.get(0).getClass()));
                        return;
                    }
                    return;
                case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                    List list2 = (List) entry.getValue();
                    if (list2 != null && !list2.isEmpty()) {
                        zzle.zza(zzf.zzb, (List) entry.getValue(), zzmr, zzky.zza().zza((Class) list2.get(0).getClass()));
                        return;
                    }
                    return;
                default:
                    return;
            }
        } else {
            switch (zzis.zza[zzf.zzc.ordinal()]) {
                case 1:
                    zzmr.zza(zzf.zzb, ((Double) entry.getValue()).doubleValue());
                    return;
                case 2:
                    zzmr.zza(zzf.zzb, ((Float) entry.getValue()).floatValue());
                    return;
                case 3:
                    zzmr.zza(zzf.zzb, ((Long) entry.getValue()).longValue());
                    return;
                case 4:
                    zzmr.zzc(zzf.zzb, ((Long) entry.getValue()).longValue());
                    return;
                case 5:
                    zzmr.zzc(zzf.zzb, ((Integer) entry.getValue()).intValue());
                    return;
                case 6:
                    zzmr.zzd(zzf.zzb, ((Long) entry.getValue()).longValue());
                    return;
                case 7:
                    zzmr.zzd(zzf.zzb, ((Integer) entry.getValue()).intValue());
                    return;
                case 8:
                    zzmr.zza(zzf.zzb, ((Boolean) entry.getValue()).booleanValue());
                    return;
                case 9:
                    zzmr.zze(zzf.zzb, ((Integer) entry.getValue()).intValue());
                    return;
                case 10:
                    zzmr.zza(zzf.zzb, ((Integer) entry.getValue()).intValue());
                    return;
                case 11:
                    zzmr.zzb(zzf.zzb, ((Long) entry.getValue()).longValue());
                    return;
                case 12:
                    zzmr.zzf(zzf.zzb, ((Integer) entry.getValue()).intValue());
                    return;
                case 13:
                    zzmr.zze(zzf.zzb, ((Long) entry.getValue()).longValue());
                    return;
                case 14:
                    zzmr.zzc(zzf.zzb, ((Integer) entry.getValue()).intValue());
                    return;
                case 15:
                    zzmr.zza(zzf.zzb, (zzht) entry.getValue());
                    return;
                case 16:
                    zzmr.zza(zzf.zzb, (String) entry.getValue());
                    return;
                case 17:
                    zzmr.zzb(zzf.zzb, entry.getValue(), zzky.zza().zza((Class) entry.getValue().getClass()));
                    return;
                case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                    zzmr.zza(zzf.zzb, entry.getValue(), zzky.zza().zza((Class) entry.getValue().getClass()));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.vision.zziq
    public final Object zza(zzio zzio, zzkk zzkk, int i) {
        return zzio.zza(zzkk, i);
    }
}
