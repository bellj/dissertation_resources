package com.google.android.gms.internal.clearcut;

/* loaded from: classes.dex */
public final class zzew extends RuntimeException {
    public zzew(zzdo zzdo) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
