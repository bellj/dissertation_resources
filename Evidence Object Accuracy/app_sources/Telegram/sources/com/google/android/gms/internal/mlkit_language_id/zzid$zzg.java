package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzid$zzg extends zzeo<zzid$zzg, zza> implements zzgb {
    private static final zzid$zzg zzd;
    private static volatile zzgj<zzid$zzg> zze;
    private zzeu zzc = zzeo.zzk();

    private zzid$zzg() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzid$zzg, zza> implements zzgb {
        private zza() {
            super(zzid$zzg.zzd);
        }

        /* synthetic */ zza(zzic zzic) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r1v13, types: [com.google.android.gms.internal.mlkit_language_id.zzeo$zza, com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzg>] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r1, java.lang.Object r2, java.lang.Object r3) {
        /*
            r0 = this;
            int[] r2 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
            r3 = 1
            int r1 = r1 - r3
            r1 = r2[r1]
            r2 = 0
            switch(r1) {
                case 1: goto L_0x0049;
                case 2: goto L_0x0043;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            r1.<init>()
            throw r1
        L_0x0010:
            return r2
        L_0x0011:
            java.lang.Byte r1 = java.lang.Byte.valueOf(r3)
            return r1
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzg> r1 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zze
            if (r1 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzg> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.class
            monitor-enter(r2)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzg> r1 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zze     // Catch: all -> 0x002c
            if (r1 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r1 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzid$zzg r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zzd     // Catch: all -> 0x002c
            r1.<init>(r3)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zze = r1     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r2)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r1 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x002c
            throw r1
        L_0x002f:
            return r1
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzg r1 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zzd
            return r1
        L_0x0033:
            java.lang.Object[] r1 = new java.lang.Object[r3]
            r2 = 0
            java.lang.String r3 = "zzc"
            r1[r2] = r3
            java.lang.String r2 = "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u0016"
            com.google.android.gms.internal.mlkit_language_id.zzid$zzg r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zzd
            java.lang.Object r1 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r3, r2, r1)
            return r1
        L_0x0043:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzg$zza r1 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzg$zza
            r1.<init>(r2)
            return r1
        L_0x0049:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzg r1 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzg
            r1.<init>()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzg.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzid$zzg zzid_zzg = new zzid$zzg();
        zzd = zzid_zzg;
        zzeo.zza(zzid$zzg.class, zzid_zzg);
    }
}
