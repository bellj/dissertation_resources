package com.google.android.gms.internal.icing;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.Arrays;

/* compiled from: com.google.firebase:firebase-appindexing@@20.0.0 */
/* loaded from: classes.dex */
public final class zzg extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzg> CREATOR = new zzh();
    final zzk[] zza;
    public final String zzb;
    public final boolean zzc;
    public final Account zzd;

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj instanceof zzg) {
            zzg zzg = (zzg) obj;
            if (Objects.equal(this.zzb, zzg.zzb) && Objects.equal(Boolean.valueOf(this.zzc), Boolean.valueOf(zzg.zzc)) && Objects.equal(this.zzd, zzg.zzd) && Arrays.equals(this.zza, zzg.zza)) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return Objects.hashCode(this.zzb, Boolean.valueOf(this.zzc), this.zzd, Integer.valueOf(Arrays.hashCode(this.zza)));
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedArray(parcel, 1, this.zza, i, false);
        SafeParcelWriter.writeString(parcel, 2, this.zzb, false);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzc);
        SafeParcelWriter.writeParcelable(parcel, 4, this.zzd, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    /* access modifiers changed from: package-private */
    public zzg(zzk[] zzkArr, String str, boolean z, Account account) {
        this.zza = zzkArr;
        this.zzb = str;
        this.zzc = z;
        this.zzd = account;
    }
}
