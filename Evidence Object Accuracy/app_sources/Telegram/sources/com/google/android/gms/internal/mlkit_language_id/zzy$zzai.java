package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzai extends zzeo<zzy$zzai, zza> implements zzgb {
    private static final zzy$zzai zzg;
    private static volatile zzgj<zzy$zzai> zzh;
    private int zzc;
    private float zzd;
    private float zze;
    private float zzf;

    private zzy$zzai() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzai, zza> implements zzgb {
        private zza() {
            super(zzy$zzai.zzg);
        }

        public final zza zza(float f) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzai) this.zza).zza(f);
            return this;
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(float f) {
        this.zzc |= 4;
        this.zzf = f;
    }

    public static zza zza() {
        return zzg.zzh();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzai>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0058;
                case 2: goto L_0x0052;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzai> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zzh
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzai> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzai.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzai> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zzh     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzai r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zzg     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zzh = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzai r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zzg
            return r2
        L_0x0033:
            r2 = 4
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ခ\u0000\u0002ခ\u0001\u0003ခ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzai r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zzg
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0052:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzai$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzai$zza
            r2.<init>(r3)
            return r2
        L_0x0058:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzai r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzai
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzai.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public static zzy$zzai zzb() {
        return zzg;
    }

    static {
        zzy$zzai zzy_zzai = new zzy$zzai();
        zzg = zzy_zzai;
        zzeo.zza(zzy$zzai.class, zzy_zzai);
    }
}
