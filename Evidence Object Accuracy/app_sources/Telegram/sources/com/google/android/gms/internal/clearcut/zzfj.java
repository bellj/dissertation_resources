package com.google.android.gms.internal.clearcut;

import java.nio.ByteBuffer;
import org.telegram.tgnet.ConnectionsManager;

/* loaded from: classes.dex */
final class zzfj extends zzfg {
    private static int zza(byte[] bArr, int i, long j, int i2) {
        if (i2 == 0) {
            return zzff.zzam(i);
        }
        if (i2 == 1) {
            return zzff.zzp(i, zzfd.zza(bArr, j));
        }
        if (i2 == 2) {
            return zzff.zzd(i, zzfd.zza(bArr, j), zzfd.zza(bArr, j + 1));
        }
        throw new AssertionError();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0061, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b6, code lost:
        return -1;
     */
    @Override // com.google.android.gms.internal.clearcut.zzfg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    final int zzb(int r16, byte[] r17, int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 219
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.zzfj.zzb(int, byte[], int, int):int");
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.clearcut.zzfg
    public final int zzb(CharSequence charSequence, byte[] bArr, int i, int i2) {
        char c;
        long j;
        int i3;
        char charAt;
        long j2 = (long) i;
        long j3 = ((long) i2) + j2;
        int length = charSequence.length();
        if (length > i2 || bArr.length - i2 < i) {
            char charAt2 = charSequence.charAt(length - 1);
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt2);
            sb.append(" at index ");
            sb.append(i + i2);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i4 = 0;
        while (true) {
            c = 128;
            j = 1;
            if (i4 >= length || (charAt = charSequence.charAt(i4)) >= 128) {
                break;
            }
            j2 = 1 + j2;
            zzfd.zza(bArr, j2, (byte) charAt);
            i4++;
        }
        if (i4 == length) {
            return (int) j2;
        }
        while (i4 < length) {
            char charAt3 = charSequence.charAt(i4);
            if (charAt3 >= c || j2 >= j3) {
                if (charAt3 < 2048 && j2 <= j3 - 2) {
                    long j4 = j2 + j;
                    zzfd.zza(bArr, j2, (byte) ((charAt3 >>> 6) | 960));
                    j2 = j4 + j;
                    zzfd.zza(bArr, j4, (byte) ((charAt3 & '?') | ConnectionsManager.RequestFlagNeedQuickAck));
                    j = j;
                } else if ((charAt3 < 55296 || 57343 < charAt3) && j2 <= j3 - 3) {
                    long j5 = j2 + j;
                    zzfd.zza(bArr, j2, (byte) ((charAt3 >>> '\f') | 480));
                    long j6 = j5 + j;
                    zzfd.zza(bArr, j5, (byte) (((charAt3 >>> 6) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                    j2 = j6 + 1;
                    zzfd.zza(bArr, j6, (byte) ((charAt3 & '?') | ConnectionsManager.RequestFlagNeedQuickAck));
                    j = 1;
                } else if (j2 <= j3 - 4) {
                    int i5 = i4 + 1;
                    if (i5 != length) {
                        char charAt4 = charSequence.charAt(i5);
                        if (Character.isSurrogatePair(charAt3, charAt4)) {
                            int codePoint = Character.toCodePoint(charAt3, charAt4);
                            long j7 = j2 + 1;
                            zzfd.zza(bArr, j2, (byte) ((codePoint >>> 18) | 240));
                            long j8 = j7 + 1;
                            zzfd.zza(bArr, j7, (byte) (((codePoint >>> 12) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            long j9 = j8 + 1;
                            zzfd.zza(bArr, j8, (byte) (((codePoint >>> 6) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            j = 1;
                            j2 = j9 + 1;
                            zzfd.zza(bArr, j9, (byte) ((codePoint & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            i4 = i5;
                        } else {
                            i4 = i5;
                        }
                    }
                    throw new zzfi(i4 - 1, length);
                } else if (55296 > charAt3 || charAt3 > 57343 || ((i3 = i4 + 1) != length && Character.isSurrogatePair(charAt3, charSequence.charAt(i3)))) {
                    StringBuilder sb2 = new StringBuilder(46);
                    sb2.append("Failed writing ");
                    sb2.append(charAt3);
                    sb2.append(" at index ");
                    sb2.append(j2);
                    throw new ArrayIndexOutOfBoundsException(sb2.toString());
                } else {
                    throw new zzfi(i4, length);
                }
                i4++;
                c = 128;
            } else {
                j2 += j;
                zzfd.zza(bArr, j2, (byte) charAt3);
                j = j;
            }
            i4++;
            c = 128;
        }
        return (int) j2;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.clearcut.zzfg
    public final void zzb(CharSequence charSequence, ByteBuffer byteBuffer) {
        char c;
        int i;
        int i2;
        char charAt;
        ByteBuffer byteBuffer2 = byteBuffer;
        long zzb = zzfd.zzb(byteBuffer);
        long position = ((long) byteBuffer.position()) + zzb;
        long limit = ((long) byteBuffer.limit()) + zzb;
        int length = charSequence.length();
        if (((long) length) <= limit - position) {
            int i3 = 0;
            while (true) {
                c = 128;
                if (i3 >= length || (charAt = charSequence.charAt(i3)) >= 128) {
                    break;
                }
                position = 1 + position;
                zzfd.zza(position, (byte) charAt);
                i3++;
            }
            if (i3 == length) {
                i = (int) (position - zzb);
            } else {
                while (i3 < length) {
                    char charAt2 = charSequence.charAt(i3);
                    if (charAt2 < c && position < limit) {
                        position++;
                        zzfd.zza(position, (byte) charAt2);
                        zzb = zzb;
                    } else if (charAt2 >= 2048 || position > limit - 2) {
                        zzb = zzb;
                        if ((charAt2 < 55296 || 57343 < charAt2) && position <= limit - 3) {
                            long j = position + 1;
                            zzfd.zza(position, (byte) ((charAt2 >>> '\f') | 480));
                            long j2 = j + 1;
                            zzfd.zza(j, (byte) (((charAt2 >>> 6) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                            position = j2 + 1;
                            zzfd.zza(j2, (byte) ((charAt2 & '?') | ConnectionsManager.RequestFlagNeedQuickAck));
                        } else if (position <= limit - 4) {
                            int i4 = i3 + 1;
                            if (i4 != length) {
                                char charAt3 = charSequence.charAt(i4);
                                if (Character.isSurrogatePair(charAt2, charAt3)) {
                                    int codePoint = Character.toCodePoint(charAt2, charAt3);
                                    long j3 = position + 1;
                                    zzfd.zza(position, (byte) ((codePoint >>> 18) | 240));
                                    long j4 = j3 + 1;
                                    zzfd.zza(j3, (byte) (((codePoint >>> 12) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                                    long j5 = j4 + 1;
                                    zzfd.zza(j4, (byte) (((codePoint >>> 6) & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                                    position = j5 + 1;
                                    zzfd.zza(j5, (byte) ((codePoint & 63) | ConnectionsManager.RequestFlagNeedQuickAck));
                                    i3 = i4;
                                } else {
                                    i3 = i4;
                                }
                            }
                            throw new zzfi(i3 - 1, length);
                        } else if (55296 > charAt2 || charAt2 > 57343 || ((i2 = i3 + 1) != length && Character.isSurrogatePair(charAt2, charSequence.charAt(i2)))) {
                            StringBuilder sb = new StringBuilder(46);
                            sb.append("Failed writing ");
                            sb.append(charAt2);
                            sb.append(" at index ");
                            sb.append(position);
                            throw new ArrayIndexOutOfBoundsException(sb.toString());
                        } else {
                            throw new zzfi(i3, length);
                        }
                    } else {
                        zzb = zzb;
                        long j6 = position + 1;
                        zzfd.zza(position, (byte) ((charAt2 >>> 6) | 960));
                        position = j6 + 1;
                        zzfd.zza(j6, (byte) ((charAt2 & '?') | ConnectionsManager.RequestFlagNeedQuickAck));
                    }
                    i3++;
                    c = 128;
                }
                i = (int) (position - zzb);
                byteBuffer2 = byteBuffer;
            }
            byteBuffer2.position(i);
            return;
        }
        char charAt4 = charSequence.charAt(length - 1);
        int limit2 = byteBuffer.limit();
        StringBuilder sb2 = new StringBuilder(37);
        sb2.append("Failed writing ");
        sb2.append(charAt4);
        sb2.append(" at index ");
        sb2.append(limit2);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }
}
