package com.google.android.gms.internal.mlkit_language_id;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzgv extends zzhb {
    private final /* synthetic */ zzgq zza;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    private zzgv(zzgq zzgq) {
        super(zzgq, null);
        this.zza = zzgq;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzhb, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zzgs(this.zza, null);
    }

    /* access modifiers changed from: package-private */
    public /* synthetic */ zzgv(zzgq zzgq, zzgt zzgt) {
        this(zzgq);
    }
}
