package com.google.android.gms.internal.mlkit_language_id;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zze {
    @NullableDecl
    String zza;
    @NullableDecl
    Object zzb;
    @NullableDecl
    zze zzc;

    private zze() {
    }
}
