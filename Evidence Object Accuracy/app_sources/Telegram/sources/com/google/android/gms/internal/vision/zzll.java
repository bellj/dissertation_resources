package com.google.android.gms.internal.vision;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzll {
    private static final Iterator<Object> zza = new zzlk();
    private static final Iterable<Object> zzb = new zzln();

    /* access modifiers changed from: package-private */
    public static <T> Iterable<T> zza() {
        return (Iterable<T>) zzb;
    }
}
