package com.google.android.gms.internal.clearcut;

import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzem {
    private static final Iterator<Object> zzot = new zzen();
    private static final Iterable<Object> zzou = new zzeo();

    /* access modifiers changed from: package-private */
    public static <T> Iterable<T> zzdx() {
        return (Iterable<T>) zzou;
    }
}
