package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import com.google.android.gms.internal.mlkit_language_id.zzy$zzau;
import com.google.android.gms.internal.mlkit_language_id.zzy$zzbh;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzad extends zzeo.zzc<zzy$zzad, zza> {
    private static final zzy$zzad zzbd;
    private static volatile zzgj<zzy$zzad> zzbe;
    private zzy$zzp zzaa;
    private zzy$zzm zzab;
    private zzy$zzo zzac;
    private zzy$zzr zzad;
    private zzy$zzq zzae;
    private zzy$zzs zzaf;
    private zzy$zzt zzag;
    private zzy$zzu zzah;
    private zzy$zzv zzai;
    private zzy$zzw zzaj;
    private zzy$zzj zzak;
    private zzy$zzl zzal;
    private zzy$zzk zzam;
    private zzy$zzah zzan;
    private zzy$zzaa zzao;
    private zzy$zza zzap;
    private zzy$zzb zzaq;
    private zzy$zzd zzar;
    private zzy$zzc zzas;
    private zzy$zze zzat;
    private zzy$zzf zzau;
    private zzy$zzi zzav;
    private zzy$zzg zzaw;
    private zzy$zzh zzax;
    private zzew<zzid$zzf> zzay = zzeo.zzl();
    private zzy$zzbg zzaz;
    private zzy$zzag zzba;
    private zzy$zzaj zzbb;
    private byte zzbc = 2;
    private int zzd;
    private int zze;
    private zzy$zzbh zzf;
    private int zzg;
    private boolean zzh;
    private zzy$zzak zzi;
    private zzy$zzz zzj;
    private zzy$zzy zzk;
    private zzy$zzx zzl;
    private zzy$zzap zzm;
    private zzy$zzbd zzn;
    private zzy$zzao zzo;
    private zzy$zzaq zzp;
    private zzy$zzas zzq;
    private zzy$zzar zzr;
    private zzy$zzav zzs;
    private zzy$zzay zzt;
    private zzy$zzax zzu;
    private zzy$zzaz zzv;
    private zzy$zzbb zzw;
    private zzy$zzbc zzx;
    private zzy$zzau zzy;
    private zzy$zzbe zzz;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzd<zzy$zzad, zza> {
        private zza() {
            super(zzy$zzad.zzbd);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }

        public final zzy$zzbh zza() {
            return ((zzy$zzad) this.zza).zza();
        }

        public final zza zza(zzy$zzbh.zza zza) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzad) this.zza).zza((zzy$zzbh) ((zzeo) zza.zzg()));
            return this;
        }

        public final zza zza(zzaj zzaj) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzad) this.zza).zza(zzaj);
            return this;
        }

        public final zza zza(boolean z) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzad) this.zza).zza(true);
            return this;
        }

        public final zza zza(zzy$zzau.zza zza) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzad) this.zza).zza((zzy$zzau) ((zzeo) zza.zzg()));
            return this;
        }
    }

    static {
        zzy$zzad zzy_zzad = new zzy$zzad();
        zzbd = zzy_zzad;
        zzeo.zza(zzy$zzad.class, zzy_zzad);
    }

    private zzy$zzad() {
    }

    public static zza zza(zzy$zzad zzy_zzad) {
        return (zza) zzbd.zza(zzy_zzad);
    }

    /* access modifiers changed from: private */
    public final void zza(zzaj zzaj) {
        this.zzg = zzaj.zza();
        this.zzd |= 2;
    }

    /* access modifiers changed from: private */
    public final void zza(zzy$zzau zzy_zzau) {
        zzy_zzau.getClass();
        this.zzy = zzy_zzau;
        this.zzd |= 524288;
    }

    /* access modifiers changed from: private */
    public final void zza(zzy$zzbh zzy_zzbh) {
        zzy_zzbh.getClass();
        this.zzf = zzy_zzbh;
        this.zzd |= 1;
    }

    /* access modifiers changed from: private */
    public final void zza(boolean z) {
        this.zzd |= 4;
        this.zzh = true;
    }

    public static zza zzb() {
        return (zza) zzbd.zzh();
    }

    public final zzy$zzbh zza() {
        zzy$zzbh zzy_zzbh = this.zzf;
        return zzy_zzbh == null ? zzy$zzbh.zzc() : zzy_zzbh;
    }

    /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzad>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r3, java.lang.Object r4, java.lang.Object r5) {
        /*
        // Method dump skipped, instructions count: 414
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzad.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
