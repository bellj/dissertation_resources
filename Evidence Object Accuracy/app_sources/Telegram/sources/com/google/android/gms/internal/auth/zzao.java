package com.google.android.gms.internal.auth;

import android.os.IBinder;

/* loaded from: classes.dex */
public final class zzao extends zza implements zzan {
    /* access modifiers changed from: package-private */
    public zzao(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.internal.IAuthService");
    }
}
