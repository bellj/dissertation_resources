package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* loaded from: classes.dex */
final class zacm implements Runnable {
    private final /* synthetic */ Result zaa;
    private final /* synthetic */ zacn zab;

    /* access modifiers changed from: package-private */
    public zacm(zacn zacn, Result result) {
        this.zab = zacn;
        this.zaa = result;
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            try {
                BasePendingResult.zaa.set(Boolean.TRUE);
                ((ResultTransform) Preconditions.checkNotNull(this.zab.zaa)).onSuccess(this.zaa);
                zacp unused = this.zab.zah;
                zacp unused2 = this.zab.zah;
                throw null;
            } catch (RuntimeException unused3) {
                zacp unused4 = this.zab.zah;
                zacp unused5 = this.zab.zah;
                throw null;
            }
        } catch (Throwable th) {
            BasePendingResult.zaa.set(Boolean.FALSE);
            zacn zacn = this.zab;
            zacn.zaa(this.zaa);
            GoogleApiClient googleApiClient = (GoogleApiClient) this.zab.zag.get();
            if (googleApiClient != null) {
                googleApiClient.zab(this.zab);
            }
            throw th;
        }
    }
}
