package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import org.telegram.messenger.FileLoader;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzbc extends zzeo<zzy$zzbc, zzb> implements zzgb {
    private static final zzy$zzbc zzi;
    private static volatile zzgj<zzy$zzbc> zzj;
    private int zzc;
    private zzy$zzaf zzd;
    private zzew<zzc> zze = zzeo.zzl();
    private int zzf;
    private int zzg;
    private int zzh;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zza implements zzet {
        NO_ERROR(0),
        STATUS_SENSITIVE_TOPIC(1),
        STATUS_QUALITY_THRESHOLDED(2),
        STATUS_INTERNAL_ERROR(3),
        STATUS_NOT_SUPPORTED_LANGUAGE(FileLoader.MEDIA_DIR_VIDEO_PUBLIC),
        STATUS_32_BIT_CPU(1001),
        STATUS_32_BIT_APP(1002);
        
        private final int zzi;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzi;
        }

        public static zzev zzb() {
            return zzbw.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        zza(int i) {
            this.zzi = i;
        }

        static {
            new zzbx();
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzc extends zzeo<zzc, zza> implements zzgb {
        private static final zzc zze;
        private static volatile zzgj<zzc> zzf;
        private int zzc;
        private float zzd;

        private zzc() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzc, zza> implements zzgb {
            private zza() {
                super(zzc.zze);
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x004e;
                    case 2: goto L_0x0048;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zzf
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zzf     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zze     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zzf = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zze
                return r2
            L_0x0033:
                r2 = 2
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                java.lang.String r3 = "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ခ\u0000"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zze
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0048:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc$zza
                r2.<init>(r3)
                return r2
            L_0x004e:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzc zzc = new zzc();
            zze = zzc;
            zzeo.zza(zzc.class, zzc);
        }
    }

    private zzy$zzbc() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo.zzb<zzy$zzbc, zzb> implements zzgb {
        private zzb() {
            super(zzy$zzbc.zzi);
        }

        /* synthetic */ zzb(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x006f;
                case 2: goto L_0x0069;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzj
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzj     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbc r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzi     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzj = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbc r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzi
            return r2
        L_0x0033:
            r2 = 8
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzc> r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzc.class
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 5
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zza.zzb()
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001ဉ\u0000\u0002\u001b\u0003ဌ\u0001\u0004င\u0002\u0005င\u0003"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbc r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zzi
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0069:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbc$zzb
            r2.<init>(r3)
            return r2
        L_0x006f:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbc r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbc
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzbc.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzbc zzy_zzbc = new zzy$zzbc();
        zzi = zzy_zzbc;
        zzeo.zza(zzy$zzbc.class, zzy_zzbc);
    }
}
