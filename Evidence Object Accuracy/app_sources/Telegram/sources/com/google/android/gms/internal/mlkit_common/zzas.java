package com.google.android.gms.internal.mlkit_common;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* compiled from: com.google.mlkit:common@@17.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzas extends WeakReference<Throwable> {
    private final int zza;

    public zzas(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.zza = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.zza;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == zzas.class) {
            if (this == obj) {
                return true;
            }
            zzas zzas = (zzas) obj;
            if (this.zza == zzas.zza && get() == zzas.get()) {
                return true;
            }
        }
        return false;
    }
}
