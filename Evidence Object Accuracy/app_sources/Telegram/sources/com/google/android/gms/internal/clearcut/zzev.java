package com.google.android.gms.internal.clearcut;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface zzev {
    int size();

    byte zzj(int i);
}
