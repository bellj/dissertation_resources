package com.google.android.gms.internal.clearcut;

import android.database.ContentObserver;
import android.os.Handler;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzz extends ContentObserver {
    /* access modifiers changed from: package-private */
    public zzz(Handler handler) {
        super(null);
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        zzy.zzct.set(true);
    }
}
