package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzam extends zzeo<zzy$zzam, zza> implements zzgb {
    private static final zzy$zzam zzh;
    private static volatile zzgj<zzy$zzam> zzi;
    private int zzc;
    private zzy$zzal zzd;
    private zzb zze;
    private zzb zzf;
    private boolean zzg;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo<zzb, zza> implements zzgb {
        private static final zzb zzh;
        private static volatile zzgj<zzb> zzi;
        private int zzc;
        private boolean zzd;
        private boolean zze;
        private boolean zzf;
        private boolean zzg;

        private zzb() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzb, zza> implements zzgb {
            private zza() {
                super(zzb.zzh);
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x005d;
                    case 2: goto L_0x0057;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zzi
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zzi     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zzh     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zzi = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zzh
                return r2
            L_0x0033:
                r2 = 5
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                r3 = 2
                java.lang.String r4 = "zze"
                r2[r3] = r4
                r3 = 3
                java.lang.String r4 = "zzf"
                r2[r3] = r4
                r3 = 4
                java.lang.String r4 = "zzg"
                r2[r3] = r4
                java.lang.String r3 = "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဇ\u0000\u0002ဇ\u0001\u0003ဇ\u0002\u0004ဇ\u0003"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zzh
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0057:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb$zza
                r2.<init>(r3)
                return r2
            L_0x005d:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zzb
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzb.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzb zzb = new zzb();
            zzh = zzb;
            zzeo.zza(zzb.class, zzb);
        }
    }

    private zzy$zzam() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzam, zza> implements zzgb {
        private zza() {
            super(zzy$zzam.zzh);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzeo$zza, com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzam>] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x005d;
                case 2: goto L_0x0057;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzam> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzi
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzam> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzam> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzi     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzam r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzh     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzi = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzam r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzh
            return r2
        L_0x0033:
            r2 = 5
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဇ\u0003"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzam r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zzh
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0057:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzam$zza
            r2.<init>(r3)
            return r2
        L_0x005d:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzam r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzam
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzam.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzam zzy_zzam = new zzy$zzam();
        zzh = zzy_zzam;
        zzeo.zza(zzy$zzam.class, zzy_zzam);
    }
}
