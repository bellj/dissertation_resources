package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzhn {
    public int zza;
    public long zzb;
    public Object zzc;
    public final zzio zzd;

    /* access modifiers changed from: package-private */
    public zzhn(zzio zzio) {
        zzio.getClass();
        this.zzd = zzio;
    }
}
