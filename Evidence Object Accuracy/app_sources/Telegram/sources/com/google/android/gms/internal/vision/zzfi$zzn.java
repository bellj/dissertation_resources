package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzn extends zzjb<zzfi$zzn, zza> implements zzkm {
    private static final zzfi$zzn zzh;
    private static volatile zzkx<zzfi$zzn> zzi;
    private int zzc;
    private zzfi$zzd zzd;
    private int zze;
    private zzfi$zzh zzf;
    private zzfi$zzc zzg;

    private zzfi$zzn() {
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzjb$zza, com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzn>] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzjb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.vision.zzfk.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x005d;
                case 2: goto L_0x0057;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzn> r2 = com.google.android.gms.internal.vision.zzfi$zzn.zzi
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zzn> r3 = com.google.android.gms.internal.vision.zzfi$zzn.class
            monitor-enter(r3)
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzn> r2 = com.google.android.gms.internal.vision.zzfi$zzn.zzi     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.vision.zzjb$zza r2 = new com.google.android.gms.internal.vision.zzjb$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzn r4 = com.google.android.gms.internal.vision.zzfi$zzn.zzh     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzn.zzi = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.vision.zzfi$zzn r2 = com.google.android.gms.internal.vision.zzfi$zzn.zzh
            return r2
        L_0x0033:
            r2 = 5
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0004\u0000\u0001\u0001\u0011\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002င\u0001\u0010ဉ\u0002\u0011ဉ\u0003"
            com.google.android.gms.internal.vision.zzfi$zzn r4 = com.google.android.gms.internal.vision.zzfi$zzn.zzh
            java.lang.Object r2 = com.google.android.gms.internal.vision.zzjb.zza(r4, r3, r2)
            return r2
        L_0x0057:
            com.google.android.gms.internal.vision.zzfi$zzn$zza r2 = new com.google.android.gms.internal.vision.zzfi$zzn$zza
            r2.<init>(r3)
            return r2
        L_0x005d:
            com.google.android.gms.internal.vision.zzfi$zzn r2 = new com.google.android.gms.internal.vision.zzfi$zzn
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfi$zzn.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class zza extends zzjb.zzb<zzfi$zzn, zza> implements zzkm {
        private zza() {
            super(zzfi$zzn.zzh);
        }

        /* synthetic */ zza(zzfk zzfk) {
            this();
        }
    }

    static {
        zzfi$zzn zzfi_zzn = new zzfi$zzn();
        zzh = zzfi_zzn;
        zzjb.zza(zzfi$zzn.class, zzfi_zzn);
    }
}
