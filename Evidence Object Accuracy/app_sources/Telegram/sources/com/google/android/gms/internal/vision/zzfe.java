package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfe {
    private static final zzfd zza;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    static final class zza extends zzfd {
        zza() {
        }

        @Override // com.google.android.gms.internal.vision.zzfd
        public final void zza(Throwable th) {
            th.printStackTrace();
        }
    }

    public static void zza(Throwable th) {
        zza.zza(th);
    }

    private static Integer zza() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    static {
        /*
            java.lang.Integer r0 = zza()     // Catch: all -> 0x002c
            if (r0 == 0) goto L_0x0014
            int r1 = r0.intValue()     // Catch: all -> 0x002a
            r2 = 19
            if (r1 < r2) goto L_0x0014
            com.google.android.gms.internal.vision.zzfj r1 = new com.google.android.gms.internal.vision.zzfj     // Catch: all -> 0x002a
            r1.<init>()     // Catch: all -> 0x002a
            goto L_0x005f
        L_0x0014:
            java.lang.String r1 = "com.google.devtools.build.android.desugar.runtime.twr_disable_mimic"
            boolean r1 = java.lang.Boolean.getBoolean(r1)     // Catch: all -> 0x002a
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0024
            com.google.android.gms.internal.vision.zzfh r1 = new com.google.android.gms.internal.vision.zzfh     // Catch: all -> 0x002a
            r1.<init>()     // Catch: all -> 0x002a
            goto L_0x005f
        L_0x0024:
            com.google.android.gms.internal.vision.zzfe$zza r1 = new com.google.android.gms.internal.vision.zzfe$zza     // Catch: all -> 0x002a
            r1.<init>()     // Catch: all -> 0x002a
            goto L_0x005f
        L_0x002a:
            r1 = move-exception
            goto L_0x002e
        L_0x002c:
            r1 = move-exception
            r0 = 0
        L_0x002e:
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.Class<com.google.android.gms.internal.vision.zzfe$zza> r3 = com.google.android.gms.internal.vision.zzfe.zza.class
            java.lang.String r3 = r3.getName()
            int r4 = r3.length()
            int r4 = r4 + 133
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy "
            r5.append(r4)
            r5.append(r3)
            java.lang.String r3 = "will be used. The error is: "
            r5.append(r3)
            java.lang.String r3 = r5.toString()
            r2.println(r3)
            java.io.PrintStream r2 = java.lang.System.err
            r1.printStackTrace(r2)
            com.google.android.gms.internal.vision.zzfe$zza r1 = new com.google.android.gms.internal.vision.zzfe$zza
            r1.<init>()
        L_0x005f:
            com.google.android.gms.internal.vision.zzfe.zza = r1
            if (r0 != 0) goto L_0x0064
            goto L_0x0067
        L_0x0064:
            r0.intValue()
        L_0x0067:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfe.<clinit>():void");
    }
}
