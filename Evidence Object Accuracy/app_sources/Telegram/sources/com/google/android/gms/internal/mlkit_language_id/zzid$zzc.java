package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzid$zzc extends zzeo<zzid$zzc, zzb> implements zzgb {
    private static final zzid$zzc zzm;
    private static volatile zzgj<zzid$zzc> zzn;
    private int zzc;
    private int zzd;
    private int zze;
    private String zzf = "";
    private String zzg = "";
    private zzid$zza zzh;
    private zzd zzi;
    private int zzj;
    private zzew<zzid$zzg> zzk = zzeo.zzl();
    private int zzl;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zza implements zzet {
        UNKNOWN_ACTION(0),
        INITIALIZATION(1),
        COMPILATION(2),
        EXECUTION(3),
        TEARDOWN(4),
        VALIDATION(5);
        
        private final int zzh;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzh;
        }

        public static zzev zzb() {
            return zzih.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzh + " name=" + name() + '>';
        }

        zza(int i) {
            this.zzh = i;
        }

        static {
            new zzig();
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzc extends zzeo<zzc, zza> implements zzgb {
        private static final zzc zzf;
        private static volatile zzgj<zzc> zzg;
        private int zzc;
        private int zzd;
        private int zze;

        private zzc() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzc, zza> implements zzgb {
            private zza() {
                super(zzc.zzf);
            }

            /* synthetic */ zza(zzic zzic) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x0053;
                    case 2: goto L_0x004d;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zzg
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zzg     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zzf     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zzg = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zzf
                return r2
            L_0x0033:
                r2 = 3
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                r3 = 2
                java.lang.String r4 = "zze"
                r2[r3] = r4
                java.lang.String r3 = "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001င\u0000\u0002င\u0001"
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zzf
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x004d:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc$zza
                r2.<init>(r3)
                return r2
            L_0x0053:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzc
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzc.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzc zzc = new zzc();
            zzf = zzc;
            zzeo.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzd extends zzeo<zzd, zza> implements zzgb {
        private static final zzd zzg;
        private static volatile zzgj<zzd> zzh;
        private int zzc;
        private zzc zzd;
        private zzc zze;
        private boolean zzf;

        private zzd() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzd, zza> implements zzgb {
            private zza() {
                super(zzd.zzg);
            }

            /* synthetic */ zza(zzic zzic) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x0058;
                    case 2: goto L_0x0052;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zzh
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zzh     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zzg     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zzh = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zzg
                return r2
            L_0x0033:
                r2 = 4
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                r3 = 2
                java.lang.String r4 = "zze"
                r2[r3] = r4
                r3 = 3
                java.lang.String r4 = "zzf"
                r2[r3] = r4
                java.lang.String r3 = "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဇ\u0002"
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zzg
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0052:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd$zza
                r2.<init>(r3)
                return r2
            L_0x0058:
                com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzd
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzd.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzd zzd = new zzd();
            zzg = zzd;
            zzeo.zza(zzd.class, zzd);
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zze implements zzet {
        UNKNOWN_STATUS(0),
        COMPLETED_EVENT(1),
        MISSING_END_EVENT(2),
        HANG(3),
        ABANDONED_FROM_HANG(4),
        FORCED_CRASH_FROM_HANG(5);
        
        private final int zzh;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzh;
        }

        public static zzev zzb() {
            return zzik.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zze.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzh + " name=" + name() + '>';
        }

        zze(int i) {
            this.zzh = i;
        }

        static {
            new zzii();
        }
    }

    private zzid$zzc() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo.zzb<zzid$zzc, zzb> implements zzgb {
        private zzb() {
            super(zzid$zzc.zzm);
        }

        /* synthetic */ zzb(zzic zzic) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x008f;
                case 2: goto L_0x0089;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzn
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzc> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzn     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzid$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzm     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzn = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzc r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzm
            return r2
        L_0x0033:
            r2 = 13
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zza.zzb()
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zze.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 10
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            r3 = 11
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzg> r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzg.class
            r2[r3] = r4
            r3 = 12
            java.lang.String r4 = "zzl"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0001\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဈ\u0002\u0004ဈ\u0003\u0005ဉ\u0004\u0006ဉ\u0005\u0007င\u0006\b\u001b\tင\u0007"
            com.google.android.gms.internal.mlkit_language_id.zzid$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zzm
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0089:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzc$zzb
            r2.<init>(r3)
            return r2
        L_0x008f:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzc r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzc
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzc.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzid$zzc zzid_zzc = new zzid$zzc();
        zzm = zzid_zzc;
        zzeo.zza(zzid$zzc.class, zzid_zzc);
    }
}
