package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzid$zzj extends zzeo<zzid$zzj, zza> implements zzgb {
    private static final zzid$zzj zzg;
    private static volatile zzgj<zzid$zzj> zzh;
    private int zzc;
    private int zzd;
    private zzid$zzh zze;
    private zzid$zze zzf;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zzb implements zzet {
        DELEGATE_NONE(0),
        NNAPI(1),
        GPU(2),
        HEXAGON(3),
        EDGETPU(4);
        
        private final int zzg;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzg;
        }

        public static zzev zzb() {
            return zzio.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
        }

        zzb(int i) {
            this.zzg = i;
        }

        static {
            new zzin();
        }
    }

    private zzid$zzj() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzid$zzj, zza> implements zzgb {
        private zza() {
            super(zzid$zzj.zzg);
        }

        /* synthetic */ zza(zzic zzic) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzj>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzic.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x005f;
                case 2: goto L_0x0059;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzj> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzh
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzid$zzj> r3 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzid$zzj> r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzh     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzid$zzj r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzg     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzh = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzj r2 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzg
            return r2
        L_0x0033:
            r2 = 5
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzb.zzb()
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဉ\u0001\u0003ဉ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzid$zzj r4 = com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zzg
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0059:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzj$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzj$zza
            r2.<init>(r3)
            return r2
        L_0x005f:
            com.google.android.gms.internal.mlkit_language_id.zzid$zzj r2 = new com.google.android.gms.internal.mlkit_language_id.zzid$zzj
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzid$zzj.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzid$zzj zzid_zzj = new zzid$zzj();
        zzg = zzid_zzj;
        zzeo.zza(zzid$zzj.class, zzid_zzj);
    }
}
