package com.google.android.gms.location;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.location.zzaf;
import com.google.android.gms.internal.location.zzaz;
import com.google.android.gms.internal.location.zzbi;
import com.google.android.gms.internal.location.zzz;

/* compiled from: com.google.android.gms:play-services-location@@18.0.0 */
/* loaded from: classes.dex */
public class LocationServices {
    @RecentlyNonNull
    public static final Api<Api.ApiOptions.NoOptions> API;
    @RecentlyNonNull
    @Deprecated
    public static final FusedLocationProviderApi FusedLocationApi = new zzz();
    @RecentlyNonNull
    @Deprecated
    public static final SettingsApi SettingsApi = new zzbi();
    private static final Api.ClientKey<zzaz> zza;
    private static final Api.AbstractClientBuilder<zzaz, Api.ApiOptions.NoOptions> zzb;

    static {
        Api.ClientKey<zzaz> clientKey = new Api.ClientKey<>();
        zza = clientKey;
        zzbh zzbh = new zzbh();
        zzb = zzbh;
        API = new Api<>("LocationServices.API", zzbh, clientKey);
        new zzaf();
    }

    public static zzaz zza(GoogleApiClient googleApiClient) {
        boolean z = true;
        Preconditions.checkArgument(googleApiClient != null, "GoogleApiClient parameter is required.");
        zzaz zzaz = (zzaz) googleApiClient.getClient(zza);
        if (zzaz == null) {
            z = false;
        }
        Preconditions.checkState(z, "GoogleApiClient is not configured to use the LocationServices.API Api. Pass thisinto GoogleApiClient.Builder#addApi() to use this feature.");
        return zzaz;
    }
}
