package com.google.android.gms.internal.play_billing;

/* compiled from: com.android.billingclient:billing@@5.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzq {
    /* access modifiers changed from: package-private */
    public static int zza(int i) {
        return (int) (((long) Integer.rotateLeft((int) (((long) i) * -862048943), 15)) * 461845907);
    }
}
