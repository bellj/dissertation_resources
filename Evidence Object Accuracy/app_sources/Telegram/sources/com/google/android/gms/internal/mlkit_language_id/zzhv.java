package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* JADX WARN: Init of enum zzc can be incorrect */
/* JADX WARN: Init of enum zzd can be incorrect */
/* JADX WARN: Init of enum zze can be incorrect */
/* JADX WARN: Init of enum zzf can be incorrect */
/* JADX WARN: Init of enum zzg can be incorrect */
/* JADX WARN: Init of enum zzj can be incorrect */
/* JADX WARN: Init of enum zzk can be incorrect */
/* JADX WARN: Init of enum zzm can be incorrect */
/* JADX WARN: Init of enum zzo can be incorrect */
/* JADX WARN: Init of enum zzp can be incorrect */
/* JADX WARN: Init of enum zzq can be incorrect */
/* JADX WARN: Init of enum zzr can be incorrect */
/* loaded from: classes.dex */
public enum zzhv {
    DOUBLE(zzhy.DOUBLE, 1),
    FLOAT(zzhy.FLOAT, 5),
    INT64(r5, 0),
    UINT64(r5, 0),
    INT32(r11, 0),
    FIXED64(r5, 1),
    FIXED32(r11, 5),
    BOOL(zzhy.BOOLEAN, 0),
    STRING(zzhy.STRING, 2) {
    },
    GROUP(r13, 3) {
    },
    MESSAGE(r13, 2) {
    },
    BYTES(zzhy.BYTE_STRING, 2) {
    },
    UINT32(r11, 0),
    ENUM(zzhy.ENUM, 0),
    SFIXED32(r11, 5),
    SFIXED64(r5, 1),
    SINT32(r11, 0),
    SINT64(r5, 0);
    
    private final zzhy zzs;
    private final int zzt;

    zzhv(zzhy zzhy, int i) {
        this.zzs = zzhy;
        this.zzt = i;
    }

    public final zzhy zza() {
        return this.zzs;
    }

    static {
        zzhy zzhy = zzhy.LONG;
        zzhy zzhy2 = zzhy.INT;
        zzhy zzhy3 = zzhy.MESSAGE;
    }
}
