package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import com.google.android.gms.internal.mlkit_language_id.zzy$zzaf;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzau extends zzeo<zzy$zzau, zza> implements zzgb {
    private static final zzy$zzau zzh;
    private static volatile zzgj<zzy$zzau> zzi;
    private int zzc;
    private zzy$zzaf zzd;
    private zzy$zzai zze;
    private zzc zzf;
    private zzd zzg;

    private zzy$zzau() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzc extends zzeo<zzc, zza> implements zzgb {
        private static final zzc zze;
        private static volatile zzgj<zzc> zzf;
        private int zzc;
        private zzb zzd;

        private zzc() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzc, zza> implements zzgb {
            private zza() {
                super(zzc.zze);
            }

            public final zza zza(zzb.zza zza) {
                if (this.zzb) {
                    zzc();
                    this.zzb = false;
                }
                ((zzc) this.zza).zza((zzb) ((zzeo) zza.zzg()));
                return this;
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        /* access modifiers changed from: private */
        public final void zza(zzb zzb) {
            zzb.getClass();
            this.zzd = zzb;
            this.zzc |= 1;
        }

        public static zza zza() {
            return zze.zzh();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x004e;
                    case 2: goto L_0x0048;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zzf
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zzf     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zze     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zzf = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zze
                return r2
            L_0x0033:
                r2 = 2
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                java.lang.String r3 = "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဉ\u0000"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zze
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0048:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc$zza
                r2.<init>(r3)
                return r2
            L_0x004e:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzc
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzc.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        public static zzc zzb() {
            return zze;
        }

        static {
            zzc zzc = new zzc();
            zze = zzc;
            zzeo.zza(zzc.class, zzc);
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzau, zza> implements zzgb {
        private zza() {
            super(zzy$zzau.zzh);
        }

        public final zza zza(zzy$zzaf.zza zza) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzau) this.zza).zza((zzy$zzaf) ((zzeo) zza.zzg()));
            return this;
        }

        public final zza zza(zzy$zzai zzy_zzai) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzau) this.zza).zza(zzy_zzai);
            return this;
        }

        public final zza zza(zzc zzc) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzau) this.zza).zza(zzc);
            return this;
        }

        public final zza zza(zzd zzd) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzau) this.zza).zza(zzd);
            return this;
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(zzy$zzaf zzy_zzaf) {
        zzy_zzaf.getClass();
        this.zzd = zzy_zzaf;
        this.zzc |= 1;
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo<zzb, zza> implements zzgb {
        private static final zzb zzf;
        private static volatile zzgj<zzb> zzg;
        private int zzc;
        private float zzd;
        private String zze = "";

        private zzb() {
        }

        /* access modifiers changed from: private */
        public final void zza(String str) {
            str.getClass();
            this.zzc |= 2;
            this.zze = str;
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzb, zza> implements zzgb {
            private zza() {
                super(zzb.zzf);
            }

            public final zza zza(String str) {
                if (this.zzb) {
                    zzc();
                    this.zzb = false;
                }
                ((zzb) this.zza).zza(str);
                return this;
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        public static zza zza() {
            return zzf.zzh();
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x0053;
                    case 2: goto L_0x004d;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zzg
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zzg     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zzf     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zzg = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zzf
                return r2
            L_0x0033:
                r2 = 3
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.String r3 = "zzd"
                r2[r4] = r3
                r3 = 2
                java.lang.String r4 = "zze"
                r2[r3] = r4
                java.lang.String r3 = "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ခ\u0000\u0002ဈ\u0001"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zzf
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x004d:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb$zza
                r2.<init>(r3)
                return r2
            L_0x0053:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzb zzb = new zzb();
            zzf = zzb;
            zzeo.zza(zzb.class, zzb);
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzd extends zzeo<zzd, zza> implements zzgb {
        private static final zzd zzd;
        private static volatile zzgj<zzd> zze;
        private zzew<zzb> zzc = zzeo.zzl();

        private zzd() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzd, zza> implements zzgb {
            private zza() {
                super(zzd.zzd);
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
            /*
                r1 = this;
                int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r4 = 1
                int r2 = r2 - r4
                r2 = r3[r2]
                r3 = 0
                switch(r2) {
                    case 1: goto L_0x004e;
                    case 2: goto L_0x0048;
                    case 3: goto L_0x0033;
                    case 4: goto L_0x0030;
                    case 5: goto L_0x0016;
                    case 6: goto L_0x0011;
                    case 7: goto L_0x0010;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
                r2.<init>()
                throw r2
            L_0x0010:
                return r3
            L_0x0011:
                java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
                return r2
            L_0x0016:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zze
                if (r2 != 0) goto L_0x002f
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.class
                monitor-enter(r3)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zze     // Catch: all -> 0x002c
                if (r2 != 0) goto L_0x002a
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zzd     // Catch: all -> 0x002c
                r2.<init>(r4)     // Catch: all -> 0x002c
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zze = r2     // Catch: all -> 0x002c
            L_0x002a:
                monitor-exit(r3)     // Catch: all -> 0x002c
                goto L_0x002f
            L_0x002c:
                r2 = move-exception
                monitor-exit(r3)     // Catch: all -> 0x002c
                throw r2
            L_0x002f:
                return r2
            L_0x0030:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zzd
                return r2
            L_0x0033:
                r2 = 2
                java.lang.Object[] r2 = new java.lang.Object[r2]
                r3 = 0
                java.lang.String r0 = "zzc"
                r2[r3] = r0
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzb.class
                r2[r4] = r3
                java.lang.String r3 = "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zzd
                java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
                return r2
            L_0x0048:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd$zza
                r2.<init>(r3)
                return r2
            L_0x004e:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zzd
                r2.<init>()
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzd.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzd zzd2 = new zzd();
            zzd = zzd2;
            zzeo.zza(zzd.class, zzd2);
        }
    }

    /* access modifiers changed from: private */
    public final void zza(zzy$zzai zzy_zzai) {
        zzy_zzai.getClass();
        this.zze = zzy_zzai;
        this.zzc |= 2;
    }

    /* access modifiers changed from: private */
    public final void zza(zzc zzc2) {
        zzc2.getClass();
        this.zzf = zzc2;
        this.zzc |= 4;
    }

    /* access modifiers changed from: private */
    public final void zza(zzd zzd2) {
        zzd2.getClass();
        this.zzg = zzd2;
        this.zzc |= 8;
    }

    public static zza zza() {
        return zzh.zzh();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x005d;
                case 2: goto L_0x0057;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzi
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzau> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzau> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzi     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzau r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzh     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzi = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzau r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzh
            return r2
        L_0x0033:
            r2 = 5
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဉ\u0003"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzau r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zzh
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0057:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau$zza
            r2.<init>(r3)
            return r2
        L_0x005d:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzau r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzau
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzau.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzau zzy_zzau = new zzy$zzau();
        zzh = zzy_zzau;
        zzeo.zza(zzy$zzau.class, zzy_zzau);
    }
}
