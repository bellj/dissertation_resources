package com.google.android.gms.cloudmessaging;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.internal.cloudmessaging.zze;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import javax.annotation.concurrent.GuardedBy;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzf implements ServiceConnection {
    @GuardedBy("this")
    int zza;
    final Messenger zzb;
    zzo zzc;
    @GuardedBy("this")
    final Queue<zzq<?>> zzd;
    @GuardedBy("this")
    final SparseArray<zzq<?>> zze;
    final /* synthetic */ zze zzf;

    private zzf(zze zze) {
        this.zzf = zze;
        this.zza = 0;
        this.zzb = new Messenger(new zze(Looper.getMainLooper(), new Handler.Callback(this) { // from class: com.google.android.gms.cloudmessaging.zzi
            private final zzf zza;

            /* access modifiers changed from: package-private */
            {
                this.zza = r1;
            }

            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                return this.zza.zza(message);
            }
        }));
        this.zzd = new ArrayDeque();
        this.zze = new SparseArray<>();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean zza(zzq<?> zzq) {
        int i = this.zza;
        if (i == 0) {
            this.zzd.add(zzq);
            Preconditions.checkState(this.zza == 0);
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Starting bind to GmsCore");
            }
            this.zza = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!ConnectionTracker.getInstance().bindService(this.zzf.zzb, intent, this, 1)) {
                zza(0, "Unable to bind to service");
            } else {
                this.zzf.zzc.schedule(new Runnable(this) { // from class: com.google.android.gms.cloudmessaging.zzh
                    private final zzf zza;

                    /* access modifiers changed from: package-private */
                    {
                        this.zza = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        this.zza.zzc();
                    }
                }, 30, TimeUnit.SECONDS);
            }
            return true;
        } else if (i == 1) {
            this.zzd.add(zzq);
            return true;
        } else if (i != 2) {
            if (!(i == 3 || i == 4)) {
                int i2 = this.zza;
                StringBuilder sb = new StringBuilder(26);
                sb.append("Unknown state: ");
                sb.append(i2);
                throw new IllegalStateException(sb.toString());
            }
            return false;
        } else {
            this.zzd.add(zzq);
            zza();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Received response to request: ");
            sb.append(i);
            Log.d("MessengerIpcClient", sb.toString());
        }
        synchronized (this) {
            zzq<?> zzq = this.zze.get(i);
            if (zzq == null) {
                StringBuilder sb2 = new StringBuilder(50);
                sb2.append("Received response for unknown request: ");
                sb2.append(i);
                Log.w("MessengerIpcClient", sb2.toString());
                return true;
            }
            this.zze.remove(i);
            zzb();
            Bundle data = message.getData();
            if (data.getBoolean("unsupported", false)) {
                zzq.zza(new zzp(4, "Not supported by GmsCore"));
            } else {
                zzq.zza(data);
            }
            return true;
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        this.zzf.zzc.execute(new Runnable(this, iBinder) { // from class: com.google.android.gms.cloudmessaging.zzk
            private final zzf zza;
            private final IBinder zzb;

            /* access modifiers changed from: package-private */
            {
                this.zza = r1;
                this.zzb = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                zzf zzf = this.zza;
                IBinder iBinder2 = this.zzb;
                synchronized (zzf) {
                    try {
                        if (iBinder2 == null) {
                            zzf.zza(0, "Null service connection");
                            return;
                        }
                        try {
                            zzf.zzc = new zzo(iBinder2);
                            zzf.zza = 2;
                            zzf.zza();
                        } catch (RemoteException e) {
                            zzf.zza(0, e.getMessage());
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public final void zza() {
        this.zzf.zzc.execute(new Runnable(this) { // from class: com.google.android.gms.cloudmessaging.zzj
            private final zzf zza;

            /* access modifiers changed from: package-private */
            {
                this.zza = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                zzq<?> poll;
                zzf zzf = this.zza;
                while (true) {
                    synchronized (zzf) {
                        if (zzf.zza == 2) {
                            if (zzf.zzd.isEmpty()) {
                                zzf.zzb();
                                return;
                            }
                            poll = zzf.zzd.poll();
                            zzf.zze.put(poll.zza, poll);
                            zzf.zzf.zzc.schedule(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0035: INVOKE  
                                  (wrap: java.util.concurrent.ScheduledExecutorService : 0x0028: ONE_ARG  (r3v2 java.util.concurrent.ScheduledExecutorService A[REMOVE]) = 
                                  (wrap: java.util.concurrent.ScheduledExecutorService : 0x0000: IGET  
                                  (wrap: com.google.android.gms.cloudmessaging.zze : 0x0026: IGET  (r3v1 com.google.android.gms.cloudmessaging.zze A[REMOVE]) = (r0v0 'zzf' com.google.android.gms.cloudmessaging.zzf) com.google.android.gms.cloudmessaging.zzf.zzf com.google.android.gms.cloudmessaging.zze)
                                 com.google.android.gms.cloudmessaging.zze.zzc java.util.concurrent.ScheduledExecutorService)
                                )
                                  (wrap: com.google.android.gms.cloudmessaging.zzl : 0x002e: CONSTRUCTOR  (r4v1 com.google.android.gms.cloudmessaging.zzl A[REMOVE]) = (r0v0 'zzf' com.google.android.gms.cloudmessaging.zzf), (r1v6 'poll' com.google.android.gms.cloudmessaging.zzq<?>) call: com.google.android.gms.cloudmessaging.zzl.<init>(com.google.android.gms.cloudmessaging.zzf, com.google.android.gms.cloudmessaging.zzq):void type: CONSTRUCTOR)
                                  (30 long)
                                  (wrap: java.util.concurrent.TimeUnit : 0x0033: SGET  (r7v0 java.util.concurrent.TimeUnit A[REMOVE]) =  java.util.concurrent.TimeUnit.SECONDS java.util.concurrent.TimeUnit)
                                 type: INTERFACE call: java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture in method: com.google.android.gms.cloudmessaging.zzj.run():void, file: classes.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeSynchronizedRegion(RegionGen.java:254)
                                	at jadx.core.dex.regions.SynchronizedRegion.generate(SynchronizedRegion.java:44)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:189)
                                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x002e: CONSTRUCTOR  (r4v1 com.google.android.gms.cloudmessaging.zzl A[REMOVE]) = (r0v0 'zzf' com.google.android.gms.cloudmessaging.zzf), (r1v6 'poll' com.google.android.gms.cloudmessaging.zzq<?>) call: com.google.android.gms.cloudmessaging.zzl.<init>(com.google.android.gms.cloudmessaging.zzf, com.google.android.gms.cloudmessaging.zzq):void type: CONSTRUCTOR in method: com.google.android.gms.cloudmessaging.zzj.run():void, file: classes.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 39 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: com.google.android.gms.cloudmessaging.zzl, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 45 more
                                */
                            /*
                                this = this;
                                com.google.android.gms.cloudmessaging.zzf r0 = r8.zza
                            L_0x0002:
                                monitor-enter(r0)
                                int r1 = r0.zza     // Catch: all -> 0x00aa
                                r2 = 2
                                if (r1 == r2) goto L_0x000a
                                monitor-exit(r0)     // Catch: all -> 0x00aa
                                return
                            L_0x000a:
                                java.util.Queue<com.google.android.gms.cloudmessaging.zzq<?>> r1 = r0.zzd     // Catch: all -> 0x00aa
                                boolean r1 = r1.isEmpty()     // Catch: all -> 0x00aa
                                if (r1 == 0) goto L_0x0017
                                r0.zzb()     // Catch: all -> 0x00aa
                                monitor-exit(r0)     // Catch: all -> 0x00aa
                                return
                            L_0x0017:
                                java.util.Queue<com.google.android.gms.cloudmessaging.zzq<?>> r1 = r0.zzd     // Catch: all -> 0x00aa
                                java.lang.Object r1 = r1.poll()     // Catch: all -> 0x00aa
                                com.google.android.gms.cloudmessaging.zzq r1 = (com.google.android.gms.cloudmessaging.zzq) r1     // Catch: all -> 0x00aa
                                android.util.SparseArray<com.google.android.gms.cloudmessaging.zzq<?>> r3 = r0.zze     // Catch: all -> 0x00aa
                                int r4 = r1.zza     // Catch: all -> 0x00aa
                                r3.put(r4, r1)     // Catch: all -> 0x00aa
                                com.google.android.gms.cloudmessaging.zze r3 = r0.zzf     // Catch: all -> 0x00aa
                                java.util.concurrent.ScheduledExecutorService r3 = com.google.android.gms.cloudmessaging.zze.zzb(r3)     // Catch: all -> 0x00aa
                                com.google.android.gms.cloudmessaging.zzl r4 = new com.google.android.gms.cloudmessaging.zzl     // Catch: all -> 0x00aa
                                r4.<init>(r0, r1)     // Catch: all -> 0x00aa
                                r5 = 30
                                java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.SECONDS     // Catch: all -> 0x00aa
                                r3.schedule(r4, r5, r7)     // Catch: all -> 0x00aa
                                monitor-exit(r0)     // Catch: all -> 0x00aa
                                java.lang.String r3 = "MessengerIpcClient"
                                r4 = 3
                                boolean r3 = android.util.Log.isLoggable(r3, r4)
                                if (r3 == 0) goto L_0x0062
                                java.lang.String r3 = "MessengerIpcClient"
                                java.lang.String r4 = java.lang.String.valueOf(r1)
                                int r5 = r4.length()
                                int r5 = r5 + 8
                                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                                r6.<init>(r5)
                                java.lang.String r5 = "Sending "
                                r6.append(r5)
                                r6.append(r4)
                                java.lang.String r4 = r6.toString()
                                android.util.Log.d(r3, r4)
                            L_0x0062:
                                com.google.android.gms.cloudmessaging.zze r3 = r0.zzf
                                android.content.Context r3 = com.google.android.gms.cloudmessaging.zze.zza(r3)
                                android.os.Messenger r4 = r0.zzb
                                android.os.Message r5 = android.os.Message.obtain()
                                int r6 = r1.zzc
                                r5.what = r6
                                int r6 = r1.zza
                                r5.arg1 = r6
                                r5.replyTo = r4
                                android.os.Bundle r4 = new android.os.Bundle
                                r4.<init>()
                                java.lang.String r6 = "oneWay"
                                boolean r7 = r1.zza()
                                r4.putBoolean(r6, r7)
                                java.lang.String r6 = "pkg"
                                java.lang.String r3 = r3.getPackageName()
                                r4.putString(r6, r3)
                                java.lang.String r3 = "data"
                                android.os.Bundle r1 = r1.zzd
                                r4.putBundle(r3, r1)
                                r5.setData(r4)
                                com.google.android.gms.cloudmessaging.zzo r1 = r0.zzc     // Catch: RemoteException -> 0x00a0
                                r1.zza(r5)     // Catch: RemoteException -> 0x00a0
                                goto L_0x0002
                            L_0x00a0:
                                r1 = move-exception
                                java.lang.String r1 = r1.getMessage()
                                r0.zza(r2, r1)
                                goto L_0x0002
                            L_0x00aa:
                                r1 = move-exception
                                monitor-exit(r0)     // Catch: all -> 0x00aa
                                goto L_0x00ae
                            L_0x00ad:
                                throw r1
                            L_0x00ae:
                                goto L_0x00ad
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.cloudmessaging.zzj.run():void");
                        }
                    });
                }

                @Override // android.content.ServiceConnection
                public final void onServiceDisconnected(ComponentName componentName) {
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Service disconnected");
                    }
                    this.zzf.zzc.execute(new Runnable(this) { // from class: com.google.android.gms.cloudmessaging.zzm
                        private final zzf zza;

                        /* access modifiers changed from: package-private */
                        {
                            this.zza = r1;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            this.zza.zza(2, "Service disconnected");
                        }
                    });
                }

                /* access modifiers changed from: package-private */
                public final synchronized void zza(int i, String str) {
                    if (Log.isLoggable("MessengerIpcClient", 3)) {
                        String valueOf = String.valueOf(str);
                        Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
                    }
                    int i2 = this.zza;
                    if (i2 == 0) {
                        throw new IllegalStateException();
                    } else if (i2 == 1 || i2 == 2) {
                        if (Log.isLoggable("MessengerIpcClient", 2)) {
                            Log.v("MessengerIpcClient", "Unbinding service");
                        }
                        this.zza = 4;
                        ConnectionTracker.getInstance().unbindService(this.zzf.zzb, this);
                        zzp zzp = new zzp(i, str);
                        for (zzq<?> zzq : this.zzd) {
                            zzq.zza(zzp);
                        }
                        this.zzd.clear();
                        for (int i3 = 0; i3 < this.zze.size(); i3++) {
                            this.zze.valueAt(i3).zza(zzp);
                        }
                        this.zze.clear();
                    } else if (i2 == 3) {
                        this.zza = 4;
                    } else if (i2 != 4) {
                        int i4 = this.zza;
                        StringBuilder sb = new StringBuilder(26);
                        sb.append("Unknown state: ");
                        sb.append(i4);
                        throw new IllegalStateException(sb.toString());
                    }
                }

                /* access modifiers changed from: package-private */
                public final synchronized void zzb() {
                    if (this.zza == 2 && this.zzd.isEmpty() && this.zze.size() == 0) {
                        if (Log.isLoggable("MessengerIpcClient", 2)) {
                            Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
                        }
                        this.zza = 3;
                        ConnectionTracker.getInstance().unbindService(this.zzf.zzb, this);
                    }
                }

                /* access modifiers changed from: package-private */
                public final synchronized void zzc() {
                    if (this.zza == 1) {
                        zza(1, "Timed out while binding");
                    }
                }

                /* access modifiers changed from: package-private */
                public final synchronized void zza(int i) {
                    zzq<?> zzq = this.zze.get(i);
                    if (zzq != null) {
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("Timing out request: ");
                        sb.append(i);
                        Log.w("MessengerIpcClient", sb.toString());
                        this.zze.remove(i);
                        zzq.zza(new zzp(3, "Timed out waiting for response"));
                        zzb();
                    }
                }
            }
