package com.google.android.gms.internal.clearcut;

import java.io.IOException;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* loaded from: classes.dex */
final class zzez extends zzex<zzey, zzey> {
    private static void zza(Object obj, zzey zzey) {
        ((zzcg) obj).zzjp = zzey;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.clearcut.zzex
    public final /* synthetic */ void zza(Object obj, int i, long j) {
        ((zzey) obj).zzb(i << 3, Long.valueOf(j));
    }

    final /* synthetic */ void zza(Object obj, int i, zzbb zzbb) {
        ((zzey) obj).zzb((i << 3) | 2, zzbb);
    }

    final /* synthetic */ void zza(Object obj, zzfr zzfr) throws IOException {
        ((zzey) obj).zzb(zzfr);
    }

    final void zzc(Object obj) {
        ((zzcg) obj).zzjp.zzv();
    }

    final /* synthetic */ void zzc(Object obj, zzfr zzfr) throws IOException {
        ((zzey) obj).zza(zzfr);
    }

    final /* synthetic */ Object zzdz() {
        return zzey.zzeb();
    }

    final /* synthetic */ void zze(Object obj, Object obj2) {
        zza(obj, (zzey) obj2);
    }

    final /* synthetic */ void zzf(Object obj, Object obj2) {
        zza(obj, (zzey) obj2);
    }

    final /* synthetic */ Object zzg(Object obj, Object obj2) {
        zzey zzey = (zzey) obj;
        zzey zzey2 = (zzey) obj2;
        return zzey2.equals(zzey.zzea()) ? zzey : zzey.zza(zzey, zzey2);
    }

    final /* synthetic */ int zzm(Object obj) {
        return ((zzey) obj).zzas();
    }

    final /* synthetic */ Object zzq(Object obj) {
        return ((zzcg) obj).zzjp;
    }

    final /* synthetic */ int zzr(Object obj) {
        return ((zzey) obj).zzec();
    }
}
