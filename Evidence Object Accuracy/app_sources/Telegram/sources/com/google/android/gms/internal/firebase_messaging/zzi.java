package com.google.android.gms.internal.firebase_messaging;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* loaded from: classes.dex */
public final class zzi {
    private static final zzf zza;
    private static volatile zzf zzb;

    static {
        zzh zzh = new zzh(null);
        zza = zzh;
        zzb = zzh;
    }

    public static zzf zza() {
        return zzb;
    }
}
