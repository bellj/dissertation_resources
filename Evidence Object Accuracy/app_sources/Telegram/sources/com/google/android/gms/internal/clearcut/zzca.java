package com.google.android.gms.internal.clearcut;

import com.google.android.gms.internal.clearcut.zzca;

/* loaded from: classes.dex */
public interface zzca<T extends zzca<T>> extends Comparable<T> {
    zzdp zza(zzdp zzdp, zzdo zzdo);

    zzdv zza(zzdv zzdv, zzdv zzdv2);

    zzfl zzau();

    zzfq zzav();

    boolean zzaw();

    boolean zzax();

    int zzc();
}
