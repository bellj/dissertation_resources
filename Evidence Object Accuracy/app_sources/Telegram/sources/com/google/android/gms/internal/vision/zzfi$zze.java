package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zze extends zzjb<zzfi$zze, zza> implements zzkm {
    private static final zzfi$zze zzl;
    private static volatile zzkx<zzfi$zze> zzm;
    private int zzc;
    private String zzd = "";
    private boolean zze;
    private int zzf;
    private long zzg;
    private long zzh;
    private long zzi;
    private String zzj = "";
    private boolean zzk;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzb implements zzje {
        REASON_UNKNOWN(0),
        REASON_MISSING(1),
        REASON_UPGRADE(2),
        REASON_INVALID(3);
        
        private final int zzf;

        @Override // com.google.android.gms.internal.vision.zzje
        public final int zza() {
            return this.zzf;
        }

        public static zzb zza(int i) {
            if (i == 0) {
                return REASON_UNKNOWN;
            }
            if (i == 1) {
                return REASON_MISSING;
            }
            if (i == 2) {
                return REASON_UPGRADE;
            }
            if (i != 3) {
                return null;
            }
            return REASON_INVALID;
        }

        public static zzjg zzb() {
            return zzfn.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        zzb(int i) {
            this.zzf = i;
        }

        static {
            new zzfm();
        }
    }

    private zzfi$zze() {
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class zza extends zzjb.zzb<zzfi$zze, zza> implements zzkm {
        private zza() {
            super(zzfi$zze.zzl);
        }

        /* synthetic */ zza(zzfk zzfk) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zze>, com.google.android.gms.internal.vision.zzjb$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzjb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.vision.zzfk.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x007b;
                case 2: goto L_0x0075;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zze> r2 = com.google.android.gms.internal.vision.zzfi$zze.zzm
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zze> r3 = com.google.android.gms.internal.vision.zzfi$zze.class
            monitor-enter(r3)
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zze> r2 = com.google.android.gms.internal.vision.zzfi$zze.zzm     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.vision.zzjb$zza r2 = new com.google.android.gms.internal.vision.zzjb$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zze r4 = com.google.android.gms.internal.vision.zzfi$zze.zzl     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zze.zzm = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.vision.zzfi$zze r2 = com.google.android.gms.internal.vision.zzfi$zze.zzl
            return r2
        L_0x0033:
            r2 = 10
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.vision.zzjg r4 = com.google.android.gms.internal.vision.zzfi$zze.zzb.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဇ\u0001\u0003ဌ\u0002\u0004ဂ\u0003\u0005ဂ\u0004\u0006ဂ\u0005\u0007ဈ\u0006\bဇ\u0007"
            com.google.android.gms.internal.vision.zzfi$zze r4 = com.google.android.gms.internal.vision.zzfi$zze.zzl
            java.lang.Object r2 = com.google.android.gms.internal.vision.zzjb.zza(r4, r3, r2)
            return r2
        L_0x0075:
            com.google.android.gms.internal.vision.zzfi$zze$zza r2 = new com.google.android.gms.internal.vision.zzfi$zze$zza
            r2.<init>(r3)
            return r2
        L_0x007b:
            com.google.android.gms.internal.vision.zzfi$zze r2 = new com.google.android.gms.internal.vision.zzfi$zze
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfi$zze.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzfi$zze zzfi_zze = new zzfi$zze();
        zzl = zzfi_zze;
        zzjb.zza(zzfi$zze.class, zzfi_zze);
    }
}
