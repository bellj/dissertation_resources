package com.google.android.gms.internal.mlkit_language_id;

import java.util.Iterator;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzgu {
    private static final Iterator<Object> zza = new zzgx();
    private static final Iterable<Object> zzb = new zzgw();

    /* access modifiers changed from: package-private */
    public static <T> Iterable<T> zza() {
        return (Iterable<T>) zzb;
    }
}
