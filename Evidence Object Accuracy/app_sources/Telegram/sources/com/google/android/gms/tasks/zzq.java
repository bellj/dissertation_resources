package com.google.android.gms.tasks;

import java.util.ArrayDeque;
import java.util.Queue;
import javax.annotation.concurrent.GuardedBy;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzq<TResult> {
    private final Object zza = new Object();
    @GuardedBy("mLock")
    private Queue<zzr<TResult>> zzb;
    @GuardedBy("mLock")
    private boolean zzc;

    public final void zza(zzr<TResult> zzr) {
        synchronized (this.zza) {
            if (this.zzb == null) {
                this.zzb = new ArrayDeque();
            }
            this.zzb.add(zzr);
        }
    }

    public final void zza(Task<TResult> task) {
        zzr<TResult> poll;
        synchronized (this.zza) {
            if (this.zzb != null && !this.zzc) {
                this.zzc = true;
                while (true) {
                    synchronized (this.zza) {
                        poll = this.zzb.poll();
                        if (poll == null) {
                            this.zzc = false;
                            return;
                        }
                    }
                    poll.zza(task);
                }
            }
        }
    }
}
