package com.google.android.gms.internal.mlkit_language_id;

import java.io.IOException;

/*  JADX ERROR: UnsupportedOperationException in pass: OverrideMethodVisitor
    java.lang.UnsupportedOperationException
    	at jadx.core.utils.ImmutableList.listIterator(ImmutableList.java:198)
    	at java.util.AbstractList.equals(AbstractList.java:519)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.fixMethodArgTypes(OverrideMethodVisitor.java:288)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processMth(OverrideMethodVisitor.java:68)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.processCls(OverrideMethodVisitor.java:53)
    	at jadx.core.dex.visitors.OverrideMethodVisitor.visit(OverrideMethodVisitor.java:45)
    */
/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzhj extends zzhh<zzhg, zzhg> {
    final void zzb(Object obj) {
        ((zzeo) obj).zzb.zzb();
    }

    final /* synthetic */ int zzd(Object obj) {
        return ((zzhg) obj).zzd();
    }

    final /* synthetic */ int zzc(Object obj) {
        return ((zzhg) obj).zzc();
    }

    final /* synthetic */ Object zzb(Object obj, Object obj2) {
        zzhg zzhg = (zzhg) obj;
        zzhg zzhg2 = (zzhg) obj2;
        if (zzhg2.equals(zzhg.zza())) {
            return zzhg;
        }
        return zzhg.zza(zzhg, zzhg2);
    }

    final /* synthetic */ void zzb(Object obj, zzib zzib) throws IOException {
        ((zzhg) obj).zza(zzib);
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzhh
    public final /* synthetic */ void zza(Object obj, zzib zzib) throws IOException {
        ((zzhg) obj).zzb(zzib);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzhh
    public final /* synthetic */ zzhg zza(Object obj) {
        return ((zzeo) obj).zzb;
    }

    final /* synthetic */ void zza(Object obj, Object obj2) {
        ((zzeo) obj).zzb = (zzhg) obj2;
    }
}
