package com.google.android.gms.internal.clearcut;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface zzeg {
    <T> zzef<T> zzd(Class<T> cls);
}
