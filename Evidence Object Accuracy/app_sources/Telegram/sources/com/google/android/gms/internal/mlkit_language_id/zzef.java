package com.google.android.gms.internal.mlkit_language_id;

import java.util.Collections;
import java.util.HashMap;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public class zzef {
    private static volatile zzef zzc;
    private static final zzef zzd = new zzef(true);

    public static zzef zza() {
        zzef zzef = zzc;
        if (zzef == null) {
            synchronized (zzef.class) {
                zzef = zzc;
                if (zzef == null) {
                    zzef = zzd;
                    zzc = zzef;
                }
            }
        }
        return zzef;
    }

    zzef() {
        new HashMap();
    }

    private zzef(boolean z) {
        Collections.emptyMap();
    }
}
