package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.telegram.messenger.R;
import org.telegram.messenger.voip.VoIPService;
import org.telegram.tgnet.ConnectionsManager;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzko<T> implements zzlc<T> {
    private static final int[] zza = new int[0];
    private static final Unsafe zzb = zzma.zzc();
    private final int[] zzc;
    private final Object[] zzd;
    private final int zze;
    private final int zzf;
    private final zzkk zzg;
    private final boolean zzh;
    private final boolean zzj;
    private final int[] zzl;
    private final int zzm;
    private final int zzn;
    private final zzks zzo;
    private final zzju zzp;
    private final zzlu<?, ?> zzq;
    private final zziq<?> zzr;
    private final zzkh zzs;

    private zzko(int[] iArr, Object[] objArr, int i, int i2, zzkk zzkk, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzks zzks, zzju zzju, zzlu<?, ?> zzlu, zziq<?> zziq, zzkh zzkh) {
        this.zzc = iArr;
        this.zzd = objArr;
        this.zze = i;
        this.zzf = i2;
        boolean z3 = zzkk instanceof zzjb;
        this.zzj = z;
        this.zzh = zziq != null && zziq.zza(zzkk);
        this.zzl = iArr2;
        this.zzm = i3;
        this.zzn = i4;
        this.zzo = zzks;
        this.zzp = zzju;
        this.zzq = zzlu;
        this.zzr = zziq;
        this.zzg = zzkk;
        this.zzs = zzkh;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x033a  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x039c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> com.google.android.gms.internal.vision.zzko<T> zza(java.lang.Class<T> r33, com.google.android.gms.internal.vision.zzki r34, com.google.android.gms.internal.vision.zzks r35, com.google.android.gms.internal.vision.zzju r36, com.google.android.gms.internal.vision.zzlu<?, ?> r37, com.google.android.gms.internal.vision.zziq<?> r38, com.google.android.gms.internal.vision.zzkh r39) {
        /*
        // Method dump skipped, instructions count: 1054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzko.zza(java.lang.Class, com.google.android.gms.internal.vision.zzki, com.google.android.gms.internal.vision.zzks, com.google.android.gms.internal.vision.zzju, com.google.android.gms.internal.vision.zzlu, com.google.android.gms.internal.vision.zziq, com.google.android.gms.internal.vision.zzkh):com.google.android.gms.internal.vision.zzko");
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + name.length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @Override // com.google.android.gms.internal.vision.zzlc
    public final T zza() {
        return (T) this.zzo.zza(this.zzg);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0038, code lost:
        if (com.google.android.gms.internal.vision.zzle.zza(com.google.android.gms.internal.vision.zzma.zzf(r10, r6), com.google.android.gms.internal.vision.zzma.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006a, code lost:
        if (com.google.android.gms.internal.vision.zzle.zza(com.google.android.gms.internal.vision.zzma.zzf(r10, r6), com.google.android.gms.internal.vision.zzma.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007e, code lost:
        if (com.google.android.gms.internal.vision.zzma.zzb(r10, r6) == com.google.android.gms.internal.vision.zzma.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0090, code lost:
        if (com.google.android.gms.internal.vision.zzma.zza(r10, r6) == com.google.android.gms.internal.vision.zzma.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a4, code lost:
        if (com.google.android.gms.internal.vision.zzma.zzb(r10, r6) == com.google.android.gms.internal.vision.zzma.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b6, code lost:
        if (com.google.android.gms.internal.vision.zzma.zza(r10, r6) == com.google.android.gms.internal.vision.zzma.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c8, code lost:
        if (com.google.android.gms.internal.vision.zzma.zza(r10, r6) == com.google.android.gms.internal.vision.zzma.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00da, code lost:
        if (com.google.android.gms.internal.vision.zzma.zza(r10, r6) == com.google.android.gms.internal.vision.zzma.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f0, code lost:
        if (com.google.android.gms.internal.vision.zzle.zza(com.google.android.gms.internal.vision.zzma.zzf(r10, r6), com.google.android.gms.internal.vision.zzma.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0106, code lost:
        if (com.google.android.gms.internal.vision.zzle.zza(com.google.android.gms.internal.vision.zzma.zzf(r10, r6), com.google.android.gms.internal.vision.zzma.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011c, code lost:
        if (com.google.android.gms.internal.vision.zzle.zza(com.google.android.gms.internal.vision.zzma.zzf(r10, r6), com.google.android.gms.internal.vision.zzma.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x012e, code lost:
        if (com.google.android.gms.internal.vision.zzma.zzc(r10, r6) == com.google.android.gms.internal.vision.zzma.zzc(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0140, code lost:
        if (com.google.android.gms.internal.vision.zzma.zza(r10, r6) == com.google.android.gms.internal.vision.zzma.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0154, code lost:
        if (com.google.android.gms.internal.vision.zzma.zzb(r10, r6) == com.google.android.gms.internal.vision.zzma.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0165, code lost:
        if (com.google.android.gms.internal.vision.zzma.zza(r10, r6) == com.google.android.gms.internal.vision.zzma.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0178, code lost:
        if (com.google.android.gms.internal.vision.zzma.zzb(r10, r6) == com.google.android.gms.internal.vision.zzma.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x018b, code lost:
        if (com.google.android.gms.internal.vision.zzma.zzb(r10, r6) == com.google.android.gms.internal.vision.zzma.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.vision.zzma.zzd(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.vision.zzma.zzd(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.vision.zzma.zze(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.vision.zzma.zze(r11, r6))) goto L_0x01c2;
     */
    @Override // com.google.android.gms.internal.vision.zzlc
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(T r10, T r11) {
        /*
        // Method dump skipped, instructions count: 640
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzko.zza(java.lang.Object, java.lang.Object):boolean");
    }

    @Override // com.google.android.gms.internal.vision.zzlc
    public final int zza(T t) {
        int i;
        int i2;
        int length = this.zzc.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int zzd = zzd(i4);
            int i5 = this.zzc[i4];
            long j = (long) (1048575 & zzd);
            int i6 = 37;
            switch ((zzd & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = zzjf.zza(Double.doubleToLongBits(zzma.zze(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(zzma.zzd(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = zzjf.zza(zzma.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = zzjf.zza(zzma.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = zzma.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = zzjf.zza(zzma.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = zzma.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = zzjf.zza(zzma.zzc(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) zzma.zzf(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object zzf = zzma.zzf(t, j);
                    if (zzf != null) {
                        i6 = zzf.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = zzma.zzf(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = zzma.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = zzma.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = zzma.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = zzjf.zza(zzma.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = zzma.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = zzjf.zza(zzma.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object zzf2 = zzma.zzf(t, j);
                    if (zzf2 != null) {
                        i6 = zzf2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                case R.styleable.MapAttrs_uiTiltGestures /* 19 */:
                case R.styleable.MapAttrs_uiZoomControls /* 20 */:
                case R.styleable.MapAttrs_uiZoomGestures /* 21 */:
                case R.styleable.MapAttrs_useViewLifecycle /* 22 */:
                case R.styleable.MapAttrs_zOrderOnTop /* 23 */:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case ConnectionsManager.RequestFlagForceDownload /* 32 */:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = zzma.zzf(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = zzma.zzf(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(Double.doubleToLongBits(zzb(t, j)));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 52:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(zzc(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 53:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 54:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 55:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 56:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 57:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 58:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(zzf(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 59:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = ((String) zzma.zzf(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 60:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzma.zzf(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 61:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzma.zzf(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 62:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 63:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 64:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case VoIPService.CALL_MIN_LAYER /* 65 */:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 66:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 67:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzjf.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
                case 68:
                    if (zza((zzko<T>) t, i5, i4)) {
                        i2 = i3 * 53;
                        i = zzma.zzf(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    } else {
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.zzq.zzb(t).hashCode();
        return this.zzh ? (hashCode * 53) + this.zzr.zza(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.vision.zzlc
    public final void zzb(T t, T t2) {
        t2.getClass();
        for (int i = 0; i < this.zzc.length; i += 3) {
            int zzd = zzd(i);
            long j = (long) (1048575 & zzd);
            int i2 = this.zzc[i];
            switch ((zzd & 267386880) >>> 20) {
                case 0:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza(t, j, zzma.zze(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 1:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zzd(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 2:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zzb(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 3:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zzb(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zza(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 5:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zzb(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 6:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zza(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 7:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza(t, j, zzma.zzc(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 8:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza(t, j, zzma.zzf(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 9:
                    zza(t, t2, i);
                    break;
                case 10:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza(t, j, zzma.zzf(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 11:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zza(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 12:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zza(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 13:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zza(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 14:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zzb(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 15:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zza(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 16:
                    if (zza((zzko<T>) t2, i)) {
                        zzma.zza((Object) t, j, zzma.zzb(t2, j));
                        zzb((zzko<T>) t, i);
                        break;
                    } else {
                        break;
                    }
                case 17:
                    zza(t, t2, i);
                    break;
                case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                case R.styleable.MapAttrs_uiTiltGestures /* 19 */:
                case R.styleable.MapAttrs_uiZoomControls /* 20 */:
                case R.styleable.MapAttrs_uiZoomGestures /* 21 */:
                case R.styleable.MapAttrs_useViewLifecycle /* 22 */:
                case R.styleable.MapAttrs_zOrderOnTop /* 23 */:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case ConnectionsManager.RequestFlagForceDownload /* 32 */:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.zzp.zza(t, t2, j);
                    break;
                case 50:
                    zzle.zza(this.zzs, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (zza((zzko<T>) t2, i2, i)) {
                        zzma.zza(t, j, zzma.zzf(t2, j));
                        zzb((zzko<T>) t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 60:
                    zzb(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case VoIPService.CALL_MIN_LAYER /* 65 */:
                case 66:
                case 67:
                    if (zza((zzko<T>) t2, i2, i)) {
                        zzma.zza(t, j, zzma.zzf(t2, j));
                        zzb((zzko<T>) t, i2, i);
                        break;
                    } else {
                        break;
                    }
                case 68:
                    zzb(t, t2, i);
                    break;
            }
        }
        zzle.zza(this.zzq, t, t2);
        if (this.zzh) {
            zzle.zza(this.zzr, t, t2);
        }
    }

    private final void zza(T t, T t2, int i) {
        long zzd = (long) (zzd(i) & 1048575);
        if (zza((zzko<T>) t2, i)) {
            Object zzf = zzma.zzf(t, zzd);
            Object zzf2 = zzma.zzf(t2, zzd);
            if (zzf != null && zzf2 != null) {
                zzma.zza(t, zzd, zzjf.zza(zzf, zzf2));
                zzb((zzko<T>) t, i);
            } else if (zzf2 != null) {
                zzma.zza(t, zzd, zzf2);
                zzb((zzko<T>) t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzd = zzd(i);
        int i2 = this.zzc[i];
        long j = (long) (zzd & 1048575);
        if (zza((zzko<T>) t2, i2, i)) {
            Object obj = null;
            if (zza((zzko<T>) t, i2, i)) {
                obj = zzma.zzf(t, j);
            }
            Object zzf = zzma.zzf(t2, j);
            if (obj != null && zzf != null) {
                zzma.zza(t, j, zzjf.zza(obj, zzf));
                zzb((zzko<T>) t, i2, i);
            } else if (zzf != null) {
                zzma.zza(t, j, zzf);
                zzb((zzko<T>) t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.google.android.gms.internal.vision.zzlc
    public final int zzb(T t) {
        int i;
        long j;
        int i2;
        int zzb2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int zzb3;
        int i8;
        int i9;
        int i10;
        int i11 = 267386880;
        int i12 = 1048575;
        int i13 = 1;
        if (this.zzj) {
            Unsafe unsafe = zzb;
            int i14 = 0;
            int i15 = 0;
            while (i14 < this.zzc.length) {
                int zzd = zzd(i14);
                int i16 = (zzd & i11) >>> 20;
                int i17 = this.zzc[i14];
                long j2 = (long) (zzd & 1048575);
                if (i16 >= zziv.DOUBLE_LIST_PACKED.zza() && i16 <= zziv.SINT64_LIST_PACKED.zza()) {
                    int i18 = this.zzc[i14 + 2];
                }
                switch (i16) {
                    case 0:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzb(i17, 0.0d);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 1:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzb(i17, 0.0f);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 2:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzd(i17, zzma.zzb(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 3:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zze(i17, zzma.zzb(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 4:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzf(i17, zzma.zza(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 5:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzg(i17, 0L);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 6:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzi(i17, 0);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 7:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzb(i17, true);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 8:
                        if (zza((zzko<T>) t, i14)) {
                            Object zzf = zzma.zzf(t, j2);
                            if (zzf instanceof zzht) {
                                zzb3 = zzii.zzc(i17, (zzht) zzf);
                                break;
                            } else {
                                zzb3 = zzii.zzb(i17, (String) zzf);
                                break;
                            }
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 9:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzle.zza(i17, zzma.zzf(t, j2), zza(i14));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 10:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzc(i17, (zzht) zzma.zzf(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 11:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzg(i17, zzma.zza(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 12:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzk(i17, zzma.zza(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 13:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzj(i17, 0);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 14:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzh(i17, 0L);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 15:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzh(i17, zzma.zza(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 16:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzf(i17, zzma.zzb(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 17:
                        if (zza((zzko<T>) t, i14)) {
                            zzb3 = zzii.zzc(i17, (zzkk) zzma.zzf(t, j2), zza(i14));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                        zzb3 = zzle.zzi(i17, zza(t, j2), false);
                        break;
                    case R.styleable.MapAttrs_uiTiltGestures /* 19 */:
                        zzb3 = zzle.zzh(i17, zza(t, j2), false);
                        break;
                    case R.styleable.MapAttrs_uiZoomControls /* 20 */:
                        zzb3 = zzle.zza(i17, (List<Long>) zza(t, j2), false);
                        break;
                    case R.styleable.MapAttrs_uiZoomGestures /* 21 */:
                        zzb3 = zzle.zzb(i17, (List<Long>) zza(t, j2), false);
                        break;
                    case R.styleable.MapAttrs_useViewLifecycle /* 22 */:
                        zzb3 = zzle.zze(i17, zza(t, j2), false);
                        break;
                    case R.styleable.MapAttrs_zOrderOnTop /* 23 */:
                        zzb3 = zzle.zzi(i17, zza(t, j2), false);
                        break;
                    case 24:
                        zzb3 = zzle.zzh(i17, zza(t, j2), false);
                        break;
                    case 25:
                        zzb3 = zzle.zzj(i17, zza(t, j2), false);
                        break;
                    case 26:
                        zzb3 = zzle.zza(i17, zza(t, j2));
                        break;
                    case 27:
                        zzb3 = zzle.zza(i17, zza(t, j2), zza(i14));
                        break;
                    case 28:
                        zzb3 = zzle.zzb(i17, zza(t, j2));
                        break;
                    case 29:
                        zzb3 = zzle.zzf(i17, zza(t, j2), false);
                        break;
                    case 30:
                        zzb3 = zzle.zzd(i17, zza(t, j2), false);
                        break;
                    case 31:
                        zzb3 = zzle.zzh(i17, zza(t, j2), false);
                        break;
                    case ConnectionsManager.RequestFlagForceDownload /* 32 */:
                        zzb3 = zzle.zzi(i17, zza(t, j2), false);
                        break;
                    case 33:
                        zzb3 = zzle.zzg(i17, zza(t, j2), false);
                        break;
                    case 34:
                        zzb3 = zzle.zzc(i17, zza(t, j2), false);
                        break;
                    case 35:
                        i9 = zzle.zzi((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 36:
                        i9 = zzle.zzh((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 37:
                        i9 = zzle.zza((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 38:
                        i9 = zzle.zzb((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 39:
                        i9 = zzle.zze((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 40:
                        i9 = zzle.zzi((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 41:
                        i9 = zzle.zzh((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 42:
                        i9 = zzle.zzj((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 43:
                        i9 = zzle.zzf((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 44:
                        i9 = zzle.zzd((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 45:
                        i9 = zzle.zzh((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 46:
                        i9 = zzle.zzi((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 47:
                        i9 = zzle.zzg((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 48:
                        i9 = zzle.zzc((List) unsafe.getObject(t, j2));
                        if (i9 > 0) {
                            i10 = zzii.zze(i17);
                            i8 = zzii.zzg(i9);
                            zzb3 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 49:
                        zzb3 = zzle.zzb(i17, (List<zzkk>) zza(t, j2), zza(i14));
                        break;
                    case 50:
                        zzb3 = this.zzs.zza(i17, zzma.zzf(t, j2), zzb(i14));
                        break;
                    case 51:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzb(i17, 0.0d);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 52:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzb(i17, 0.0f);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 53:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzd(i17, zze(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 54:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zze(i17, zze(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 55:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzf(i17, zzd(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 56:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzg(i17, 0L);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 57:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzi(i17, 0);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 58:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzb(i17, true);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 59:
                        if (zza((zzko<T>) t, i17, i14)) {
                            Object zzf2 = zzma.zzf(t, j2);
                            if (zzf2 instanceof zzht) {
                                zzb3 = zzii.zzc(i17, (zzht) zzf2);
                                break;
                            } else {
                                zzb3 = zzii.zzb(i17, (String) zzf2);
                                break;
                            }
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 60:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzle.zza(i17, zzma.zzf(t, j2), zza(i14));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 61:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzc(i17, (zzht) zzma.zzf(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 62:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzg(i17, zzd(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 63:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzk(i17, zzd(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 64:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzj(i17, 0);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case VoIPService.CALL_MIN_LAYER /* 65 */:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzh(i17, 0L);
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 66:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzh(i17, zzd(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 67:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzf(i17, zze(t, j2));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    case 68:
                        if (zza((zzko<T>) t, i17, i14)) {
                            zzb3 = zzii.zzc(i17, (zzkk) zzma.zzf(t, j2), zza(i14));
                            break;
                        } else {
                            continue;
                            i14 += 3;
                            i11 = 267386880;
                        }
                    default:
                        i14 += 3;
                        i11 = 267386880;
                }
                i15 += zzb3;
                i14 += 3;
                i11 = 267386880;
            }
            return i15 + zza((zzlu) this.zzq, (Object) t);
        }
        Unsafe unsafe2 = zzb;
        int i19 = 0;
        int i20 = 0;
        int i21 = 1048575;
        int i22 = 0;
        while (i19 < this.zzc.length) {
            int zzd2 = zzd(i19);
            int[] iArr = this.zzc;
            int i23 = iArr[i19];
            int i24 = (zzd2 & 267386880) >>> 20;
            if (i24 <= 17) {
                int i25 = iArr[i19 + 2];
                int i26 = i25 & i12;
                i = i13 << (i25 >>> 20);
                if (i26 != i21) {
                    i22 = unsafe2.getInt(t, (long) i26);
                    i21 = i26;
                }
            } else {
                i = 0;
            }
            long j3 = (long) (zzd2 & i12);
            switch (i24) {
                case 0:
                    j = 0;
                    if ((i22 & i) != 0) {
                        i20 += zzii.zzb(i23, 0.0d);
                        continue;
                        i19 += 3;
                        i12 = 1048575;
                        i13 = 1;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i22 & i) != 0) {
                        i20 += zzii.zzb(i23, 0.0f);
                        break;
                    }
                    break;
                case 2:
                    j = 0;
                    if ((i & i22) != 0) {
                        i2 = zzii.zzd(i23, unsafe2.getLong(t, j3));
                        i20 += i2;
                        break;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i & i22) != 0) {
                        i2 = zzii.zze(i23, unsafe2.getLong(t, j3));
                        i20 += i2;
                        break;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i & i22) != 0) {
                        i2 = zzii.zzf(i23, unsafe2.getInt(t, j3));
                        i20 += i2;
                        break;
                    }
                    break;
                case 5:
                    j = 0;
                    if ((i22 & i) != 0) {
                        i2 = zzii.zzg(i23, 0L);
                        i20 += i2;
                        break;
                    }
                    break;
                case 6:
                    if ((i22 & i) != 0) {
                        i20 += zzii.zzi(i23, 0);
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 7:
                    if ((i22 & i) != 0) {
                        i20 += zzii.zzb(i23, true);
                        j = 0;
                        i19 += 3;
                        i12 = 1048575;
                        i13 = 1;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 8:
                    if ((i22 & i) != 0) {
                        Object object = unsafe2.getObject(t, j3);
                        if (object instanceof zzht) {
                            zzb2 = zzii.zzc(i23, (zzht) object);
                        } else {
                            zzb2 = zzii.zzb(i23, (String) object);
                        }
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 9:
                    if ((i22 & i) != 0) {
                        zzb2 = zzle.zza(i23, unsafe2.getObject(t, j3), zza(i19));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 10:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzc(i23, (zzht) unsafe2.getObject(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 11:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzg(i23, unsafe2.getInt(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 12:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzk(i23, unsafe2.getInt(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 13:
                    if ((i22 & i) != 0) {
                        i3 = zzii.zzj(i23, 0);
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 14:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzh(i23, 0L);
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 15:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzh(i23, unsafe2.getInt(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 16:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzf(i23, unsafe2.getLong(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 17:
                    if ((i22 & i) != 0) {
                        zzb2 = zzii.zzc(i23, (zzkk) unsafe2.getObject(t, j3), zza(i19));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                    zzb2 = zzle.zzi(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case R.styleable.MapAttrs_uiTiltGestures /* 19 */:
                    i4 = zzle.zzh(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case R.styleable.MapAttrs_uiZoomControls /* 20 */:
                    i4 = zzle.zza(i23, (List<Long>) ((List) unsafe2.getObject(t, j3)), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case R.styleable.MapAttrs_uiZoomGestures /* 21 */:
                    i4 = zzle.zzb(i23, (List<Long>) ((List) unsafe2.getObject(t, j3)), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case R.styleable.MapAttrs_useViewLifecycle /* 22 */:
                    i4 = zzle.zze(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case R.styleable.MapAttrs_zOrderOnTop /* 23 */:
                    i4 = zzle.zzi(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 24:
                    i4 = zzle.zzh(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 25:
                    i4 = zzle.zzj(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 26:
                    zzb2 = zzle.zza(i23, (List) unsafe2.getObject(t, j3));
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 27:
                    zzb2 = zzle.zza(i23, (List<?>) ((List) unsafe2.getObject(t, j3)), zza(i19));
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 28:
                    zzb2 = zzle.zzb(i23, (List) unsafe2.getObject(t, j3));
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 29:
                    zzb2 = zzle.zzf(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 30:
                    i4 = zzle.zzd(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 31:
                    i4 = zzle.zzh(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case ConnectionsManager.RequestFlagForceDownload /* 32 */:
                    i4 = zzle.zzi(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 33:
                    i4 = zzle.zzg(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 34:
                    i4 = zzle.zzc(i23, (List) unsafe2.getObject(t, j3), false);
                    i20 += i4;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 35:
                    i7 = zzle.zzi((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 36:
                    i7 = zzle.zzh((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 37:
                    i7 = zzle.zza((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 38:
                    i7 = zzle.zzb((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 39:
                    i7 = zzle.zze((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 40:
                    i7 = zzle.zzi((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 41:
                    i7 = zzle.zzh((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 42:
                    i7 = zzle.zzj((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 43:
                    i7 = zzle.zzf((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 44:
                    i7 = zzle.zzd((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 45:
                    i7 = zzle.zzh((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 46:
                    i7 = zzle.zzi((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 47:
                    i7 = zzle.zzg((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 48:
                    i7 = zzle.zzc((List) unsafe2.getObject(t, j3));
                    if (i7 > 0) {
                        i6 = zzii.zze(i23);
                        i5 = zzii.zzg(i7);
                        i3 = i6 + i5 + i7;
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 49:
                    zzb2 = zzle.zzb(i23, (List) unsafe2.getObject(t, j3), zza(i19));
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 50:
                    zzb2 = this.zzs.zza(i23, unsafe2.getObject(t, j3), zzb(i19));
                    i20 += zzb2;
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 51:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzb(i23, 0.0d);
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 52:
                    if (zza((zzko<T>) t, i23, i19)) {
                        i3 = zzii.zzb(i23, 0.0f);
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 53:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzd(i23, zze(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 54:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zze(i23, zze(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 55:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzf(i23, zzd(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 56:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzg(i23, 0L);
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 57:
                    if (zza((zzko<T>) t, i23, i19)) {
                        i3 = zzii.zzi(i23, 0);
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 58:
                    if (zza((zzko<T>) t, i23, i19)) {
                        i3 = zzii.zzb(i23, true);
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 59:
                    if (zza((zzko<T>) t, i23, i19)) {
                        Object object2 = unsafe2.getObject(t, j3);
                        if (object2 instanceof zzht) {
                            zzb2 = zzii.zzc(i23, (zzht) object2);
                        } else {
                            zzb2 = zzii.zzb(i23, (String) object2);
                        }
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 60:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzle.zza(i23, unsafe2.getObject(t, j3), zza(i19));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 61:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzc(i23, (zzht) unsafe2.getObject(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 62:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzg(i23, zzd(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 63:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzk(i23, zzd(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 64:
                    if (zza((zzko<T>) t, i23, i19)) {
                        i3 = zzii.zzj(i23, 0);
                        i20 += i3;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case VoIPService.CALL_MIN_LAYER /* 65 */:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzh(i23, 0L);
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 66:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzh(i23, zzd(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 67:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzf(i23, zze(t, j3));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                case 68:
                    if (zza((zzko<T>) t, i23, i19)) {
                        zzb2 = zzii.zzc(i23, (zzkk) unsafe2.getObject(t, j3), zza(i19));
                        i20 += zzb2;
                    }
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
                default:
                    j = 0;
                    i19 += 3;
                    i12 = 1048575;
                    i13 = 1;
            }
            i19 += 3;
            i12 = 1048575;
            i13 = 1;
        }
        int i27 = 0;
        int zza2 = i20 + zza((zzlu) this.zzq, (Object) t);
        if (!this.zzh) {
            return zza2;
        }
        zziu<?> zza3 = this.zzr.zza(t);
        for (int i28 = 0; i28 < zza3.zza.zzc(); i28++) {
            Map.Entry<?, Object> zzb4 = zza3.zza.zzb(i28);
            i27 += zziu.zzc((zziw) zzb4.getKey(), zzb4.getValue());
        }
        for (Map.Entry<?, Object> entry : zza3.zza.zzd()) {
            i27 += zziu.zzc((zziw) entry.getKey(), entry.getValue());
        }
        return zza2 + i27;
    }

    private static <UT, UB> int zza(zzlu<UT, UB> zzlu, T t) {
        return zzlu.zzf(zzlu.zzb(t));
    }

    private static List<?> zza(Object obj, long j) {
        return (List) zzma.zzf(obj, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0552  */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x0a2a  */
    @Override // com.google.android.gms.internal.vision.zzlc
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r14, com.google.android.gms.internal.vision.zzmr r15) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 2916
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzko.zza(java.lang.Object, com.google.android.gms.internal.vision.zzmr):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0491  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(T r18, com.google.android.gms.internal.vision.zzmr r19) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzko.zzb(java.lang.Object, com.google.android.gms.internal.vision.zzmr):void");
    }

    private final <K, V> void zza(zzmr zzmr, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzmr.zza(i, this.zzs.zzb(zzb(i2)), this.zzs.zzc(obj));
        }
    }

    private static <UT, UB> void zza(zzlu<UT, UB> zzlu, T t, zzmr zzmr) throws IOException {
        zzlu.zza((zzlu<UT, UB>) zzlu.zzb(t), zzmr);
    }

    private static zzlx zze(Object obj) {
        zzjb zzjb = (zzjb) obj;
        zzlx zzlx = zzjb.zzb;
        if (zzlx != zzlx.zza()) {
            return zzlx;
        }
        zzlx zzb2 = zzlx.zzb();
        zzjb.zzb = zzb2;
        return zzb2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:114:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int zza(T r16, byte[] r17, int r18, int r19, int r20, int r21, int r22, int r23, long r24, int r26, long r27, com.google.android.gms.internal.vision.zzhn r29) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1126
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzko.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.vision.zzhn):int");
    }

    private final <K, V> int zza(T t, byte[] bArr, int i, int i2, int i3, long j, zzhn zzhn) throws IOException {
        Unsafe unsafe = zzb;
        Object zzb2 = zzb(i3);
        Object object = unsafe.getObject(t, j);
        if (this.zzs.zzd(object)) {
            Object zzf = this.zzs.zzf(zzb2);
            this.zzs.zza(zzf, object);
            unsafe.putObject(t, j, zzf);
            object = zzf;
        }
        this.zzs.zzb(zzb2);
        this.zzs.zza(object);
        int zza2 = zzhl.zza(bArr, i, zzhn);
        int i4 = zzhn.zza;
        if (i4 < 0 || i4 > i2 - zza2) {
            throw zzjk.zza();
        }
        throw null;
    }

    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzhn zzhn) throws IOException {
        int i9;
        Unsafe unsafe = zzb;
        long j2 = (long) (this.zzc[i8 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Double.valueOf(zzhl.zzc(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Float.valueOf(zzhl.zzd(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    i9 = zzhl.zzb(bArr, i, zzhn);
                    unsafe.putObject(t, j, Long.valueOf(zzhn.zzb));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    i9 = zzhl.zza(bArr, i, zzhn);
                    unsafe.putObject(t, j, Integer.valueOf(zzhn.zza));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 56:
            case VoIPService.CALL_MIN_LAYER /* 65 */:
                if (i5 == 1) {
                    unsafe.putObject(t, j, Long.valueOf(zzhl.zzb(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(t, j, Integer.valueOf(zzhl.zza(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    i9 = zzhl.zzb(bArr, i, zzhn);
                    unsafe.putObject(t, j, Boolean.valueOf(zzhn.zzb != 0));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    int zza2 = zzhl.zza(bArr, i, zzhn);
                    int i10 = zzhn.zza;
                    if (i10 == 0) {
                        unsafe.putObject(t, j, "");
                    } else if ((i6 & 536870912) == 0 || zzmd.zza(bArr, zza2, zza2 + i10)) {
                        unsafe.putObject(t, j, new String(bArr, zza2, i10, zzjf.zza));
                        zza2 += i10;
                    } else {
                        throw zzjk.zzh();
                    }
                    unsafe.putInt(t, j2, i4);
                    return zza2;
                }
                return i;
            case 60:
                if (i5 == 2) {
                    int zza3 = zzhl.zza(zza(i8), bArr, i, i2, zzhn);
                    Object object = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object == null) {
                        unsafe.putObject(t, j, zzhn.zzc);
                    } else {
                        unsafe.putObject(t, j, zzjf.zza(object, zzhn.zzc));
                    }
                    unsafe.putInt(t, j2, i4);
                    return zza3;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    i9 = zzhl.zze(bArr, i, zzhn);
                    unsafe.putObject(t, j, zzhn.zzc);
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    int zza4 = zzhl.zza(bArr, i, zzhn);
                    int i11 = zzhn.zza;
                    zzjg zzc = zzc(i8);
                    if (zzc == null || zzc.zza(i11)) {
                        unsafe.putObject(t, j, Integer.valueOf(i11));
                        i9 = zza4;
                        unsafe.putInt(t, j2, i4);
                        return i9;
                    }
                    zze(t).zza(i3, Long.valueOf((long) i11));
                    return zza4;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    i9 = zzhl.zza(bArr, i, zzhn);
                    unsafe.putObject(t, j, Integer.valueOf(zzif.zze(zzhn.zza)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    i9 = zzhl.zzb(bArr, i, zzhn);
                    unsafe.putObject(t, j, Long.valueOf(zzif.zza(zzhn.zzb)));
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    i9 = zzhl.zza(zza(i8), bArr, i, i2, (i3 & -8) | 4, zzhn);
                    Object object2 = unsafe.getInt(t, j2) == i4 ? unsafe.getObject(t, j) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j, zzhn.zzc);
                    } else {
                        unsafe.putObject(t, j, zzjf.zza(object2, zzhn.zzc));
                    }
                    unsafe.putInt(t, j2, i4);
                    return i9;
                }
                return i;
            default:
                return i;
        }
    }

    private final zzlc zza(int i) {
        int i2 = (i / 3) << 1;
        zzlc zzlc = (zzlc) this.zzd[i2];
        if (zzlc != null) {
            return zzlc;
        }
        zzlc<T> zza2 = zzky.zza().zza((Class) ((Class) this.zzd[i2 + 1]));
        this.zzd[i2] = zza2;
        return zza2;
    }

    private final Object zzb(int i) {
        return this.zzd[(i / 3) << 1];
    }

    private final zzjg zzc(int i) {
        return (zzjg) this.zzd[((i / 3) << 1) + 1];
    }

    /* JADX DEBUG: Type inference failed for r5v3. Raw type applied. Possible types: com.google.android.gms.internal.vision.zzlu<?, ?>, com.google.android.gms.internal.vision.zzlu<UT, UB> */
    /* access modifiers changed from: package-private */
    public final int zza(T t, byte[] bArr, int i, int i2, int i3, zzhn zzhn) throws IOException {
        Unsafe unsafe;
        int i4;
        int i5;
        T t2;
        zzko<T> zzko;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        boolean z;
        int i15;
        int i16;
        T t3;
        byte[] bArr2;
        zzhn zzhn2;
        int i17;
        Object obj;
        Object zza2;
        long j;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        T t4;
        int i24;
        int i25;
        zzko<T> zzko2 = this;
        T t5 = t;
        byte[] bArr3 = bArr;
        int i26 = i2;
        int i27 = i3;
        zzhn zzhn3 = zzhn;
        Unsafe unsafe2 = zzb;
        int i28 = i;
        int i29 = -1;
        int i30 = 0;
        int i31 = 0;
        int i32 = 0;
        int i33 = 1048575;
        while (true) {
            Object obj2 = null;
            if (i28 < i26) {
                int i34 = i28 + 1;
                byte b = bArr3[i28];
                if (b < 0) {
                    int zza3 = zzhl.zza(b, bArr3, i34, zzhn3);
                    i8 = zzhn3.zza;
                    i34 = zza3;
                } else {
                    i8 = b;
                }
                int i35 = i8 >>> 3;
                int i36 = i8 & 7;
                if (i35 > i29) {
                    i9 = zzko2.zza(i35, i30 / 3);
                } else {
                    i9 = zzko2.zzg(i35);
                }
                if (i9 == -1) {
                    i10 = i35;
                    i11 = i34;
                    i12 = i8;
                    i13 = i32;
                    unsafe = unsafe2;
                    i14 = i27;
                    z = true;
                    i15 = 0;
                } else {
                    int[] iArr = zzko2.zzc;
                    int i37 = iArr[i9 + 1];
                    int i38 = (i37 & 267386880) >>> 20;
                    long j2 = (long) (i37 & 1048575);
                    if (i38 <= 17) {
                        int i39 = iArr[i9 + 2];
                        int i40 = 1 << (i39 >>> 20);
                        int i41 = i39 & 1048575;
                        if (i41 != i33) {
                            if (i33 != 1048575) {
                                long j3 = (long) i33;
                                t4 = t;
                                j = j2;
                                unsafe2.putInt(t4, j3, i32);
                            } else {
                                t4 = t;
                                j = j2;
                            }
                            i32 = unsafe2.getInt(t4, (long) i41);
                            t5 = t4;
                        } else {
                            t5 = t;
                            j = j2;
                            i41 = i33;
                        }
                        switch (i38) {
                            case 0:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 1) {
                                    zzma.zza(t5, j, zzhl.zzc(bArr3, i34));
                                    i28 = i34 + 8;
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 1:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 5) {
                                    zzma.zza((Object) t5, j, zzhl.zzd(bArr3, i34));
                                    i28 = i34 + 4;
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 2:
                            case 3:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 0) {
                                    i22 = zzhl.zzb(bArr3, i34, zzhn3);
                                    unsafe2.putLong(t, j, zzhn3.zzb);
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i28 = i22;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 4:
                            case 11:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 0) {
                                    i28 = zzhl.zza(bArr3, i34, zzhn3);
                                    unsafe2.putInt(t5, j, zzhn3.zza);
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 5:
                            case 14:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 1) {
                                    unsafe2.putLong(t, j, zzhl.zzb(bArr3, i34));
                                    i28 = i34 + 8;
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 6:
                            case 13:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 5) {
                                    unsafe2.putInt(t5, j, zzhl.zza(bArr3, i34));
                                    i28 = i34 + 4;
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 7:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 0) {
                                    i28 = zzhl.zzb(bArr3, i34, zzhn3);
                                    zzma.zza(t5, j, zzhn3.zzb != 0);
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 8:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 2) {
                                    if ((536870912 & i37) == 0) {
                                        i28 = zzhl.zzc(bArr3, i34, zzhn3);
                                    } else {
                                        i28 = zzhl.zzd(bArr3, i34, zzhn3);
                                    }
                                    unsafe2.putObject(t5, j, zzhn3.zzc);
                                    i32 = i13 | i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 9:
                                i19 = i35;
                                i20 = i9;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 2) {
                                    int zza4 = zzhl.zza(zzko2.zza(i20), bArr3, i34, i2, zzhn3);
                                    if ((i32 & i40) == 0) {
                                        unsafe2.putObject(t5, j, zzhn3.zzc);
                                    } else {
                                        unsafe2.putObject(t5, j, zzjf.zza(unsafe2.getObject(t5, j), zzhn3.zzc));
                                    }
                                    i32 |= i40;
                                    i33 = i18;
                                    i31 = i21;
                                    i29 = i19;
                                    i26 = i2;
                                    i28 = zza4;
                                    i30 = i20;
                                    i27 = i3;
                                    break;
                                } else {
                                    i13 = i32;
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 10:
                                i19 = i35;
                                i20 = i9;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 2) {
                                    i23 = zzhl.zze(bArr3, i34, zzhn3);
                                    unsafe2.putObject(t5, j, zzhn3.zzc);
                                    i32 |= i40;
                                    i33 = i18;
                                    i28 = i23;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i13 = i32;
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 12:
                                i19 = i35;
                                i20 = i9;
                                i18 = i41;
                                i21 = i8;
                                if (i36 != 0) {
                                    i13 = i32;
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                } else {
                                    i23 = zzhl.zza(bArr3, i34, zzhn3);
                                    int i42 = zzhn3.zza;
                                    zzjg zzc = zzko2.zzc(i20);
                                    if (zzc == null || zzc.zza(i42)) {
                                        unsafe2.putInt(t5, j, i42);
                                        i32 |= i40;
                                        i33 = i18;
                                        i28 = i23;
                                        i31 = i21;
                                        i30 = i20;
                                        i29 = i19;
                                        i26 = i2;
                                        i27 = i3;
                                        break;
                                    } else {
                                        zze(t).zza(i21, Long.valueOf((long) i42));
                                        i28 = i23;
                                        i32 = i32;
                                        i31 = i21;
                                        i30 = i20;
                                        i29 = i19;
                                        i33 = i18;
                                        i26 = i2;
                                        i27 = i3;
                                    }
                                }
                                break;
                            case 15:
                                i19 = i35;
                                i20 = i9;
                                i18 = i41;
                                i21 = i8;
                                if (i36 == 0) {
                                    i23 = zzhl.zza(bArr3, i34, zzhn3);
                                    unsafe2.putInt(t5, j, zzif.zze(zzhn3.zza));
                                    i32 |= i40;
                                    i33 = i18;
                                    i28 = i23;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i13 = i32;
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 16:
                                i19 = i35;
                                i20 = i9;
                                if (i36 == 0) {
                                    i22 = zzhl.zzb(bArr3, i34, zzhn3);
                                    i18 = i41;
                                    i21 = i8;
                                    unsafe2.putLong(t, j, zzif.zza(zzhn3.zzb));
                                    i32 |= i40;
                                    i33 = i18;
                                    i28 = i22;
                                    i31 = i21;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i18 = i41;
                                    i21 = i8;
                                    i13 = i32;
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            case 17:
                                if (i36 == 3) {
                                    i19 = i35;
                                    i20 = i9;
                                    i28 = zzhl.zza(zzko2.zza(i9), bArr, i34, i2, (i35 << 3) | 4, zzhn);
                                    if ((i32 & i40) == 0) {
                                        unsafe2.putObject(t5, j, zzhn3.zzc);
                                    } else {
                                        unsafe2.putObject(t5, j, zzjf.zza(unsafe2.getObject(t5, j), zzhn3.zzc));
                                    }
                                    i32 |= i40;
                                    i31 = i8;
                                    i33 = i41;
                                    i30 = i20;
                                    i29 = i19;
                                    i26 = i2;
                                    i27 = i3;
                                    break;
                                } else {
                                    i19 = i35;
                                    i20 = i9;
                                    i13 = i32;
                                    i18 = i41;
                                    i21 = i8;
                                    i33 = i18;
                                    i14 = i3;
                                    i11 = i34;
                                    i12 = i21;
                                    unsafe = unsafe2;
                                    i15 = i20;
                                    i10 = i19;
                                    z = true;
                                    break;
                                }
                            default:
                                i19 = i35;
                                i20 = i9;
                                i13 = i32;
                                i18 = i41;
                                i21 = i8;
                                i33 = i18;
                                i14 = i3;
                                i11 = i34;
                                i12 = i21;
                                unsafe = unsafe2;
                                i15 = i20;
                                i10 = i19;
                                z = true;
                                break;
                        }
                    } else {
                        i13 = i32;
                        t5 = t;
                        if (i38 != 27) {
                            i15 = i9;
                            if (i38 <= 49) {
                                i25 = i8;
                                z = true;
                                unsafe = unsafe2;
                                i14 = i3;
                                i10 = i35;
                                i28 = zza((zzko<T>) t, bArr, i34, i2, i8, i35, i36, i15, (long) i37, i38, j2, zzhn);
                                if (i28 == i34) {
                                    i11 = i28;
                                } else {
                                    t5 = t;
                                    bArr3 = bArr;
                                    i26 = i2;
                                    zzhn3 = zzhn;
                                    i27 = i14;
                                    i31 = i25;
                                    i33 = i33;
                                    i32 = i13;
                                    i30 = i15;
                                    i29 = i10;
                                    unsafe2 = unsafe;
                                    zzko2 = this;
                                }
                            } else {
                                i14 = i3;
                                i24 = i34;
                                i25 = i8;
                                unsafe = unsafe2;
                                i10 = i35;
                                z = true;
                                if (i38 != 50) {
                                    i28 = zza((zzko<T>) t, bArr, i24, i2, i25, i10, i36, i37, i38, j2, i15, zzhn);
                                    if (i28 != i24) {
                                        t5 = t;
                                        bArr3 = bArr;
                                        i26 = i2;
                                        zzhn3 = zzhn;
                                        i31 = i25;
                                        i27 = i14;
                                        i33 = i33;
                                        i32 = i13;
                                        i30 = i15;
                                        i29 = i10;
                                        unsafe2 = unsafe;
                                        zzko2 = this;
                                    }
                                } else if (i36 == 2) {
                                    i28 = zza((zzko<T>) t, bArr, i24, i2, i15, j2, zzhn);
                                    if (i28 != i24) {
                                        t5 = t;
                                        bArr3 = bArr;
                                        i26 = i2;
                                        zzhn3 = zzhn;
                                        i27 = i14;
                                        i31 = i25;
                                        i33 = i33;
                                        i32 = i13;
                                        i30 = i15;
                                        i29 = i10;
                                        unsafe2 = unsafe;
                                        zzko2 = this;
                                    }
                                } else {
                                    i11 = i24;
                                }
                                i11 = i28;
                            }
                        } else if (i36 == 2) {
                            zzjl zzjl = (zzjl) unsafe2.getObject(t5, j2);
                            if (!zzjl.zza()) {
                                int size = zzjl.size();
                                zzjl = zzjl.zza(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t5, j2, zzjl);
                            }
                            i28 = zzhl.zza(zzko2.zza(i9), i8, bArr, i34, i2, zzjl, zzhn);
                            i27 = i3;
                            i31 = i8;
                            i29 = i35;
                            i33 = i33;
                            i32 = i13;
                            i30 = i9;
                            i26 = i2;
                        } else {
                            i15 = i9;
                            i14 = i3;
                            i24 = i34;
                            i25 = i8;
                            unsafe = unsafe2;
                            i10 = i35;
                            z = true;
                            i11 = i24;
                        }
                        i12 = i25;
                        i33 = i33;
                    }
                }
                if (i12 != i14 || i14 == 0) {
                    if (this.zzh) {
                        zzhn2 = zzhn;
                        if (zzhn2.zzd != zzio.zzb()) {
                            zzjb.zze zza5 = zzhn2.zzd.zza(this.zzg, i10);
                            if (zza5 == null) {
                                i28 = zzhl.zza(i12, bArr, i11, i2, zze(t), zzhn);
                                t3 = t;
                                i16 = i33;
                                i10 = i10;
                                bArr2 = bArr;
                                i17 = i2;
                            } else {
                                t3 = t;
                                zzjb.zzc zzc2 = (zzjb.zzc) t3;
                                zzc2.zza();
                                zziu<zzjb.zzf> zziu = zzc2.zzc;
                                zzjb.zzf zzf = zza5.zzd;
                                boolean z2 = zzf.zzd;
                                zzml zzml = zzf.zzc;
                                if (zzml != zzml.ENUM) {
                                    int[] iArr2 = zzhk.zza;
                                    switch (iArr2[zzml.ordinal()]) {
                                        case 1:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            obj2 = Double.valueOf(zzhl.zzc(bArr2, i11));
                                            i11 += 8;
                                            obj = obj2;
                                            break;
                                        case 2:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            obj2 = Float.valueOf(zzhl.zzd(bArr2, i11));
                                            i11 += 4;
                                            obj = obj2;
                                            break;
                                        case 3:
                                        case 4:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zzb(bArr2, i11, zzhn2);
                                            obj2 = Long.valueOf(zzhn2.zzb);
                                            obj = obj2;
                                            break;
                                        case 5:
                                        case 6:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zza(bArr2, i11, zzhn2);
                                            obj2 = Integer.valueOf(zzhn2.zza);
                                            obj = obj2;
                                            break;
                                        case 7:
                                        case 8:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            obj2 = Long.valueOf(zzhl.zzb(bArr2, i11));
                                            i11 += 8;
                                            obj = obj2;
                                            break;
                                        case 9:
                                        case 10:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            obj2 = Integer.valueOf(zzhl.zza(bArr2, i11));
                                            i11 += 4;
                                            obj = obj2;
                                            break;
                                        case 11:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zzb(bArr2, i11, zzhn2);
                                            if (zzhn2.zzb == 0) {
                                                z = false;
                                            }
                                            obj2 = Boolean.valueOf(z);
                                            obj = obj2;
                                            break;
                                        case 12:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zza(bArr2, i11, zzhn2);
                                            obj2 = Integer.valueOf(zzif.zze(zzhn2.zza));
                                            obj = obj2;
                                            break;
                                        case 13:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zzb(bArr2, i11, zzhn2);
                                            obj2 = Long.valueOf(zzif.zza(zzhn2.zzb));
                                            obj = obj2;
                                            break;
                                        case 14:
                                            throw new IllegalStateException("Shouldn't reach here.");
                                        case 15:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zze(bArr2, i11, zzhn2);
                                            obj = zzhn2.zzc;
                                            break;
                                        case 16:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            i11 = zzhl.zzc(bArr2, i11, zzhn2);
                                            obj = zzhn2.zzc;
                                            break;
                                        case 17:
                                            int i43 = (i10 << 3) | 4;
                                            i16 = i33;
                                            i17 = i2;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i11 = zzhl.zza(zzky.zza().zza((Class) zza5.zzc.getClass()), bArr, i11, i2, i43, zzhn);
                                            obj = zzhn2.zzc;
                                            break;
                                        case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                                            i11 = zzhl.zza(zzky.zza().zza((Class) zza5.zzc.getClass()), bArr, i11, i2, zzhn2);
                                            obj = zzhn2.zzc;
                                            i16 = i33;
                                            i10 = i10;
                                            i17 = i2;
                                            bArr2 = bArr;
                                            break;
                                        default:
                                            i16 = i33;
                                            i10 = i10;
                                            bArr2 = bArr;
                                            i17 = i2;
                                            obj = obj2;
                                            break;
                                    }
                                    zzjb.zzf zzf2 = zza5.zzd;
                                    if (zzf2.zzd) {
                                        zziu.zzb(zzf2, obj);
                                    } else {
                                        int i44 = iArr2[zzf2.zzc.ordinal()];
                                        if ((i44 == 17 || i44 == 18) && (zza2 = zziu.zza((zziu<zzjb.zzf>) zza5.zzd)) != null) {
                                            obj = zzjf.zza(zza2, obj);
                                        }
                                        zziu.zza((zziu<zzjb.zzf>) zza5.zzd, obj);
                                    }
                                    i28 = i11;
                                } else {
                                    zzhl.zza(bArr, i11, zzhn2);
                                    throw null;
                                }
                            }
                            i31 = i12;
                            zzko2 = this;
                            bArr3 = bArr2;
                            t5 = t3;
                            i32 = i13;
                            i30 = i15;
                            i29 = i10;
                            i26 = i17;
                            i27 = i14;
                            zzhn3 = zzhn2;
                            unsafe2 = unsafe;
                            i33 = i16;
                        } else {
                            t3 = t;
                            bArr2 = bArr;
                        }
                    } else {
                        t3 = t;
                        bArr2 = bArr;
                        zzhn2 = zzhn;
                    }
                    i16 = i33;
                    i17 = i2;
                    i28 = zzhl.zza(i12, bArr, i11, i2, zze(t), zzhn);
                    i31 = i12;
                    zzko2 = this;
                    bArr3 = bArr2;
                    t5 = t3;
                    i32 = i13;
                    i30 = i15;
                    i29 = i10;
                    i26 = i17;
                    i27 = i14;
                    zzhn3 = zzhn2;
                    unsafe2 = unsafe;
                    i33 = i16;
                } else {
                    zzko = this;
                    t2 = t;
                    i28 = i11;
                    i6 = i33;
                    i31 = i12;
                    i4 = i14;
                    i32 = i13;
                    i7 = 1048575;
                    i5 = i2;
                }
            } else {
                unsafe = unsafe2;
                i4 = i27;
                i5 = i26;
                t2 = t5;
                zzko = zzko2;
                i6 = i33;
                i7 = 1048575;
            }
        }
        if (i6 != i7) {
            unsafe.putInt(t2, (long) i6, i32);
        }
        zzlx zzlx = null;
        for (int i45 = zzko.zzm; i45 < zzko.zzn; i45++) {
            zzlx = (zzlx) zzko.zza((Object) t2, zzko.zzl[i45], (int) zzlx, (zzlu<UT, int>) zzko.zzq);
        }
        if (zzlx != null) {
            zzko.zzq.zzb((Object) t2, (T) zzlx);
        }
        if (i4 == 0) {
            if (i28 != i5) {
                throw zzjk.zzg();
            }
        } else if (i28 > i5 || i31 != i4) {
            throw zzjk.zzg();
        }
        return i28;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x02dc, code lost:
        if (r0 == r4) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x02e0, code lost:
        r15 = r30;
        r14 = r31;
        r12 = r32;
        r13 = r34;
        r11 = r35;
        r2 = r18;
        r1 = r25;
        r6 = r27;
        r7 = r28;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0323, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0346, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0348, code lost:
        r2 = r0;
     */
    @Override // com.google.android.gms.internal.vision.zzlc
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r31, byte[] r32, int r33, int r34, com.google.android.gms.internal.vision.zzhn r35) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 966
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzko.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.vision.zzhn):void");
    }

    @Override // com.google.android.gms.internal.vision.zzlc
    public final void zzc(T t) {
        int i;
        int i2 = this.zzm;
        while (true) {
            i = this.zzn;
            if (i2 >= i) {
                break;
            }
            long zzd = (long) (zzd(this.zzl[i2]) & 1048575);
            Object zzf = zzma.zzf(t, zzd);
            if (zzf != null) {
                zzma.zza(t, zzd, this.zzs.zze(zzf));
            }
            i2++;
        }
        int length = this.zzl.length;
        while (i < length) {
            this.zzp.zzb(t, (long) this.zzl[i]);
            i++;
        }
        this.zzq.zzd(t);
        if (this.zzh) {
            this.zzr.zzc(t);
        }
    }

    /* JADX DEBUG: Type inference failed for r4v0. Raw type applied. Possible types: java.util.Map<?, ?>, java.util.Map<K, V> */
    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzlu<UT, UB> zzlu) {
        int i2 = this.zzc[i];
        Object zzf = zzma.zzf(obj, (long) (zzd(i) & 1048575));
        if (zzf == null) {
            return ub;
        }
        zzjg zzc = zzc(i);
        if (zzc == null) {
            return ub;
        }
        return (UB) zza(i, i2, this.zzs.zza(zzf), zzc, (zzjg) ub, (zzlu<UT, zzjg>) zzlu);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzjg zzjg, UB ub, zzlu<UT, UB> zzlu) {
        zzkf<?, ?> zzb2 = this.zzs.zzb(zzb(i));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!zzjg.zza(((Integer) next.getValue()).intValue())) {
                if (ub == null) {
                    ub = zzlu.zza();
                }
                zzib zzc = zzht.zzc(zzkc.zza(zzb2, next.getKey(), next.getValue()));
                try {
                    zzkc.zza(zzc.zzb(), zzb2, next.getKey(), next.getValue());
                    zzlu.zza((zzlu<UT, UB>) ub, i2, zzc.zza());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v8, resolved type: com.google.android.gms.internal.vision.zzlc */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzlc
    public final boolean zzd(T t) {
        int i;
        int i2;
        int i3 = 1048575;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i5 >= this.zzm) {
                return !this.zzh || this.zzr.zza(t).zzf();
            }
            int i6 = this.zzl[i5];
            int i7 = this.zzc[i6];
            int zzd = zzd(i6);
            int i8 = this.zzc[i6 + 2];
            int i9 = i8 & 1048575;
            int i10 = 1 << (i8 >>> 20);
            if (i9 != i3) {
                if (i9 != 1048575) {
                    i4 = zzb.getInt(t, (long) i9);
                }
                i = i4;
                i2 = i9;
            } else {
                i2 = i3;
                i = i4;
            }
            if (((268435456 & zzd) != 0) && !zza((zzko<T>) t, i6, i2, i, i10)) {
                return false;
            }
            int i11 = (267386880 & zzd) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (zza((zzko<T>) t, i7, i6) && !zza(t, zzd, zza(i6))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 == 50 && !this.zzs.zzc(zzma.zzf(t, (long) (zzd & 1048575))).isEmpty()) {
                            this.zzs.zzb(zzb(i6));
                            throw null;
                        }
                    }
                }
                List list = (List) zzma.zzf(t, (long) (zzd & 1048575));
                if (!list.isEmpty()) {
                    zzlc zza2 = zza(i6);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!zza2.zzd(list.get(i12))) {
                            z = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza((zzko<T>) t, i6, i2, i, i10) && !zza(t, zzd, zza(i6))) {
                return false;
            }
            i5++;
            i3 = i2;
            i4 = i;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.google.android.gms.internal.vision.zzlc */
    /* JADX WARN: Multi-variable type inference failed */
    private static boolean zza(Object obj, int i, zzlc zzlc) {
        return zzlc.zzd(zzma.zzf(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzmr zzmr) throws IOException {
        if (obj instanceof String) {
            zzmr.zza(i, (String) obj);
        } else {
            zzmr.zza(i, (zzht) obj);
        }
    }

    private final int zzd(int i) {
        return this.zzc[i + 1];
    }

    private final int zze(int i) {
        return this.zzc[i + 2];
    }

    private static <T> double zzb(T t, long j) {
        return ((Double) zzma.zzf(t, j)).doubleValue();
    }

    private static <T> float zzc(T t, long j) {
        return ((Float) zzma.zzf(t, j)).floatValue();
    }

    private static <T> int zzd(T t, long j) {
        return ((Integer) zzma.zzf(t, j)).intValue();
    }

    private static <T> long zze(T t, long j) {
        return ((Long) zzma.zzf(t, j)).longValue();
    }

    private static <T> boolean zzf(T t, long j) {
        return ((Boolean) zzma.zzf(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza((zzko<T>) t, i) == zza((zzko<T>) t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3, int i4) {
        if (i2 == 1048575) {
            return zza((zzko<T>) t, i);
        }
        return (i3 & i4) != 0;
    }

    private final boolean zza(T t, int i) {
        int zze = zze(i);
        long j = (long) (zze & 1048575);
        if (j == 1048575) {
            int zzd = zzd(i);
            long j2 = (long) (zzd & 1048575);
            switch ((zzd & 267386880) >>> 20) {
                case 0:
                    return zzma.zze(t, j2) != 0.0d;
                case 1:
                    return zzma.zzd(t, j2) != 0.0f;
                case 2:
                    return zzma.zzb(t, j2) != 0;
                case 3:
                    return zzma.zzb(t, j2) != 0;
                case 4:
                    return zzma.zza(t, j2) != 0;
                case 5:
                    return zzma.zzb(t, j2) != 0;
                case 6:
                    return zzma.zza(t, j2) != 0;
                case 7:
                    return zzma.zzc(t, j2);
                case 8:
                    Object zzf = zzma.zzf(t, j2);
                    if (zzf instanceof String) {
                        return !((String) zzf).isEmpty();
                    }
                    if (zzf instanceof zzht) {
                        return !zzht.zza.equals(zzf);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzma.zzf(t, j2) != null;
                case 10:
                    return !zzht.zza.equals(zzma.zzf(t, j2));
                case 11:
                    return zzma.zza(t, j2) != 0;
                case 12:
                    return zzma.zza(t, j2) != 0;
                case 13:
                    return zzma.zza(t, j2) != 0;
                case 14:
                    return zzma.zzb(t, j2) != 0;
                case 15:
                    return zzma.zza(t, j2) != 0;
                case 16:
                    return zzma.zzb(t, j2) != 0;
                case 17:
                    return zzma.zzf(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return (zzma.zza(t, j) & (1 << (zze >>> 20))) != 0;
        }
    }

    private final void zzb(T t, int i) {
        int zze = zze(i);
        long j = (long) (1048575 & zze);
        if (j != 1048575) {
            zzma.zza((Object) t, j, (1 << (zze >>> 20)) | zzma.zza(t, j));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzma.zza(t, (long) (zze(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzma.zza((Object) t, (long) (zze(i2) & 1048575), i);
    }

    private final int zzg(int i) {
        if (i < this.zze || i > this.zzf) {
            return -1;
        }
        return zzb(i, 0);
    }

    private final int zza(int i, int i2) {
        if (i < this.zze || i > this.zzf) {
            return -1;
        }
        return zzb(i, i2);
    }

    private final int zzb(int i, int i2) {
        int length = (this.zzc.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzc[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
