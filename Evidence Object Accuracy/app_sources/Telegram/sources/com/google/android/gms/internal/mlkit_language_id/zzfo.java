package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzfo implements zzfw {
    private zzfw[] zza;

    /* access modifiers changed from: package-private */
    public zzfo(zzfw... zzfwArr) {
        this.zza = zzfwArr;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfw
    public final boolean zza(Class<?> cls) {
        for (zzfw zzfw : this.zza) {
            if (zzfw.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfw
    public final zzfx zzb(Class<?> cls) {
        zzfw[] zzfwArr = this.zza;
        for (zzfw zzfw : zzfwArr) {
            if (zzfw.zza(cls)) {
                return zzfw.zzb(cls);
            }
        }
        String name = cls.getName();
        throw new UnsupportedOperationException(name.length() != 0 ? "No factory is available for message type: ".concat(name) : new String("No factory is available for message type: "));
    }
}
