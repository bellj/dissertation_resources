package com.google.android.gms.internal.firebase_messaging;

import java.lang.annotation.Annotation;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzu implements zzz {
    private final int zza;
    private final zzy zzb;

    /* access modifiers changed from: package-private */
    public zzu(int i, zzy zzy) {
        this.zza = i;
        this.zzb = zzy;
    }

    @Override // java.lang.annotation.Annotation
    public final Class<? extends Annotation> annotationType() {
        return zzz.class;
    }

    @Override // java.lang.annotation.Annotation, java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzz)) {
            return false;
        }
        zzz zzz = (zzz) obj;
        return this.zza == zzz.zza() && this.zzb.equals(zzz.zzb());
    }

    @Override // java.lang.annotation.Annotation, java.lang.Object
    public final int hashCode() {
        return (this.zza ^ 14552422) + (this.zzb.hashCode() ^ 2041407134);
    }

    @Override // java.lang.annotation.Annotation, java.lang.Object
    public final String toString() {
        return "@com.google.firebase.encoders.proto.Protobuf(tag=" + this.zza + "intEncoding=" + this.zzb + ')';
    }

    @Override // com.google.android.gms.internal.firebase_messaging.zzz
    public final int zza() {
        return this.zza;
    }

    @Override // com.google.android.gms.internal.firebase_messaging.zzz
    public final zzy zzb() {
        return this.zzb;
    }
}
