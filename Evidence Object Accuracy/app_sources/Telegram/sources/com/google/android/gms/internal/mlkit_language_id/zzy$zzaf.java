package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzaf extends zzeo<zzy$zzaf, zza> implements zzgb {
    private static final zzy$zzaf zzl;
    private static volatile zzgj<zzy$zzaf> zzm;
    private int zzc;
    private long zzd;
    private int zze;
    private boolean zzf;
    private boolean zzg;
    private boolean zzh;
    private boolean zzi;
    private int zzj;
    private zzew<zzy$zzbf> zzk = zzeo.zzl();

    private zzy$zzaf() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzaf, zza> implements zzgb {
        private zza() {
            super(zzy$zzaf.zzl);
        }

        public final zza zza(long j) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzaf) this.zza).zza(j);
            return this;
        }

        public final zza zza(zzai zzai) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzaf) this.zza).zza(zzai);
            return this;
        }

        public final zza zza(boolean z) {
            if (this.zzb) {
                zzc();
                this.zzb = false;
            }
            ((zzy$zzaf) this.zza).zza(z);
            return this;
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(long j) {
        this.zzc |= 1;
        this.zzd = j;
    }

    /* access modifiers changed from: private */
    public final void zza(zzai zzai) {
        this.zze = zzai.zza();
        this.zzc |= 2;
    }

    /* access modifiers changed from: private */
    public final void zza(boolean z) {
        this.zzc |= 4;
        this.zzf = z;
    }

    public static zza zza() {
        return zzl.zzh();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzaf>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0081;
                case 2: goto L_0x007b;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzaf> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zzm
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzaf> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzaf> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zzm     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzaf r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zzl     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zzm = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzaf r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zzl
            return r2
        L_0x0033:
            r2 = 11
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzai.zzb()
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            r3 = 10
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbf> r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbf.class
            r2[r3] = r4
            java.lang.String r3 = "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001ဃ\u0000\u0002ဌ\u0001\u0003ဇ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ဇ\u0005\u0007ဋ\u0006\b\u001b"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzaf r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zzl
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x007b:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzaf$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzaf$zza
            r2.<init>(r3)
            return r2
        L_0x0081:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzaf r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzaf
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzaf.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzaf zzy_zzaf = new zzy$zzaf();
        zzl = zzy_zzaf;
        zzeo.zza(zzy$zzaf.class, zzy_zzaf);
    }
}
