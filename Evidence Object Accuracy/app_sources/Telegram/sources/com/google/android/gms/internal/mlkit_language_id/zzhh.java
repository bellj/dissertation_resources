package com.google.android.gms.internal.mlkit_language_id;

import java.io.IOException;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public abstract class zzhh<T, B> {
    /* access modifiers changed from: package-private */
    public abstract T zza(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zza(T t, zzib zzib) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(Object obj, T t);

    /* access modifiers changed from: package-private */
    public abstract T zzb(T t, T t2);

    /* access modifiers changed from: package-private */
    public abstract void zzb(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zzb(T t, zzib zzib) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract int zzc(T t);

    /* access modifiers changed from: package-private */
    public abstract int zzd(T t);
}
