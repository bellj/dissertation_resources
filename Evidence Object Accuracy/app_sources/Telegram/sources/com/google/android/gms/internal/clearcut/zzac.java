package com.google.android.gms.internal.clearcut;

import android.database.ContentObserver;
import android.os.Handler;

/* loaded from: classes.dex */
final class zzac extends ContentObserver {
    private final /* synthetic */ zzab zzdm;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public zzac(zzab zzab, Handler handler) {
        super(null);
        this.zzdm = zzab;
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        this.zzdm.zzh();
        zzab.zza(this.zzdm);
    }
}
