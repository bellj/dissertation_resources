package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;
import com.google.android.gms.internal.mlkit_language_id.zzy$zzy;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzb extends zzeo<zzy$zzb, zza> implements zzgb {
    private static final zzy$zzb zzg;
    private static volatile zzgj<zzy$zzb> zzh;
    private int zzc;
    private zzb zzd;
    private int zze;
    private zzy$zzab zzf;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo<zzb, zza> implements zzgb {
        private static final zzb zzi;
        private static volatile zzgj<zzb> zzj;
        private int zzc;
        private int zzd;
        private boolean zze;
        private zzew<zzy$zzy.zzb> zzf = zzeo.zzl();
        private zzew<zzy$zzy.zzb> zzg = zzeo.zzl();
        private zzy$zzam zzh;

        private zzb() {
        }

        /* compiled from: com.google.mlkit:language-id@@16.1.1 */
        /* loaded from: classes.dex */
        public static final class zza extends zzeo.zzb<zzb, zza> implements zzgb {
            private zza() {
                super(zzb.zzi);
            }

            /* synthetic */ zza(zzx zzx) {
                this();
            }
        }

        /* JADX WARN: Type inference failed for: r3v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object zza(int r3, java.lang.Object r4, java.lang.Object r5) {
            /*
                r2 = this;
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzy$zzb> r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzy.zzb.class
                int[] r5 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
                r0 = 1
                int r3 = r3 - r0
                r3 = r5[r3]
                r5 = 0
                switch(r3) {
                    case 1: goto L_0x0073;
                    case 2: goto L_0x006d;
                    case 3: goto L_0x0035;
                    case 4: goto L_0x0032;
                    case 5: goto L_0x0018;
                    case 6: goto L_0x0013;
                    case 7: goto L_0x0012;
                    default: goto L_0x000c;
                }
            L_0x000c:
                java.lang.UnsupportedOperationException r3 = new java.lang.UnsupportedOperationException
                r3.<init>()
                throw r3
            L_0x0012:
                return r5
            L_0x0013:
                java.lang.Byte r3 = java.lang.Byte.valueOf(r0)
                return r3
            L_0x0018:
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zzj
                if (r3 != 0) goto L_0x0031
                java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb> r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.class
                monitor-enter(r4)
                com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zzj     // Catch: all -> 0x002e
                if (r3 != 0) goto L_0x002c
                com.google.android.gms.internal.mlkit_language_id.zzeo$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002e
                com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb r5 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zzi     // Catch: all -> 0x002e
                r3.<init>(r5)     // Catch: all -> 0x002e
                com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zzj = r3     // Catch: all -> 0x002e
            L_0x002c:
                monitor-exit(r4)     // Catch: all -> 0x002e
                goto L_0x0031
            L_0x002e:
                r3 = move-exception
                monitor-exit(r4)     // Catch: all -> 0x002e
                throw r3
            L_0x0031:
                return r3
            L_0x0032:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zzi
                return r3
            L_0x0035:
                r3 = 9
                java.lang.Object[] r3 = new java.lang.Object[r3]
                r5 = 0
                java.lang.String r1 = "zzc"
                r3[r5] = r1
                java.lang.String r5 = "zzd"
                r3[r0] = r5
                r5 = 2
                com.google.android.gms.internal.mlkit_language_id.zzev r0 = com.google.android.gms.internal.mlkit_language_id.zzai.zzb()
                r3[r5] = r0
                r5 = 3
                java.lang.String r0 = "zze"
                r3[r5] = r0
                r5 = 4
                java.lang.String r0 = "zzf"
                r3[r5] = r0
                r5 = 5
                r3[r5] = r4
                r5 = 6
                java.lang.String r0 = "zzg"
                r3[r5] = r0
                r5 = 7
                r3[r5] = r4
                r4 = 8
                java.lang.String r5 = "zzh"
                r3[r4] = r5
                java.lang.String r4 = "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001ဌ\u0000\u0002ဇ\u0001\u0003\u001b\u0004\u001b\u0005ဉ\u0002"
                com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb r5 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zzi
                java.lang.Object r3 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r5, r4, r3)
                return r3
            L_0x006d:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb$zza r3 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb$zza
                r3.<init>(r5)
                return r3
            L_0x0073:
                com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb r3 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zzb
                r3.<init>()
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzb.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
        }

        static {
            zzb zzb = new zzb();
            zzi = zzb;
            zzeo.zza(zzb.class, zzb);
        }
    }

    private zzy$zzb() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzb, zza> implements zzgb {
        private zza() {
            super(zzy$zzb.zzg);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzb>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0058;
                case 2: goto L_0x0052;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzh
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzb> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzb> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzh     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzg     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzh = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzb r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzg
            return r2
        L_0x0033:
            r2 = 4
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဋ\u0001\u0003ဉ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzb r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zzg
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0052:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzb$zza
            r2.<init>(r3)
            return r2
        L_0x0058:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzb
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzb.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzb zzy_zzb = new zzy$zzb();
        zzg = zzy_zzb;
        zzeo.zza(zzy$zzb.class, zzy_zzb);
    }
}
