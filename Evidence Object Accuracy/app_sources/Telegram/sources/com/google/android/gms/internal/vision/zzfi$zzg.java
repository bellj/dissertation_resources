package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzg extends zzjb<zzfi$zzg, zza> implements zzkm {
    private static final zzfi$zzg zzj;
    private static volatile zzkx<zzfi$zzg> zzk;
    private int zzc;
    private int zzd;
    private int zze;
    private int zzf;
    private boolean zzg;
    private boolean zzh;
    private float zzi;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzb implements zzje {
        CLASSIFICATION_UNKNOWN(0),
        CLASSIFICATION_NONE(1),
        CLASSIFICATION_ALL(2);
        
        private final int zze;

        @Override // com.google.android.gms.internal.vision.zzje
        public final int zza() {
            return this.zze;
        }

        public static zzb zza(int i) {
            if (i == 0) {
                return CLASSIFICATION_UNKNOWN;
            }
            if (i == 1) {
                return CLASSIFICATION_NONE;
            }
            if (i != 2) {
                return null;
            }
            return CLASSIFICATION_ALL;
        }

        public static zzjg zzb() {
            return zzfr.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zze + " name=" + name() + '>';
        }

        zzb(int i) {
            this.zze = i;
        }

        static {
            new zzfq();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzc implements zzje {
        LANDMARK_UNKNOWN(0),
        LANDMARK_NONE(1),
        LANDMARK_ALL(2),
        LANDMARK_CONTOUR(3);
        
        private final int zzf;

        @Override // com.google.android.gms.internal.vision.zzje
        public final int zza() {
            return this.zzf;
        }

        public static zzc zza(int i) {
            if (i == 0) {
                return LANDMARK_UNKNOWN;
            }
            if (i == 1) {
                return LANDMARK_NONE;
            }
            if (i == 2) {
                return LANDMARK_ALL;
            }
            if (i != 3) {
                return null;
            }
            return LANDMARK_CONTOUR;
        }

        public static zzjg zzb() {
            return zzfs.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        zzc(int i) {
            this.zzf = i;
        }

        static {
            new zzft();
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zzd implements zzje {
        MODE_UNKNOWN(0),
        MODE_ACCURATE(1),
        MODE_FAST(2),
        MODE_SELFIE(3);
        
        private final int zzf;

        @Override // com.google.android.gms.internal.vision.zzje
        public final int zza() {
            return this.zzf;
        }

        public static zzd zza(int i) {
            if (i == 0) {
                return MODE_UNKNOWN;
            }
            if (i == 1) {
                return MODE_ACCURATE;
            }
            if (i == 2) {
                return MODE_FAST;
            }
            if (i != 3) {
                return null;
            }
            return MODE_SELFIE;
        }

        public static zzjg zzb() {
            return zzfv.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzd.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        zzd(int i) {
            this.zzf = i;
        }

        static {
            new zzfu();
        }
    }

    private zzfi$zzg() {
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzg>, com.google.android.gms.internal.vision.zzjb$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzjb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.vision.zzfk.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x007f;
                case 2: goto L_0x0079;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzg> r2 = com.google.android.gms.internal.vision.zzfi$zzg.zzk
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zzg> r3 = com.google.android.gms.internal.vision.zzfi$zzg.class
            monitor-enter(r3)
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzg> r2 = com.google.android.gms.internal.vision.zzfi$zzg.zzk     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.vision.zzjb$zza r2 = new com.google.android.gms.internal.vision.zzjb$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzg r4 = com.google.android.gms.internal.vision.zzfi$zzg.zzj     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzg.zzk = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.vision.zzfi$zzg r2 = com.google.android.gms.internal.vision.zzfi$zzg.zzj
            return r2
        L_0x0033:
            r2 = 10
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            com.google.android.gms.internal.vision.zzjg r4 = com.google.android.gms.internal.vision.zzfi$zzg.zzd.zzb()
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.vision.zzjg r4 = com.google.android.gms.internal.vision.zzfi$zzg.zzc.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 6
            com.google.android.gms.internal.vision.zzjg r4 = com.google.android.gms.internal.vision.zzfi$zzg.zzb.zzb()
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဌ\u0002\u0004ဇ\u0003\u0005ဇ\u0004\u0006ခ\u0005"
            com.google.android.gms.internal.vision.zzfi$zzg r4 = com.google.android.gms.internal.vision.zzfi$zzg.zzj
            java.lang.Object r2 = com.google.android.gms.internal.vision.zzjb.zza(r4, r3, r2)
            return r2
        L_0x0079:
            com.google.android.gms.internal.vision.zzfi$zzg$zza r2 = new com.google.android.gms.internal.vision.zzfi$zzg$zza
            r2.<init>(r3)
            return r2
        L_0x007f:
            com.google.android.gms.internal.vision.zzfi$zzg r2 = new com.google.android.gms.internal.vision.zzfi$zzg
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfi$zzg.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class zza extends zzjb.zzb<zzfi$zzg, zza> implements zzkm {
        private zza() {
            super(zzfi$zzg.zzj);
        }

        /* synthetic */ zza(zzfk zzfk) {
            this();
        }
    }

    static {
        zzfi$zzg zzfi_zzg = new zzfi$zzg();
        zzj = zzfi_zzg;
        zzjb.zza(zzfi$zzg.class, zzfi_zzg);
    }
}
