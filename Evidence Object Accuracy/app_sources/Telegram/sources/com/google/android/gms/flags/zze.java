package com.google.android.gms.flags;

import android.os.IBinder;
import com.google.android.gms.internal.flags.zza;

/* loaded from: classes.dex */
public final class zze extends zza implements zzc {
    /* access modifiers changed from: package-private */
    public zze(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.flags.IFlagProvider");
    }
}
