package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.maps.internal.zza;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;

/* compiled from: com.google.android.gms:play-services-maps@@17.0.1 */
/* loaded from: classes.dex */
public final class GoogleMapOptions extends AbstractSafeParcelable implements ReflectedParcelable {
    @RecentlyNonNull
    public static final Parcelable.Creator<GoogleMapOptions> CREATOR = new zzab();
    private Boolean zza;
    private Boolean zzb;
    private int zzc = -1;
    private CameraPosition zzd;
    private Boolean zze;
    private Boolean zzf;
    private Boolean zzg;
    private Boolean zzh;
    private Boolean zzi;
    private Boolean zzj;
    private Boolean zzk;
    private Boolean zzl;
    private Boolean zzm;
    private Float zzn = null;
    private Float zzo = null;
    private LatLngBounds zzp = null;
    private Boolean zzq;

    @RecentlyNullable
    public CameraPosition getCamera() {
        return this.zzd;
    }

    @RecentlyNullable
    public LatLngBounds getLatLngBoundsForCameraTarget() {
        return this.zzp;
    }

    public int getMapType() {
        return this.zzc;
    }

    @RecentlyNullable
    public Float getMaxZoomPreference() {
        return this.zzo;
    }

    @RecentlyNullable
    public Float getMinZoomPreference() {
        return this.zzn;
    }

    @Override // java.lang.Object
    @RecentlyNonNull
    public String toString() {
        return Objects.toStringHelper(this).add("MapType", Integer.valueOf(this.zzc)).add("LiteMode", this.zzk).add("Camera", this.zzd).add("CompassEnabled", this.zzf).add("ZoomControlsEnabled", this.zze).add("ScrollGesturesEnabled", this.zzg).add("ZoomGesturesEnabled", this.zzh).add("TiltGesturesEnabled", this.zzi).add("RotateGesturesEnabled", this.zzj).add("ScrollGesturesEnabledDuringRotateOrZoom", this.zzq).add("MapToolbarEnabled", this.zzl).add("AmbientEnabled", this.zzm).add("MinZoomPreference", this.zzn).add("MaxZoomPreference", this.zzo).add("LatLngBoundsForCameraTarget", this.zzp).add("ZOrderOnTop", this.zza).add("UseViewLifecycleInFragment", this.zzb).toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByte(parcel, 2, zza.zzb(this.zza));
        SafeParcelWriter.writeByte(parcel, 3, zza.zzb(this.zzb));
        SafeParcelWriter.writeInt(parcel, 4, getMapType());
        SafeParcelWriter.writeParcelable(parcel, 5, getCamera(), i, false);
        SafeParcelWriter.writeByte(parcel, 6, zza.zzb(this.zze));
        SafeParcelWriter.writeByte(parcel, 7, zza.zzb(this.zzf));
        SafeParcelWriter.writeByte(parcel, 8, zza.zzb(this.zzg));
        SafeParcelWriter.writeByte(parcel, 9, zza.zzb(this.zzh));
        SafeParcelWriter.writeByte(parcel, 10, zza.zzb(this.zzi));
        SafeParcelWriter.writeByte(parcel, 11, zza.zzb(this.zzj));
        SafeParcelWriter.writeByte(parcel, 12, zza.zzb(this.zzk));
        SafeParcelWriter.writeByte(parcel, 14, zza.zzb(this.zzl));
        SafeParcelWriter.writeByte(parcel, 15, zza.zzb(this.zzm));
        SafeParcelWriter.writeFloatObject(parcel, 16, getMinZoomPreference(), false);
        SafeParcelWriter.writeFloatObject(parcel, 17, getMaxZoomPreference(), false);
        SafeParcelWriter.writeParcelable(parcel, 18, getLatLngBoundsForCameraTarget(), i, false);
        SafeParcelWriter.writeByte(parcel, 19, zza.zzb(this.zzq));
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    /* access modifiers changed from: package-private */
    public GoogleMapOptions(byte b, byte b2, int i, CameraPosition cameraPosition, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9, byte b10, byte b11, Float f, Float f2, LatLngBounds latLngBounds, byte b12) {
        this.zza = zza.zza(b);
        this.zzb = zza.zza(b2);
        this.zzc = i;
        this.zzd = cameraPosition;
        this.zze = zza.zza(b3);
        this.zzf = zza.zza(b4);
        this.zzg = zza.zza(b5);
        this.zzh = zza.zza(b6);
        this.zzi = zza.zza(b7);
        this.zzj = zza.zza(b8);
        this.zzk = zza.zza(b9);
        this.zzl = zza.zza(b10);
        this.zzm = zza.zza(b11);
        this.zzn = f;
        this.zzo = f2;
        this.zzp = latLngBounds;
        this.zzq = zza.zza(b12);
    }
}
