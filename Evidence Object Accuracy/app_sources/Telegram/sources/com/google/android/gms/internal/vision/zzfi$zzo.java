package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzo extends zzjb<zzfi$zzo, zza> implements zzkm {
    private static final zzfi$zzo zzi;
    private static volatile zzkx<zzfi$zzo> zzj;
    private int zzc;
    private zzfi$zze zzd;
    private zzfi$zzk zze;
    private zzfi$zzi zzf;
    private int zzg;
    private boolean zzh;

    private zzfi$zzo() {
    }

    /* access modifiers changed from: private */
    public final void zza(zzfi$zzi zzfi_zzi) {
        zzfi_zzi.getClass();
        this.zzf = zzfi_zzi;
        this.zzc |= 4;
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class zza extends zzjb.zzb<zzfi$zzo, zza> implements zzkm {
        private zza() {
            super(zzfi$zzo.zzi);
        }

        public final zza zza(zzfi$zzi zzfi_zzi) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zzo) this.zza).zza(zzfi_zzi);
            return this;
        }

        /* synthetic */ zza(zzfk zzfk) {
            this();
        }
    }

    public static zza zza() {
        return zzi.zzj();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzjb$zza, com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzo>] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzjb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.vision.zzfk.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0062;
                case 2: goto L_0x005c;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzo> r2 = com.google.android.gms.internal.vision.zzfi$zzo.zzj
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zzo> r3 = com.google.android.gms.internal.vision.zzfi$zzo.class
            monitor-enter(r3)
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzo> r2 = com.google.android.gms.internal.vision.zzfi$zzo.zzj     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.vision.zzjb$zza r2 = new com.google.android.gms.internal.vision.zzjb$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzo r4 = com.google.android.gms.internal.vision.zzfi$zzo.zzi     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzo.zzj = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.vision.zzfi$zzo r2 = com.google.android.gms.internal.vision.zzfi$zzo.zzi
            return r2
        L_0x0033:
            r2 = 6
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004င\u0003\u0005ဇ\u0004"
            com.google.android.gms.internal.vision.zzfi$zzo r4 = com.google.android.gms.internal.vision.zzfi$zzo.zzi
            java.lang.Object r2 = com.google.android.gms.internal.vision.zzjb.zza(r4, r3, r2)
            return r2
        L_0x005c:
            com.google.android.gms.internal.vision.zzfi$zzo$zza r2 = new com.google.android.gms.internal.vision.zzfi$zzo$zza
            r2.<init>(r3)
            return r2
        L_0x0062:
            com.google.android.gms.internal.vision.zzfi$zzo r2 = new com.google.android.gms.internal.vision.zzfi$zzo
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfi$zzo.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzfi$zzo zzfi_zzo = new zzfi$zzo();
        zzi = zzfi_zzo;
        zzjb.zza(zzfi$zzo.class, zzfi_zzo);
    }
}
