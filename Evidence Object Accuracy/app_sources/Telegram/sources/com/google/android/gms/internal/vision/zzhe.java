package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzhe;
import com.google.android.gms.internal.vision.zzhf;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public abstract class zzhe<MessageType extends zzhf<MessageType, BuilderType>, BuilderType extends zzhe<MessageType, BuilderType>> implements zzkn {
    protected abstract BuilderType zza(MessageType messagetype);

    public abstract BuilderType zza(byte[] bArr, int i, int i2, zzio zzio) throws zzjk;

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.google.android.gms.internal.vision.zzhe<MessageType extends com.google.android.gms.internal.vision.zzhf<MessageType, BuilderType>, BuilderType extends com.google.android.gms.internal.vision.zzhe<MessageType, BuilderType>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.android.gms.internal.vision.zzkn
    public final /* synthetic */ zzkn zza(zzkk zzkk) {
        if (zzr().getClass().isInstance(zzkk)) {
            return zza((zzhe<MessageType, BuilderType>) ((zzhf) zzkk));
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
