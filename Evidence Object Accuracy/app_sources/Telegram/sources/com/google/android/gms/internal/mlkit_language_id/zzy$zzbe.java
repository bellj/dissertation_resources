package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzbe extends zzeo<zzy$zzbe, zza> implements zzgb {
    private static final zzy$zzbe zzl;
    private static volatile zzgj<zzy$zzbe> zzm;
    private int zzc;
    private zzy$zzaf zzd;
    private zzy$zzbi zze;
    private int zzf;
    private int zzg;
    private int zzh;
    private int zzi;
    private int zzj;
    private int zzk;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zzb implements zzet {
        NO_ERROR(0),
        METADATA_FILE_UNAVAILABLE(1),
        METADATA_ENTRY_NOT_FOUND(2),
        METADATA_JSON_INVALID(3),
        METADATA_HASH_NOT_FOUND(4),
        DOWNLOAD_MANAGER_SERVICE_MISSING(5),
        DOWNLOAD_MANAGER_HTTP_UNKNOWN_STATUS(6),
        DOWNLOAD_MANAGER_HTTP_BAD_REQUEST(400),
        DOWNLOAD_MANAGER_HTTP_UNAUTHORIZED(401),
        DOWNLOAD_MANAGER_HTTP_FORBIDDEN(403),
        DOWNLOAD_MANAGER_HTTP_NOT_FOUND(404),
        DOWNLOAD_MANAGER_HTTP_REQUEST_TIMEOUT(408),
        DOWNLOAD_MANAGER_HTTP_ABORTED(409),
        DOWNLOAD_MANAGER_HTTP_TOO_MANY_REQUESTS(429),
        DOWNLOAD_MANAGER_HTTP_CANCELLED(499),
        DOWNLOAD_MANAGER_HTTP_UNIMPLEMENTED(501),
        DOWNLOAD_MANAGER_HTTP_INTERNAL_SERVICE_ERROR(500),
        DOWNLOAD_MANAGER_HTTP_SERVICE_UNAVAILABLE(503),
        DOWNLOAD_MANAGER_HTTP_DEADLINE_EXCEEDED(504),
        DOWNLOAD_MANAGER_HTTP_NETWORK_AUTHENTICATION_REQUIRED(511),
        DOWNLOAD_MANAGER_FILE_ERROR(7),
        DOWNLOAD_MANAGER_UNHANDLED_HTTP_CODE(8),
        DOWNLOAD_MANAGER_HTTP_DATA_ERROR(9),
        DOWNLOAD_MANAGER_TOO_MANY_REDIRECTS(10),
        DOWNLOAD_MANAGER_INSUFFICIENT_SPACE(11),
        DOWNLOAD_MANAGER_DEVICE_NOT_FOUND(12),
        DOWNLOAD_MANAGER_CANNOT_RESUME(13),
        DOWNLOAD_MANAGER_FILE_ALREADY_EXISTS(14),
        DOWNLOAD_MANAGER_UNKNOWN_ERROR(15),
        POST_DOWNLOAD_FILE_NOT_FOUND(16),
        POST_DOWNLOAD_MOVE_FILE_FAILED(17),
        POST_DOWNLOAD_UNZIP_FAILED(18),
        RAPID_RESPONSE_COULD_NOT_BE_WRITTEN(19),
        DRIVER_OBJECT_DEALLOCATED(20);
        
        private final int zzaj;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zzaj;
        }

        public static zzev zzb() {
            return zzbz.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzb.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzaj + " name=" + name() + '>';
        }

        zzb(int i) {
            this.zzaj = i;
        }

        static {
            new zzby();
        }
    }

    private zzy$zzbe() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zza extends zzeo.zzb<zzy$zzbe, zza> implements zzgb {
        private zza() {
            super(zzy$zzbe.zzl);
        }

        /* synthetic */ zza(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbe>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x007b;
                case 2: goto L_0x0075;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbe> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzm
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzbe> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzbe> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzm     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbe r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzl     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzm = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbe r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzl
            return r2
        L_0x0033:
            r2 = 10
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 8
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzb.zzb()
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            java.lang.String r3 = "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003င\u0002\u0004င\u0003\u0005င\u0004\u0006င\u0005\u0007ဌ\u0006\bင\u0007"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbe r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zzl
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0075:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbe$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbe$zza
            r2.<init>(r3)
            return r2
        L_0x007b:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzbe r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzbe
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzbe.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzbe zzy_zzbe = new zzy$zzbe();
        zzl = zzy_zzbe;
        zzeo.zza(zzy$zzbe.class, zzy_zzbe);
    }
}
