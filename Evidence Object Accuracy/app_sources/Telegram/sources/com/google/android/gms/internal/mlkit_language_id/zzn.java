package com.google.android.gms.internal.mlkit_language_id;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzn<E> extends zzi<E> {
    private final zzk<E> zza;

    /* access modifiers changed from: package-private */
    public zzn(zzk<E> zzk, int i) {
        super(zzk.size(), i);
        this.zza = zzk;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzi
    protected final E zza(int i) {
        return this.zza.get(i);
    }
}
