package com.google.android.gms.internal.vision;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzlt implements zzls {
    private final /* synthetic */ zzht zza;

    /* access modifiers changed from: package-private */
    public zzlt(zzht zzht) {
        this.zza = zzht;
    }

    @Override // com.google.android.gms.internal.vision.zzls
    public final int zza() {
        return this.zza.zza();
    }

    @Override // com.google.android.gms.internal.vision.zzls
    public final byte zza(int i) {
        return this.zza.zza(i);
    }
}
