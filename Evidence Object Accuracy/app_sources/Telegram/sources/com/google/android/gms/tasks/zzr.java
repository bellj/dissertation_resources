package com.google.android.gms.tasks;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface zzr<TResult> {
    void zza();

    void zza(Task<TResult> task);
}
