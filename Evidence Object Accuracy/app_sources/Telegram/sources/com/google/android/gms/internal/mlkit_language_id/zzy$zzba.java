package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzeo;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
public final class zzy$zzba extends zzeo<zzy$zzba, zzb> implements zzgb {
    private static final zzy$zzba zzg;
    private static volatile zzgj<zzy$zzba> zzh;
    private int zzc;
    private int zzd;
    private int zze;
    private int zzf;

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zza implements zzet {
        INVALID_MODE(0),
        STREAM(1),
        SINGLE_IMAGE(2);
        
        private final int zze;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zze;
        }

        public static zzev zzb() {
            return zzbs.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zze + " name=" + name() + '>';
        }

        zza(int i) {
            this.zze = i;
        }

        static {
            new zzbt();
        }
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public enum zzc implements zzet {
        UNKNOWN_PERFORMANCE(0),
        FAST(1),
        ACCURATE(2);
        
        private final int zze;

        @Override // com.google.android.gms.internal.mlkit_language_id.zzet
        public final int zza() {
            return this.zze;
        }

        public static zzev zzb() {
            return zzbv.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zzc.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zze + " name=" + name() + '>';
        }

        zzc(int i) {
            this.zze = i;
        }

        static {
            new zzbu();
        }
    }

    private zzy$zzba() {
    }

    /* compiled from: com.google.mlkit:language-id@@16.1.1 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzeo.zzb<zzy$zzba, zzb> implements zzgb {
        private zzb() {
            super(zzy$zzba.zzg);
        }

        /* synthetic */ zzb(zzx zzx) {
            this();
        }
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzba>, com.google.android.gms.internal.mlkit_language_id.zzeo$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.mlkit_language_id.zzeo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.mlkit_language_id.zzx.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x006d;
                case 2: goto L_0x0067;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzba> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzh
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.mlkit_language_id.zzy$zzba> r3 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.class
            monitor-enter(r3)
            com.google.android.gms.internal.mlkit_language_id.zzgj<com.google.android.gms.internal.mlkit_language_id.zzy$zzba> r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzh     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.mlkit_language_id.zzeo$zza r2 = new com.google.android.gms.internal.mlkit_language_id.zzeo$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzba r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzg     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzh = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzba r2 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzg
            return r2
        L_0x0033:
            r2 = 7
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zza.zzb()
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 4
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzc.zzb()
            r2[r3] = r4
            r3 = 5
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 6
            com.google.android.gms.internal.mlkit_language_id.zzev r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzc.zzb()
            r2[r3] = r4
            java.lang.String r3 = "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဌ\u0000\u0002ဌ\u0001\u0003ဌ\u0002"
            com.google.android.gms.internal.mlkit_language_id.zzy$zzba r4 = com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zzg
            java.lang.Object r2 = com.google.android.gms.internal.mlkit_language_id.zzeo.zza(r4, r3, r2)
            return r2
        L_0x0067:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzba$zzb r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzba$zzb
            r2.<init>(r3)
            return r2
        L_0x006d:
            com.google.android.gms.internal.mlkit_language_id.zzy$zzba r2 = new com.google.android.gms.internal.mlkit_language_id.zzy$zzba
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzy$zzba.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzy$zzba zzy_zzba = new zzy$zzba();
        zzg = zzy_zzba;
        zzeo.zza(zzy$zzba.class, zzy_zzba);
    }
}
