package com.google.android.gms.internal.p000authapiphone;

import android.os.RemoteException;

/* renamed from: com.google.android.gms.internal.auth-api-phone.zzk  reason: invalid package */
/* loaded from: classes.dex */
final class zzk extends zzm {
    /* access modifiers changed from: package-private */
    public zzk(zzj zzj) {
        super(null);
    }

    @Override // com.google.android.gms.internal.p000authapiphone.zzm
    protected final void zza(zze zze) throws RemoteException {
        zze.zza(new zzl(this));
    }
}
