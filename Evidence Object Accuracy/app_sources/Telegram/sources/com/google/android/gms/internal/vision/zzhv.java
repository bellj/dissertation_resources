package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.mlkit_language_id.zzdp$$ExternalSyntheticBackport0;
import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
final class zzhv implements Comparator<zzht> {
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(zzht zzht, zzht zzht2) {
        zzht zzht3 = zzht;
        zzht zzht4 = zzht2;
        zzhy zzhy = (zzhy) zzht3.iterator();
        zzhy zzhy2 = (zzhy) zzht4.iterator();
        while (zzhy.hasNext() && zzhy2.hasNext()) {
            int m = zzdp$$ExternalSyntheticBackport0.m(zzht.zzb(zzhy.zza()), zzht.zzb(zzhy2.zza()));
            if (m != 0) {
                return m;
            }
        }
        return zzdp$$ExternalSyntheticBackport0.m(zzht3.zza(), zzht4.zza());
    }
}
