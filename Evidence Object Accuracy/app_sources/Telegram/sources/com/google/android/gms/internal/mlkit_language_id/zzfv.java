package com.google.android.gms.internal.mlkit_language_id;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
final class zzfv implements zzfs {
    @Override // com.google.android.gms.internal.mlkit_language_id.zzfs
    public final zzfq<?, ?> zzc(Object obj) {
        zzfr zzfr = (zzfr) obj;
        throw new NoSuchMethodError();
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfs
    public final Map<?, ?> zza(Object obj) {
        return (zzft) obj;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfs
    public final Object zzb(Object obj) {
        ((zzft) obj).zzb();
        return obj;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfs
    public final Object zza(Object obj, Object obj2) {
        zzft zzft = (zzft) obj;
        zzft zzft2 = (zzft) obj2;
        if (!zzft2.isEmpty()) {
            if (!zzft.zzc()) {
                zzft = zzft.zza();
            }
            zzft.zza(zzft2);
        }
        return zzft;
    }

    @Override // com.google.android.gms.internal.mlkit_language_id.zzfs
    public final int zza(int i, Object obj, Object obj2) {
        zzft zzft = (zzft) obj;
        zzfr zzfr = (zzfr) obj2;
        if (zzft.isEmpty()) {
            return 0;
        }
        Iterator it = zzft.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }
}
