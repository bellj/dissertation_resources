package com.google.android.gms.internal.clearcut;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public abstract class zzcy {
    private static final zzcy zzlt = new zzda();
    private static final zzcy zzlu = new zzdb();

    private zzcy() {
    }

    /* access modifiers changed from: package-private */
    public static zzcy zzbv() {
        return zzlt;
    }

    /* access modifiers changed from: package-private */
    public static zzcy zzbw() {
        return zzlu;
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(Object obj, long j);

    /* access modifiers changed from: package-private */
    public abstract <L> void zza(Object obj, Object obj2, long j);
}
