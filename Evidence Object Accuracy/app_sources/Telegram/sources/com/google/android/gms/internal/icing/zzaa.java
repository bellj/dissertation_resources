package com.google.android.gms.internal.icing;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.firebase.appindexing.internal.zzc;

/* compiled from: com.google.firebase:firebase-appindexing@@20.0.0 */
/* loaded from: classes.dex */
public final class zzaa extends zza {
    /* access modifiers changed from: package-private */
    public zzaa(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.appdatasearch.internal.ILightweightAppDataSearch");
    }

    public final void zze(zzac zzac, zzc[] zzcArr) throws RemoteException {
        Parcel zza = zza();
        zzc.zzc(zza, zzac);
        zza.writeTypedArray(zzcArr, 0);
        zzc(7, zza);
    }
}
