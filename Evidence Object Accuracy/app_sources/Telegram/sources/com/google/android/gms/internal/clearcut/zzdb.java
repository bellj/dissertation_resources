package com.google.android.gms.internal.clearcut;

/* loaded from: classes.dex */
final class zzdb extends zzcy {
    private zzdb() {
        super();
    }

    private static <E> zzcn<E> zzc(Object obj, long j) {
        return (zzcn) zzfd.zzo(obj, j);
    }

    @Override // com.google.android.gms.internal.clearcut.zzcy
    final void zza(Object obj, long j) {
        zzc(obj, j).zzv();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:9:0x0022 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
    @Override // com.google.android.gms.internal.clearcut.zzcy
    final <E> void zza(Object obj, Object obj2, long j) {
        zzcn<E> zzc = zzc(obj, j);
        zzcn<E> zzc2 = zzc(obj2, j);
        int size = zzc.size();
        int size2 = zzc2.size();
        zzc2 = zzc;
        zzc2 = zzc;
        if (size > 0 && size2 > 0) {
            boolean zzu = zzc.zzu();
            zzcn<E> zzcn = zzc;
            if (!zzu) {
                zzcn = zzc.zzi(size2 + size);
            }
            zzcn.addAll(zzc2);
            zzc2 = zzcn;
        }
        if (size > 0) {
        }
        zzfd.zza(obj, j, zzc2);
    }
}
