package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.telegram.messenger.R;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzej<T extends zzel<T>> {
    private static final zzej zzd = new zzej(true);
    final zzgq<T, Object> zza;
    private boolean zzb;
    private boolean zzc;

    private zzej() {
        this.zza = zzgq.zza(16);
    }

    private zzej(boolean z) {
        this(zzgq.zza(0));
        zzb();
    }

    private zzej(zzgq<T, Object> zzgq) {
        this.zza = zzgq;
        zzb();
    }

    public static <T extends zzel<T>> zzej<T> zza() {
        return zzd;
    }

    public final void zzb() {
        if (!this.zzb) {
            this.zza.zza();
            this.zzb = true;
        }
    }

    public final boolean zzc() {
        return this.zzb;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzej)) {
            return false;
        }
        return this.zza.equals(((zzej) obj).zza);
    }

    public final int hashCode() {
        return this.zza.hashCode();
    }

    public final Iterator<Map.Entry<T, Object>> zzd() {
        if (this.zzc) {
            return new zzff(this.zza.entrySet().iterator());
        }
        return this.zza.entrySet().iterator();
    }

    /* access modifiers changed from: package-private */
    public final Iterator<Map.Entry<T, Object>> zze() {
        if (this.zzc) {
            return new zzff(this.zza.zze().iterator());
        }
        return this.zza.zze().iterator();
    }

    private final Object zza(T t) {
        Object obj = this.zza.get(t);
        if (!(obj instanceof zzfa)) {
            return obj;
        }
        zzfa zzfa = (zzfa) obj;
        return zzfa.zza();
    }

    private final void zzb(T t, Object obj) {
        if (!t.zzd()) {
            zza(t.zzb(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                zza(t.zzb(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof zzfa) {
            this.zzc = true;
        }
        this.zza.zza((zzgq<T, Object>) t, (T) obj);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if ((r3 instanceof com.google.android.gms.internal.mlkit_language_id.zzet) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if ((r3 instanceof com.google.android.gms.internal.mlkit_language_id.zzfa) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void zza(com.google.android.gms.internal.mlkit_language_id.zzhv r2, java.lang.Object r3) {
        /*
            com.google.android.gms.internal.mlkit_language_id.zzeq.zza(r3)
            int[] r0 = com.google.android.gms.internal.mlkit_language_id.zzei.zza
            com.google.android.gms.internal.mlkit_language_id.zzhy r2 = r2.zza()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0037;
                case 5: goto L_0x0034;
                case 6: goto L_0x0031;
                case 7: goto L_0x0028;
                case 8: goto L_0x001f;
                case 9: goto L_0x0016;
                default: goto L_0x0014;
            }
        L_0x0014:
            r0 = 0
            goto L_0x0042
        L_0x0016:
            boolean r2 = r3 instanceof com.google.android.gms.internal.mlkit_language_id.zzfz
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.mlkit_language_id.zzfa
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x001f:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.mlkit_language_id.zzet
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0028:
            boolean r2 = r3 instanceof com.google.android.gms.internal.mlkit_language_id.zzdn
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0031:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0042
        L_0x0034:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0042
        L_0x0037:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0042
        L_0x003a:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0042
        L_0x003d:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0042
        L_0x0040:
            boolean r0 = r3 instanceof java.lang.Integer
        L_0x0042:
            if (r0 == 0) goto L_0x0045
            return
        L_0x0045:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            goto L_0x004e
        L_0x004d:
            throw r2
        L_0x004e:
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.mlkit_language_id.zzej.zza(com.google.android.gms.internal.mlkit_language_id.zzhv, java.lang.Object):void");
    }

    public final boolean zzf() {
        for (int i = 0; i < this.zza.zzc(); i++) {
            if (!zza((Map.Entry) this.zza.zzb(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.zza.zzd()) {
            if (!zza((Map.Entry) entry)) {
                return false;
            }
        }
        return true;
    }

    private static <T extends zzel<T>> boolean zza(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.zzc() == zzhy.MESSAGE) {
            if (key.zzd()) {
                for (zzfz zzfz : (List) entry.getValue()) {
                    if (!zzfz.zzi()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof zzfz) {
                    if (!((zzfz) value).zzi()) {
                        return false;
                    }
                } else if (value instanceof zzfa) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public final void zza(zzej<T> zzej) {
        for (int i = 0; i < zzej.zza.zzc(); i++) {
            zzb(zzej.zza.zzb(i));
        }
        for (Map.Entry<T, Object> entry : zzej.zza.zzd()) {
            zzb(entry);
        }
    }

    private static Object zza(Object obj) {
        if (obj instanceof zzgf) {
            return ((zzgf) obj).zza();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    private final void zzb(Map.Entry<T, Object> entry) {
        zzfz zzfz;
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof zzfa) {
            zzfa zzfa = (zzfa) value;
            value = zzfa.zza();
        }
        if (key.zzd()) {
            Object zza = zza((zzej<T>) key);
            if (zza == null) {
                zza = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) zza).add(zza(obj));
            }
            this.zza.zza((zzgq<T, Object>) key, (T) zza);
        } else if (key.zzc() == zzhy.MESSAGE) {
            Object zza2 = zza((zzej<T>) key);
            if (zza2 == null) {
                this.zza.zza((zzgq<T, Object>) key, (T) zza(value));
                return;
            }
            if (zza2 instanceof zzgf) {
                zzfz = key.zza((zzgf) zza2, (zzgf) value);
            } else {
                zzfz = key.zza(((zzfz) zza2).zzm(), (zzfz) value).zzg();
            }
            this.zza.zza((zzgq<T, Object>) key, (T) zzfz);
        } else {
            this.zza.zza((zzgq<T, Object>) key, (T) zza(value));
        }
    }

    public final int zzg() {
        int i = 0;
        for (int i2 = 0; i2 < this.zza.zzc(); i2++) {
            i += zzc(this.zza.zzb(i2));
        }
        for (Map.Entry<T, Object> entry : this.zza.zzd()) {
            i += zzc(entry);
        }
        return i;
    }

    private static int zzc(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.zzc() != zzhy.MESSAGE || key.zzd() || key.zze()) {
            return zza((zzel<?>) key, value);
        }
        if (value instanceof zzfa) {
            return zzea.zzb(entry.getKey().zza(), (zzfa) value);
        }
        return zzea.zzb(entry.getKey().zza(), (zzfz) value);
    }

    static int zza(zzhv zzhv, int i, Object obj) {
        int zze = zzea.zze(i);
        if (zzhv == zzhv.GROUP) {
            zzeq.zza((zzfz) obj);
            zze <<= 1;
        }
        return zze + zzb(zzhv, obj);
    }

    private static int zzb(zzhv zzhv, Object obj) {
        switch (zzei.zzb[zzhv.ordinal()]) {
            case 1:
                return zzea.zzb(((Double) obj).doubleValue());
            case 2:
                return zzea.zzb(((Float) obj).floatValue());
            case 3:
                return zzea.zzd(((Long) obj).longValue());
            case 4:
                return zzea.zze(((Long) obj).longValue());
            case 5:
                return zzea.zzf(((Integer) obj).intValue());
            case 6:
                return zzea.zzg(((Long) obj).longValue());
            case 7:
                return zzea.zzi(((Integer) obj).intValue());
            case 8:
                return zzea.zzb(((Boolean) obj).booleanValue());
            case 9:
                return zzea.zzc((zzfz) obj);
            case 10:
                if (obj instanceof zzfa) {
                    return zzea.zza((zzfa) obj);
                }
                return zzea.zzb((zzfz) obj);
            case 11:
                if (obj instanceof zzdn) {
                    return zzea.zzb((zzdn) obj);
                }
                return zzea.zzb((String) obj);
            case 12:
                if (obj instanceof zzdn) {
                    return zzea.zzb((zzdn) obj);
                }
                return zzea.zzb((byte[]) obj);
            case 13:
                return zzea.zzg(((Integer) obj).intValue());
            case 14:
                return zzea.zzj(((Integer) obj).intValue());
            case 15:
                return zzea.zzh(((Long) obj).longValue());
            case 16:
                return zzea.zzh(((Integer) obj).intValue());
            case 17:
                return zzea.zzf(((Long) obj).longValue());
            case R.styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom /* 18 */:
                if (obj instanceof zzet) {
                    return zzea.zzk(((zzet) obj).zza());
                }
                return zzea.zzk(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int zza(zzel<?> zzel, Object obj) {
        zzhv zzb = zzel.zzb();
        int zza = zzel.zza();
        if (!zzel.zzd()) {
            return zza(zzb, zza, obj);
        }
        int i = 0;
        if (zzel.zze()) {
            for (Object obj2 : (List) obj) {
                i += zzb(zzb, obj2);
            }
            return zzea.zze(zza) + i + zzea.zzl(i);
        }
        for (Object obj3 : (List) obj) {
            i += zza(zzb, zza, obj3);
        }
        return i;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzej zzej = new zzej();
        for (int i = 0; i < this.zza.zzc(); i++) {
            Map.Entry<T, Object> zzb = this.zza.zzb(i);
            zzej.zzb((zzej) zzb.getKey(), zzb.getValue());
        }
        for (Map.Entry<T, Object> entry : this.zza.zzd()) {
            zzej.zzb((zzej) entry.getKey(), entry.getValue());
        }
        zzej.zzc = this.zzc;
        return zzej;
    }
}
