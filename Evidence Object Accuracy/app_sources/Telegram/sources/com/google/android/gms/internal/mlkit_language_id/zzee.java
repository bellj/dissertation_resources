package com.google.android.gms.internal.mlkit_language_id;

import com.google.android.gms.internal.mlkit_language_id.zzel;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* loaded from: classes.dex */
abstract class zzee<T extends zzel<T>> {
    /* access modifiers changed from: package-private */
    public abstract int zza(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    public abstract zzej<T> zza(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zza(zzib zzib, Map.Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzfz zzfz);

    /* access modifiers changed from: package-private */
    public abstract zzej<T> zzb(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zzc(Object obj);
}
