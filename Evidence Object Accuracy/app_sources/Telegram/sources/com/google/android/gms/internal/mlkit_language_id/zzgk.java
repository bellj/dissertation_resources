package com.google.android.gms.internal.mlkit_language_id;

import j$.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: com.google.mlkit:language-id@@16.1.1 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzgk {
    private static final zzgk zza = new zzgk();
    private final zzgo zzb = new zzfm();
    private final ConcurrentMap<Class<?>, zzgp<?>> zzc = new ConcurrentHashMap();

    public static zzgk zza() {
        return zza;
    }

    public final <T> zzgp<T> zza(Class<T> cls) {
        zzeq.zza(cls, "messageType");
        zzgp<T> zzgp = (zzgp<T>) this.zzc.get(cls);
        if (zzgp != null) {
            return zzgp;
        }
        zzgp<T> zza2 = this.zzb.zza(cls);
        zzeq.zza(cls, "messageType");
        zzeq.zza(zza2, "schema");
        zzgp<T> zzgp2 = (zzgp<T>) this.zzc.putIfAbsent(cls, zza2);
        return zzgp2 != null ? zzgp2 : zza2;
    }

    public final <T> zzgp<T> zza(T t) {
        return zza((Class) t.getClass());
    }

    private zzgk() {
    }
}
