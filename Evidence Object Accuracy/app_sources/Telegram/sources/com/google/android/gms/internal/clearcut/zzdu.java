package com.google.android.gms.internal.clearcut;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes.dex */
final class zzdu<T> implements zzef<T> {
    private final zzdo zzmn;
    private final boolean zzmo;
    private final zzex<?, ?> zzmx;
    private final zzbu<?> zzmy;

    private zzdu(zzex<?, ?> zzex, zzbu<?> zzbu, zzdo zzdo) {
        this.zzmx = zzex;
        this.zzmo = zzbu.zze(zzdo);
        this.zzmy = zzbu;
        this.zzmn = zzdo;
    }

    /* access modifiers changed from: package-private */
    public static <T> zzdu<T> zza(zzex<?, ?> zzex, zzbu<?> zzbu, zzdo zzdo) {
        return new zzdu<>(zzex, zzbu, zzdo);
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final boolean equals(T t, T t2) {
        if (!this.zzmx.zzq(t).equals(this.zzmx.zzq(t2))) {
            return false;
        }
        if (this.zzmo) {
            return this.zzmy.zza(t).equals(this.zzmy.zza(t2));
        }
        return true;
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final int hashCode(T t) {
        int hashCode = this.zzmx.zzq(t).hashCode();
        return this.zzmo ? (hashCode * 53) + this.zzmy.zza(t).hashCode() : hashCode;
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final T newInstance() {
        return (T) this.zzmn.zzbd().zzbi();
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final void zza(T t, zzfr zzfr) throws IOException {
        Iterator<Map.Entry<?, Object>> it = this.zzmy.zza(t).iterator();
        while (it.hasNext()) {
            Map.Entry<?, Object> next = it.next();
            zzca zzca = (zzca) next.getKey();
            if (zzca.zzav() != zzfq.MESSAGE || zzca.zzaw() || zzca.zzax()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
            zzfr.zza(zzca.zzc(), next instanceof zzct ? ((zzct) next).zzbs().zzr() : next.getValue());
        }
        zzex<?, ?> zzex = this.zzmx;
        zzex.zzc(zzex.zzq(t), zzfr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0061 A[EDGE_INSN: B:50:0x0061->B:26:0x0061 ?: BREAK  , SYNTHETIC] */
    @Override // com.google.android.gms.internal.clearcut.zzef
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r8, byte[] r9, int r10, int r11, com.google.android.gms.internal.clearcut.zzay r12) throws java.io.IOException {
        /*
            r7 = this;
            com.google.android.gms.internal.clearcut.zzcg r8 = (com.google.android.gms.internal.clearcut.zzcg) r8
            com.google.android.gms.internal.clearcut.zzey r0 = r8.zzjp
            com.google.android.gms.internal.clearcut.zzey r1 = com.google.android.gms.internal.clearcut.zzey.zzea()
            if (r0 != r1) goto L_0x0010
            com.google.android.gms.internal.clearcut.zzey r0 = com.google.android.gms.internal.clearcut.zzey.zzeb()
            r8.zzjp = r0
        L_0x0010:
            r8 = r0
        L_0x0011:
            if (r10 >= r11) goto L_0x006b
            int r2 = com.google.android.gms.internal.clearcut.zzax.zza(r9, r10, r12)
            int r0 = r12.zzfd
            r10 = 11
            r1 = 2
            if (r0 == r10) goto L_0x0030
            r10 = r0 & 7
            if (r10 != r1) goto L_0x002b
            r1 = r9
            r3 = r11
            r4 = r8
            r5 = r12
            int r10 = com.google.android.gms.internal.clearcut.zzax.zza(r0, r1, r2, r3, r4, r5)
            goto L_0x0011
        L_0x002b:
            int r10 = com.google.android.gms.internal.clearcut.zzax.zza(r0, r9, r2, r11, r12)
            goto L_0x0011
        L_0x0030:
            r10 = 0
            r0 = 0
        L_0x0032:
            if (r2 >= r11) goto L_0x0061
            int r2 = com.google.android.gms.internal.clearcut.zzax.zza(r9, r2, r12)
            int r3 = r12.zzfd
            int r4 = r3 >>> 3
            r5 = r3 & 7
            if (r4 == r1) goto L_0x004f
            r6 = 3
            if (r4 == r6) goto L_0x0044
            goto L_0x0058
        L_0x0044:
            if (r5 != r1) goto L_0x0058
            int r2 = com.google.android.gms.internal.clearcut.zzax.zze(r9, r2, r12)
            java.lang.Object r0 = r12.zzff
            com.google.android.gms.internal.clearcut.zzbb r0 = (com.google.android.gms.internal.clearcut.zzbb) r0
            goto L_0x0032
        L_0x004f:
            if (r5 != 0) goto L_0x0058
            int r2 = com.google.android.gms.internal.clearcut.zzax.zza(r9, r2, r12)
            int r10 = r12.zzfd
            goto L_0x0032
        L_0x0058:
            r4 = 12
            if (r3 == r4) goto L_0x0061
            int r2 = com.google.android.gms.internal.clearcut.zzax.zza(r3, r9, r2, r11, r12)
            goto L_0x0032
        L_0x0061:
            if (r0 == 0) goto L_0x0069
            int r10 = r10 << 3
            r10 = r10 | r1
            r8.zzb(r10, r0)
        L_0x0069:
            r10 = r2
            goto L_0x0011
        L_0x006b:
            if (r10 != r11) goto L_0x006e
            return
        L_0x006e:
            com.google.android.gms.internal.clearcut.zzco r8 = com.google.android.gms.internal.clearcut.zzco.zzbo()
            goto L_0x0074
        L_0x0073:
            throw r8
        L_0x0074:
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.clearcut.zzdu.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.clearcut.zzay):void");
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final void zzc(T t) {
        this.zzmx.zzc(t);
        this.zzmy.zzc(t);
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final void zzc(T t, T t2) {
        zzeh.zza(this.zzmx, t, t2);
        if (this.zzmo) {
            zzeh.zza(this.zzmy, t, t2);
        }
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final int zzm(T t) {
        zzex<?, ?> zzex = this.zzmx;
        int zzr = zzex.zzr(zzex.zzq(t)) + 0;
        return this.zzmo ? zzr + this.zzmy.zza(t).zzat() : zzr;
    }

    @Override // com.google.android.gms.internal.clearcut.zzef
    public final boolean zzo(T t) {
        return this.zzmy.zza(t).isInitialized();
    }
}
