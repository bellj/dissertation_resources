package com.google.android.gms.internal.clearcut;

import java.util.Arrays;

/* loaded from: classes.dex */
final class zzbd implements zzbf {
    private zzbd() {
    }

    /* access modifiers changed from: package-private */
    public /* synthetic */ zzbd(zzbc zzbc) {
        this();
    }

    @Override // com.google.android.gms.internal.clearcut.zzbf
    public final byte[] zzc(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
