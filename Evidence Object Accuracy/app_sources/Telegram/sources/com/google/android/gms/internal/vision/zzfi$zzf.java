package com.google.android.gms.internal.vision;

import com.google.android.gms.internal.vision.zzjb;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* loaded from: classes.dex */
public final class zzfi$zzf extends zzjb<zzfi$zzf, zzb> implements zzkm {
    private static final zzfi$zzf zzl;
    private static volatile zzkx<zzfi$zzf> zzm;
    private int zzc;
    private String zzd = "";
    private String zze = "";
    private zzjl<String> zzf = zzjb.zzo();
    private int zzg;
    private String zzh = "";
    private long zzi;
    private long zzj;
    private zzjl<zzfi$zzn> zzk = zzjb.zzo();

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public enum zza implements zzje {
        RESULT_UNKNOWN(0),
        RESULT_SUCCESS(1),
        RESULT_FAIL(2),
        RESULT_SKIPPED(3);
        
        private final int zzf;

        @Override // com.google.android.gms.internal.vision.zzje
        public final int zza() {
            return this.zzf;
        }

        public static zza zza(int i) {
            if (i == 0) {
                return RESULT_UNKNOWN;
            }
            if (i == 1) {
                return RESULT_SUCCESS;
            }
            if (i == 2) {
                return RESULT_FAIL;
            }
            if (i != 3) {
                return null;
            }
            return RESULT_SKIPPED;
        }

        public static zzjg zzb() {
            return zzfo.zza;
        }

        @Override // java.lang.Enum, java.lang.Object
        public final String toString() {
            return "<" + zza.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzf + " name=" + name() + '>';
        }

        zza(int i) {
            this.zzf = i;
        }

        static {
            new zzfp();
        }
    }

    private zzfi$zzf() {
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* loaded from: classes.dex */
    public static final class zzb extends zzjb.zzb<zzfi$zzf, zzb> implements zzkm {
        private zzb() {
            super(zzfi$zzf.zzl);
        }

        public final zzb zza(String str) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zzf) this.zza).zza(str);
            return this;
        }

        public final zzb zza(long j) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zzf) this.zza).zza(j);
            return this;
        }

        public final zzb zzb(long j) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zzf) this.zza).zzb(j);
            return this;
        }

        public final zzb zza(Iterable<? extends zzfi$zzn> iterable) {
            if (this.zzb) {
                zzb();
                this.zzb = false;
            }
            ((zzfi$zzf) this.zza).zza(iterable);
            return this;
        }

        /* synthetic */ zzb(zzfk zzfk) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    /* access modifiers changed from: private */
    public final void zza(long j) {
        this.zzc |= 16;
        this.zzi = j;
    }

    /* access modifiers changed from: private */
    public final void zzb(long j) {
        this.zzc |= 32;
        this.zzj = j;
    }

    /* access modifiers changed from: private */
    public final void zza(Iterable<? extends zzfi$zzn> iterable) {
        zzjl<zzfi$zzn> zzjl = this.zzk;
        if (!zzjl.zza()) {
            this.zzk = zzjb.zza(zzjl);
        }
        zzhf.zza(iterable, this.zzk);
    }

    public static zzb zza() {
        return zzl.zzj();
    }

    /* JADX WARN: Type inference failed for: r2v14, types: [com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzf>, com.google.android.gms.internal.vision.zzjb$zza] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.google.android.gms.internal.vision.zzjb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object zza(int r2, java.lang.Object r3, java.lang.Object r4) {
        /*
            r1 = this;
            int[] r3 = com.google.android.gms.internal.vision.zzfk.zza
            r4 = 1
            int r2 = r2 - r4
            r2 = r3[r2]
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0081;
                case 2: goto L_0x007b;
                case 3: goto L_0x0033;
                case 4: goto L_0x0030;
                case 5: goto L_0x0016;
                case 6: goto L_0x0011;
                case 7: goto L_0x0010;
                default: goto L_0x000a;
            }
        L_0x000a:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            r2.<init>()
            throw r2
        L_0x0010:
            return r3
        L_0x0011:
            java.lang.Byte r2 = java.lang.Byte.valueOf(r4)
            return r2
        L_0x0016:
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzf> r2 = com.google.android.gms.internal.vision.zzfi$zzf.zzm
            if (r2 != 0) goto L_0x002f
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zzf> r3 = com.google.android.gms.internal.vision.zzfi$zzf.class
            monitor-enter(r3)
            com.google.android.gms.internal.vision.zzkx<com.google.android.gms.internal.vision.zzfi$zzf> r2 = com.google.android.gms.internal.vision.zzfi$zzf.zzm     // Catch: all -> 0x002c
            if (r2 != 0) goto L_0x002a
            com.google.android.gms.internal.vision.zzjb$zza r2 = new com.google.android.gms.internal.vision.zzjb$zza     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzf r4 = com.google.android.gms.internal.vision.zzfi$zzf.zzl     // Catch: all -> 0x002c
            r2.<init>(r4)     // Catch: all -> 0x002c
            com.google.android.gms.internal.vision.zzfi$zzf.zzm = r2     // Catch: all -> 0x002c
        L_0x002a:
            monitor-exit(r3)     // Catch: all -> 0x002c
            goto L_0x002f
        L_0x002c:
            r2 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x002c
            throw r2
        L_0x002f:
            return r2
        L_0x0030:
            com.google.android.gms.internal.vision.zzfi$zzf r2 = com.google.android.gms.internal.vision.zzfi$zzf.zzl
            return r2
        L_0x0033:
            r2 = 11
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            java.lang.String r0 = "zzc"
            r2[r3] = r0
            java.lang.String r3 = "zzd"
            r2[r4] = r3
            r3 = 2
            java.lang.String r4 = "zze"
            r2[r3] = r4
            r3 = 3
            java.lang.String r4 = "zzf"
            r2[r3] = r4
            r3 = 4
            java.lang.String r4 = "zzg"
            r2[r3] = r4
            r3 = 5
            com.google.android.gms.internal.vision.zzjg r4 = com.google.android.gms.internal.vision.zzfi$zzf.zza.zzb()
            r2[r3] = r4
            r3 = 6
            java.lang.String r4 = "zzh"
            r2[r3] = r4
            r3 = 7
            java.lang.String r4 = "zzi"
            r2[r3] = r4
            r3 = 8
            java.lang.String r4 = "zzj"
            r2[r3] = r4
            r3 = 9
            java.lang.String r4 = "zzk"
            r2[r3] = r4
            r3 = 10
            java.lang.Class<com.google.android.gms.internal.vision.zzfi$zzn> r4 = com.google.android.gms.internal.vision.zzfi$zzn.class
            r2[r3] = r4
            java.lang.String r3 = "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0002\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003\u001a\u0004ဌ\u0002\u0005ဈ\u0003\u0006ဂ\u0004\u0007ဂ\u0005\b\u001b"
            com.google.android.gms.internal.vision.zzfi$zzf r4 = com.google.android.gms.internal.vision.zzfi$zzf.zzl
            java.lang.Object r2 = com.google.android.gms.internal.vision.zzjb.zza(r4, r3, r2)
            return r2
        L_0x007b:
            com.google.android.gms.internal.vision.zzfi$zzf$zzb r2 = new com.google.android.gms.internal.vision.zzfi$zzf$zzb
            r2.<init>(r3)
            return r2
        L_0x0081:
            com.google.android.gms.internal.vision.zzfi$zzf r2 = new com.google.android.gms.internal.vision.zzfi$zzf
            r2.<init>()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.vision.zzfi$zzf.zza(int, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    static {
        zzfi$zzf zzfi_zzf = new zzfi$zzf();
        zzl = zzfi_zzf;
        zzjb.zza(zzfi$zzf.class, zzfi_zzf);
    }
}
