package com.google.android.gms.phenotype;

import android.net.Uri;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.phenotype.zzd;
import com.google.android.gms.internal.phenotype.zze;

/* loaded from: classes.dex */
public final class Phenotype {
    private static final Api.AbstractClientBuilder<zze, Api.ApiOptions.NoOptions> CLIENT_BUILDER;
    private static final Api.ClientKey<zze> CLIENT_KEY;

    static {
        Api.ClientKey<zze> clientKey = new Api.ClientKey<>();
        CLIENT_KEY = clientKey;
        zzl zzl = new zzl();
        CLIENT_BUILDER = zzl;
        new Api("Phenotype.API", zzl, clientKey);
        new zzd();
    }

    public static Uri getContentProviderUri(String str) {
        String valueOf = String.valueOf(Uri.encode(str));
        return Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
    }
}
