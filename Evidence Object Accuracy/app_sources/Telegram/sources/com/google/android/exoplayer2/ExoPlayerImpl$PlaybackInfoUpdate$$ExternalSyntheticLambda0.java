package com.google.android.exoplayer2;

import com.google.android.exoplayer2.BasePlayer;
import com.google.android.exoplayer2.ExoPlayerImpl;
import com.google.android.exoplayer2.Player;

/* loaded from: classes.dex */
public final /* synthetic */ class ExoPlayerImpl$PlaybackInfoUpdate$$ExternalSyntheticLambda0 implements BasePlayer.ListenerInvocation {
    public final /* synthetic */ ExoPlayerImpl.PlaybackInfoUpdate f$0;

    public /* synthetic */ ExoPlayerImpl$PlaybackInfoUpdate$$ExternalSyntheticLambda0(ExoPlayerImpl.PlaybackInfoUpdate playbackInfoUpdate) {
        this.f$0 = playbackInfoUpdate;
    }

    @Override // com.google.android.exoplayer2.BasePlayer.ListenerInvocation
    public final void invokeListener(Player.EventListener eventListener) {
        ExoPlayerImpl.PlaybackInfoUpdate.m19$r8$lambda$06WAXA7vLSBfQvmAx45uxQjmso(this.f$0, eventListener);
    }
}
