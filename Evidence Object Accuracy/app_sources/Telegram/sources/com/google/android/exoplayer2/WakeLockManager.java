package com.google.android.exoplayer2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class WakeLockManager {
    private boolean enabled;
    private boolean stayAwake;
    private PowerManager.WakeLock wakeLock;

    public WakeLockManager(Context context) {
        PowerManager powerManager = (PowerManager) context.getApplicationContext().getSystemService("power");
    }

    public void setStayAwake(boolean z) {
        this.stayAwake = z;
        updateWakeLock();
    }

    @SuppressLint({"WakelockTimeout"})
    private void updateWakeLock() {
        PowerManager.WakeLock wakeLock = this.wakeLock;
        if (wakeLock != null) {
            if (!this.enabled || !this.stayAwake) {
                wakeLock.release();
            } else {
                wakeLock.acquire();
            }
        }
    }
}
