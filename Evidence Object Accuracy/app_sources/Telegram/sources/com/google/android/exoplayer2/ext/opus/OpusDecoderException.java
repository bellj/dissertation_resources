package com.google.android.exoplayer2.ext.opus;

import com.google.android.exoplayer2.audio.AudioDecoderException;

/* loaded from: classes.dex */
public final class OpusDecoderException extends AudioDecoderException {
    /* access modifiers changed from: package-private */
    public OpusDecoderException(String str) {
        super(str);
    }

    /* access modifiers changed from: package-private */
    public OpusDecoderException(String str, Throwable th) {
        super(str, th);
    }
}
