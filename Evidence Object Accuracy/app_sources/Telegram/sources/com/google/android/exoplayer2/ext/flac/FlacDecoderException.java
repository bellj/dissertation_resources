package com.google.android.exoplayer2.ext.flac;

import com.google.android.exoplayer2.audio.AudioDecoderException;

/* loaded from: classes.dex */
public final class FlacDecoderException extends AudioDecoderException {
    /* access modifiers changed from: package-private */
    public FlacDecoderException(String str) {
        super(str);
    }

    /* access modifiers changed from: package-private */
    public FlacDecoderException(String str, Throwable th) {
        super(str, th);
    }
}
