package com.google.android.exoplayer2;

import android.content.Context;
import android.net.wifi.WifiManager;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class WifiLockManager {
    private boolean enabled;
    private boolean stayAwake;
    private WifiManager.WifiLock wifiLock;

    public WifiLockManager(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi");
    }

    public void setStayAwake(boolean z) {
        this.stayAwake = z;
        updateWifiLock();
    }

    private void updateWifiLock() {
        WifiManager.WifiLock wifiLock = this.wifiLock;
        if (wifiLock != null) {
            if (!this.enabled || !this.stayAwake) {
                wifiLock.release();
            } else {
                wifiLock.acquire();
            }
        }
    }
}
