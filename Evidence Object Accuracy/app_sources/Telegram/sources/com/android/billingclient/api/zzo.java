package com.android.billingclient.api;

import android.content.Context;
import android.content.IntentFilter;

/* compiled from: com.android.billingclient:billing@@5.0.0 */
/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class zzo {
    private final Context zza;
    private final zzn zzb;

    /* access modifiers changed from: package-private */
    public zzo(Context context, zzbe zzbe) {
        this.zza = context;
        this.zzb = new zzn(this, null, null);
    }

    /* access modifiers changed from: package-private */
    public final zzbe zzb() {
        zzn.zza(this.zzb);
        return null;
    }

    /* access modifiers changed from: package-private */
    public final PurchasesUpdatedListener zzc() {
        return zzn.zzb(this.zzb);
    }

    /* access modifiers changed from: package-private */
    public final void zze() {
        IntentFilter intentFilter = new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
        intentFilter.addAction("com.android.vending.billing.ALTERNATIVE_BILLING");
        this.zzb.zzc(this.zza, intentFilter);
    }

    /* access modifiers changed from: package-private */
    public zzo(Context context, PurchasesUpdatedListener purchasesUpdatedListener, zzc zzc) {
        this.zza = context;
        this.zzb = new zzn(this, purchasesUpdatedListener, zzc, null);
    }
}
