package j$.util;

import j$.util.function.Consumer;
import j$.util.function.f;

/* loaded from: classes2.dex */
public interface t extends w {
    @Override // j$.util.u
    boolean b(Consumer consumer);

    void e(f fVar);

    @Override // j$.util.u
    void forEachRemaining(Consumer consumer);

    boolean k(f fVar);

    @Override // j$.util.w, j$.util.u
    t trySplit();
}
