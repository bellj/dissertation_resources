package j$.util;

import j$.util.function.Consumer;
import j$.util.function.f;

/* renamed from: j$.util.n  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC0016n extends p {
    void e(f fVar);

    @Override // j$.util.Iterator
    void forEachRemaining(Consumer consumer);

    @Override // java.util.Iterator, j$.util.Iterator
    Double next();

    double nextDouble();
}
