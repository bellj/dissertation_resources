package j$.util.function;

/* loaded from: classes2.dex */
public interface Predicate<T> {

    /* renamed from: j$.util.function.Predicate$-CC  reason: invalid class name */
    /* loaded from: classes2.dex */
    public final /* synthetic */ class CC {
        /* JADX WARN: Incorrect args count in method signature: (Lj$/util/function/Predicate<-TT;>;)Lj$/util/function/Predicate<TT;>; */
        public static Predicate $default$and(Predicate predicate, Predicate predicate2) {
            predicate2.getClass();
            return new x(predicate, predicate2, 0);
        }

        /* JADX WARN: Incorrect args count in method signature: ()Lj$/util/function/Predicate<TT;>; */
        public static Predicate $default$negate(Predicate predicate) {
            return new C0008a(predicate);
        }

        /* JADX WARN: Incorrect args count in method signature: (Lj$/util/function/Predicate<-TT;>;)Lj$/util/function/Predicate<TT;>; */
        public static Predicate $default$or(Predicate predicate, Predicate predicate2) {
            predicate2.getClass();
            return new x(predicate, predicate2, 1);
        }
    }

    Predicate<T> and(Predicate<? super T> predicate);

    Predicate<T> negate();

    Predicate<T> or(Predicate<? super T> predicate);

    boolean test(T t);
}
