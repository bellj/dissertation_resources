package j$.util;

import j$.util.function.Consumer;
import j$.util.function.l;
import java.util.Iterator;

/* loaded from: classes2.dex */
public interface p extends Iterator {

    /* loaded from: classes2.dex */
    public interface a extends p {
        void c(l lVar);

        @Override // j$.util.Iterator
        void forEachRemaining(Consumer consumer);

        @Override // java.util.Iterator
        Integer next();

        int nextInt();
    }

    void forEachRemaining(Object obj);
}
