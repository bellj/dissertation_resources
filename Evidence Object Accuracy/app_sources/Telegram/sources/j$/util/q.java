package j$.util;

import j$.util.function.Consumer;
import j$.util.function.p;

/* loaded from: classes2.dex */
public final /* synthetic */ class q implements j$.util.function.q {
    public final /* synthetic */ Consumer a;

    public /* synthetic */ q(Consumer consumer) {
        this.a = consumer;
    }

    @Override // j$.util.function.q
    public final void accept(long j) {
        this.a.accept(Long.valueOf(j));
    }

    @Override // j$.util.function.q
    public j$.util.function.q f(j$.util.function.q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }
}
