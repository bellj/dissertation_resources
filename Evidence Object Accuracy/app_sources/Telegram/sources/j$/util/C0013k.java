package j$.util;

import java.util.NoSuchElementException;

/* renamed from: j$.util.k  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0013k {
    private static final C0013k c = new C0013k();
    private final boolean a;
    private final int b;

    private C0013k() {
        this.a = false;
        this.b = 0;
    }

    private C0013k(int i) {
        this.a = true;
        this.b = i;
    }

    public static C0013k a() {
        return c;
    }

    public static C0013k d(int i) {
        return new C0013k(i);
    }

    public int b() {
        if (this.a) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public boolean c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0013k)) {
            return false;
        }
        C0013k kVar = (C0013k) obj;
        boolean z = this.a;
        if (!z || !kVar.a) {
            if (z == kVar.a) {
                return true;
            }
        } else if (this.b == kVar.b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.a) {
            return this.b;
        }
        return 0;
    }

    public String toString() {
        return this.a ? String.format("OptionalInt[%s]", Integer.valueOf(this.b)) : "OptionalInt.empty";
    }
}
