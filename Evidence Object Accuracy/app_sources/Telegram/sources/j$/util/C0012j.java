package j$.util;

import java.util.NoSuchElementException;

/* renamed from: j$.util.j  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0012j {
    private static final C0012j c = new C0012j();
    private final boolean a;
    private final double b;

    private C0012j() {
        this.a = false;
        this.b = Double.NaN;
    }

    private C0012j(double d) {
        this.a = true;
        this.b = d;
    }

    public static C0012j a() {
        return c;
    }

    public static C0012j d(double d) {
        return new C0012j(d);
    }

    public double b() {
        if (this.a) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public boolean c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0012j)) {
            return false;
        }
        C0012j jVar = (C0012j) obj;
        boolean z = this.a;
        if (!z || !jVar.a) {
            if (z == jVar.a) {
                return true;
            }
        } else if (Double.compare(this.b, jVar.b) == 0) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (!this.a) {
            return 0;
        }
        long doubleToLongBits = Double.doubleToLongBits(this.b);
        return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
    }

    public String toString() {
        return this.a ? String.format("OptionalDouble[%s]", Double.valueOf(this.b)) : "OptionalDouble.empty";
    }
}
