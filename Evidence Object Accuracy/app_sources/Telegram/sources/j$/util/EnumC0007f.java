package j$.util;

import j$.util.function.A;
import j$.util.function.z;
import j$.wrappers.B0;
import j$.wrappers.D0;
import j$.wrappers.F0;
import j$.wrappers.M;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.f  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC0007f implements Comparator, AbstractC0006e {
    INSTANCE;

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((Comparable) obj).compareTo((Comparable) obj2);
    }

    @Override // java.util.Comparator
    public Comparator reversed() {
        return Comparator$CC.reverseOrder();
    }

    @Override // java.util.Comparator
    public Comparator thenComparing(Comparator comparator) {
        comparator.getClass();
        return new C0004c(this, comparator);
    }

    @Override // java.util.Comparator
    public Comparator thenComparingDouble(ToDoubleFunction toDoubleFunction) {
        z a2 = B0.a(toDoubleFunction);
        a2.getClass();
        return AbstractC0002a.H(this, new C0005d(a2));
    }

    @Override // java.util.Comparator
    public Comparator thenComparingInt(ToIntFunction toIntFunction) {
        return AbstractC0002a.H(this, Comparator$CC.comparingInt(D0.a(toIntFunction)));
    }

    @Override // java.util.Comparator
    public Comparator thenComparingLong(ToLongFunction toLongFunction) {
        A a2 = F0.a(toLongFunction);
        a2.getClass();
        return AbstractC0002a.H(this, new C0005d(a2));
    }

    @Override // java.util.Comparator
    public Comparator thenComparing(Function function) {
        return AbstractC0002a.H(this, Comparator$CC.comparing(M.a(function)));
    }

    @Override // java.util.Comparator
    public Comparator thenComparing(Function function, Comparator comparator) {
        j$.util.function.Function a2 = M.a(function);
        a2.getClass();
        comparator.getClass();
        return AbstractC0002a.H(this, new C0004c(comparator, a2));
    }
}
