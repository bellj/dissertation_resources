package j$.util;

import j$.util.function.Consumer;
import j$.util.function.q;

/* loaded from: classes2.dex */
public interface r extends p {
    void d(q qVar);

    @Override // j$.util.Iterator
    void forEachRemaining(Consumer consumer);

    @Override // java.util.Iterator
    Long next();

    long nextLong();
}
