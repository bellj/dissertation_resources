package j$.util;

import j$.util.function.Consumer;
import j$.util.function.q;

/* loaded from: classes2.dex */
public interface v extends w {
    @Override // j$.util.u
    boolean b(Consumer consumer);

    void d(q qVar);

    @Override // j$.util.u
    void forEachRemaining(Consumer consumer);

    boolean i(q qVar);

    @Override // j$.util.w, j$.util.u
    v trySplit();
}
