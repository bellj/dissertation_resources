package j$.util;

import j$.util.Iterator;
import j$.util.function.Consumer;
import j$.util.function.p;
import j$.util.function.q;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
class z implements r, q, Iterator {
    boolean a = false;
    long b;
    final /* synthetic */ v c;

    /* access modifiers changed from: package-private */
    public z(v vVar) {
        this.c = vVar;
    }

    @Override // j$.util.function.q
    public void accept(long j) {
        this.a = true;
        this.b = j;
    }

    @Override // j$.util.r
    /* renamed from: d */
    public void forEachRemaining(q qVar) {
        qVar.getClass();
        while (hasNext()) {
            qVar.accept(nextLong());
        }
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    @Override // j$.util.r, j$.util.Iterator
    public void forEachRemaining(Consumer consumer) {
        if (consumer instanceof q) {
            forEachRemaining((q) consumer);
            return;
        }
        consumer.getClass();
        if (!N.a) {
            forEachRemaining(new q(consumer));
        } else {
            N.a(z.class, "{0} calling PrimitiveIterator.OfLong.forEachRemainingLong(action::accept)");
            throw null;
        }
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public boolean hasNext() {
        if (!this.a) {
            this.c.i(this);
        }
        return this.a;
    }

    @Override // j$.util.r, java.util.Iterator
    public Long next() {
        if (!N.a) {
            return Long.valueOf(nextLong());
        }
        N.a(z.class, "{0} calling PrimitiveIterator.OfLong.nextLong()");
        throw null;
    }

    @Override // j$.util.r
    public long nextLong() {
        if (this.a || hasNext()) {
            this.a = false;
            return this.b;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public /* synthetic */ void remove() {
        Iterator.CC.a(this);
        throw null;
    }
}
