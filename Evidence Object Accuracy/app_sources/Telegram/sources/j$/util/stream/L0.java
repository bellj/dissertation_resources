package j$.util.stream;

import j$.util.C0010h;
import j$.util.C0012j;
import j$.util.C0013k;
import j$.util.L;
import j$.util.function.BiConsumer;
import j$.util.function.j;
import j$.util.function.l;
import j$.util.function.m;
import j$.util.function.n;
import j$.util.function.v;
import j$.util.function.y;
import j$.util.p;
import j$.util.u;
import j$.wrappers.C0166b0;
import j$.wrappers.V;
import j$.wrappers.X;
import java.util.Iterator;

/* loaded from: classes2.dex */
abstract class L0 extends AbstractC0029c implements IntStream {
    /* access modifiers changed from: package-private */
    public L0(AbstractC0029c cVar, int i) {
        super(cVar, i);
    }

    /* access modifiers changed from: package-private */
    public L0(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    /* access modifiers changed from: private */
    public static u.a M0(u uVar) {
        if (uVar instanceof u.a) {
            return (u.a) uVar;
        }
        if (Q4.a) {
            Q4.a(AbstractC0029c.class, "using IntStream.adapt(Spliterator<Integer> s)");
            throw null;
        }
        throw new UnsupportedOperationException("IntStream.adapt(Spliterator<Integer> s)");
    }

    @Override // j$.util.stream.IntStream
    public final U A(X x) {
        x.getClass();
        return new K(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n, x);
    }

    @Override // j$.util.stream.AbstractC0029c
    final void A0(u uVar, AbstractC0093m3 m3Var) {
        l lVar;
        u.a M0 = M0(uVar);
        if (m3Var instanceof l) {
            lVar = (l) m3Var;
        } else if (!Q4.a) {
            lVar = new B0(m3Var);
        } else {
            Q4.a(AbstractC0029c.class, "using IntStream.adapt(Sink<Integer> s)");
            throw null;
        }
        while (!m3Var.o() && M0.g(lVar)) {
        }
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final EnumC0046e4 B0() {
        return EnumC0046e4.INT_VALUE;
    }

    @Override // j$.util.stream.IntStream
    public final boolean C(V v) {
        return ((Boolean) x0(AbstractC0103o1.v(v, EnumC0079k1.ALL))).booleanValue();
    }

    @Override // j$.util.stream.IntStream
    public final boolean F(V v) {
        return ((Boolean) x0(AbstractC0103o1.v(v, EnumC0079k1.ANY))).booleanValue();
    }

    @Override // j$.util.stream.IntStream
    public void I(l lVar) {
        lVar.getClass();
        x0(new C0084l0(lVar, true));
    }

    @Override // j$.util.stream.IntStream
    public final Stream J(m mVar) {
        mVar.getClass();
        return new L(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n, mVar);
    }

    @Override // j$.util.stream.AbstractC0029c
    final u K0(AbstractC0156y2 y2Var, y yVar, boolean z) {
        return new C0118q4(y2Var, yVar, z);
    }

    @Override // j$.util.stream.IntStream
    public final int N(int i, j jVar) {
        jVar.getClass();
        return ((Integer) x0(new L2(EnumC0046e4.INT_VALUE, jVar, i))).intValue();
    }

    @Override // j$.util.stream.IntStream
    public final IntStream P(m mVar) {
        return new M(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, mVar);
    }

    @Override // j$.util.stream.IntStream
    public void U(l lVar) {
        lVar.getClass();
        x0(new C0084l0(lVar, false));
    }

    @Override // j$.util.stream.IntStream
    public final C0013k a0(j jVar) {
        jVar.getClass();
        return (C0013k) x0(new D2(EnumC0046e4.INT_VALUE, jVar));
    }

    @Override // j$.util.stream.IntStream
    public final U asDoubleStream() {
        return new O(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n);
    }

    @Override // j$.util.stream.IntStream
    public final AbstractC0043e1 asLongStream() {
        return new G0(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n);
    }

    @Override // j$.util.stream.IntStream
    public final C0012j average() {
        long[] jArr = (long[]) k0(C0139v0.a, C0134u0.a, C0149x0.a);
        if (jArr[0] <= 0) {
            return C0012j.a();
        }
        double d = (double) jArr[1];
        double d2 = (double) jArr[0];
        Double.isNaN(d);
        Double.isNaN(d2);
        return C0012j.d(d / d2);
    }

    @Override // j$.util.stream.IntStream
    public final Stream boxed() {
        return J(C0.a);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream c0(l lVar) {
        lVar.getClass();
        return new M(this, this, EnumC0046e4.INT_VALUE, 0, lVar);
    }

    @Override // j$.util.stream.IntStream
    public final long count() {
        return ((AbstractC0037d1) f(E0.a)).sum();
    }

    @Override // j$.util.stream.IntStream
    public final IntStream distinct() {
        return ((AbstractC0045e3) J(C0.a)).distinct().m(C0144w0.a);
    }

    @Override // j$.util.stream.IntStream
    public final AbstractC0043e1 f(n nVar) {
        nVar.getClass();
        return new N(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n, nVar);
    }

    @Override // j$.util.stream.IntStream
    public final C0013k findAny() {
        return (C0013k) x0(new C0036d0(false, EnumC0046e4.INT_VALUE, C0013k.a(), X.a, C0018a0.a));
    }

    @Override // j$.util.stream.IntStream
    public final C0013k findFirst() {
        return (C0013k) x0(new C0036d0(true, EnumC0046e4.INT_VALUE, C0013k.a(), X.a, C0018a0.a));
    }

    @Override // j$.util.stream.IntStream
    public final IntStream h(V v) {
        v.getClass();
        return new M(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.t, v);
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    public final p.a mo66iterator() {
        return L.g(spliterator());
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator  reason: collision with other method in class */
    public Iterator mo66iterator() {
        return L.g(spliterator());
    }

    @Override // j$.util.stream.IntStream
    public final Object k0(y yVar, v vVar, BiConsumer biConsumer) {
        C c = new C(biConsumer, 1);
        yVar.getClass();
        vVar.getClass();
        return x0(new C0161z2(EnumC0046e4.INT_VALUE, c, vVar, yVar));
    }

    @Override // j$.util.stream.IntStream
    public final IntStream limit(long j) {
        if (j >= 0) {
            return B3.g(this, 0, j);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.IntStream
    public final C0013k max() {
        return a0(C0159z0.a);
    }

    @Override // j$.util.stream.IntStream
    public final C0013k min() {
        return a0(A0.a);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream q(C0166b0 b0Var) {
        b0Var.getClass();
        return new M(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.p | EnumC0040d4.n, b0Var);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream skip(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i >= 0) {
            return i == 0 ? this : B3.g(this, j, -1);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.IntStream
    public final IntStream sorted() {
        return new K3(this);
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g
    public final u.a spliterator() {
        return M0(super.spliterator());
    }

    @Override // j$.util.stream.IntStream
    public final int sum() {
        return ((Integer) x0(new L2(EnumC0046e4.INT_VALUE, C0154y0.a, 0))).intValue();
    }

    @Override // j$.util.stream.IntStream
    public final C0010h summaryStatistics() {
        return (C0010h) k0(C0071j.a, C0129t0.a, C0124s0.a);
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final AbstractC0125s1 t0(long j, m mVar) {
        return AbstractC0151x2.p(j);
    }

    @Override // j$.util.stream.IntStream
    public final int[] toArray() {
        return (int[]) AbstractC0151x2.n((AbstractC0145w1) y0(D0.a)).e();
    }

    @Override // j$.util.stream.AbstractC0053g
    public AbstractC0053g unordered() {
        return !C0() ? this : new H0(this, this, EnumC0046e4.INT_VALUE, EnumC0040d4.r);
    }

    @Override // j$.util.stream.IntStream
    public final boolean v(V v) {
        return ((Boolean) x0(AbstractC0103o1.v(v, EnumC0079k1.NONE))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0029c
    final A1 z0(AbstractC0156y2 y2Var, u uVar, boolean z, m mVar) {
        return AbstractC0151x2.g(y2Var, uVar, z);
    }
}
