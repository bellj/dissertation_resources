package j$.util.stream;

import j$.util.concurrent.ConcurrentHashMap;
import j$.util.u;
import java.util.concurrent.CountedCompleter;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.q0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0114q0 extends CountedCompleter {
    public static final /* synthetic */ int h = 0;
    private final AbstractC0156y2 a;
    private u b;
    private final long c;
    private final ConcurrentHashMap d;
    private final AbstractC0093m3 e;
    private final C0114q0 f;
    private A1 g;

    C0114q0(C0114q0 q0Var, u uVar, C0114q0 q0Var2) {
        super(q0Var);
        this.a = q0Var.a;
        this.b = uVar;
        this.c = q0Var.c;
        this.d = q0Var.d;
        this.e = q0Var.e;
        this.f = q0Var2;
    }

    /* access modifiers changed from: protected */
    public C0114q0(AbstractC0156y2 y2Var, u uVar, AbstractC0093m3 m3Var) {
        super(null);
        this.a = y2Var;
        this.b = uVar;
        this.c = AbstractC0047f.h(uVar.estimateSize());
        this.d = new ConcurrentHashMap(Math.max(16, AbstractC0047f.g << 1));
        this.e = m3Var;
        this.f = null;
    }

    @Override // java.util.concurrent.CountedCompleter
    public final void compute() {
        u trySplit;
        u uVar = this.b;
        long j = this.c;
        boolean z = false;
        C0114q0 q0Var = this;
        while (uVar.estimateSize() > j && (trySplit = uVar.trySplit()) != null) {
            C0114q0 q0Var2 = new C0114q0(q0Var, trySplit, q0Var.f);
            C0114q0 q0Var3 = new C0114q0(q0Var, uVar, q0Var2);
            q0Var.addToPendingCount(1);
            q0Var3.addToPendingCount(1);
            q0Var.d.put(q0Var2, q0Var3);
            if (q0Var.f != null) {
                q0Var2.addToPendingCount(1);
                if (q0Var.d.replace(q0Var.f, q0Var, q0Var2)) {
                    q0Var.addToPendingCount(-1);
                } else {
                    q0Var2.addToPendingCount(-1);
                }
            }
            if (z) {
                uVar = trySplit;
                q0Var = q0Var2;
                q0Var2 = q0Var3;
            } else {
                q0Var = q0Var3;
            }
            z = !z;
            q0Var2.fork();
        }
        if (q0Var.getPendingCount() > 0) {
            C0108p0 p0Var = C0108p0.a;
            AbstractC0156y2 y2Var = q0Var.a;
            AbstractC0125s1 t0 = y2Var.t0(y2Var.q0(uVar), p0Var);
            AbstractC0029c cVar = (AbstractC0029c) q0Var.a;
            cVar.getClass();
            t0.getClass();
            cVar.n0(cVar.v0(t0), uVar);
            q0Var.g = t0.mo70a();
            q0Var.b = null;
        }
        q0Var.tryComplete();
    }

    @Override // java.util.concurrent.CountedCompleter
    public void onCompletion(CountedCompleter countedCompleter) {
        A1 a1 = this.g;
        if (a1 != null) {
            a1.forEach(this.e);
            this.g = null;
        } else {
            u uVar = this.b;
            if (uVar != null) {
                AbstractC0156y2 y2Var = this.a;
                AbstractC0093m3 m3Var = this.e;
                AbstractC0029c cVar = (AbstractC0029c) y2Var;
                cVar.getClass();
                m3Var.getClass();
                cVar.n0(cVar.v0(m3Var), uVar);
                this.b = null;
            }
        }
        C0114q0 q0Var = (C0114q0) this.d.remove(this);
        if (q0Var != null) {
            q0Var.tryComplete();
        }
    }
}
