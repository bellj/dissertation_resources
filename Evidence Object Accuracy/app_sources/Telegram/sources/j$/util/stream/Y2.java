package j$.util.stream;

import j$.lang.a;
import j$.util.function.A;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.function.ToIntFunction;
import j$.util.function.z;

/* loaded from: classes2.dex */
class Y2 extends AbstractC0069i3 {
    public final /* synthetic */ int b = 5;
    final /* synthetic */ Object c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(K k, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = k;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        switch (this.b) {
            case 0:
                ((Consumer) ((L) this.c).m).accept(obj);
                this.a.accept((AbstractC0093m3) obj);
                return;
            case 1:
                if (((Predicate) ((L) this.c).m).test(obj)) {
                    this.a.accept((AbstractC0093m3) obj);
                    return;
                }
                return;
            case 2:
                this.a.accept((AbstractC0093m3) ((C0021a3) this.c).m.apply(obj));
                return;
            case 3:
                this.a.accept(((ToIntFunction) ((M) this.c).m).applyAsInt(obj));
                return;
            case 4:
                this.a.accept(((A) ((N) this.c).m).applyAsLong(obj));
                return;
            case 5:
                this.a.accept(((z) ((K) this.c).m).applyAsDouble(obj));
                return;
            default:
                Stream stream = (Stream) ((C0021a3) this.c).m.apply(obj);
                if (stream != null) {
                    try {
                        ((Stream) stream.sequential()).forEach(this.a);
                    } catch (Throwable th) {
                        try {
                            stream.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                if (stream != null) {
                    stream.close();
                    return;
                }
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        switch (this.b) {
            case 1:
                this.a.n(-1);
                return;
            case 6:
                this.a.n(-1);
                return;
            default:
                this.a.n(j);
                return;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(L l, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = l;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(L l, AbstractC0093m3 m3Var, a aVar) {
        super(m3Var);
        this.c = l;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(M m, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = m;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(N n, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = n;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(C0021a3 a3Var, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = a3Var;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Y2(C0021a3 a3Var, AbstractC0093m3 m3Var, a aVar) {
        super(m3Var);
        this.c = a3Var;
    }
}
