package j$.util.stream;

import j$.util.C0014l;
import j$.util.function.p;
import j$.util.function.q;

/* renamed from: j$.util.stream.g0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0054g0 extends AbstractC0066i0 implements AbstractC0087l3 {
    @Override // j$.util.stream.AbstractC0066i0, j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        accept(Long.valueOf(j));
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    @Override // j$.util.function.y
    public Object get() {
        if (this.a) {
            return C0014l.d(((Long) this.b).longValue());
        }
        return null;
    }
}
