package j$.util.stream;

import j$.util.concurrent.ConcurrentHashMap;
import j$.util.function.m;
import j$.util.u;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.s  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public class C0123s extends AbstractC0033c3 {
    /* access modifiers changed from: package-private */
    public C0123s(AbstractC0029c cVar, EnumC0046e4 e4Var, int i) {
        super(cVar, e4Var, i);
    }

    @Override // j$.util.stream.AbstractC0029c
    A1 E0(AbstractC0156y2 y2Var, u uVar, m mVar) {
        if (EnumC0040d4.DISTINCT.d(y2Var.s0())) {
            return y2Var.p0(uVar, false, mVar);
        }
        if (EnumC0040d4.ORDERED.d(y2Var.s0())) {
            return L0(y2Var, uVar);
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        new C0096n0(new C0101o(atomicBoolean, concurrentHashMap), false).c(y2Var, uVar);
        Collection keySet = concurrentHashMap.keySet();
        if (atomicBoolean.get()) {
            HashSet hashSet = new HashSet(keySet);
            hashSet.add(null);
            keySet = hashSet;
        }
        return new E1(keySet);
    }

    @Override // j$.util.stream.AbstractC0029c
    u F0(AbstractC0156y2 y2Var, u uVar) {
        return EnumC0040d4.DISTINCT.d(y2Var.s0()) ? y2Var.w0(uVar) : EnumC0040d4.ORDERED.d(y2Var.s0()) ? ((E1) L0(y2Var, uVar)).mo69spliterator() : new C0094m4(y2Var.w0(uVar));
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        m3Var.getClass();
        return EnumC0040d4.DISTINCT.d(i) ? m3Var : EnumC0040d4.SORTED.d(i) ? new C0113q(this, m3Var) : new r(this, m3Var);
    }

    A1 L0(AbstractC0156y2 y2Var, u uVar) {
        C0107p pVar = C0107p.a;
        C0089m mVar = C0089m.a;
        return new E1((Collection) new C0161z2(EnumC0046e4.REFERENCE, C0095n.a, mVar, pVar).c(y2Var, uVar));
    }
}
