package j$.util.stream;

import j$.util.Comparator$CC;
import j$.util.function.m;
import j$.util.u;
import java.util.Arrays;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class M3 extends AbstractC0033c3 {
    private final boolean l;
    private final Comparator m;

    /* access modifiers changed from: package-private */
    public M3(AbstractC0029c cVar) {
        super(cVar, EnumC0046e4.REFERENCE, EnumC0040d4.q | EnumC0040d4.o);
        this.l = true;
        this.m = Comparator$CC.a();
    }

    /* access modifiers changed from: package-private */
    public M3(AbstractC0029c cVar, Comparator comparator) {
        super(cVar, EnumC0046e4.REFERENCE, EnumC0040d4.q | EnumC0040d4.p);
        this.l = false;
        comparator.getClass();
        this.m = comparator;
    }

    @Override // j$.util.stream.AbstractC0029c
    public A1 E0(AbstractC0156y2 y2Var, u uVar, m mVar) {
        if (EnumC0040d4.SORTED.d(y2Var.s0()) && this.l) {
            return y2Var.p0(uVar, false, mVar);
        }
        Object[] q = y2Var.p0(uVar, true, mVar).q(mVar);
        Arrays.sort(q, this.m);
        return new D1(q);
    }

    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        m3Var.getClass();
        return (!EnumC0040d4.SORTED.d(i) || !this.l) ? EnumC0040d4.SIZED.d(i) ? new R3(m3Var, this.m) : new N3(m3Var, this.m) : m3Var;
    }
}
