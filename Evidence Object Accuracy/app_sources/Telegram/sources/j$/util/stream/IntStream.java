package j$.util.stream;

import j$.util.C0010h;
import j$.util.C0012j;
import j$.util.C0013k;
import j$.util.function.BiConsumer;
import j$.util.function.j;
import j$.util.function.l;
import j$.util.function.m;
import j$.util.function.n;
import j$.util.function.v;
import j$.util.function.y;
import j$.util.p;
import j$.util.u;
import j$.wrappers.C0166b0;
import j$.wrappers.V;
import j$.wrappers.X;

/* loaded from: classes2.dex */
public interface IntStream extends AbstractC0053g {
    U A(X x);

    boolean C(V v);

    boolean F(V v);

    void I(l lVar);

    Stream J(m mVar);

    int N(int i, j jVar);

    IntStream P(m mVar);

    void U(l lVar);

    C0013k a0(j jVar);

    U asDoubleStream();

    AbstractC0043e1 asLongStream();

    C0012j average();

    Stream boxed();

    IntStream c0(l lVar);

    long count();

    IntStream distinct();

    AbstractC0043e1 f(n nVar);

    C0013k findAny();

    C0013k findFirst();

    IntStream h(V v);

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    p.a mo66iterator();

    Object k0(y yVar, v vVar, BiConsumer biConsumer);

    IntStream limit(long j);

    C0013k max();

    C0013k min();

    IntStream parallel();

    IntStream q(C0166b0 b0Var);

    IntStream sequential();

    IntStream skip(long j);

    IntStream sorted();

    @Override // j$.util.stream.AbstractC0053g
    u.a spliterator();

    int sum();

    C0010h summaryStatistics();

    int[] toArray();

    boolean v(V v);
}
