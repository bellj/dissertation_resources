package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.e;
import j$.util.function.f;

/* renamed from: j$.util.stream.n4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0100n4 implements AbstractC0075j3 {
    public final /* synthetic */ int a = 0;
    public final /* synthetic */ Object b;

    public /* synthetic */ C0100n4(f fVar) {
        this.b = fVar;
    }

    @Override // j$.util.stream.AbstractC0075j3, j$.util.stream.AbstractC0093m3
    public final void accept(double d) {
        switch (this.a) {
            case 0:
                ((f) this.b).accept(d);
                return;
            default:
                ((U3) this.b).accept(d);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    public /* synthetic */ void b(Double d) {
        switch (this.a) {
            case 0:
                AbstractC0103o1.a(this, d);
                return;
            default:
                AbstractC0103o1.a(this, d);
                return;
        }
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        switch (this.a) {
            case 0:
                fVar.getClass();
                return new e(this, fVar);
            default:
                fVar.getClass();
                return new e(this, fVar);
        }
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void n(long j) {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }

    public /* synthetic */ C0100n4(U3 u3) {
        this.b = u3;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        switch (this.a) {
            case 0:
                AbstractC0103o1.d(this);
                throw null;
            default:
                AbstractC0103o1.d(this);
                throw null;
        }
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        switch (this.a) {
            case 0:
                AbstractC0103o1.e(this);
                throw null;
            default:
                AbstractC0103o1.e(this);
                throw null;
        }
    }

    @Override // j$.util.function.Consumer
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        switch (this.a) {
            case 0:
                b((Double) obj);
                return;
            default:
                b((Double) obj);
                return;
        }
    }
}
