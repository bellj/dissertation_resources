package j$.util.stream;

import j$.util.function.e;
import j$.util.function.f;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.k0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0078k0 extends AbstractC0102o0 implements AbstractC0075j3 {
    final f b;

    /* access modifiers changed from: package-private */
    public C0078k0(f fVar, boolean z) {
        super(z);
        this.b = fVar;
    }

    @Override // j$.util.stream.AbstractC0102o0, j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        this.b.accept(d);
    }

    /* renamed from: e */
    public /* synthetic */ void accept(Double d) {
        AbstractC0103o1.a(this, d);
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }
}
