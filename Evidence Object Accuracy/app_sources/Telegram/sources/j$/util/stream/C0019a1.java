package j$.util.stream;

import j$.util.function.q;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.a1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public class C0019a1 extends AbstractC0037d1 {
    /* access modifiers changed from: package-private */
    public C0019a1(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    @Override // j$.util.stream.AbstractC0029c
    final boolean G0() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0037d1, j$.util.stream.AbstractC0043e1
    public void Z(q qVar) {
        if (!isParallel()) {
            AbstractC0037d1.M0(J0()).d(qVar);
            return;
        }
        qVar.getClass();
        x0(new C0090m0(qVar, true));
    }

    @Override // j$.util.stream.AbstractC0037d1, j$.util.stream.AbstractC0043e1
    public void d(q qVar) {
        if (!isParallel()) {
            AbstractC0037d1.M0(J0()).d(qVar);
        } else {
            super.d(qVar);
        }
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ AbstractC0043e1 parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ AbstractC0043e1 sequential() {
        sequential();
        return this;
    }
}
