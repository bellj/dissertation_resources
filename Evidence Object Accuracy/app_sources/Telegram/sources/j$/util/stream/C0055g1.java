package j$.util.stream;

import j$.util.function.k;
import j$.util.function.l;
import j$.wrappers.V;

/* renamed from: j$.util.stream.g1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0055g1 extends AbstractC0073j1 implements AbstractC0081k3 {
    final /* synthetic */ EnumC0079k1 c;
    final /* synthetic */ V d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0055g1(EnumC0079k1 k1Var, V v) {
        super(k1Var);
        this.c = k1Var;
        this.d = v;
    }

    @Override // j$.util.stream.AbstractC0073j1, j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        if (!this.a && this.d.b(i) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Integer num) {
        AbstractC0103o1.b(this, num);
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }
}
