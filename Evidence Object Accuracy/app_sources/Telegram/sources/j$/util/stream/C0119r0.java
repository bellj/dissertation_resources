package j$.util.stream;

import j$.util.u;
import java.util.concurrent.CountedCompleter;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.r0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0119r0 extends CountedCompleter {
    private u a;
    private final AbstractC0093m3 b;
    private final AbstractC0156y2 c;
    private long d;

    C0119r0(C0119r0 r0Var, u uVar) {
        super(r0Var);
        this.a = uVar;
        this.b = r0Var.b;
        this.d = r0Var.d;
        this.c = r0Var.c;
    }

    /* access modifiers changed from: package-private */
    public C0119r0(AbstractC0156y2 y2Var, u uVar, AbstractC0093m3 m3Var) {
        super(null);
        this.b = m3Var;
        this.c = y2Var;
        this.a = uVar;
        this.d = 0;
    }

    @Override // java.util.concurrent.CountedCompleter
    public void compute() {
        u trySplit;
        u uVar = this.a;
        long estimateSize = uVar.estimateSize();
        long j = this.d;
        if (j == 0) {
            j = AbstractC0047f.h(estimateSize);
            this.d = j;
        }
        boolean d = EnumC0040d4.SHORT_CIRCUIT.d(this.c.s0());
        boolean z = false;
        AbstractC0093m3 m3Var = this.b;
        C0119r0 r0Var = this;
        while (true) {
            if (d && m3Var.o()) {
                break;
            } else if (estimateSize <= j || (trySplit = uVar.trySplit()) == null) {
                break;
            } else {
                r0Var = new C0119r0(r0Var, trySplit);
                r0Var.addToPendingCount(1);
                if (z) {
                    uVar = trySplit;
                } else {
                    r0Var = r0Var;
                    r0Var = r0Var;
                }
                z = !z;
                r0Var.fork();
                estimateSize = uVar.estimateSize();
            }
        }
        r0Var.c.n0(m3Var, uVar);
        r0Var.a = null;
        r0Var.propagateCompletion();
    }
}
