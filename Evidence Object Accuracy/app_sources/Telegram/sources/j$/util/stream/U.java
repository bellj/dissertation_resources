package j$.util.stream;

import j$.util.AbstractC0016n;
import j$.util.C0009g;
import j$.util.C0012j;
import j$.util.function.BiConsumer;
import j$.util.function.d;
import j$.util.function.f;
import j$.util.function.g;
import j$.util.function.h;
import j$.util.function.u;
import j$.util.function.y;
import j$.util.t;
import j$.wrappers.E;
import j$.wrappers.G;
import j$.wrappers.K;

/* loaded from: classes2.dex */
public interface U extends AbstractC0053g {
    C0012j G(d dVar);

    Object H(y yVar, u uVar, BiConsumer biConsumer);

    double K(double d, d dVar);

    Stream M(g gVar);

    IntStream R(G g);

    boolean Y(E e);

    C0012j average();

    U b(f fVar);

    Stream boxed();

    long count();

    U distinct();

    C0012j findAny();

    C0012j findFirst();

    boolean h0(E e);

    boolean i0(E e);

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    AbstractC0016n mo66iterator();

    void j(f fVar);

    void l0(f fVar);

    U limit(long j);

    C0012j max();

    C0012j min();

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    U parallel();

    U r(E e);

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    U sequential();

    U skip(long j);

    U sorted();

    @Override // j$.util.stream.AbstractC0053g
    t spliterator();

    double sum();

    C0009g summaryStatistics();

    double[] toArray();

    U w(g gVar);

    AbstractC0043e1 x(h hVar);

    U y(K k);
}
