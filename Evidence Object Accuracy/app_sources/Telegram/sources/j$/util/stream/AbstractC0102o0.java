package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.o0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0102o0 implements N4, O4 {
    private final boolean a;

    /* access modifiers changed from: protected */
    public AbstractC0102o0(boolean z) {
        this.a = z;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.N4
    public int b() {
        if (this.a) {
            return 0;
        }
        return EnumC0040d4.r;
    }

    @Override // j$.util.stream.N4
    public Object c(AbstractC0156y2 y2Var, u uVar) {
        (this.a ? new C0114q0(y2Var, uVar, this) : new C0119r0(y2Var, uVar, y2Var.v0(this))).invoke();
        return null;
    }

    @Override // j$.util.stream.N4
    public Object d(AbstractC0156y2 y2Var, u uVar) {
        AbstractC0029c cVar = (AbstractC0029c) y2Var;
        cVar.n0(cVar.v0(this), uVar);
        return null;
    }

    @Override // j$.util.function.y
    public /* bridge */ /* synthetic */ Object get() {
        return null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void n(long j) {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
