package j$.util.stream;

import j$.util.function.p;
import j$.util.function.q;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.q2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0116q2 extends AbstractC0126s2 implements AbstractC0087l3 {
    private final long[] h;

    C0116q2(C0116q2 q2Var, u uVar, long j, long j2) {
        super(q2Var, uVar, j, j2, q2Var.h.length);
        this.h = q2Var.h;
    }

    /* access modifiers changed from: package-private */
    public C0116q2(u uVar, AbstractC0156y2 y2Var, long[] jArr) {
        super(uVar, y2Var, jArr.length);
        this.h = jArr;
    }

    @Override // j$.util.stream.AbstractC0126s2, j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        int i = this.f;
        if (i < this.g) {
            long[] jArr = this.h;
            this.f = i + 1;
            jArr[i] = j;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.stream.AbstractC0126s2
    AbstractC0126s2 b(u uVar, long j, long j2) {
        return new C0116q2(this, uVar, j, j2);
    }

    /* renamed from: c */
    public /* synthetic */ void accept(Long l) {
        AbstractC0103o1.c(this, l);
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }
}
