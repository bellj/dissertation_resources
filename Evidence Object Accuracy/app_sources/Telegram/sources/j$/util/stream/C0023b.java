package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.c;
import j$.util.function.m;
import j$.util.function.r;
import j$.util.function.y;
import j$.util.u;
import java.util.List;

/* renamed from: j$.util.stream.b  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0023b implements y, r, Consumer, c {
    public final /* synthetic */ int a = 2;
    public final /* synthetic */ Object b;

    public /* synthetic */ C0023b(u uVar) {
        this.b = uVar;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        switch (this.a) {
            case 3:
                ((AbstractC0093m3) this.b).accept((AbstractC0093m3) obj);
                return;
            default:
                ((List) this.b).add(obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 3:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.function.r
    public Object apply(long j) {
        int i = H1.k;
        return AbstractC0151x2.d(j, (m) this.b);
    }

    @Override // j$.util.function.y
    public Object get() {
        switch (this.a) {
            case 0:
                return (u) this.b;
            default:
                return ((AbstractC0029c) this.b).D0();
        }
    }

    public /* synthetic */ C0023b(m mVar) {
        this.b = mVar;
    }

    public /* synthetic */ C0023b(AbstractC0029c cVar) {
        this.b = cVar;
    }

    public /* synthetic */ C0023b(AbstractC0093m3 m3Var) {
        this.b = m3Var;
    }

    public /* synthetic */ C0023b(C0106o4 o4Var) {
        this.b = o4Var;
    }

    public /* synthetic */ C0023b(C0118q4 q4Var) {
        this.b = q4Var;
    }

    public /* synthetic */ C0023b(s4 s4Var) {
        this.b = s4Var;
    }

    public /* synthetic */ C0023b(L4 l4) {
        this.b = l4;
    }

    public /* synthetic */ C0023b(List list) {
        this.b = list;
    }
}
