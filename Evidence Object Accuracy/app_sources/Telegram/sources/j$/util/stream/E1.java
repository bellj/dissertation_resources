package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.AbstractC0003b;
import j$.util.Collection$EL;
import j$.util.function.Consumer;
import j$.util.function.m;
import j$.util.u;
import java.util.Collection;

/* loaded from: classes2.dex */
final class E1 implements A1 {
    private final Collection a;

    /* access modifiers changed from: package-private */
    public E1(Collection collection) {
        this.a = collection;
    }

    @Override // j$.util.stream.A1
    public A1 b(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A1
    public long count() {
        return (long) this.a.size();
    }

    @Override // j$.util.stream.A1
    public void forEach(Consumer consumer) {
        Collection$EL.a(this.a, consumer);
    }

    @Override // j$.util.stream.A1
    public void i(Object[] objArr, int i) {
        for (Object obj : this.a) {
            i++;
            objArr[i] = obj;
        }
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ int p() {
        return 0;
    }

    @Override // j$.util.stream.A1
    public Object[] q(m mVar) {
        Collection collection = this.a;
        return collection.toArray((Object[]) mVar.apply(collection.size()));
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ A1 r(long j, long j2, m mVar) {
        return AbstractC0103o1.q(this, j, j2, mVar);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator */
    public u mo69spliterator() {
        Collection collection = this.a;
        return (collection instanceof AbstractC0003b ? ((AbstractC0003b) collection).stream() : AbstractC0002a.i(collection)).spliterator();
    }

    public String toString() {
        return String.format("CollectionNode[%d][%s]", Integer.valueOf(this.a.size()), this.a);
    }
}
