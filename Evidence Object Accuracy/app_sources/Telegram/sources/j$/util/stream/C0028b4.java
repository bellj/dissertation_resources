package j$.util.stream;

import java.util.Map;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.b4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public class C0028b4 {
    final Map a;

    /* access modifiers changed from: package-private */
    public C0028b4(Map map) {
        this.a = map;
    }

    /* access modifiers changed from: package-private */
    public C0028b4 a(EnumC0034c4 c4Var) {
        this.a.put(c4Var, 2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public C0028b4 b(EnumC0034c4 c4Var) {
        this.a.put(c4Var, 1);
        return this;
    }

    /* access modifiers changed from: package-private */
    public C0028b4 c(EnumC0034c4 c4Var) {
        this.a.put(c4Var, 3);
        return this;
    }
}
