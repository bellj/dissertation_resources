package j$.util.stream;

import j$.util.u;
import j$.util.w;
import java.util.Deque;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.i2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0068i2 extends AbstractC0080k2 implements w {
    /* access modifiers changed from: package-private */
    public AbstractC0068i2(AbstractC0160z1 z1Var) {
        super(z1Var);
    }

    @Override // j$.util.w
    /* renamed from: forEachRemaining */
    public void e(Object obj) {
        if (this.a != null) {
            if (this.d == null) {
                u uVar = this.c;
                if (uVar == null) {
                    Deque f = f();
                    while (true) {
                        AbstractC0160z1 z1Var = (AbstractC0160z1) a(f);
                        if (z1Var != null) {
                            z1Var.g(obj);
                        } else {
                            this.a = null;
                            return;
                        }
                    }
                } else {
                    ((w) uVar).forEachRemaining(obj);
                }
            } else {
                do {
                } while (k(obj));
            }
        }
    }

    @Override // j$.util.w
    /* renamed from: tryAdvance */
    public boolean k(Object obj) {
        AbstractC0160z1 z1Var;
        if (!h()) {
            return false;
        }
        boolean tryAdvance = ((w) this.d).tryAdvance(obj);
        if (!tryAdvance) {
            if (this.c != null || (z1Var = (AbstractC0160z1) a(this.e)) == null) {
                this.a = null;
            } else {
                w spliterator = z1Var.mo69spliterator();
                this.d = spliterator;
                return spliterator.tryAdvance(obj);
            }
        }
        return tryAdvance;
    }
}
