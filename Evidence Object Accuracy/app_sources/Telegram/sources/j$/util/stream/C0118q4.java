package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.function.l;
import j$.util.function.y;
import j$.util.u;

/* renamed from: j$.util.stream.q4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0118q4 extends AbstractC0052f4 implements u.a {
    /* access modifiers changed from: package-private */
    public C0118q4(AbstractC0156y2 y2Var, y yVar, boolean z) {
        super(y2Var, yVar, z);
    }

    C0118q4(AbstractC0156y2 y2Var, u uVar, boolean z) {
        super(y2Var, uVar, z);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.k(this, consumer);
    }

    @Override // j$.util.u.a
    /* renamed from: c */
    public void forEachRemaining(l lVar) {
        if (this.h != null || this.i) {
            do {
            } while (tryAdvance(lVar));
            return;
        }
        lVar.getClass();
        h();
        this.b.u0(new C0112p4(lVar), this.d);
        this.i = true;
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.c(this, consumer);
    }

    @Override // j$.util.u.a
    /* renamed from: g */
    public boolean tryAdvance(l lVar) {
        lVar.getClass();
        boolean a = a();
        if (a) {
            W3 w3 = (W3) this.h;
            long j = this.g;
            int w = w3.w(j);
            lVar.accept((w3.c == 0 && w == 0) ? ((int[]) w3.e)[(int) j] : ((int[][]) w3.f)[w][(int) (j - w3.d[w])]);
        }
        return a;
    }

    @Override // j$.util.stream.AbstractC0052f4
    void j() {
        W3 w3 = new W3();
        this.h = w3;
        this.e = this.b.v0(new C0112p4(w3));
        this.f = new C0023b(this);
    }

    @Override // j$.util.stream.AbstractC0052f4
    AbstractC0052f4 l(u uVar) {
        return new C0118q4(this.b, uVar, this.a);
    }

    @Override // j$.util.stream.AbstractC0052f4, j$.util.u
    public u.a trySplit() {
        return (u.a) super.trySplit();
    }
}
