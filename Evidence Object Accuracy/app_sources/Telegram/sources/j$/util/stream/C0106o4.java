package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.function.f;
import j$.util.function.y;
import j$.util.t;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.o4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0106o4 extends AbstractC0052f4 implements t {
    /* access modifiers changed from: package-private */
    public C0106o4(AbstractC0156y2 y2Var, y yVar, boolean z) {
        super(y2Var, yVar, z);
    }

    C0106o4(AbstractC0156y2 y2Var, u uVar, boolean z) {
        super(y2Var, uVar, z);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.j(this, consumer);
    }

    @Override // j$.util.t
    /* renamed from: e */
    public void forEachRemaining(f fVar) {
        if (this.h != null || this.i) {
            do {
            } while (tryAdvance(fVar));
            return;
        }
        fVar.getClass();
        h();
        this.b.u0(new C0100n4(fVar), this.d);
        this.i = true;
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.b(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0052f4
    void j() {
        U3 u3 = new U3();
        this.h = u3;
        this.e = this.b.v0(new C0100n4(u3));
        this.f = new C0023b(this);
    }

    @Override // j$.util.t
    /* renamed from: k */
    public boolean tryAdvance(f fVar) {
        fVar.getClass();
        boolean a = a();
        if (a) {
            U3 u3 = (U3) this.h;
            long j = this.g;
            int w = u3.w(j);
            fVar.accept((u3.c == 0 && w == 0) ? ((double[]) u3.e)[(int) j] : ((double[][]) u3.f)[w][(int) (j - u3.d[w])]);
        }
        return a;
    }

    @Override // j$.util.stream.AbstractC0052f4
    AbstractC0052f4 l(u uVar) {
        return new C0106o4(this.b, uVar, this.a);
    }

    @Override // j$.util.stream.AbstractC0052f4, j$.util.u
    public t trySplit() {
        return (t) super.trySplit();
    }
}
