package j$.util.stream;

import j$.lang.a;
import j$.lang.b;
import j$.lang.c;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.t;
import j$.wrappers.C0182j0;
import j$.wrappers.C0186l0;
import j$.wrappers.C0190n0;

/* loaded from: classes2.dex */
class Z0 extends AbstractC0063h3 {
    public final /* synthetic */ int b = 4;
    final /* synthetic */ Object c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(K k, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = k;
    }

    @Override // j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        switch (this.b) {
            case 0:
                this.a.accept((double) j);
                return;
            case 1:
                this.a.accept(((t) ((N) this.c).m).applyAsLong(j));
                return;
            case 2:
                this.a.accept((AbstractC0093m3) ((r) ((L) this.c).m).apply(j));
                return;
            case 3:
                this.a.accept(((C0190n0) ((M) this.c).m).a(j));
                return;
            case 4:
                this.a.accept(((C0186l0) ((K) this.c).m).a(j));
                return;
            case 5:
                AbstractC0043e1 e1Var = (AbstractC0043e1) ((r) ((N) this.c).m).apply(j);
                if (e1Var != null) {
                    try {
                        e1Var.sequential().d(new W0(this));
                    } catch (Throwable th) {
                        try {
                            e1Var.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                if (e1Var != null) {
                    e1Var.close();
                    return;
                }
                return;
            case 6:
                if (((C0182j0) ((N) this.c).m).b(j)) {
                    this.a.accept(j);
                    return;
                }
                return;
            default:
                ((q) ((N) this.c).m).accept(j);
                this.a.accept(j);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        switch (this.b) {
            case 5:
                this.a.n(-1);
                return;
            case 6:
                this.a.n(-1);
                return;
            default:
                this.a.n(j);
                return;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(L l, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = l;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(M m, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = m;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(N n, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = n;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(N n, AbstractC0093m3 m3Var, a aVar) {
        super(m3Var);
        this.c = n;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(N n, AbstractC0093m3 m3Var, b bVar) {
        super(m3Var);
        this.c = n;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(N n, AbstractC0093m3 m3Var, c cVar) {
        super(m3Var);
        this.c = n;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Z0(O o, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.c = o;
    }
}
