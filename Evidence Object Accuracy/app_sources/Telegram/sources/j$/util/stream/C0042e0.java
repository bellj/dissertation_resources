package j$.util.stream;

import j$.util.C0012j;
import j$.util.function.e;
import j$.util.function.f;

/* renamed from: j$.util.stream.e0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0042e0 extends AbstractC0066i0 implements AbstractC0075j3 {
    @Override // j$.util.stream.AbstractC0066i0, j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        accept(Double.valueOf(d));
    }

    @Override // j$.util.function.y
    public Object get() {
        if (this.a) {
            return C0012j.d(((Double) this.b).doubleValue());
        }
        return null;
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }
}
