package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.b;
import j$.util.function.e;
import j$.util.function.f;
import j$.util.function.u;
import j$.util.function.y;

/* loaded from: classes2.dex */
class F2 extends T2 implements S2, AbstractC0075j3 {
    final /* synthetic */ y b;
    final /* synthetic */ u c;
    final /* synthetic */ b d;

    /* access modifiers changed from: package-private */
    public F2(y yVar, u uVar, b bVar) {
        this.b = yVar;
        this.c = uVar;
        this.d = bVar;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        this.c.accept(this.a, d);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Double d) {
        AbstractC0103o1.a(this, d);
    }

    @Override // j$.util.stream.S2
    public void h(S2 s2) {
        this.a = this.d.apply(this.a, ((F2) s2).a);
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a = this.b.get();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
