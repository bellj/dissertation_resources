package j$.util.stream;

import j$.util.function.m;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.y2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0156y2 {
    /* access modifiers changed from: package-private */
    public abstract void n0(AbstractC0093m3 m3Var, u uVar);

    /* access modifiers changed from: package-private */
    public abstract void o0(AbstractC0093m3 m3Var, u uVar);

    /* access modifiers changed from: package-private */
    public abstract A1 p0(u uVar, boolean z, m mVar);

    /* access modifiers changed from: package-private */
    public abstract long q0(u uVar);

    /* access modifiers changed from: package-private */
    public abstract EnumC0046e4 r0();

    /* access modifiers changed from: package-private */
    public abstract int s0();

    /* access modifiers changed from: package-private */
    public abstract AbstractC0125s1 t0(long j, m mVar);

    /* access modifiers changed from: package-private */
    public abstract AbstractC0093m3 u0(AbstractC0093m3 m3Var, u uVar);

    /* access modifiers changed from: package-private */
    public abstract AbstractC0093m3 v0(AbstractC0093m3 m3Var);

    /* access modifiers changed from: package-private */
    public abstract u w0(u uVar);
}
