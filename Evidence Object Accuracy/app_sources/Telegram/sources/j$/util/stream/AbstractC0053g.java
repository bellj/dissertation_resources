package j$.util.stream;

import j$.util.u;
import java.util.Iterator;

/* renamed from: j$.util.stream.g  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC0053g extends AutoCloseable {
    @Override // java.lang.AutoCloseable
    void close();

    boolean isParallel();

    /* renamed from: iterator */
    Iterator mo66iterator();

    AbstractC0053g onClose(Runnable runnable);

    @Override // j$.util.stream.IntStream
    AbstractC0053g parallel();

    @Override // j$.util.stream.IntStream
    AbstractC0053g sequential();

    u spliterator();

    AbstractC0053g unordered();
}
