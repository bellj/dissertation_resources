package j$.util.stream;

import j$.util.function.y;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.l1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0085l1 implements N4 {
    private final EnumC0046e4 a;
    final EnumC0079k1 b;
    final y c;

    /* access modifiers changed from: package-private */
    public C0085l1(EnumC0046e4 e4Var, EnumC0079k1 k1Var, y yVar) {
        this.a = e4Var;
        this.b = k1Var;
        this.c = yVar;
    }

    @Override // j$.util.stream.N4
    public int b() {
        return EnumC0040d4.u | EnumC0040d4.r;
    }

    @Override // j$.util.stream.N4
    public Object c(AbstractC0156y2 y2Var, u uVar) {
        return (Boolean) new C0091m1(this, y2Var, uVar).invoke();
    }

    @Override // j$.util.stream.N4
    public Object d(AbstractC0156y2 y2Var, u uVar) {
        AbstractC0073j1 j1Var = (AbstractC0073j1) this.c.get();
        AbstractC0029c cVar = (AbstractC0029c) y2Var;
        j1Var.getClass();
        cVar.n0(cVar.v0(j1Var), uVar);
        return Boolean.valueOf(j1Var.b);
    }
}
