package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.y;
import j$.util.u;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class L4 extends AbstractC0052f4 {
    /* access modifiers changed from: package-private */
    public L4(AbstractC0156y2 y2Var, y yVar, boolean z) {
        super(y2Var, yVar, z);
    }

    L4(AbstractC0156y2 y2Var, u uVar, boolean z) {
        super(y2Var, uVar, z);
    }

    @Override // j$.util.u
    public boolean b(Consumer consumer) {
        Object obj;
        consumer.getClass();
        boolean a = a();
        if (a) {
            C0022a4 a4Var = (C0022a4) this.h;
            long j = this.g;
            if (a4Var.c == 0) {
                if (j < ((long) a4Var.b)) {
                    obj = a4Var.e[(int) j];
                } else {
                    throw new IndexOutOfBoundsException(Long.toString(j));
                }
            } else if (j < a4Var.count()) {
                for (int i = 0; i <= a4Var.c; i++) {
                    long[] jArr = a4Var.d;
                    long j2 = jArr[i];
                    Object[][] objArr = a4Var.f;
                    if (j < j2 + ((long) objArr[i].length)) {
                        obj = objArr[i][(int) (j - jArr[i])];
                    }
                }
                throw new IndexOutOfBoundsException(Long.toString(j));
            } else {
                throw new IndexOutOfBoundsException(Long.toString(j));
            }
            consumer.accept(obj);
        }
        return a;
    }

    @Override // j$.util.u
    public void forEachRemaining(Consumer consumer) {
        if (this.h != null || this.i) {
            do {
            } while (b(consumer));
            return;
        }
        consumer.getClass();
        h();
        this.b.u0(new K4(consumer), this.d);
        this.i = true;
    }

    @Override // j$.util.stream.AbstractC0052f4
    void j() {
        C0022a4 a4Var = new C0022a4();
        this.h = a4Var;
        this.e = this.b.v0(new K4(a4Var));
        this.f = new C0023b(this);
    }

    @Override // j$.util.stream.AbstractC0052f4
    AbstractC0052f4 l(u uVar) {
        return new L4(this.b, uVar, this.a);
    }
}
