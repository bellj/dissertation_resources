package j$.util.stream;

import j$.util.function.Consumer;

/* renamed from: j$.util.stream.k4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0082k4 extends AbstractC0088l4 implements Consumer {
    final Object[] b;

    /* access modifiers changed from: package-private */
    public C0082k4(int i) {
        this.b = new Object[i];
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        Object[] objArr = this.b;
        int i = this.a;
        this.a = i + 1;
        objArr[i] = obj;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }
}
