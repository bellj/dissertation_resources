package j$.util.stream;

import j$.util.function.b;
import j$.util.function.d;
import j$.util.function.j;
import j$.util.function.o;

/* loaded from: classes2.dex */
class D2 extends U2 {
    public final /* synthetic */ int b = 1;
    final /* synthetic */ Object c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public D2(EnumC0046e4 e4Var, b bVar) {
        super(e4Var);
        this.c = bVar;
    }

    @Override // j$.util.stream.U2
    public S2 a() {
        switch (this.b) {
            case 0:
                return new E2((d) this.c);
            case 1:
                return new H2((b) this.c);
            case 2:
                return new N2((j) this.c);
            default:
                return new R2((o) this.c);
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public D2(EnumC0046e4 e4Var, d dVar) {
        super(e4Var);
        this.c = dVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public D2(EnumC0046e4 e4Var, j jVar) {
        super(e4Var);
        this.c = jVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public D2(EnumC0046e4 e4Var, o oVar) {
        super(e4Var);
        this.c = oVar;
    }
}
