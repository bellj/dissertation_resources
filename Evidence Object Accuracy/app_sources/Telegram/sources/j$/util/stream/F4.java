package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.function.k;
import j$.util.function.l;
import j$.util.u;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class F4 extends H4 implements u.a, l {
    int e;

    /* access modifiers changed from: package-private */
    public F4(u.a aVar, long j, long j2) {
        super(aVar, j, j2);
    }

    F4(u.a aVar, F4 f4) {
        super(aVar, f4);
    }

    @Override // j$.util.function.l
    public void accept(int i) {
        this.e = i;
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.k(this, consumer);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.c(this, consumer);
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }

    @Override // j$.util.stream.J4
    protected u q(u uVar) {
        return new F4((u.a) uVar, this);
    }

    @Override // j$.util.stream.H4
    protected void s(Object obj) {
        ((l) obj).accept(this.e);
    }

    @Override // j$.util.stream.H4
    protected AbstractC0076j4 t(int i) {
        return new C0064h4(i);
    }
}
