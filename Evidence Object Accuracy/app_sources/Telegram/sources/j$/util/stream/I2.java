package j$.util.stream;

import j$.util.function.BiConsumer;
import j$.util.function.b;
import j$.util.function.y;
import j$.wrappers.J0;

/* loaded from: classes2.dex */
class I2 extends U2 {
    final /* synthetic */ b b;
    final /* synthetic */ BiConsumer c;
    final /* synthetic */ y d;
    final /* synthetic */ J0 e;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public I2(EnumC0046e4 e4Var, b bVar, BiConsumer biConsumer, y yVar, J0 j0) {
        super(e4Var);
        this.b = bVar;
        this.c = biConsumer;
        this.d = yVar;
        this.e = j0;
    }

    @Override // j$.util.stream.U2
    public S2 a() {
        return new J2(this.d, this.c, this.b);
    }

    @Override // j$.util.stream.U2, j$.util.stream.N4
    public int b() {
        if (this.e.b().contains(EnumC0059h.UNORDERED)) {
            return EnumC0040d4.r;
        }
        return 0;
    }
}
