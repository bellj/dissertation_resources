package j$.util.stream;

import j$.util.u;

/* loaded from: classes2.dex */
abstract class U2 implements N4 {
    private final EnumC0046e4 a;

    /* access modifiers changed from: package-private */
    public U2(EnumC0046e4 e4Var) {
        this.a = e4Var;
    }

    public abstract S2 a();

    @Override // j$.util.stream.N4
    public /* synthetic */ int b() {
        return 0;
    }

    @Override // j$.util.stream.N4
    public Object c(AbstractC0156y2 y2Var, u uVar) {
        return ((S2) new V2(this, y2Var, uVar).invoke()).get();
    }

    @Override // j$.util.stream.N4
    public Object d(AbstractC0156y2 y2Var, u uVar) {
        S2 a = a();
        AbstractC0029c cVar = (AbstractC0029c) y2Var;
        a.getClass();
        cVar.n0(cVar.v0(a), uVar);
        return a.get();
    }
}
