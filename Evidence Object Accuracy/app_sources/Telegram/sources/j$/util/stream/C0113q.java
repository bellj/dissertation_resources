package j$.util.stream;

/* renamed from: j$.util.stream.q  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0113q extends AbstractC0069i3 {
    boolean b;
    Object c;

    /* access modifiers changed from: package-private */
    public C0113q(C0123s sVar, AbstractC0093m3 m3Var) {
        super(m3Var);
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        if (obj != null) {
            Object obj2 = this.c;
            if (obj2 == null || !obj.equals(obj2)) {
                AbstractC0093m3 m3Var = this.a;
                this.c = obj;
                m3Var.accept((AbstractC0093m3) obj);
            }
        } else if (!this.b) {
            this.b = true;
            AbstractC0093m3 m3Var2 = this.a;
            this.c = null;
            m3Var2.accept((AbstractC0093m3) null);
        }
    }

    @Override // j$.util.stream.AbstractC0069i3, j$.util.stream.AbstractC0093m3
    public void m() {
        this.b = false;
        this.c = null;
        this.a.m();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.b = false;
        this.c = null;
        this.a.n(-1);
    }
}
