package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.o;
import j$.util.function.p;
import j$.util.function.q;

/* loaded from: classes2.dex */
class Q2 implements S2, AbstractC0087l3 {
    private long a;
    final /* synthetic */ long b;
    final /* synthetic */ o c;

    /* access modifiers changed from: package-private */
    public Q2(long j, o oVar) {
        this.b = j;
        this.c = oVar;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        this.a = this.c.applyAsLong(this.a, j);
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Long l) {
        AbstractC0103o1.c(this, l);
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    @Override // j$.util.function.y
    public Object get() {
        return Long.valueOf(this.a);
    }

    @Override // j$.util.stream.S2
    public void h(S2 s2) {
        accept(((Q2) s2).a);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a = this.b;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
