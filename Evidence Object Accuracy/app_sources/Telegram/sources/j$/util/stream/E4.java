package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.function.e;
import j$.util.function.f;
import j$.util.t;
import j$.util.u;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class E4 extends H4 implements t, f {
    double e;

    /* access modifiers changed from: package-private */
    public E4(t tVar, long j, long j2) {
        super(tVar, j, j2);
    }

    E4(t tVar, E4 e4) {
        super(tVar, e4);
    }

    @Override // j$.util.function.f
    public void accept(double d) {
        this.e = d;
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.j(this, consumer);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.b(this, consumer);
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }

    @Override // j$.util.stream.J4
    protected u q(u uVar) {
        return new E4((t) uVar, this);
    }

    @Override // j$.util.stream.H4
    protected void s(Object obj) {
        ((f) obj).accept(this.e);
    }

    @Override // j$.util.stream.H4
    protected AbstractC0076j4 t(int i) {
        return new C0058g4(i);
    }
}
