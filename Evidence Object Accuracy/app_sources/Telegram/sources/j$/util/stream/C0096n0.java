package j$.util.stream;

import j$.util.function.Consumer;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.n0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0096n0 extends AbstractC0102o0 {
    final Consumer b;

    /* access modifiers changed from: package-private */
    public C0096n0(Consumer consumer, boolean z) {
        super(z);
        this.b = consumer;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        this.b.accept(obj);
    }
}
