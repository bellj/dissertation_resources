package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.u;

/* renamed from: j$.util.stream.g2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0056g2 extends AbstractC0068i2 implements u.a {
    /* access modifiers changed from: package-private */
    public C0056g2(AbstractC0145w1 w1Var) {
        super(w1Var);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.k(this, consumer);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.c(this, consumer);
    }
}
