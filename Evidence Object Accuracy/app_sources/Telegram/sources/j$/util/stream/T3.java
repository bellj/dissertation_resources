package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.L;
import j$.util.function.Consumer;
import j$.util.function.f;
import j$.util.stream.Z3;
import j$.util.t;
import j$.util.w;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class T3 extends Z3.a implements t {
    final /* synthetic */ U3 g;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public T3(U3 u3, int i, int i2, int i3, int i4) {
        super(i, i2, i3, i4);
        this.g = u3;
    }

    @Override // j$.util.stream.Z3.a
    void a(Object obj, int i, Object obj2) {
        ((f) obj2).accept(((double[]) obj)[i]);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.j(this, consumer);
    }

    @Override // j$.util.stream.Z3.a
    w f(Object obj, int i, int i2) {
        return L.j((double[]) obj, i, i2 + i, 1040);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.b(this, consumer);
    }

    @Override // j$.util.stream.Z3.a
    w h(int i, int i2, int i3, int i4) {
        return new T3(this.g, i, i2, i3, i4);
    }
}
