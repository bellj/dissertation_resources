package j$.util.stream;

import j$.util.concurrent.a;
import j$.util.function.BiConsumer;

/* renamed from: j$.util.stream.x0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0149x0 implements BiConsumer {
    public static final /* synthetic */ C0149x0 a = new C0149x0();

    private /* synthetic */ C0149x0() {
    }

    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        long[] jArr = (long[]) obj;
        long[] jArr2 = (long[]) obj2;
        jArr[0] = jArr[0] + jArr2[0];
        jArr[1] = jArr[1] + jArr2[1];
    }

    @Override // j$.util.function.BiConsumer
    public BiConsumer b(BiConsumer biConsumer) {
        biConsumer.getClass();
        return new a(this, biConsumer);
    }
}
