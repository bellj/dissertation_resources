package j$.util.stream;

import j$.util.function.e;
import j$.util.function.f;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.o2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0104o2 extends AbstractC0126s2 implements AbstractC0075j3 {
    private final double[] h;

    C0104o2(C0104o2 o2Var, u uVar, long j, long j2) {
        super(o2Var, uVar, j, j2, o2Var.h.length);
        this.h = o2Var.h;
    }

    /* access modifiers changed from: package-private */
    public C0104o2(u uVar, AbstractC0156y2 y2Var, double[] dArr) {
        super(uVar, y2Var, dArr.length);
        this.h = dArr;
    }

    @Override // j$.util.stream.AbstractC0126s2, j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        int i = this.f;
        if (i < this.g) {
            double[] dArr = this.h;
            this.f = i + 1;
            dArr[i] = d;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.stream.AbstractC0126s2
    AbstractC0126s2 b(u uVar, long j, long j2) {
        return new C0104o2(this, uVar, j, j2);
    }

    /* renamed from: c */
    public /* synthetic */ void accept(Double d) {
        AbstractC0103o1.a(this, d);
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }
}
