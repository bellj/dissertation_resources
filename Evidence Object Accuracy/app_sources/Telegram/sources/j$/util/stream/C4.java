package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.u;
import java.util.Comparator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class C4 extends D4 implements u {
    /* access modifiers changed from: package-private */
    public C4(u uVar, long j, long j2) {
        super(uVar, j, j2, 0, Math.min(uVar.estimateSize(), j2));
    }

    private C4(u uVar, long j, long j2, long j3, long j4) {
        super(uVar, j, j2, j3, j4);
    }

    @Override // j$.util.stream.D4
    protected u a(u uVar, long j, long j2, long j3, long j4) {
        return new C4(uVar, j, j2, j3, j4);
    }

    @Override // j$.util.u
    public boolean b(Consumer consumer) {
        long j;
        consumer.getClass();
        if (this.a >= this.e) {
            return false;
        }
        while (true) {
            long j2 = this.a;
            j = this.d;
            if (j2 <= j) {
                break;
            }
            this.c.b(B4.a);
            this.d++;
        }
        if (j >= this.e) {
            return false;
        }
        this.d = j + 1;
        return this.c.b(consumer);
    }

    @Override // j$.util.u
    public void forEachRemaining(Consumer consumer) {
        consumer.getClass();
        long j = this.a;
        long j2 = this.e;
        if (j < j2) {
            long j3 = this.d;
            if (j3 < j2) {
                if (j3 < j || this.c.estimateSize() + j3 > this.b) {
                    while (this.a > this.d) {
                        this.c.b(A4.a);
                        this.d++;
                    }
                    while (this.d < this.e) {
                        this.c.b(consumer);
                        this.d++;
                    }
                    return;
                }
                this.c.forEachRemaining(consumer);
                this.d = this.e;
            }
        }
    }

    @Override // j$.util.u
    public Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.u
    public /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0002a.e(this);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0002a.f(this, i);
    }
}
