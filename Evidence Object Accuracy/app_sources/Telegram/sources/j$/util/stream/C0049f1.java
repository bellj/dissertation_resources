package j$.util.stream;

import j$.util.function.Predicate;

/* renamed from: j$.util.stream.f1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0049f1 extends AbstractC0073j1 {
    final /* synthetic */ EnumC0079k1 c;
    final /* synthetic */ Predicate d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0049f1(EnumC0079k1 k1Var, Predicate predicate) {
        super(k1Var);
        this.c = k1Var;
        this.d = predicate;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        if (!this.a && this.d.test(obj) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }
}
