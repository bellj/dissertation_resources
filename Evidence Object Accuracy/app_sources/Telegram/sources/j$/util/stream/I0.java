package j$.util.stream;

import j$.util.function.l;
import j$.util.u;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class I0 extends L0 {
    /* access modifiers changed from: package-private */
    public I0(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    @Override // j$.util.stream.AbstractC0029c
    final boolean G0() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.L0, j$.util.stream.IntStream
    public void I(l lVar) {
        if (!isParallel()) {
            L0.M0(J0()).c(lVar);
            return;
        }
        lVar.getClass();
        x0(new C0084l0(lVar, true));
    }

    @Override // j$.util.stream.L0, j$.util.stream.IntStream
    public void U(l lVar) {
        if (!isParallel()) {
            L0.M0(J0()).c(lVar);
        } else {
            super.U(lVar);
        }
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ IntStream parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ IntStream sequential() {
        sequential();
        return this;
    }
}
