package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.function.f;
import j$.util.function.l;
import j$.util.function.m;
import j$.util.function.q;
import j$.util.t;
import j$.util.u;
import j$.util.v;
import j$.wrappers.C0182j0;
import j$.wrappers.E;
import j$.wrappers.V;

/* renamed from: j$.util.stream.o1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract /* synthetic */ class AbstractC0103o1 {
    public static void a(AbstractC0075j3 j3Var, Double d) {
        if (!Q4.a) {
            j3Var.accept(d.doubleValue());
        } else {
            Q4.a(j3Var.getClass(), "{0} calling Sink.OfDouble.accept(Double)");
            throw null;
        }
    }

    public static void b(AbstractC0081k3 k3Var, Integer num) {
        if (!Q4.a) {
            k3Var.accept(num.intValue());
        } else {
            Q4.a(k3Var.getClass(), "{0} calling Sink.OfInt.accept(Integer)");
            throw null;
        }
    }

    public static void c(AbstractC0087l3 l3Var, Long l) {
        if (!Q4.a) {
            l3Var.accept(l.longValue());
        } else {
            Q4.a(l3Var.getClass(), "{0} calling Sink.OfLong.accept(Long)");
            throw null;
        }
    }

    public static void d(AbstractC0093m3 m3Var) {
        throw new IllegalStateException("called wrong accept method");
    }

    public static void e(AbstractC0093m3 m3Var) {
        throw new IllegalStateException("called wrong accept method");
    }

    public static void f(AbstractC0093m3 m3Var) {
        throw new IllegalStateException("called wrong accept method");
    }

    public static Object[] g(AbstractC0160z1 z1Var, m mVar) {
        if (Q4.a) {
            Q4.a(z1Var.getClass(), "{0} calling Node.OfPrimitive.asArray");
            throw null;
        } else if (z1Var.count() < 2147483639) {
            Object[] objArr = (Object[]) mVar.apply((int) z1Var.count());
            z1Var.i(objArr, 0);
            return objArr;
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static void h(AbstractC0135u1 u1Var, Double[] dArr, int i) {
        if (!Q4.a) {
            double[] dArr2 = (double[]) u1Var.e();
            for (int i2 = 0; i2 < dArr2.length; i2++) {
                dArr[i + i2] = Double.valueOf(dArr2[i2]);
            }
            return;
        }
        Q4.a(u1Var.getClass(), "{0} calling Node.OfDouble.copyInto(Double[], int)");
        throw null;
    }

    public static void i(AbstractC0145w1 w1Var, Integer[] numArr, int i) {
        if (!Q4.a) {
            int[] iArr = (int[]) w1Var.e();
            for (int i2 = 0; i2 < iArr.length; i2++) {
                numArr[i + i2] = Integer.valueOf(iArr[i2]);
            }
            return;
        }
        Q4.a(w1Var.getClass(), "{0} calling Node.OfInt.copyInto(Integer[], int)");
        throw null;
    }

    public static void j(AbstractC0155y1 y1Var, Long[] lArr, int i) {
        if (!Q4.a) {
            long[] jArr = (long[]) y1Var.e();
            for (int i2 = 0; i2 < jArr.length; i2++) {
                lArr[i + i2] = Long.valueOf(jArr[i2]);
            }
            return;
        }
        Q4.a(y1Var.getClass(), "{0} calling Node.OfInt.copyInto(Long[], int)");
        throw null;
    }

    public static void k(AbstractC0135u1 u1Var, Consumer consumer) {
        if (consumer instanceof f) {
            u1Var.g((f) consumer);
        } else if (!Q4.a) {
            ((t) u1Var.mo69spliterator()).forEachRemaining(consumer);
        } else {
            Q4.a(u1Var.getClass(), "{0} calling Node.OfLong.forEachRemaining(Consumer)");
            throw null;
        }
    }

    public static void l(AbstractC0145w1 w1Var, Consumer consumer) {
        if (consumer instanceof l) {
            w1Var.g((l) consumer);
        } else if (!Q4.a) {
            ((u.a) w1Var.mo69spliterator()).forEachRemaining(consumer);
        } else {
            Q4.a(w1Var.getClass(), "{0} calling Node.OfInt.forEachRemaining(Consumer)");
            throw null;
        }
    }

    public static void m(AbstractC0155y1 y1Var, Consumer consumer) {
        if (consumer instanceof q) {
            y1Var.g((q) consumer);
        } else if (!Q4.a) {
            ((v) y1Var.mo69spliterator()).forEachRemaining(consumer);
        } else {
            Q4.a(y1Var.getClass(), "{0} calling Node.OfLong.forEachRemaining(Consumer)");
            throw null;
        }
    }

    public static AbstractC0135u1 n(AbstractC0135u1 u1Var, long j, long j2, m mVar) {
        if (j == 0 && j2 == u1Var.count()) {
            return u1Var;
        }
        long j3 = j2 - j;
        t tVar = (t) u1Var.mo69spliterator();
        AbstractC0109p1 j4 = AbstractC0151x2.j(j3);
        j4.n(j3);
        for (int i = 0; ((long) i) < j && tVar.k(C0130t1.a); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && tVar.k(j4); i2++) {
        }
        j4.m();
        return j4.mo70a();
    }

    public static AbstractC0145w1 o(AbstractC0145w1 w1Var, long j, long j2, m mVar) {
        if (j == 0 && j2 == w1Var.count()) {
            return w1Var;
        }
        long j3 = j2 - j;
        u.a aVar = (u.a) w1Var.mo69spliterator();
        AbstractC0115q1 p = AbstractC0151x2.p(j3);
        p.n(j3);
        for (int i = 0; ((long) i) < j && aVar.g(C0140v1.a); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && aVar.g(p); i2++) {
        }
        p.m();
        return p.mo70a();
    }

    public static AbstractC0155y1 p(AbstractC0155y1 y1Var, long j, long j2, m mVar) {
        if (j == 0 && j2 == y1Var.count()) {
            return y1Var;
        }
        long j3 = j2 - j;
        v vVar = (v) y1Var.mo69spliterator();
        AbstractC0120r1 q = AbstractC0151x2.q(j3);
        q.n(j3);
        for (int i = 0; ((long) i) < j && vVar.i(C0150x1.a); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && vVar.i(q); i2++) {
        }
        q.m();
        return q.mo70a();
    }

    public static A1 q(A1 a1, long j, long j2, m mVar) {
        if (j == 0 && j2 == a1.count()) {
            return a1;
        }
        u spliterator = a1.mo69spliterator();
        long j3 = j2 - j;
        AbstractC0125s1 d = AbstractC0151x2.d(j3, mVar);
        d.n(j3);
        for (int i = 0; ((long) i) < j && spliterator.b(C0097n1.a); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && spliterator.b(d); i2++) {
        }
        d.m();
        return d.mo70a();
    }

    public static U r(t tVar, boolean z) {
        return new P(tVar, EnumC0040d4.c(tVar), z);
    }

    public static IntStream s(u.a aVar, boolean z) {
        return new I0(aVar, EnumC0040d4.c(aVar), z);
    }

    public static AbstractC0043e1 t(v vVar, boolean z) {
        return new C0019a1(vVar, EnumC0040d4.c(vVar), z);
    }

    public static N4 u(E e, EnumC0079k1 k1Var) {
        e.getClass();
        k1Var.getClass();
        return new C0085l1(EnumC0046e4.DOUBLE_VALUE, k1Var, new C0101o(k1Var, e));
    }

    public static N4 v(V v, EnumC0079k1 k1Var) {
        v.getClass();
        k1Var.getClass();
        return new C0085l1(EnumC0046e4.INT_VALUE, k1Var, new C0101o(k1Var, v));
    }

    public static N4 w(C0182j0 j0Var, EnumC0079k1 k1Var) {
        j0Var.getClass();
        k1Var.getClass();
        return new C0085l1(EnumC0046e4.LONG_VALUE, k1Var, new C0101o(k1Var, j0Var));
    }

    public static N4 x(Predicate predicate, EnumC0079k1 k1Var) {
        predicate.getClass();
        k1Var.getClass();
        return new C0085l1(EnumC0046e4.REFERENCE, k1Var, new C0101o(k1Var, predicate));
    }

    public static Stream y(u uVar, boolean z) {
        uVar.getClass();
        return new C0027b3(uVar, EnumC0040d4.c(uVar), z);
    }
}
