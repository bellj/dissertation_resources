package j$.util.stream;

import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.r2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0121r2 extends AbstractC0126s2 {
    private final Object[] h;

    C0121r2(C0121r2 r2Var, u uVar, long j, long j2) {
        super(r2Var, uVar, j, j2, r2Var.h.length);
        this.h = r2Var.h;
    }

    /* access modifiers changed from: package-private */
    public C0121r2(u uVar, AbstractC0156y2 y2Var, Object[] objArr) {
        super(uVar, y2Var, objArr.length);
        this.h = objArr;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        int i = this.f;
        if (i < this.g) {
            Object[] objArr = this.h;
            this.f = i + 1;
            objArr[i] = obj;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.stream.AbstractC0126s2
    AbstractC0126s2 b(u uVar, long j, long j2) {
        return new C0121r2(this, uVar, j, j2);
    }
}
