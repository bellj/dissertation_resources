package j$.util.stream;

import j$.util.function.m;
import j$.util.u;
import java.util.Arrays;

/* loaded from: classes2.dex */
final class L3 extends AbstractC0025b1 {
    /* access modifiers changed from: package-private */
    public L3(AbstractC0029c cVar) {
        super(cVar, EnumC0046e4.LONG_VALUE, EnumC0040d4.q | EnumC0040d4.o);
    }

    @Override // j$.util.stream.AbstractC0029c
    public A1 E0(AbstractC0156y2 y2Var, u uVar, m mVar) {
        if (EnumC0040d4.SORTED.d(y2Var.s0())) {
            return y2Var.p0(uVar, false, mVar);
        }
        long[] jArr = (long[]) ((AbstractC0155y1) y2Var.p0(uVar, true, mVar)).e();
        Arrays.sort(jArr);
        return new C0086l2(jArr);
    }

    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        m3Var.getClass();
        return EnumC0040d4.SORTED.d(i) ? m3Var : EnumC0040d4.SIZED.d(i) ? new Q3(m3Var) : new I3(m3Var);
    }
}
