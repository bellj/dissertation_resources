package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.u;
import java.util.concurrent.CountedCompleter;

/* renamed from: j$.util.stream.s2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
abstract class AbstractC0126s2 extends CountedCompleter implements AbstractC0093m3 {
    protected final u a;
    protected final AbstractC0156y2 b;
    protected final long c;
    protected long d;
    protected long e;
    protected int f;
    protected int g;

    /* access modifiers changed from: package-private */
    public AbstractC0126s2(AbstractC0126s2 s2Var, u uVar, long j, long j2, int i) {
        super(s2Var);
        this.a = uVar;
        this.b = s2Var.b;
        this.c = s2Var.c;
        this.d = j;
        this.e = j2;
        if (j < 0 || j2 < 0 || (j + j2) - 1 >= ((long) i)) {
            throw new IllegalArgumentException(String.format("offset and length interval [%d, %d + %d) is not within array size interval [0, %d)", Long.valueOf(j), Long.valueOf(j), Long.valueOf(j2), Integer.valueOf(i)));
        }
    }

    /* access modifiers changed from: package-private */
    public AbstractC0126s2(u uVar, AbstractC0156y2 y2Var, int i) {
        this.a = uVar;
        this.b = y2Var;
        this.c = AbstractC0047f.h(uVar.estimateSize());
        this.d = 0;
        this.e = (long) i;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    abstract AbstractC0126s2 b(u uVar, long j, long j2);

    @Override // java.util.concurrent.CountedCompleter
    public void compute() {
        u trySplit;
        u uVar = this.a;
        AbstractC0126s2 s2Var = this;
        while (uVar.estimateSize() > s2Var.c && (trySplit = uVar.trySplit()) != null) {
            s2Var.setPendingCount(1);
            long estimateSize = trySplit.estimateSize();
            s2Var.b(trySplit, s2Var.d, estimateSize).fork();
            s2Var = s2Var.b(uVar, s2Var.d + estimateSize, s2Var.e - estimateSize);
        }
        AbstractC0029c cVar = (AbstractC0029c) s2Var.b;
        cVar.getClass();
        cVar.n0(cVar.v0(s2Var), uVar);
        s2Var.propagateCompletion();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        long j2 = this.e;
        if (j <= j2) {
            int i = (int) this.d;
            this.f = i;
            this.g = i + ((int) j2);
            return;
        }
        throw new IllegalStateException("size passed to Sink.begin exceeds array length");
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
