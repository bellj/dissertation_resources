package j$.util.stream;

import j$.util.concurrent.a;
import j$.util.function.BiConsumer;

/* loaded from: classes2.dex */
public final /* synthetic */ class A implements BiConsumer {
    public static final /* synthetic */ A a = new A();

    private /* synthetic */ A() {
    }

    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        double[] dArr = (double[]) obj;
        double[] dArr2 = (double[]) obj2;
        AbstractC0083l.b(dArr, dArr2[0]);
        AbstractC0083l.b(dArr, dArr2[1]);
        dArr[2] = dArr[2] + dArr2[2];
        dArr[3] = dArr[3] + dArr2[3];
    }

    @Override // j$.util.function.BiConsumer
    public BiConsumer b(BiConsumer biConsumer) {
        biConsumer.getClass();
        return new a(this, biConsumer);
    }
}
