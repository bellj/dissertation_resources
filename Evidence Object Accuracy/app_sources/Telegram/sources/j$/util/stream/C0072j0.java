package j$.util.stream;

import j$.util.u;
import java.util.concurrent.CountedCompleter;

/* renamed from: j$.util.stream.j0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0072j0 extends AbstractC0035d {
    private final C0036d0 j;

    /* access modifiers changed from: package-private */
    public C0072j0(C0036d0 d0Var, AbstractC0156y2 y2Var, u uVar) {
        super(y2Var, uVar);
        this.j = d0Var;
    }

    C0072j0(C0072j0 j0Var, u uVar) {
        super(j0Var, uVar);
        this.j = j0Var.j;
    }

    private void m(Object obj) {
        boolean z;
        C0072j0 j0Var = this;
        while (true) {
            if (j0Var != null) {
                AbstractC0047f c = j0Var.c();
                if (c != null && c.d != j0Var) {
                    z = false;
                    break;
                }
                j0Var = c;
            } else {
                z = true;
                break;
            }
        }
        if (z) {
            l(obj);
        } else {
            j();
        }
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0047f
    public Object a() {
        AbstractC0156y2 y2Var = this.a;
        O4 o4 = (O4) this.j.e.get();
        y2Var.u0(o4, this.b);
        Object obj = o4.get();
        if (!this.j.b) {
            if (obj != null) {
                l(obj);
            }
            return null;
        } else if (obj == null) {
            return null;
        } else {
            m(obj);
            return obj;
        }
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0047f
    public AbstractC0047f f(u uVar) {
        return new C0072j0(this, uVar);
    }

    @Override // j$.util.stream.AbstractC0035d
    protected Object k() {
        return this.j.c;
    }

    @Override // j$.util.stream.AbstractC0047f, java.util.concurrent.CountedCompleter
    public void onCompletion(CountedCompleter countedCompleter) {
        if (this.j.b) {
            C0072j0 j0Var = (C0072j0) this.d;
            C0072j0 j0Var2 = null;
            while (true) {
                if (j0Var != j0Var2) {
                    Object b = j0Var.b();
                    if (b != null && this.j.d.test(b)) {
                        g(b);
                        m(b);
                        break;
                    }
                    j0Var = (C0072j0) this.e;
                    j0Var2 = j0Var;
                } else {
                    break;
                }
            }
        }
        this.b = null;
        this.e = null;
        this.d = null;
    }
}
