package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.p;
import j$.util.function.q;

/* renamed from: j$.util.stream.h3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0063h3 implements AbstractC0087l3 {
    protected final AbstractC0093m3 a;

    public AbstractC0063h3(AbstractC0093m3 m3Var) {
        m3Var.getClass();
        this.a = m3Var;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Long l) {
        AbstractC0103o1.c(this, l);
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void m() {
        this.a.m();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public boolean o() {
        return this.a.o();
    }
}
