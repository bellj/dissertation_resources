package j$.util.stream;

import j$.util.concurrent.a;
import j$.util.function.BiConsumer;
import java.util.LinkedHashSet;

/* renamed from: j$.util.stream.n  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0095n implements BiConsumer {
    public static final /* synthetic */ C0095n a = new C0095n();

    private /* synthetic */ C0095n() {
    }

    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        ((LinkedHashSet) obj).addAll((LinkedHashSet) obj2);
    }

    @Override // j$.util.function.BiConsumer
    public BiConsumer b(BiConsumer biConsumer) {
        biConsumer.getClass();
        return new a(this, biConsumer);
    }
}
