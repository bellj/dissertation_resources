package j$.util.stream;

import j$.util.function.m;
import j$.util.u;
import java.util.Arrays;

/* loaded from: classes2.dex */
final class K3 extends J0 {
    /* access modifiers changed from: package-private */
    public K3(AbstractC0029c cVar) {
        super(cVar, EnumC0046e4.INT_VALUE, EnumC0040d4.q | EnumC0040d4.o);
    }

    @Override // j$.util.stream.AbstractC0029c
    public A1 E0(AbstractC0156y2 y2Var, u uVar, m mVar) {
        if (EnumC0040d4.SORTED.d(y2Var.s0())) {
            return y2Var.p0(uVar, false, mVar);
        }
        int[] iArr = (int[]) ((AbstractC0145w1) y2Var.p0(uVar, true, mVar)).e();
        Arrays.sort(iArr);
        return new C0032c2(iArr);
    }

    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        m3Var.getClass();
        return EnumC0040d4.SORTED.d(i) ? m3Var : EnumC0040d4.SIZED.d(i) ? new P3(m3Var) : new H3(m3Var);
    }
}
