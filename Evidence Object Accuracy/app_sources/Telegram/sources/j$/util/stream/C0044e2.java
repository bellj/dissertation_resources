package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.l;
import j$.util.function.m;
import j$.util.u;
import j$.util.w;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.e2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0044e2 extends W3 implements AbstractC0145w1, AbstractC0115q1 {
    @Override // j$.util.stream.W3
    public u.a B() {
        return super.mo71spliterator();
    }

    /* renamed from: C */
    public /* synthetic */ void accept(Integer num) {
        AbstractC0103o1.b(this, num);
    }

    /* renamed from: D */
    public /* synthetic */ void i(Integer[] numArr, int i) {
        AbstractC0103o1.i(this, numArr, i);
    }

    /* renamed from: E */
    public /* synthetic */ AbstractC0145w1 r(long j, long j2, m mVar) {
        return AbstractC0103o1.o(this, j, j2, mVar);
    }

    @Override // j$.util.stream.AbstractC0115q1, j$.util.stream.AbstractC0125s1
    /* renamed from: a */
    public A1 mo70a() {
        return this;
    }

    @Override // j$.util.stream.AbstractC0115q1, j$.util.stream.AbstractC0125s1
    /* renamed from: a  reason: collision with other method in class */
    public AbstractC0145w1 mo70a() {
        return this;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.W3, j$.util.function.l
    public void accept(int i) {
        super.accept(i);
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0160z1, j$.util.stream.A1
    public AbstractC0160z1 b(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.Z3, j$.util.stream.AbstractC0160z1
    public void d(Object obj, int i) {
        super.d((int[]) obj, i);
    }

    @Override // j$.util.stream.Z3, j$.util.stream.AbstractC0160z1
    public Object e() {
        return (int[]) super.e();
    }

    @Override // j$.util.stream.Z3, j$.util.stream.AbstractC0160z1
    public void g(Object obj) {
        super.g((l) obj);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        clear();
        x(j);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ int p() {
        return 0;
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ Object[] q(m mVar) {
        return AbstractC0103o1.g(this, mVar);
    }

    @Override // j$.util.stream.W3, j$.util.stream.Z3, java.lang.Iterable, j$.lang.e
    /* renamed from: spliterator  reason: collision with other method in class */
    public w mo71spliterator() {
        return super.mo71spliterator();
    }

    @Override // j$.util.stream.W3, j$.util.stream.Z3, java.lang.Iterable, j$.lang.e
    /* renamed from: spliterator */
    public u mo71spliterator() {
        return super.mo71spliterator();
    }
}
