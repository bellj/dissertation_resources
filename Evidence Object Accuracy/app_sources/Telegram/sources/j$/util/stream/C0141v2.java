package j$.util.stream;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.v2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public class C0141v2 extends AbstractC0146w2 {
    public final /* synthetic */ int c;
    private final Object d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0141v2(AbstractC0160z1 z1Var, Object obj, int i) {
        super(z1Var, i);
        this.c = 0;
        this.d = obj;
    }

    @Override // j$.util.stream.AbstractC0146w2
    void a() {
        switch (this.c) {
            case 0:
                ((AbstractC0160z1) this.a).d(this.d, this.b);
                return;
            default:
                this.a.i((Object[]) this.d, this.b);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0146w2
    AbstractC0146w2 b(int i, int i2) {
        switch (this.c) {
            case 0:
                return new C0141v2(this, ((AbstractC0160z1) this.a).b(i), i2);
            default:
                return new C0141v2(this, this.a.b(i), i2);
        }
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0141v2(AbstractC0160z1 z1Var, Object obj, int i, B1 b1) {
        this(z1Var, obj, i);
        this.c = 0;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0141v2(A1 a1, Object[] objArr, int i, B1 b1) {
        super(a1, i);
        this.c = 1;
        this.c = 1;
        this.d = objArr;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0141v2(C0141v2 v2Var, AbstractC0160z1 z1Var, int i) {
        super(v2Var, z1Var, i);
        this.c = 0;
        this.d = v2Var.d;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0141v2(C0141v2 v2Var, A1 a1, int i) {
        super(v2Var, a1, i);
        this.c = 1;
        this.d = (Object[]) v2Var.d;
    }
}
