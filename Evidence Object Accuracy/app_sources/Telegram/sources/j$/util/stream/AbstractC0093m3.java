package j$.util.stream;

import j$.util.function.Consumer;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.m3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC0093m3 extends Consumer {
    void accept(double d);

    void accept(int i);

    @Override // j$.util.stream.AbstractC0087l3, j$.util.function.q
    void accept(long j);

    void m();

    void n(long j);

    boolean o();
}
