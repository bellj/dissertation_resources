package j$.util.stream;

import java.util.Arrays;

/* loaded from: classes2.dex */
final class H3 extends D3 {
    private W3 c;

    /* access modifiers changed from: package-private */
    public H3(AbstractC0093m3 m3Var) {
        super(m3Var);
    }

    @Override // j$.util.stream.AbstractC0081k3, j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        this.c.accept(i);
    }

    @Override // j$.util.stream.AbstractC0057g3, j$.util.stream.AbstractC0093m3
    public void m() {
        int[] iArr = (int[]) this.c.e();
        Arrays.sort(iArr);
        this.a.n((long) iArr.length);
        int i = 0;
        if (!this.b) {
            int length = iArr.length;
            while (i < length) {
                this.a.accept(iArr[i]);
                i++;
            }
        } else {
            int length2 = iArr.length;
            while (i < length2) {
                int i2 = iArr[i];
                if (this.a.o()) {
                    break;
                }
                this.a.accept(i2);
                i++;
            }
        }
        this.a.m();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        if (j < 2147483639) {
            this.c = j > 0 ? new W3((int) j) : new W3();
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }
}
