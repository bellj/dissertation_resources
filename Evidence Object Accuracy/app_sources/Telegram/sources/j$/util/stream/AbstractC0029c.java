package j$.util.stream;

import j$.util.function.m;
import j$.util.function.y;
import j$.util.u;

/* renamed from: j$.util.stream.c  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
abstract class AbstractC0029c extends AbstractC0156y2 implements AbstractC0053g {
    private final AbstractC0029c a;
    private final AbstractC0029c b;
    protected final int c;
    private AbstractC0029c d;
    private int e;
    private int f;
    private u g;
    private boolean h;
    private boolean i;
    private Runnable j;
    private boolean k;

    /* access modifiers changed from: package-private */
    public AbstractC0029c(AbstractC0029c cVar, int i) {
        if (!cVar.h) {
            cVar.h = true;
            cVar.d = this;
            this.b = cVar;
            this.c = EnumC0040d4.h & i;
            this.f = EnumC0040d4.a(i, cVar.f);
            AbstractC0029c cVar2 = cVar.a;
            this.a = cVar2;
            if (G0()) {
                cVar2.i = true;
            }
            this.e = cVar.e + 1;
            return;
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    /* access modifiers changed from: package-private */
    public AbstractC0029c(u uVar, int i, boolean z) {
        this.b = null;
        this.g = uVar;
        this.a = this;
        int i2 = EnumC0040d4.g & i;
        this.c = i2;
        this.f = ((i2 << 1) ^ -1) & EnumC0040d4.l;
        this.e = 0;
        this.k = z;
    }

    private u I0(int i) {
        int i2;
        int i3;
        AbstractC0029c cVar = this.a;
        u uVar = cVar.g;
        if (uVar != null) {
            cVar.g = null;
            if (cVar.k && cVar.i) {
                AbstractC0029c cVar2 = cVar.d;
                int i4 = 1;
                while (cVar != this) {
                    int i5 = cVar2.c;
                    if (cVar2.G0()) {
                        i4 = 0;
                        if (EnumC0040d4.SHORT_CIRCUIT.d(i5)) {
                            i5 &= EnumC0040d4.u ^ -1;
                        }
                        uVar = cVar2.F0(cVar, uVar);
                        if (uVar.hasCharacteristics(64)) {
                            i3 = i5 & (EnumC0040d4.t ^ -1);
                            i2 = EnumC0040d4.s;
                        } else {
                            i3 = i5 & (EnumC0040d4.s ^ -1);
                            i2 = EnumC0040d4.t;
                        }
                        i5 = i3 | i2;
                    }
                    i4++;
                    cVar2.e = i4;
                    cVar2.f = EnumC0040d4.a(i5, cVar.f);
                    cVar2 = cVar2.d;
                    cVar = cVar2;
                }
            }
            if (i != 0) {
                this.f = EnumC0040d4.a(i, this.f);
            }
            return uVar;
        }
        throw new IllegalStateException("source already consumed or closed");
    }

    abstract void A0(u uVar, AbstractC0093m3 m3Var);

    /* access modifiers changed from: package-private */
    public abstract EnumC0046e4 B0();

    /* access modifiers changed from: package-private */
    public final boolean C0() {
        return EnumC0040d4.ORDERED.d(this.f);
    }

    public /* synthetic */ u D0() {
        return I0(0);
    }

    A1 E0(AbstractC0156y2 y2Var, u uVar, m mVar) {
        throw new UnsupportedOperationException("Parallel evaluation is not supported");
    }

    u F0(AbstractC0156y2 y2Var, u uVar) {
        return E0(y2Var, uVar, C0017a.a).mo69spliterator();
    }

    abstract boolean G0();

    /* access modifiers changed from: package-private */
    public abstract AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var);

    /* access modifiers changed from: package-private */
    public final u J0() {
        AbstractC0029c cVar = this.a;
        if (this != cVar) {
            throw new IllegalStateException();
        } else if (!this.h) {
            this.h = true;
            u uVar = cVar.g;
            if (uVar != null) {
                cVar.g = null;
                return uVar;
            }
            throw new IllegalStateException("source already consumed or closed");
        } else {
            throw new IllegalStateException("stream has already been operated upon or closed");
        }
    }

    abstract u K0(AbstractC0156y2 y2Var, y yVar, boolean z);

    @Override // j$.util.stream.AbstractC0053g, java.lang.AutoCloseable
    public void close() {
        this.h = true;
        this.g = null;
        AbstractC0029c cVar = this.a;
        Runnable runnable = cVar.j;
        if (runnable != null) {
            cVar.j = null;
            runnable.run();
        }
    }

    @Override // j$.util.stream.AbstractC0053g
    public final boolean isParallel() {
        return this.a.k;
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final void n0(AbstractC0093m3 m3Var, u uVar) {
        m3Var.getClass();
        if (!EnumC0040d4.SHORT_CIRCUIT.d(this.f)) {
            m3Var.n(uVar.getExactSizeIfKnown());
            uVar.forEachRemaining(m3Var);
            m3Var.m();
            return;
        }
        o0(m3Var, uVar);
    }

    @Override // j$.util.stream.AbstractC0156y2
    final void o0(AbstractC0093m3 m3Var, u uVar) {
        AbstractC0029c cVar = this;
        while (cVar.e > 0) {
            cVar = cVar.b;
        }
        m3Var.n(uVar.getExactSizeIfKnown());
        cVar.A0(uVar, m3Var);
        m3Var.m();
    }

    @Override // j$.util.stream.AbstractC0053g
    public AbstractC0053g onClose(Runnable runnable) {
        AbstractC0029c cVar = this.a;
        Runnable runnable2 = cVar.j;
        if (runnable2 != null) {
            runnable = new M4(runnable2, runnable);
        }
        cVar.j = runnable;
        return this;
    }

    @Override // j$.util.stream.AbstractC0156y2
    final A1 p0(u uVar, boolean z, m mVar) {
        if (this.a.k) {
            return z0(this, uVar, z, mVar);
        }
        AbstractC0125s1 t0 = t0(q0(uVar), mVar);
        t0.getClass();
        n0(v0(t0), uVar);
        return t0.mo70a();
    }

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public final AbstractC0053g parallel() {
        this.a.k = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final long q0(u uVar) {
        if (EnumC0040d4.SIZED.d(this.f)) {
            return uVar.getExactSizeIfKnown();
        }
        return -1;
    }

    @Override // j$.util.stream.AbstractC0156y2
    final EnumC0046e4 r0() {
        AbstractC0029c cVar = this;
        while (cVar.e > 0) {
            cVar = cVar.b;
        }
        return cVar.B0();
    }

    @Override // j$.util.stream.AbstractC0156y2
    final int s0() {
        return this.f;
    }

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public final AbstractC0053g sequential() {
        this.a.k = false;
        return this;
    }

    @Override // j$.util.stream.AbstractC0053g
    public u spliterator() {
        if (!this.h) {
            this.h = true;
            AbstractC0029c cVar = this.a;
            if (this != cVar) {
                return K0(this, new C0023b(this), cVar.k);
            }
            u uVar = cVar.g;
            if (uVar != null) {
                cVar.g = null;
                return uVar;
            }
            throw new IllegalStateException("source already consumed or closed");
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    @Override // j$.util.stream.AbstractC0156y2
    final AbstractC0093m3 u0(AbstractC0093m3 m3Var, u uVar) {
        m3Var.getClass();
        n0(v0(m3Var), uVar);
        return m3Var;
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final AbstractC0093m3 v0(AbstractC0093m3 m3Var) {
        m3Var.getClass();
        for (AbstractC0029c cVar = this; cVar.e > 0; cVar = cVar.b) {
            m3Var = cVar.H0(cVar.b.f, m3Var);
        }
        return m3Var;
    }

    @Override // j$.util.stream.AbstractC0156y2
    final u w0(u uVar) {
        if (this.e == 0) {
            return uVar;
        }
        return K0(this, new C0023b(uVar), this.a.k);
    }

    /* access modifiers changed from: package-private */
    public final Object x0(N4 n4) {
        if (!this.h) {
            this.h = true;
            return this.a.k ? n4.c(this, I0(n4.b())) : n4.d(this, I0(n4.b()));
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    /* access modifiers changed from: package-private */
    public final A1 y0(m mVar) {
        if (!this.h) {
            this.h = true;
            if (!this.a.k || this.b == null || !G0()) {
                return p0(I0(0), true, mVar);
            }
            this.e = 0;
            AbstractC0029c cVar = this.b;
            return E0(cVar, cVar.I0(0), mVar);
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    abstract A1 z0(AbstractC0156y2 y2Var, u uVar, boolean z, m mVar);
}
