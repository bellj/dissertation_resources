package j$.util.stream;

import j$.util.concurrent.ConcurrentHashMap;
import j$.util.function.BiConsumer;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.function.y;
import j$.wrappers.C0182j0;
import j$.wrappers.E;
import j$.wrappers.V;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: j$.util.stream.o  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0101o implements Consumer, y {
    public final /* synthetic */ int a = 5;
    public final /* synthetic */ Object b;
    public final /* synthetic */ Object c;

    public /* synthetic */ C0101o(BiConsumer biConsumer, Object obj) {
        this.b = biConsumer;
        this.c = obj;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        switch (this.a) {
            case 0:
                AtomicBoolean atomicBoolean = (AtomicBoolean) this.b;
                ConcurrentHashMap concurrentHashMap = (ConcurrentHashMap) this.c;
                if (obj == null) {
                    atomicBoolean.set(true);
                    return;
                } else {
                    concurrentHashMap.putIfAbsent(obj, Boolean.TRUE);
                    return;
                }
            case 5:
                ((BiConsumer) this.b).accept(this.c, obj);
                return;
            default:
                ((C0094m4) this.b).f((Consumer) this.c, obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            case 5:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.function.y
    public Object get() {
        switch (this.a) {
            case 1:
                return new C0067i1((EnumC0079k1) this.b, (E) this.c);
            case 2:
                return new C0055g1((EnumC0079k1) this.b, (V) this.c);
            case 3:
                return new C0061h1((EnumC0079k1) this.b, (C0182j0) this.c);
            default:
                return new C0049f1((EnumC0079k1) this.b, (Predicate) this.c);
        }
    }

    public /* synthetic */ C0101o(EnumC0079k1 k1Var, Predicate predicate) {
        this.b = k1Var;
        this.c = predicate;
    }

    public /* synthetic */ C0101o(EnumC0079k1 k1Var, E e) {
        this.b = k1Var;
        this.c = e;
    }

    public /* synthetic */ C0101o(EnumC0079k1 k1Var, V v) {
        this.b = k1Var;
        this.c = v;
    }

    public /* synthetic */ C0101o(EnumC0079k1 k1Var, C0182j0 j0Var) {
        this.b = k1Var;
        this.c = j0Var;
    }

    public /* synthetic */ C0101o(C0094m4 m4Var, Consumer consumer) {
        this.b = m4Var;
        this.c = consumer;
    }

    public /* synthetic */ C0101o(AtomicBoolean atomicBoolean, ConcurrentHashMap concurrentHashMap) {
        this.b = atomicBoolean;
        this.c = concurrentHashMap;
    }
}
