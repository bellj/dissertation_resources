package j$.util.stream;

import j$.util.L;
import j$.util.function.Consumer;
import j$.util.function.p;
import j$.util.function.q;
import j$.util.v;
import java.util.Arrays;
import java.util.Iterator;

/* loaded from: classes2.dex */
class Y3 extends Z3 implements q {
    /* access modifiers changed from: package-private */
    public Y3() {
    }

    /* access modifiers changed from: package-private */
    public Y3(int i) {
        super(i);
    }

    /* renamed from: B */
    public v mo71spliterator() {
        return new X3(this, 0, this.c, 0, this.b);
    }

    @Override // j$.util.function.q
    public void accept(long j) {
        A();
        int i = this.b;
        this.b = i + 1;
        ((long[]) this.e)[i] = j;
    }

    @Override // j$.util.stream.Z3
    public Object c(int i) {
        return new long[i];
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    @Override // j$.lang.e
    public void forEach(Consumer consumer) {
        if (consumer instanceof q) {
            g((q) consumer);
        } else if (!Q4.a) {
            mo71spliterator().forEachRemaining(consumer);
        } else {
            Q4.a(getClass(), "{0} calling SpinedBuffer.OfLong.forEach(Consumer)");
            throw null;
        }
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return L.h(mo71spliterator());
    }

    @Override // j$.util.stream.Z3
    protected void t(Object obj, int i, int i2, Object obj2) {
        long[] jArr = (long[]) obj;
        q qVar = (q) obj2;
        while (i < i2) {
            qVar.accept(jArr[i]);
            i++;
        }
    }

    @Override // java.lang.Object
    public String toString() {
        long[] jArr = (long[]) e();
        return jArr.length < 200 ? String.format("%s[length=%d, chunks=%d]%s", getClass().getSimpleName(), Integer.valueOf(jArr.length), Integer.valueOf(this.c), Arrays.toString(jArr)) : String.format("%s[length=%d, chunks=%d]%s...", getClass().getSimpleName(), Integer.valueOf(jArr.length), Integer.valueOf(this.c), Arrays.toString(Arrays.copyOf(jArr, 200)));
    }

    @Override // j$.util.stream.Z3
    protected int u(Object obj) {
        return ((long[]) obj).length;
    }

    @Override // j$.util.stream.Z3
    protected Object[] z(int i) {
        return new long[i];
    }
}
