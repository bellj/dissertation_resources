package j$.util.stream;

import j$.util.L;
import j$.util.Optional;
import j$.util.function.A;
import j$.util.function.BiConsumer;
import j$.util.function.BiFunction;
import j$.util.function.C0008a;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.function.ToIntFunction;
import j$.util.function.b;
import j$.util.function.m;
import j$.util.function.y;
import j$.util.function.z;
import j$.util.u;
import j$.wrappers.J0;
import java.util.Comparator;
import java.util.Iterator;

/* renamed from: j$.util.stream.e3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
abstract class AbstractC0045e3 extends AbstractC0029c implements Stream {
    /* access modifiers changed from: package-private */
    public AbstractC0045e3(AbstractC0029c cVar, int i) {
        super(cVar, i);
    }

    /* access modifiers changed from: package-private */
    public AbstractC0045e3(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    @Override // j$.util.stream.AbstractC0029c
    final void A0(u uVar, AbstractC0093m3 m3Var) {
        while (!m3Var.o() && uVar.b(m3Var)) {
        }
    }

    @Override // j$.util.stream.Stream
    public final Object B(Object obj, BiFunction biFunction, b bVar) {
        biFunction.getClass();
        bVar.getClass();
        return x0(new C0161z2(EnumC0046e4.REFERENCE, bVar, biFunction, obj));
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final EnumC0046e4 B0() {
        return EnumC0046e4.REFERENCE;
    }

    @Override // j$.util.stream.Stream
    public final U E(Function function) {
        function.getClass();
        return new K(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, function);
    }

    @Override // j$.util.stream.AbstractC0029c
    final u K0(AbstractC0156y2 y2Var, y yVar, boolean z) {
        return new L4(y2Var, yVar, z);
    }

    @Override // j$.util.stream.Stream
    public final Stream T(Predicate predicate) {
        predicate.getClass();
        return new L(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.t, predicate);
    }

    @Override // j$.util.stream.Stream
    public final Stream V(Consumer consumer) {
        consumer.getClass();
        return new L(this, this, EnumC0046e4.REFERENCE, 0, consumer);
    }

    @Override // j$.util.stream.Stream
    public final boolean W(Predicate predicate) {
        return ((Boolean) x0(AbstractC0103o1.x(predicate, EnumC0079k1.ALL))).booleanValue();
    }

    @Override // j$.util.stream.Stream
    public final AbstractC0043e1 X(Function function) {
        function.getClass();
        return new N(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, function);
    }

    @Override // j$.util.stream.Stream
    public final boolean a(Predicate predicate) {
        return ((Boolean) x0(AbstractC0103o1.x(predicate, EnumC0079k1.ANY))).booleanValue();
    }

    @Override // j$.util.stream.Stream
    public final Object b0(J0 j0) {
        Object obj;
        if (!isParallel() || !j0.b().contains(EnumC0059h.CONCURRENT) || (C0() && !j0.b().contains(EnumC0059h.UNORDERED))) {
            j0.getClass();
            y f = j0.f();
            obj = x0(new I2(EnumC0046e4.REFERENCE, j0.c(), j0.a(), f, j0));
        } else {
            obj = j0.f().get();
            forEach(new C0101o(j0.a(), obj));
        }
        return j0.b().contains(EnumC0059h.IDENTITY_FINISH) ? obj : j0.e().apply(obj);
    }

    @Override // j$.util.stream.Stream
    public final IntStream c(Function function) {
        function.getClass();
        return new M(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, function);
    }

    @Override // j$.util.stream.Stream
    public final long count() {
        return ((AbstractC0037d1) g0(X2.a)).sum();
    }

    @Override // j$.util.stream.Stream
    public final boolean d0(Predicate predicate) {
        return ((Boolean) x0(AbstractC0103o1.x(predicate, EnumC0079k1.NONE))).booleanValue();
    }

    @Override // j$.util.stream.Stream
    public final Stream distinct() {
        return new C0123s(this, EnumC0046e4.REFERENCE, EnumC0040d4.m | EnumC0040d4.t);
    }

    @Override // j$.util.stream.Stream
    public void e(Consumer consumer) {
        consumer.getClass();
        x0(new C0096n0(consumer, true));
    }

    @Override // j$.util.stream.Stream
    public final Optional findAny() {
        return (Optional) x0(new C0036d0(false, EnumC0046e4.REFERENCE, Optional.empty(), V.a, C0030c0.a));
    }

    @Override // j$.util.stream.Stream
    public final Optional findFirst() {
        return (Optional) x0(new C0036d0(true, EnumC0046e4.REFERENCE, Optional.empty(), V.a, C0030c0.a));
    }

    @Override // j$.util.stream.Stream
    public void forEach(Consumer consumer) {
        consumer.getClass();
        x0(new C0096n0(consumer, false));
    }

    @Override // j$.util.stream.Stream
    public final AbstractC0043e1 g0(A a) {
        a.getClass();
        return new N(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n, a);
    }

    @Override // j$.util.stream.Stream
    public final Object i(y yVar, BiConsumer biConsumer, BiConsumer biConsumer2) {
        yVar.getClass();
        biConsumer.getClass();
        biConsumer2.getClass();
        return x0(new C0161z2(EnumC0046e4.REFERENCE, biConsumer2, biConsumer, yVar));
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    public final Iterator mo66iterator() {
        return L.i(spliterator());
    }

    @Override // j$.util.stream.Stream
    public final U j0(z zVar) {
        zVar.getClass();
        return new K(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n, zVar);
    }

    @Override // j$.util.stream.Stream
    public final Object[] l(m mVar) {
        return AbstractC0151x2.l(y0(mVar), mVar).q(mVar);
    }

    @Override // j$.util.stream.Stream
    public final Stream limit(long j) {
        if (j >= 0) {
            return B3.i(this, 0, j);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.Stream
    public final IntStream m(ToIntFunction toIntFunction) {
        toIntFunction.getClass();
        return new M(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n, toIntFunction);
    }

    @Override // j$.util.stream.Stream
    public final Object m0(Object obj, b bVar) {
        bVar.getClass();
        return x0(new C0161z2(EnumC0046e4.REFERENCE, bVar, bVar, obj));
    }

    @Override // j$.util.stream.Stream
    public final Optional max(Comparator comparator) {
        comparator.getClass();
        return t(new C0008a(comparator, 0));
    }

    @Override // j$.util.stream.Stream
    public final Optional min(Comparator comparator) {
        comparator.getClass();
        return t(new C0008a(comparator, 1));
    }

    @Override // j$.util.stream.Stream
    public final Stream n(Function function) {
        function.getClass();
        return new C0021a3(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n, function, 0);
    }

    @Override // j$.util.stream.Stream
    public final Stream o(Function function) {
        function.getClass();
        return new C0021a3(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, function, 1);
    }

    @Override // j$.util.stream.Stream
    public final Stream skip(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i >= 0) {
            return i == 0 ? this : B3.i(this, j, -1);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.Stream
    public final Stream sorted() {
        return new M3(this);
    }

    @Override // j$.util.stream.Stream
    public final Optional t(b bVar) {
        bVar.getClass();
        return (Optional) x0(new D2(EnumC0046e4.REFERENCE, bVar));
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final AbstractC0125s1 t0(long j, m mVar) {
        return AbstractC0151x2.d(j, mVar);
    }

    @Override // j$.util.stream.Stream
    public final Object[] toArray() {
        W2 w2 = W2.a;
        return AbstractC0151x2.l(y0(w2), w2).q(w2);
    }

    @Override // j$.util.stream.AbstractC0053g
    public AbstractC0053g unordered() {
        return !C0() ? this : new Z2(this, this, EnumC0046e4.REFERENCE, EnumC0040d4.r);
    }

    @Override // j$.util.stream.AbstractC0029c
    final A1 z0(AbstractC0156y2 y2Var, u uVar, boolean z, m mVar) {
        return AbstractC0151x2.e(y2Var, uVar, z, mVar);
    }

    @Override // j$.util.stream.Stream
    public final Stream sorted(Comparator comparator) {
        return new M3(this, comparator);
    }
}
