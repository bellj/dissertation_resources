package j$.util.stream;

import j$.util.function.Consumer;

/* renamed from: j$.util.stream.i3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0069i3 implements AbstractC0093m3 {
    protected final AbstractC0093m3 a;

    public AbstractC0069i3(AbstractC0093m3 m3Var) {
        m3Var.getClass();
        this.a = m3Var;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void m() {
        this.a.m();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public boolean o() {
        return this.a.o();
    }
}
