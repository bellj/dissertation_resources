package j$.util.stream;

import j$.util.function.f;
import j$.util.u;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class P extends T {
    /* access modifiers changed from: package-private */
    public P(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    @Override // j$.util.stream.AbstractC0029c
    final boolean G0() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.T, j$.util.stream.U
    public void j(f fVar) {
        if (!isParallel()) {
            T.M0(J0()).e(fVar);
        } else {
            super.j(fVar);
        }
    }

    @Override // j$.util.stream.T, j$.util.stream.U
    public void l0(f fVar) {
        if (!isParallel()) {
            T.M0(J0()).e(fVar);
            return;
        }
        fVar.getClass();
        x0(new C0078k0(fVar, true));
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ U parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ U sequential() {
        sequential();
        return this;
    }
}
