package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.b;
import j$.util.function.k;
import j$.util.function.l;
import j$.util.function.v;
import j$.util.function.y;

/* loaded from: classes2.dex */
class O2 extends T2 implements S2, AbstractC0081k3 {
    final /* synthetic */ y b;
    final /* synthetic */ v c;
    final /* synthetic */ b d;

    /* access modifiers changed from: package-private */
    public O2(y yVar, v vVar, b bVar) {
        this.b = yVar;
        this.c = vVar;
        this.d = bVar;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        this.c.accept(this.a, i);
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Integer num) {
        AbstractC0103o1.b(this, num);
    }

    @Override // j$.util.stream.S2
    public void h(S2 s2) {
        this.a = this.d.apply(this.a, ((O2) s2).a);
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a = this.b.get();
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
