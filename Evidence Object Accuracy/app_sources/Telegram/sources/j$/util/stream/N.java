package j$.util.stream;

import j$.lang.a;
import j$.lang.b;
import j$.lang.c;
import j$.util.function.A;
import j$.util.function.Function;
import j$.util.function.h;
import j$.util.function.n;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.t;
import j$.wrappers.C0182j0;

/* loaded from: classes2.dex */
class N extends AbstractC0031c1 {
    public final /* synthetic */ int l = 1;
    final /* synthetic */ Object m;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(T t, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, h hVar) {
        super(cVar, e4Var, i);
        this.m = hVar;
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        switch (this.l) {
            case 0:
                return new J(this, m3Var);
            case 1:
                return new F0(this, m3Var);
            case 2:
                return new Z0(this, m3Var);
            case 3:
                return new Z0(this, m3Var, (a) null);
            case 4:
                return new Z0(this, m3Var, (b) null);
            case 5:
                return new Z0(this, m3Var, (c) null);
            case 6:
                return new r(this, m3Var);
            default:
                return new Y2(this, m3Var);
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(L0 l0, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, n nVar) {
        super(cVar, e4Var, i);
        this.m = nVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(AbstractC0037d1 d1Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, q qVar) {
        super(cVar, e4Var, i);
        this.m = qVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(AbstractC0037d1 d1Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, r rVar) {
        super(cVar, e4Var, i);
        this.m = rVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(AbstractC0037d1 d1Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, t tVar) {
        super(cVar, e4Var, i);
        this.m = tVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(AbstractC0037d1 d1Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, C0182j0 j0Var) {
        super(cVar, e4Var, i);
        this.m = j0Var;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(AbstractC0045e3 e3Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, Function function) {
        super(cVar, e4Var, i);
        this.m = function;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public N(AbstractC0045e3 e3Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, A a) {
        super(cVar, e4Var, i);
        this.m = a;
    }
}
