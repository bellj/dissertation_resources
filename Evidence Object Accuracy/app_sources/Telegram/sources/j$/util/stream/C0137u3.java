package j$.util.stream;

/* renamed from: j$.util.stream.u3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0137u3 extends AbstractC0063h3 {
    long b;
    long c;
    final /* synthetic */ C0142v3 d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0137u3(C0142v3 v3Var, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = v3Var;
        this.b = v3Var.l;
        long j = v3Var.m;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        long j2 = this.b;
        if (j2 == 0) {
            long j3 = this.c;
            if (j3 > 0) {
                this.c = j3 - 1;
                this.a.accept(j);
                return;
            }
            return;
        }
        this.b = j2 - 1;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a.n(B3.c(j, this.d.l, this.c));
    }

    @Override // j$.util.stream.AbstractC0063h3, j$.util.stream.AbstractC0093m3
    public boolean o() {
        return this.c == 0 || this.a.o();
    }
}
