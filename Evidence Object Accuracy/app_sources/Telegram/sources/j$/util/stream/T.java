package j$.util.stream;

import j$.util.AbstractC0016n;
import j$.util.C0009g;
import j$.util.C0012j;
import j$.util.L;
import j$.util.function.BiConsumer;
import j$.util.function.d;
import j$.util.function.f;
import j$.util.function.g;
import j$.util.function.h;
import j$.util.function.m;
import j$.util.function.y;
import j$.util.t;
import j$.util.u;
import j$.wrappers.E;
import j$.wrappers.G;
import j$.wrappers.K;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class T extends AbstractC0029c implements U {
    /* access modifiers changed from: package-private */
    public T(AbstractC0029c cVar, int i) {
        super(cVar, i);
    }

    /* access modifiers changed from: package-private */
    public T(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    /* access modifiers changed from: private */
    public static t M0(u uVar) {
        if (uVar instanceof t) {
            return (t) uVar;
        }
        if (Q4.a) {
            Q4.a(AbstractC0029c.class, "using DoubleStream.adapt(Spliterator<Double> s)");
            throw null;
        }
        throw new UnsupportedOperationException("DoubleStream.adapt(Spliterator<Double> s)");
    }

    @Override // j$.util.stream.AbstractC0029c
    final void A0(u uVar, AbstractC0093m3 m3Var) {
        f fVar;
        t M0 = M0(uVar);
        if (m3Var instanceof f) {
            fVar = (f) m3Var;
        } else if (!Q4.a) {
            fVar = new F(m3Var);
        } else {
            Q4.a(AbstractC0029c.class, "using DoubleStream.adapt(Sink<Double> s)");
            throw null;
        }
        while (!m3Var.o() && M0.k(fVar)) {
        }
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final EnumC0046e4 B0() {
        return EnumC0046e4.DOUBLE_VALUE;
    }

    @Override // j$.util.stream.U
    public final C0012j G(d dVar) {
        dVar.getClass();
        return (C0012j) x0(new D2(EnumC0046e4.DOUBLE_VALUE, dVar));
    }

    @Override // j$.util.stream.U
    public final Object H(y yVar, j$.util.function.u uVar, BiConsumer biConsumer) {
        C c = new C(biConsumer, 0);
        yVar.getClass();
        uVar.getClass();
        return x0(new C0161z2(EnumC0046e4.DOUBLE_VALUE, c, uVar, yVar));
    }

    @Override // j$.util.stream.U
    public final double K(double d, d dVar) {
        dVar.getClass();
        return ((Double) x0(new B2(EnumC0046e4.DOUBLE_VALUE, dVar, d))).doubleValue();
    }

    @Override // j$.util.stream.AbstractC0029c
    final u K0(AbstractC0156y2 y2Var, y yVar, boolean z) {
        return new C0106o4(y2Var, yVar, z);
    }

    @Override // j$.util.stream.U
    public final Stream M(g gVar) {
        gVar.getClass();
        return new L(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.p | EnumC0040d4.n, gVar);
    }

    @Override // j$.util.stream.U
    public final IntStream R(G g) {
        g.getClass();
        return new M(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.p | EnumC0040d4.n, g);
    }

    @Override // j$.util.stream.U
    public final boolean Y(E e) {
        return ((Boolean) x0(AbstractC0103o1.u(e, EnumC0079k1.ALL))).booleanValue();
    }

    @Override // j$.util.stream.U
    public final C0012j average() {
        double[] dArr = (double[]) H(C0148x.a, C0138v.a, A.a);
        return dArr[2] > 0.0d ? C0012j.d(AbstractC0083l.a(dArr) / dArr[2]) : C0012j.a();
    }

    @Override // j$.util.stream.U
    public final U b(f fVar) {
        fVar.getClass();
        return new K(this, this, EnumC0046e4.DOUBLE_VALUE, 0, fVar);
    }

    @Override // j$.util.stream.U
    public final Stream boxed() {
        return M(G.a);
    }

    @Override // j$.util.stream.U
    public final long count() {
        return ((AbstractC0037d1) x(H.a)).sum();
    }

    @Override // j$.util.stream.U
    public final U distinct() {
        return ((AbstractC0045e3) M(G.a)).distinct().j0(C0158z.a);
    }

    @Override // j$.util.stream.U
    public final C0012j findAny() {
        return (C0012j) x0(new C0036d0(false, EnumC0046e4.DOUBLE_VALUE, C0012j.a(), W.a, Z.a));
    }

    @Override // j$.util.stream.U
    public final C0012j findFirst() {
        return (C0012j) x0(new C0036d0(true, EnumC0046e4.DOUBLE_VALUE, C0012j.a(), W.a, Z.a));
    }

    @Override // j$.util.stream.U
    public final boolean h0(E e) {
        return ((Boolean) x0(AbstractC0103o1.u(e, EnumC0079k1.ANY))).booleanValue();
    }

    @Override // j$.util.stream.U
    public final boolean i0(E e) {
        return ((Boolean) x0(AbstractC0103o1.u(e, EnumC0079k1.NONE))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    public final AbstractC0016n mo66iterator() {
        return L.f(spliterator());
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator  reason: collision with other method in class */
    public Iterator mo66iterator() {
        return L.f(spliterator());
    }

    @Override // j$.util.stream.U
    public void j(f fVar) {
        fVar.getClass();
        x0(new C0078k0(fVar, false));
    }

    @Override // j$.util.stream.U
    public void l0(f fVar) {
        fVar.getClass();
        x0(new C0078k0(fVar, true));
    }

    @Override // j$.util.stream.U
    public final U limit(long j) {
        if (j >= 0) {
            return B3.f(this, 0, j);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.U
    public final C0012j max() {
        return G(D.a);
    }

    @Override // j$.util.stream.U
    public final C0012j min() {
        return G(E.a);
    }

    @Override // j$.util.stream.U
    public final U r(E e) {
        e.getClass();
        return new K(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.t, e);
    }

    @Override // j$.util.stream.U
    public final U skip(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i >= 0) {
            return i == 0 ? this : B3.f(this, j, -1);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.U
    public final U sorted() {
        return new J3(this);
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g
    public final t spliterator() {
        return M0(super.spliterator());
    }

    @Override // j$.util.stream.U
    public final double sum() {
        return AbstractC0083l.a((double[]) H(C0153y.a, C0143w.a, B.a));
    }

    @Override // j$.util.stream.U
    public final C0009g summaryStatistics() {
        return (C0009g) H(C0065i.a, C0133u.a, C0128t.a);
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final AbstractC0125s1 t0(long j, m mVar) {
        return AbstractC0151x2.j(j);
    }

    @Override // j$.util.stream.U
    public final double[] toArray() {
        return (double[]) AbstractC0151x2.m((AbstractC0135u1) y0(I.a)).e();
    }

    @Override // j$.util.stream.AbstractC0053g
    public AbstractC0053g unordered() {
        return !C0() ? this : new O(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.r);
    }

    @Override // j$.util.stream.U
    public final U w(g gVar) {
        return new K(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, gVar);
    }

    @Override // j$.util.stream.U
    public final AbstractC0043e1 x(h hVar) {
        hVar.getClass();
        return new N(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.p | EnumC0040d4.n, hVar);
    }

    @Override // j$.util.stream.U
    public final U y(K k) {
        k.getClass();
        return new K(this, this, EnumC0046e4.DOUBLE_VALUE, EnumC0040d4.p | EnumC0040d4.n, k);
    }

    @Override // j$.util.stream.AbstractC0029c
    final A1 z0(AbstractC0156y2 y2Var, u uVar, boolean z, m mVar) {
        return AbstractC0151x2.f(y2Var, uVar, z);
    }
}
