package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.m;
import j$.util.u;
import j$.util.w;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class Q1 extends R1 implements AbstractC0155y1 {
    /* access modifiers changed from: package-private */
    public Q1(AbstractC0155y1 y1Var, AbstractC0155y1 y1Var2) {
        super(y1Var, y1Var2);
    }

    /* renamed from: a */
    public /* synthetic */ void i(Long[] lArr, int i) {
        AbstractC0103o1.j(this, lArr, i);
    }

    /* renamed from: f */
    public long[] c(int i) {
        return new long[i];
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0103o1.m(this, consumer);
    }

    /* renamed from: h */
    public /* synthetic */ AbstractC0155y1 r(long j, long j2, m mVar) {
        return AbstractC0103o1.p(this, j, j2, mVar);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator  reason: collision with other method in class */
    public w mo69spliterator() {
        return new C0062h2(this);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator */
    public u mo69spliterator() {
        return new C0062h2(this);
    }
}
