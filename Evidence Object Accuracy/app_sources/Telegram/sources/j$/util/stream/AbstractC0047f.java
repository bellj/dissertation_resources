package j$.util.stream;

import j$.util.u;
import java.util.concurrent.CountedCompleter;
import java.util.concurrent.ForkJoinPool;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.f  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0047f extends CountedCompleter {
    static final int g = (ForkJoinPool.getCommonPoolParallelism() << 2);
    protected final AbstractC0156y2 a;
    protected u b;
    protected long c;
    protected AbstractC0047f d;
    protected AbstractC0047f e;
    private Object f;

    /* access modifiers changed from: protected */
    public AbstractC0047f(AbstractC0047f fVar, u uVar) {
        super(fVar);
        this.b = uVar;
        this.a = fVar.a;
        this.c = fVar.c;
    }

    /* access modifiers changed from: protected */
    public AbstractC0047f(AbstractC0156y2 y2Var, u uVar) {
        super(null);
        this.a = y2Var;
        this.b = uVar;
        this.c = 0;
    }

    public static long h(long j) {
        long j2 = j / ((long) g);
        if (j2 > 0) {
            return j2;
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public abstract Object a();

    /* access modifiers changed from: protected */
    public Object b() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public AbstractC0047f c() {
        return (AbstractC0047f) getCompleter();
    }

    @Override // java.util.concurrent.CountedCompleter
    public void compute() {
        u trySplit;
        u uVar = this.b;
        long estimateSize = uVar.estimateSize();
        long j = this.c;
        if (j == 0) {
            j = h(estimateSize);
            this.c = j;
        }
        boolean z = false;
        AbstractC0047f fVar = this;
        while (estimateSize > j && (trySplit = uVar.trySplit()) != null) {
            AbstractC0047f f = fVar.f(trySplit);
            fVar.d = f;
            AbstractC0047f f2 = fVar.f(uVar);
            fVar.e = f2;
            fVar.setPendingCount(1);
            if (z) {
                uVar = trySplit;
                fVar = f;
                f = f2;
            } else {
                fVar = f2;
            }
            z = !z;
            f.fork();
            estimateSize = uVar.estimateSize();
        }
        fVar.g(fVar.a());
        fVar.tryComplete();
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.d == null;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return c() == null;
    }

    /* access modifiers changed from: protected */
    public abstract AbstractC0047f f(u uVar);

    /* access modifiers changed from: protected */
    public void g(Object obj) {
        this.f = obj;
    }

    @Override // java.util.concurrent.CountedCompleter, java.util.concurrent.ForkJoinTask
    public Object getRawResult() {
        return this.f;
    }

    @Override // java.util.concurrent.CountedCompleter
    public void onCompletion(CountedCompleter countedCompleter) {
        this.b = null;
        this.e = null;
        this.d = null;
    }

    @Override // java.util.concurrent.CountedCompleter, java.util.concurrent.ForkJoinTask
    protected void setRawResult(Object obj) {
        if (obj != null) {
            throw new IllegalStateException();
        }
    }
}
