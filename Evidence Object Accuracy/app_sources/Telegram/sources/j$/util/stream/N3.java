package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.Collection$EL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/* loaded from: classes2.dex */
final class N3 extends F3 {
    private ArrayList d;

    /* access modifiers changed from: package-private */
    public N3(AbstractC0093m3 m3Var, Comparator comparator) {
        super(m3Var, comparator);
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        this.d.add(obj);
    }

    @Override // j$.util.stream.AbstractC0069i3, j$.util.stream.AbstractC0093m3
    public void m() {
        AbstractC0002a.G(this.d, this.b);
        this.a.n((long) this.d.size());
        if (!this.c) {
            ArrayList arrayList = this.d;
            AbstractC0093m3 m3Var = this.a;
            m3Var.getClass();
            Collection$EL.a(arrayList, new C0023b(m3Var));
        } else {
            Iterator it = this.d.iterator();
            while (it.hasNext()) {
                Object next = it.next();
                if (this.a.o()) {
                    break;
                }
                this.a.accept((AbstractC0093m3) next);
            }
        }
        this.a.m();
        this.d = null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        if (j < 2147483639) {
            this.d = j >= 0 ? new ArrayList((int) j) : new ArrayList();
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }
}
