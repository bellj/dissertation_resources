package j$.util.stream;

import j$.util.function.k;
import j$.util.function.l;

/* renamed from: j$.util.stream.h4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0064h4 extends AbstractC0076j4 implements l {
    final int[] c;

    /* access modifiers changed from: package-private */
    public C0064h4(int i) {
        this.c = new int[i];
    }

    @Override // j$.util.function.l
    public void accept(int i) {
        int[] iArr = this.c;
        int i2 = this.b;
        this.b = i2 + 1;
        iArr[i2] = i;
    }

    @Override // j$.util.stream.AbstractC0076j4
    public void b(Object obj, long j) {
        l lVar = (l) obj;
        for (int i = 0; ((long) i) < j; i++) {
            lVar.accept(this.c[i]);
        }
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }
}
