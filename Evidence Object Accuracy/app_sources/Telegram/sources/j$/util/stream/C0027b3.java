package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.b3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public class C0027b3 extends AbstractC0045e3 {
    /* access modifiers changed from: package-private */
    public C0027b3(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    @Override // j$.util.stream.AbstractC0029c
    final boolean G0() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0045e3, j$.util.stream.Stream
    public void e(Consumer consumer) {
        if (!isParallel()) {
            J0().forEachRemaining(consumer);
            return;
        }
        consumer.getClass();
        x0(new C0096n0(consumer, true));
    }

    @Override // j$.util.stream.AbstractC0045e3, j$.util.stream.Stream
    public void forEach(Consumer consumer) {
        if (!isParallel()) {
            J0().forEachRemaining(consumer);
        } else {
            super.forEach(consumer);
        }
    }
}
