package j$.util.stream;

import j$.util.function.m;
import j$.util.u;

/* loaded from: classes2.dex */
final class A3 extends AbstractC0035d {
    private final AbstractC0029c j;
    private final m k;
    private final long l;
    private final long m;
    private long n;
    private volatile boolean o;

    A3(A3 a3, u uVar) {
        super(a3, uVar);
        this.j = a3.j;
        this.k = a3.k;
        this.l = a3.l;
        this.m = a3.m;
    }

    /* access modifiers changed from: package-private */
    public A3(AbstractC0029c cVar, AbstractC0156y2 y2Var, u uVar, m mVar, long j, long j2) {
        super(y2Var, uVar);
        this.j = cVar;
        this.k = mVar;
        this.l = j;
        this.m = j2;
    }

    private long m(long j) {
        if (this.o) {
            return this.n;
        }
        A3 a3 = (A3) this.d;
        A3 a32 = (A3) this.e;
        if (a3 == null || a32 == null) {
            return this.n;
        }
        long m = a3.m(j);
        return m >= j ? m : m + a32.m(j);
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0047f
    public Object a() {
        long j = -1;
        if (e()) {
            if (EnumC0040d4.SIZED.e(this.j.c)) {
                j = this.j.q0(this.b);
            }
            AbstractC0125s1 t0 = this.j.t0(j, this.k);
            AbstractC0093m3 H0 = this.j.H0(this.a.s0(), t0);
            AbstractC0156y2 y2Var = this.a;
            y2Var.o0(y2Var.v0(H0), this.b);
            return t0.mo70a();
        }
        AbstractC0156y2 y2Var2 = this.a;
        AbstractC0125s1 t02 = y2Var2.t0(-1, this.k);
        y2Var2.u0(t02, this.b);
        A1 a = t02.mo70a();
        this.n = a.count();
        this.o = true;
        this.b = null;
        return a;
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0047f
    public AbstractC0047f f(u uVar) {
        return new A3(this, uVar);
    }

    @Override // j$.util.stream.AbstractC0035d
    protected void i() {
        this.i = true;
        if (this.o) {
            g(k());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public final A1 k() {
        return AbstractC0151x2.k(this.j.B0());
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    @Override // j$.util.stream.AbstractC0047f, java.util.concurrent.CountedCompleter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onCompletion(java.util.concurrent.CountedCompleter r12) {
        /*
        // Method dump skipped, instructions count: 228
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.stream.A3.onCompletion(java.util.concurrent.CountedCompleter):void");
    }
}
