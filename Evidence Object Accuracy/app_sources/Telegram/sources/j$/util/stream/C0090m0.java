package j$.util.stream;

import j$.util.function.p;
import j$.util.function.q;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.m0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0090m0 extends AbstractC0102o0 implements AbstractC0087l3 {
    final q b;

    /* access modifiers changed from: package-private */
    public C0090m0(q qVar, boolean z) {
        super(z);
        this.b = qVar;
    }

    @Override // j$.util.stream.AbstractC0102o0, j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        this.b.accept(j);
    }

    /* renamed from: e */
    public /* synthetic */ void accept(Long l) {
        AbstractC0103o1.c(this, l);
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }
}
