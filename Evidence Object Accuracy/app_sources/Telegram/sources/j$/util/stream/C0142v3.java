package j$.util.stream;

import j$.util.function.m;
import j$.util.u;
import j$.util.v;

/* renamed from: j$.util.stream.v3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0142v3 extends AbstractC0025b1 {
    final /* synthetic */ long l;
    final /* synthetic */ long m;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0142v3(AbstractC0029c cVar, EnumC0046e4 e4Var, int i, long j, long j2) {
        super(cVar, e4Var, i);
        this.l = j;
        this.m = j2;
    }

    @Override // j$.util.stream.AbstractC0029c
    A1 E0(AbstractC0156y2 y2Var, u uVar, m mVar) {
        long q0 = y2Var.q0(uVar);
        if (q0 > 0 && uVar.hasCharacteristics(16384)) {
            return AbstractC0151x2.h(y2Var, B3.b(y2Var.r0(), uVar, this.l, this.m), true);
        }
        return !EnumC0040d4.ORDERED.d(y2Var.s0()) ? AbstractC0151x2.h(this, N0((v) y2Var.w0(uVar), this.l, this.m, q0), true) : (A1) new A3(this, y2Var, uVar, mVar, this.l, this.m).invoke();
    }

    @Override // j$.util.stream.AbstractC0029c
    u F0(AbstractC0156y2 y2Var, u uVar) {
        long q0 = y2Var.q0(uVar);
        if (q0 > 0 && uVar.hasCharacteristics(16384)) {
            long j = this.l;
            return new y4((v) y2Var.w0(uVar), j, B3.a(j, this.m));
        }
        return !EnumC0040d4.ORDERED.d(y2Var.s0()) ? N0((v) y2Var.w0(uVar), this.l, this.m, q0) : ((A1) new A3(this, y2Var, uVar, C0132t3.a, this.l, this.m).invoke()).mo69spliterator();
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        return new C0137u3(this, m3Var);
    }

    v N0(v vVar, long j, long j2, long j3) {
        long j4;
        long j5;
        if (j <= j3) {
            long j6 = j3 - j;
            j4 = j2 >= 0 ? Math.min(j2, j6) : j6;
            j5 = 0;
        } else {
            j5 = j;
            j4 = j2;
        }
        return new G4(vVar, j5, j4);
    }
}
