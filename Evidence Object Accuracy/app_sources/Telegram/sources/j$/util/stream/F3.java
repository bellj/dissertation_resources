package j$.util.stream;

import java.util.Comparator;

/* loaded from: classes2.dex */
abstract class F3 extends AbstractC0069i3 {
    protected final Comparator b;
    protected boolean c;

    /* access modifiers changed from: package-private */
    public F3(AbstractC0093m3 m3Var, Comparator comparator) {
        super(m3Var);
        this.b = comparator;
    }

    @Override // j$.util.stream.AbstractC0069i3, j$.util.stream.AbstractC0093m3
    public final boolean o() {
        this.c = true;
        return false;
    }
}
