package j$.util.stream;

import j$.util.function.Function;
import j$.util.function.f;
import j$.util.function.l;
import j$.util.function.q;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes2.dex */
class r extends AbstractC0069i3 {
    public final /* synthetic */ int b = 3;
    Object c;
    final /* synthetic */ Object d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public r(C0123s sVar, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = sVar;
    }

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        IntStream intStream;
        switch (this.b) {
            case 0:
                if (!((Set) this.c).contains(obj)) {
                    ((Set) this.c).add(obj);
                    this.a.accept((AbstractC0093m3) obj);
                    return;
                }
                return;
            case 1:
                intStream = (AbstractC0043e1) ((Function) ((N) this.d).m).apply(obj);
                if (intStream != null) {
                    try {
                        intStream.sequential().d((q) this.c);
                    } finally {
                        try {
                            intStream.close();
                        } catch (Throwable unused) {
                        }
                    }
                }
                if (intStream != null) {
                    intStream.close();
                    return;
                }
                return;
            case 2:
                intStream = (IntStream) ((Function) ((M) this.d).m).apply(obj);
                if (intStream != null) {
                    try {
                        intStream.sequential().U((l) this.c);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                if (intStream != null) {
                    intStream.close();
                    return;
                }
                return;
            default:
                U u = (U) ((Function) ((K) this.d).m).apply(obj);
                if (u != null) {
                    try {
                        u.sequential().j((f) this.c);
                    } finally {
                        try {
                            u.close();
                        } catch (Throwable unused2) {
                        }
                    }
                }
                if (u != null) {
                    u.close();
                    return;
                }
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0069i3, j$.util.stream.AbstractC0093m3
    public void m() {
        switch (this.b) {
            case 0:
                this.c = null;
                this.a.m();
                return;
            default:
                this.a.m();
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        switch (this.b) {
            case 0:
                this.c = new HashSet();
                this.a.n(-1);
                return;
            case 1:
                this.a.n(-1);
                return;
            case 2:
                this.a.n(-1);
                return;
            default:
                this.a.n(-1);
                return;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public r(K k, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = k;
        this.c = new F(m3Var);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public r(M m, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = m;
        this.c = new B0(m3Var);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public r(N n, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = n;
        this.c = new W0(m3Var);
    }
}
