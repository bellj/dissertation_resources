package j$.util.stream;

import j$.util.function.Consumer;

/* renamed from: j$.util.stream.j1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
abstract class AbstractC0073j1 implements AbstractC0093m3 {
    boolean a;
    boolean b;

    /* access modifiers changed from: package-private */
    public AbstractC0073j1(EnumC0079k1 k1Var) {
        this.b = !k1Var.b;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void n(long j) {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public boolean o() {
        return this.a;
    }
}
