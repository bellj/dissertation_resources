package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.w;
import java.util.Comparator;
import org.telegram.tgnet.ConnectionsManager;

/* loaded from: classes2.dex */
abstract class H4 extends J4 implements w {
    /* access modifiers changed from: package-private */
    public H4(w wVar, long j, long j2) {
        super(wVar, j, j2);
    }

    /* access modifiers changed from: package-private */
    public H4(w wVar, H4 h4) {
        super(wVar, h4);
    }

    @Override // j$.util.w
    /* renamed from: forEachRemaining */
    public void e(Object obj) {
        obj.getClass();
        AbstractC0076j4 j4Var = null;
        while (true) {
            int r = r();
            if (r == 1) {
                return;
            }
            if (r == 2) {
                if (j4Var == null) {
                    j4Var = t(ConnectionsManager.RequestFlagNeedQuickAck);
                } else {
                    j4Var.b = 0;
                }
                long j = 0;
                while (((w) this.a).tryAdvance(j4Var)) {
                    j++;
                    if (j >= 128) {
                        break;
                    }
                }
                if (j != 0) {
                    j4Var.b(obj, p(j));
                } else {
                    return;
                }
            } else {
                ((w) this.a).forEachRemaining(obj);
                return;
            }
        }
    }

    @Override // j$.util.u
    public Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.u
    public /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0002a.e(this);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0002a.f(this, i);
    }

    protected abstract void s(Object obj);

    protected abstract AbstractC0076j4 t(int i);

    @Override // j$.util.w
    /* renamed from: tryAdvance */
    public boolean k(Object obj) {
        obj.getClass();
        while (r() != 1 && ((w) this.a).tryAdvance(this)) {
            if (p(1) == 1) {
                s(obj);
                return true;
            }
        }
        return false;
    }
}
