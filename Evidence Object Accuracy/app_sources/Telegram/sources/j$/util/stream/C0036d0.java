package j$.util.stream;

import j$.util.function.Predicate;
import j$.util.function.y;
import j$.util.u;

/* renamed from: j$.util.stream.d0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0036d0 implements N4 {
    private final EnumC0046e4 a;
    final boolean b;
    final Object c;
    final Predicate d;
    final y e;

    /* access modifiers changed from: package-private */
    public C0036d0(boolean z, EnumC0046e4 e4Var, Object obj, Predicate predicate, y yVar) {
        this.b = z;
        this.a = e4Var;
        this.c = obj;
        this.d = predicate;
        this.e = yVar;
    }

    @Override // j$.util.stream.N4
    public int b() {
        return EnumC0040d4.u | (this.b ? 0 : EnumC0040d4.r);
    }

    @Override // j$.util.stream.N4
    public Object c(AbstractC0156y2 y2Var, u uVar) {
        return new C0072j0(this, y2Var, uVar).invoke();
    }

    @Override // j$.util.stream.N4
    public Object d(AbstractC0156y2 y2Var, u uVar) {
        O4 o4 = (O4) this.e.get();
        AbstractC0029c cVar = (AbstractC0029c) y2Var;
        o4.getClass();
        cVar.n0(cVar.v0(o4), uVar);
        Object obj = o4.get();
        return obj != null ? obj : this.c;
    }
}
