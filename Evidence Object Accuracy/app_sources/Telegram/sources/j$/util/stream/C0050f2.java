package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.t;

/* renamed from: j$.util.stream.f2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0050f2 extends AbstractC0068i2 implements t {
    /* access modifiers changed from: package-private */
    public C0050f2(AbstractC0135u1 u1Var) {
        super(u1Var);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.j(this, consumer);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.b(this, consumer);
    }
}
