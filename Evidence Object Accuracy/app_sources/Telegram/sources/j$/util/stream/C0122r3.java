package j$.util.stream;

/* renamed from: j$.util.stream.r3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0122r3 extends AbstractC0057g3 {
    long b;
    long c;
    final /* synthetic */ C0127s3 d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0122r3(C0127s3 s3Var, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = s3Var;
        this.b = s3Var.l;
        long j = s3Var.m;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.stream.AbstractC0081k3, j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        long j = this.b;
        if (j == 0) {
            long j2 = this.c;
            if (j2 > 0) {
                this.c = j2 - 1;
                this.a.accept(i);
                return;
            }
            return;
        }
        this.b = j - 1;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a.n(B3.c(j, this.d.l, this.c));
    }

    @Override // j$.util.stream.AbstractC0057g3, j$.util.stream.AbstractC0093m3
    public boolean o() {
        return this.c == 0 || this.a.o();
    }
}
