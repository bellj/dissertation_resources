package j$.util.stream;

import j$.util.function.k;
import j$.util.function.l;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.p2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0110p2 extends AbstractC0126s2 implements AbstractC0081k3 {
    private final int[] h;

    C0110p2(C0110p2 p2Var, u uVar, long j, long j2) {
        super(p2Var, uVar, j, j2, p2Var.h.length);
        this.h = p2Var.h;
    }

    /* access modifiers changed from: package-private */
    public C0110p2(u uVar, AbstractC0156y2 y2Var, int[] iArr) {
        super(uVar, y2Var, iArr.length);
        this.h = iArr;
    }

    @Override // j$.util.stream.AbstractC0126s2, j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        int i2 = this.f;
        if (i2 < this.g) {
            int[] iArr = this.h;
            this.f = i2 + 1;
            iArr[i2] = i;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.stream.AbstractC0126s2
    AbstractC0126s2 b(u uVar, long j, long j2) {
        return new C0110p2(this, uVar, j, j2);
    }

    /* renamed from: c */
    public /* synthetic */ void accept(Integer num) {
        AbstractC0103o1.b(this, num);
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }
}
