package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.u;
import j$.util.v;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class y4 extends z4 implements v {
    /* access modifiers changed from: package-private */
    public y4(v vVar, long j, long j2) {
        super(vVar, j, j2);
    }

    y4(v vVar, long j, long j2, long j3, long j4) {
        super(vVar, j, j2, j3, j4, null);
    }

    @Override // j$.util.stream.D4
    protected u a(u uVar, long j, long j2, long j3, long j4) {
        return new y4((v) uVar, j, j2, j3, j4);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.l(this, consumer);
    }

    @Override // j$.util.stream.z4
    protected /* bridge */ /* synthetic */ Object f() {
        return x4.a;
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.d(this, consumer);
    }
}
