package j$.util.stream;

import j$.util.L;
import j$.util.function.Consumer;
import j$.util.function.m;
import j$.util.function.q;
import j$.util.u;
import j$.util.w;
import java.util.Arrays;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.l2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public class C0086l2 implements AbstractC0155y1 {
    final long[] a;
    int b;

    /* access modifiers changed from: package-private */
    public C0086l2(long j) {
        if (j < 2147483639) {
            this.a = new long[(int) j];
            this.b = 0;
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    /* access modifiers changed from: package-private */
    public C0086l2(long[] jArr) {
        this.a = jArr;
        this.b = jArr.length;
    }

    @Override // j$.util.stream.AbstractC0160z1, j$.util.stream.A1
    public AbstractC0160z1 b(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A1
    public long count() {
        return (long) this.b;
    }

    @Override // j$.util.stream.AbstractC0160z1
    public void d(Object obj, int i) {
        System.arraycopy(this.a, 0, (long[]) obj, i, this.b);
    }

    @Override // j$.util.stream.AbstractC0160z1
    public Object e() {
        long[] jArr = this.a;
        int length = jArr.length;
        int i = this.b;
        return length == i ? jArr : Arrays.copyOf(jArr, i);
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0103o1.m(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0160z1
    public void g(Object obj) {
        q qVar = (q) obj;
        for (int i = 0; i < this.b; i++) {
            qVar.accept(this.a[i]);
        }
    }

    /* renamed from: j */
    public /* synthetic */ void i(Long[] lArr, int i) {
        AbstractC0103o1.j(this, lArr, i);
    }

    /* renamed from: k */
    public /* synthetic */ AbstractC0155y1 r(long j, long j2, m mVar) {
        return AbstractC0103o1.p(this, j, j2, mVar);
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ int p() {
        return 0;
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ Object[] q(m mVar) {
        return AbstractC0103o1.g(this, mVar);
    }

    @Override // j$.util.stream.AbstractC0160z1, j$.util.stream.A1
    /* renamed from: spliterator  reason: collision with other method in class */
    public w mo69spliterator() {
        return L.l(this.a, 0, this.b, 1040);
    }

    public String toString() {
        return String.format("LongArrayNode[%d][%s]", Integer.valueOf(this.a.length - this.b), Arrays.toString(this.a));
    }

    @Override // j$.util.stream.AbstractC0160z1, j$.util.stream.A1
    /* renamed from: spliterator */
    public u mo69spliterator() {
        return L.l(this.a, 0, this.b, 1040);
    }
}
