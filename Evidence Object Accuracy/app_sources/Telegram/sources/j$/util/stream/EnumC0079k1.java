package j$.util.stream;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.k1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC0079k1 {
    ANY(true, true),
    ALL(false, false),
    NONE(true, false);
    
    private final boolean a;
    private final boolean b;

    EnumC0079k1(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }
}
