package j$.util.stream;

import j$.util.u;

/* renamed from: j$.util.stream.m1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0091m1 extends AbstractC0035d {
    private final C0085l1 j;

    /* access modifiers changed from: package-private */
    public C0091m1(C0085l1 l1Var, AbstractC0156y2 y2Var, u uVar) {
        super(y2Var, uVar);
        this.j = l1Var;
    }

    C0091m1(C0091m1 m1Var, u uVar) {
        super(m1Var, uVar);
        this.j = m1Var.j;
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0047f
    public Object a() {
        AbstractC0156y2 y2Var = this.a;
        AbstractC0073j1 j1Var = (AbstractC0073j1) this.j.c.get();
        y2Var.u0(j1Var, this.b);
        boolean z = j1Var.b;
        if (z != this.j.b.b) {
            return null;
        }
        l(Boolean.valueOf(z));
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0047f
    public AbstractC0047f f(u uVar) {
        return new C0091m1(this, uVar);
    }

    @Override // j$.util.stream.AbstractC0035d
    protected Object k() {
        return Boolean.valueOf(!this.j.b.b);
    }
}
