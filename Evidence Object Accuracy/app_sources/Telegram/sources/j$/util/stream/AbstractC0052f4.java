package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.c;
import j$.util.function.y;
import j$.util.u;
import java.util.Comparator;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.f4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0052f4 implements u {
    final boolean a;
    final AbstractC0156y2 b;
    private y c;
    u d;
    AbstractC0093m3 e;
    c f;
    long g;
    AbstractC0041e h;
    boolean i;

    /* access modifiers changed from: package-private */
    public AbstractC0052f4(AbstractC0156y2 y2Var, y yVar, boolean z) {
        this.b = y2Var;
        this.c = yVar;
        this.d = null;
        this.a = z;
    }

    /* access modifiers changed from: package-private */
    public AbstractC0052f4(AbstractC0156y2 y2Var, u uVar, boolean z) {
        this.b = y2Var;
        this.c = null;
        this.d = uVar;
        this.a = z;
    }

    private boolean f() {
        boolean z;
        while (this.h.count() == 0) {
            if (!this.e.o()) {
                C0023b bVar = (C0023b) this.f;
                switch (bVar.a) {
                    case 4:
                        C0106o4 o4Var = (C0106o4) bVar.b;
                        z = o4Var.d.b(o4Var.e);
                        break;
                    case 5:
                        C0118q4 q4Var = (C0118q4) bVar.b;
                        z = q4Var.d.b(q4Var.e);
                        break;
                    case 6:
                        s4 s4Var = (s4) bVar.b;
                        z = s4Var.d.b(s4Var.e);
                        break;
                    default:
                        L4 l4 = (L4) bVar.b;
                        z = l4.d.b(l4.e);
                        break;
                }
                if (z) {
                    continue;
                }
            }
            if (this.i) {
                return false;
            }
            this.e.m();
            this.i = true;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        AbstractC0041e eVar = this.h;
        boolean z = false;
        if (eVar != null) {
            long j = this.g + 1;
            this.g = j;
            if (j < eVar.count()) {
                z = true;
            }
            if (z) {
                return z;
            }
            this.g = 0;
            this.h.clear();
            return f();
        } else if (this.i) {
            return false;
        } else {
            h();
            j();
            this.g = 0;
            this.e.n(this.d.getExactSizeIfKnown());
            return f();
        }
    }

    @Override // j$.util.u
    public final int characteristics() {
        h();
        int g = EnumC0040d4.g(this.b.s0()) & EnumC0040d4.f;
        return (g & 64) != 0 ? (g & -16449) | (this.d.characteristics() & 16448) : g;
    }

    @Override // j$.util.u
    public final long estimateSize() {
        h();
        return this.d.estimateSize();
    }

    @Override // j$.util.u
    public Comparator getComparator() {
        if (AbstractC0002a.f(this, 4)) {
            return null;
        }
        throw new IllegalStateException();
    }

    @Override // j$.util.u
    public final long getExactSizeIfKnown() {
        h();
        if (EnumC0040d4.SIZED.d(this.b.s0())) {
            return this.d.getExactSizeIfKnown();
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        if (this.d == null) {
            this.d = (u) this.c.get();
            this.c = null;
        }
    }

    @Override // j$.util.u
    public /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0002a.f(this, i);
    }

    abstract void j();

    abstract AbstractC0052f4 l(u uVar);

    public final String toString() {
        return String.format("%s[%s]", getClass().getName(), this.d);
    }

    @Override // j$.util.u
    public u trySplit() {
        if (!this.a || this.i) {
            return null;
        }
        h();
        u trySplit = this.d.trySplit();
        if (trySplit == null) {
            return null;
        }
        return l(trySplit);
    }
}
