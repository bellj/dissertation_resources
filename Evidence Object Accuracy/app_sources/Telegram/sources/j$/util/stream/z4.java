package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.w;
import java.util.Comparator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class z4 extends D4 implements w {
    /* access modifiers changed from: package-private */
    public z4(w wVar, long j, long j2) {
        super(wVar, j, j2, 0, Math.min(wVar.estimateSize(), j2));
    }

    protected abstract Object f();

    @Override // j$.util.w
    /* renamed from: forEachRemaining */
    public void e(Object obj) {
        obj.getClass();
        long j = this.a;
        long j2 = this.e;
        if (j < j2) {
            long j3 = this.d;
            if (j3 < j2) {
                if (j3 < j || ((w) this.c).estimateSize() + j3 > this.b) {
                    while (this.a > this.d) {
                        ((w) this.c).tryAdvance(f());
                        this.d++;
                    }
                    while (this.d < this.e) {
                        ((w) this.c).tryAdvance(obj);
                        this.d++;
                    }
                    return;
                }
                ((w) this.c).forEachRemaining(obj);
                this.d = this.e;
            }
        }
    }

    @Override // j$.util.u
    public Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.u
    public /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0002a.e(this);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0002a.f(this, i);
    }

    @Override // j$.util.w
    /* renamed from: tryAdvance */
    public boolean k(Object obj) {
        long j;
        obj.getClass();
        if (this.a >= this.e) {
            return false;
        }
        while (true) {
            long j2 = this.a;
            j = this.d;
            if (j2 <= j) {
                break;
            }
            ((w) this.c).tryAdvance(f());
            this.d++;
        }
        if (j >= this.e) {
            return false;
        }
        this.d = j + 1;
        return ((w) this.c).tryAdvance(obj);
    }

    /* access modifiers changed from: package-private */
    public z4(w wVar, long j, long j2, long j3, long j4, AbstractC0103o1 o1Var) {
        super(wVar, j, j2, j3, j4);
    }
}
