package j$.util.stream;

import j$.util.function.m;
import j$.util.u;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.x2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0151x2 {
    private static final A1 a = new Z1(null);
    private static final AbstractC0145w1 b = new X1();
    private static final AbstractC0155y1 c = new Y1();
    private static final AbstractC0135u1 d = new W1();
    private static final int[] e = new int[0];
    private static final long[] f = new long[0];
    private static final double[] g = new double[0];

    /* access modifiers changed from: package-private */
    public static AbstractC0125s1 d(long j, m mVar) {
        return (j < 0 || j >= 2147483639) ? new C0131t2() : new C0026b2(j, mVar);
    }

    public static A1 e(AbstractC0156y2 y2Var, u uVar, boolean z, m mVar) {
        long q0 = y2Var.q0(uVar);
        if (q0 < 0 || !uVar.hasCharacteristics(16384)) {
            A1 a1 = (A1) new H1(y2Var, mVar, uVar).invoke();
            return z ? l(a1, mVar) : a1;
        } else if (q0 < 2147483639) {
            Object[] objArr = (Object[]) mVar.apply((int) q0);
            new C0121r2(uVar, y2Var, objArr).invoke();
            return new D1(objArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static AbstractC0135u1 f(AbstractC0156y2 y2Var, u uVar, boolean z) {
        long q0 = y2Var.q0(uVar);
        if (q0 < 0 || !uVar.hasCharacteristics(16384)) {
            AbstractC0135u1 u1Var = (AbstractC0135u1) new H1(y2Var, uVar, 0).invoke();
            return z ? m(u1Var) : u1Var;
        } else if (q0 < 2147483639) {
            double[] dArr = new double[(int) q0];
            new C0104o2(uVar, y2Var, dArr).invoke();
            return new T1(dArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static AbstractC0145w1 g(AbstractC0156y2 y2Var, u uVar, boolean z) {
        long q0 = y2Var.q0(uVar);
        if (q0 < 0 || !uVar.hasCharacteristics(16384)) {
            AbstractC0145w1 w1Var = (AbstractC0145w1) new H1(y2Var, uVar, 1).invoke();
            return z ? n(w1Var) : w1Var;
        } else if (q0 < 2147483639) {
            int[] iArr = new int[(int) q0];
            new C0110p2(uVar, y2Var, iArr).invoke();
            return new C0032c2(iArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static AbstractC0155y1 h(AbstractC0156y2 y2Var, u uVar, boolean z) {
        long q0 = y2Var.q0(uVar);
        if (q0 < 0 || !uVar.hasCharacteristics(16384)) {
            AbstractC0155y1 y1Var = (AbstractC0155y1) new H1(y2Var, uVar, 2).invoke();
            return z ? o(y1Var) : y1Var;
        } else if (q0 < 2147483639) {
            long[] jArr = new long[(int) q0];
            new C0116q2(uVar, y2Var, jArr).invoke();
            return new C0086l2(jArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    /* access modifiers changed from: package-private */
    public static A1 i(EnumC0046e4 e4Var, A1 a1, A1 a12) {
        int i = B1.a[e4Var.ordinal()];
        if (i == 1) {
            return new S1(a1, a12);
        }
        if (i == 2) {
            return new P1((AbstractC0145w1) a1, (AbstractC0145w1) a12);
        }
        if (i == 3) {
            return new Q1((AbstractC0155y1) a1, (AbstractC0155y1) a12);
        }
        if (i == 4) {
            return new O1((AbstractC0135u1) a1, (AbstractC0135u1) a12);
        }
        throw new IllegalStateException("Unknown shape " + e4Var);
    }

    /* access modifiers changed from: package-private */
    public static AbstractC0109p1 j(long j) {
        return (j < 0 || j >= 2147483639) ? new V1() : new U1(j);
    }

    /* access modifiers changed from: package-private */
    public static A1 k(EnumC0046e4 e4Var) {
        int i = B1.a[e4Var.ordinal()];
        if (i == 1) {
            return a;
        }
        if (i == 2) {
            return b;
        }
        if (i == 3) {
            return c;
        }
        if (i == 4) {
            return d;
        }
        throw new IllegalStateException("Unknown shape " + e4Var);
    }

    public static A1 l(A1 a1, m mVar) {
        if (a1.p() <= 0) {
            return a1;
        }
        long count = a1.count();
        if (count < 2147483639) {
            Object[] objArr = (Object[]) mVar.apply((int) count);
            new C0141v2(a1, objArr, 0, (B1) null).invoke();
            return new D1(objArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0135u1 m(AbstractC0135u1 u1Var) {
        if (u1Var.p() <= 0) {
            return u1Var;
        }
        long count = u1Var.count();
        if (count < 2147483639) {
            double[] dArr = new double[(int) count];
            new C0136u2(u1Var, dArr, 0).invoke();
            return new T1(dArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0145w1 n(AbstractC0145w1 w1Var) {
        if (w1Var.p() <= 0) {
            return w1Var;
        }
        long count = w1Var.count();
        if (count < 2147483639) {
            int[] iArr = new int[(int) count];
            new C0136u2(w1Var, iArr, 0).invoke();
            return new C0032c2(iArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0155y1 o(AbstractC0155y1 y1Var) {
        if (y1Var.p() <= 0) {
            return y1Var;
        }
        long count = y1Var.count();
        if (count < 2147483639) {
            long[] jArr = new long[(int) count];
            new C0136u2(y1Var, jArr, 0).invoke();
            return new C0086l2(jArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    /* access modifiers changed from: package-private */
    public static AbstractC0115q1 p(long j) {
        return (j < 0 || j >= 2147483639) ? new C0044e2() : new C0038d2(j);
    }

    /* access modifiers changed from: package-private */
    public static AbstractC0120r1 q(long j) {
        return (j < 0 || j >= 2147483639) ? new C0098n2() : new C0092m2(j);
    }
}
