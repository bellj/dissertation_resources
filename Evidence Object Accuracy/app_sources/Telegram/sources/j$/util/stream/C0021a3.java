package j$.util.stream;

import j$.lang.a;
import j$.util.function.Function;

/* renamed from: j$.util.stream.a3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0021a3 extends AbstractC0039d3 {
    public final /* synthetic */ int l;
    final /* synthetic */ Function m;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0021a3(AbstractC0045e3 e3Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, Function function, int i2) {
        super(cVar, e4Var, i);
        this.l = i2;
        if (i2 != 1) {
            this.m = function;
            return;
        }
        this.m = function;
        super(cVar, e4Var, i);
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        switch (this.l) {
            case 0:
                return new Y2(this, m3Var);
            default:
                return new Y2(this, m3Var, (a) null);
        }
    }
}
