package j$.util.stream;

import j$.util.function.p;
import j$.util.function.q;

/* renamed from: j$.util.stream.i4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0070i4 extends AbstractC0076j4 implements q {
    final long[] c;

    /* access modifiers changed from: package-private */
    public C0070i4(int i) {
        this.c = new long[i];
    }

    @Override // j$.util.function.q
    public void accept(long j) {
        long[] jArr = this.c;
        int i = this.b;
        this.b = i + 1;
        jArr[i] = j;
    }

    @Override // j$.util.stream.AbstractC0076j4
    public void b(Object obj, long j) {
        q qVar = (q) obj;
        for (int i = 0; ((long) i) < j; i++) {
            qVar.accept(this.c[i]);
        }
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }
}
