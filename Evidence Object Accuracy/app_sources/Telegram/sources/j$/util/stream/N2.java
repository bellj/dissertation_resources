package j$.util.stream;

import j$.util.C0013k;
import j$.util.function.Consumer;
import j$.util.function.j;
import j$.util.function.k;
import j$.util.function.l;

/* loaded from: classes2.dex */
class N2 implements S2, AbstractC0081k3 {
    private boolean a;
    private int b;
    final /* synthetic */ j c;

    /* access modifiers changed from: package-private */
    public N2(j jVar) {
        this.c = jVar;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(double d) {
        AbstractC0103o1.f(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        if (this.a) {
            this.a = false;
        } else {
            i = this.c.applyAsInt(this.b, i);
        }
        this.b = i;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Integer num) {
        AbstractC0103o1.b(this, num);
    }

    @Override // j$.util.function.y
    public Object get() {
        return this.a ? C0013k.a() : C0013k.d(this.b);
    }

    @Override // j$.util.stream.S2
    public void h(S2 s2) {
        N2 n2 = (N2) s2;
        if (!n2.a) {
            accept(n2.b);
        }
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a = true;
        this.b = 0;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
