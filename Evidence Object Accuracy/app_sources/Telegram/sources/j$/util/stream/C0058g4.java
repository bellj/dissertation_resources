package j$.util.stream;

import j$.util.function.e;
import j$.util.function.f;

/* renamed from: j$.util.stream.g4  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0058g4 extends AbstractC0076j4 implements f {
    final double[] c;

    /* access modifiers changed from: package-private */
    public C0058g4(int i) {
        this.c = new double[i];
    }

    @Override // j$.util.function.f
    public void accept(double d) {
        double[] dArr = this.c;
        int i = this.b;
        this.b = i + 1;
        dArr[i] = d;
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0076j4
    public void b(Object obj, long j) {
        f fVar = (f) obj;
        for (int i = 0; ((long) i) < j; i++) {
            fVar.accept(this.c[i]);
        }
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }
}
