package j$.util.stream;

import j$.util.C0012j;
import j$.util.function.Consumer;
import j$.util.function.d;
import j$.util.function.e;
import j$.util.function.f;

/* loaded from: classes2.dex */
class E2 implements S2, AbstractC0075j3 {
    private boolean a;
    private double b;
    final /* synthetic */ d c;

    /* access modifiers changed from: package-private */
    public E2(d dVar) {
        this.c = dVar;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        if (this.a) {
            this.a = false;
        } else {
            d = this.c.applyAsDouble(this.b, d);
        }
        this.b = d;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void accept(int i) {
        AbstractC0103o1.d(this);
        throw null;
    }

    @Override // j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public /* synthetic */ void accept(long j) {
        AbstractC0103o1.e(this);
        throw null;
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Double d) {
        AbstractC0103o1.a(this, d);
    }

    @Override // j$.util.function.y
    public Object get() {
        return this.a ? C0012j.a() : C0012j.d(this.b);
    }

    @Override // j$.util.stream.S2
    public void h(S2 s2) {
        E2 e2 = (E2) s2;
        if (!e2.a) {
            accept(e2.b);
        }
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ void m() {
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a = true;
        this.b = 0.0d;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public /* synthetic */ boolean o() {
        return false;
    }
}
