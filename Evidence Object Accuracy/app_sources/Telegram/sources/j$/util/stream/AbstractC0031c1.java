package j$.util.stream;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.c1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0031c1 extends AbstractC0037d1 {
    /* access modifiers changed from: package-private */
    public AbstractC0031c1(AbstractC0029c cVar, EnumC0046e4 e4Var, int i) {
        super(cVar, i);
    }

    @Override // j$.util.stream.AbstractC0029c
    final boolean G0() {
        return false;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ AbstractC0043e1 parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ AbstractC0043e1 sequential() {
        sequential();
        return this;
    }
}
