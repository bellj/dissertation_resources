package j$.util.stream;

import j$.util.C0011i;
import j$.util.C0012j;
import j$.util.C0014l;
import j$.util.L;
import j$.util.function.BiConsumer;
import j$.util.function.m;
import j$.util.function.o;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.t;
import j$.util.function.w;
import j$.util.function.y;
import j$.util.u;
import j$.util.v;
import j$.wrappers.C0182j0;
import j$.wrappers.C0186l0;
import j$.wrappers.C0190n0;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.d1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0037d1 extends AbstractC0029c implements AbstractC0043e1 {
    /* access modifiers changed from: package-private */
    public AbstractC0037d1(AbstractC0029c cVar, int i) {
        super(cVar, i);
    }

    /* access modifiers changed from: package-private */
    public AbstractC0037d1(u uVar, int i, boolean z) {
        super(uVar, i, z);
    }

    /* access modifiers changed from: private */
    public static v M0(u uVar) {
        if (uVar instanceof v) {
            return (v) uVar;
        }
        if (Q4.a) {
            Q4.a(AbstractC0029c.class, "using LongStream.adapt(Spliterator<Long> s)");
            throw null;
        }
        throw new UnsupportedOperationException("LongStream.adapt(Spliterator<Long> s)");
    }

    @Override // j$.util.stream.AbstractC0029c
    final void A0(u uVar, AbstractC0093m3 m3Var) {
        q qVar;
        v M0 = M0(uVar);
        if (m3Var instanceof q) {
            qVar = (q) m3Var;
        } else if (!Q4.a) {
            qVar = new W0(m3Var);
        } else {
            Q4.a(AbstractC0029c.class, "using LongStream.adapt(Sink<Long> s)");
            throw null;
        }
        while (!m3Var.o() && M0.i(qVar)) {
        }
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public final EnumC0046e4 B0() {
        return EnumC0046e4.LONG_VALUE;
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final long D(long j, o oVar) {
        oVar.getClass();
        return ((Long) x0(new P2(EnumC0046e4.LONG_VALUE, oVar, j))).longValue();
    }

    @Override // j$.util.stream.AbstractC0029c
    final u K0(AbstractC0156y2 y2Var, y yVar, boolean z) {
        return new s4(y2Var, yVar, z);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final boolean L(C0182j0 j0Var) {
        return ((Boolean) x0(AbstractC0103o1.w(j0Var, EnumC0079k1.ALL))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final U O(C0186l0 l0Var) {
        l0Var.getClass();
        return new K(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.p | EnumC0040d4.n, l0Var);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final Stream Q(r rVar) {
        rVar.getClass();
        return new L(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.p | EnumC0040d4.n, rVar);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final boolean S(C0182j0 j0Var) {
        return ((Boolean) x0(AbstractC0103o1.w(j0Var, EnumC0079k1.NONE))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public void Z(q qVar) {
        qVar.getClass();
        x0(new C0090m0(qVar, true));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final U asDoubleStream() {
        return new O(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.p | EnumC0040d4.n);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0012j average() {
        long[] jArr = (long[]) f0(P0.a, O0.a, R0.a);
        if (jArr[0] <= 0) {
            return C0012j.a();
        }
        double d = (double) jArr[1];
        double d2 = (double) jArr[0];
        Double.isNaN(d);
        Double.isNaN(d2);
        return C0012j.d(d / d2);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final Stream boxed() {
        return Q(X0.a);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final long count() {
        return ((AbstractC0037d1) z(Y0.a)).sum();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public void d(q qVar) {
        qVar.getClass();
        x0(new C0090m0(qVar, false));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 distinct() {
        return ((AbstractC0045e3) Q(X0.a)).distinct().g0(Q0.a);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final IntStream e0(C0190n0 n0Var) {
        n0Var.getClass();
        return new M(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.p | EnumC0040d4.n, n0Var);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final Object f0(y yVar, w wVar, BiConsumer biConsumer) {
        C c = new C(biConsumer, 2);
        yVar.getClass();
        wVar.getClass();
        return x0(new C0161z2(EnumC0046e4.LONG_VALUE, c, wVar, yVar));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0014l findAny() {
        return (C0014l) x0(new C0036d0(false, EnumC0046e4.LONG_VALUE, C0014l.a(), Y.a, C0024b0.a));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0014l findFirst() {
        return (C0014l) x0(new C0036d0(true, EnumC0046e4.LONG_VALUE, C0014l.a(), Y.a, C0024b0.a));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0014l g(o oVar) {
        oVar.getClass();
        return (C0014l) x0(new D2(EnumC0046e4.LONG_VALUE, oVar));
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    public final j$.util.r mo66iterator() {
        return L.h(spliterator());
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator  reason: collision with other method in class */
    public Iterator mo66iterator() {
        return L.h(spliterator());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final boolean k(C0182j0 j0Var) {
        return ((Boolean) x0(AbstractC0103o1.w(j0Var, EnumC0079k1.ANY))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 limit(long j) {
        if (j >= 0) {
            return B3.h(this, 0, j);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0014l max() {
        return g(U0.a);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0014l min() {
        return g(V0.a);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 p(q qVar) {
        qVar.getClass();
        return new N(this, this, EnumC0046e4.LONG_VALUE, 0, qVar);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 s(r rVar) {
        return new N(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.p | EnumC0040d4.n | EnumC0040d4.t, rVar);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 skip(long j) {
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i >= 0) {
            return i == 0 ? this : B3.h(this, j, -1);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 sorted() {
        return new L3(this);
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g
    public final v spliterator() {
        return M0(super.spliterator());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final long sum() {
        return ((Long) x0(new P2(EnumC0046e4.LONG_VALUE, T0.a, 0))).longValue();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final C0011i summaryStatistics() {
        return (C0011i) f0(C0077k.a, N0.a, M0.a);
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0156y2
    public final AbstractC0125s1 t0(long j, m mVar) {
        return AbstractC0151x2.q(j);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final long[] toArray() {
        return (long[]) AbstractC0151x2.o((AbstractC0155y1) y0(S0.a)).e();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 u(C0182j0 j0Var) {
        j0Var.getClass();
        return new N(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.t, j0Var);
    }

    @Override // j$.util.stream.AbstractC0053g
    public AbstractC0053g unordered() {
        return !C0() ? this : new G0(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.r);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public final AbstractC0043e1 z(t tVar) {
        tVar.getClass();
        return new N(this, this, EnumC0046e4.LONG_VALUE, EnumC0040d4.p | EnumC0040d4.n, tVar);
    }

    @Override // j$.util.stream.AbstractC0029c
    final A1 z0(AbstractC0156y2 y2Var, u uVar, boolean z, m mVar) {
        return AbstractC0151x2.h(y2Var, uVar, z);
    }
}
