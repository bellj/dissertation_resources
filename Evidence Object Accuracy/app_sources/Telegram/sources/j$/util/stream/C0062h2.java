package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.v;

/* renamed from: j$.util.stream.h2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0062h2 extends AbstractC0068i2 implements v {
    /* access modifiers changed from: package-private */
    public C0062h2(AbstractC0155y1 y1Var) {
        super(y1Var);
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.l(this, consumer);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.d(this, consumer);
    }
}
