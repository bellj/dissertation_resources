package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.u;
import java.util.Deque;

/* renamed from: j$.util.stream.j2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0074j2 extends AbstractC0080k2 {
    /* access modifiers changed from: package-private */
    public C0074j2(A1 a1) {
        super(a1);
    }

    @Override // j$.util.u
    public boolean b(Consumer consumer) {
        A1 a;
        if (!h()) {
            return false;
        }
        boolean b = this.d.b(consumer);
        if (!b) {
            if (this.c != null || (a = a(this.e)) == null) {
                this.a = null;
            } else {
                u spliterator = a.mo69spliterator();
                this.d = spliterator;
                return spliterator.b(consumer);
            }
        }
        return b;
    }

    @Override // j$.util.u
    public void forEachRemaining(Consumer consumer) {
        if (this.a != null) {
            if (this.d == null) {
                u uVar = this.c;
                if (uVar == null) {
                    Deque f = f();
                    while (true) {
                        A1 a = a(f);
                        if (a != null) {
                            a.forEach(consumer);
                        } else {
                            this.a = null;
                            return;
                        }
                    }
                } else {
                    uVar.forEachRemaining(consumer);
                }
            } else {
                do {
                } while (b(consumer));
            }
        }
    }
}
