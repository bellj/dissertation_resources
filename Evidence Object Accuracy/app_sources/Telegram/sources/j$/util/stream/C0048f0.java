package j$.util.stream;

import j$.util.C0013k;
import j$.util.function.k;
import j$.util.function.l;

/* renamed from: j$.util.stream.f0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
final class C0048f0 extends AbstractC0066i0 implements AbstractC0081k3 {
    @Override // j$.util.stream.AbstractC0066i0, j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        accept(Integer.valueOf(i));
    }

    @Override // j$.util.function.y
    public Object get() {
        if (this.a) {
            return C0013k.d(((Integer) this.b).intValue());
        }
        return null;
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }
}
