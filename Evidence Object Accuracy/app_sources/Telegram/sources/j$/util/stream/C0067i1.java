package j$.util.stream;

import j$.util.function.e;
import j$.util.function.f;
import j$.wrappers.E;

/* renamed from: j$.util.stream.i1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0067i1 extends AbstractC0073j1 implements AbstractC0075j3 {
    final /* synthetic */ EnumC0079k1 c;
    final /* synthetic */ E d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0067i1(EnumC0079k1 k1Var, E e) {
        super(k1Var);
        this.c = k1Var;
        this.d = e;
    }

    @Override // j$.util.stream.AbstractC0073j1, j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        if (!this.a && this.d.b(d) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Double d) {
        AbstractC0103o1.a(this, d);
    }

    @Override // j$.util.function.f
    public f j(f fVar) {
        fVar.getClass();
        return new e(this, fVar);
    }
}
