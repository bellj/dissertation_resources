package j$.util.stream;

import j$.lang.a;
import j$.lang.b;
import j$.lang.c;
import j$.util.function.Function;
import j$.util.function.f;
import j$.util.function.g;
import j$.util.function.z;
import j$.wrappers.C0186l0;
import j$.wrappers.E;
import j$.wrappers.X;

/* loaded from: classes2.dex */
class K extends S {
    public final /* synthetic */ int l = 4;
    final /* synthetic */ Object m;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(T t, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, f fVar) {
        super(cVar, e4Var, i);
        this.m = fVar;
    }

    /* access modifiers changed from: package-private */
    @Override // j$.util.stream.AbstractC0029c
    public AbstractC0093m3 H0(int i, AbstractC0093m3 m3Var) {
        switch (this.l) {
            case 0:
                return new J(this, m3Var);
            case 1:
                return new J(this, m3Var, (a) null);
            case 2:
                return new J(this, m3Var, (b) null);
            case 3:
                return new J(this, m3Var, (c) null);
            case 4:
                return new F0(this, m3Var);
            case 5:
                return new Z0(this, m3Var);
            case 6:
                return new Y2(this, m3Var);
            default:
                return new r(this, m3Var);
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(T t, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, g gVar) {
        super(cVar, e4Var, i);
        this.m = gVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(T t, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, E e) {
        super(cVar, e4Var, i);
        this.m = e;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(T t, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, j$.wrappers.K k) {
        super(cVar, e4Var, i);
        this.m = k;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(L0 l0, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, X x) {
        super(cVar, e4Var, i);
        this.m = x;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(AbstractC0037d1 d1Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, C0186l0 l0Var) {
        super(cVar, e4Var, i);
        this.m = l0Var;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(AbstractC0045e3 e3Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, Function function) {
        super(cVar, e4Var, i);
        this.m = function;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public K(AbstractC0045e3 e3Var, AbstractC0029c cVar, EnumC0046e4 e4Var, int i, z zVar) {
        super(cVar, e4Var, i);
        this.m = zVar;
    }
}
