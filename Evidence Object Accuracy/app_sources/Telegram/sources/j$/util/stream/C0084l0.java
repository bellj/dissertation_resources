package j$.util.stream;

import j$.util.function.k;
import j$.util.function.l;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.l0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0084l0 extends AbstractC0102o0 implements AbstractC0081k3 {
    final l b;

    /* access modifiers changed from: package-private */
    public C0084l0(l lVar, boolean z) {
        super(z);
        this.b = lVar;
    }

    @Override // j$.util.stream.AbstractC0102o0, j$.util.stream.AbstractC0093m3
    public void accept(int i) {
        this.b.accept(i);
    }

    /* renamed from: e */
    public /* synthetic */ void accept(Integer num) {
        AbstractC0103o1.b(this, num);
    }

    @Override // j$.util.function.l
    public l l(l lVar) {
        lVar.getClass();
        return new k(this, lVar);
    }
}
