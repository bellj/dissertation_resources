package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.m;
import j$.util.u;
import j$.util.w;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class O1 extends R1 implements AbstractC0135u1 {
    /* access modifiers changed from: package-private */
    public O1(AbstractC0135u1 u1Var, AbstractC0135u1 u1Var2) {
        super(u1Var, u1Var2);
    }

    /* renamed from: a */
    public /* synthetic */ void i(Double[] dArr, int i) {
        AbstractC0103o1.h(this, dArr, i);
    }

    /* renamed from: f */
    public double[] c(int i) {
        return new double[i];
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0103o1.k(this, consumer);
    }

    /* renamed from: h */
    public /* synthetic */ AbstractC0135u1 r(long j, long j2, m mVar) {
        return AbstractC0103o1.n(this, j, j2, mVar);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator  reason: collision with other method in class */
    public w mo69spliterator() {
        return new C0050f2(this);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator */
    public u mo69spliterator() {
        return new C0050f2(this);
    }
}
