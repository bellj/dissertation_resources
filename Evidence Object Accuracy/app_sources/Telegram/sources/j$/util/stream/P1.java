package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.m;
import j$.util.u;
import j$.util.w;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class P1 extends R1 implements AbstractC0145w1 {
    /* access modifiers changed from: package-private */
    public P1(AbstractC0145w1 w1Var, AbstractC0145w1 w1Var2) {
        super(w1Var, w1Var2);
    }

    /* renamed from: a */
    public /* synthetic */ void i(Integer[] numArr, int i) {
        AbstractC0103o1.i(this, numArr, i);
    }

    /* renamed from: f */
    public int[] c(int i) {
        return new int[i];
    }

    @Override // j$.util.stream.A1
    public /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0103o1.l(this, consumer);
    }

    /* renamed from: h */
    public /* synthetic */ AbstractC0145w1 r(long j, long j2, m mVar) {
        return AbstractC0103o1.o(this, j, j2, mVar);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator  reason: collision with other method in class */
    public w mo69spliterator() {
        return new C0056g2(this);
    }

    @Override // j$.util.stream.A1
    /* renamed from: spliterator */
    public u mo69spliterator() {
        return new C0056g2(this);
    }
}
