package j$.util.stream;

import j$.util.C0011i;
import j$.util.C0012j;
import j$.util.C0014l;
import j$.util.function.BiConsumer;
import j$.util.function.o;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.t;
import j$.util.function.w;
import j$.util.function.y;
import j$.util.v;
import j$.wrappers.C0182j0;
import j$.wrappers.C0186l0;
import j$.wrappers.C0190n0;

/* renamed from: j$.util.stream.e1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC0043e1 extends AbstractC0053g {
    long D(long j, o oVar);

    boolean L(C0182j0 j0Var);

    U O(C0186l0 l0Var);

    Stream Q(r rVar);

    boolean S(C0182j0 j0Var);

    void Z(q qVar);

    U asDoubleStream();

    C0012j average();

    Stream boxed();

    long count();

    void d(q qVar);

    AbstractC0043e1 distinct();

    IntStream e0(C0190n0 n0Var);

    Object f0(y yVar, w wVar, BiConsumer biConsumer);

    C0014l findAny();

    C0014l findFirst();

    C0014l g(o oVar);

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    j$.util.r mo66iterator();

    boolean k(C0182j0 j0Var);

    AbstractC0043e1 limit(long j);

    C0014l max();

    C0014l min();

    AbstractC0043e1 p(q qVar);

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    AbstractC0043e1 parallel();

    AbstractC0043e1 s(r rVar);

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    AbstractC0043e1 sequential();

    AbstractC0043e1 skip(long j);

    AbstractC0043e1 sorted();

    @Override // j$.util.stream.AbstractC0053g
    v spliterator();

    long sum();

    C0011i summaryStatistics();

    long[] toArray();

    AbstractC0043e1 u(C0182j0 j0Var);

    AbstractC0043e1 z(t tVar);
}
