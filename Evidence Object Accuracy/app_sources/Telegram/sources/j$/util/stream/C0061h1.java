package j$.util.stream;

import j$.util.function.p;
import j$.util.function.q;
import j$.wrappers.C0182j0;

/* renamed from: j$.util.stream.h1  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0061h1 extends AbstractC0073j1 implements AbstractC0087l3 {
    final /* synthetic */ EnumC0079k1 c;
    final /* synthetic */ C0182j0 d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0061h1(EnumC0079k1 k1Var, C0182j0 j0Var) {
        super(k1Var);
        this.c = k1Var;
        this.d = j0Var;
    }

    @Override // j$.util.stream.AbstractC0073j1, j$.util.stream.AbstractC0093m3, j$.util.stream.AbstractC0087l3, j$.util.function.q
    public void accept(long j) {
        if (!this.a && this.d.b(j) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }

    /* renamed from: b */
    public /* synthetic */ void accept(Long l) {
        AbstractC0103o1.c(this, l);
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }
}
