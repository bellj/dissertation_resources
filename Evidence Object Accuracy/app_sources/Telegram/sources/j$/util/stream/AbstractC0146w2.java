package j$.util.stream;

import java.util.concurrent.CountedCompleter;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.w2  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC0146w2 extends CountedCompleter {
    protected final A1 a;
    protected final int b;

    /* access modifiers changed from: package-private */
    public AbstractC0146w2(A1 a1, int i) {
        this.a = a1;
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public AbstractC0146w2(AbstractC0146w2 w2Var, A1 a1, int i) {
        super(w2Var);
        this.a = a1;
        this.b = i;
    }

    abstract void a();

    abstract AbstractC0146w2 b(int i, int i2);

    @Override // java.util.concurrent.CountedCompleter
    public void compute() {
        AbstractC0146w2 w2Var = this;
        while (w2Var.a.p() != 0) {
            w2Var.setPendingCount(w2Var.a.p() - 1);
            int i = 0;
            int i2 = 0;
            while (i < w2Var.a.p() - 1) {
                AbstractC0146w2 b = w2Var.b(i, w2Var.b + i2);
                i2 = (int) (((long) i2) + b.a.count());
                b.fork();
                i++;
            }
            w2Var = w2Var.b(i, w2Var.b + i2);
        }
        w2Var.a();
        w2Var.propagateCompletion();
    }
}
