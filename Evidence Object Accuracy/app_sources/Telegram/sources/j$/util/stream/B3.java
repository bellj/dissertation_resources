package j$.util.stream;

import j$.util.t;
import j$.util.u;
import j$.util.v;

/* loaded from: classes2.dex */
abstract class B3 {
    /* access modifiers changed from: package-private */
    public static u b(EnumC0046e4 e4Var, u uVar, long j, long j2) {
        long d = d(j, j2);
        int i = AbstractC0162z3.a[e4Var.ordinal()];
        if (i == 1) {
            return new C4(uVar, j, d);
        }
        if (i == 2) {
            return new w4((u.a) uVar, j, d);
        }
        if (i == 3) {
            return new y4((v) uVar, j, d);
        }
        if (i == 4) {
            return new u4((t) uVar, j, d);
        }
        throw new IllegalStateException("Unknown shape " + e4Var);
    }

    /* access modifiers changed from: package-private */
    public static long c(long j, long j2, long j3) {
        if (j >= 0) {
            return Math.max(-1L, Math.min(j - j2, j3));
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public static long d(long j, long j2) {
        long j3 = j2 >= 0 ? j + j2 : Long.MAX_VALUE;
        if (j3 >= 0) {
            return j3;
        }
        return Long.MAX_VALUE;
    }

    private static int e(long j) {
        return (j != -1 ? EnumC0040d4.u : 0) | EnumC0040d4.t;
    }

    public static U f(AbstractC0029c cVar, long j, long j2) {
        if (j >= 0) {
            return new C0157y3(cVar, EnumC0046e4.DOUBLE_VALUE, e(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static IntStream g(AbstractC0029c cVar, long j, long j2) {
        if (j >= 0) {
            return new C0127s3(cVar, EnumC0046e4.INT_VALUE, e(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static AbstractC0043e1 h(AbstractC0029c cVar, long j, long j2) {
        if (j >= 0) {
            return new C0142v3(cVar, EnumC0046e4.LONG_VALUE, e(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static Stream i(AbstractC0029c cVar, long j, long j2) {
        if (j >= 0) {
            return new C0111p3(cVar, EnumC0046e4.REFERENCE, e(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }
}
