package j$.util.stream;

/* renamed from: j$.util.stream.x3  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
class C0152x3 extends AbstractC0051f3 {
    long b;
    long c;
    final /* synthetic */ C0157y3 d;

    /* access modifiers changed from: package-private */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0152x3(C0157y3 y3Var, AbstractC0093m3 m3Var) {
        super(m3Var);
        this.d = y3Var;
        this.b = y3Var.l;
        long j = y3Var.m;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.stream.AbstractC0075j3, j$.util.stream.AbstractC0093m3
    public void accept(double d) {
        long j = this.b;
        if (j == 0) {
            long j2 = this.c;
            if (j2 > 0) {
                this.c = j2 - 1;
                this.a.accept(d);
                return;
            }
            return;
        }
        this.b = j - 1;
    }

    @Override // j$.util.stream.AbstractC0093m3
    public void n(long j) {
        this.a.n(B3.c(j, this.d.l, this.c));
    }

    @Override // j$.util.stream.AbstractC0051f3, j$.util.stream.AbstractC0093m3
    public boolean o() {
        return this.c == 0 || this.a.o();
    }
}
