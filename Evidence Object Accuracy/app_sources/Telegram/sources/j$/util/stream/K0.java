package j$.util.stream;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class K0 extends L0 {
    /* access modifiers changed from: package-private */
    public K0(AbstractC0029c cVar, EnumC0046e4 e4Var, int i) {
        super(cVar, i);
    }

    @Override // j$.util.stream.AbstractC0029c
    final boolean G0() {
        return false;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ IntStream parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0029c, j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* bridge */ /* synthetic */ IntStream sequential() {
        sequential();
        return this;
    }
}
