package j$.util.stream;

import j$.util.AbstractC0002a;
import j$.util.function.Consumer;
import j$.util.function.p;
import j$.util.function.q;
import j$.util.u;
import j$.util.v;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class G4 extends H4 implements v, q {
    long e;

    /* access modifiers changed from: package-private */
    public G4(v vVar, long j, long j2) {
        super(vVar, j, j2);
    }

    G4(v vVar, G4 g4) {
        super(vVar, g4);
    }

    @Override // j$.util.function.q
    public void accept(long j) {
        this.e = j;
    }

    @Override // j$.util.u
    public /* synthetic */ boolean b(Consumer consumer) {
        return AbstractC0002a.l(this, consumer);
    }

    @Override // j$.util.function.q
    public q f(q qVar) {
        qVar.getClass();
        return new p(this, qVar);
    }

    @Override // j$.util.u
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0002a.d(this, consumer);
    }

    @Override // j$.util.stream.J4
    protected u q(u uVar) {
        return new G4((v) uVar, this);
    }

    @Override // j$.util.stream.H4
    protected void s(Object obj) {
        ((q) obj).accept(this.e);
    }

    @Override // j$.util.stream.H4
    protected AbstractC0076j4 t(int i) {
        return new C0070i4(i);
    }
}
