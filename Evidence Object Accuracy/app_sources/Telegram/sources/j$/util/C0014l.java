package j$.util;

import java.util.NoSuchElementException;

/* renamed from: j$.util.l  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C0014l {
    private static final C0014l c = new C0014l();
    private final boolean a;
    private final long b;

    private C0014l() {
        this.a = false;
        this.b = 0;
    }

    private C0014l(long j) {
        this.a = true;
        this.b = j;
    }

    public static C0014l a() {
        return c;
    }

    public static C0014l d(long j) {
        return new C0014l(j);
    }

    public long b() {
        if (this.a) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public boolean c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0014l)) {
            return false;
        }
        C0014l lVar = (C0014l) obj;
        boolean z = this.a;
        if (!z || !lVar.a) {
            if (z == lVar.a) {
                return true;
            }
        } else if (this.b == lVar.b) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (!this.a) {
            return 0;
        }
        long j = this.b;
        return (int) (j ^ (j >>> 32));
    }

    public String toString() {
        return this.a ? String.format("OptionalLong[%s]", Long.valueOf(this.b)) : "OptionalLong.empty";
    }
}
