package j$.time.temporal;

import j$.time.b;

/* loaded from: classes2.dex */
enum i implements m {
    WEEK_BASED_YEARS("WeekBasedYears", b.c(31556952)),
    QUARTER_YEARS("QuarterYears", b.c(7889238));
    
    private final String a;

    i(String str, b bVar) {
        this.a = str;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.a;
    }
}
