package j$.time.temporal;

/* loaded from: classes2.dex */
enum h implements k {
    DAY_OF_QUARTER {
        @Override // j$.time.temporal.k
        public j$.time.temporal.n a(
/*
[11] Method generation error in method: j$.time.temporal.d.a():j$.time.temporal.n, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.d.a():j$.time.temporal.n, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/

        @Override // java.lang.Enum, java.lang.Object
        public java.lang.String toString(
/*
[3] Method generation error in method: j$.time.temporal.d.toString():java.lang.String, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.d.toString():java.lang.String, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/
    },
    QUARTER_OF_YEAR {
        @Override // j$.time.temporal.k
        public j$.time.temporal.n a(
/*
[9] Method generation error in method: j$.time.temporal.e.a():j$.time.temporal.n, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.e.a():j$.time.temporal.n, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/

        @Override // java.lang.Enum, java.lang.Object
        public java.lang.String toString(
/*
[3] Method generation error in method: j$.time.temporal.e.toString():java.lang.String, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.e.toString():java.lang.String, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/
    },
    WEEK_OF_WEEK_BASED_YEAR {
        @Override // j$.time.temporal.k
        public j$.time.temporal.n a(
/*
[11] Method generation error in method: j$.time.temporal.f.a():j$.time.temporal.n, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.f.a():j$.time.temporal.n, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/

        @Override // java.lang.Enum, java.lang.Object
        public java.lang.String toString(
/*
[3] Method generation error in method: j$.time.temporal.f.toString():java.lang.String, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.f.toString():java.lang.String, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/
    },
    WEEK_BASED_YEAR {
        @Override // j$.time.temporal.k
        public j$.time.temporal.n a(
/*
[7] Method generation error in method: j$.time.temporal.g.a():j$.time.temporal.n, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.g.a():j$.time.temporal.n, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/

        @Override // java.lang.Enum, java.lang.Object
        public java.lang.String toString(
/*
[3] Method generation error in method: j$.time.temporal.g.toString():java.lang.String, file: classes2.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Method arg registers not loaded: j$.time.temporal.g.toString():java.lang.String, class status: GENERATED_AND_UNLOADED
        	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:297)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:138)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        
*/
    };

    /* access modifiers changed from: package-private */
    h(c cVar) {
    }
}
