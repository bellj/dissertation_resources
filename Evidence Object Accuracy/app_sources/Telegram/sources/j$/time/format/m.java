package j$.time.format;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class m implements g {
    private final String a;

    /* access modifiers changed from: package-private */
    public m(String str) {
        this.a = str;
    }

    public String toString() {
        String replace = this.a.replace("'", "''");
        return "'" + replace + "'";
    }
}
