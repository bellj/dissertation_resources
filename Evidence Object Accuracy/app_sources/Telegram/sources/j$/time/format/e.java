package j$.time.format;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class e implements g {
    private final char a;

    /* access modifiers changed from: package-private */
    public e(char c) {
        this.a = c;
    }

    public String toString() {
        if (this.a == '\'') {
            return "''";
        }
        return "'" + this.a + "'";
    }
}
