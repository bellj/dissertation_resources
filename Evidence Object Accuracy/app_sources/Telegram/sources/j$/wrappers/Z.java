package j$.wrappers;

import j$.util.function.n;
import java.util.function.IntToLongFunction;

/* loaded from: classes2.dex */
public final /* synthetic */ class Z implements n {
    final /* synthetic */ IntToLongFunction a;

    private /* synthetic */ Z(IntToLongFunction intToLongFunction) {
        this.a = intToLongFunction;
    }

    public static /* synthetic */ n a(IntToLongFunction intToLongFunction) {
        if (intToLongFunction == null) {
            return null;
        }
        return intToLongFunction instanceof C0164a0 ? ((C0164a0) intToLongFunction).a : new Z(intToLongFunction);
    }

    @Override // j$.util.function.n
    public /* synthetic */ long applyAsLong(int i) {
        return this.a.applyAsLong(i);
    }
}
