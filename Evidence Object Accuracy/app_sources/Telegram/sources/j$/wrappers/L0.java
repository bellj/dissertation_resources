package j$.wrappers;

import j$.util.AbstractC0002a;
import j$.util.C0009g;
import j$.util.C0012j;
import j$.util.function.BiConsumer;
import j$.util.function.d;
import j$.util.function.f;
import j$.util.function.g;
import j$.util.function.h;
import j$.util.function.u;
import j$.util.function.y;
import j$.util.stream.AbstractC0043e1;
import j$.util.stream.AbstractC0053g;
import j$.util.stream.IntStream;
import j$.util.stream.Stream;
import j$.util.stream.U;
import java.util.stream.DoubleStream;

/* loaded from: classes2.dex */
public final /* synthetic */ class L0 implements U {
    final /* synthetic */ DoubleStream a;

    private /* synthetic */ L0(DoubleStream doubleStream) {
        this.a = doubleStream;
    }

    public static /* synthetic */ U n0(DoubleStream doubleStream) {
        if (doubleStream == null) {
            return null;
        }
        return doubleStream instanceof M0 ? ((M0) doubleStream).a : new L0(doubleStream);
    }

    @Override // j$.util.stream.U
    public /* synthetic */ C0012j G(d dVar) {
        return AbstractC0002a.q(this.a.reduce(C0203z.a(dVar)));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ Object H(y yVar, u uVar, BiConsumer biConsumer) {
        return this.a.collect(A0.a(yVar), s0.a(uVar), r.a(biConsumer));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ double K(double d, d dVar) {
        return this.a.reduce(d, C0203z.a(dVar));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ Stream M(g gVar) {
        return C$r8$wrapper$java$util$stream$Stream$VWRP.convert(this.a.mapToObj(D.a(gVar)));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ IntStream R(G g) {
        return C$r8$wrapper$java$util$stream$IntStream$VWRP.convert(this.a.mapToInt(g == null ? null : g.a));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ boolean Y(E e) {
        return this.a.allMatch(F.a(e));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ C0012j average() {
        return AbstractC0002a.q(this.a.average());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U b(f fVar) {
        return n0(this.a.peek(B.a(fVar)));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ Stream boxed() {
        return C$r8$wrapper$java$util$stream$Stream$VWRP.convert(this.a.boxed());
    }

    @Override // j$.util.stream.AbstractC0053g, java.lang.AutoCloseable
    public /* synthetic */ void close() {
        this.a.close();
    }

    @Override // j$.util.stream.U
    public /* synthetic */ long count() {
        return this.a.count();
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U distinct() {
        return n0(this.a.distinct());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ C0012j findAny() {
        return AbstractC0002a.q(this.a.findAny());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ C0012j findFirst() {
        return AbstractC0002a.q(this.a.findFirst());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ boolean h0(E e) {
        return this.a.anyMatch(F.a(e));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ boolean i0(E e) {
        return this.a.noneMatch(F.a(e));
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ boolean isParallel() {
        return this.a.isParallel();
    }

    @Override // j$.util.stream.U
    public /* synthetic */ void j(f fVar) {
        this.a.forEach(B.a(fVar));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ void l0(f fVar) {
        this.a.forEachOrdered(B.a(fVar));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U limit(long j) {
        return n0(this.a.limit(j));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ C0012j max() {
        return AbstractC0002a.q(this.a.max());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ C0012j min() {
        return AbstractC0002a.q(this.a.min());
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ AbstractC0053g onClose(Runnable runnable) {
        return H0.n0(this.a.onClose(runnable));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U r(E e) {
        return n0(this.a.filter(F.a(e)));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U skip(long j) {
        return n0(this.a.skip(j));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U sorted() {
        return n0(this.a.sorted());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ double sum() {
        return this.a.sum();
    }

    @Override // j$.util.stream.U
    public C0009g summaryStatistics() {
        this.a.summaryStatistics();
        throw new Error("Java 8+ API desugaring (library desugaring) cannot convert from java.util.DoubleSummaryStatistics");
    }

    @Override // j$.util.stream.U
    public /* synthetic */ double[] toArray() {
        return this.a.toArray();
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ AbstractC0053g unordered() {
        return H0.n0(this.a.unordered());
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U w(g gVar) {
        return n0(this.a.flatMap(D.a(gVar)));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ AbstractC0043e1 x(h hVar) {
        return N0.n0(this.a.mapToLong(J.a(hVar)));
    }

    @Override // j$.util.stream.U
    public /* synthetic */ U y(K k) {
        return n0(this.a.map(L.a(k)));
    }
}
