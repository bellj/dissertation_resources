package j$.wrappers;

import j$.util.function.q;
import java.util.function.LongConsumer;

/* renamed from: j$.wrappers.g0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0176g0 implements LongConsumer {
    final /* synthetic */ q a;

    private /* synthetic */ C0176g0(q qVar) {
        this.a = qVar;
    }

    public static /* synthetic */ LongConsumer a(q qVar) {
        if (qVar == null) {
            return null;
        }
        return qVar instanceof C0174f0 ? ((C0174f0) qVar).a : new C0176g0(qVar);
    }

    @Override // java.util.function.LongConsumer
    public /* synthetic */ void accept(long j) {
        this.a.accept(j);
    }

    @Override // java.util.function.LongConsumer
    public /* synthetic */ LongConsumer andThen(LongConsumer longConsumer) {
        return a(this.a.f(C0174f0.b(longConsumer)));
    }
}
