package j$.wrappers;

import j$.util.AbstractC0002a;
import j$.util.C0011i;
import j$.util.C0012j;
import j$.util.C0014l;
import j$.util.function.BiConsumer;
import j$.util.function.o;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.t;
import j$.util.function.w;
import j$.util.function.y;
import j$.util.stream.AbstractC0043e1;
import j$.util.stream.AbstractC0053g;
import j$.util.stream.IntStream;
import j$.util.stream.Stream;
import j$.util.stream.U;
import java.util.stream.LongStream;

/* loaded from: classes2.dex */
public final /* synthetic */ class N0 implements AbstractC0043e1 {
    final /* synthetic */ LongStream a;

    private /* synthetic */ N0(LongStream longStream) {
        this.a = longStream;
    }

    public static /* synthetic */ AbstractC0043e1 n0(LongStream longStream) {
        if (longStream == null) {
            return null;
        }
        return longStream instanceof O0 ? ((O0) longStream).a : new N0(longStream);
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ long D(long j, o oVar) {
        return this.a.reduce(j, C0172e0.a(oVar));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ boolean L(C0182j0 j0Var) {
        return this.a.allMatch(AbstractC0184k0.a(j0Var));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ U O(C0186l0 l0Var) {
        return L0.n0(this.a.mapToDouble(l0Var == null ? null : l0Var.a));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ Stream Q(r rVar) {
        return C$r8$wrapper$java$util$stream$Stream$VWRP.convert(this.a.mapToObj(C0180i0.a(rVar)));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ boolean S(C0182j0 j0Var) {
        return this.a.noneMatch(AbstractC0184k0.a(j0Var));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ void Z(q qVar) {
        this.a.forEachOrdered(C0176g0.a(qVar));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ U asDoubleStream() {
        return L0.n0(this.a.asDoubleStream());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ C0012j average() {
        return AbstractC0002a.q(this.a.average());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ Stream boxed() {
        return C$r8$wrapper$java$util$stream$Stream$VWRP.convert(this.a.boxed());
    }

    @Override // j$.util.stream.AbstractC0053g, java.lang.AutoCloseable
    public /* synthetic */ void close() {
        this.a.close();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ long count() {
        return this.a.count();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ void d(q qVar) {
        this.a.forEach(C0176g0.a(qVar));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 distinct() {
        return n0(this.a.distinct());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ IntStream e0(C0190n0 n0Var) {
        return C$r8$wrapper$java$util$stream$IntStream$VWRP.convert(this.a.mapToInt(n0Var == null ? null : n0Var.a));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ Object f0(y yVar, w wVar, BiConsumer biConsumer) {
        return this.a.collect(A0.a(yVar), w0.a(wVar), r.a(biConsumer));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ C0014l findAny() {
        return AbstractC0002a.s(this.a.findAny());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ C0014l findFirst() {
        return AbstractC0002a.s(this.a.findFirst());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ C0014l g(o oVar) {
        return AbstractC0002a.s(this.a.reduce(C0172e0.a(oVar)));
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ boolean isParallel() {
        return this.a.isParallel();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ boolean k(C0182j0 j0Var) {
        return this.a.anyMatch(AbstractC0184k0.a(j0Var));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 limit(long j) {
        return n0(this.a.limit(j));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ C0014l max() {
        return AbstractC0002a.s(this.a.max());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ C0014l min() {
        return AbstractC0002a.s(this.a.min());
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ AbstractC0053g onClose(Runnable runnable) {
        return H0.n0(this.a.onClose(runnable));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 p(q qVar) {
        return n0(this.a.peek(C0176g0.a(qVar)));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 s(r rVar) {
        return n0(this.a.flatMap(C0180i0.a(rVar)));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 skip(long j) {
        return n0(this.a.skip(j));
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 sorted() {
        return n0(this.a.sorted());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ long sum() {
        return this.a.sum();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public C0011i summaryStatistics() {
        this.a.summaryStatistics();
        throw new Error("Java 8+ API desugaring (library desugaring) cannot convert from java.util.LongSummaryStatistics");
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ long[] toArray() {
        return this.a.toArray();
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 u(C0182j0 j0Var) {
        return n0(this.a.filter(AbstractC0184k0.a(j0Var)));
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ AbstractC0053g unordered() {
        return H0.n0(this.a.unordered());
    }

    @Override // j$.util.stream.AbstractC0043e1
    public /* synthetic */ AbstractC0043e1 z(t tVar) {
        return n0(this.a.map(q0.a(tVar)));
    }
}
