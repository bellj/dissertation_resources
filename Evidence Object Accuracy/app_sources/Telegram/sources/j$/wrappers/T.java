package j$.wrappers;

import j$.util.function.m;
import java.util.function.IntFunction;

/* loaded from: classes2.dex */
public final /* synthetic */ class T implements m {
    final /* synthetic */ IntFunction a;

    private /* synthetic */ T(IntFunction intFunction) {
        this.a = intFunction;
    }

    public static /* synthetic */ m a(IntFunction intFunction) {
        if (intFunction == null) {
            return null;
        }
        return intFunction instanceof U ? ((U) intFunction).a : new T(intFunction);
    }

    @Override // j$.util.function.m
    public /* synthetic */ Object apply(int i) {
        return this.a.apply(i);
    }
}
