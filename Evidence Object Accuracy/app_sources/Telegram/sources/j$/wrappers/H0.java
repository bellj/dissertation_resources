package j$.wrappers;

import j$.util.stream.AbstractC0053g;
import j$.util.u;
import java.util.Iterator;
import java.util.stream.BaseStream;

/* loaded from: classes2.dex */
public final /* synthetic */ class H0 implements AbstractC0053g {
    final /* synthetic */ BaseStream a;

    private /* synthetic */ H0(BaseStream baseStream) {
        this.a = baseStream;
    }

    public static /* synthetic */ AbstractC0053g n0(BaseStream baseStream) {
        if (baseStream == null) {
            return null;
        }
        return baseStream instanceof I0 ? ((I0) baseStream).a : new H0(baseStream);
    }

    @Override // j$.util.stream.AbstractC0053g, java.lang.AutoCloseable
    public /* synthetic */ void close() {
        this.a.close();
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ boolean isParallel() {
        return this.a.isParallel();
    }

    @Override // j$.util.stream.AbstractC0053g
    /* renamed from: iterator */
    public /* synthetic */ Iterator mo66iterator() {
        return this.a.iterator();
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ AbstractC0053g onClose(Runnable runnable) {
        return n0(this.a.onClose(runnable));
    }

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* synthetic */ AbstractC0053g parallel() {
        return n0(this.a.parallel());
    }

    @Override // j$.util.stream.AbstractC0053g, j$.util.stream.IntStream
    public /* synthetic */ AbstractC0053g sequential() {
        return n0(this.a.sequential());
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ u spliterator() {
        return C0175g.a(this.a.spliterator());
    }

    @Override // j$.util.stream.AbstractC0053g
    public /* synthetic */ AbstractC0053g unordered() {
        return n0(this.a.unordered());
    }
}
