package j$.wrappers;

import j$.util.function.o;
import java.util.function.LongBinaryOperator;

/* renamed from: j$.wrappers.d0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0170d0 implements o {
    final /* synthetic */ LongBinaryOperator a;

    private /* synthetic */ C0170d0(LongBinaryOperator longBinaryOperator) {
        this.a = longBinaryOperator;
    }

    public static /* synthetic */ o a(LongBinaryOperator longBinaryOperator) {
        if (longBinaryOperator == null) {
            return null;
        }
        return longBinaryOperator instanceof C0172e0 ? ((C0172e0) longBinaryOperator).a : new C0170d0(longBinaryOperator);
    }

    @Override // j$.util.function.o
    public /* synthetic */ long applyAsLong(long j, long j2) {
        return this.a.applyAsLong(j, j2);
    }
}
