package j$.wrappers;

import j$.util.function.d;
import java.util.function.DoubleBinaryOperator;

/* renamed from: j$.wrappers.z */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0203z implements DoubleBinaryOperator {
    final /* synthetic */ d a;

    private /* synthetic */ C0203z(d dVar) {
        this.a = dVar;
    }

    public static /* synthetic */ DoubleBinaryOperator a(d dVar) {
        if (dVar == null) {
            return null;
        }
        return dVar instanceof C0202y ? ((C0202y) dVar).a : new C0203z(dVar);
    }

    @Override // java.util.function.DoubleBinaryOperator
    public /* synthetic */ double applyAsDouble(double d, double d2) {
        return this.a.applyAsDouble(d, d2);
    }
}
