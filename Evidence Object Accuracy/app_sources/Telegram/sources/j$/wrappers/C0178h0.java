package j$.wrappers;

import j$.util.function.r;
import java.util.function.LongFunction;

/* renamed from: j$.wrappers.h0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0178h0 implements r {
    final /* synthetic */ LongFunction a;

    private /* synthetic */ C0178h0(LongFunction longFunction) {
        this.a = longFunction;
    }

    public static /* synthetic */ r a(LongFunction longFunction) {
        if (longFunction == null) {
            return null;
        }
        return longFunction instanceof C0180i0 ? ((C0180i0) longFunction).a : new C0178h0(longFunction);
    }

    @Override // j$.util.function.r
    public /* synthetic */ Object apply(long j) {
        return this.a.apply(j);
    }
}
