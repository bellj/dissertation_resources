package j$.wrappers;

import j$.util.function.b;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;

/* renamed from: j$.wrappers.v */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0199v implements BinaryOperator {
    final /* synthetic */ b a;

    private /* synthetic */ C0199v(b bVar) {
        this.a = bVar;
    }

    public static /* synthetic */ BinaryOperator a(b bVar) {
        if (bVar == null) {
            return null;
        }
        return bVar instanceof C0198u ? ((C0198u) bVar).a : new C0199v(bVar);
    }

    @Override // java.util.function.BiFunction
    public /* synthetic */ BiFunction andThen(Function function) {
        return C0197t.a(this.a.andThen(M.a(function)));
    }

    @Override // java.util.function.BiFunction
    public /* synthetic */ Object apply(Object obj, Object obj2) {
        return this.a.apply(obj, obj2);
    }
}
