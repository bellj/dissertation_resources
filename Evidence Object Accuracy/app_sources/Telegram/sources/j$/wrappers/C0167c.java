package j$.wrappers;

import j$.util.function.Consumer;
import j$.util.function.l;
import j$.util.p;
import java.util.PrimitiveIterator;

/* renamed from: j$.wrappers.c  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0167c implements p.a {
    final /* synthetic */ PrimitiveIterator.OfInt a;

    private /* synthetic */ C0167c(PrimitiveIterator.OfInt ofInt) {
        this.a = ofInt;
    }

    public static /* synthetic */ p.a a(PrimitiveIterator.OfInt ofInt) {
        if (ofInt == null) {
            return null;
        }
        return ofInt instanceof C0169d ? ((C0169d) ofInt).a : new C0167c(ofInt);
    }

    @Override // j$.util.p.a
    public /* synthetic */ void c(l lVar) {
        this.a.forEachRemaining(S.a(lVar));
    }

    @Override // j$.util.p.a, j$.util.Iterator
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        this.a.forEachRemaining(C0201x.a(consumer));
    }

    @Override // j$.util.p
    public /* synthetic */ void forEachRemaining(Object obj) {
        this.a.forEachRemaining((PrimitiveIterator.OfInt) obj);
    }

    @Override // java.util.Iterator
    public /* synthetic */ boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // j$.util.p.a
    public /* synthetic */ int nextInt() {
        return this.a.nextInt();
    }

    @Override // java.util.Iterator
    public /* synthetic */ void remove() {
        this.a.remove();
    }
}
