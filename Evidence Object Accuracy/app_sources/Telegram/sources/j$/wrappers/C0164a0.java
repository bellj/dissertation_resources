package j$.wrappers;

import j$.util.function.n;
import java.util.function.IntToLongFunction;

/* renamed from: j$.wrappers.a0  reason: case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0164a0 implements IntToLongFunction {
    final /* synthetic */ n a;

    private /* synthetic */ C0164a0(n nVar) {
        this.a = nVar;
    }

    public static /* synthetic */ IntToLongFunction a(n nVar) {
        if (nVar == null) {
            return null;
        }
        return nVar instanceof Z ? ((Z) nVar).a : new C0164a0(nVar);
    }

    @Override // java.util.function.IntToLongFunction
    public /* synthetic */ long applyAsLong(int i) {
        return this.a.applyAsLong(i);
    }
}
