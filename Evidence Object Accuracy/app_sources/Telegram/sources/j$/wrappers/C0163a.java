package j$.wrappers;

import j$.util.AbstractC0016n;
import j$.util.function.Consumer;
import j$.util.function.f;
import java.util.PrimitiveIterator;

/* renamed from: j$.wrappers.a */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0163a implements AbstractC0016n {
    final /* synthetic */ PrimitiveIterator.OfDouble a;

    private /* synthetic */ C0163a(PrimitiveIterator.OfDouble ofDouble) {
        this.a = ofDouble;
    }

    public static /* synthetic */ AbstractC0016n a(PrimitiveIterator.OfDouble ofDouble) {
        if (ofDouble == null) {
            return null;
        }
        return ofDouble instanceof C0165b ? ((C0165b) ofDouble).a : new C0163a(ofDouble);
    }

    @Override // j$.util.AbstractC0016n
    public /* synthetic */ void e(f fVar) {
        this.a.forEachRemaining(B.a(fVar));
    }

    @Override // j$.util.AbstractC0016n, j$.util.Iterator
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        this.a.forEachRemaining(C0201x.a(consumer));
    }

    @Override // j$.util.p
    public /* synthetic */ void forEachRemaining(Object obj) {
        this.a.forEachRemaining((PrimitiveIterator.OfDouble) obj);
    }

    @Override // java.util.Iterator
    public /* synthetic */ boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // j$.util.AbstractC0016n
    public /* synthetic */ double nextDouble() {
        return this.a.nextDouble();
    }

    @Override // java.util.Iterator
    public /* synthetic */ void remove() {
        this.a.remove();
    }
}
