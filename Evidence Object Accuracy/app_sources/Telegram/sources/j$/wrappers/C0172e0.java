package j$.wrappers;

import j$.util.function.o;
import java.util.function.LongBinaryOperator;

/* renamed from: j$.wrappers.e0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0172e0 implements LongBinaryOperator {
    final /* synthetic */ o a;

    private /* synthetic */ C0172e0(o oVar) {
        this.a = oVar;
    }

    public static /* synthetic */ LongBinaryOperator a(o oVar) {
        if (oVar == null) {
            return null;
        }
        return oVar instanceof C0170d0 ? ((C0170d0) oVar).a : new C0172e0(oVar);
    }

    @Override // java.util.function.LongBinaryOperator
    public /* synthetic */ long applyAsLong(long j, long j2) {
        return this.a.applyAsLong(j, j2);
    }
}
