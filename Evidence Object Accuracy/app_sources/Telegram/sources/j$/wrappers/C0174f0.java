package j$.wrappers;

import j$.util.function.q;
import java.util.function.LongConsumer;

/* renamed from: j$.wrappers.f0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0174f0 implements q {
    final /* synthetic */ LongConsumer a;

    private /* synthetic */ C0174f0(LongConsumer longConsumer) {
        this.a = longConsumer;
    }

    public static /* synthetic */ q b(LongConsumer longConsumer) {
        if (longConsumer == null) {
            return null;
        }
        return longConsumer instanceof C0176g0 ? ((C0176g0) longConsumer).a : new C0174f0(longConsumer);
    }

    @Override // j$.util.function.q
    public /* synthetic */ void accept(long j) {
        this.a.accept(j);
    }

    @Override // j$.util.function.q
    public /* synthetic */ q f(q qVar) {
        return b(this.a.andThen(C0176g0.a(qVar)));
    }
}
