package j$.wrappers;

import j$.util.function.t;
import java.util.function.LongUnaryOperator;

/* loaded from: classes2.dex */
public final /* synthetic */ class q0 implements LongUnaryOperator {
    final /* synthetic */ t a;

    private /* synthetic */ q0(t tVar) {
        this.a = tVar;
    }

    public static /* synthetic */ LongUnaryOperator a(t tVar) {
        if (tVar == null) {
            return null;
        }
        return tVar instanceof C0194p0 ? ((C0194p0) tVar).a : new q0(tVar);
    }

    @Override // java.util.function.LongUnaryOperator
    public /* synthetic */ LongUnaryOperator andThen(LongUnaryOperator longUnaryOperator) {
        return a(this.a.a(C0194p0.c(longUnaryOperator)));
    }

    @Override // java.util.function.LongUnaryOperator
    public /* synthetic */ long applyAsLong(long j) {
        return this.a.applyAsLong(j);
    }

    @Override // java.util.function.LongUnaryOperator
    public /* synthetic */ LongUnaryOperator compose(LongUnaryOperator longUnaryOperator) {
        return a(this.a.b(C0194p0.c(longUnaryOperator)));
    }
}
