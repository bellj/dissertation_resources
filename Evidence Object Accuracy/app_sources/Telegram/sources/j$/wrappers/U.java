package j$.wrappers;

import j$.util.function.m;
import java.util.function.IntFunction;

/* loaded from: classes2.dex */
public final /* synthetic */ class U implements IntFunction {
    final /* synthetic */ m a;

    private /* synthetic */ U(m mVar) {
        this.a = mVar;
    }

    public static /* synthetic */ IntFunction a(m mVar) {
        if (mVar == null) {
            return null;
        }
        return mVar instanceof T ? ((T) mVar).a : new U(mVar);
    }

    @Override // java.util.function.IntFunction
    public /* synthetic */ Object apply(int i) {
        return this.a.apply(i);
    }
}
