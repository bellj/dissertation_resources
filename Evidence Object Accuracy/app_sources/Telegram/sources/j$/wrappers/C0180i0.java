package j$.wrappers;

import j$.util.function.r;
import java.util.function.LongFunction;

/* renamed from: j$.wrappers.i0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0180i0 implements LongFunction {
    final /* synthetic */ r a;

    private /* synthetic */ C0180i0(r rVar) {
        this.a = rVar;
    }

    public static /* synthetic */ LongFunction a(r rVar) {
        if (rVar == null) {
            return null;
        }
        return rVar instanceof C0178h0 ? ((C0178h0) rVar).a : new C0180i0(rVar);
    }

    @Override // java.util.function.LongFunction
    public /* synthetic */ Object apply(long j) {
        return this.a.apply(j);
    }
}
