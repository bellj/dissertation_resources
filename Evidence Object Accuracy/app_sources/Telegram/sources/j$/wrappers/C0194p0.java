package j$.wrappers;

import j$.util.function.t;
import java.util.function.LongUnaryOperator;

/* renamed from: j$.wrappers.p0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0194p0 implements t {
    final /* synthetic */ LongUnaryOperator a;

    private /* synthetic */ C0194p0(LongUnaryOperator longUnaryOperator) {
        this.a = longUnaryOperator;
    }

    public static /* synthetic */ t c(LongUnaryOperator longUnaryOperator) {
        if (longUnaryOperator == null) {
            return null;
        }
        return longUnaryOperator instanceof q0 ? ((q0) longUnaryOperator).a : new C0194p0(longUnaryOperator);
    }

    @Override // j$.util.function.t
    public /* synthetic */ t a(t tVar) {
        return c(this.a.andThen(q0.a(tVar)));
    }

    @Override // j$.util.function.t
    public /* synthetic */ long applyAsLong(long j) {
        return this.a.applyAsLong(j);
    }

    @Override // j$.util.function.t
    public /* synthetic */ t b(t tVar) {
        return c(this.a.compose(q0.a(tVar)));
    }
}
