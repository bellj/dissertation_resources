package j$.wrappers;

import j$.util.function.d;
import java.util.function.DoubleBinaryOperator;

/* renamed from: j$.wrappers.y */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0202y implements d {
    final /* synthetic */ DoubleBinaryOperator a;

    private /* synthetic */ C0202y(DoubleBinaryOperator doubleBinaryOperator) {
        this.a = doubleBinaryOperator;
    }

    public static /* synthetic */ d a(DoubleBinaryOperator doubleBinaryOperator) {
        if (doubleBinaryOperator == null) {
            return null;
        }
        return doubleBinaryOperator instanceof C0203z ? ((C0203z) doubleBinaryOperator).a : new C0202y(doubleBinaryOperator);
    }

    @Override // j$.util.function.d
    public /* synthetic */ double applyAsDouble(double d, double d2) {
        return this.a.applyAsDouble(d, d2);
    }
}
