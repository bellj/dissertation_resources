package j$.wrappers;

import j$.util.function.BiFunction;
import j$.util.function.Function;
import j$.util.function.b;
import java.util.function.BinaryOperator;

/* renamed from: j$.wrappers.u */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0198u implements b {
    final /* synthetic */ BinaryOperator a;

    private /* synthetic */ C0198u(BinaryOperator binaryOperator) {
        this.a = binaryOperator;
    }

    public static /* synthetic */ b a(BinaryOperator binaryOperator) {
        if (binaryOperator == null) {
            return null;
        }
        return binaryOperator instanceof C0199v ? ((C0199v) binaryOperator).a : new C0198u(binaryOperator);
    }

    @Override // j$.util.function.BiFunction
    public /* synthetic */ BiFunction andThen(Function function) {
        return C0196s.a(this.a.andThen(N.a(function)));
    }

    @Override // j$.util.function.BiFunction
    public /* synthetic */ Object apply(Object obj, Object obj2) {
        return this.a.apply(obj, obj2);
    }
}
