#!/bin/bash
# Download of apks was manual - 6months apart 5 years
# to run: ./exp4_pt3.sh 

# RESULTS_FOLDER="/mnt/2TBdrive/test/exp4/PT3"
RESULTS_FOLDER="./results"
APK_DOWNLOAD_FOLDER="$RESULTS_FOLDER/apks_versions_downloaded"
APK_DOWNLOAD_DECOMPILED_FOLDER="$APK_DOWNLOAD_FOLDER/decompiled"

API_CNT_FILE=$RESULTS_FOLDER/api_count
API_FOUND_FILE=$RESULTS_FOLDER/api_found

mkdir -p $APK_DOWNLOAD_DECOMPILED_FOLDER

echo "** Starting run **"
now="$(date +'%d/%m/%Y %T')"
echo "** Current date in format $now **"

now="$(date +'%d/%m/%Y %T')"

source=""

declare -A app_date

app_date['com.whatsapp_2.22.4.75.apk']='17-02-2022'
app_date['com.whatsapp_2.21.17.1.apk']='9-08-2021'
app_date['com.whatsapp_2.21.3.13.apk']='6-02-2021'
app_date['com.whatsapp_2.20.196.16.apk']='5-08-2020'
app_date['com.whatsapp_2.20.22.apk']='13-02-2020'
app_date['com.whatsapp_2.19.216.apk']='7-08-2019'
app_date['com.whatsapp_2.19.34.apk']='8-02-2019'
app_date['com.whatsapp_2.18.248.apk']='18-08-2018'
app_date['com.whatsapp_2.18.46.apk']='9-02-2018'
app_date['com.whatsapp_2.17.296.apk']='11-08-2017'
app_date['org.telegram.messenger_8.5.2.apk']='14-02-2022'
app_date['org.telegram.messenger_7.9.3.apk']='7-08-2021'
app_date['org.telegram.messenger_7.4.2.apk']='18-02-2021'
app_date['org.telegram.messenger_7.0.0.apk']='16-08-2020'
app_date['org.telegram.messenger_5.15.0.apk']='16-02-2020'
app_date['org.telegram.messenger_5.10.0.apk']='24-08-2019'
app_date['org.telegram.messenger_5.3.1.apk']='9-02-2019'
app_date['org.telegram.messenger_4.9.1.apk']='30-08-2018'
app_date['org.telegram.messenger_4.8.4.apk']='19-02-2018'
app_date['org.telegram.messenger_4.2.2.apk']='5-08-2017'
app_date['org.thoughtcrime.securesms_5.32.7.apk']='18-02-2022'
app_date['org.thoughtcrime.securesms_5.21.5.apk']='20-08-2021'
app_date['org.thoughtcrime.securesms_5.4.6.apk']='18-02-2021'
app_date['org.thoughtcrime.securesms_4.69.4.apk']='20-08-2020'
app_date['org.thoughtcrime.securesms_4.55.8.apk']='12-02-2020'
app_date['org.thoughtcrime.securesms_4.45.2.apk']='9-08-2019'
app_date['org.thoughtcrime.securesms_4.33.5.apk']='9-02-2019'
app_date['org.thoughtcrime.securesms_4.24.8.apk']='6-08-2018'
app_date['org.thoughtcrime.securesms_4.16.9.apk']='28-02-2018'
app_date['org.thoughtcrime.securesms_4.9.9.apk']='23-08-2017'

search(){
    for f in $APK_DOWNLOAD_DECOMPILED_FOLDER/*.apk; do
        if grep -riq sqlite $f/lib/ ; then 
            ans_lib="FOUND"
        else
            ans_lib="NOT_FOUND" 
        fi
       
        if [[ -z `find $f -name arch -print -quit` ]]; then 
            ans_arch="NOT_FOUND"
        else
            ans_arch="FOUND"
        fi

        if [[ "`basename $f`" == *"telegram"* ]]; then
            app_name="Telegram"
        elif [[ "`basename $f`" == *"thoughtcrime"* ]]; then
            app_name="Signal"
        elif [[ "`basename $f`" = *"whatsapp"* ]]; then
            app_name="WhatsApp"
        fi

        subfolder_name_with_extension=${f##*/}
        # echo $subfolder_name_with_extension
        subfolder_name=${subfolder_name_with_extension%.*}
        subfolder_to_search=`echo  "$subfolder_name" |cut -d '_' -f1  | tr . /`
        # echo $subfolder_to_search
        
        if grep -PRiq --include *.smali "Landroid/database/sqlite.*;->" $f/smali*/$subfolder_to_search ; then
            ans_andsqlite_pkg="FOUND"
        else
            ans_andsqlite_pkg="NOT_FOUND" 
        fi

        echo ${app_date[`basename $f`]},$app_name,"`basename $f`,${ans_lib},${ans_arch},${ans_andsqlite_pkg}"

        
        # grep -Fri sqlite $f/lib/ 
        # grep -Fri sqlite $f/lib/ | cut -d '/' -f1 | uniq
        # echo "`basename $f`|${app_date[`basename $f`]}"
    done
}


rename_apks(){
    for f in $APK_DOWNLOAD_FOLDER/*/*.apk; do mv "$f" "$(echo "$f" | sed s/WhatsApp_/com.whatsapp_/)";  done
    for f in $APK_DOWNLOAD_FOLDER/*/*.apk; do mv "$f" "$(echo "$f" | sed s/Telegram_/org.telegram.messenger_/)"; done
    for f in $APK_DOWNLOAD_FOLDER/*/*.apk; do mv "$f" "$(echo "$f" | sed s/org.telegram.messenger.web_/org.telegram.messenger_/)"; done
    for f in $APK_DOWNLOAD_FOLDER/*/*.apk; do mv "$f" "$(echo "$f" | sed s/Signal_/org.thoughtcrime.securesms_/)"; done
    for f in $APK_DOWNLOAD_FOLDER/*/*.apk; do mv "$f" "$(echo "$f" | sed s/_apkcombo.com//)"; done
}

decompile_apks_downloaded(){
    apk_cnt=`ls -l  $APK_DOWNLOAD_FOLDER/*/*.apk |wc -l`
    r=0

    for f in $APK_DOWNLOAD_FOLDER/*/*.xapk
    do
        apk_name_no_extention="${f%.*}"
        if [ ! -f $apk_name_no_extention.apk ]; then
            echo "Unzipping $f"
            mv $f $apk_name_no_extention.zip
            base_name=`basename $apk_name_no_extention`
            unzip -p $apk_name_no_extention.zip $base_name.apk > $apk_name_no_extention.apk
            mv $apk_name_no_extention.zip $apk_name_no_extention.xapk 
        fi
    done

    for f in $APK_DOWNLOAD_FOLDER/*/*.apk
    do
        let "r=r+1"
        echo "[**] >> APK $r of $apk_cnt "
        now="$(date +'%d/%m/%Y %T')"

        if [ ! -d $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/} ]; then
            echo "[**] Starting Decompilation of ${f##*/} at: $now"
            ./apktool/apktool.sh 2.5 d $f -o $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
            if [ "$?" -ne 0 ]; then
                # apktool command failed;
                rm -rf $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                ./apktool/apktool.sh 2.6.1 d $f -o $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
                echo $APK_DOWNLOAD_DECOMPILED_FOLDER/${f##*/}
            fi
            now="$(date +'%d/%m/%Y %T')"
            echo "[**] Finished Decompilation of ${f##*/} at: $now"
        else
            echo "[**] APK already decompiled"
        fi
    done
}

smali_lib_search(){
    apk_cnt=`ls $APK_DOWNLOAD_DECOMPILED_FOLDER |wc -l`
    r=0

    e2ee_api=("sqlcipher.*" "iocipher.*")

    database_api=("sqlite.*" "firebase.*" "greendao.*" "realm.*" "ormlite.*")

    network_api=("retrofit.*" "volley.*" "grpc.*" "squareup.*" "okhttp3.*")

    #START MAIN_PKG SEARCH
    current_api_cnt_file="$API_CNT_FILE"_lib_search.csv
    current_api_found_file="$API_FOUND_FILE"_lib_search.csv
    search_typ="lib_search"
    if [ ! -f $current_api_cnt_file ]; then
        touch $current_api_cnt_file
        touch $current_api_found_file
        echo "app|source|api_category|api|search_typ|count" >> $current_api_cnt_file
    fi

    for f in $APK_DOWNLOAD_DECOMPILED_FOLDER/*
    do

        if  ! grep -Fq "${f##*/}" "${APKPURE_LIST}"; then
            source="apkpure"
        else
            source="playstore"
        fi

        let "r=r+1"
        echo "[**] >> APK Search $r of $apk_cnt "
        now="$(date +'%d/%m/%Y %T')"
        echo "[**] Starting Search of ${f##*/} at: $now"

        for api in ${database_api[@]}; do
            TYPE="DATABASE_APIS"
            if  ! grep -Fq "${f##*/}|$source|$TYPE|$api|$search_typ" "${current_api_cnt_file}"; then
                echo "[**] Starting Search of ${api} ${f##*/} at: $now"
                echo "****** APP *******:"${f##*/} >> $current_api_found_file
                echo "****** TRIGGER *******:"${api} >> $current_api_found_file

                api_found=`grep -PRi $api $f/lib/`
                echo "${api_found}" >> $current_api_found_file    
                
                api_cnt=`echo "${api_found}" | sed -r '/^\s*$/d' | wc -l`

                echo ">>>>>> [" ${f##*/} "] ->>>>> "$api_cnt 
                echo "${f##*/}|$source|$TYPE|$api|$search_typ|$api_cnt" >> $current_api_cnt_file

                now="$(date +'%d/%m/%Y %T')"
                echo "[**] Finished Search of ${api} ${f##*/} at: $now"
            else
                echo "Already performed this search: ${f##*/}|$source|$TYPE|$api|$search_typ"
            fi
        done    
        
        for api in ${network_api[@]}; do
            TYPE="NETWORK_APIS"
            if  ! grep -Fq "${f##*/}|$source|$TYPE|$api|$search_typ" "${current_api_cnt_file}"; then
                echo "[**] Starting Search of ${api} ${f##*/} at: $now"
                echo "****** APP *******:"${f##*/} >> $current_api_found_file
                echo "****** TRIGGER *******:"${api} >> $current_api_found_file

                api_found=`grep -PRi $api $f/lib/`
                echo "${api_found}" >> $current_api_found_file    
                
                api_cnt=`echo "${api_found}" | sed -r '/^\s*$/d' | wc -l`

                echo ">>>>>> [" ${f##*/} "] ->>>>> "$api_cnt 
                echo "${f##*/}|$source|$TYPE|$api|$search_typ|$api_cnt" >> $current_api_cnt_file

                now="$(date +'%d/%m/%Y %T')"
                echo "[**] Finished Search of ${api} ${f##*/} at: $now"
            else
                echo "Already performed this search: ${f##*/}|$source|$TYPE|$api|$search_typ"
            fi
        done    
        
        for api in ${e2ee_api[@]}; do
            TYPE="E2EE_APIS"
            if  ! grep -Fq "${f##*/}|$source|$TYPE|$api|$search_typ" "${current_api_cnt_file}"; then
                echo "[**] Starting Search of ${api} ${f##*/} at: $now"
                echo "****** APP *******:"${f##*/} >> $current_api_found_file
                echo "****** TRIGGER *******:"${api} >> $current_api_found_file

                api_found=`grep -PRi $api $f/lib/`
                echo "${api_found}" >> $current_api_found_file    
                
                api_cnt=`echo "${api_found}" | sed -r '/^\s*$/d' | wc -l`

                echo ">>>>>> [" ${f##*/} "] ->>>>> "$api_cnt 
                echo "${f##*/}|$source|$TYPE|$api|$search_typ|$api_cnt" >> $current_api_cnt_file

                now="$(date +'%d/%m/%Y %T')"
                echo "[**] Finished Search of ${api} ${f##*/} at: $now"
            else
                echo "Already performed this search: ${f##*/}|$source|$TYPE|$api|$search_typ"
            fi
        done    
    done
    #END LIB SEARCH
    now="$(date +'%d/%m/%Y %T')"
    echo "[**] Finished Search of ${f##*/} at: $now"
}

# rename_apks
# decompile_apks_downloaded
search
# smali_appbrain_search

# echo "Synching"
# ./rsync.sh &
# echo "Finished Synching"