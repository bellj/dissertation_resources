import os
directory = '/mnt/2TBdrive/gitlab/experiment_3/results/results_GLOBAL_LOOP_5_MSG_LOOP_10_IN_MSG_W8_5-2021-08-29T14:34:53'
result_filename = '/mnt/2TBdrive/gitlab/experiment_3/results/results_GLOBAL_LOOP_5_MSG_LOOP_10_IN_MSG_W8_5-2021-08-29T14:34:53/exp2_csv_output.txt'


result_file = open(result_filename,'a+')
result_file.write("APP,SAMPLING CATEGORY,CUSTOM ORDER,SAMPLING STRATEGY,RUN,JANKY,CRASHES,ANR FILE SIZE (bytes), ANR FILE SIZE (MB),SIZE OF JITMF LOGS ON DISk (bytes),SIZE OF JITMF LOGS ON DISk (MB)\n")

list_of_files = []
custom_file_order = {}
insert = True

for filename in os.listdir(directory):
    if filename != 'exp2_csv_output.txt' and filename.endswith(".txt"):
        insert = True
        app,_,samplingStrategy = os.path.splitext(filename)[0].split("_",maxsplit=2)
        samplingStrategy = samplingStrategy.replace("samping","sampling") #grammatical mistake
        run=samplingStrategy.split("_")[-1]
        samplingStrategy = '_'.join(samplingStrategy.split("_")[:-1])

        if samplingStrategy == "nosampling":
            insert = False

        if insert:
            list_of_files.append(samplingStrategy)        

list_of_files.sort(key=lambda f: int(''.join(filter(str.isdigit, f))))
counter = 0

for f in list_of_files:
    if f not in custom_file_order:
        counter+=1
        custom_file_order[f]=counter

for filename in os.listdir(directory):
    if filename != 'exp2_csv_output.txt' and filename.endswith(".txt"):
        app,_,samplingStrategy = os.path.splitext(filename)[0].split("_",maxsplit=2)
        samplingStrategy = samplingStrategy.replace("samping","sampling") #grammatical mistake
        run=samplingStrategy.split("_")[-1]
        samplingStrategy = '_'.join(samplingStrategy.split("_")[:-1])

        if samplingStrategy == "nosampling":
            category = "N/A"
        elif "calls" in samplingStrategy:
            category = "TP Hit Frequency"
        else:
            category = "Timeseries"
      
        try:
            counter_to_insert = custom_file_order[samplingStrategy]
        except:
            counter_to_insert = 0
        string_to_append = app+","+category+","+str(counter_to_insert)+","+samplingStrategy+","+run+","

        filepath = os.path.join(directory,filename)
        with open(filepath) as f:
            crashFlag = False
            jankyFlag = False

            for line in f:
                contents = line.split()
                if 'Janky' in contents[0]:
                    string_to_append += contents[3].replace('(','').replace(')','')
                    jankyFlag = True
                if 'Fatal signal' in line and not crashFlag:
                    string_to_append += ",1"
                    crashFlag = True

            if not crashFlag:
                string_to_append += ","

            jitmflogs_folder_path = (os.path.join(filepath.split('.')[0],'jitmflogs')) 
            anr_folder_path = (os.path.join(filepath.split('.')[0],'anr')) 
        
            jitmflog_size = 0
            anr_size = 0

            for anr in os.scandir(anr_folder_path):
                anr_size+=os.path.getsize(anr)
            string_to_append += ","+str(anr_size)

            string_to_append += ","+str((anr_size/1024)/1024)

            # get size
            for jitmflog in os.scandir(jitmflogs_folder_path):
                jitmflog_size+=os.path.getsize(jitmflog)
            string_to_append += ","+str(jitmflog_size)

            string_to_append += ","+str((jitmflog_size/1024)/1024)

        result_file.write(string_to_append.rstrip(",")+"\n")
result_file.close()
