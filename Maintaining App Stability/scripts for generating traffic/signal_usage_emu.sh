export PATH=${PATH}:~/Android/Sdk/platform-tools/

# # launched and in chat ready to write
# # run cmd: sudo ./adb_monkey_runner_6.1.1.sh "Sending\ text\ message\ 4" .
adb  shell monkey -p org.thoughtcrime.securesms -c android.intent.category.LAUNCHER 1 # launch Telegram
sleep 5
adb  shell input tap 720 518 # Pick first chat - used for telegram SP
# adb  shell input tap 313 745 # Pick second chat - used for telegram CP

adb  shell input tap 400 2670 # click on text box
sleep 1
adb  shell input keyboard text "$1" # input text
sleep 2
adb  shell input tap 1264 1689 # press send
sleep 2
adb  shell input tap 297 2941 #minimize
sleep 2
adb shell input keyevent 4 #back
sleep 2
adb shell input keyevent 3 #home
sleep 2
