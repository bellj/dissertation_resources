#!/bin/bash

# firefox -ProfileManager 
# pick a profile named Jennifer
# Jennifer profile needs to have pushbullet logged in
adb  shell monkey -p com.pushbullet.android -c android.intent.category.LAUNCHER 1 # launch Telegram
filename=$(find ~/.mozilla/firefox/ -name *Jennifer)
# source ./automate_attack/pushbulletenv/bin/activate
#python3 $ROOT_EXP_DIR/automate_attack/pushbullet/selenium_start_browser_session.py $filename ${1} ${2}
python3 selenium_start_browser_session.py $filename ${1} ${2}
# deactivate
# LOG GROUND TRUTH HERE
rm -rf .fuse_*
rm -rf geckodriver.log
