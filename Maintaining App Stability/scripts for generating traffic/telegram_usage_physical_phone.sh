# pixel 3xl physical
export PATH=${PATH}:~/Android/Sdk/platform-tools/

# # launched and in chat ready to write
# # run cmd: sudo ./adb_monkey_runner_6.1.1.sh "Sending\ text\ message\ 4" . 
# OR
# adb shell getevent -l
adb  shell monkey -p org.telegram.messenger -c android.intent.category.LAUNCHER 1 # launch Telegram
sleep 5
adb  shell input tap 313 545 # Pick chat - used for telegram SP

adb  shell input tap 886 2694
sleep 3 #wait for load
adb  shell input keyboard text "$1" # input text
sleep 2 
adb  shell input tap 996 1248 # press send  
sleep 1
# adb  shell input tap 817 1121 # hold
# sleep 1
# adb  shell input tap 365 1721 # delete
# sleep 1
# adb  shell input tap 224 1141 # also delete
# sleep 1
# adb  shell input tap 891 1253 # delete
# sleep 1
adb  shell input tap 216 2168 # minimize
sleep 2
adb shell input keyevent 4 #back
sleep 2
adb shell input keyevent 3 #home
sleep 2
