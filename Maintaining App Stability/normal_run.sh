#!/bin/bash

# getting performance results for uninstrumented versions of the same apps. Called by start_experiment.sh

GLOBAL_LOOP=$3
MSG_LOOP=$4
IN_MSG_W8=$5

results_folder_name=$1
mkdir -p $results_folder_name
setup=$2

echo "Starting Telegram - normal run"
for ((j=1; j <= $GLOBAL_LOOP; j++)); #overall loop
do
    folder_name="telegram_vanilla_run$j"
    mkdir ./$results_folder_name/$folder_name

     echo "RUN: $folder_name Global loop $j of $GLOBAL_LOOP"

    echo "Starting emulator"
    nohup ~/Android/Sdk/tools/emulator -avd exp3_jennifer -no-snapshot-save > /dev/null 2>&1 &
    sleep 5
    adb root
    adb shell dumpsys gfxinfo org.telegram.messenger reset 
    adb shell am force-stop org.telegram.messenger # close app
    adb shell rm -rf /sdcard/jitmflogs/* 
    adb shell rm -rf /data/local/tmp/* #removing residue drivers
    cd ./scripts\ for\ generating\ traffic/
    adb logcat -c

    sleep 2

    for ((i=1; i <= $MSG_LOOP; i++));
    do
        len=`shuf -i 10-100 -n 1`
        string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`

        wait_v=`shuf -i 0-$IN_MSG_W8 -n 1`
        echo "ROUND $i of $MSG_LOOP"
        echo "Sleeping for $wait_v"
        sleep $wait_v

        echo "Sending msg"
        ./telegram_usage_$setup.sh "ConfidentialInfo_$string" 
        current_date=`date "+%m-%d %T"`
        echo "$current_date V/[ACC_ATTACK_LOG]( 8430): Telegram, Sent message: ConfidentialInfo_$string " >> ../$results_folder_name/${folder_name}/ground_truth_timeline.txt
    done
    sleep 5

    cd ..

    adb shell dumpsys gfxinfo org.telegram.messenger | grep Janky >> ./$results_folder_name/$folder_name.txt 
    adb logcat -d  -b crash -v time >> ./$results_folder_name/$folder_name.txt
    adb logcat -d -v time >> ./$results_folder_name/$folder_name/logcat.txt
    adb pull /data/anr/ ./$results_folder_name/$folder_name/ 
    adb shell rm -rf /data/anr/*

    adb  shell am force-stop org.telegram.messenger 
    echo "Killing emulator"
    adb emu kill
    sleep 5
done

sleep 5

echo "Starting Signal"
for ((j=1; j <= $GLOBAL_LOOP; j++)); #overall loop
do
    folder_name="signal_vanilla_run$j"
    mkdir ./$results_folder_name/$folder_name

    echo "RUN: $folder_name Global loop $j"
    
    echo "Starting emulator"
    nohup ~/Android/Sdk/tools/emulator -avd exp3_jennifer -no-snapshot-save > /dev/null 2>&1 &
    sleep 5

    adb shell dumpsys gfxinfo org.thoughtcrime.securesms reset  
    adb shell am force-stop org.thoughtcrime.securesms # close app
    adb shell rm -rf /data/local/tmp/* #removing residue drivers
    cd ./scripts\ for\ generating\ traffic/
    adb logcat -c

    sleep 2

    for ((i=1; i <= $MSG_LOOP; i++));
    do
        len=`shuf -i 10-100 -n 1`
        string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`

        wait_v=`shuf -i 0-$IN_MSG_W8 -n 1`
        echo "ROUND $i"
        echo "Sleeping for $wait_v"
        sleep $wait_v

        echo "Sending msg"
        ./signal_usage_$setup.sh "ConfidentialInfo_$string" 
        current_date=`date "+%m-%d %T"`
        echo "$current_date V/[ACC_ATTACK_LOG]( 8430): Telegram, Sent message: ConfidentialInfo_$string " >> ../$results_folder_name/${folder_name}/ground_truth_timeline.txt
    done
    sleep 5

    cd ..

    adb shell dumpsys gfxinfo org.thoughtcrime.securesms | grep Janky >> ./$results_folder_name/$folder_name.txt 
    adb logcat -d  -b crash -v time >> ./$results_folder_name/$folder_name.txt
    adb logcat -d -v time >> ./$results_folder_name/$folder_name/logcat.txt
    adb pull /data/anr/ ./$results_folder_name/$folder_name/ 
    adb shell rm -rf /data/anr/*

    adb  shell am force-stop org.thoughtcrime.securesms 
    echo "Killing emulator"
    adb emu kill
    sleep 5
done

sleep 5

echo "Starting Pushbullet"
for ((j=1; j <= $GLOBAL_LOOP; j++)); #overall loop
do
    folder_name="pushbullet_vanilla_run$j"
    mkdir ./$results_folder_name/$folder_name

    echo "RUN: $folder_name Global loop $j"
    
    echo "Starting emulator"
    nohup ~/Android/Sdk/tools/emulator -avd exp3_jennifer_pushbulletonly -no-snapshot-save > /dev/null 2>&1 &
    sleep 5

    adb shell dumpsys gfxinfo com.pushbullet.android reset 
    adb shell am force-stop com.pushbullet.android # close app
    adb shell rm -rf /data/local/tmp/* #removing residue drivers
    adb shell rm -rf /sdcard/jitmflogs/* 
    cd ./scripts\ for\ generating\ traffic/
    adb logcat -c

    sleep 2

    for ((i=1; i <= $MSG_LOOP; i++));
    do
        len=`shuf -i 10-100 -n 1`
        string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`

        wait_v=`shuf -i 0-$IN_MSG_W8 -n 1`
        echo "ROUND $i"
        echo "Sleeping for $wait_v"
        sleep $wait_v

        echo "Sending msg"
        ./pushbullet_usage_$setup.sh "ConfidentialInfo_$string"  ../$results_folder_name/${folder_name}/ground_truth_timeline.txt
    done
    sleep 5

    cd ..

    adb shell dumpsys gfxinfo com.pushbullet.android | grep Janky >> ./$results_folder_name/$folder_name.txt 
    adb logcat -d  -b crash -v time >> ./$results_folder_name/$folder_name.txt
    adb logcat -d -v time >> ./$results_folder_name/$folder_name/logcat.txt
    adb pull /data/anr/ ./$results_folder_name/$folder_name/ 
    adb shell rm -rf /data/anr/*
    
    adb  shell am force-stop orcom.pushbullet.android 
    echo "Killing emulator"
    adb emu kill
    sleep 5
done