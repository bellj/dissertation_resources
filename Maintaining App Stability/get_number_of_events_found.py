# /instrumented_runs_combined/ -> the folder containing sampling results from JIT-MF instrumented apps
# generates results_number_of_events_found.json

# python3 to_get_number_of_events_found.py

import os
import json

if __name__ == '__main__':
    results = {"results": []}

    app_order={
        "pushbullet": 1000000,
        "telegram": 2000000,
        "signal": 3000000
    }

    for file in os.listdir("instrumented_runs_combined"):
        if not os.path.isfile(f"instrumented_runs_combined/{file}"):
            with open(f"instrumented_runs_combined/{file}/ground_truth_timeline.txt", "r") as ground_truth_timeline_file:
                total_number_of_messages = 0
                messages_found = 0
                for line in ground_truth_timeline_file.readlines():

                    confidential_message = [string for string in line.split(' ') if "ConfidentialInfo" in string][0].replace("'", "")
                    total_number_of_messages += 1
                    if not os.path.exists(f"instrumented_runs_combined/{file}/jitmflogs"):
                        continue

                    for file_jitmflogs_path in os.listdir(f"instrumented_runs_combined/{file}/jitmflogs"):
                        if ".jitmf" in file_jitmflogs_path:
                            with open(f"instrumented_runs_combined/{file}/jitmflogs/{file_jitmflogs_path}", "r") as file_jitmflogs_file:
                                if confidential_message in file_jitmflogs_file.read():
                                    messages_found += 1
                                    break

                app, _, samplingStrategy = os.path.splitext(file)[0].split("_", maxsplit=2)
                samplingStrategy = samplingStrategy.replace("samping", "sampling")  # grammatical mistake
                run = samplingStrategy.split("_")[-1]
                samplingStrategy = '_'.join(samplingStrategy.split("_")[:-1])

                digits = ''.join(filter(str.isdigit, samplingStrategy))
                digits = digits or 0
                total_order = app_order[app] + (100000 if "calls" in samplingStrategy else 0) + int(digits)

                results["results"].append({
                    "order": total_order,
                    "name": file.replace(run, ""),
                    "total_messages":total_number_of_messages,
                    "messages_found": messages_found,
                    "run": run
                })
    f = open("results.json", "w")
    json.dump(results, f)