# Contents of this Repository
This repository contains resources (scripts, tools, experimentation setup and results) related to my dissertation titled "Responding to stealthy attacks on Android using timely-captured memory dumps". The resources are a reflection of work carried out for 3 conference papers and 2 journal papers (as mentioned below).

`Trigger Point Accuracy/*` -> This folder contains experiment scripts and resources for Section 5.2 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers and original) \
`   ├── hooking_with_server_IM` -> Backup copy of `hooking_with_server_IM_PENTEST`  \
`   ├── hooking_with_server_IM_PENTEST` -> Contains scripts used to run and process results for instant messaging case study (Telegram) \
`   │   ├── monkeyrunner` ->   Contains the scripts used in the experiment to simulate accessibility attack misuse. \
`   │   ├── hook_template.js` -> JIT-MF driver template, populated by variables in `invoke_exp_run.py`\
`   │   ├── hook_native.js` -> Equivalent to `hook_template.js` but used for native drivers.\
`   |   ├── invoke_exp_run.py` -> Starts experiment. Contains list of drivers, pipe-seperated as "event|trigger point type|function, to populate `hook_template.js` \
`   |   ├── post<-|_>.sh` -> Post-processing scripts\
`   |   └── res-agg.sh` -> Aggregates results\
`   ├── hooking_with_server_SMS` ->   Contains scripts used to run and process results for SMS case study (Pushbullet) \
`   │   ├── android_scripts` -> Contains the scripts used in the experiment to simulate accessibility attack misuse. Spying occurs when remote Pushbullet screen is loaded in the attacker's browser. \
`   │   ├── hook_template.js` -> JIT-MF driver template, populated by variables in `invoke_exp_run.py`\
`   │   ├── hook_native.js` ->  Equivalent to `hook_template.js` but used for native drivers.\
`   |   ├── invoke_exp_run.py` -> Starts experiment. Contains list of drivers, pipe-seperated as "event|trigger point type|function, to populate `hook_template.js`\
`   |   ├── post<-|_>.sh` -> Post-processing scripts\
`   |   └── time_console_res_agg.sh` -> Aggregates results\
`   └── pentest_tool_outputs` ->  \
`   └── results/*` ->  Experiment processed results \
> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_1)**  \
> **JIT-MF drivers used:** `Trigger Point Accuracy/hooking_with_server_<IM_PENTEST/SMS>/hook_template.js` (in this repo. Specific trigger points used are listed in `invoke_exp_run.py`)\
> **Results:** In folder ./results \
> **[Related Conference Paper](https://link.springer.com/chapter/10.1007/978-3-030-70852-8_2)**  \
> **[Related Journal Paper](https://ieeexplore.ieee.org/abstract/document/9737464)**

\
`Forensic Timeline Accuracy/*` -> This folder contains experiment scripts and resources for  Section 5.3 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers and original) \
`   ├── automate_attack/*` -> Contains the scripts used in the experiment to simulate accessibility attack misuse. `_sp` indicate spying attack scenarios and `cp` indicate crime-proxy attack scenarios. For Pushbullet case study, spying occurs when remote Pushbullet screen is loaded in the attacker's browser. \
`   ├── docker_metasploit_env/*` -> Docker environment required for experiment setup to carry out Metasploit Accessibility Attacks\
`   ├── experiment_run/*` -> This folder contains experiment automation scripts \
`   │    ├── full_run_<app>_<attack_type>.sh` -> Scripts used to initiate experiment run for <app> and <attack_type>. These scripts insert JIT-MF driver, generate noise, simulate accessibility attack, gather baseline forensic artefacts, gather JIT-MF forensic artefacts and generate forensic timelines using Plaso. \
`   │    ├── create_kb.py` -> Generates a knowledge base from the sources gathered. \
`   │    └── phone_automation_scripts/*` -> Scripts used to automate noise generation on the phone.\
`   ├── git_repos/` -> Forked public repositories used for parsing forensic artefacts collected to create a knowledge base. Plaso was modfified to include a JIT-MF parser.\
`   │    ├── plaso` \
`   │    ├── signalbackup-tools` \
`   │    ├── teleparser` \
`   │    └── walitean ` \
`   ├── Dockerfile` -> Docker environment required for experiment setup. Initiates scripts from `experiment_run/*` \
`   └── startExpDocker.sh` -> Starts the docker container  \
`   └── results/*` -> Experiment processed results

> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_2)**\
> **[JIT-MF drivers used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_2/resources/drivers)**\
> **Results:** In folder ./results \
> **[Related Conference Paper](https://www.scitepress.org/PublishedPapers/2021/106036/106036.pdf)**\
> **[Related Journal Paper](https://ieeexplore.ieee.org/abstract/document/9737464)**

\
`Evidence Object Accuracy/*` -> This folder contains experiment scripts and resources for  Section 5.4 \
`PT1 - QUALITATIVE`\
`PT2 - QUANTITATIVE (replaced by appbrain)`\
`PT3 - QUANTITATIVE (version grep search)`\
`PT2- was a quantitative experiment that we tried to do on 1000 most popular apps. This has now been put to the side as appbrain gives us "better" ready, statistics` \
`   ├── apps/*` -> Contains the apps used in the experiment  \
`   ├── PT1/*` -> Qualitative experiment related to effectiveness of infrastructure-centric drivers. The folder includes setup scripts used. \
`   ├── PT2/*` -> Qualitative experiment related to permeance of infrastructure usage across app versions. The folder includes setup scripts used. \
`   ├── PT3/*` -> Quantitative experiment related to infrastructure usage across apps - through statistics obtained from AppBrain. The folder includes setup scripts used. \
`   ├── app_sources/*` -> Contains the app source code \
`   ├── results/*` -> Processed Results of PT1, PT2 and PT3 experiments \
`   └── loc_comparison.py` -> Calculates lines of code of app source code (./app_sources) that include reference to SQLite API usage. 

> **[JIT-MF drivers used](https://gitlab.com/bellj/infrastructure_based_agents/-/blob/main/sqlite_driver.js?ref_type=heads)**\
> **JIT-MF drivers used:** As found in ./results folder. \
> **Results:** In folder ./results \
> **[Related Conference Paper](http://staff.um.edu.mt/__data/assets/pdf_file/0019/511075/ICISSP_2023_31_CR.pdf)**\
> **[Related Conference Paper Github Repo](https://gitlab.com/bellj/infrastructure_based_agents)**
> **[Related Journal Paper](https://www.mdpi.com/2624-800X/3/3/19)**

\
`Maintaining App Stability/*` -> This folder contains experiment scripts and resources for Section 5.5 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers and original) \
`   │    ├── Systematic_sampling_TP_limit_findout/*` -> Contains scripts used to find out the number of times a TP is hit - required for determining the required sample window size for an app when using Systematic Sampling.\
`   │    ├── sampling_<app>_drivers/*` -> These folders contain the JIT-MF drivers generated for app (pushbullet|signal|telegram), satering for every sampling window value and method. JIT-MF Drivers called "\*_1inrandom_lessthan\<X\>.js" are drivers implementing a periodic sampling approach. Those called "\*_1inrandom_calls_lessthan\<X\>.js" are drivers implementing a systematic sampling approach based on TP hits. X is the sample window value.\
`   │    └── scripts for generating traffic/*` -> Contains scripts used to automate typical app usage.\
`   ├── get_number_of_events_found.py` -> Gets the number of events found in a JIT-MF log file / number of events that occurred as per known ground truth.\
`   ├── start_experiment.sh` -> Script used to initiate experiment run. It pushes the JIT-MF driver, automates typical app usage, logs events in usage, collects JIT-MF forensic artifacts and gets number of crashed, Janky Frames and size of JIT-MF files accumulated on the phone.\
`   ├── process_findings.py` -> Processes output of `normal_run.sh` to output an excel sheet. \
`   ├── normal_run.sh` -> Same as `start_experiment.sh`, but without JIT-MF driver. \
`   └── process_normal_runs_findings.py` -> Same as `process_findings.py` for `normal_run.sh` \
`   └── results/*` -> Experiment processed results

> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_3)** \
> **JIT-MF drivers used**: `Sampling_Experiment/sampling_<app>_drivers` (in this repo) \
> **Results:** In folder ./results \
> **[Related Journal Paper](https://ieeexplore.ieee.org/abstract/document/9737464)**

\
`Evaluation with Mobile Forensics Tools/*` -> This folder contains experiment scripts and resources for Section 6.1 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers, original and malicious app) \
`   ├── scripts_for_generating_traffic/*` ->   \
`   ├── whatsapp_driver/*` -> Contains the JIT-MF drivers used for this experiment  \
`   ├── abe.jar` -> Unpacks WhatApp backup files to produce a tar file from which the WhatsApp encryption key can be extracted.  \
`   ├── dumpsys_parser.py` -> Parser for `dumpsys` logs to be transformed in csv output to be ingested by Timesketch  \
`   ├── parse_csv_tools_to_timesketch.py` -> Parses output produced by Belkasoft, XRY to produce a csv out format that can be ingested by Timesketch  \
`   ├── pull_evidence.sh` -> Pulls evidence from forensic sources on Android phone.  \
`   ├── pull_evidence_android.sh` -> Same as `pull_evidence.sh`, required for Android 8 Device.  \
`   └── start_experiment.sh` -> Initiates the experiment. Initiates noise and stealthy removal of WhatsApp Pink messages. Pulls evidence and parses output. 

> **[MobFor Tool](https://gitlab.com/mobfor/mobfor-project)** \
> **[JIT-MF version used in MobFor Tool](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_3)** \
> **JIT-MF drivers used**: `Evaluation with Mobile Forensics Tools/whatsapp_driver` (in this repo) \
> **[Results](https://drive.google.com/drive/u/0/folders/1FUgug7-UZlpjbBWEZp7vLaSsDxrDhIsz)** \
> **[Related Journal Paper](https://ieeexplore.ieee.org/abstract/document/9737464)**

\
`JIT-MF_Logs for Anomaly Detection/*` -> This folder contains experiment scripts and resources for Section 6.2
`Using ReLF on GRR as EDR` \
`RELF-Server - branch 3.2.1.1-android (Commit ID: 8973437 on Oct 28)` \
`RELF-Client - branch master (Commit ID: 579fbb4 on Jul 5, 2021) - contains own changes in file org.nexus_lab.relf.lib.rdfvalues.android.RDFAndroidTelephonyInfo.java;`\
`   ├── EDRs/*` -> Contains EDR setup used \
`   ├── Virtual_App_ARM_Up_to_12/*` -> VirtualApp Version used, including Frida gadget and JIT-MF driver. \
`   ├── frida-material/*` ->  Frida gadget version used and config \
`   ├── obj1_exp/*` ->  App brain statistics for infrastructure usage across messaging and banking apps. The folder includes srcipts to run the experiment. \
`   ├── obj2_exp/*` ->  CPU and memory usage, while using VirtualApp. The folder includes srcipts to run the experiment. \
`   ├── obj3_exp/*` ->  Setup scripts for qualitative experiment. The subfolder structure mimics the setup described [here](https://gitlab.com/bellj/vedrando). \
`   ├── results/*` ->  -> Processed Results of PT1, PT2 and PT3 experiments \

> **JIT-MF drivers used**: `JIT-MF_Logs for Anomaly Detection/Android10/frida-scripts/driver.js` (in this repo) \
> **Results**: In folder ./results \
> **[Google BigQuery datasets and models](https://console.cloud.google.com/bigquery?ws=!1m4!1m3!3m2!1sutility-ratio-120412!2sgrr_app_models)** generated using the scripts `./obj_exp3/model_generator.sql` \
> **[Related Journal Paper](https://www.mdpi.com/2624-800X/3/3/19)** \
> **[Related Journal Paper Github Repo](https://gitlab.com/bellj/vedrando)**
