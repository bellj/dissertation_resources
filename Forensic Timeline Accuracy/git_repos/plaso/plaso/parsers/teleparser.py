#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Parser for teleparser (csv) files."""

from csv import reader
import codecs

from dfdatetime import posix_time as dfdatetime_posix_time
from dfdatetime import semantic_time as dfdatetime_semantic_time

from plaso.lib import errors
from plaso.lib import definitions
from plaso.containers import events
from plaso.containers import time_events
from plaso.parsers import interface
from plaso.parsers import manager


class TeleparserEventData(events.EventData):

  """Android application usage event data.

  Attributes:
  package (str): name of the Android application.
  component (str): name of the individual component of the application.
  """

  DATA_TYPE = 'android:telegram:event:time'

  def __init__(self):
    """Initializes event data."""

    super(TeleparserEventData,
            self).__init__(data_type=self.DATA_TYPE)
    self.log_source = None
    self.source = None
    self.id = None
    self.type = None
    self.from_user = None
    self.from_id = None
    self.to_user = None
    self.to_id = None
    self.dialog = None
    self.dialog_type = None
    self.content = None
    self.media = None
    self.extra = None

class TeleparserParser(interface.FileObjectParser):

  """Parses the Teleparser (csv) file."""

  NAME = 'teleparser'
  DATA_FORMAT = 'Teleparser (csv) file'
  _ENCODING = 'utf-8'

  def ParseFileObject(self, parser_mediator, file_object):
    """Parses an Android usage-history file-like object.
    Args:
    parser_mediator (ParserMediator): mediates interactions between parsers
    and other components, such as storage and dfvfs.
    file_object (dfvfs.FileIO): file-like object.

    Raises:
    UnableToParseFile: when the file cannot be parsed.
    """
    filename = parser_mediator.GetFilename()
    if filename != 'timeline.csv':
      raise errors.UnableToParseFile(
        'Not an Android usage history file [not XML]')

    file_content = file_object.read()
    file_content = codecs.decode(file_content, self._ENCODING)
    lines = file_content.splitlines()
    csv_reader = reader(lines)
    line_count = 0
    for row in csv_reader:
      if line_count != 0:
        line_count+=1

        event_data = TeleparserEventData()
        event_data.log_source = "teleparser_timeline.csv"

        event_data.source = row[1]
        event_data.id = row[2]
        event_data.type = row[3]
        event_data.from_user = row[4]
        event_data.from_id = row[5]
        event_data.to_user = row[6]
        event_data.to_id = row[7]
        event_data.dialog = row[8]
        event_data.dialog_type = row[9]
        event_data.content = row[10]
        event_data.media = row[11]
        event_data.extra = row[12]

        if len(row[0]) != 0:
          time_string = int(row[0])
          date_time = dfdatetime_posix_time.PosixTime(timestamp=time_string)
        else:
          parser_mediator.ProduceExtractionWarning((
              'Unable to parse start time string: {0:s} with error: No time available').format(row[0]))
          date_time = dfdatetime_semantic_time.InvalidTime()

        event = time_events.DateTimeValuesEvent(date_time,definitions.TIME_DESCRIPTION_START)
        parser_mediator.ProduceEventWithEventData(event, event_data)
      else:
        line_count+=1

# The current offset of the file-like object needs to point at
# the start of the file for ElementTree to parse the XML data correctly.

manager.ParsersManager.RegisterParser(TeleparserParser)
