import json
import glob
import sys
import os

read_files = glob.glob(sys.argv[1]+"/*.jitmflog")
lines_seen = set()
id_name_map = {}

def create_json(evidence_object, date_time):
    entry={}
    evidence_json = json.loads(evidence_object)
    entry["to_id"] = evidence_json["to_id"]
    entry["from_id"] = evidence_json["from_id"]
    entry["message_id"] = evidence_json["message_id"]
    entry["date"] = date_time
    entry["text"] = evidence_json["text"]
    return json.dumps(entry)

def check_entry(evidence_object, data, scenario):
    add = True
    evidence_json = json.loads(evidence_object)
  
    if len(evidence_json['to_name']) <=0: #adds name if id exists in map and entry has no name
        try:
            evidence_json["to_name"]=id_name_map[evidence_json['to_id']]
        except:
            pass

    if len(evidence_json['from_name']) <=0: #adds name if id exists in map and entry has no name
        try:
            evidence_json["from_name"]=id_name_map[evidence_json['from_id']]
        except:
            pass

    if isinstance(json.dumps(evidence_json['message_id']).strip('"'), int) and int(json.dumps(evidence_json['message_id']).strip('"')) < 0: #removes entries where message id <0
        add &= False
    if "_sp" in scenario:
        time = data["time"]
    elif "_cp" in scenario:
        time = evidence_json["date"]
    
    entry = create_json(evidence_object, time)
    if entry in lines_seen: #removes entry if duplicate
        add &= False

    return (add,evidence_json,entry)

def populate_map(evidence_json): #adds names to ids in map
    if len(evidence_json['to_name']) >0:
        id_name_map[evidence_json['to_id']]=evidence_json['to_name']
    if len(evidence_json['from_name']) >0:
        id_name_map[evidence_json['from_id']]=evidence_json['from_name'] 
   
scenario = sys.argv[3]
with open(os.path.join(sys.argv[2],"merged-logs.jitmflog"), "wb") as outfile:
    for f in read_files:
        with open(f, "rb") as infile:
            outfile.write(infile.read())

with open(os.path.join(sys.argv[2],"merged-logs.jitmflog")) as f:
    for line in f:
        data = json.loads(line)
        populate_map(data['object'])

outfile = open(os.path.join(sys.argv[2],"merged-logs-uniq.jitmflog"), "w")

with open(os.path.join(sys.argv[2],"merged-logs.jitmflog")) as f:
    for line in f:
        data = json.loads(line)
        evidence_object=json.dumps(data['object'])
        add_line, evidence_json, entry = check_entry(evidence_object, data, scenario)
        data['object']=evidence_json
        
        if add_line: # not a duplicate
            outfile.write(json.dumps(data))
            outfile.write('\n')
            
            lines_seen.add(entry)
     
outfile.close()
