#!/bin/bash
# $1 - initial counter for folder
# $2 - number of runs
# NOISE_RUNS=$3
# ATTACK_RUNS=$4
# NO_RUNS=$5 # No. of runs

# to be implemented (TBI): 
# $5 - attack: telegram_cp, telegram_sp as per folder contents of: /mnt/2TBdrive/gitlab/experiment_2/automate_attack/


# ./full_run.sh 4 2 3 2         <- start with folder name 4, numer of runs(niose + attack), no. of noise loops, number of attack loops, number of 

# set up plaso and pushbullet environments - for pushbullet scenarios - no docker
# virtualenv ../git_repos/plasoenv
# source ../git_repos/plasoenv/bin/activate
# cd ../git_repos/plaso
# pip3 install --upgrade pip
# pip3 install -r requirements.txt
# deactivate
# cd ../experiment_run

# virtualenv ../automate_attack/pushbulletenv
# source ../automate_attack/pushbulletenv/bin/activate
# cd ../automate_attack/pushbullet
# pip3 install --upgrade pip
# pip3 install -r requirements.txt
# deactivate
# cd ../experiment_run

# make jitmflog dir
# nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &

# nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_EXPERIMENT2_telegram5.12 -no-snapshot-save > /dev/null 2>&1 &
# sleep 5
# adb shell mkdir /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs 
# adb shell mkdir /sdcard/jitmflogs 
# adb emu kill

start=`date +%s`
curr_count=1
START_LOOP=148
NO_RUNS=2

for ((i=$START_LOOP; i < $NO_RUNS; i++));
do
    echo "Round $i"
# --- TELEGRAM CP ----    
    # NAME_APPEND=large_run_$i
    # NOISE_RUNS=1
    # ATTACK_RUNS=1

    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5

    # adb root
    # docker run --rm -v ~/.msf4:/root/.msf4 -v /home/uom-lab-locard/jennifer-exp2:/jennifer-exp2 --privileged --network="host" jb_exp2 ./full_run_telegram_cp.sh run_result_telegram_cp_$NAME_APPEND $curr_count $NOISE_RUNS $ATTACK_RUNS $NO_RUNS 
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS

    # adb emu kill
    # rm -rf *.log.gz

# --- TELEGRAM SP ----
    # NAME_APPEND=large_run_$i
    # NOISE_RUNS=13
    # ATTACK_RUNS=1

    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5

    # adb root
    # docker run --rm -v ~/.msf4:/root/.msf4 -v /home/uom-lab-locard/jennifer-exp2:/jennifer-exp2 --privileged --network="host" jb_exp2 ./full_run_telegram_sp.sh run_result_telegram_sp_$NAME_APPEND $curr_count $NOISE_RUNS $ATTACK_RUNS $NO_RUNS 
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS

    # /home/uom-lab-locard/jennifer-exp2/experiment_run/phone_automation_scripts/delete_history_andstudio_pxl5.sh emulator-5554

    # sleep 3 
    # adb emu kill
    # rm -rf *.log.gz
    # sleep 3 

# --- SIGNAL CP ----    
    # NAME_APPEND=large_run_$i
    # NOISE_RUNS=6
    # ATTACK_RUNS=3

    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5

    # adb root
    # docker run --rm -v ~/.msf4:/root/.msf4 -v /home/uom-lab-locard/jennifer-exp2:/jennifer-exp2 --privileged --network="host" jb_exp2 ./full_run_signal_cp.sh run_result_SIGNAL_cp_$NAME_APPEND $curr_count $NOISE_RUNS $ATTACK_RUNS $NO_RUNS 
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS

    # adb emu kill
    # rm -rf *.log.gz

# --- SIGNAL SP ----    
    # NAME_APPEND=large_run_$i
    # NOISE_RUNS=13
    # ATTACK_RUNS=1

    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5

    # adb root
    # docker run --rm -v ~/.msf4:/root/.msf4 -v /home/uom-lab-locard/jennifer-exp2:/jennifer-exp2 --privileged --network="host" jb_exp2 ./full_run_signal_sp.sh run_result_SIGNAL_sp_$NAME_APPEND $curr_count $NOISE_RUNS $ATTACK_RUNS $NO_RUNS 
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS

    # adb emu kill
    # rm -rf *.log.gz

# --- PUSHBULLET CP ----    
    # NAME_APPEND=$i
    # NOISE_RUNS=6
    # ATTACK_RUNS=3

    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_EXPERIMENT2_telegram5.12 -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5
    # export JITMF_PATH=/mnt/2TBdrive/gitlab/jit-mf
    # export ROOT_EXP_DIR=/mnt/2TBdrive/gitlab/experiment_2
    # ./full_run_pushbullet_cp.sh run_results_pushbullet_cp/run_result_pushbullet_cp_$NAME_APPEND $curr_count $NOISE_RUNS $ATTACK_RUNS $NO_RUNS 
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS

    # adb emu kill
    # rm -rf *.log.gz

# --- PUSHBULLET SP ----    
    # NAME_APPEND=$i
    # NOISE_RUNS=13
    # ATTACK_RUNS=1

    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_EXPERIMENT2_telegram5.12 -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5
    # export JITMF_PATH=/mnt/2TBdrive/gitlab/jit-mf
    # export ROOT_EXP_DIR=/mnt/2TBdrive/gitlab/experiment_2
    # ./full_run_pushbullet_sp.sh run_results_pushbullet_sp/run_result_pushbullet_sp_$NAME_APPEND $curr_count $NOISE_RUNS $ATTACK_RUNS $NO_RUNS 
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS

    # adb emu kill
    # rm -rf *.log.gz


# --- MEMORY TEST ----
    # nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &
    # sleep 5

    # adb root
    # docker run --rm -v ~/.msf4:/root/.msf4 -v /home/uom-lab-locard/jennifer-exp2:/jennifer-exp2 --privileged --network="host" jb_exp2 ./telegram_memory_test.sh run_result_telegram_mem_test
    
    # ./full_run_telegram_sp.sh run_result_telegram_sp_$i telegram_sp
    # ./full_run_signal_cp.sh run_result_signal_cp_$i signal_cp
    # ./full_run_signal_sp.sh run_result_signal_sp_$i signal_sp
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS+NOISE_RUNS

# --- FOR TIME ----

    nohup ~/Android/Sdk/tools/emulator -avd Pixel_3_XL_API_29_jennifer_experiment -no-snapshot-save > /dev/null 2>&1 &
    sleep 5

    adb root
    docker run --rm -v ~/.msf4:/root/.msf4 -v /home/uom-lab-locard/jennifer-exp2:/jennifer-exp2 --privileged --network="host" jb_exp2 ./telegram_time_test.sh run_result_telegram_time_test
    adb emu kill
    # ./full_run_telegram_sp.sh run_result_telegram_sp_$i telegram_sp
    # ./full_run_signal_cp.sh run_result_signal_cp_$i signal_cp
    # ./full_run_signal_sp.sh run_result_signal_sp_$i signal_sp
    # let curr_count=curr_count+NOISE_RUNS+ATTACK_RUNS+NOISE_RUNS


done
end=`date +%s`
echo Execution time was `expr $end - $start` seconds.
# rm -rf git_repos/plasoenv
# rm -rf automate_attack/pushbulletenv