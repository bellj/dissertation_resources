adb -s $1 shell monkey -p org.thoughtcrime.securesms -c android.intent.category.LAUNCHER 1 # launch Signal
sleep 5
adb -s $1 shell input tap 1354 271 # click hamburger
sleep 2
adb -s $1 shell input tap 976 770 # click settings
sleep 2
adb -s $1 shell input tap 288 1825 # click chats
sleep 2
adb -s $1 shell input tap 450 2176 # click chat backups
sleep 2
adb -s $1 shell input tap 396 703 # click create backup
sleep 6
adb -s $1 shell am force-stop org.thoughtcrime.securesms # close app