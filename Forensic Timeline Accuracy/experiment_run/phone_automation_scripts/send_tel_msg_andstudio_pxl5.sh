# # launched and in chat ready to write
# # run cmd: sudo ./adb_monkey_runner_6.1.1.sh "Sending\ text\ message\ 4" .
adb -s $1 shell monkey -p org.telegram.messenger -c android.intent.category.LAUNCHER 1 # launch Telegram
sleep 5
adb -s $1 shell input tap 313 545 # Pick first chat - used for telegram SP
# adb -s $1 shell input tap 313 745 # Pick second chat - used for telegram CP

adb -s $1 shell input tap 864 518 # click on text box
adb -s $1 shell input tap 886 2694
sleep 1
adb -s $1 shell input keyboard text "$2" # input text
sleep 2
adb -s $1 shell input tap 1359 1726 # press send
sleep 1
adb -s $1 shell input tap 301 2867 #minimize keyboard
sleep 1
adb -s $1 shell am force-stop org.telegram.messenger # close app