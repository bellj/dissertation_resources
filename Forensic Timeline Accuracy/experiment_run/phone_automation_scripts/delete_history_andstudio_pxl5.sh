# # launched and in chat ready to write
# # run cmd: sudo ./adb_monkey_runner_6.1.1.sh "Sending\ text\ message\ 4" .
adb -s $1 shell monkey -p org.telegram.messenger -c android.intent.category.LAUNCHER 1 # launch Telegram
sleep 5
adb -s $1 shell input tap 313 545 # Pick first chat
# adb -s $1 shell input tap 313 745 # Pick second chat
sleep 2
adb -s $1 shell input tap 1381 277 # press options
sleep 1
adb -s $1 shell input tap 976 801 # press clear history 
sleep 1
adb -s $1 shell input tap 297 1652 # press ok
sleep 1
adb -s $1 shell input tap 1008 1788 # press ok

sleep 5
adb -s $1 shell am force-stop org.telegram.messenger # close app
