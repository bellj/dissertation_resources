import json
import glob
import sys
import os

read_files = glob.glob(sys.argv[1]+"/*.jitmflog")
lines_seen = []
id_name_map = {}
   
scenario = sys.argv[3]
with open(os.path.join(sys.argv[2],"merged-logs.jitmflog"), "wb") as outfile:
    for f in read_files:
        with open(f, "rb") as infile:
            outfile.write(infile.read())

outfile = open(os.path.join(sys.argv[2],"merged-logs-uniq.jitmflog"), "w")

with open(os.path.join(sys.argv[2],"merged-logs.jitmflog")) as f:
    for line in f:
        data = json.loads(line)
        print(data)
        if data not in lines_seen:
            outfile.write(json.dumps(data))
            outfile.write('\n')
            
            lines_seen.append(data)
     
outfile.close()
