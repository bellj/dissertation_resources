#!/bin/bash
# $1 - parent dir to store results in 

j=$2 # full-run.sh will give "current" count
END_LOOP=$3 # full-run.sh will give number of "normal" runs
ATTACK_END_LOOP=$4 # full-run.sh will give number of attack runs
emulator=emulator-5554
scenario=pushbullet_cp

adb root

echo "Starting: $scenario"

echo "Pushing the driver $JITMF_PATH/resources/drivers/cp_pb_online_native_hook.js"
adb push $JITMF_PATH/resources/drivers/cp_pb_online_native_hook.js /data/local/tmp/cp_pb_online_native_hook.js

echo "Stopping Pushbullet"
adb shell am force-stop com.pushbullet.android

echo "Starting Pushbullet"
adb shell monkey -p com.pushbullet.android -c android.intent.category.LAUNCHER 1
sleep 2
echo "Removing residue JITMF logs"
adb shell rm -rf /sdcard/jitmflogs/*

echo "Removing residue logcat logs"
adb logcat -c

echo "Loop cnt: $z"

mkdir -p $1

for ((i=1; i <= $END_LOOP; i++));
do
    let "j=j+1"
    echo "Generating Noise $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Pushbullet Noise"
    ./phone_automation_scripts/send_sms_andstudio_pxl5.sh $emulator "Noise_$string"
    sleep 2
done

for ((i=1; i <= $ATTACK_END_LOOP; i++));
do
    wait_v=`shuf -i 10-20 -n 1`
    echo "Sleeping for $wait_v"
    sleep $wait_v
    
    echo "Generating Attack $i"
    let "j=j+1"
    $ROOT_EXP_DIR/automate_attack/pushbullet/pushbullet_send_pb_sms_from_browser.sh "Sending_Attack_$j" ${1}/ground_truth_timeline.txt
done

for ((i=1; i <= $END_LOOP; i++));
do
    let "j=j+1"
    echo "Generating Noise $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Pushbullet Noise"
    ./phone_automation_scripts/send_sms_andstudio_pxl5.sh $emulator "Noise_$string"
   
    sleep 2
done
echo "Launching Pushbullet"
adb shell monkey -p com.pushbullet.android -c android.intent.category.LAUNCHER 1
sleep 5

# so as to get the logs at the same time
echo "Gathering baseline logs..."
./pull_files.sh $emulator $scenario $1/baseline

echo "Gathering baseline logs and JIT-MF sources..."
./pull_files.sh $emulator $scenario $1/enhanced process_jitmflogs

echo "Starting baseline timeline..."
# Generate Baseline timeline - using local/custom plaso
export PYTHONPATH=$PYTHONPATH:$ROOT_EXP_DIR/git_repos/plaso
# source ./git_repos/plasoenv/bin/activate
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/log2timeline.py --parsers android $1/baseline/baseline.plaso $1/baseline/data_extracted_msg_send/
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/psort.py -o L2tcsv $1/baseline/baseline.plaso -w $1/baseline/supertimeline.csv
# deactivate

# Generate Baseline timeline - putting all data in an sqlite table
# TO CHANGE
python3 $ROOT_EXP_DIR/experiment_run/create_kb.py $1/baseline/ $scenario

echo "Cleaning JITMF logs..."
python3 $ROOT_EXP_DIR/experiment_run/post-parser.py $1/enhanced/data_extracted_msg_send/sdcard/jitmflogs $1/enhanced/data_extracted_msg_send $scenario

echo "Starting enhanced timeline..."
# Generate JITMF enhanced timeline - using local/custom plaso
# source ./git_repos/plasoenv/bin/activate
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/log2timeline.py --parsers android $1/enhanced/enhanced.plaso $1/enhanced/data_extracted_msg_send/
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/psort.py -o L2tcsv $1/enhanced/enhanced.plaso -w $1/enhanced/supertimeline.csv
# deactivate

# Generate JITMF enhanced timeline - putting all data in an sqlite table
# TO CHANGE
python3 $ROOT_EXP_DIR/experiment_run/create_kb.py $1/enhanced/ $scenario

echo "Remove any residue JIT-MF logs on the device..."
# Remove any residue JIT-MF logs on the device
adb shell rm -rf /sdcard/jitmflogs/*