# python3 query_pushbullet_sp.py 
# $1 => results dir
# comment query() to just get stats. BUt insert query folder name e.g. query_output_<AI>

import sqlite3
from sqlite3 import Error
import sys
import os
import csv
import datetime 
import itertools
import pytz
import json
import statistics
import math

gtt_activity={}
et_activity={}
et_only_activity={}
bt_activity={}

gtt_activity_with_time={}
et_activity_with_time={}
et_only_activity_with_time={}
bt_activity_with_time={}

minimum=-1
maximum=0

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn
# ignoring history clear in both baseline and enhanced because that is my doing
def history(log):
    return_val=False
    if "history" in log.lower() and "clear" in log.lower():
        return_val=True
    else:
        return_val=False
    return return_val

def anomalous_attack(log):
    return_val=False
    erronous_msgs=[]
    # telegram
    # erronous_msgs =['Sending_Attack_449','Sending_Attack_441','Sending_Attack_421','Sending_Attack_405','Sending_Attack_403','Sending_Attack_360','Sending_Attack_351','Sending_Attack_332','Sending_Attack_331','Sending_Attack_315','Sending_Attack_313','Sending_Attack_305','Sending_Attack_296','Sending_Attack_295','Sending_Attack_286','Sending_Attack_269','Sending_Attack_251','Sending_Attack_232','Sending_Attack_187']
    # signal
    # erronous_msgs =['Sending_Attack_9','Sending_Attack_133','Sending_Attack_135','Sending_Attack_333','Sending_Attack_394','Sending_Attack_395','Sending_Attack_396']
    for m in erronous_msgs:
        if m in log:
            return_val=True
            break
    return return_val

def timestamp_from_datetime(dt):
    return dt.replace(tzinfo=pytz.utc).timestamp()

def process_tg_cp_gtt(dir):
    i=1
    with open(os.path.join(dir,"ground_truth_timeline.txt")) as f:
        global minimum
        global maximum
        minimum=-1

        write_to_file(os.path.join(dir,f.name+"_timesketch.csv"),"message,timestamp,datetime,timestamp_desc\n")
        for line in f:
            try:
                time = line.split(" V/[ACC_ATTACK_LOG]")[0]
                current_year = datetime.datetime.now().year     
                # ev_time = str(current_year)+"-"+time.split('.')[0]                
                ev_time = str(current_year)+"-"+time
                # adb logcat gets local datetime
                local_time = pytz.timezone("Europe/Malta")
                test_date = datetime.datetime.strptime(ev_time, "%Y-%m-%d %H:%M:%S.%f")
                local_datetime = local_time.localize(test_date, is_dst=None)
                utc_datetime = local_datetime.astimezone(pytz.utc)

                timestamp=timestamp_from_datetime(utc_datetime)
                iso_date = utc_datetime.isoformat()
                app =  line.split(',')[0].split(': ')[1]
                log_content = line.split(',')[1]    

                if "Sending message" in log_content and not history(log_content) and not anomalous_attack(log_content):
                    event ='Message Sent'
                    message = (log_content.split("'")[1::2])[0].strip()
                    # to = log_content.split(':')[2].strip()
                    to="+35699846611" #my fault for writing ground truth wrong
                    gtt_activity[event+"|"+message+"|"+to]=i
                    gtt_activity_with_time[event+"|"+message+"|"+to]=utc_datetime.replace(tzinfo=None) #removing timezone awareness

                    ts_message = event+": '"+message+"' to: '"+to+"'"
                    string_to_write=ts_message+","+str(timestamp)+","+iso_date+","+"Event Time\n"
                    write_to_file(os.path.join(dir,f.name+"_timesketch.csv"),string_to_write)
                    
                    i+=1
                if "Stolen" in log_content and not history(log_content) and not anomalous_attack(log_content):
                    event = 'Message synced'
                    message = (log_content.split("'")[1::2])[0].strip()
                    from_user = log_content.split(':')[2].strip() #Stealing message: '...' as opposed to Sending message '...'
                            
                    gtt_activity[str(i)+"|"+event]=i
                    gtt_activity_with_time[str(i)+"|"+event]=utc_datetime.replace(tzinfo=None) #removing timezone awareness

                    ts_message = event+": '"+message+"' from: '"+from_user+"'"
                    string_to_write=ts_message+","+str(timestamp)+","+iso_date+","+"Event Time\n"
                    write_to_file(os.path.join(dir,f.name+"_timesketch.csv"),string_to_write)
                    
                    i+=1
                
                if minimum==-1 and len(message)>0:
                    minimum=int(message.split('_')[-1])
                maximum=int(message.split('_')[-1])
            except:
                pass

def process_tg_cp_timelines(dir, file_start,activity_list, activity_time_list,et_only,scenario):
    i=1
    for file in os.listdir(dir):
        if file.startswith(file_start) and not file.endswith("_timesketch.csv"):
            with open(os.path.join(dir,file)) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                file_name = file
                for row in csv_reader:
                    if line_count == 0:
                        write_to_file(os.path.join(dir,file_name+"_timesketch.csv"),"message,timestamp,datetime,timestamp_desc\n")
                    else:
                        produce=False
                        if et_only:
                            if row[2]=="JITMF Event":
                                produce=True
                                file_name = file+"_nonoise"
                        else:
                            produce=True

                        if produce and not history(row[5]):
                            event=row[3].strip()
                            if "message" in event.lower() and "sent" in event.lower():
                                event="Message Sent"
                            ev_object=row[5].strip()
                            ev_subject=row[4].strip()
                            ev_time = row[1]
                            
                            try:
                                test_date = datetime.datetime.strptime(ev_time, "%Y-%m-%d %H:%M:%S")
                                timestamp=timestamp_from_datetime(test_date)
                                # timestamp = (test_date - datetime.datetime(1970, 1, 1)).total_seconds()
                                iso_date = test_date.isoformat()
                            except:
                                test_date=ev_time
                                timestamp=0
                                iso_date='0000-00-00T00:00:00'
                            if scenario=='spying':
                                if(ev_object not in activity_list):    
                                    if "Pushbullet Message Loaded" in event:
                                        event="Message synced"
                                    activity_list[str(i)+"|"+event]=i
                                    activity_time_list[str(i)+"|"+event]=test_date
                                    
                                    ts_message = event+": '"+ev_object+"' to: '"+ev_subject+"'"
                                    

                                    string_to_write=ts_message+","+str(timestamp)+","+iso_date+","+"Event Time\n"

                                    write_to_file(os.path.join(dir,file_name+"_timesketch.csv"),string_to_write)
                                    i+=1
                            else:                            
                                try:
                                    if int(ev_object.split('_')[-1]) < minimum or int(ev_object.split('_')[-1]) > maximum:
                                        pass
                                    else:
                                            
                                        if(ev_object not in activity_list):    
                                            activity_list[event+"|"+ev_object+"|"+ev_subject]=i
                                            activity_time_list[event+"|"+ev_object+"|"+ev_subject]=test_date
                                        
                                        ts_message = event+": '"+ev_object+"' to: '"+ev_subject+"'"
                                        

                                        string_to_write=ts_message+","+str(timestamp)+","+iso_date+","+"Event Time\n"

                                        write_to_file(os.path.join(dir,file_name+"_timesketch.csv"),string_to_write)
                                        i+=1
                                except:
                                    pass
                                            
                    line_count+=1

def write_to_file(file,line):
    with open(file, 'a+') as f:
        f.write(line)

def percentage_missed_events(gtt, lst1):
    gtt_events = set(gtt.keys())
    timeline_events = set(lst1.keys())

    count=0
    for e in gtt_events:
        if e in timeline_events:
            count+=1
    
    total_events = len(gtt_events)
    percentage_missed=((total_events-count)/total_events)*100
    return percentage_missed

def precision_and_recall(gtt, lst1):

    true_positive =len((gtt.keys() & lst1.keys()))
    if len(lst1.keys()) == 0:
        precision = 'None'
    else:    
        precision = true_positive/len(lst1.keys())

    recall = true_positive/len(gtt.keys())
    return precision,recall


# def jaccard_attribution(gtt, lst1, union_len, jaccard_diff):
#     if jaccard_diff !=0:
#         gtt_events = set(gtt.keys())
#         timeline_events = set(lst1.keys())

#         percentage_jaccard_noise = ((len(timeline_events-gtt_events)/union_len)/jaccard_diff)*100
#         percentage_jaccard_missed_events = ((len(gtt_events-timeline_events)/union_len)/jaccard_diff)*100
#     else:
#         percentage_jaccard_noise = 0
#         percentage_jaccard_missed_events = 0
#     # percentage_missed_events = (len(gtt_events-timeline_events)/union_len)*100
#     return percentage_jaccard_noise,percentage_jaccard_missed_events

def jaccard(gtt, lst1):
    list_without_noise = gtt.keys() & lst1.keys()
    
    gtt_list = gtt.keys()

    intersection = len(list_without_noise & gtt_list)    
    union = len(list_without_noise.union(gtt_list))
    return (1 - (float(intersection) / union)) #returning distance and union length

# def jaccard(lst1, lst2):
#     list1=set(lst1.keys())
#     list2=set(lst2.keys())
#     intersection = len(list(list1.intersection(list2)))
    
#     union = len(list1.union(list2))
#     return (1 - (float(intersection) / union)),union #returning distance and union length

def workout_kendalltau(lst1, lst2):           
    # https://stats.stackexchange.com/questions/168602/whats-the-kendall-taus-distance-between-these-2-rankings            
    unioned_activities = list(lst1.keys() & lst2.keys())  # unioned set where one is there but not in other then difference
    pairs = itertools.combinations(unioned_activities, 2)
    
    distance = 0
    for x, y in pairs:
        a = lst1[x] - lst1[y]
        b = lst2[x] - lst2[y]

        #  if discordant (different signs)
        if (a * b < 0):
            # DISCORD
            distance += 1
    
    # not being used - for percentage/normalisation
    if distance != 0:
        len_tot_activities = len(unioned_activities)
        normalised_output = distance/((len_tot_activities*(len_tot_activities-1))/2)
    else:
        normalised_output = 0

    print("Normalised KT: "+str(normalised_output))
    return normalised_output

def workout_time_diff(file, lst1, lst2):
    header ='event,enhanced,gtt,time_diff'
    write_to_file(file, header)
    times=[]
    time2=None   
    l1keys = lst1.keys()
    for elem in l1keys:
        time1 = lst1[elem]
        try:
            if isinstance(lst2[elem],str):
                time2 = datetime.datetime.strptime(lst2[elem],"%Y-%m-%d %H:%M:%S.%f")
            else:
                time2 = lst2[elem]
            
            try:        
                time_diff=time2-time1 #assumption that time from logs is greater then ground truth-lst1
                time_diff_out=time_diff.total_seconds()
                times.append(time_diff_out)
                time_diff_out=str(time_diff_out)+" seconds"
                
            except:
                print(elem)
                time_diff_out='N/A'
        except Exception as e:
            print(e)
            time_diff_out='MISSING EVENT'
            print(time_diff_out)
               
        line=elem+","+ str(time2)+","+str(time1)+","+time_diff_out+"\n" # ONE HOUR LATER
        write_to_file(file, line)
    try:
        result=statistics.stdev(times)
    except:
        result='N/A'
    return result
   
def do_query_to_csv_file(conn, sql, csv_file):
    try:
        cursor = conn.cursor()
        cursor.execute(sql)
        if csv_file != '':
            file_header = ''
            f = open(csv_file, 'w')
            columns = [column[0] for column in cursor.description]
            for col in columns:
                file_header = file_header + col + ','
            f.write(file_header[:-1] + '\n')
            for row in cursor:
                file_row = ''
                for element in row:
                    if element is None:
                        element = ''
                    file_row = file_row + element.__str__().replace(',', ' ') + ','
                f.write(file_row[:-1] + '\n')
            f.close()
    except Exception as e:
        print(e)

def query(rootdir, query_dir,query_name,subject,ev_object,event,date_from,date_to):
    folders=['baseline','enhanced']

    if not os.path.exists(query_dir):
        try:
            os.makedirs(query_dir)
        except OSError as exc: # Guard against race condition
            print(exc)

        sql=  """ SELECT * FROM knowledgebase
            WHERE (subject LIKE '%"""+subject+"""%')
            AND (object LIKE '%"""+ev_object+"""%' )
            AND event LIKE '%"""+event+"""%'
            order by time, object
            """
        # AND (datetime(time) <='"""+date_to+"""')  

    write_to_file(os.path.join(rootdir,query_name+"_events_lists.txt"),"Query: %s \n\n" % sql)
    # or time='00/00/0000 --:--:--' -> ACCOUNTING FOR WALITEAN

    update_noise_sql = """update knowledgebase
            set subject='jabella93'
            where subject like '%Yonleg%'
            and object like '%Noise%'"""
    # attacks that weren't deleted
    # remove_anomalous_attacks = """DELETE FROM KNOWLEDGEBASE 
                                # WHERE OBJECT like '%Sending_Attack_9%'
                                # OR OBJECT like '%Sending_Attack_133%'
                                # OR OBJECT like '%Sending_Attack_135%'
                                # OR OBJECT like '%Sending_Attack_333%'
                                # OR OBJECT like '%Sending_Attack_394%'
                                # OR OBJECT like '%Sending_Attack_395%'
                                # OR OBJECT like '%Sending_Attack_396%'"""

    for f in folders:
        database = os.path.join(rootdir,f,'knowledgebase.db')
        conn = create_connection(database)
        with conn:
            do_query_to_csv_file(conn,update_noise_sql,'')
            # do_query_to_csv_file(conn,remove_anomalous_attacks,'')
            do_query_to_csv_file(conn,sql,os.path.join(query_dir,f+'.csv'))

def process_results(results_csv,basename_rootdir,rootdir,query_name,subject,object,event,start,end,scenario):

    et_activity={}
    et_only_activity={}
    bt_activity={}

    query_dir = os.path.join(rootdir,"query_output_"+query_name)

    query(rootdir,query_dir,query_name,subject,object,event,start,end)

    if scenario=="tg_cp":
        process_tg_cp_timelines(query_dir,"baseline",bt_activity,bt_activity_with_time, False,'cp')
        process_tg_cp_timelines(query_dir,"enhanced",et_activity,et_activity_with_time, False,'cp')
    elif scenario=="tg_sp":
        process_tg_cp_timelines(query_dir,"baseline",bt_activity,bt_activity_with_time, False,'spying')
        process_tg_cp_timelines(query_dir,"enhanced",et_activity,et_activity_with_time, False,'spying')

    # process_tg_cp_timelines(query_dir,"enhanced",et_only_activity,et_only_activity_with_time, True)

    bt_jaccard_dissimilarity = jaccard(gtt_activity, bt_activity)
    et_jaccard_dissimilarity = jaccard(gtt_activity, et_activity)
    print("Jaccard distance between baseline and ground truth timeline: %s" % bt_jaccard_dissimilarity)
    print("Jaccard distance between enhanced and ground truth timeline: %s" % et_jaccard_dissimilarity)

    # bt_jacc_noise=bt_jacc_missed_events=bt_missed_events=et_jacc_noise=et_jacc_missed_events=et_missed_events=0
    # bt_jacc_noise, bt_jacc_missed_events = jaccard_attribution(gtt_activity,bt_activity,bt_union_len,bt_jaccard_dissimilarity)
    # et_jacc_noise, et_jacc_missed_events = jaccard_attribution(gtt_activity,et_activity,et_union_len,et_jaccard_dissimilarity)
    bt_precision, bt_recall = precision_and_recall(gtt_activity,bt_activity)
    en_precision, en_recall = precision_and_recall(gtt_activity,et_activity)

    # et_missed_events=percentage_missed_events(gtt_activity,et_activity)
    # bt_missed_events=percentage_missed_events(gtt_activity,bt_activity)
    print("precision,recall Baseline: %s %s" % (str(bt_precision), str(bt_recall)))
    print("precision,recall Enhanced: %s %s" % (str(en_precision), str(en_recall)))

    gtt_bt_kt_value = workout_kendalltau(gtt_activity,bt_activity)
    print("Kendall Tau GTT vs Baseline Timeline: %s " % gtt_bt_kt_value)
    gtt_et_kt_value = workout_kendalltau(gtt_activity,et_activity)
    print("Kendall Tau GTT vs Enhanced Timeline: %s \n" % gtt_et_kt_value)

    content="""Baseline Jaccard dissimilarity: \t {0}\n
                Enhanced Jaccard dissimilarity: \t {1}\n 
                Precision Baseline: \t {2}\n
                Recall Baseline: \t {3}\n
                Precision Enhanced: \t {4}\n
                Recall Enhanced: \t {5}\n
                Kendall Tau GTT vs Baseline Timeline: \t {7}\n 
                Kendall Tau GTT vs Enhanced Timeline: \t {6}\n 
                """

    file_path=os.path.join(rootdir,query_name+"_events_lists.txt")

    write_to_file(file_path,"Ground Truth Sequence Events:\n")
    write_to_file(file_path,json.dumps(gtt_activity))
    write_to_file(file_path,"\n\nBaseline Sequence Events:\n")
    write_to_file(file_path,json.dumps(bt_activity))
    write_to_file(file_path,"\n\nEnhanced Truth Sequence Events:\n")
    write_to_file(file_path,json.dumps(et_activity))

    write_to_file(file_path,"\n\n\n\nGround Truth Time Events:\n")
    write_to_file(file_path,str(gtt_activity_with_time))
    write_to_file(file_path,"\n\nBaseline Time Events:\n")
    write_to_file(file_path,str(bt_activity_with_time))
    write_to_file(file_path,"\n\nEnhanced Truth Time Events:\n")
    write_to_file(file_path,str(et_activity_with_time))

    write_to_file(results_csv,basename_rootdir+','+query_name+','+str(bt_jaccard_dissimilarity)+','+str(et_jaccard_dissimilarity)+','+str(bt_precision)+','+str(bt_recall)+','+str(en_precision)+','+str(en_recall)+','+str(gtt_bt_kt_value)+','+str(gtt_et_kt_value)+'\n')

    write_to_file(os.path.join(rootdir,query_name+"_results.txt"), content.format(bt_jaccard_dissimilarity,et_jaccard_dissimilarity,bt_precision,bt_recall,en_precision,en_recall,gtt_et_kt_value,gtt_bt_kt_value))

parentdir = sys.argv[1]
_, subdirs,_ = next(os.walk(parentdir)) # only get immediate children
results_csv=os.path.join(parentdir,'results.csv')
write_to_file(results_csv,'run,query_name,GTT BL Jacc diss.,GTT Enh. Jacc diss.,BL Precision.,BL Recall,Enh Precision,Enh Recall,Kendall Tau GTT vs Baseline,Kendall Tau GTT vs Enh.\n')
for rootdir in subdirs:
    print("\n\n"+rootdir)
    gtt_activity={}
    full_rootdir_path=os.path.join(parentdir,rootdir)
    process_tg_cp_gtt(full_rootdir_path)
    if len(gtt_activity) !=0:
        # PB For SP
        process_results(results_csv,rootdir,full_rootdir_path,'ai','+35679928853','','','[DATE_FROM]','2021-03-26 10:00:00','tg_sp')
        process_results(results_csv,rootdir,full_rootdir_path,'aii','','confidential','message','[DATE_FROM]','2021-03-26 23:59:59','tg_sp')
        process_results(results_csv,rootdir,full_rootdir_path,'aiii','','','message loaded','[DATE_FROM]','2021-03-26 23:59:59','tg_sp')
   
        # PB For CP
        # process_results(results_csv,rootdir,full_rootdir_path,'ai','+35699846611','','','2021-03-1 15:00:00','2021-03-31 20:00:00','tg_sp')
        # process_results(results_csv,rootdir,full_rootdir_path,'aii','','attack','','2021-03-1 15:00:00','2021-03-31 20:00:00','tg_sp')
        # process_results(results_csv,rootdir,full_rootdir_path,'aiii','','','message sent','2021-03-1 15:00:00','2021-03-31 20:00:00','tg_sp')

std_dev_et = workout_time_diff(os.path.join(parentdir,'total_time_diff_enh_gtt.csv'),gtt_activity_with_time,et_activity_with_time)
write_to_file(results_csv,'\nStdDev GTT vs Enh. time event recorded,'+str(std_dev_et))
std_dev_bt = workout_time_diff(os.path.join(parentdir,'total_time_diff_enh_gtt.csv'),gtt_activity_with_time,bt_activity_with_time)
write_to_file(results_csv,'\nStdDev GTT vs Enh. time event recorded,'+str(std_dev_bt))

    # print("\n\nStandard Deviation in Time sent is: %s " % std_dev)
