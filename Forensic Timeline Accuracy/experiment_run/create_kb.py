# processing_csv enhanced
# processing_csv baseline

import sqlite3
import csv
import sys
import json
import re
import os
from datetime import datetime
import subprocess
import fnmatch
import pytz
import json

parent_dir = sys.argv[1]
scenario = sys.argv[2]
filepath = os.path.join(parent_dir,"supertimeline.csv")

def process_pushbullet_database(parent_dir, line_count):
    signal_db = os.path.join(parent_dir,"data_extracted_msg_send","data","com.pushbullet.android","databases","syncables.db")
    conn = sqlite3.connect(signal_db)
    cursor = conn.cursor()

    conn2 = sqlite3.connect(os.path.join(parent_dir,'knowledgebase.db'))
    cursor2 = conn2.cursor()
    sql_insert = ''' INSERT INTO knowledgebase(id, time, source, event, subject, object)
            VALUES(?,?,?,?,?,?) '''

    dataset = cursor.execute(''' select a.data, a.modified
                                    from texts a ''').fetchall()
    for row in dataset:
        data = json.loads(row[0])
        time = row[1]
        try:
            if data["data"]["status"]=="sent":
                event="Message Sent"
        except:
            event="Message Event"

        try:
            event_time = datetime.utcfromtimestamp(int(data["data"]["timestamp"]))
        except:
            try:
                event_time = datetime.utcfromtimestamp(int(time))
            except:
                event_time = "00/00/0000 --:--:--"

        data_tuple=(line_count, event_time, "Pushbullet Database", event, data["data"]["addresses"][0], data["data"]["message"])
        count = cursor2.execute(sql_insert,data_tuple)
        conn2.commit()    
        line_count+=1
    
    conn.close()   
    conn2.close()   

def process_signal_database(parent_dir, line_count):
    signal_db = os.path.join(parent_dir,"backup_database","signal_backup_decrypted","database.sqlite")
    conn = sqlite3.connect(signal_db)
    cursor = conn.cursor()

    conn2 = sqlite3.connect(os.path.join(parent_dir,'knowledgebase.db'))
    cursor2 = conn2.cursor()
    sql_insert = ''' INSERT INTO knowledgebase(id, time, source, event, subject, object)
            VALUES(?,?,?,?,?,?) '''

    dataset = cursor.execute(''' select a.date_sent, a.body, b.phone, b._id 
                                    from sms a, recipient b
                                    where a.address = b._id ''').fetchall()
    for row in dataset:
        if row[3]!=1:
            event="Signal Message Sent"
        else:
            event="Signal Message Received"
        event_time = datetime.utcfromtimestamp(int(row[0])/1000.0)
        data_tuple=(line_count, event_time, "Signal Event", event, row[2], row[1])
        count = cursor2.execute(sql_insert,data_tuple)
        conn2.commit()    
        line_count+=1
    
    conn.close()   
    conn2.close() 

def process_logcat(parent_dir, line_count):
    conn = sqlite3.connect(os.path.join(parent_dir,'knowledgebase.db'))
    cursor = conn.cursor()
    with open(os.path.join(parent_dir,"logcat.txt")) as f:
        for line in f:
            content = line.split(": ")
            desc = content[1]
            init = content[0]

            day = init.split(" ")[0]
            time = init.split(" ")[1]
            title = init.split(" ")[2]

            if "[ACC_ATTACK_LOG]" in title:
                pass
            else:
                current_year = datetime.now().year        
                ev_time = str(current_year)+"-"+day+" "+time
                local_time = pytz.timezone("Europe/Malta")
                test_date = datetime.strptime(ev_time, "%Y-%m-%d %H:%M:%S.%f")
                local_datetime = local_time.localize(test_date, is_dst=None)
                utc_datetime = local_datetime.astimezone(pytz.utc)

                source = "Logcat"


                sql_insert = ''' INSERT INTO knowledgebase(id, time, source, event, subject, object)
                        VALUES(?,?,?,?,?,?) '''

                data_tuple=(line_count, utc_datetime, source,title,desc,"")
                count = cursor.execute(sql_insert,data_tuple)
                conn.commit()    
                line_count+=1
    conn.close

def process_wal(parent_dir, line_count):
    for f in os.listdir(os.path.join(parent_dir,"walitean_out")):
        if fnmatch.fnmatch(f,'telegram_walitean.sqlite'):
            walitean_db = os.path.join(parent_dir,"walitean_out","telegram_walitean.sqlite")
            conn = sqlite3.connect(walitean_db)
            cursor = conn.cursor()

            with open(os.path.join(parent_dir,"walitean_out","telegram_walitean.tmp"), "wb") as f:
                dataset = cursor.execute(''' select unknown4 from IIUIBUUIUUU ''').fetchall()
                for row in dataset:
                    f.write(row[0])
                    f.write(b'------END OF ROW----------')
        
            conn.close()   
            tmpfile = os.path.join(parent_dir,"walitean_out","telegram_walitean.tmp")
            result = subprocess.run(["strings",tmpfile], stdout=subprocess.PIPE)
            
            event = "Telegram Message Event"
            event_subject = ""
            event_time = "00/00/0000 --:--:--"
            source = "cache4.db-wal Walitean"
            event_object = ""

            conn = sqlite3.connect(os.path.join(parent_dir,'knowledgebase.db'))
            cursor = conn.cursor()
            sql_insert = ''' INSERT INTO knowledgebase(id, time, source, event, subject, object)
                    VALUES(?,?,?,?,?,?) '''

            for s in result.stdout.splitlines():
                s = s.decode('utf-8')
                if("------END OF ROW----------" in s):
                    data_tuple=(line_count, event_time, source, event,event_subject,event_object)
                    count = cursor.execute(sql_insert,data_tuple)
                    conn.commit()    
                    line_count+=1

                    event_object = ""
                else:
                    event_object = event_object+" "+s #maybe put in obj

            os.remove(os.path.join(parent_dir,"walitean_out","telegram_walitean.tmp"))
            conn.close() 

conn = sqlite3.connect(os.path.join(parent_dir,'knowledgebase.db'))
cursor = conn.cursor()

cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='knowledgebase' ''')

#if the count is 1, then table exists
if cursor.fetchone()[0]==1 : 
	print('Table exists.')
else :
    cursor.execute('''CREATE TABLE knowledgebase (id int, time text, source text, event text, subject text, object text)''')

sql_insert = ''' INSERT INTO knowledgebase(id, time, source, event, subject, object)
              VALUES(?,?,?,?,?,?) '''

line_count = 0

with open(filepath) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    for row in csv_reader:
        if line_count == 0:
            pass
        else:
            
            date = row[0]
            time = row[1]
            source = row[5]
            extra_text = row[16]

            try:
                event_time = datetime.strptime(date+' '+time,'%m/%d/%Y %H:%M:%S')
            except:
                event_time = date+' '+time
            event_subject=''
            event=''
            event_object=''

            if source =="File stat":
                event=row[6]
                event_object=row[12]
                if '/data/org.telegram.messenger' in event_object:
                    event_subject = 'Telegram Process'

            # OBJECT is the message being sent/read
            # SUBJECT is the recipient of the message in CRIME PROXY
            # SUBJECT is the sender of the message in SPYING


            elif source =="JITMF Event": 
                extra = dict(i.split(':') for i in extra_text.split(';'))
                event = extra["event"].strip()

                json_data = row[10].split("Content:")[1].replace('"  "','","')
                data = json.loads(json_data)
                event_object = json.dumps(data['text']).strip('"')
                event_subject = json.dumps(data['to_name']).strip('"')  

                if event_subject=="null" or len(event_subject) ==0:
                    event_subject= json.dumps(data['to_phone']).strip('"')   

                if 'Telegram Message Sent' in event or 'Signal Message Sent' in event:
                    event_time = datetime.utcfromtimestamp(int(json.dumps(data['date']).strip('"'))/1000.0) #in case of ms
    
            elif source =="Telegram Event":
                telegram_logtype = row[9]
                if telegram_logtype == "Source: messages":
                    match1 = re.search("from_user: .*? \(owner\); id: .*? to_user: (.*?);", extra_text)
                    match2 = re.search("from_user: (.*?); .*? to_user: .*? \(owner\); type:", extra_text)
                    if match1:
                        event = "Telegram Message Sent"
                        event_subject = match1.group(1)
                        event_object = row[10].split("Content:")[1]
                    elif match2:
                        event = "Telegram Message Received"
                        event_subject = match2.group(1)
                        event_object = row[10].split("Content:")[1]
                    else:
                        print("Couldnt parse 1: "+str(row))
                elif telegram_logtype == "Source: users":
                    event = "User Status Update"
                    event_object = row[10].split("Content:")[1]
                elif telegram_logtype == "Source: dialogs":
                    event = "Chat Last Update"
                    event_object = row[10].split("Content:")[1]
                else:
                    print("Couldnt parse 2: "+str(row))
            elif source =="Android SMS messages":
                data = row[10].split(' ')
                sent_or_received = data[1]
                sender_recipient=data[3]
                event=source+" "+sent_or_received
                event_object=row[9]
                event_subject = sender_recipient
                source="mmssms.db"

            else:
                print("Couldnt parse 3: "+str(row))

            data_tuple=(line_count, event_time, source, event,event_subject,event_object)
            count = cursor.execute(sql_insert,data_tuple)
            conn.commit()    
        line_count+=1
conn.close()    

print("Finished processing Plaso data")

if "telegram" in scenario:
    process_wal(parent_dir,line_count)       
    print("Finished processing WAL database")
elif "signal" in scenario:
    process_signal_database(parent_dir,line_count)
    print("Finished processing signal database")
    process_logcat(parent_dir,line_count)
    print("Finished processing logcat")
elif "pushbullet" in scenario:
    process_pushbullet_database(parent_dir,line_count)
    print("Finished processing pushbullet database")
    process_logcat(parent_dir,line_count)
    print("Finished processing logcat")

