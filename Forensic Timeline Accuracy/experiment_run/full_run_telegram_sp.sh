#!/bin/bash
# $1 - parent dir to store results in 

j=$2 # full-run.sh will give "current" count
END_LOOP=$3 # full-run.sh will give number of "normal" runs
ATTACK_END_LOOP=$4 # full-run.sh will give number of attack runs

# NO_RUNS=$6
emulator=emulator-5554
scenario=telegram_sp

echo "Starting: $scenario"

echo "Pushing the driver $JITMF_PATH/resources/drivers/sp_tg_online_native_hook.js"
adb push $JITMF_PATH/resources/drivers/sp_tg_online_native_hook.js /data/local/tmp/cp_tg_online_native_hook.js

echo "Stopping Telegram"
adb shell am force-stop org.telegram.messenger

echo "Removing residue JITMF logs"
adb shell rm -rf /sdcard/jitmflogs/*

echo "Removing residue logcat logs"
adb logcat -c

echo "Loop cnt: $z"
# Generate noise
for ((i=1; i <= $END_LOOP; i++));
do
    let "j=j+1"
    echo "Generating Confidential_Info $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Telegram Attack"
    ./phone_automation_scripts/send_tel_msg_andstudio_pxl5.sh $emulator "Confidential_Info_$string"
    
    sleep 2
done

curr_dir=`pwd`
cd $ROOT_EXP_DIR/automate_attack
wait_v=`shuf -i 10-20 -n 1`
echo "Sleeping for $wait_v"
sleep $wait_v

for ((i=1; i <= $ATTACK_END_LOOP; i++));
do    
    echo "Generating Attack $i"
    let "j=j+1"
    ./activate_accessibility_payload_pxl5.sh $emulator
    ./run_attack.sh $scenario $j
    adb shell am force-stop org.telegram.messenger
done
cd $curr_dir

mkdir -p $1
echo "Ground Truth - every 50 messages"
adb logcat -d -v time -s  "[ACC_ATTACK_LOG]" >> ${1}/ground_truth_timeline.txt

# so as to get the logs at the same time
echo "Gathering baseline logs..."
./pull_files.sh $emulator $scenario $1/baseline
echo "Gathering baseline logs and JIT-MF sources..."
./pull_files.sh $emulator $scenario $1/enhanced process_jitmflogs

echo "Parsing baseline cache4.db..."
mkdir $1/baseline/teleparser_out/
python3 $ROOT_EXP_DIR/git_repos/teleparser/teleparser.py $1/baseline/data_extracted_msg_send/data/org.telegram.messenger/files/cache4.db $1/baseline/teleparser_out/

echo "Parsing baseline cache4.db-wal..."
mkdir $1/baseline/walitean_out/
python3 $ROOT_EXP_DIR/git_repos/walitean/walitean.py -f $1/baseline/data_extracted_msg_send/data/org.telegram.messenger/files/cache4.db-wal -x $1/baseline/walitean_out/telegram_walitean.sqlite

cp $1/baseline/teleparser_out/timeline.csv $1/baseline/data_extracted_msg_send/timeline.csv
echo "Starting baseline timeline..."
# Generate Baseline timeline - using local/custom plaso
export PYTHONPATH=$PYTHONPATH:$ROOT_EXP_DIR/git_repos/plaso
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/log2timeline.py --parsers android $1/baseline/baselinse.plaso $1/baseline/data_extracted_msg_send/
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/psort.py -o L2tcsv $1/baseline/baselinse.plaso -w $1/baseline/supertimeline.csv
# Generate Baseline timeline - putting all data in an sqlite table
python3 $ROOT_EXP_DIR/experiment_run/create_kb.py $1/baseline/ $scenario

# Collection of JITMF enhanced forensic sources

echo "Parsing enhanced cache4.db..."
mkdir $1/enhanced/teleparser_out
python3 $ROOT_EXP_DIR/git_repos/teleparser/teleparser.py $1/enhanced/data_extracted_msg_send/data/org.telegram.messenger/files/cache4.db $1/enhanced/teleparser_out/
cp $1/enhanced/teleparser_out/timeline.csv $1/enhanced/data_extracted_msg_send/timeline.csv

echo "Parsing enhanced cache4.db-wal..."
mkdir $1/enhanced/walitean_out/
python3 $ROOT_EXP_DIR/git_repos/walitean/walitean.py -f $1/enhanced/data_extracted_msg_send/data/org.telegram.messenger/files/cache4.db-wal -x $1/enhanced/walitean_out/telegram_walitean.sqlite

echo "Cleaning JITMF logs..."
python3 $ROOT_EXP_DIR/experiment_run/post-parser.py $1/enhanced/data_extracted_msg_send/sdcard/jitmflogs $1/enhanced/data_extracted_msg_send $scenario

echo "Starting enhanced timeline..."
# Generate JITMF enhanced timeline - using local/custom plaso
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/log2timeline.py --parsers android $1/enhanced/enhanced.plaso $1/enhanced/data_extracted_msg_send/
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/psort.py -o L2tcsv $1/enhanced/enhanced.plaso -w $1/enhanced/supertimeline.csv
# Generate JITMF enhanced timeline - putting all data in an sqlite table
python3 $ROOT_EXP_DIR/experiment_run/create_kb.py $1/enhanced/ $scenario

echo "Remove any residue JIT-MF logs on the device..."
# Remove any residue JIT-MF logs on the device
adb shell rm -rf /sdcard/jitmflogs/*

# to query knowledgebase
# python3 query.py $1/baseline/ $1/baseline/supertimeline.sqlite
